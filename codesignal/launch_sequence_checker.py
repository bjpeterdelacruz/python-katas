"""
Launch Sequence Checker

:Author: BJ Peter Dela Cruz
"""
from collections import OrderedDict


def launch_sequence_checker(system_names: list[str], step_numbers: list[int]) -> bool:
    """
    This function verifies that all the system sequences are in strictly increasing order.

    :param system_names: The input list of system names
    :param step_numbers: The input list of values
    :return: True if all the system sequences are in strictly increasing order, False otherwise
    """
    sequences = OrderedDict()
    for idx, _ in enumerate(system_names):
        if system_names[idx] in sequences:
            sequences[system_names[idx]].append(step_numbers[idx])
        else:
            sequences[system_names[idx]] = [step_numbers[idx]]
    for value in sequences.values():
        for index in range(1, len(value)):
            if value[index - 1] >= value[index]:
                return False
    return True
