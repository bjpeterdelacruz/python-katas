"""
Shopping Cart

:Author: BJ Peter Dela Cruz
"""
from collections import OrderedDict


def shopping_cart(requests: list[str]) -> list[str]:
    """
    This function processes a list of requests for an online shopping cart. The requests are: add
    (adds an item to the cart), remove (removes an item from the cart), quantity_upd (either adds or
    subtracts a number for an item in the cart), and checkout (clears the cart).

    :param requests: The input list of strings representing requests
    :return: A list of strings representing the final state of the shopping cart
    """
    cart = OrderedDict()
    for request in requests:
        if request == "checkout":
            cart.clear()
            continue
        arr = request.split(":")
        command = arr[0].strip()
        item = arr[1].strip()
        if command == "quantity_upd":
            quantity = arr[2].strip()
            cart[item] += int(quantity[1:]) if quantity[0] == '+' else int(quantity)
        elif command == "add":
            cart[item] = 1
        elif command == "remove":
            del cart[item]
    return [f"{key} : {value}" for key, value in cart.items()]
