"""
Fare Estimator

:Author: BJ Peter Dela Cruz
"""


def fare_estimator(ride_time: int, ride_distance: int, cost_per_minute: list[float],
                   cost_per_mile: list[float]) -> list[float]:
    """
    This function returns a list of fare estimates for each Uber car type.

    :param ride_time: The ride duration in minutes
    :param ride_distance: The ride distance in miles
    :param cost_per_minute: A list of costs per minute for each car type
    :param cost_per_mile: A list of costs per mile for each car type
    :return: A list of fare estimates for each car type
    """
    results = [ride_time * cost + ride_distance * cost_per_mile[idx]
               for idx, cost in enumerate(cost_per_minute)]
    return list(map(lambda result: float(f"{result:.2f}"), results))
