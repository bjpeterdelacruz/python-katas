"""
Find Middle Element

:Author: BJ Peter Dela Cruz
"""

# pylint: disable=too-few-public-methods


class ListNode:
    """
    This class represents a node in a singly-linked list.
    """
    def __init__(self, value):
        """
        Initializes a node in a singly-linked list.

        :param value: The value of the node
        """
        self.value = value
        self.next = None


def find_middle_element(linked_list: ListNode) -> int:
    """
    This function finds the value of the middle node in :attr:`linked_list`. If the list has an even
    number of nodes, then there will be two middle nodes. In this case, the value of the second
    middle node will be returned.

    :param linked_list: A singly-linked list
    :return: The value of the node in the middle of the linked list
    """
    length = 0
    current_node = linked_list
    while True:
        if current_node:
            length += 1
        if not current_node.next:
            break
        current_node = current_node.next
    middle_position = int(length / 2)

    current_node = linked_list
    current_position = 0
    while True:
        if current_position == middle_position:
            return current_node.value
        current_position += 1
        current_node = current_node.next
