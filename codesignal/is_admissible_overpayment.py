"""
Is Admissible Overpayment

:Author: BJ Peter Dela Cruz
"""


def is_admissible_overpayment(prices: list[int], notes: list[str], sensitivity: int) -> bool:
    """
    This function determines whether a given customer will be willing to pay for the items in their
    cart based on their price sensitivity.

    :param prices: The input list of integers that represent prices for items
    :param notes: The input list of strings that represent notes for each item
    :param sensitivity: The customer's price sensitivity
    :return: True if the calculation for the customer's price sensitivity is below
        :attr:`sensitivity`, False otherwise
    """
    total = 0
    for idx, _ in enumerate(prices):
        note = notes[idx].split()
        online = prices[idx]
        try:
            percentage = float(note[0][:-1])
            if note[1] == "lower":
                in_store = online / (100 - percentage) * 100
                total -= (in_store - online)
            else:
                in_store = online / (100 + percentage) * 100
                total += (online - in_store)
        except ValueError:
            total += 0
    return total <= sensitivity
