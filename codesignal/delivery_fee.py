"""
Delivery Fee

:Author: BJ Peter Dela Cruz
"""
from datetime import datetime


def delivery_fee(intervals, fees, deliveries):
    """
    This function returns True if the delivery fee is directly correlated to the order volume in
    each interval, i.e. the value computed by dividing the fee for an interval by the number of
    deliveries in that interval is constant for all intervals.

    :param intervals: The list of intervals
    :param fees: The list of fees
    :param deliveries: The list of times at which a delivery was made
    :return: True if the delivery fee is directly correlated to the order volume in each interval,
        False otherwise
    """
    start_times = []
    end_times = []
    delivery_times = []
    for interval in intervals:
        start_times.append(datetime.strptime(str(interval), '%H'))
    for idx in range(1, len(intervals)):
        end_times.append(datetime.strptime(str(intervals[idx] - 1) + ":59", '%H:%M'))
    end_times.append(datetime.strptime("23:59", '%H:%M'))
    for delivery in deliveries:
        delivery_times.append(datetime.strptime(f"{delivery[0]}:{delivery[1]}", '%H:%M'))
    buckets = {idx: [] for idx, _ in enumerate(intervals)}
    for delivery_time in delivery_times:
        for idx, _ in enumerate(start_times):
            if start_times[idx] <= delivery_time <= end_times[idx]:
                buckets[idx].append(delivery_time)
    ratios = set()
    for key, value in buckets.items():
        if fees[key] == 0:
            ratios.add(0)
        else:
            ratios.add(len(value) / fees[key])
    return len(ratios) == 1
