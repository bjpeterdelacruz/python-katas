"""
Company Bot Strategy

:Author: BJ Peter Dela Cruz
"""


def company_bot_strategy(training_data: list[list[int]]) -> int:
    """
    This function returns a bot's average answer time, given a list of pairs. Each pair consists of
    a bot's answer time and correctness. If correctness is 1, the bot answered correctly. If
    correctness is -1, the bot answered incorrectly. If correctness is 0, the bot did not answer at
    all. Only the bot's answer time is used in the calculation of the average if the correctness is
    1. If correctness is 0, the bot's answer time is also 0.

    :param training_data: The list of training data, which consists of a bot's answer time and
        correctness
    :return: The average answer time
    """
    total = 0
    count = 0
    for data in training_data:
        if data[1] == 1:
            total += data[0]
            count += 1
    return 0 if count == 0 else total / count
