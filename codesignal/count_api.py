"""
Count API

:Author: BJ Peter Dela Cruz
"""


# pylint: disable=too-many-branches

def count_api(calls: list[str]) -> list[str]:
    """
    This function counts the number of each API call in :attr:`calls` and returns a list of strings
    that represents all the projects, subprojects, and methods of the API, along with the number of
    calls, sorted in the same order as they are in :attr:`calls`. Each project in the output list
    has two hyphens in front of it, each subproject has four hyphens in front of it, and each method
    has six hyphens in front of it.

    :param calls: The input list of strings representing API calls
    :return: A list of strings representing API calls in a hierarchical structure along with their
        counts
    """
    projects = {}
    counts = {}
    for call in calls:
        project, subproject, method = call[1:].split("/")
        if project not in counts:
            counts[project] = 1
        else:
            counts[project] += 1
        if f"{project}/{subproject}" not in counts:
            counts[f"{project}/{subproject}"] = 1
        else:
            counts[f"{project}/{subproject}"] += 1
        if f"{project}/{subproject}/{method}" not in counts:
            counts[f"{project}/{subproject}/{method}"] = 1
        else:
            counts[f"{project}/{subproject}/{method}"] += 1
        if project not in projects:
            subprojects = {subproject: [method]}
            projects[project] = subprojects
        elif subproject not in projects[project]:
            projects[project][subproject] = [method]
        elif method not in projects[project][subproject]:
            projects[project][subproject].append(method)
    api_calls = []
    for project_name, subprojects in projects.items():
        api_calls.append(f"--{project_name} ({counts[project_name]})")
        for subproject_name, methods in subprojects.items():
            api_calls.append(
                f"----{subproject_name} ({counts[project_name + '/' + subproject_name]})")
            for method_name in methods:
                api_calls.append(
                    f"------{method_name}"
                    f" ({counts[project_name + '/' + subproject_name + '/' + method_name]})")
    return api_calls
