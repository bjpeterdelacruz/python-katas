"""
Linked List Utility Classes and Functions

:Author: BJ Peter Dela Cruz
"""

# pylint: disable=too-few-public-methods


class Node:
    """
    This class represents a node in a singly-linked list.
    """
    def __init__(self, data):
        if not data:
            raise ValueError("data must not be None")
        self.data = data
        self.next = None

    def __str__(self):
        """
        Prints the contents of this node: the data and the pointer to the next node or None

        :return: A string representing the contents of this node
        """
        return f"Node(data = {self.data}, next = {self.next})"


def push(head, data) -> Node:
    """
    This function creates a new node if head is None. If data is not None, a new node is created,
    and the next pointer is set to :attr:`head`, thus making the new node the new head of the linked
    list.

    :param head: The head of a linked list
    :param data: The data to add to the linked list
    :return: A new node that will be the new head of the linked list
    """
    if not head:
        return Node(data)
    new_head = Node(data)
    new_head.next = head
    return new_head


def build_one_two() -> Node:
    """
    This function builds the following linked list: 1 -> 2 -> None

    :return: A linked list with two elements
    """
    return push(push(None, 2), 1)


def build_two_one() -> Node:
    """
    This function builds the following linked list: 2 -> 1 -> None

    :return: A linked list with two elements
    """
    return push(push(None, 1), 2)


def build_one_two_three() -> Node:
    """
    This function builds the following linked list: 1 -> 2 -> 3 -> None

    :return: A linked list with three elements
    """
    return push(push(push(None, 3), 2), 1)


def build_three_two_one() -> Node:
    """
    This function builds the following linked list: 3 -> 2 -> 1 -> None

    :return: A linked list with three elements
    """
    return push(push(push(None, 1), 2), 3)


def build_nodes(arr) -> Node:
    """
    This function creates a new linked list from the given list of data.

    :param arr: A list of data from which to build a new linked list
    :return: A new linked list
    """
    node = None
    for element in arr:
        node = push(node, element)
    return node
