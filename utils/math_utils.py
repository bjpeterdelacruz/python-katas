"""
This script contains math utility functions.

:Author: BJ Peter Dela Cruz
"""
from math import sqrt


def sieve_of_eratosthenes(maximum: int) -> list[int]:
    """
    This function implements the Sieve of Eratosthenes algorithm, which will produce an array of
    prime numbers from 2 to square root of :attr:`maximum`.

    :param maximum: The upper limit for the algorithm
    :return: An array of prime numbers between 2 and square root of :attr:`maximum`
    """
    arr = [True for _ in range(0, maximum)]
    for outer_idx in range(2, int(sqrt(maximum))):
        if arr[outer_idx]:
            inner_idx = 0
            pos = (outer_idx ** 2) + (inner_idx * outer_idx)
            while pos < maximum:
                arr[pos] = False
                inner_idx += 1
                pos = (outer_idx ** 2) + (inner_idx * outer_idx)
    arr = [idx for idx, boolean in enumerate(arr) if boolean]
    arr.remove(0)
    arr.remove(1)
    return arr
