import os


dirs = [('coderbyte', 'tests/coderbyte'), ('codesignal', 'tests/codesignal'),
        ('codewars', 'tests/codewars'), ('leetcode', 'tests/leetcode')]
for directories in dirs:
    code = [file[:file.index('.py')] for file in os.listdir(directories[0])
            if '.py' in file and file != "__init__.py"]
    tests = [test_file[:test_file.index('_test.py')] for test_file in os.listdir(directories[1])
             if '_test.py' in test_file]
    files = [file for file in code if file not in tests]
    for file in files:
        with open(f'{directories[1]}/{file}_test.py', 'w') as f:
            f.writelines([])
