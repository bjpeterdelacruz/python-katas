# Python Katas

Katas are programming exercises designed to help programmers improve their skills through practice and repetition.

Number of completed katas as of **May 14, 2023**:

- Coderbyte: 74
- Codesignal: 8
- Codewars: 614
- LeetCode: 8
- **TOTAL: 704**

## Getting Started

Create a new environment and then activate it:

```
python3 -m venv venv
source venv/bin/activate
```

Install all the packages that are listed in the `requirements.txt` file.

```
pip3 install -r requirements.txt
```

Execute unit tests and generate a code coverage report:

```
pytest --cov=coderbyte --cov=codesignal --cov=codewars --cov=leetcode --cov-report=html
```

Execute `pylint` on all source code files:

```
./pylint.sh
```

Create documentation using Sphinx:

```
cd docs
make docs && make clean html
```

### Links

- [Online Documentation](https://bjdelacruz.dev/python/katas/index.html)

#### Profiles

- [Developer's Website](https://bjdelacruz.dev)
- [Coderbyte](https://coderbyte.com/profile/bjpeterdelacruz)
- [Codesignal](https://app.codesignal.com/profile/bjpeter)
- [Codewars](https://www.codewars.com/users/bjpeterdelacruz/stats)
- [LeetCode](https://leetcode.com/bjpeterdelacruz)
