"""
Off Binary

:Author: BJ Peter Dela Cruz
"""


def off_binary(str_arr: list[str]) -> int:
    """
    This function takes a list of two strings: the first string represents a positive decimal number
    and the second string represents a binary number. This function determines the number of digits
    in the binary number that need to be changed to correctly represent the first decimal number.

    For example, given "56" and "011000", this function will return 1 because only one digit needs
    to be changed in the binary number (the first 0 needs to be changed to 1) to correctly represent
    56 in binary.

    :param str_arr: The input list of strings
    :return: The number of digits that need to be changed in the binary number
    """
    binary = str_arr[1]
    number_as_binary = ("{0:" + str(len(binary)) + "b}").format(int(str_arr[0]))
    count = 0
    for idx, _ in enumerate(number_as_binary):
        if number_as_binary[idx] != binary[idx]:
            count += 1
    return count
