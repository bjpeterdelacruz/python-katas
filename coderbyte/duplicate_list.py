"""
Duplicate List

:Author: BJ Peter Dela Cruz
"""


def duplicate_list(integers: list[int]) -> int:
    """
    This function returns the number of duplicate numbers in :attr:`integers`.

    :param integers: The input list of integers
    :return: The number of duplicate numbers in :attr:`integers`
    """
    integers.sort()
    return sum(1 if integers[idx - 1] == integers[idx] else 0 for idx in range(1, len(integers)))
