"""
Counting Minutes

:Author: BJ Peter Dela Cruz
"""
from datetime import datetime, timedelta


def counting_minutes(string: str) -> int:
    """
    This function takes a string that represents two times and calculates the total number of
    minutes between the two. For example, given the input string "1:23am-1:08am", this function
    will return 1425 (1:08am represents the next day). The times will be in a 12-hour clock format,
    separated by a hyphen.

    :param string: The input string
    :return: The number of minutes between the two times
    """
    times = string.split("-")
    first_time = datetime.strptime(times[0], "%I:%M%p")
    second_time = datetime.strptime(times[1], "%I:%M%p")
    if second_time < first_time:
        second_time += timedelta(days=1)
    diff = second_time - first_time
    hours = diff.seconds // 3600
    return (hours * 60) + (diff.seconds // 60) % 60
