"""
Swap II

:Author: BJ Peter Dela Cruz
"""
import re


def swap_ii(string: str) -> str:
    """
    This function swaps the case of all the letters in :attr:`string`. If one or more letters are
    surrounded by numbers, e.g. "5lol6", then the numbers are switched.

    :param string: The input string
    :return: The same string but with the cases of all the letters swapped and the positions of the
        numbers switched if the numbers surround one or more letters
    """
    result = [char.lower() if 65 <= ord(char) <= 90 else char.upper() for char in string]
    result = ''.join(result)
    matches = re.findall(r"([0-9][A-Za-z]+[0-9])", result)
    for match in matches:
        first_number = match[0]
        last_number = match[-1]
        letters = match[1:-1]
        result = result.replace(match, f"{last_number}{letters}{first_number}")
    return result
