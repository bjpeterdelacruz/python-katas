"""
Reverse Polish Notation

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=eval-used


def reverse_polish_notation(string: str) -> int:
    """
    This function computes the equation in :attr:`string`, which is written in reverse Polish
    notation.

    :param string: The input string, an equation written in reverse Polish notation
    :return: The result of the equation
    """
    arr = []
    for chars in string.split():
        if chars.isnumeric():
            arr.append(chars)
        else:
            num_2 = arr.pop()
            num_1 = arr.pop()
            if num_2 == '0' and chars == '/':
                raise ValueError("Cannot divide by 0")
            arr.append(eval(f"{num_1}{chars}{num_2}"))
    return arr[0]
