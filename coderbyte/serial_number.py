"""
Serial Number

:Author: BJ Peter Dela Cruz
"""


def serial_number(serial: str) -> str:
    """
    This function returns "true" if the given :attr:`serial` is a valid serial number.

    :param serial: The input string that represents a serial number
    :return: "true" if the string is a valid serial number, "false" otherwise
    """
    strings = serial.split('.')
    if len(strings) != 3:
        return "false"
    first, second, third = strings
    if sum(map(int, first)) % 2 != 0:
        return "false"
    if sum(map(int, second)) % 2 == 0:
        return "false"
    last_first, last_second, last_third = int(first[-1]), int(second[-1]), int(third[-1])
    if len(first) == 1 or len(second) == 1 or len(third) == 1:
        return "false"
    if check_string(first[:-1], last_first) and check_string(second[:-1], last_second) and \
            check_string(third[:-1], last_third):
        return "true"
    return "false"


def check_string(string: str, number: int) -> bool:
    """
    This helper function returns True if all the digits in :attr:`string` are less than
    :attr:`number`.

    :param string: A string of numbers
    :param number: The number that each digit in :attr:`string` must be less than
    :return: True if all the digits in :attr:`string` are less than :attr:`number`, False otherwise
    """
    for digit in string:
        if number <= int(digit):
            return False
    return True
