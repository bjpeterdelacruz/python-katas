"""
Simple Mode

:Author: BJ Peter Dela Cruz
"""
from collections import OrderedDict


def simple_mode(int_arr: list[int]) -> int:
    """
    This function returns the number that appears the most frequently (i.e. mode) in
    :attr:`int_arr`. If there is more than one node, the number that appears in the list first will
    be returned.

    :param int_arr: The input list of integers
    :return: The number that appears the most frequently, or -1 if all the numbers appear once (
        i.e. there is no mode)
    """
    if len(int_arr) == 0:
        raise ValueError("list must not be empty")
    numbers = OrderedDict()
    maximum = 1
    for number in int_arr:
        if number in numbers:
            numbers[number] += 1
            if numbers[number] > maximum:
                maximum = numbers[number]
        else:
            numbers[number] = 1
    for number, count in numbers.items():
        if count == maximum and maximum > 1:
            return number
    return -1
