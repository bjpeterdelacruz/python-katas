"""
Array Couples

:Author: BJ Peter Dela Cruz
"""


def array_couples(integers: list[int]) -> str:
    """
    This function determines whether each pair of integers in :attr:`integers` has a corresponding
    reversed pair somewhere else in the list. For example, given [4, 5, 1, 4, 5, 4, 4, 1], this
    function will return "yes" because the first pair "4, 5" has the reversed pair "5, 4" in the
    list, and the next pair "1, 4" has the reversed pair "4, 1".

    If :attr:`integers` does not contain all pairs with their reversed pairs, a string of the
    integer pairs that are incorrect is returned.

    :param integers: The input list of integers, which must contain an even number of elements
    :return: "yes" if all pairs have reversed pairs, or a string containing the incorrect pairs
    """
    if len(integers) % 2 != 0:
        raise ValueError("list must contain an even number of elements")
    int_arr = [(integers[idx - 1], integers[idx]) for idx in range(1, len(integers), 2)]
    index = 0
    while index < len(int_arr):
        tup = int_arr[index]
        (first, second) = tup[1], tup[0]
        if (first, second) in int_arr:
            if first == second:
                if int_arr.count(tup) > 1:
                    int_arr.remove(tup)
                    int_arr.remove(tup)
                else:
                    index += 1
            else:
                int_arr.remove(tup)
                int_arr.remove((first, second))
        else:
            index += 1
    if len(int_arr) == 0:
        return "yes"
    return ','.join(str(tup[0]) + "," + str(tup[1]) for tup in int_arr)
