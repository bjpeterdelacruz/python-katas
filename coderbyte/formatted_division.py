"""
Formatted Division

:Author: BJ Peter Dela Cruz
"""


def formatted_division(numerator: int, denominator: int) -> str:
    """
    This function returns the result of a division as a string with properly formatted commas and
    four significant digits after the decimal place

    :param numerator: The input number
    :param denominator: The input number
    :return: A formatted result with properly formatted commas and four significant digits after the
        decimal place
    """
    return f"{numerator / denominator:,.4f}"
