"""
Find Intersection

:Author: BJ Peter Dela Cruz
"""


def find_intersection(str_arr: list[str]) -> str:
    """
    This function takes a list of two strings. Both strings represent a comma-separated list of
    numbers that are sorted in ascending order. A string representing a comma-separated list of
    numbers that appear in both lists will be returned.

    :param str_arr: The input list of strings
    :return: A string representing a comma-separated list of numbers that appear in both lists
    """
    intersect = list(set(str_arr[0].split(", ")) & set(str_arr[1].split(", ")))
    if len(intersect) == 0:
        return "false"
    return ",".join(map(str, sorted(map(int, intersect))))
