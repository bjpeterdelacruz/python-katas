"""
Tree Constructor

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby
from ast import literal_eval


def tree_constructor(str_arr: list[str]) -> str:
    """
    This function takes a list of strings that will contain pairs of integers in the following
    format: (integer_1, integer_2) where integer_1 represents a child node in a tree and integer_2
    represents the parent node of integer_1.

    This function returns "true" if a valid binary tree can be formed, or "false" otherwise. It is
    assumed that all integers within the tree will be unique, which means that there can only be one
    node in the tree with the given integer value.

    :param str_arr: The input list of strings representing nodes in a tree
    :return: "true" if a valid binary tree can be formed, "false" otherwise
    """
    if len(str_arr) == 0 or str_arr[0] == "":
        raise ValueError("str_array must contain at least one node")

    tuples = [literal_eval(string) for string in str_arr]

    children = {}
    parents = {}
    tuples = sorted(tuples, key=lambda tup: tup[1])
    for parent, child in groupby(tuples, key=lambda tup: tup[1]):
        children[parent] = list(child)
        if len(children[parent]) > 2:
            return "false"
        for child_node in children[parent]:
            parents[child_node[0]] = parent

    root_count = 0
    for parent, _ in children.items():
        if parent not in parents:
            root_count += 1

    return "true" if root_count == 1 else "false"
