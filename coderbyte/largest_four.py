"""
Largest Four

:Author: BJ Peter Dela Cruz
"""


def largest_four(int_arr: list[int]) -> int:
    """
    This function returns the sum of the four largest integers in :attr:`int_arr`.

    :param int_arr: The input list of integers
    :return: The sum of the four largest integers in :attr:`int_arr`
    """
    arr = sorted(int_arr)
    return sum(arr[-4:])
