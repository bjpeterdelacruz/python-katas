"""
Prime Mover

:Author: BJ Peter Dela Cruz
"""
from utils.math_utils import sieve_of_eratosthenes


def prime_mover(position: int) -> int:
    """
    This function returns the :attr:`position`-th prime number.

    :param position: The input integer
    :return: The :attr:`position`-th prime number
    """
    return sieve_of_eratosthenes(10 ** 4)[position - 1]
