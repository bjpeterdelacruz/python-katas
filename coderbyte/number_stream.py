"""
Number Stream

:Author: BJ Peter Dela Cruz
"""


def number_stream(string: str) -> str:
    """
    This function returns "true" if there is a consecutive stream of digits of at least N length
    where N is the actual digit value in :attr:`string`, or "false" otherwise.

    :param string: The input string
    :return: "true" if there is a consecutive stream of digits, "false" otherwise
    """
    numbers = [string[0]]
    idx = 1
    for index in range(1, len(string)):
        if numbers[idx - 1] == string[index]:
            numbers.append(string[index])
            if len(numbers) == int(string[index]):
                return "true"
            idx += 1
        else:
            numbers.clear()
            numbers.append(string[index])
            idx = 1
    return "false"
