"""
Second GreatLow

:Author: BJ Peter Dela Cruz
"""


def second_greatlow(int_arr: list[int]) -> str:
    """
    This function returns the second lowest and second greatest numbers in :attr:`int_arr`,
    respectively. If the length of :attr:`int_arr` is 1, then the second lowest and second greatest
    numbers are the same number. If the length is 2, the highest number is the second lowest, and
    the lowest number is the second highest.

    :param int_arr: The input list of integers
    :return: The second lowest and second highest numbers
    """
    arr = list(sorted(set(int_arr)))
    if len(arr) == 2:
        return f"{arr[1]} {arr[0]}"
    if len(arr) == 1:
        return f"{arr[0]} {arr[0]}"
    return f"{arr[1]} {arr[-2]}"
