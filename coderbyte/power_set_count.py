"""
Power Set Count

:Author: BJ Peter Dela Cruz
"""
from itertools import permutations


def power_set_count(int_arr: list[int]) -> int:
    """
    This function returns the length of the power set of the given list of integers. A power set is
    the set of all subsets of a set.

    :param int_arr: The input list of integers
    :return: The power set of :attr:`int_arr`
    """
    sets = set()
    for count in range(0, len(int_arr) + 1):
        for permutation in permutations(int_arr, count):
            sets.add(tuple(sorted(list(permutation))))
    return len(sorted(list(sets)))
