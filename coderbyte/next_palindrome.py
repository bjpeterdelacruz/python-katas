"""
Next Palindrome

:Author: BJ Peter Dela Cruz
"""


def next_palindrome(number: int) -> int:
    """
    This function returns the next palindrome greater than :attr:`number`. For example, given 100,
    the next palindrome is 101.

    :param number: The input integer
    :return: The next palindrome greater than :attr:`number`
    """
    next_number = number + 1
    while True:
        if str(next_number) == str(next_number)[::-1]:
            return next_number
        next_number += 1
