"""
Non-repeating Character

:Author: BJ Peter Dela Cruz
"""
from collections import OrderedDict


def nonrepeating_character(string: str) -> str:
    """
    This function returns the first non-repeating character in :attr:`string`. :attr:`string` will
    always contain at least one character.

    :param string: The input string
    :return: The first non-repeating character in :attr:`string`
    """
    if len(string) == 0:
        raise ValueError("string must contain at least one character")
    chars = OrderedDict()
    for char in string:
        if char not in chars:
            chars[char] = 1
        else:
            chars[char] += 1
    return next(filter(lambda item: item[1] == 1, chars.items()))[0]
