"""
Swap Case

:Author: BJ Peter Dela Cruz
"""


def swap_case(string: str) -> str:
    """
    This function swaps the case of all the letters in :attr:`string`: lowercase letters are turned
    to uppercase letters, and vice versa.

    :param string: The input string
    :return: The same string but with the cases swapped
    """
    return ''.join([char.lower() if 65 <= ord(char) <= 90 else char.upper() for char in string])
