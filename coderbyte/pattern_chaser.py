"""
Pattern Chaser

:Author: BJ Peter Dela Cruz
"""


def pattern_chaser(string: str) -> str:
    """
    This function finds patterns that match this characteristic: two or more adjacent characters
    within :attr:`string` that is repeated at least twice, and returns the longest pattern in the
    string.

    :param string: The input string
    :return: The longest pattern in :attr:`string`
    """
    substrings = []
    for length in range(2, len(string)):
        for idx, _ in enumerate(string):
            substrings.append(string[idx:idx + length])
    substrings = sorted(list(set(substrings)))
    counts = {substring: string.count(substring) for substring in substrings if string.count(
        substring) >= 2 and len(substring) >= 2}
    longest_substring = ""
    for key in counts.keys():
        if len(key) > len(longest_substring):
            longest_substring = key
    return f"yes {longest_substring}" if longest_substring else "no null"
