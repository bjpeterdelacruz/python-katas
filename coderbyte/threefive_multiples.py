"""
ThreeFive Multiples

:Author: BJ Peter Dela Cruz
"""


def threefive_multiples(number: int) -> int:
    """
    This function returns the sum of all the multiples of 3 and 5 that are below :attr:`number`.

    :param number: The input integer
    :return: The sum of all the multiples of 3 and 5 that are below :attr:`number`
    """
    threes = [3 * idx for idx in range(1, number) if 3 * idx < number]
    fives = [5 * idx for idx in range(1, number) if 5 * idx < number]
    threes.extend(fives)
    return sum(set(threes))
