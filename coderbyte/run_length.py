"""
Run Length

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def run_length(string: str) -> str:
    """
    This function compresses :attr:`string` using the run-length encoding algorithm and returns the
    result. For example, given the input string "wwwggopp", the result will be "3w2g1o2p". It is
    assumed that :attr:`string` will not contain any numbers, punctuation, or symbols.

    :param string: The input string
    :return: A run-length-encoded version of :attr:`string`
    """
    result = []
    for key, value in groupby(string, key=lambda character: character):
        result.append(f"{len(list(value))}{key}")
    return ''.join(result)
