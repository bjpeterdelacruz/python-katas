"""
Distinct Characters

:Author: BJ Peter Dela Cruz
"""


def distinct_characters(string: str) -> str:
    """
    This function returns "true" if :attr:`string` contains at least 10 distinct characters,
    otherwise returns "false".

    :param string: The input string
    :return: "true" if :attr:`string` contains at least 10 distinct characters, "false" otherwise
    """
    return "true" if len(set(string)) >= 10 else "false"
