"""
Array Addition I

:Author: BJ Peter Dela Cruz
"""
from itertools import permutations


def array_addition_i(int_arr: list[int]) -> str:
    """
    This function returns "true" if any combination of the numbers in :attr:`int_arr`, excluding the
    largest number, produces a sum equal to the largest number, or "false" if no combination
    produces a sum.

    :param int_arr: The input list of integers
    :return: "true" if there exists a combination of numbers that produces a sum equal to the
        largest number in :attr:`int_arr`, "false" otherwise
    """
    sorted_list = sorted(int_arr)
    largest_number = sorted_list.pop()
    for counter in range(2, len(sorted_list) + 1):
        for permutation in permutations(sorted_list, counter):
            if sum(permutation) == largest_number:
                return "true"
    return "false"
