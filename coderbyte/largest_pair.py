"""
Largest Pair

:Author: BJ Peter Dela Cruz
"""


def largest_pair(number: int) -> int:
    """
    This function returns the largest two-digit number in :attr:`number`.

    :param number: The input integer
    :return: The largest two-digit number in :attr:`number`
    """
    string = str(number)
    largest_number = int(string[0:2])
    for idx in range(2, len(string)):
        current_number = int(string[idx - 1:idx + 1])
        if current_number > largest_number:
            largest_number = current_number
    return largest_number
