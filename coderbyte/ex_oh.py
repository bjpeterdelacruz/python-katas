"""
Ex Oh

:Author: BJ Peter Dela Cruz
"""


def ex_oh(string: str) -> str:
    """
    This function returns "true" if the number of x's in :attr:`string` is equal to the number of
    o's.

    :param string: The input string
    :return: "true" if there is an equal number of x's and o's in :attr:`string`, "false" otherwise
    """
    counts = {char: string.count(char) for char in string}
    return "true" if counts.get("x", 0) == counts.get("o", 0) else "false"
