"""
Square Figures

:Author: BJ Peter Dela Cruz
"""
from math import sqrt


def square_figures(num: int) -> int:
    """
    This function returns the smallest integer that, when squared, has a length equal to
    :attr:`num`.

    :param num: The input integer
    :return: The smallest integer squared that has a length equal to :attr:`num`
    """
    result = int(sqrt(10 ** (num - 1))) - 1
    while True:
        if len(str(result ** 2)) >= num:
            return result
        result += 1
