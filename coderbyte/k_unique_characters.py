"""
K Unique Characters

:Author: BJ Peter Dela Cruz
"""


def k_unique_characters(string: str) -> str:
    """
    This function returns the longest substring that contains N unique characters, where N is the
    number at position 0 in :attr:`string`.

    :param string: The input string that contains N at position 0 followed by characters
    :return: The longest substring in :attr:`string` that contains N unique characters
    """
    count = int(string[0])
    characters = string[1:]
    words = [characters[idx:idx + length] for length in range(count, len(characters))
             for idx, _ in enumerate(characters)]
    words = sorted(words, key=len)
    longest_word = words[0]
    for word in words:
        if len(set(word)) == count and len(word) > len(longest_word):
            longest_word = word
    return longest_word
