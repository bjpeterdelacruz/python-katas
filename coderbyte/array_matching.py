"""
Array Matching

:Author: BJ Peter Dela Cruz
"""
from ast import literal_eval


def array_matching(int_arr: list[str]) -> str:
    """
    This function sums the elements at indexes in :attr:`int_arr[0]` with the elements at the same
    indexes in :attr:`int_arr[1]`. If one of the lists is longer than the other, then the elements
    in the longer list are appended to the end of the list containing the sums.

    :param int_arr: The input list of integers
    :return: A list containing the sums
    """
    arr_1 = literal_eval(int_arr[0])
    arr_2 = literal_eval(int_arr[1])
    length = min(len(arr_1), len(arr_2))
    if length == len(arr_1):
        first_half = arr_2[:length]
        second_half = arr_2[length:]
    else:
        first_half = arr_1[:length]
        second_half = arr_1[length:]
    sums = [arr_1[idx] + arr_2[idx] for idx, _ in enumerate(first_half)]
    sums.extend(second_half)
    return '-'.join(map(str, sums))
