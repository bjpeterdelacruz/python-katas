"""
String Reduction

:Author: BJ Peter Dela Cruz
"""
replacements = {"ab": "c", "ac": "b",
                "ba": "c", "bc": "a",
                "ca": "b", "cb": "a"}


def string_reduction(string: str) -> int:
    """
    This function replaces two different adjacent letters in :attr:`string` with a third, and it is
    assumed that :attr:`string` will only contain the letters a, b, and c. For example, "ab" will be
    replaced with "c", but "aa" will not be replaced with anything. When :attr:`string` cannot be
    further reduced, the length of the resulting string is returned.

    :param string: The input string
    :return: The length of the string after it is reduced
    """
    result = string
    while len(set(result)) > 1:
        for key, value in replacements.items():
            result = result.replace(key, value, 1)
    return len(result)
