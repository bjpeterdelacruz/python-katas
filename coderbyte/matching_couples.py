"""
Matching Couples

:Author: BJ Peter Dela Cruz
"""
from math import factorial


def matching_couples(int_arr: list[int]) -> int:
    """
    This function returns the number of ways boys and girls can be matched given B (the number of
    boys), G (the number of girls), and N (the number of people to pair up) in :attr:`int_arr`.

    :param int_arr: The input list of three integers
    :return: The number of ways boys and girls can be matched
    """
    boys, girls, num_people = int_arr
    if num_people % 2 != 0:
        raise ValueError("N must be an even number")
    if num_people > max(boys, girls):
        raise ValueError("N must not be greater than the maximum of B and G")
    if boys == 0 or girls == 0:
        raise ValueError("B and G must be greater than 0")
    comb_boys = combination(boys, num_people // 2)
    comb_girls = combination(girls, num_people // 2)
    return comb_boys * comb_girls * factorial(num_people // 2)


def combination(num_people: int, sample: int) -> int:
    """
    Generates a combination given the number of people and sample size.

    :param num_people: The number of people
    :param sample: The sample size
    :return: The number of combinations
    """
    return factorial(num_people) // (factorial(sample) * factorial(num_people - sample))
