"""
Parallel Sums

:Author: BJ Peter Dela Cruz
"""
from itertools import permutations


def parallel_sums(int_arr: list[int]) -> str:
    """
    This function divides :attr:`int_arr` in half and returns a permutation of each half such that
    both halves sum to the same number. Each half is sorted in ascending order, and the half that
    begins with the smallest number is displayed first followed by the other half.

    :param int_arr: The input list of integers
    :return: Both halves of the list, each half sorted in ascending order with the half that begins
        with the smallest number displayed first
    """
    partitions = set()
    for arr in permutations(int_arr, len(int_arr) // 2):
        partitions.add(tuple(sorted(arr)))
    if len(partitions) == 1:
        return ','.join(list(map(str, int_arr)))
    sums = {}
    for partition in partitions:
        result = sum(partition)
        if result not in sums:
            sums[result] = [partition]
        else:
            sums[result].append(partition)
    for value in sums.values():
        if len(value) == 2:
            arr = list(value[0])
            arr.extend(list(value[1]))
            if sorted(arr) == sorted(int_arr):
                element_1 = value[0][0]
                element_2 = value[1][0]
                list_1 = ','.join(map(str, value[0]))
                list_2 = ','.join(map(str, value[1]))
                return f"{list_1},{list_2}" if element_1 < element_2 else f"{list_2},{list_1}"
    return "-1"
