"""
Wildcard Characters

:Author: BJ Peter Dela Cruz
"""
import re


def wildcard_characters(strings: str) -> str:
    """
    This function returns whether the second string in :attr:`strings` exactly matches the pattern
    described by the first string. The plus (+) character represents a single alphabetic character,
    and the asterisk (*) character represents a sequence of the same character of length 3 unless
    it is followed by {N}, which represents the number of characters that should appear in the
    sequence, and N will be at least 1.

    :param strings: The input list of strings
    :return: "true" if the second string exactly matches the pattern, "false" otherwise
    """
    first_str, second_str = strings.split()
    first_str = re.split(r"(\*{[0-9]+})", first_str)
    positions = []
    for elem in first_str:
        if '{' in elem:
            positions.append(int(re.split(r"{([0-9]+)}", elem)[1]))
        else:
            positions.extend([1 if char == '+' else 3 for char in elem])
    if len(second_str) != sum(positions):
        return "false"
    return "true" if check_characters(second_str, positions) else "false"


def check_characters(string: str, positions: list[int]) -> str:
    """
    This helper function takes two parameters: a string and a list of integers representing the
    number of repeated characters in the string. The integers are used to create substrings, and
    each represents the length of a substring. If an integer N is greater than or equal to 3,
    then the substring must only contain the same character N times. If a substring contains any
    other characters, False is returned.

    :param string: The string from which to create substrings of different lengths
    :param positions: The list of integers, each of which represents the length N of a substring in
        :attr:`string`
    :return: True if each substring contains only one character repeated N times, False otherwise
    """
    first = 0
    index = 0
    last = positions[index]
    while True:
        substring = string[first:last]
        if len(substring) >= 3 and len(set(substring)) != 1:
            return False
        index += 1
        if index == len(positions):
            break
        first = last
        last = last + positions[index]
    return True
