"""
Blackjack Highest

:Author: BJ Peter Dela Cruz
"""
cards = {"two": 2, "three": 3, "four": 4, "five": 5, "six": 6, "seven": 7,
         "eight": 8, "nine": 9, "ten": 10, "jack": 10, "queen": 10, "king": 10, "ace": 1}

highest_cards = ["king", "queen", "jack", "ten", "nine", "eight", "seven", "six", "five", "four",
                 "three", "two"]


def blackjack_highest(hand: list[str]) -> str:
    """
    This function returns whether the :attr:`hand` (list of cards) is above 21, below 21,
    or equal to 21 (blackjack). The highest card in relation to the hand is also returned.

    :param hand: The input string, a list of cards
    :return: Whether the hand is above 21, below 21, or equal to 21 (blackjack), and the highest
        card in the hand
    """
    total = sum(cards[card] for card in hand)
    if total < 12 and "ace" in hand:
        total += 10
    highest_card = get_highest_card(hand, total)
    if total > 21:
        return f"above {highest_card}"
    if total < 21:
        return f"below {highest_card}"
    return f"blackjack {highest_card}"


def get_highest_card(hand: list[str], total: int) -> str:
    """
    This function returns the highest card based on :attr:`total`, the value of the hand.

    :param hand: The list of cards
    :param total: The total value of all the cards
    :return: The highest card
    """
    if total == 21 and "ace" in hand and len(hand) == 2:
        return "ace"
    return list(filter(lambda x: x in hand, highest_cards))[0]
