"""
Bracket Matcher

:Author: BJ Peter Dela Cruz
"""


def bracket_matcher(string: str) -> int:
    """
    This function returns 1 if the brackets are correctly matched in :attr:`string` or
    :attr:`string` contains no brackets, otherwise returns 0.

    :param string: The input string
    :return: 1 if all brackets are correctly matched, 0 otherwise
    """
    brackets = [char for char in string if char in ['(', ')']]
    brackets = ''.join(brackets)
    while '()' in brackets:
        brackets = brackets.replace('()', '')
    return 1 if len(brackets) == 0 else 0
