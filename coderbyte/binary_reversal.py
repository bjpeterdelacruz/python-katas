"""
Binary Reversal

:Author: BJ Peter Dela Cruz
"""


def binary_reversal(string: str) -> int:
    """
    This function converts :attr:`string` into a binary number, pads the left side of the binary
    number with zeroes if necessary, reverses the binary number, and returns the decimal
    representation.

    :param string: The input string, a binary number
    :return: The decimal representation of the binary number after it is reversed
    """
    binary_number = list(f"{int(string):b}")
    if len(binary_number) % 8 != 0:
        padding = 8 - (len(binary_number) % 8)
        for _ in range(0, padding):
            binary_number.insert(0, "0")
    binary_number.reverse()
    return int(''.join(binary_number), 2)
