"""
AB Check

:Author: BJ Peter Dela Cruz
"""


def ab_check(string: str) -> str:
    """
    This function returns "true" if there are three characters between the characters 'a' and 'b',
    or "false" otherwise.

    :param string: The input string
    :return: "true" if there are three characters between 'a' and 'b', "false" otherwise
    """
    start_index = 0
    while True:
        try:
            substring = string[start_index:]
            if abs(substring.index('b') - substring.index('a')) == 4:
                return "true"
            start_index += 1
        except ValueError:
            return "false"
