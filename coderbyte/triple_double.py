"""
Triple Double

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def triple_double(triple: int, double: int) -> int:
    """
    This function returns 1 if a number appears at least three times in a row in :attr:`triple` and
    also appears at least two times in a row in :attr:`double`. If there are no numbers that appear
    at least three times in a row in :attr:`triple` and two times in a row in :attr:`double`, the
    function returns 0.

    :param triple: The input integer
    :param double: The input integer
    :return: 1 if a number appears at least three times in a row in :attr:`triple` and at least two
        times in a row in :attr:`double`, 0 otherwise
    """
    for key1, value1 in groupby(str(triple), key=lambda char: char):
        if len(list(value1)) >= 3:
            for key2, value2 in groupby(str(double), key=lambda char: char):
                if key1 == key2 and len(list(value2)) >= 2:
                    return 1
    return 0
