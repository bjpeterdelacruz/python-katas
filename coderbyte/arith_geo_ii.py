"""
Arith Geo II

:Author: BJ Peter Dela Cruz
"""


def arith_geo_ii(int_arr: list[int]) -> str:
    """
    This function returns "Arithmetic" if the integers in :attr:`int_arr` follow an arithmetic
    pattern, "Geometric" if the integers follow a geometric pattern, or "-1" if the integers do not
    follow any pattern.

    :param int_arr: The input list of integers
    :return: "Arithmetic", "Geometric", or "-1"
    """
    difference = int_arr[1] - int_arr[0]
    is_arithmetic = True
    for idx, _ in enumerate(int_arr[:-1]):
        if int_arr[idx] + difference != int_arr[idx + 1]:
            is_arithmetic = False
            break
    if is_arithmetic:
        return "Arithmetic"
    quotient = int_arr[1] / int_arr[0]
    for idx, _ in enumerate(int_arr[:-1]):
        if int_arr[idx] * quotient != int_arr[idx + 1]:
            return "-1"
    return "Geometric"
