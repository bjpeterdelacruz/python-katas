"""
Even Pairs

:Author: BJ Peter Dela Cruz
"""
import re


def even_pairs(string: str) -> str:
    """
    This function determines whether an adjacent pair of even numbers exists in :attr:`string`. For
    example, given "7r5gg812", the pair is "812" (8 and 12).

    :param string: The input string
    :return: "true" if a pair of even numbers exists in :attr:`string`, "false" otherwise
    """
    arr = [num for num in re.split(r"([0-9]+)", string) if num.isnumeric()]
    for num in arr:
        numbers = [1 for number in num if int(number) % 2 == 0]
        if sum(numbers) >= 2 and int(num[-1]) % 2 == 0:
            return "true"
    return "false"
