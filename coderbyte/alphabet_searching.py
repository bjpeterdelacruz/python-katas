"""
Alphabet Searching

:Author: BJ Peter Dela Cruz
"""


def alphabet_searching(string: str) -> str:
    """
    This function returns "true" if every single letter of the English alphabet exists in
    :attr:`string`, otherwise returns "false".

    :param string: The input string
    :return: "true" if every letter in the English alphabet exists in the string, "false" otherwise
    """
    characters = {c for c in string if 65 <= ord(c.upper()) <= 90}
    return "true" if len(characters) == 26 else "false"
