"""
Number Search

:Author: BJ Peter Dela Cruz
"""
import re
from decimal import Decimal, ROUND_HALF_UP


def number_search(string: str) -> Decimal:
    """
    This function sums all the numbers found in :attr:`string` and then divides the sum by the
    number of letters in :attr:`string`. The final result is rounded to the nearest whole number.

    :param string: The input string
    :return: Sum of all the numbers divided by the number of letters rounded to the nearest whole
        number
    """
    string = re.sub('\\W+', '', string)
    numbers = [int(num) for num in string if num.isnumeric()]
    chars = [char for char in string if not char.isnumeric()]
    return Decimal(sum(numbers) / len(chars)).to_integral_value(rounding=ROUND_HALF_UP)
