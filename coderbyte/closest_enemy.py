"""
Closest Enemy

:Author: BJ Peter Dela Cruz
"""


def closest_enemy(int_arr: list[int]) -> int:
    """
    This function returns the index of 2 that is closest to 1 in :attr:`int_arr`. If 2 is not in the
    list, 0 is returned.

    :param int_arr: The input list of integers
    :return: The index of 2 that is closest to 1, or 0 if 2 is not in :attr:`int_arr`
    """
    if 1 not in int_arr:
        raise ValueError("1 must be in the list")
    if 2 not in int_arr:
        return 0
    positions = {1: [], 2: []}
    for idx, _ in enumerate(int_arr):
        if int_arr[idx] in [1, 2]:
            positions[int_arr[idx]].append(idx)
    distances = [abs(positions[1][0] - pos) for pos in positions[2]]
    distances.sort()
    return distances[0]
