"""
Noughts Determiner

:Author: BJ Peter Dela Cruz
"""


def noughts_determiner(spaces: list[str]) -> int:
    """
    This function outputs the space in :attr:`spaces`, which represents a Tic-Tac-Toe board, by
    which a player can win by putting down either an "X" or "O".

    :param spaces: The input list of strings, a Tic-Tac-Toe board filled with X's and O's
    :return: The space on the board by which a player can win
    """
    board = []
    for idx, _ in enumerate(spaces):
        if "<>" not in spaces[idx]:
            board.append((idx, spaces[idx]))
    # columns
    for index in range(0, 3):
        first = board[index]
        second = board[index + 3]
        third = board[index + 6]
        result = check(first[1] + second[1] + third[1], first, second, third)
        if result != -1:
            return result
    # rows
    for index in range(0, len(board), 3):
        first = board[index]
        second = board[index + 1]
        third = board[index + 2]
        result = check(first[1] + second[1] + third[1], first, second, third)
        if result != -1:
            return result
    # left-to-right diagonal
    result = check(board[0][1] + board[4][1] + board[8][1], board[0], board[4], board[8])
    if result != -1:
        return result
    # right-to-left diagonal
    return check(board[2][1] + board[4][1] + board[6][1], board[2], board[4], board[6])


def check(string: str, first: tuple[int, str], second: tuple[int, str], third: tuple[int,
                                                                                     str]) -> int:
    """
    This helper function checks whether a player can win in a row, column, or diagonal.

    :param string: A string representing a row, column, or diagonal
    :param first: The first space
    :param second: The second space
    :param third: The third space
    :return: The index of :attr:`first`, :attr:`second`, or :attr:`third`
    """
    if (string.count('-') == 1 and string.count('X') == 2) \
            or (string.count('-') == 1 and string.count('O') == 2):
        if string.index('-') == 0:
            return first[0]
        if string.index('-') == 1:
            return second[0]
        return third[0]
    return -1
