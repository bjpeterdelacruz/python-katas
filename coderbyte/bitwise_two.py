"""
Bitwise Two

:Author: BJ Peter Dela Cruz
"""


def bitwise_two(str_arr: list[str]) -> str:
    """
    This function returns the bitwise AND of the two binary numbers in :attr:`str_arr`.

    :param str_arr: The input list of binary numbers
    :return: The bitwise AND of the two binary numbers
    """
    if len(str_arr[0]) != len(str_arr[1]):
        raise ValueError("Strings must be of the same length")
    result = ["1" if str_arr[0][idx] == "1" and str_arr[1][idx] == "1" else "0"
              for idx, _ in enumerate(str_arr[0])]
    return ''.join(result)
