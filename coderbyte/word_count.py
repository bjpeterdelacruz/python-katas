"""
Word Count

:Author: BJ Peter Dela Cruz
"""


def word_count(string: str) -> int:
    """
    This function returns the number of words in :attr:`string`. Each word in the string is
    separated by a space.

    :param string: The input string
    :return: The number of words in :attr:`string`
    """
    return len(string.split())
