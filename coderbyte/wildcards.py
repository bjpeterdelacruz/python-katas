"""
Wildcards

:Author: BJ Peter Dela Cruz
"""
import re
from coderbyte.wildcard_characters import check_characters


def wildcards(strings: str) -> str:
    """
    This function returns whether the second string in :attr:`strings` exactly matches the pattern
    described by the first string. The plus (+) character represents a single alphabetic character,
    the dollar sign ($) represents a numeric character between 1-9, and the asterisk (*) character
    represents a sequence of the same character of length 3 unless it is followed by {N}, which
    represents the number of characters that should appear in the sequence, and N will be at least
    1.

    :param strings: The input list of strings
    :return: "true" if the second string exactly matches the pattern, "false" otherwise
    """
    first_str, second_str = strings.split()
    first_str = re.split(r"(\*{[0-9]+})", first_str)
    symbols = []
    positions = []
    for elem in first_str:
        if '{' in elem:
            num_asterisks = int(re.split(r"{([0-9]+)}", elem)[1])
            symbols.append("*" * num_asterisks)
            positions.append(num_asterisks)
        else:
            symbols.extend(["+" if char == "+" else "$" if char == "$" else "***" for char in elem])
            positions.extend([1 if char in ["+", '$'] else 3 for char in elem])
    symbols = ''.join(symbols)
    if len(second_str) != len(symbols) or not check_characters(second_str, positions):
        return "false"
    arr = {0 for idx, sym in enumerate(symbols) if sym == "$" and not second_str[idx].isnumeric()}
    return "true" if 0 not in arr else "false"
