"""
Third Greatest

:Author: BJ Peter Dela Cruz
"""
from collections import OrderedDict


def third_greatest(strings: list[str]) -> str:
    """
    This function returns the third longest string in :attr:`strings`.

    :param strings: The input list of strings
    :return: The third longest string in :attr:`strings`
    """
    lengths = OrderedDict()
    for string in strings:
        size = len(string)
        if size not in lengths:
            lengths[size] = [string]
        else:
            lengths[size].append(string)
    lengths = OrderedDict(reversed(sorted(lengths.items())))
    count = 1
    third_longest_word = None
    for value in lengths.values():
        for word in value:
            if count == 3:
                third_longest_word = word
                break
            count += 1
        if third_longest_word:
            break
    return third_longest_word
