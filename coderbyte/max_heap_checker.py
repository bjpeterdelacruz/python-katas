"""
Max Heap Checker

:Author: BJ Peter Dela Cruz
"""


def max_heap_checker(heap: list[int]) -> str:
    """
    This function returns "max" if :attr:`heap` represents a valid max heap. Otherwise, this
    function returns a comma-separated list of nodes whose values are greater than the value in the
    parent node.

    :param heap: The input list of integers that represents a heap
    :return: "max" if the heap is a valid max heap, otherwise a list of child nodes whose values are
        greater than the value in the parent node
    """
    nodes = []
    for idx, _ in enumerate(heap):
        left_child = 2 * idx + 1
        right_child = 2 * idx + 2
        if left_child < len(heap) and heap[left_child] > heap[idx]:
            nodes.append(heap[left_child])
        if right_child < len(heap) and heap[right_child] > heap[idx]:
            nodes.append(heap[right_child])
    return "max" if len(nodes) == 0 else ','.join(map(str, nodes))
