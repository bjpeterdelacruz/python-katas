"""
Prime Checker

:Author: BJ Peter Dela Cruz
"""
from itertools import permutations
from utils.math_utils import sieve_of_eratosthenes


def prime_checker(number: int) -> int:
    """
    This function returns 1 if any arrangement of :attr:`number` is a prime number, or 0 otherwise.

    :param number: The input integer
    :return: 1 if any arrangement of :attr:`number` is a prime number, 0 otherwise
    """
    arr = [int(''.join(chars)) for chars in list(permutations(str(number)))]
    prime_numbers = sieve_of_eratosthenes(max(arr) ** 2)
    primes = [num for num in arr if num in prime_numbers]
    return 1 if len(primes) > 0 else 0
