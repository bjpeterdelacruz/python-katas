"""
Formatted Number

:Author: BJ Peter Dela Cruz
"""
import re


def formatted_number(str_arr: list[str]) -> str:
    """
    This function takes a list containing only one string and returns "true" if the string is a
    valid formatted number. For example, given the input string "1,093,222.04", the function will
    return "true", and given the input string "1,093,22.04", the function will return "false".

    :param str_arr: The input list of strings
    :return: "true" if the string in the list is a valid formatted number, "false" otherwise
    """
    numbers_only = re.sub('[,.]', '', str_arr[0])
    if not numbers_only.isnumeric():
        return "false"
    numbers = str_arr[0].split('.')
    if len(numbers) > 2:
        return "false"
    if len(numbers) == 2:
        # a number should appear in front of the decimal point, and no commas should come after
        # the decimal point
        if numbers[0] == '' or not numbers[1].isnumeric():
            return "false"
    numbers = numbers[0].split(',')
    if len(numbers[0]) > 3:
        return "false"
    for index in range(1, len(numbers)):  # there should be three numbers between each comma
        if len(numbers[index]) != 3:
            return "false"
    return "true"
