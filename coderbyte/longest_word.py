"""
Longest Word

:Author: BJ Peter Dela Cruz
"""
import re


def longest_word(string: str) -> str:
    """
    This function returns the longest word in :attr:`string`. If there are two or more words that
    are the same length, the first word is returned. All punctuation will be ignored.

    :param string: The input string
    :return: The longest word in :attr:`string`
    """
    lengths = []
    for chars in string.split():
        word = re.sub('[^A-Za-z0-9]', '', chars)
        lengths.append((len(word), word))
    return max(lengths, key=lambda tup: tup[0])[1]
