"""
Caesar Cipher

:Author: BJ Peter Dela Cruz
"""


def caesar_cipher(string: str, num: int) -> str:
    """
    This function performs a Caesar Cipher on :attr:`string`. :attr:`num` is the number of
    characters to use in the cipher. Punctuation, spaces, and capitalization are unaffected.

    :param string: The input string
    :param num: The number of characters to shift
    :return: The result of the Caesar Cipher on :attr:`string`
    """
    if num == 0:
        return string
    new_str = ''.join(shift_character(c, num, 65) if 65 <= ord(c) <= 90 else c for c in string)
    return ''.join(shift_character(c, num, 97) if 97 <= ord(c) <= 122 else c for c in new_str)


def shift_character(char: str, num: int, offset: int) -> str:
    """
    This function shifts the character :attr:`char` to the right :attr:`num` times after first
    subtracting :attr:`offset`. Because the ASCII code for lowercase a and uppercase A do not
    start at 0, :attr:`offset` has to be subtracted from the ASCII value of :attr:`char`. After the
    shift is applied, :attr:`offset` is added back.

    :param char: The character to shift
    :param num: The number of times to shift the character to the right
    :param offset: The offset
    :return: A character
    """
    return chr((ord(char) - offset + num) % 26 + offset)
