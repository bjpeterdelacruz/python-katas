"""
Counting Anagrams

:Author: BJ Peter Dela Cruz
"""
from itertools import permutations


def counting_anagrams(string: str) -> int:
    """
    This function counts the number of anagrams in :attr:`string`.

    :param string: The input string containing anagrams
    :return: The number of anagrams in :attr:`string`
    """
    count = 0
    seen = []
    words = list(set(string.split()))
    while len(words) > 0:
        for permutation in permutations(words.pop()):
            word = ''.join(permutation)
            if word in words and word not in seen:
                seen.append(word)
                count += 1
    return count
