"""
Dash Insert

:Author: BJ Peter Dela Cruz
"""


def dash_insert(string: str) -> str:
    """
    This function returns a string that has a dash inserted between all odd numbers.

    :param string: The input string
    :return: :attr:`string` but with dashes inserted between all odd numbers
    """
    arr = []
    for idx in range(1, len(string)):
        arr.append(string[idx - 1])
        if int(string[idx - 1]) % 2 != 0 and int(string[idx]) % 2 != 0:
            arr.append("-")
    arr.append(string[-1])
    return ''.join(arr)
