"""
Vowel Count

:Author: BJ Peter Dela Cruz
"""


def vowel_count(string: str) -> int:
    """
    This function returns the number of vowels in :attr:`string`. The letter Y is not counted as a
    vowel.

    :param string: The input string
    :return: The number of vowels in :attr:`string`
    """
    counts = {vowel: string.count(vowel) for vowel in string if vowel in 'aeiou'}
    return sum(counts.values())
