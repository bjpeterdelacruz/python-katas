"""
Superincreasing

:Author: BJ Peter Dela Cruz
"""


def superincreasing(int_arr: list[int]) -> str:
    """
    This function returns "true" if :attr:`int_arr` forms a superincreasing sequence where each
    element in the array is greater than the sum of all previous elements.

    :param int_arr: The input list of integers
    :return: "true" if the list forms a superincreasing sequence, "false" if an element is less than
        or equal to the sum of all previous elements
    """
    for idx in range(1, len(int_arr)):
        if int_arr[idx] <= sum(int_arr[:idx]):
            return "false"
    return "true"
