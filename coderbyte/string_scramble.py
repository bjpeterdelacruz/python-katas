"""
String Scramble

:Author: BJ Peter Dela Cruz
"""


def string_scramble(string1: str, string2: str) -> str:
    """
    This function returns "true" if some of the characters in :attr:`string1` can be rearranged to
    match :attr:`string2`.

    :param string1: The string from which to rearrange characters
    :param string2: The string to match
    :return: "true" if the characters in :attr:`string1` can be rearranged to match :attr:`string2`
        or "false" otherwise
    """
    dict1 = {char: string1.count(char) for char in string1}
    dict2 = {char: string2.count(char) for char in string2}
    for key in dict2.keys():
        if dict1.get(key) is None or dict2[key] > dict1.get(key):
            return "false"
    return "true"
