"""
Overlapping Ranges

:Author: BJ Peter Dela Cruz
"""


def overlapping_ranges(int_arr: list[int]) -> str:
    """
    This function returns "true" if the two ranges specified in :attr:`int_arr` overlap by at least
    N elements, where N is the last number in :attr:`int_arr`.

    :param int_arr: The input list of integers, specifies the two ranges and the number N of
        overlapping elements
    :return: "true" if the two ranges overlap by at least N elements, "false" otherwise
    """
    first_range = set(range(int_arr[0], int_arr[1] + 1))
    second_range = set(range(int_arr[2], int_arr[3] + 1))
    return "true" if len(first_range.intersection(second_range)) >= int_arr[4] else "false"
