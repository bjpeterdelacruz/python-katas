"""
Three Sum

:Author: BJ Peter Dela Cruz
"""
from itertools import permutations


def three_sum(int_arr: list[int]) -> str:
    """
    This function returns "true" if any three numbers in :attr:`int_arr` (excluding the first
    number) sums up to the first number in :attr:`int_arr`.

    :param int_arr: The input list of integers
    :return: "true" if a set of three numbers that sum up to the first number in :attr:`int_arr` is
        found, "false" otherwise
    """
    number = int_arr.pop(0)
    for permutation in permutations(int_arr, 3):
        if sum(permutation) == number:
            return "true"
    return "false"
