"""
Product Digits

:Author: BJ Peter Dela Cruz
"""


def product_digits(number: int) -> int:
    """
    This function returns the minimum number of digits needed to multiply to produce :attr:`number`.
    For example, given 90, this function will return 3 because 10 * 9 = 90, and there are two digits
    in 10 and only one digit in 9.

    :param number: The input integer
    :return: The minimum number of digits needed to multiply to produce :attr:`number`
    """
    arr = []
    for outer in range(1, number + 1):
        for inner in range(1, number + 1):
            if outer * inner == number:
                arr.append(len(str(outer)) + len(str(inner)))
    return sorted(arr)[0]
