"""
Mean Mode

:Author: BJ Peter Dela Cruz
"""


def mean_mode(int_arr: list[int]) -> int:
    """
    This function returns 1 if the mode of the list of integers is equal to the mean; otherwise, 0
    is returned. It is assumed that the list will only contain positive integers and not contain
    more than one mode.

    :param int_arr: The input list of integers
    :return: 1 if the mode is equal to the mean, or 0 otherwise
    """
    numbers = {number: int_arr.count(number) for number in int_arr}
    return 1 if max(numbers, key=numbers.get) == sum(int_arr) / len(int_arr) else 0
