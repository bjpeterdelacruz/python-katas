"""
HTML Elements

:Author: BJ Peter Dela Cruz
"""
from html.parser import HTMLParser


class HTMLElementsParser(HTMLParser):
    """
    This class uses a list to keep track of DOM elements so that :meth:`~validate_html` can
    determine whether they are nested correctly.
    """

    def __init__(self):
        """
        Initializes an empty list that will contain DOM elements.
        """
        super().__init__()
        self.tags = []

    def error(self, message) -> None:
        """
        This method does nothing.

        :param message: A message
        :return: None
        """

    def handle_starttag(self, tag: str, attrs) -> None:
        """
        Adds the given opening DOM element to the list.

        :param tag: The opening DOM element to add to the list
        :param attrs: (not used)
        :return: None
        """
        self.tags.append(tag)

    def handle_endtag(self, tag: str) -> None:
        """
        When a closing DOM element is encountered, if its corresponding opening element was
        previously encountered, the DOM element will be removed from the list.

        If the corresponding opening element was not previously encountered, the closing DOM
        element will be added to the list.

        :param tag: The closing DOM element
        :return: None
        """
        if tag in self.tags:
            self.tags.remove(tag)
        else:
            self.tags.append(f"</{tag}>")

    def validate_html(self) -> str:
        """
        If the list contains at least one tag, then the string of DOM elements is not valid.
        Otherwise, "true" is returned.

        :return: "true" if the length of the list is 0; otherwise, the DOM element that needs to
            be replaced in order for the string to be valid
        """
        if len(self.tags) >= 2:
            return self.tags[0] if "</" in self.tags[1] else self.tags[1]
        return "true" if len(self.tags) == 0 else self.tags[0]


def html_elements(string: str) -> str:
    """
    This function passes :attr:`string` into :class:`HTMLElementsParser`'s :meth:`feed` method
    and then calls :meth:`validate_html` to determine whether the string contains valid nested DOM
    elements.

    :param string: The input string
    :return: "true" if :attr:`string` contains valid nested DOM elements, or the DOM element that
        needs to be replaced in order for :attr:`string` to be valid
    """
    parser = HTMLElementsParser()
    parser.feed(string)
    return parser.validate_html()
