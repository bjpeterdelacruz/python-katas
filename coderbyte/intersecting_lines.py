"""
Intersecting Lines

:Author: BJ Peter Dela Cruz
"""
from ast import literal_eval
from fractions import Fraction


def calculate_slope(line: list[tuple[int, int]]) -> float:
    """
    This helper function calculates the slope of a line given two points in :attr:`line`.

    :param line: The line
    :return: None if the line has no slope (vertical), or a float representing the slope of
        :attr:`line`
    """
    delta_y = line[0][1] - line[1][1]
    delta_x = line[0][0] - line[1][0]
    return None if delta_x == 0 else delta_y / delta_x


def calculate_c_value(point: tuple[int, int], slope: float) -> float:
    """
    Given a point and the slope of a line, this helper function calculates the value c in the
    equation: y = ax + c where a is the slope of the line, and x and y are the coordinates of the
    point on the line.

    :param point: The point with x and y coordinates
    :param slope: The slope a
    :return: The value c
    """
    x_value = point[0]
    y_value = point[1]
    return y_value - (slope * x_value)


def intersecting_lines(strings: list[str]) -> str:
    """
    This function returns the point at which the two lines represented by the points in
    :attr:`string` intersect.

    :param strings: The input list of tuples, each of which represents a point
    :return: The point at which both lines intersect, or "no intersection" if both lines are
        parallel to each other
    """
    line1 = [literal_eval(strings[0]), literal_eval(strings[1])]
    line2 = [literal_eval(strings[2]), literal_eval(strings[3])]
    slope1 = calculate_slope(line1)
    slope2 = calculate_slope(line2)
    if slope1 is None and slope2 is not None:
        y_result = int(slope2 * line1[0][0] + calculate_c_value(line2[0], slope2))
        return f"({line1[0][0]},{y_result})"
    if slope2 is None and slope1 is not None:
        y_result = int(slope1 * line2[0][0] + calculate_c_value(line1[0], slope1))
        return f"({line2[0][0]},{y_result})"
    if slope1 == slope2:
        return "no intersection"
    c_1 = calculate_c_value(line1[0], slope1)
    c_2 = calculate_c_value(line2[0], slope2)
    x_result = (c_1 - c_2) / (slope2 - slope1)
    y_result = ((c_1 * slope2) - (c_2 * slope1)) / (slope2 - slope1)
    return f"({str(Fraction(x_result).limit_denominator())}," \
           f"{str(Fraction(y_result).limit_denominator())})"
