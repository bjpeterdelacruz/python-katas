"""
Alphabet Soup

:Author: BJ Peter Dela Cruz
"""


def alphabet_soup(string: str) -> str:
    """
    This function returns the characters in the word :attr:`string` in sorted order.

    :param string: The input string
    :return: The characters in :attr:`string` in sorted order
    """
    return ''.join(sorted(list(string)))
