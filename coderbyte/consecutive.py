"""
Consecutive

:Author: BJ Peter Dela Cruz
"""


def consecutive(int_arr: list[int]) -> int:
    """
    This function returns the minimum number of integers needed to make the contents of
    :attr:`int_arr` consecutive from the lowest number to the highest number.

    :param int_arr: The input list of integers
    :return: The minimum number of integers needed to make the list consecutive from lowest number
        to highest number
    """
    if len(int_arr) < 2:
        raise ValueError("list must have at least two elements")
    int_arr.sort()
    return sum(abs(int_arr[idx - 1] - int_arr[idx]) - 1 for idx in range(1, len(int_arr)))
