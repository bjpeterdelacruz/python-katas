"""
Binary Converter

:Author: BJ Peter Dela Cruz
"""


def binary_converter(string: str) -> int:
    """
    This function returns the decimal representation of :attr:`string`, a binary number.

    :param string: The input string representing a binary number
    :return: The decimal representation of the binary number
    """
    values = [2 ** idx for idx, char in enumerate(string[::-1]) if char == '1']
    return sum(values)
