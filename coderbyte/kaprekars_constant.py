"""
Kaprekar's Constant

:Author: BJ Peter Dela Cruz
"""


def kaprekars_constant(num: int) -> int:
    """
    This function returns the number of iterations it took to find Kaprekar's Constant, which is
    6174.

    :param num: The input integer
    :return: The number of times it took to find Kaprekar's Constant given :attr:`num`
    """
    count = 1
    difference = num
    while True:
        difference = ''.join(sorted(str(difference)))
        string_1 = difference
        while len(string_1) < 4:
            string_1 = f"0{string_1}"
        string_2 = difference[::-1]
        while len(string_2) < 4:
            string_2 = f"{string_2}0"
        difference = int(string_2) - int(string_1)
        if difference == 6174:
            break
        count += 1
    return count
