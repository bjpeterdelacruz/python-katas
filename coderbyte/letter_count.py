"""
Letter Count

:Author: BJ Peter Dela Cruz
"""


def letter_count(string: str) -> str:
    """
    This function returns the first word in :attr:`string` with the greatest number of repeated
    letters.

    :param string: The input string
    :return: The word with the greatest number of repeated letters, or "-1" if there are no
        repeated letters
    """
    word_with_highest_count = None
    highest_count = -1
    for word in string.split():
        counts = {word.count(character) for character in word}
        for count in counts:
            if count > 1 and count > highest_count:
                word_with_highest_count = word
                highest_count = count
    return word_with_highest_count if word_with_highest_count is not None else "-1"
