"""
Two Sum

:Author: BJ Peter Dela Cruz
"""
from itertools import permutations


def two_sum(integers: list[int]) -> str:
    """
    This function returns pairs of integers that sum to the first element in :attr:`integers`. If no
    two numbers sum to the first element in the list, "-1" is returned.

    :param integers: The input list of integers
    :return: A string that contains pairs of integers that sum to the first element in
        :attr:`integers`, or "-1" if there are none
    """
    values = []
    results = []
    for tup in list(permutations(integers[1:], 2)):
        if tup[0] + tup[1] == integers[0] and tup[0] not in values and tup[1] not in values:
            results.append(f"{tup[0]},{tup[1]}")
            values.append(tup[0])
            values.append(tup[1])
    return ' '.join(results) if len(results) > 0 else "-1"
