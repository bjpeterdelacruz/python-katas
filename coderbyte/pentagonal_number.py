"""
Pentagonal Number

:Author: BJ Peter Dela Cruz
"""


def pentagonal_number(integer: int) -> int:
    """
    This function returns the number of dots that exist in a pentagonal shape with a center dot on
    the :attr:`integer`-th iteration.

    :param integer: The input integer that represents the N-th iteration
    :return: The number of dots
    """
    counter = integer
    total = 1
    while counter > 1:
        total += counter * 5 - 5
        counter -= 1
    return total
