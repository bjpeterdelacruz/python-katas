"""
Dash Insert II

:Author: BJ Peter Dela Cruz
"""


def dash_insert_ii(num: int) -> str:
    """
    This function inserts a dash between two odd numbers in :attr:`num` and an asterisk between
    two even numbers. Zeroes are not counted as odd or even numbers.

    :param num: The input integer
    :return: A string with dashes inserted between odd numbers and asterisks between even numbers
    """
    string = str(num)
    results = [string[0]]
    idx = 1
    while idx < len(string):
        first_number = int(string[idx - 1])
        second_number = int(string[idx])
        if first_number == 0 or second_number == 0:
            results.append(string[idx])
        elif first_number % 2 == 0 and second_number % 2 == 0:
            results.append("*")
            results.append(string[idx])
        elif first_number % 2 != 0 and second_number % 2 != 0:
            results.append("-")
            results.append(string[idx])
        else:
            results.append(string[idx])
        idx += 1
    return ''.join(results)
