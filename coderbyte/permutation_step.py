"""
Permutation Step

:Author: BJ Peter Dela Cruz
"""
from itertools import permutations


def permutation_step(integer: int) -> int:
    """
    This function returns the next number greater than :attr:`integer` using all the digits in
    :attr:`integer`. For example, given 123, the next number greater than it is 132.

    :param integer: The input integer
    :return: The next number greater than :attr:`integer` using all the digits in :attr:`integer`
    """
    arr = sorted(list(set(map(int, [''.join(permutation)
                                    for permutation in permutations(str(integer))]))))
    return arr[arr.index(integer) + 1] if integer != arr[-1] else -1
