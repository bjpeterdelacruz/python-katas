"""
Missing Digit II

:Author: BJ Peter Dela Cruz
"""
operations = {"*": "//", "/": "*", "+": "-", "-": "+"}

# pylint: disable=eval-used


def missing_digit_ii(equation: str) -> str:
    """
    This function finds the missing digit in the first and third numbers in :attr:`equation`. For
    example, given "56? * 106 = 5?678", the function will return "3 9", which corresponds to the
    first question mark and the second question mark, respectively.

    :param equation: The input string, a mathematical equation
    :return: The missing digits
    """
    arr = equation.split()
    possible_first_numbers = {num: int(arr[0].replace("?", str(num))) for num in range(0, 10)}
    symbol = arr[1]
    second_number = int(arr[2])
    possible_third_numbers = {num: int(arr[4].replace("?", str(num))) for num in range(0, 10)}
    first_digit = None
    third_digit = None
    for key1, value1 in possible_first_numbers.items():
        if first_digit and third_digit:
            break
        for key3, value3 in possible_third_numbers.items():
            if eval(f"{value3} {operations[symbol]} {second_number}") == value1:
                first_digit = key1
                third_digit = key3
                break
    return f"{first_digit} {third_digit}"
