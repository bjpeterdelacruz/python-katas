"""
359. Logger Rate Limit

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class Logger:
    """
    This class represents a logger that will log messages.
    """
    def __init__(self):
        """
        Creates a dictionary that will contain log messages and timestamps at which they appeared.
        """
        self.logs = {}

    def should_print_message(self, timestamp: int, message: str) -> bool:
        """
        This method returns True if the given log message will be printed, False otherwise. A log
        message will be printed if the same log message appeared eleven or more seconds ago.

        :param timestamp: The timestamp of the log message
        :param message: The log message that should be printed
        :return: True if the log message should be printed, False otherwise
        """
        if message not in self.logs:
            self.logs[message] = timestamp
        elif self.logs[message] + 10 > timestamp:
            return False
        else:
            self.logs[message] = timestamp
        return True
