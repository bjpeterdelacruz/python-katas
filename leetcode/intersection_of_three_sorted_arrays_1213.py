"""
1213. Intersection of Three Sorted Arrays

:Author: BJ Peter Dela Cruz
"""


def arrays_intersection(arr1: list[int], arr2: list[int], arr3: list[int]) -> list[int]:
    """
    This function returns a sorted list that contains integers that are present in all three lists.

    :param arr1: The first input list of integers
    :param arr2: The second input list of integers
    :param arr3: The third input list of integers
    :return: A sorted list that contains integers that are present in all three lists
    """
    return sorted(set(arr1).intersection(set(arr2), set(arr3)))
