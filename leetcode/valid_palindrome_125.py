"""
125. Valid Palindrome

:Author: BJ Peter Dela Cruz
"""
from string import ascii_letters


def is_palindrome(string: str) -> bool:
    """
    This function returns True if the given string is a palindrome after first removing all non-
    alphanumeric characters and then converting all ASCII characters to lowercase.

    :param string: The input string, may contain non-alphanumeric characters and uppercase ASCII
        characters
    :return: True if the string is a palindrome, False otherwise
    """
    new_string = ''.join(char for char in string if char in f'{ascii_letters}0123456789').lower()
    return new_string == ''.join(reversed(new_string))
