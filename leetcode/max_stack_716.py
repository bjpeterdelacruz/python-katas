"""
716. Max Stack

:Author: BJ Peter Dela Cruz
"""


class MaxStack:
    """
    This class represents a stack data structure that allows users to push integers onto the stack,
    get or pop the integer that is at the top of the stack, and get or pop the largest integer that
    is in the stack.
    """

    def __init__(self):
        """
        Creates an empty stack.
        """
        self.stack = []

    def push(self, number: int) -> None:
        """
        This method pushes the given integer onto the top of the stack.

        :param number: The integer to push onto the top of the stack
        :return: None
        """
        self.stack.append(number)

    def pop(self) -> int:
        """
        This method removes the integer that is at the top of the stack and returns it.

        :return: The integer that was on top of the stack
        """
        return self.stack.pop()

    def top(self) -> int:
        """
        This method returns the integer that is at the top of the stack without removing it from the
        stack.

        :return: The integer that is on top of the stack
        """
        return self.stack[-1]

    def peek_max(self) -> int:
        """
        This method returns the largest integer that is in the stack. The largest integer is not
        removed from the stack.

        :return: The largest integer in the stack
        """
        return max(self.stack)

    def pop_max(self) -> int:
        """
        This method removes the largest integer from the stack and returns it. If there are two or
        more integers that are the largest in the stack, the rightmost integer is removed and
        returned, i.e. the most recent integer that was pushed onto the stack.

        :return: The largest integer in the stack
        """
        maximum = max(self.stack)
        for idx in range(len(self.stack) - 1, -1, -1):
            if self.stack[idx] == maximum:
                self.stack.pop(idx)
                break
        return maximum
