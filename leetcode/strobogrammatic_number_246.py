"""
246. Strobogrammatic Number

:Author: BJ Peter Dela Cruz
"""


def is_strobogrammatic(num: str) -> bool:
    """
    This function tests whether the given number is strobogrammatic. A strobogrammatic number is a
    number whose numeral is rotationally symmetric so that it appears the same when it is rotated
    180 degrees. For example, 689 is a strobogrammatic number whereas 6, 120, and 1003 are not. If
    the number contains 2, 3, 4, 5, or 7, it is not strobogrammatic. If a number contains one or
    more 6's, the number must contain the same number of 9's. Also, 6 or 9 cannot be in the exact
    middle of the number. For example, 169 is not a strobogrammatic number.

    :param num: The input integer as a string
    :return: True if the number is strobogrammatic, False otherwise
    """
    nums = set(num)
    for number in "23457":
        if number in nums:
            return False
    if len(num) == 1 and num in "69":
        return False
    middle = len(num) // 2
    left = num[:middle]
    right = num[middle:] if len(num) % 2 == 0 else num[middle + 1:]
    if len(num) % 2 == 1 and num[middle] in "69":
        return False
    right = list(reversed(right))
    for idx, number in enumerate(right):
        if number == "6":
            right[idx] = "9"
        elif number == "9":
            right[idx] = "6"
    return left == ''.join(right)
