"""
448. Find All Numbers Disappeared in an Array

:Author: BJ Peter Dela Cruz
"""


def find_disappeared_numbers(nums: list[int]) -> list[int]:
    """
    This function finds all the integers between 1 and N (both inclusive, where N is the number of
    integers in :attr:`nums`) that do not appear in :attr:`nums`.

    :param nums: The input list of integers
    :return: A list of integers between 1 and N that do not appear in :attr:`nums`
    """
    counts = set(nums)
    return [num for num in range(1, len(nums) + 1) if num not in counts]
