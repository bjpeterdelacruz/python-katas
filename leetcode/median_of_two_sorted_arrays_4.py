"""
4. Median of Two Sorted Arrays

:Author: BJ Peter Dela Cruz
"""


def find_middle(nums: list[int]) -> float:
    """
    This helper function finds the median of the given list of integers. If the list contains an
    even number of integers, the middle two elements are summed together and then the sum is divided
    by two.

    :param nums: The list of integers
    :return: The median of the list
    """
    middle = len(nums) // 2
    if len(nums) % 2 == 0:
        return (nums[middle - 1] + nums[middle]) / 2
    return nums[middle]


def find_median_sorted_arrays(nums1: list[int], nums2: list[int]) -> float:
    """
    This function combines two sorted lists of integers together into one sorted list and then finds
    the median of the combined list.

    :param nums1: The first sorted list of integers
    :param nums2: The second sorted list of integers
    :return: The median of the combined list of integers
    """
    if nums1 and not nums2:
        return find_middle(nums1)
    if not nums1 and nums2:
        return find_middle(nums2)
    arr = nums1.copy()
    arr_idx = 0
    for nums2_idx, num in enumerate(nums2):
        if arr_idx < len(arr):
            while arr_idx < len(arr) and num > arr[arr_idx]:
                arr_idx += 1
            arr.insert(arr_idx, num)
            arr_idx += 1
        else:
            arr.extend(nums2[nums2_idx:])
            break
    return find_middle(arr)
