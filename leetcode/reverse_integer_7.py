"""
7. Reverse Integer

:Author: BJ Peter Dela Cruz
"""


def reverse(num: int) -> int:
    """
    This function reverses all the digits in the given positive or negative integer. If the reversed
    integer cannot fit into 32 bits, then zero is returned.

    :param num: The input integer
    :return: The integer after all of its digits have been reversed, or zero if it cannot fit into
        32 bits
    """
    x_str = ''.join(reversed(str(num)[1:])) if num < 0 else ''.join(reversed(str(num)))
    reversed_x = int(f'-{x_str}') if num < 0 else int(f'{x_str}')
    return reversed_x if -2 ** 31 <= reversed_x <= (2 ** 31) - 1 else 0
