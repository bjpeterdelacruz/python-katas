echo "Pylinting coderbyte package..."
pylint coderbyte

echo "Pylinting codesignal package..."
pylint codesignal

echo "Pylinting codewars package..."
pylint codewars

echo "Pylinting leetcode package..."
pylint leetcode

echo "Pylinting tests package..."
pylint tests

echo "Pylinting utils package..."
pylint utils

echo "Done!"