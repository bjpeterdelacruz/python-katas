"""
Unit tests for All Star Code Challenge #16 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.all_star_code_challenge_number_16 import no_repeat


@pytest.mark.parametrize("test_input, expected",
                         [("aabbccdde", "e"),
                          ("wxyz", "w"),
                          ("testing", "e"),
                          ("codewars", "c"),
                          ("Testing", "T")])
def test_no_repeat(test_input, expected):
    """
    Tests the All Star Code Challenge #16 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert no_repeat(test_input) == expected
