"""
Unit tests for Count Adjacent Pairs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.count_adjacent_pairs import count_adjacent_pairs


@pytest.mark.parametrize("test_input, expected",
                         [('', 0), ('orange Orange kiwi pineapple apple', 1),
                          ('banana banana banana', 1), ('cat cat dog dog cat cat', 3),
                          ('banana banana banana terracotta banana terracotta terracotta pie!', 2),
                          ('pineapple apple', 0), ('dog dog dog dog cat cat', 2),
                          ('dog DOG cat', 1)])
def test_count_adjacent_pairs(test_input, expected):
    """
    Tests the Count Adjacent Pairs solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert count_adjacent_pairs(test_input) == expected
