"""
Unit tests for Sort the Odd challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sort_the_odd import sort_the_odd


@pytest.mark.parametrize("test_input, expected",
                         [([9, 8, 7, 6, 5, 4, 3, 2, 1, 0], [1, 8, 3, 6, 5, 4, 7, 2, 9, 0]),
                          ([], [])])
def test_sort_the_odd(test_input, expected):
    """
    Tests the Sort the Odd solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sort_the_odd(test_input) == expected
