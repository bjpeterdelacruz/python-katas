"""
Unit tests for Filtering Even Numbers (Bug Fixes) challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.filtering_even_numbers_bug_fixes import kata_13_december


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 3, 4, 5, 6], [1, 3, 5]), ([11, 12, 13, 14, 15], [11, 13, 15])])
def test_kata_13_december(test_input, expected):
    """
    Tests the Filtering Even Numbers (Bug Fixes) solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert kata_13_december(test_input) == expected
