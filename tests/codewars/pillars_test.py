"""
Unit tests for Pillars challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.pillars import pillars


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(1, 10, 10, 0), (2, 20, 25, 2000), (11, 15, 30, 15270)])
def test_pillars(test_input1, test_input2, test_input3, expected):
    """
    Tests the Pillars solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert pillars(test_input1, test_input2, test_input3) == expected
