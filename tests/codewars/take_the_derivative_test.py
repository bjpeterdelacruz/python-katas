"""
Unit tests for Take the Derivative challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.take_the_derivative import derive


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(5, 4, "20x^3"), (1, 2, "2x^1")])
def test_derive(test_input1, test_input2, expected):
    """
    Tests the Take the Derivative solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert derive(test_input1, test_input2) == expected
