"""
Unit tests for Thinkful: Quarks challenge

:Author: BJ Peter Dela Cruz
"""
import pytest

from codewars.thinkful_object_drills_quarks import Quark


def test_quark():
    """
    Tests the Thinkful: Quarks solution.

    :return: None
    """
    quark1 = Quark("red", "up")
    assert quark1.color == "red"
    assert quark1.flavor == "up"

    quark2 = Quark("blue", "strange")
    assert quark2.baryon_number == 1.0 / 3
    assert quark1.baryon_number == 1.0 / 3

    quark1.interact(quark2)
    assert quark1.color == "blue"
    assert quark2.color == "red"

    with pytest.raises(ValueError):
        Quark("black", "up")
        assert False

    with pytest.raises(ValueError):
        Quark("red", "middle")
        assert False
