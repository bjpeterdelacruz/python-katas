"""
Unit tests for Populate Hash with Default Value challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.populate_hash_with_array_keys_and_default_value import populate_dict


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([], True, {}), (['a', 'b', 'c'], 2, {'a': 2, 'b': 2, 'c': 2})])
def test_populate_dict(test_input1, test_input2, expected):
    """
    Tests the Populate Hash with Default Value solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert populate_dict(test_input1, test_input2) == expected
