"""
Unit tests for Simple String Expansion challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_string_expansion import solve


@pytest.mark.parametrize("test_input, expected",
                         [('2(a10(b))', 'abbbbbbbbbbabbbbbbbbbb'),
                          ('3(b(2(c)))', 'bccbccbcc')])
def test_solve(test_input, expected):
    """
    Tests the Simple String Expansion solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert solve(test_input) == expected
