"""
Unit tests for Duplicates Everywhere challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.duplicates_duplicates_everywhere import remove_duplicate_ids


@pytest.mark.parametrize("test_input, expected",
                         [({
                               "1": ["A", "B", "C"],
                               "2": ["A", "B", "D", "A"],
                           }, {
                               "1": ["C"],
                               "2": ["A", "B", "D"]
                           }),
                             ({
                                  "1": ["C", "F", "G"],
                                  "2": ["A", "B", "C"],
                                  "3": ["A", "B", "D"],
                              }, {
                                  "1": ["F", "G"],
                                  "2": ["C"],
                                  "3": ["A", "B", "D"]
                              }), ({
                                       "1": ["A"],
                                       "2": ["A"],
                                       "3": ["A"],
                                   }, {
                                       "1": [],
                                       "2": [],
                                       "3": ["A"],
                                   }), ({
                                            "432": ["A", "A", "B", "D"],
                                            "53": ["L", "G", "B", "C"],
                                            "236": ["L", "A", "X", "G", "H", "X"],
                                            "11": ["P", "R", "S", "D"],
                                        }, {
                                            "432": ["A", "B", "D"],
                                            "53": ["C"],
                                            "236": ["L", "X", "G", "H"],
                                            "11": ["P", "R", "S"],
                                        })])
def test_remove_duplicate_ids(test_input, expected):
    """
    Tests the Duplicates Everywhere solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove_duplicate_ids(test_input) == expected
