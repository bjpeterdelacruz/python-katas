"""
Unit tests for Volume of a Cuboid challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.volume_of_a_cuboid import get_volume_of_cuboid


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(2, 5, 6, 60), (6.3, 3, 5, 94.5)])
def test_get_volume_of_cuboid(test_input1, test_input2, test_input3, expected):
    """
    Tests the Volume of a Cuboid solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_volume_of_cuboid(test_input1, test_input2, test_input3) == expected
