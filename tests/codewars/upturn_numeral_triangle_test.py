"""
Unit tests for Upturn Numerical Triangle challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.upturn_numeral_triangle import pattern


@pytest.mark.parametrize("test_input, expected",
                         [(7,
                           " 1 1 1 1 1 1 1\n  2 2 2 2 2 2\n   3 3 3 3 3\n    4 4 4 4\n     5 5 5"
                           "\n      6 6\n       7"),
                          (12,
                           " 1 1 1 1 1 1 1 1 1 1 1 1\n  2 2 2 2 2 2 2 2 2 2 2"
                           "\n   3 3 3 3 3 3 3 3 3 3\n    4 4 4 4 4 4 4 4 4\n     5 5 5 5 5 5 5 5"
                           "\n      6 6 6 6 6 6 6\n       7 7 7 7 7 7\n        8 8 8 8 8"
                           "\n         9 9 9 9\n          0 0 0\n           1 1\n            2"),
                          (21,
                           " 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1"
                           "\n  2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2"
                           "\n   3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3"
                           "\n    4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4"
                           "\n     5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5"
                           "\n      6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6"
                           "\n       7 7 7 7 7 7 7 7 7 7 7 7 7 7 7"
                           "\n        8 8 8 8 8 8 8 8 8 8 8 8 8 8"
                           "\n         9 9 9 9 9 9 9 9 9 9 9 9 9"
                           "\n          0 0 0 0 0 0 0 0 0 0 0 0"
                           "\n           1 1 1 1 1 1 1 1 1 1 1"
                           "\n            2 2 2 2 2 2 2 2 2 2\n             3 3 3 3 3 3 3 3 3"
                           "\n              4 4 4 4 4 4 4 4\n               5 5 5 5 5 5 5"
                           "\n                6 6 6 6 6 6\n                 7 7 7 7 7"
                           "\n                  8 8 8 8\n                   9 9 9"
                           "\n                    0 0\n                     1")
                          ])
def test_pattern(test_input, expected):
    """
    Tests the Upturn Numerical Triangle solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert pattern(test_input) == expected
