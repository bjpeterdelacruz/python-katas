"""
Unit tests for Clean up After Your Dog challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.clean_up_after_your_dog import crap


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [([['_', '_', '_', '_'], ['_', '_', '_', '@'], ['_', '_', '@', '_']], 2, 2,
                           "Clean"),
                          ([['_', '_', '_', '_'], ['_', '_', '_', '@'], ['_', '_', '@', '_']], 1, 1,
                           "Cr@p"),
                          ([['_', '_'], ['_', '@'], ['D', '_']], 2, 2, "Dog!!"),
                          ([['_', '_', '_', '_'], ['_', '_', '_', '_'], ['_', '_', '_', '_']], 2, 2,
                           "Clean"),
                          ([['@', '@'], ['@', '@'], ['@', '@']], 3, 2, "Clean")])
def test_crap(test_input1, test_input2, test_input3, expected):
    """
    Tests the Clean up After Your Dog solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert crap(test_input1, test_input2, test_input3) == expected
