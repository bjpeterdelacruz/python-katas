"""
Unit tests for Email Address Obfuscation challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.email_address_obfuscator import obfuscate


@pytest.mark.parametrize("test_input, expected",
                         [("test.1.2.3@gmail.com",
                           "test [dot] 1 [dot] 2 [dot] 3 [at] gmail [dot] com"),
                          ("test123@gmail.com", "test123 [at] gmail [dot] com")])
def test_obfuscate(test_input, expected):
    """
    Tests the Email Address Obfuscation solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert obfuscate(test_input) == expected
