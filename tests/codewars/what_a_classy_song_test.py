"""
Unit tests for What a Classy Song challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.what_a_classy_song import Song


def test_song():
    """
    Tests the Weight for Weight solution.

    :return: None
    """
    mount_moose = Song('Mount Moose', 'The Snazzy Moose')
    assert mount_moose.title == 'Mount Moose'
    assert mount_moose.artist == 'The Snazzy Moose'

    assert mount_moose.how_many(['John', 'Fred', 'Bob', 'Carl', 'RyAn']) == 5
    assert mount_moose.how_many(['JoHn', 'Luke', 'AmAndA']) == 2
    assert mount_moose.how_many(['Amanda', 'CalEb', 'CarL', 'Ferguson']) == 2
    assert mount_moose.how_many(['JOHN', 'FRED', 'BOB', 'CARL', 'RYAN', 'KATE']) == 1

    assert len(mount_moose.listeners) == 10
