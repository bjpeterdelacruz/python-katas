"""
Unit tests for Simple Encryption #4: Qwerty challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_encryption_number_4_qwerty import encrypt, decrypt


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("A", 111, "S"),
                          ("Abc", 212, "Smb"),
                          ("Wave", 0, "Wave"),
                          ("Wave", 345, "Tg.y"),
                          ("Ball", 134, ">fdd"),
                          ("Ball", 444, ">gff"),
                          ("This is a test.", 348, "Iaqh qh g iyhi,"),
                          ("Do the kata Kobayashi Maru Test. Endless fun and excitement when "
                           "finding a solution.", 583,
                           "Sr pgi jlpl Jr,lqlage Zlow Piapc I.skiaa dw. l.s ibnepizi.p ugi. de.se."
                           "f l arkwper.c")])
def test_encrypt(test_input1, test_input2, expected):
    """
    Tests the encryption process of the Simple Encryption #4: Qwerty solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert encrypt(test_input1, test_input2) == expected


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("S", 111, "A"),
                          ("Smb", 212, "Abc"),
                          ("Wave", 0, "Wave"),
                          ("Tg.y", 345, "Wave"),
                          (">fdd", 134, "Ball"),
                          (">gff", 444, "Ball"),
                          ("Iaqh qh g iyhi,", 348, "This is a test."),
                          (
                          "Sr pgi jlpl Jr,lqlage Zlow Piapc I.skiaa dw. l.s ibnepizi.p ugi. de.se.f"
                          " l arkwper.c", 583,
                          "Do the kata Kobayashi Maru Test. Endless fun and excitement when finding"
                          " a solution.")])
def test_decrypt(test_input1, test_input2, expected):
    """
    Tests the decryption process of the Simple Encryption #4: Qwerty solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert decrypt(test_input1, test_input2) == expected
