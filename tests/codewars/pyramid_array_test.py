"""
Unit tests for Pyramid Array challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.pyramid_array import pyramid


@pytest.mark.parametrize("test_input, expected",
                         [(0, []), (1, [[1]]), (2, [[1], [1, 1]]), (3, [[1], [1, 1], [1, 1, 1]])])
def test_pyramid(test_input, expected):
    """
    Tests the Pyramid Array solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert pyramid(test_input) == expected
