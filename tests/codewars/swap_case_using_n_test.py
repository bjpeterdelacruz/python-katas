"""
Unit tests for Swap Case Using N challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.swap_case_using_n import swap


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('Hello world!', 11, 'heLLO wORLd!'),
                          ('the quick broWn fox leapt over the fence', 9,
                           'The QUicK BrowN foX LeaPT ovER thE FenCE'),
                          ('eVerybody likes ice cReam', 85, 'EVErYbODy LiKeS IcE creAM'),
                          ('gOOd MOrniNg', 7864, 'GooD MorNIng'),
                          ('how are you today?', 12345, 'HOw are yoU TOdaY?'),
                          ('the lord of the rings', 0, 'the lord of the rings'), ('', 11345, '')])
def test_swap(test_input1, test_input2, expected):
    """
    Tests the Swap Case Using N solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert swap(test_input1, test_input2) == expected
