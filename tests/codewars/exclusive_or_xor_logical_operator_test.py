"""
Unit tests for Exclusive "or" (xor) Logical Operator challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclusive_or_xor_logical_operator import xor


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(True, False, True), (False, True, True), (False, False, False),
                          (True, True, False)])
def test_xor(test_input1, test_input2, expected):
    """
    Tests the Exclusive "or" (xor) Logical Operator solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert xor(test_input1, test_input2) == expected
