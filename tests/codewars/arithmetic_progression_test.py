"""
Unit tests for Arithmetic Progression challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.arithmetic_progression import arithmetic_sequence_elements


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(0, 2, 5, '0, 2, 4, 6, 8'), (0, 5, 0, ''),
                          (-100, -5, 3, '-100, -105, -110'), (-10, 4, 5, '-10, -6, -2, 2, 6')])
def test_arithmetic_sequence_elements(test_input1, test_input2, test_input3, expected):
    """
    Tests the Arithmetic Progression solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert arithmetic_sequence_elements(test_input1, test_input2, test_input3) == expected


def test_seqlist_invalid_input():
    """
    Tests the Arithmetic Progression solution with invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        arithmetic_sequence_elements(0, 5, -1)
