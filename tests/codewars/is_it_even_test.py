"""
Unit tests for Is It Even? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.is_it_even import is_even


@pytest.mark.parametrize("test_input, expected",
                         [(-4, True),
                          (-3, False),
                          (-0.2, False),
                          (-0.1, False),
                          (0, True),
                          (0.4, False),
                          (0.5, False),
                          (1, False),
                          (2, True),
                          (2.0, False),
                          (15, False),
                          (20, True)])
def test_is_even(test_input, expected):
    """
    Tests the Is It Even? solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_even(test_input) == expected
