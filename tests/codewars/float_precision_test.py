"""
Unit tests for Float Precision challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.float_precision import solution


@pytest.mark.parametrize("test_input, expected",
                         [(12.344, 12.34), (23.456, 23.46), (21, 21)])
def test_solution(test_input, expected):
    """
    Tests the Float Precision solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert solution(test_input) == expected
