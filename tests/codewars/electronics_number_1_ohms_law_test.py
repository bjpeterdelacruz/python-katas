"""
Unit tests for Electronics #1: Ohm's Law challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.electronics_number_1_ohms_law import ohms_law


@pytest.mark.parametrize("test_input, expected",
                         [('3.3e-3A 1e3R', '3.3V'),
                          ('2200R 5V', '0.002273A'),
                          ('25V 1e-2A', '2500.0R'),
                          ('2e-3A 1e3R', '2.0V'),
                          ('5V 10e-3A', '500.0R'),
                          ('1e3R 2e-3A', '2.0V'),
                          ('0.005A 30V', '6000.0R'),
                          ('0R 0A', '0.0V'),
                          ('30V 5000R', '0.006A')])
def test_ohms_law(test_input, expected):
    """
    Tests the Electronics #1: Ohm's Law solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert ohms_law(test_input) == expected
