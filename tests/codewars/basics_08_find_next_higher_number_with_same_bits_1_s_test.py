"""
Unit tests for Find Next Highest Number with Same Bits challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.basics_08_find_next_higher_number_with_same_bits_1_s import next_higher


@pytest.mark.parametrize("test_input, expected",
                         [(128, 256), (1, 2), (1022, 1279), (127, 191), (1253343, 1253359)])
def test_next_higher(test_input, expected):
    """
    Tests the Find Next Highest Number with Same Bits solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert next_higher(test_input) == expected
