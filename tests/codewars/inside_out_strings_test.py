"""
Unit tests for Inside Out challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.inside_out_strings import inside_out


@pytest.mark.parametrize("test_input, expected",
                         [('man i need a taxi up to ubud', 'man i ende a atix up to budu'),
                          ('what time are we climbing up the volcano',
                           'hwta item are we milcgnib up the lovcona'),
                          ('take me to semynak', 'atek me to mesykan'),
                          ('massage yes massage yes massage', 'samsega yes samsega yes samsega'),
                          ('take bintang and a dance please', 'atek nibtgna and a adnec elpesa')])
def test_inside_out(test_input, expected):
    """
    Tests the Inside Out solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert inside_out(test_input) == expected
