"""
Unit tests for Urban Dictionary challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.urban_dictionary import WordDictionary


def test_search():
    """
    Tests the Urban Dictionary solution.

    :return: None
    """
    word_dict = WordDictionary()
    word_dict.add_word("a")
    word_dict.add_word("at")
    word_dict.add_word("ate")
    word_dict.add_word("ear")
    assert word_dict.search("a") is True
    assert word_dict.search("a.") is True
    assert word_dict.search("a.e") is True
    assert word_dict.search("b") is False
    assert word_dict.search("e.") is False
    assert word_dict.search("ea.") is True
    assert word_dict.search("ea..") is False
    word_dict.add_word("co")
    word_dict.add_word("cod")
    word_dict.add_word("code")
    word_dict.add_word("codewars")
    assert word_dict.search("........") is True
    assert word_dict.search("c.o") is False
    assert word_dict.search("cod.") is True
    assert word_dict.search("c.o") is False
    assert word_dict.search("co..w..s") is True
    assert word_dict.search("co..w..") is False
