"""
Unit tests for N Seconds Ago challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.n_seconds_ago import seconds_ago


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('2000-01-01 00:00:00', 1, '1999-12-31 23:59:59'),
                          ('2000-01-01 00:00:00', -1, '2000-01-01 00:00:01'),
                          ('0001-02-03 04:05:06', 7, '0001-02-03 04:04:59')])
def test_seconds_ago(test_input1, test_input2, expected):
    """
    Tests the N Seconds Ago solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert seconds_ago(test_input1, test_input2) == expected
