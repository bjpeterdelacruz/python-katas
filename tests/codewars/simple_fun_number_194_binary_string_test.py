"""
Unit tests for Simple Fun #194: Binary String challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_fun_number_194_binary_string import bin_str


@pytest.mark.parametrize("test_input, expected",
                         [("", 0), ("0101", 3),
                          ("10000", 2),
                          ("0000000000", 0),
                          ("1111111111", 1),
                          ("10101010101010", 14),
                          ("11111000011111", 3),
                          ("000001111100000", 2),
                          ("111000000000", 2),
                          ("00000000111111111", 1),
                          ("1010101011111111111111000000000", 10)])
def test_bin_str(test_input, expected):
    """
    Tests the Simple Fun #194: Binary String solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert bin_str(test_input) == expected
