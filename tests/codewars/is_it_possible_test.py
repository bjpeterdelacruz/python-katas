"""
Unit tests for Check That the Situation is Correct challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.is_it_possible import is_it_possible


@pytest.mark.parametrize("test_input, expected",
                         [("XXXXXXXXX", False), ("0XXXX000X", True), ("XXX0_0___", True),
                          ("_________", True), ("__0______", False), ("____X____", True),
                          ("XXX000___", False), ("0X_0X0_X_", False), ("0X00X0__X", False),
                          ("XX_X_X000", False), ("X000X0XXX", True), ("X_00X0XX0", True),
                          ("X0X___0XX", False), ("_X0_X00XX", True), ("0_X0_X0XX", False),
                          ("_0X_X_X00", False), ("0_0X00XXX", False), ("XXX00_X00", False),
                          ("X_0X0X0X_", False), ("XX0X0_0X_", False), ("___XXX___0", False)])
def test_int32_to_ip(test_input, expected):
    """
    Tests the Check That the Situation is Correct solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_it_possible(test_input) == expected
