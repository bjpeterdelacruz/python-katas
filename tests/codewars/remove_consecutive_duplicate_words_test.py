"""
Unit tests for Remove Consecutive Duplicate Words challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.remove_consecutive_duplicate_words import remove_consecutive_duplicates


@pytest.mark.parametrize("test_input, expected",
                         [("", ""),
                          ('alpha beta beta gamma gamma gamma delta alpha beta beta gamma gamma '
                          'gamma delta', 'alpha beta gamma delta alpha beta gamma delta'),
                          ("iIi IiI", "iIi IiI"),
                          ("aa a aa a aa", "aa a aa a aa"),
                          ("this his is is sih siht", "this his is sih siht"),
                          ("don't don t do not dont", "don't don t do not dont")])
def test_remove_consecutive_duplicates(test_input, expected):
    """
    Tests the Remove Consecutive Duplicate Words solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove_consecutive_duplicates(test_input) == expected
