"""
Unit tests for Array Mash challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.array_mash import array_mash


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3], ['a', 'b', 'c'], [1, 'a', 2, 'b', 3, 'c']),
                          ([1, 2, 3, 4, 5], ['a', 'b', 'c', 'd', 'e'],
                           [1, 'a', 2, 'b', 3, 'c', 4, 'd', 5, 'e']),
                          ([1, 1, 1, 1], [2, 2, 2, 2], [1, 2, 1, 2, 1, 2, 1, 2]),
                          ([1, 8, 'hello', 'dog'], ['fish', '2', 9, 10],
                           [1, "fish", 8, "2", "hello", 9, "dog", 10]),
                          ([None, 4], [None, 'hello'], [None, None, 4, "hello"]),
                          ([1], [2], [1, 2]),
                          (['h', 'l', 'o', 'o', 'l'], ['e', 'l', 'w', 'r', 'd'],
                           ["h", "e", "l", "l", "o", "w", "o", "r", "l", "d"])])
def test_array_mash(test_input1, test_input2, expected):
    """
    Tests the Array Mash solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert array_mash(test_input1, test_input2) == expected
