"""
Unit tests for Bubbly Programming Language challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.a_bubbly_programming_language import start, end, push, add, sub, mul, div


def test_methods():
    """
    Tests the Bubbly Programming Language solution.

    :return: None
    """
    assert (start)(push)(4)(push)(9)(div)(end) == 2
    assert (start)(push)(4)(push)(5)(add)(end) == 9
    assert (start)(push)(4)(push)(5)(push)(10)(add)(mul)(end) == 60
    assert (start)(push)(2)(push)(3)(push)(5)(push)(5)(add)(div)(sub)(end) == 1
    assert (start)(push)(2)(push)(1)(push)(5)(push)(10)(sub)(mul)(mul)(end) == 10
