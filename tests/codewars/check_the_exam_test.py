"""
Unit tests for Check the Exam challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.check_the_exam import check_exam


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(["a", "a", "b", "b"], ["a", "c", "b", "d"], 6),
                          (["a", "a", "c", "b"], ["a", "a", "b", ""], 7),
                          (["a", "a", "b", "c"], ["a", "a", "b", "c"], 16),
                          (["b", "c", "b", "a"], ["", "a", "a", "c"], 0)])
def test_check_exam(test_input1, test_input2, expected):
    """
    Tests the Check the Exam solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert check_exam(test_input1, test_input2) == expected
