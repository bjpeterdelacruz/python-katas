"""
Unit tests for Number of Trailing Zeros of N! challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.number_of_trailing_zeros_of_n import zeros


@pytest.mark.parametrize("test_input, expected",
                         [(315394464, 78848611), (269043054, 67260756), (140195193, 35048791),
                          (4, 0), (5, 1), (9, 1), (10, 2), (24, 4), (25, 6), (49, 10), (1000, 249)])
def test_zeros(test_input, expected):
    """
    Tests the Number of Trailing Zeros of N! solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert zeros(test_input) == expected
