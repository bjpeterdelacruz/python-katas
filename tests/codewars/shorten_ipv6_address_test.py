"""
Unit tests for Shorten IPv6 Address challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.shorten_ipv6_address import shorten


def test_ipv6_cannot_be_shortened():
    """
    Tests valid IPv6 addresses that cannot be shortened.

    :return: None
    """
    assert shorten('A43B:1CF6:541C:98AA:CC43:1092:E932:90AD') == \
           'A43B:1CF6:541C:98AA:CC43:1092:E932:90AD'
    assert shorten('9000:B004:C13A:594C:19CD:102D:394F:FCD1') == \
           '9000:B004:C13A:594C:19CD:102D:394F:FCD1'


def test_ipv6_leading_zeros():
    """
    Tests valid IPv6 addresses that has quadruplets that have leading zeros.

    :return: None
    """
    assert shorten('043B:00F6:541C:08AA:0003:1092:000D:90AD') == '43B:F6:541C:8AA:3:1092:D:90AD'
    assert shorten('9000:0004:000A:094C:00CD:102D:394F:0001') == '9000:4:A:94C:CD:102D:394F:1'


def test_ipv6_one_quadruplet_contains_only_zeros():
    """
    Tests valid IPv6 addresses that have only one quadruplet that contains all zeros.

    :return: None
    """
    assert shorten('3BDF:000E:0004:0ECD:0000:0009:3C7F:734F') == '3BDF:E:4:ECD::9:3C7F:734F'
    assert shorten('0388:0B7B:004D:0000:00D3:FDC1:E0E8:08D7') == '388:B7B:4D::D3:FDC1:E0E8:8D7'


def test_ipv6_multiple_quadruplets_contain_only_zeros():
    """
    Tests valid IPv6 addresses that have two or more quadruplets that contain all zeros.

    :return: None
    """
    assert shorten('0018:000A:0F0C:10B2:668D:0000:0000:009B') == '18:A:F0C:10B2:668D::9B'
    assert shorten('00AF:0000:0000:0000:0000:704E:EC20:3DAA') == 'AF::704E:EC20:3DAA'


def test_ipv6_quadruplets_contain_only_zeros_at_start_and_end():
    """
    Tests valid IPv6 addresses that have two or more quadruplets that contain all zeros at the
    beginning or at the end of the address.

    :return: None
    """
    assert shorten('0000:0000:0000:0000:0000:0C30:00DA:29CB') == '::C30:DA:29CB'
    assert shorten('97CA:4C84:B62B:C3A8:00F4:0000:0000:0000') == '97CA:4C84:B62B:C3A8:F4::'


def test_ipv6_multiple_quadruplets_contain_only_zeros_same_length():
    """
    Tests valid IPv6 addresses that have two or more sequences of quadruplets of the same length
    that contain all zeros.

    :return: None
    """
    assert shorten('A2A5:03DB:0000:60A5:0000:0005:BD22:0000') == 'A2A5:3DB::60A5:0:5:BD22:0'
    assert shorten('0000:0391:F08E:0F28:0000:0003:0037:0006') == '::391:F08E:F28:0:3:37:6'


def test_ipv6_multiple_quadruplets_contain_only_zeros_different_lengths():
    """
    Tests valid IPv6 addresses that have two or more sequences of quadruplets of different lengths
    that contain all zeros.

    :return: None
    """
    assert shorten('00C8:0000:0243:0050:ED26:008F:0000:0000') == 'C8:0:243:50:ED26:8F::'
    assert shorten('0000:0FBA:0000:0000:0000:0000:057E:AFFD') == '0:FBA::57E:AFFD'
    assert shorten('0000:0000:0001:0007:0F63:0000:4FF7:0000') == '::1:7:F63:0:4FF7:0'
    assert shorten('0000:0000:6B6F:63B3:0000:0001:0000:0000') == '::6B6F:63B3:0:1:0:0'
