"""
Unit tests for Well of Ideas: Easy Version challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.well_of_ideas_easy_version import well


@pytest.mark.parametrize("test_input, expected",
                         [(['bad', 'bad', 'bad'], "Fail!"),
                          (['good', 'bad', 'bad', 'bad', 'bad'], "Publish!"),
                          (['good', 'bad', 'bad', 'bad', 'bad', 'good', 'bad', 'bad', 'good'],
                           "I smell a series!")])
def test_well(test_input, expected):
    """
    Tests the Well of Ideas: Easy Version solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert well(test_input) == expected
