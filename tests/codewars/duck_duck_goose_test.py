"""
Unit tests for Duck Duck Goose challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.duck_duck_goose import duck_duck_goose, Player

p1 = Player("a")
p2 = Player("b")
p3 = Player("c")
p4 = Player("d")
players = [p1, p2, p3, p4]


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(players, 1, "a"), (players, 2, "b"), (players, 3, "c"),
                          (players, 4, "d"), (players, 5, "a")])
def test_duck_duck_goose(test_input1, test_input2, expected):
    """
    Tests the Duck Duck Goose solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert duck_duck_goose(test_input1, test_input2) == expected
