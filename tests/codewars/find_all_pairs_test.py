"""
Unit tests for Find All Pairs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_all_pairs import duplicates


@pytest.mark.parametrize("arr_input, expected_count", [
    ([], 0),  # Empty array
    ([1, 2, 3, 4, 5], 0),  # No duplicates
    ([1, 1, 1, 1], 2),  # All duplicates
    ([1, 2, 2, 3, 3, 3, 4, 5, 5], 3),  # Some duplicates
    ([1, 1, 2, 2, 3, 3, 3, 4, 5], 3),  # Odd counts
    ([1000, 1000, 2000, 2000, 3000, 3000], 3)  # Large numbers
])
def test_duplicates(arr_input, expected_count):
    """
    Tests the Find All Pairs solution.

    :param arr_input: A list of integers
    :param expected_count: The expected number of pairs
    :return: None
    """
    assert duplicates(arr_input) == expected_count
