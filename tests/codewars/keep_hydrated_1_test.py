"""
Unit tests for Keep Hydrated! challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.keep_hydrated_1 import litres


@pytest.mark.parametrize("test_input, expected",
                         [(2, 1),
                          (1.4, 0),
                          (12.3, 6),
                          (0.82, 0),
                          (11.8, 5),
                          (1787, 893),
                          (0, 0)])
def test_litres(test_input, expected):
    """
    Tests the Keep Hydrated! solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert litres(test_input) == expected
