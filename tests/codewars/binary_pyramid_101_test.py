"""
Unit tests for Binary Pyramid 101 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.binary_pyramid_101 import binary_pyramid


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(1, 4, "1111010"),
                          (4, 1, "1111010"),
                          (1, 6, "101001101"),
                          (6, 20, "1110010110100011"),
                          (21, 60, "1100000100010001010100"),
                          (100, 100, "100001100100101000100"),
                          (1, 1, "1"),
                          (0, 1, "1"),
                          (1, 0, "1"),
                          (0, 0, "0"),
                          (1, 100, "10011101010010110000101010"),
                          (100, 1000, "111111001111110110011011000101110101110")])
def test_binary_pyramid(test_input1, test_input2, expected):
    """
    Tests the Binary Pyramid 101 solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert binary_pyramid(test_input1, test_input2) == expected
