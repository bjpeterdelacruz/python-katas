"""
Unit tests for Array Element Parity challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.array_element_parity import solve


@pytest.mark.parametrize("test_input, expected",
                         [([-1, 1, 2, -2, 3], 3), ([-3, 3, -4, -4, 2, -2, -1, 1], -4)])
def test_solve(test_input, expected):
    """
    Tests the Array Element Parity solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert solve(test_input) == expected
