"""
Unit tests for Finish Guess the Number Game challenge

:Author: BJ Peter Dela Cruz
"""
from pytest import raises
from codewars.finish_guess_the_number_game import Guesser


def test_one_life_win():
    """
    Tests the Finish Guess the Number Game solution with only one life.

    :return: None
    """
    guesser = Guesser(5, 1)
    assert guesser.number == 5
    assert guesser.lives == 1
    assert guesser.guess(5) is True
    assert guesser.lives == 1


def test_one_life_game_over():
    """
    Tests the Finish Guess the Number Game solution with only one life.

    :return: None
    """
    guesser = Guesser(5, 1)
    assert guesser.number == 5
    assert guesser.lives == 1
    assert guesser.guess(6) is False
    assert guesser.lives == 0
    with raises(ValueError):
        guesser.guess(4)
        assert False


def test_no_life():
    """
    Tests the Finish Guess the Number Game solution with only no lives.

    :return: None
    """
    guesser = Guesser(5, 0)
    assert guesser.number == 5
    assert guesser.lives == 0
    with raises(ValueError):
        guesser.guess(5)
        assert False


def test_three_lives_win():
    """
    Tests the Finish Guess the Number Game solution with three lives.

    :return: None
    """
    guesser = Guesser(5, 3)
    assert guesser.number == 5
    assert guesser.lives == 3
    assert guesser.guess(6) is False
    assert guesser.lives == 2
    assert guesser.guess(4) is False
    assert guesser.lives == 1
    assert guesser.guess(5) is True
    assert guesser.lives == 1
