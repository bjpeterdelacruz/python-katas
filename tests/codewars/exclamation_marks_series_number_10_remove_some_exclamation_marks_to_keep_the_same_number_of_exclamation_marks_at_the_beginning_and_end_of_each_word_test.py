"""
Unit tests for Exclamation Marks Series #10 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_10_remove_some_exclamation_marks_to_keep_the_same_number_of_exclamation_marks_at_the_beginning_and_end_of_each_word import remove  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [('Hi!', 'Hi'), ('!Hi!!!', '!Hi!'), ('!Hi', 'Hi'), ('!Hi!', '!Hi!'),
                          ('Hi! Hi!', 'Hi Hi'), ('!!Hi!!! !!Hello !!!Bye!', '!!Hi!! Hello !Bye!')])
def test_remove(test_input, expected):
    """
    Tests the Exclamation Marks Series #10 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove(test_input) == expected
