"""
Unit tests for Thinking and Testing: A and B? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinking_and_testing_a_and_b import a_and_b


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(0, 1, 1), (1, 2, 3), (10, 20, 30), (1, 1, 1), (1, 3, 3)])
def test_a_and_b(test_input1, test_input2, expected):
    """
    Tests the Thinking and Testing: A and B? solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert a_and_b(test_input1, test_input2) == expected
