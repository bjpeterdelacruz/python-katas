"""
Unit tests for int32 to IPv4 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.int32_to_ipv4 import int32_to_ip


@pytest.mark.parametrize("test_input, expected",
                         [(0, "0.0.0.0"), (2149583361, "128.32.10.1"), (32, "0.0.0.32"),
                          (168430090, "10.10.10.10")])
def test_int32_to_ip(test_input, expected):
    """
    Tests the int32 to IPv4 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert int32_to_ip(test_input) == expected
