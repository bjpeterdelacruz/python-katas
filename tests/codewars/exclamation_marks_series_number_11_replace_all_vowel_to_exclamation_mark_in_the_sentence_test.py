"""
Unit tests for Exclamation Marks Series #11 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_11_replace_all_vowel_to_exclamation_mark_in_the_sentence import replace_exclamation  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [('Aloha', '!l!h!'), ('', ''), ('me_happy_0', 'm!_h!ppy_0'),
                          ('p@ssw0rd!', 'p@ssw0rd!')])
def test_replace_exclamation(test_input, expected):
    """
    Tests the Exclamation Marks Series #11 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert replace_exclamation(test_input) == expected
