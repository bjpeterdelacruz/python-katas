"""
Unit tests for Kill K-th Bit challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_fun_number_8_kill_k_th_bit import kill_kth_bit


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(37, 3, 33),
                          (37, 4, 37),
                          (0, 4, 0),
                          (2147483647, 16, 2147450879),
                          (374823748, 13, 374819652),
                          (1084197039, 15, 1084197039),
                          (1459345320, 31, 385603496)])
def test_kill_kth_bit(test_input1, test_input2, expected):
    """
    Tests the Kill K-th Bit solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert kill_kth_bit(test_input1, test_input2) == expected
