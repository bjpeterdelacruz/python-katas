"""
Unit tests for Number of Letters of Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.numbers_of_letters_of_numbers import numbers_of_letters


@pytest.mark.parametrize("test_input, expected",
                         [(1, ["one", "three", "five", "four"]),
                          (2, ["two", "three", "five", "four"]),
                          (3, ["three", "five", "four"]),
                          (4, ["four"]),
                          (12, ["onetwo", "six", "three", "five", "four"]),
                          (37, ["threeseven", "onezero", "seven", "five", "four"]),
                          (311, ['threeoneone', 'oneone', 'six', 'three', 'five', 'four']),
                          (999, ["nineninenine", "onetwo", "six", "three", "five", "four"])])
def test_numbers_of_letters(test_input, expected):
    """
    Tests the Number of Letters of Numbers solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert numbers_of_letters(test_input) == expected
