"""
Unit tests for Age Range Compatibility Equation challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.age_range_compatibility_equation import dating_range


@pytest.mark.parametrize("test_input, expected",
                         [(17, "15-20"),
                          (40, "27-66"),
                          (15, "14-16"),
                          (35, "24-56"),
                          (10, "9-11"),
                          (53, "33-92"),
                          (19, "16-24"),
                          (12, "10-13"),
                          (7, "6-7"),
                          (33, "23-52")])
def test_dating_range(test_input, expected):
    """
    Tests the Age Range Compatibility Equation solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert dating_range(test_input) == expected
