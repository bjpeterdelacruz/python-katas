"""
Unit tests for Looking for a Benefactor challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.looking_for_a_benefactor import new_avg


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([14, 30, 5, 7, 9, 11, 16], 90, 628),
                          ([14, 30, 5, 7, 9, 11, 15], 92, 645),
                          ([1400.25, 30000.76, 5.56, 7, 9, 11, 15.48, 120.98], 10000, 58430),
                          ([1400.25, 30000.76, 5.56, 7, 9, 11, 15.48, 120.98], 5800, 20630),
                          ([1400.25, 30000.76, 5.56, 7, 9, 11, 15.48, 120.98], 4800, 11630),
                          ([], 90, 90)])
def test_new_avg(test_input1, test_input2, expected):
    """
    Tests the Looking for a Benefactor solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert new_avg(test_input1, test_input2) == expected


@pytest.mark.parametrize("test_input1, test_input2",
                         [([14, 30, 5, 7, 9, 11, 15], 2),
                          ([14.25, 30.76, 5.56, 7, 9, 11, 15.48, 12.987], 5),
                          ([1400.25, 30000.76, 5.56, 7, 9, 11, 15.48, 120.98], 2000)])
def test_new_avg_error(test_input1, test_input2):
    """
    Tests whether a ValueError is raised on invalid input.

    :param test_input1: Test input
    :param test_input2: Test input
    :return: None
    """
    with pytest.raises(ValueError):
        new_avg(test_input1, test_input2)
        assert False
