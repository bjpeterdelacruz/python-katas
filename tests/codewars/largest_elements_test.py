"""
Unit tests for Largest Elements challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.largest_elements import largest


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(2, [10, 9, 8, 7, 6, 5, 4, 3, 2, 1], [9, 10]),
                          (3, [1, 5, 3, 5, 7], [5, 5, 7])])
def test_largest(test_input1, test_input2, expected):
    """
    Tests the Largest Elements solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert largest(test_input1, test_input2) == expected
