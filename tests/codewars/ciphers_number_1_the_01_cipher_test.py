"""
Unit tests for Ciphers #1 - The 01 Cipher challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.ciphers_number_1_the_01_cipher import encode


@pytest.mark.parametrize("test_input, expected",
                         [("Hello World!", "10110 00111!"), ("~~ | abcd | ~~", "~~ | 0101 | ~~")])
def test_encode(test_input, expected):
    """
    Tests the Ciphers #1 - The 01 Cipher solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert encode(test_input) == expected
