"""
Unit tests for Address Book by State challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.address_book_by_state import by_state

CASE_1 = """John Daggett, 341 King Road, Plymouth MA
Ann Ford, 22 East Broadway, Richmond VA
Alice Ford, 123 East Broadway, Richmond VA
Sal Carpenter, 73 6th Street, Boston MA

"""

CASE_2 = """John Daggett, 341 King Road, Plymouth MA
Alice Ford, 22 East Broadway, Richmond VA
Orville Thomas, 11345 Oak Bridge Road, Tulsa OK
Terry Kalkas, 402 Lans Road, Beaver Falls PA
Eric Adams, 20 Post Road, Sudbury MA
Hubert Sims, 328A Brook Road, Roanoke MA
Amy Wilde, 334 Bayshore Pkwy, Mountain View CA
Sal Carpenter, 73 6th Street, Boston MA
"""


def test_by_state():
    """
    Tests the Address Book by State solution.

    :return: None
    """
    answer = "Massachusetts\r\n..... John Daggett 341 King Road Plymouth Massachusetts" \
             "\r\n..... Sal Carpenter 73 6th Street Boston Massachusetts\r\n Virginia" \
             "\r\n..... Alice Ford 123 East Broadway Richmond Virginia" \
             "\r\n..... Ann Ford 22 East Broadway Richmond Virginia"
    assert by_state(CASE_1) == answer

    answer = "California\r\n..... Amy Wilde 334 Bayshore Pkwy Mountain View California" \
             "\r\n Massachusetts\r\n..... Eric Adams 20 Post Road Sudbury Massachusetts" \
             "\r\n..... Hubert Sims 328A Brook Road Roanoke Massachusetts" \
             "\r\n..... John Daggett 341 King Road Plymouth Massachusetts" \
             "\r\n..... Sal Carpenter 73 6th Street Boston Massachusetts\r\n Oklahoma" \
             "\r\n..... Orville Thomas 11345 Oak Bridge Road Tulsa Oklahoma\r\n Pennsylvania" \
             "\r\n..... Terry Kalkas 402 Lans Road Beaver Falls Pennsylvania\r\n Virginia" \
             "\r\n..... Alice Ford 22 East Broadway Richmond Virginia"
    assert by_state(CASE_2) == answer
