"""
Unit tests for Bits Battle challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.bits_battle import bits_battle


@pytest.mark.parametrize("test_input, expected",
                         [([5, 3, 14], "odds win"), ([3, 8, 22, 15, 78], "evens win"), ([], "tie"),
                          ([1, 13, 16], "tie")])
def test_bits_battle(test_input, expected):
    """
    Tests the Bits Battle solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert bits_battle(test_input) == expected
