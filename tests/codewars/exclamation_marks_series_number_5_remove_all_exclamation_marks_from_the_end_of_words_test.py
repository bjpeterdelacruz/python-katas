"""
Unit tests for Exclamation Marks Series #5 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_5_remove_all_exclamation_marks_from_the_end_of_words import remove  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [('Hi!', 'Hi'), ('Hi!!!', 'Hi'), ('!Hi', '!Hi'), ('!Hi!', '!Hi'),
                          ('Hi! Hi!', 'Hi Hi'), ('Hi', 'Hi'),
                          ('Hi! Hello!! Bye!!!', 'Hi Hello Bye')])
def test_remove(test_input, expected):
    """
    Tests the Exclamation Marks Series #5 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove(test_input) == expected
