"""
Unit tests for Filling an Array (Part 1) challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.filling_an_array_part_1 import arr


@pytest.mark.parametrize("test_input, expected",
                         [(3, [0, 1, 2]), (10, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]), (0, [])])
def test_arr_1(test_input, expected):
    """
    Tests the Filling an Array (Part 1) solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert arr(test_input) == expected


def test_arr_2():
    """
    Tests the Filling an Array (Part 1) solution without any parameters.

    :return: None
    """
    assert not arr()
