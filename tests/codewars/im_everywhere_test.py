"""
Unit tests for I'm Everywhere challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.im_everywhere import i


@pytest.mark.parametrize("test_input, expected",
                         [('Phone', 'iPhone'), ('World', 'iWorld'), ('Human', 'iHuman'),
                          ('Programmer', 'iProgrammer'), ('', 'Invalid word'),
                          ('Inspire', 'Invalid word'), ('East', 'Invalid word'),
                          ('Peace', 'Invalid word'), ('road', 'Invalid word')])
def test_i(test_input, expected):
    """
    Tests the I'm Everywhere solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert i(test_input) == expected
