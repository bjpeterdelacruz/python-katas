"""
Unit tests for Make the Deadfish Swim challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.make_the_deadfish_swim import parse


@pytest.mark.parametrize("test_input, expected",
                         [("iiisdoso", [8, 64]), (["ooo", [0, 0, 0]]), ("abc", [])])
def test_parse(test_input, expected):
    """
    Tests the Make the Deadfish Swim solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert parse(test_input) == expected
