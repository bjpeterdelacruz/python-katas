"""
Unit tests for Swap Items in a Dictionary challenge

:Author: BJ Peter Dela Cruz
"""
from collections import Counter
import pytest
from codewars.swap_items_in_a_dictionary import switch_dict


@pytest.mark.parametrize("test_input, expected",
                         [({
                               'Ice': 'Cream',
                               'Age': '21',
                               'Light': 'Cream',
                               'Double': 'Cream'
                           }, {
                               'Cream': ['Ice', 'Double', 'Light'],
                               '21': ['Age']
                           }), ({
                                    'Ice': 'Cream',
                                    'Heavy': 'Cream',
                                    'Light': 'Cream',
                                    'Double': 'Cream'
                                }, {'Cream': ['Ice', 'Double', 'Light', 'Heavy']})])
def test_switch_dict(test_input, expected):
    """
    Tests the Swap Items in a Dictionary solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    actual = switch_dict(test_input)
    for key, value in actual.items():
        assert key in expected
        assert Counter(value) == Counter(expected[key])
