"""
Unit tests for Exclamation Marks Series #16 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_16_find_the_longest_successive_exclamation_marks_and_question_marks_combination_in_the_string import find  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [("!!", ""), ("!??", "!??"), ("!?!!", "?!!"), ("!!???!????", "!!???"),
                          ("!!???!?????", "!?????"), ("!????!!!?", "????!!!"),
                          ("!?!!??!!!?", "??!!!")])
def test_find(test_input, expected):
    """
    Tests the Exclamation Marks Series #16 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find(test_input) == expected
