"""
Unit tests for Moves in Squared Strings 4 challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.moves_in_squared_strings_iv import diag_2_sym, rot_90_counter, \
    selfie_diag2_counterclock, oper


def test_oper():
    """
    Tests the Moves in Squared Strings 4 solution.

    :return: None
    """
    test = "abcd\nefgh\nijkl\nmnop"

    assert oper(diag_2_sym, test) == "plhd\nokgc\nnjfb\nmiea"
    assert oper(rot_90_counter, test) == "dhlp\ncgko\nbfjn\naeim"
    assert oper(selfie_diag2_counterclock, test) == "abcd|plhd|dhlp\nefgh|okgc|cgko" \
                                                    "\nijkl|njfb|bfjn\nmnop|miea|aeim"

    assert oper(rot_90_counter, "EcGcXJ\naaygcA\nNgIshN\nyOrCZE\neBEqpm\nNkxCgw") == \
           "JANEmw\nXchZpg\ncgsCqC\nGyIrEx\ncagOBk\nEaNyeN"
    assert oper(diag_2_sym, "LmvLyg\nDKELBm\nylJhui\nXRXqHD\nzlisCT\nhPqxYb") == \
           "bTDimg\nYCHuBy\nxsqhLL\nqiXJEv\nPlRlKm\nhzXyDL"
    assert oper(selfie_diag2_counterclock, "NJVGhr\nMObsvw\ntPhCtl\nsoEnhi\nrtQRLK\nzjliWg") == \
           "NJVGhr|gKilwr|rwliKg\nMObsvw|WLhtvh|hvthLW\ntPhCtl|iRnCsG|GsCnRi" \
           "\nsoEnhi|lQEhbV|VbhEQl\nrtQRLK|jtoPOJ|JOPotj\nzjliWg|zrstMN|NMtsrz"
