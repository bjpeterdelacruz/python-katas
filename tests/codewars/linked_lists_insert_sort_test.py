"""
Unit tests for Linked Lists: Insert Sort challenge

:Author: BJ Peter Dela Cruz
"""
from utils.linked_list import Node, build_one_two, build_two_one
from utils.linked_list import build_one_two_three, build_three_two_one, build_nodes
from codewars.linked_lists_insert_sort import insert_sort


def test_insert_sort_none_or_one():
    """
    Assert that None is returned or a linked list with only one element is returned.

    :return: None
    """
    assert insert_sort(None) is None

    assert insert_sort(Node(5)).data == 5
    assert insert_sort(Node(5)).next is None


def test_insert_sort_three_elements():
    """
    Assert that elements are in sorted order as they are being inserted into a linked list.

    :return: None
    """
    assert insert_sort(build_one_two()).data == 1
    assert insert_sort(build_one_two()).next.data == 2
    assert insert_sort(build_one_two()).next.next is None

    assert insert_sort(build_two_one()).data == 1
    assert insert_sort(build_two_one()).next.data == 2
    assert insert_sort(build_two_one()).next.next is None

    assert insert_sort(build_one_two_three()).data == 1
    assert insert_sort(build_one_two_three()).next.data == 2
    assert insert_sort(build_one_two_three()).next.next.data == 3
    assert insert_sort(build_one_two_three()).next.next.next is None

    assert insert_sort(build_three_two_one()).data == 1
    assert insert_sort(build_three_two_one()).next.data == 2
    assert insert_sort(build_three_two_one()).next.next.data == 3
    assert insert_sort(build_three_two_one()).next.next.next is None


def test_insert_sort_more_elements():
    """
    Assert that elements are in sorted order as they are being inserted into a linked list.

    :return: None
    """
    assert insert_sort(build_nodes([4, 8, 1, 3, 2, 9, 6, 5, 9, 2])).data == 1
    assert insert_sort(build_nodes([4, 8, 1, 3, 2, 9, 6, 5, 9, 2])).next.data == 2
    assert insert_sort(build_nodes([4, 8, 1, 3, 2, 9, 6, 5, 9, 2])).next.next.data == 2
    assert insert_sort(build_nodes([4, 8, 1, 3, 2, 9, 6, 5, 9, 2])).next.next.next.data == 3
    assert insert_sort(build_nodes([4, 8, 1, 3, 2, 9, 6, 5, 9, 2])).next.next.next.next.data == 4
    assert insert_sort(build_nodes([4, 8, 1, 3, 2, 9, 6, 5, 9, 2]))\
               .next.next.next.next.next.data == 5
    assert insert_sort(build_nodes([4, 8, 1, 3, 2, 9, 6, 5, 9, 2]))\
               .next.next.next.next.next.next.data == 6
    assert insert_sort(build_nodes([4, 8, 1, 3, 2, 9, 6, 5, 9, 2]))\
               .next.next.next.next.next.next.next.data == 8
    assert insert_sort(build_nodes([4, 8, 1, 3, 2, 9, 6, 5, 9, 2]))\
               .next.next.next.next.next.next.next.next.data == 9
    assert insert_sort(build_nodes([4, 8, 1, 3, 2, 9, 6, 5, 9, 2]))\
               .next.next.next.next.next.next.next.next.next.data == 9
    assert insert_sort(build_nodes([4, 8, 1, 3, 2, 9, 6, 5, 9, 2]))\
               .next.next.next.next.next.next.next.next.next.next is None
