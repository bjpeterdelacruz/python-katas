"""
Unit tests for I Love You challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.i_love_you_a_little_a_lot_passionately_dot_dot_dot_not_at_all \
    import how_much_i_love_you


@pytest.mark.parametrize("test_input, expected",
                         [(1, "I love you"),
                          (3, "a lot"),
                          (6, "not at all"),
                          (7, "I love you"),
                          (10, "passionately")])
def test_how_much_i_love_you(test_input, expected):
    """
    Tests the I Love You solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert how_much_i_love_you(test_input) == expected
