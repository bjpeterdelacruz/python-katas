"""
Unit tests for Twisted Sum challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.twisted_sum import compute_sum


@pytest.mark.parametrize("test_input, expected",
                         [(1, 1), (2, 3), (3, 6), (4, 10), (10, 46), (12, 51)])
def test_compute_sum(test_input, expected):
    """
    Tests the Twisted Sum solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert compute_sum(test_input) == expected
