"""
Unit tests for Transportation on Vacation challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.transportation_on_vacation import rental_car_cost


@pytest.mark.parametrize("test_input, expected",
                         [(2, 80), (3, 100), (4, 140), (6, 220), (7, 230), (8, 270)])
def test_rental_car_cost(test_input, expected):
    """
    Tests the Transportation on Vacation solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert rental_car_cost(test_input) == expected
