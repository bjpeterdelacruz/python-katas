"""
Unit tests for Bit Counting challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.bit_counting import count_bits


@pytest.mark.parametrize("test_input, expected",
                         [(1234, 5)])
def test_count_bits(test_input, expected):
    """
    Tests the Bit Counting solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert count_bits(test_input) == expected
