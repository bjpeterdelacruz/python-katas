"""
Unit tests for Cut Rope challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.cut_rope import cut_rope


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(6, 2, 3, {'1cm': 2, '2cm': 2}),
                          (7, 2, 3, {'1cm': 3, '2cm': 2}),
                          (10, 2, 3, {'1cm': 4, '2cm': 3}),
                          (10, 2, 5, {'1cm': 2, '2cm': 4}),
                          (11, 2, 5, {'1cm': 3, '2cm': 4}),
                          (10000, 3, 5, {'1cm': 1334, '2cm': 1333, '3cm': 2000}),
                          (1, 2, 3, {'1cm': 1}),
                          (2, 2, 3, {'2cm': 1}),
                          (3, 2, 3, {'1cm': 1, '2cm': 1}),
                          (100, 100, 1, {'1cm': 100})
                          ])
def test_cut_rope(test_input1, test_input2, test_input3, expected):
    """
    Tests the Cut Rope solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert cut_rope(test_input1, test_input2, test_input3) == expected
