"""
Unit tests for Sort and Star challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sort_and_star import two_sort


@pytest.mark.parametrize("test_input, expected",
                         [(["bitcoin", "take", "over", "the", "world", "maybe", "who", "knows",
                            "perhaps"],
                           'b***i***t***c***o***i***n'),
                          (["turns", "out", "random", "test", "cases", "are", "easier",
                            "than", "writing", "out", "basic",
                            "ones"], 'a***r***e'),
                          (["lets", "talk", "about", "javascript", "the", "best", "language"],
                           'a***b***o***u***t'),
                          (["i", "want", "to", "travel", "the", "world", "writing", "code", "one",
                            "day"],
                           'c***o***d***e'),
                          (["Lets", "all", "go", "on", "holiday", "somewhere", "very", "cold"],
                           'L***e***t***s')])
def test_two_sort(test_input, expected):
    """
    Tests the Sort and Star solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert two_sort(test_input) == expected
