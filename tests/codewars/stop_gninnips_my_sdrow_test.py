"""
Unit tests for Stop Spinning My Words challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.stop_gninnips_my_sdrow import spin_words


@pytest.mark.parametrize("test_input, expected",
                         [("Hey fellow warriors", "Hey wollef sroirraw"),
                          ("This is another test", "This is rehtona test")])
def test_spin_words(test_input, expected):
    """
    Tests the Stop Spinning My Words solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert spin_words(test_input) == expected
