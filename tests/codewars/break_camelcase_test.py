"""
Unit tests for Break camelCase challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.break_camelcase import solution


@pytest.mark.parametrize("test_input, expected",
                         [("helloWorld", "hello World"), ("camelCase", "camel Case"),
                          ("breakCamelCase", "break Camel Case")])
def test_solution(test_input, expected):
    """
    Tests the Break camelCase solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert solution(test_input) == expected
