"""
Unit tests for Count Consonants challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.count_consonants import consonant_count


@pytest.mark.parametrize("test_input, expected",
                         [('', 0),
                          ('aaaaa', 0),
                          ('XaeiouX', 2),
                          ('Bbbbb', 5),
                          ('helLo world', 7),
                          ('h^$&^#$&^elLo world', 7),
                          ('0123456789', 0),
                          ('012345_Cb', 2)])
def test_consonant_count(test_input, expected):
    """
    Tests the Count Consonants solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert consonant_count(test_input) == expected
