"""
Unit tests for Thinkful: User Contacts challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_dictionary_drills_user_contacts import user_contacts


@pytest.mark.parametrize("test_input, expected",
                         [([["Gary", 1234], ["Larry"]], {"Gary": 1234, "Larry": None})])
def test_user_contacts(test_input, expected):
    """
    Tests the Thinkful: User Contacts solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert user_contacts(test_input) == expected
