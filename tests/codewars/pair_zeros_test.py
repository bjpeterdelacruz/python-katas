"""
Unit tests for Pair Zeros challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.pair_zeros import pair_zeros


@pytest.mark.parametrize("test_input, expected",
                         [([], []), ([1], [1]), ([1, 2, 3], [1, 2, 3]), ([0], [0]), ([0, 0], [0]),
                          ([1, 0, 1, 0, 2, 0, 0], [1, 0, 1, 2, 0]), ([0, 0, 0], [0, 0]),
                          ([1, 0, 1, 0, 2, 0, 0, 3, 0], [1, 0, 1, 2, 0, 3, 0]),
                          ([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]),
                          ([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0])])
def test_pair_zeros(test_input, expected):
    """
    Tests the Pair Zeros solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert pair_zeros(test_input) == expected
