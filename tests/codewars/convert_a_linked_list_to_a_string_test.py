"""
Unit tests for Convert Linked List to a String challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.convert_a_linked_list_to_a_string import Node, stringify


@pytest.mark.parametrize("test_input, expected",
                         [(Node(0, Node(1, Node(2, Node(3)))), '0 -> 1 -> 2 -> 3 -> None'),
                          (None, 'None'),
                          (Node(0, Node(1, Node(4, Node(9, Node(16))))),
                           '0 -> 1 -> 4 -> 9 -> 16 -> None'),
                          (Node(23), '23 -> None'),
                          (None, 'None'),
                          (Node(4, Node(25)), '4 -> 25 -> None')])
def test_stringify(test_input, expected):
    """
    Tests the Convert Linked List to a String solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert stringify(test_input) == expected
