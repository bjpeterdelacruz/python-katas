"""
Unit tests for Thinkful: Logic Drills - Traffic Light challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_logic_drills_traffic_light import update_light


@pytest.mark.parametrize("test_input, expected",
                         [("green", "yellow"), ("yellow", "red"), ("red", "green")])
def test_update_light(test_input, expected):
    """
    Tests the Thinkful: Logic Drills - Traffic Light solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert update_light(test_input) == expected
