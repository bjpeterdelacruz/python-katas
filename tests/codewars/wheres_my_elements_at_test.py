"""
Unit tests for Where's My Elements At? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.wheres_my_elements_at import element_location


@pytest.mark.parametrize("test_input1, test_input2, test_input3, test_input4, expected",
                         [(0x1000, 0x1040, 0x3, 0x8, 0x1018),
                          (0x2000, 0x2100, 0x3, 0x4, 0x200C),
                          (0x2000, 0x2100, 0x0, 0x4, 0x2000),
                          (0x60D837C, 0x60D84D0, 0x1, 0x4, 0x60D8380),
                          (0x30A7C, 0x130BF0, 0x3000, 0x4, 0x3CA7C)])
def test_element_location(test_input1, test_input2, test_input3, test_input4, expected):
    """
    Tests the Where's My Elements At? solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param test_input4: Test input
    :param expected: Expected result
    :return: None
    """
    assert element_location(test_input1, test_input2, test_input3, test_input4) == expected


def test_element_location_indexerror_1():
    """
    Tests the Where's My Elements At? solution with invalid input.

    :return: None
    """
    with pytest.raises(IndexError):
        element_location(0x1000, 0x1040, 0x8, 0x8)
        assert False


def test_element_location_indexerror_2():
    """
    Tests the Where's My Elements At? solution with invalid input.

    :return: None
    """
    with pytest.raises(IndexError):
        element_location(0x1000, 0x1040, -1, 0x8)
        assert False


def test_element_location_indexerror_3():
    """
    Tests the Where's My Elements At? solution with invalid input.

    :return: None
    """
    with pytest.raises(IndexError):
        element_location(0x60D837C, 0x60D84D0, -7, 0x4)
        assert False


def test_element_location_indexerror_4():
    """
    Tests the Where's My Elements At? solution with invalid input.

    :return: None
    """
    with pytest.raises(IndexError):
        element_location(0x60D837C, 0x60D84D0, 0xF0, 0x4)
        assert False
