"""
Unit tests for Human Readable Duration Format challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.human_readable_duration_format import format_duration


@pytest.mark.parametrize("test_input, expected",
                         [(0, "now"), (60, "1 minute"), (61, "1 minute and 1 second"),
                          ((60 * 60 * 24 * 365) + (60 * 60 * 24 * 2) + 61,
                           "1 year, 2 days, 1 minute and 1 second")])
def test_format_duration(test_input, expected):
    """
    Tests the Human Readable Duration Format solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert format_duration(test_input) == expected
