"""
Unit tests for Are They The "Same"? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.are_they_the_same import comp


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([121, 144, 19, 161, 19, 144, 19, 11],
                           [11 * 11, 121 * 121, 144 * 144, 19 * 19, 161 * 161, 19 * 19, 144 * 144,
                            19 * 19], True), ([], [121], False), ([11], [], False),
                          ([11], [121, 100], False), (None, [121], False), ([11], None, False),
                          ([10], [121], False),
                          ([64, 49, 46, 68, 9, 13, 9, 89],
                           [4096, 2401, 2116, 4624, 81, 169, 82, 7921], False),
                          ([2, 2, 3], [4, 9, 9], False)])
def test_comp(test_input1, test_input2, expected):
    """
    Tests the "Are They The "Same"?" solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert comp(test_input1, test_input2) == expected
