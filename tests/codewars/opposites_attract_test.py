"""
Unit tests for Opposites Attract challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.opposites_attract import lovefunc


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(5, 2, True), (4, 1, True), (2, 4, False), (3, 1, False)])
def test_lovefunc(test_input1, test_input2, expected):
    """
    Tests the Opposites Attract solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert lovefunc(test_input1, test_input2) == expected
