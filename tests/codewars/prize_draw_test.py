"""
Unit tests for Prize Draw challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.prize_draw import rank


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [("Addison,Jayden,Sofia,Michael,Andrew,Lily,Benjamin",
                           [4, 2, 1, 4, 3, 1, 2], 4, "Benjamin"),
                          ("Lagon,Lily", [1, 5], 2, "Lagon"),
                          ("Addison,Jayden,Sofia,Michael,Andrew,Lily,Benjamin",
                           [4, 2, 1, 4, 3, 1, 2], 8, "Not enough participants"),
                          ("", [4, 2, 1, 4, 3, 1, 2], 6, "No participants")])
def test_rank(test_input1, test_input2, test_input3, expected):
    """
    Tests the Prize Draw solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert rank(test_input1, test_input2, test_input3) == expected
