"""
Unit tests for Sum Fibs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sumfibs import sum_fibs


@pytest.mark.parametrize("test_input, expected",
                         [(5, 2), (9, 44), (10, 44), (11, 44), (20, 3382)])
def test_sum_fibs(test_input, expected):
    """
    Tests the Sum Fibs solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_fibs(test_input) == expected
