"""
Unit tests for Find All Occurrences of an Element in an Array challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_all_occurrences_of_an_element_in_an_array import find_all


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3, 4, 5], 10, []), ([1, 2, 1, 3, 4, 5, 1], 1, [0, 2, 6])])
def test_find_all(test_input1, test_input2, expected):
    """
    Tests the Find All Occurrences of an Element in an Array solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_all(test_input1, test_input2) == expected
