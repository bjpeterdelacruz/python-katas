"""
Unit tests for Factor Sum challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_fun_number_115_factor_sum import factor_sum


@pytest.mark.parametrize("test_input, expected",
                         [(24, 5)])
def test_buddy(test_input, expected):
    """
    Tests the Factor Sum solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert factor_sum(test_input) == expected
