"""
Unit tests for Multiples and Digit Sums challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.multiples_and_digit_sums import procedure


@pytest.mark.parametrize("test_input, expected",
                         [(30, 18),
                          (12, 72),
                          (49, 30),
                          (17, 48),
                          (10, 46)])
def test_procedure(test_input, expected):
    """
    Tests the Multiples and Digit Sums solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert procedure(test_input) == expected
