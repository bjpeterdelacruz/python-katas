"""
Unit tests for Compare Two-Digit numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.compare_two_digit_numbers import compare


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(10, 11, "50%"),
                          (33, 33, "100%"),
                          (45, 45, "100%"),
                          (29, 92, "100%"),
                          (14, 24, "50%"),
                          (56, 57, "50%"),
                          (38, 84, "50%"),
                          (10, 22, "0%"),
                          (11, 44, "0%"),
                          (98, 70, "0%"),
                          (66, 16, "50%"),
                          (98, 88, "50%"),
                          (78, 58, "50%"),
                          (47, 56, "0%")])
def test_compare(test_input1, test_input2, expected):
    """
    Tests the Compare Two-Digit Numbers solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert compare(test_input1, test_input2) == expected
