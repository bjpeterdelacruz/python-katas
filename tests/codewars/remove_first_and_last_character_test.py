"""
Unit tests for Remove First and Last Character challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.remove_first_and_last_character import remove_char


@pytest.mark.parametrize("test_input, expected",
                         [("ab", ""), ("abcd", "bc")])
def test_remove_char(test_input, expected):
    """
    Tests the Remove First and Last Character solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove_char(test_input) == expected
