"""
Unit tests for Bubblesort Once challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.bubblesort_once import bubblesort_once


@pytest.mark.parametrize("test_input, expected",
                         [([], []), ([1, 2], [1, 2]),
                          ([2, 1], [1, 2]),
                          ([1, 3], [1, 3]),
                          ([3, 1], [1, 3]),
                          ([24, 57], [24, 57]),
                          ([89, 36], [36, 89]),
                          ([1, 2, 3], [1, 2, 3]),
                          ([2, 4, 1], [2, 1, 4]),
                          ([17, 5, 11], [5, 11, 17]),
                          ([25, 16, 9], [16, 9, 25]),
                          ([103, 87, 113], [87, 103, 113]),
                          ([1032, 3192, 2864], [1032, 2864, 3192]),
                          ([1, 2, 3, 4], [1, 2, 3, 4]),
                          ([2, 3, 4, 1], [2, 3, 1, 4]),
                          ([3, 4, 1, 2], [3, 1, 2, 4]),
                          ([4, 1, 2, 3], [1, 2, 3, 4]),
                          ([7, 5, 3, 1], [5, 3, 1, 7]),
                          ([5, 3, 7, 7], [3, 5, 7, 7]),
                          ([3, 1, 8, 5], [1, 3, 5, 8]),
                          ([1, 9, 5, 5], [1, 5, 5, 9]),
                          ([6, 3, 4, 9, 1, 2, 7, 8, 5], [3, 4, 6, 1, 2, 7, 8, 5, 9]),
                          ([6, 3, 4, 15, 14, 9, 1, 2, 7, 8, 5, 14, 11, 15, 17, 19],
                           [3, 4, 6, 14, 9, 1, 2, 7, 8, 5, 14, 11, 15, 15, 17, 19])])
def test_bubblesort_once(test_input, expected):
    """
    Tests the Bubblesort Once solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert bubblesort_once(test_input) == expected
