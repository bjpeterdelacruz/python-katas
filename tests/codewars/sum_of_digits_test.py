"""
Unit tests for Sum of Digits challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_of_digits import sum_of_digits


@pytest.mark.parametrize("test_input, expected",
                         [("3433", "3 + 4 + 3 + 3 = 13"),
                          ("64323", "6 + 4 + 3 + 2 + 3 = 18"),
                          ("8", "8 = 8"),
                          ("0001", "0 + 0 + 0 + 1 = 1"),
                          ("1001", "1 + 0 + 0 + 1 = 2"),
                          ("0", "0 = 0"),
                          ("9007199254740991",
                           "9 + 0 + 0 + 7 + 1 + 9 + 9 + 2 + 5 + 4 + 7 + 4 + 0 + 9 + 9 + 1 = 76"),
                          (3433, "3 + 4 + 3 + 3 = 13"),
                          (64323, "6 + 4 + 3 + 2 + 3 = 18"),
                          (8, "8 = 8"),
                          (0, "0 = 0"),
                          (9007199254740991,
                           "9 + 0 + 0 + 7 + 1 + 9 + 9 + 2 + 5 + 4 + 7 + 4 + 0 + 9 + 9 + 1 = 76"),
                          ("hello world", ""), (None, "")])
def test_sum_of_digits(test_input, expected):
    """
    Tests the Sum of Digits solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_of_digits(test_input) == expected
