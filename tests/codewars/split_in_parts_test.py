"""
Unit tests for Split in Parts challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.split_in_parts import split_in_parts


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("supercalifragilisticexpialidocious", 3,
                           "sup erc ali fra gil ist ice xpi ali doc iou s"),
                          ("HelloKata", 1, "H e l l o K a t a"),
                          ("HelloKata", 9, "HelloKata")])
def test_split_in_parts(test_input1, test_input2, expected):
    """
    Tests the Split in Parts solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert split_in_parts(test_input1, test_input2) == expected
