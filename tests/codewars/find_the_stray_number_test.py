"""
Unit tests for Find the Stray Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_the_stray_number import stray


@pytest.mark.parametrize("test_input, expected",
                         [([1, 1, 2, 1, 1], 2), ([3, 2, 2, 2, 2], 3), ([1, 1, 1, 1, 4], 4)])
def test_stray(test_input, expected):
    """
    Tests the Find the Stray Number solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert stray(test_input) == expected
