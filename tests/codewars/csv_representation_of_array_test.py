"""
Unit tests for CSV Representation of Array challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.csv_representation_of_array import to_csv_text


@pytest.mark.parametrize("test_input, expected",
                         [(
                                 [
                                     [0, 1, 2, 3, 45],
                                     [10, 11, 12, 13, 14],
                                     [20, 21, 22, 23, 24],
                                     [30, 31, 32, 33, 34],
                                 ],
                                 "0,1,2,3,45\n10,11,12,13,14\n20,21,22,23,24\n30,31,32,33,34",
                         ),
                             (
                                     [[-25, 21, 2, -33, 48], [30, 31, -32, 33, -34]],
                                     "-25,21,2,-33,48\n30,31,-32,33,-34",
                             ),
                             (
                                     [[5, 55, 5, 5, 55], [6, 6, 66, 23, 24], [666, 31, 66, 33, 7]],
                                     "5,55,5,5,55\n6,6,66,23,24\n666,31,66,33,7",
                             )
                         ])
def test_to_csv_text(test_input, expected):
    """
    Tests the CSV Representation of Array solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert to_csv_text(test_input) == expected
