"""
Unit tests for Condi Cipher challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.condi_cipher import encode, decode


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(" .gx.x nwovmxautggmney,  zjqq vxjtff e", "iyugqdaqkzoakwqrw", 1,
                           " .qg.x ndnzlmaoiykteoh,  wiso gvjfcd q"),
                          ("on", "cryptogram", 10, "jx"),
                          ("cryptogram", "cryptogram", 0, "cytgmdfmbk"),
                          ("on the first day i got lost.", "cryptogram", 10,
                           "jx wnz xrkvz jnd l ufd vwcz."),
                          ("i will never eat any grapes again", "superkey", 4,
                           "n ggka cvssb bfe esz omgdyr bqqva")])
def test_encode(test_input1, test_input2, test_input3, expected):
    """
    Tests the Condi Cipher solution (encode).

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert encode(test_input1, test_input2, test_input3) == expected


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [("jx", "cryptogram", 10, "on"), ("...,", "cryptogram", 10, "...,"),
                          ("abc", "keykeykeykey", 10, "sit"),
                          ("jx wnz xrkvz jnd l ufd vwcz.", "cryptogram", 10,
                           "on the first day i got lost."),
                          ("n ggka cvssb bfe esz omgdyr bqqva", "superkey", 30,
                           "i will never eat any grapes again"),
                          ("qvf cmnxmdkjfca.p,ab mf,byokf vjhwpcyb", "nqhbfgmi", 28,
                           "zva nguhbmmgydx.s,ok se,rmafz vpedgbua")])
def test_decode(test_input1, test_input2, test_input3, expected):
    """
    Tests the Condi Cipher solution (decode).

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert decode(test_input1, test_input2, test_input3) == expected
