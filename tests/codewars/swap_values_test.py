"""
Unit tests for Swap Values challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.swap_values import swap_values


def test_swap_values():
    """
    Tests the Swap Values solution.

    :return: None
    """
    arr = [0, 1]
    swap_values(arr)
    assert arr == [1, 0]
