"""
Unit tests for Shared Bit Counter challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.shared_bit_counter import shared_bits


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(1, 2, False), (16, 8, False), (1, 1, False), (2, 3, False),
                          (7, 10, False), (43, 77, True), (7, 15, True), (23, 7, True)])
def test_shared_bits(test_input1, test_input2, expected):
    """
    Tests the Rotate the Shared Bit Counter solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert shared_bits(test_input1, test_input2) == expected
