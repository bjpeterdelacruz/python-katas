"""
Unit tests for Back to Basics challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.back_to_basics import types


@pytest.mark.parametrize("test_input, expected",
                         [(10, "int"),
                          (9.7, "float"),
                          ("Hello World!", "str"),
                          ([1, 2, 3, 4], "list"),
                          (1023, "int"),
                          (True, "bool"),
                          ("True", "str"),
                          ({"name": "John", "age": 32}, "dict"),
                          (None, "NoneType"),
                          (3.141, "float"),
                          (False, "bool"),
                          ({1, 2, 3}, "set"),
                          ("8.6", "str"),
                          (("test", "case"), "tuple"),
                          ("*&^", "str"),
                          (4.5, "float")])
def test_types(test_input, expected):
    """
    Tests the Back to Basics solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert types(test_input) == expected
