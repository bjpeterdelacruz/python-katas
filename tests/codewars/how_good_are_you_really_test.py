"""
Unit tests for How Good Are You Really? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.how_good_are_you_really import better_than_average


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([2, 3], 5, True),
                          ([100, 40, 34, 57, 29, 72, 57, 88], 75, True),
                          ([12, 23, 34, 45, 56, 67, 78, 89, 90], 69, True)])
def test_better_than_average(test_input1, test_input2, expected):
    """
    Tests the How Good Are You Really? solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert better_than_average(test_input1, test_input2) == expected
