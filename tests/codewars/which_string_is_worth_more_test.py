"""
Unit tests for Which String is Worth More challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.which_string_is_worth_more import highest_value


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("AaBbCcXxYyZz0189", "KkLlMmNnOoPp4567", "KkLlMmNnOoPp4567"),
                          ("ABcd", "0123", "ABcd"),
                          ("!\"?$%^&*()", "{}[]@~'#:;", "{}[]@~'#:;"),
                          ("ABCD", "DCBA", "ABCD")])
def test_highest_value(test_input1, test_input2, expected):
    """
    Tests the Which String is Worth More solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert highest_value(test_input1, test_input2) == expected
