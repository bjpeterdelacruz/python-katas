"""
Unit tests for Exclamation Marks Series #1 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_1_remove_an_exclamation_mark_from_the_end_of_string import remove  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [('Hi!', 'Hi'), ('Hi!!!', 'Hi!!'), ('!Hi', '!Hi'), ('!Hi!', '!Hi'),
                          ('Hi! Hi!', 'Hi! Hi'), ('Hi', 'Hi')])
def test_remove(test_input, expected):
    """
    Tests the Exclamation Marks Series #1 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove(test_input) == expected
