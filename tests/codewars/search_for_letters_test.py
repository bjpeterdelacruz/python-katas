"""
Unit tests for Search for Letters challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.search_for_letters import change


@pytest.mark.parametrize("test_input, expected",
                         [("a **&  bZ", "11000000000000000000000001"),
                          ('Abc e  $$  z', "11101000000000000000000001"),
                          ("!!a$%&RgTT", "10000010000000000101000000"),
                          ("", "00000000000000000000000000"),
                          ("abcdefghijklmnopqrstuvwxyz", "11111111111111111111111111"),
                          ("aaaaaaaaaaa", "10000000000000000000000000"),
                          ("&%&%/$%$%$%$%GYtf67fg34678hgfdyd", "00010111000000000001000010")])
def test_change(test_input, expected):
    """
    Tests the Search for Letters solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert change(test_input) == expected
