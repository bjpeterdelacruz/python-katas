"""
Unit tests for Abbreviate Two Word Name challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.abbreviate_a_two_word_name import abbrev_name


@pytest.mark.parametrize("test_input, expected",
                         [("John Doe", "J.D"), ("Matcha Udon", "M.U")])
def test_abbrev_name(test_input, expected):
    """
    Tests the Abbreviate Two Word Name solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert abbrev_name(test_input) == expected
