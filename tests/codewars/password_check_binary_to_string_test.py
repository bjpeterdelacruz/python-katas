"""
Unit tests for Password Check challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.password_check_binary_to_string import decode_pass


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(['password123', 'admin', 'admin1'],
                           '01110000 01100001 01110011 01110011 01110111 01101111 01110010 '
                           '01100100 00110001 00110010 00110011',
                           'password123'),
                          (['password321', 'admin', 'admin1'],
                           '01110000 01100001 01110011 01110011 01110111 01101111 01110010 '
                           '01100100 00110001 00110010 00110011',
                           False),
                          (['password456', 'pass1', 'test12'],
                           '01110000 01100001 01110011 01110011 01110111 01101111 01110010 '
                           '01100100 00110001 00110010 00110011',
                           False),
                          (['password123', 'admin', 'admin1'],
                           '01110000 01100001 01110011 01110011 01110111 01101111 01110010 '
                           '01100100 00110001 00110010 00110011',
                           'password123'),
                          (['password', 'admin', 'admin1'],
                           '01110000 01100001 01110011 01110011 01110111 01101111 01110010 '
                           '01100100 00110001 00110010 00110011',
                           False),
                          (['password', 'pass', 'test'],
                           '01110000 01100001 01110011 01110011 01110111 01101111 01110010 '
                           '01100100 00110001 00110010 00110011',
                           False),
                          (['password123', 'admin123'],
                           '01100001 01100100 01101101 01101001 01101110 00110001 00110010 '
                           '00110011',
                           'admin123'),
                          (['password123', 'admin1', 'login'],
                           '01100001 01100100 01101101 01101001 01101110 00110001 00110010 '
                           '00110011',
                           False),
                          (['password123', 'admin321'],
                           '01100001 01100100 01101101 01101001 01101110 00110001 00110010 '
                           '00110011',
                           False),
                          (['password123', 'admin123', 'a'], '01100001', 'a'),
                          (['password123', 'admin123', 'b'], '01100001', False),
                          (['password123', 'admin123', 'c'], '01100001', False)])
def test_decode_pass(test_input1, test_input2, expected):
    """
    Tests the Password Check solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert decode_pass(test_input1, test_input2) == expected
