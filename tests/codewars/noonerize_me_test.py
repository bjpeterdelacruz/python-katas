"""
Unit tests for Noonerize Me challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.noonerize_me import noonerize


@pytest.mark.parametrize("test_input, expected",
                         [([12, 34], 18),
                          ([55, 63], 12),
                          ([357, 579], 178),
                          ([1000000, 9999999], 7000001),
                          ([1000000, "hello"], "invalid array"),
                          (["pippi", 9999999], "invalid array"),
                          (["pippi", "hello"], "invalid array"),
                          ([1, 1], 0),
                          ([1, 0], 1),
                          ([0, 1], 1),
                          ([True, 1], "invalid array"),
                          ([1, False], "invalid array")])
def test_numbers_of_letters(test_input, expected):
    """
    Tests the Noonerize Me solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert noonerize(test_input) == expected
