"""
Unit tests for Every Nth Array Element (Basic) challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.every_nth_array_element_basic import every


@pytest.mark.parametrize("test_input, expected",
                         [([0, 1, 2, 3, 4], [0, 1, 2, 3, 4]),
                          ([0, 1, 2, 3, 4], [0, 1, 2, 3, 4]),
                          (['t', 'e', 's', 't'], ['t', 'e', 's', 't'])])
def test_every_1(test_input, expected):
    """
    Tests the Every Nth Array Element (Basic) solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert every(test_input) == expected


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([0, 1, 2, 3, 4], 1, [0, 1, 2, 3, 4]),
                          ([0, 1, 2, 3, 4], 2, [0, 2, 4]),
                          ([0, 1, 2, 3, 4], 3, [0, 3]),
                          ([0, 1, 2, 3, 4], 4, [0, 4]),
                          ([0, 1, 2, 3, 4], 5, [0]),
                          ([0, 1, 2, 3, 4], 1, [0, 1, 2, 3, 4]),
                          ([0, 1, 2, 3, 4], 2, [0, 2, 4]),
                          ([0, 1, 2, 3, 4], 3, [0, 3]),
                          (['t', 'e', 's', 't'], 2, ['t', 's']),
                          ([None, 1, ['two'], 'three', {4: 'IV'}], 1,
                           [None, 1, ['two'], 'three', {4: 'IV'}]),
                          ([None] * 5, 2, [None] * 3)])
def test_every_2(test_input1, test_input2, expected):
    """
    Tests the Every Nth Array Element (Basic) solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert every(test_input1, test_input2) == expected


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [([0, 1, 2, 3, 4], 1, 1, [1, 2, 3, 4]),
                          ([0, 1, 2, 3, 4], 2, 1, [1, 3]),
                          ([0, 1, 2, 3, 4], 3, 1, [1, 4]),
                          ([0, 1, 2, 3, 4], 4, 1, [1]),
                          ([0, 1, 2, 3, 4], 5, 1, [1]),
                          ([0, 1, 2, 3, 4], 1, 3, [3, 4]),
                          ([0, 1, 2, 3, 4], 3, 1, [1, 4]),
                          ([0, 1, 2, 3, 4], 3, 4, [4]),
                          (['t', 'e', 's', 't'], 2, 1, ['e', 't']),
                          ([None, 1, ['two'], 'three', {4: 'IV'}], 2, 2, [['two'], {4: 'IV'}])])
def test_every_3(test_input1, test_input2, test_input3, expected):
    """
    Tests the Every Nth Array Element (Basic) solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert every(test_input1, test_input2, test_input3) == expected
