"""
Unit tests for Thinkful: Repeater Level 2 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_string_drills_repeater_level_2 import repeater


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('Hello', 5, '"Hello" repeated 5 times is: "HelloHelloHelloHelloHello"'),
                          ('Bye', 0, '"Bye" repeated 0 times is: ""')])
def test_repeater(test_input1, test_input2, expected):
    """
    Tests the Thinkful: Repeater Level 2 solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert repeater(test_input1, test_input2) == expected
