"""
Unit tests for Uncollapse Digits challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.uncollapse_digits import uncollapse


@pytest.mark.parametrize("test_input, expected",
                         [("", ""), ("three", "three"), ("eightsix", "eight six"),
                          ("fivefourseven", "five four seven"),
                          ("ninethreesixthree", "nine three six three"),
                          ("fivethreefivesixthreenineonesevenoneeight",
                           "five three five six three nine one seven one eight")])
def test_uncollapse(test_input, expected):
    """
    Tests the Uncollapse Digits solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert uncollapse(test_input) == expected
