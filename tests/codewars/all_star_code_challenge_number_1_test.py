"""
Unit tests for All Star Code Challenge #1 challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.all_star_code_challenge_number_1 import sum_ppg


def test_sum_ppg():
    """
    Tests the All Star Code Challenge #1 solution.

    :return: None
    """
    player1 = {'team': 'p1_team', 'ppg': 20.2}
    player2 = {'team': 'p2_team', 'ppg': 2.6}
    player3 = {'team': 'p3_team', 'ppg': 2023.2}
    player4 = {'team': 'p4_team', 'ppg': 0}
    player5 = {'team': 'p5_team', 'ppg': -5.8}
    assert sum_ppg(player1, player2) == 22.8
    assert sum_ppg(player3, player1) == 2043.4
    assert sum_ppg(player3, player4) == 2023.2
    assert sum_ppg(player4, player5) == -5.8
    assert sum_ppg(player5, player2) == 2.6 - 5.8
