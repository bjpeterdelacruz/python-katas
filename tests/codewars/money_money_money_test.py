"""
Unit tests for Money Money Money challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.money_money_money import calculate_years


@pytest.mark.parametrize("test_input1, test_input2, test_input3, test_input4, expected",
                         [(1000, 0.05, 0.18, 1100, 3),
                          (1000, 0.01625, 0.18, 1200, 14),
                          (1000, 0.05, 0.18, 1000, 0)])
def test_calculate_years(test_input1, test_input2, test_input3, test_input4, expected):
    """
    Tests the Money Money Money solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param test_input4: Test input
    :param expected: Expected result
    :return: None
    """
    assert calculate_years(test_input1, test_input2, test_input3, test_input4) == expected
