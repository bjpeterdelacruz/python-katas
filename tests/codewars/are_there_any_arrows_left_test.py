"""
Unit tests for Are There Any Arrows Left? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.are_there_any_arrows_left import any_arrows


@pytest.mark.parametrize("test_input, expected",
                         [([], False),
                          ([{'range': 5, 'damaged': False}], True),
                          ([{'range': 5, 'damaged': False}, {'range': 15, 'damaged': True}], True),
                          ([{'range': 5}, {'range': 10, 'damaged': True}, {'damaged': True}], True),
                          ([{'range': 10, 'damaged': True}, {'damaged': True}], False)])
def test_any_arrows(test_input, expected):
    """
    Tests the "Are There Any Arrows Left?" solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert any_arrows(test_input) == expected
