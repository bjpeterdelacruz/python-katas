"""
Unit tests for Mobile Device Keystrokes challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.mobile_display_keystrokes import mobile_keyboard


@pytest.mark.parametrize("test_input, expected",
                         [("", 0),
                          ("123", 3),
                          ("codewars", 26),
                          ("zruf", 16),
                          ("thisisasms", 37),
                          ("longwordwhichdontreallymakessense", 107)])
def test_mobile_keyboard(test_input, expected):
    """
    Tests the Mobile Device Keystrokes solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert mobile_keyboard(test_input) == expected
