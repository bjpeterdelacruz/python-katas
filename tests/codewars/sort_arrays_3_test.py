"""
Unit tests for Sort Arrays - 3 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sort_arrays_3 import sort_me


@pytest.mark.parametrize("test_input, expected",
                         [(['aeb-1305', 'site-1305', 'play-1215', 'web-1304', 'site-1304',
                            'beb-1305'],
                           ["play-1215", "site-1304", "web-1304", "aeb-1305", "beb-1305",
                            "site-1305"])])
def test_sort_me(test_input, expected):
    """
    Tests the Sort Arrays - 3 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sort_me(test_input) == expected
