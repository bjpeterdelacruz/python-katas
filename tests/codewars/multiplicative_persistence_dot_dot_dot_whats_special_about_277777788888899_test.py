"""
Unit tests for Multiplicative Persistence... What's special about 277777788888899? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.multiplicative_persistence_dot_dot_dot_whats_special_about_277777788888899 import per


@pytest.mark.parametrize("test_input, expected",
                         [(1234567890, [0]),
                          (123456789, [362880, 0]),
                          (12345678, [40320, 0]),
                          (1234567, [5040, 0]),
                          (123456, [720, 0]),
                          (12345, [120, 0]),
                          (2379, [378, 168, 48, 32, 6]),
                          (777, [343, 36, 18, 8]),
                          (25, [10, 0]),
                          (277777788888899,
                           [4996238671872, 438939648, 4478976, 338688, 27648, 2688, 768, 336, 54,
                            20, 0]),
                          (3778888999,
                           [438939648, 4478976, 338688, 27648, 2688, 768, 336, 54, 20, 0])])
def test_per(test_input, expected):
    """
    Tests the Multiplicative Persistence... What's special about 277777788888899? solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert per(test_input) == expected
