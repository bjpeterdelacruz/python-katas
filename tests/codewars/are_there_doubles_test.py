"""
Unit tests for Are There Doubles? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.are_there_doubles import double_check


@pytest.mark.parametrize("test_input, expected",
                         [("abca", False),
                          ("aabc", True),
                          ("a 11 c d", True),
                          ("AabBcC", True),
                          ("a b  c", True),
                          ("a b c d e f g h i h k", False),
                          ("2020", False),
                          ("a!@€£#$%^&*()_-+=}]{[|\':;?/>.<,~", False)])
def test_double_check(test_input, expected):
    """
    Tests the "Are There Doubles?" solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert double_check(test_input) == expected
