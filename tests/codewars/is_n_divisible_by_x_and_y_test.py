"""
Unit tests for Is N Divisible by X and Y? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.is_n_divisible_by_x_and_y import is_divisible


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(3, 2, 2, False),
                          (3, 3, 4, False),
                          (12, 3, 4, True),
                          (8, 3, 4, False),
                          (48, 3, 4, True),
                          (100, 5, 10, True),
                          (100, 5, 3, False),
                          (4, 4, 2, True),
                          (5, 2, 3, False),
                          (17, 17, 17, True),
                          (17, 1, 17, True)])
def test_is_divisible(test_input1, test_input2, test_input3, expected):
    """
    Tests the Is N Divisible by X and Y? solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_divisible(test_input1, test_input2, test_input3) == expected
