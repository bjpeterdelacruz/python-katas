"""
Unit tests for Thinkful: Order Filler challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_dictionary_drills_order_filler import fillable

stock = {
    'football': 4,
    'boardgame': 10,
    'leggos': 1,
    'doll': 5}


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('football', 3, True), ('leggos', 2, False), ('action figure', 1, False)])
def test_fillable(test_input1, test_input2, expected):
    """
    Tests the Thinkful: Order Filler solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert fillable(stock, test_input1, test_input2) == expected
