"""
Unit tests for Refactored Greeting challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.refactored_greeting import Person


def test_person():
    """
    Tests the Refactored Greeting solution.

    :return: None
    """
    person = Person("Jack")
    assert person.name == "Jack"
    assert person.greet("Jill") == "Hello Jill, my name is Jack"
