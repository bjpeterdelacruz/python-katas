"""
Unit tests for 1 Two 3 Four 5! challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.one_two_3_four_5 import conv


@pytest.mark.parametrize("test_input, expected",
                         [(0, "0"),
                          (11, "11"),
                          (1101, "11zer1"),
                          (54563, "F4FIV6THREE"),
                          (47309534, "f73zero953fourFOUR"),
                          (34266262106, "T4266262ONEoneONE06"),
                          (15795379351687, "15795379351sixSIXsixSIXeightEIGHTeig7"),
                          (157953793516872,
                           "OFISEVNINEFIVEfTHREEtSEVENseNINEnineTHREEthreFIVEfiveFIONEoneONEon68"
                           "SEVENsevenSEVE2")])
def test_conv(test_input, expected):
    """
    Tests the 1 Two 3 Four 5! solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert conv(test_input) == expected
