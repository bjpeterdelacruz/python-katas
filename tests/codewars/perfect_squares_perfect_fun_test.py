"""
Unit tests for Perfect Squares, Perfect Fun challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.perfect_squares_perfect_fun import square_it


@pytest.mark.parametrize("test_input, expected",
                         [(7615208105, "Not a perfect square!"),
                          (9857127238612311683071154470670308213, "Not a perfect square!"),
                          (43748, "Not a perfect square!"),
                          (29053666486889391130122715422426213289497497375510,
                           "Not a perfect square!"),
                          (2884528083, "Not a perfect square!"),
                          (5265268855398235, "5265\n2688\n5539\n8235"),
                          (4724560952868207213259492, "47245\n60952\n86820\n72132\n59492"),
                          (870513600, "870\n513\n600"),
                          (4588, "45\n88")])
def test_square_it(test_input, expected):
    """
    Tests the Perfect Squares, Perfect Fun solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert square_it(test_input) == expected
