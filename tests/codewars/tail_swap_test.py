"""
Unit tests for Tail Swap challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.tail_swap import tail_swap


@pytest.mark.parametrize("test_input, expected",
                         [(["abc:123", "xyz:456"], ["abc:456", "xyz:123"]),
                          (["a:12345", "bcdef:6"], ["a:6", "bcdef:12345"])])
def test_tail_swap(test_input, expected):
    """
    Tests the Tail Swap solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert tail_swap(test_input) == expected
