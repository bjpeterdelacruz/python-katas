"""
Unit tests for RGB To Hex Conversion challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.rgb_to_hex_conversion import rgb


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(-1, -1, -1, "000000"), (256, 256, 256, "FFFFFF"),
                          (10, 10, 10, "0A0A0A")])
def test_rgb(test_input1, test_input2, test_input3, expected):
    """
    Tests the RGB To Hex Conversion solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert rgb(test_input1, test_input2, test_input3) == expected
