"""
Unit tests for Word a10n (abbreviation) challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.word_a10n_abbreviation import abbreviate


@pytest.mark.parametrize("test_input, expected",
                         [("elephant-rides are really fun!", "e6t-r3s are r4y fun!"),
                          ("internationalization", "i18n"),
                          ("elephant-rides are&&really fun!", "e6t-r3s are&&r4y fun!"),
                          ("!elephant-rides are really fun!", "!e6t-r3s are r4y fun!")])
def test_abbreviate(test_input, expected):
    """
    Tests the Word a10n (abbreviation) solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert abbreviate(test_input) == expected
