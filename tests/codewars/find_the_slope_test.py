"""
Unit tests for Find the Slope challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_the_slope import find_slope


@pytest.mark.parametrize("test_input, expected",
                         [([1, 24, 2, 88], "64"),
                          ([4, 384, 8, 768], "96"),
                          ([4, 16, 4, 18], "undefined"),
                          ([7, 28, 9, 64], "18"),
                          ([18, -36, 12, 36], "-12"),
                          ([36, 580, 42, 40], "-90"),
                          ([1, 2, 2, 6], "4"),
                          ([-6, 57, -6, 84], "undefined"),
                          ([92, 12, 96, 64], "13"),
                          ([1, 2, 2, 6], "4"),
                          ([90, 54, 90, 2], "undefined"),
                          ([3, 6, 4, 9], "3"),
                          ([-2, -5, 2, 3], "2"),
                          ([3, 3, 2, 0], "3")])
def test_find_slope(test_input, expected):
    """
    Tests the Find the Slope solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_slope(test_input) == expected
