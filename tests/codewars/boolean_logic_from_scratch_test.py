"""
Unit tests for Boolean Logic from Scratch challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.boolean_logic_from_scratch import func_or, func_xor


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(True, True, True),
                          (True, False, True),
                          (False, False, False),
                          (0, 11, True),
                          (None, [], False)])
def test_func_or(test_input1, test_input2, expected):
    """
    Tests the OR function of the Boolean Logic from Scratch solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert func_or(test_input1, test_input2) == expected


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(True, True, False),
                          (True, False, True),
                          (False, False, False),
                          (0, 11, True),
                          (None, [], False)])
def test_func_xor(test_input1, test_input2, expected):
    """
    Tests the XOR function of the Boolean Logic from Scratch solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert func_xor(test_input1, test_input2) == expected
