"""
Unit tests for Least Larger challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.least_larger import least_larger


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 3, 5, 7, 2, 2, 0], 0, 4), ([2, 4, 6, 6], 2, -1)])
def test_least_larger(test_input1, test_input2, expected):
    """
    Tests the Least Larger solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert least_larger(test_input1, test_input2) == expected
