"""
Unit tests for Fundamentals: Return challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.fundamentals_return import add, multiply, divide, mod, exponent, subt


def test_functions():
    """
    Tests all the functions for the Fundamentals: Return solution.

    :return: None
    """
    assert add(5, 0) == 5
    assert add(-1, 6) == 5
    assert multiply(5, 0) == 0
    assert multiply(-1, -6) == 6
    assert divide(10, 2) == 5
    assert divide(-5, -5) == 1
    assert mod(10, 3) == 1
    assert mod(100, 10) == 0
    assert exponent(2, 3) == 8
    assert exponent(2, -3) == 1/8
    assert subt(5, 3) == 2
    assert subt(-5, -7) == 2
