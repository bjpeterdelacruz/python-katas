"""
Unit tests for Beeramid challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.beeramid import beeramid


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(1500, 2, 12), (5000, 3, 16), (9, 2, 1), (21, 1.5, 3), (-1, 4, 0)])
def test_beeramid(test_input1, test_input2, expected):
    """
    Tests the Beeramid solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert beeramid(test_input1, test_input2) == expected
