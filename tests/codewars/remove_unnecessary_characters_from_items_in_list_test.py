"""
Unit tests for Remove Unnecessary Characters from Items in List challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.remove_unnecessary_characters_from_items_in_list import remove_char

# pylint: disable=implicit-str-concat


@pytest.mark.parametrize("test_input, expected",
                         [(['$5.$6.6x.s4', '{$33ae.5(9', '$29..4e9', '%.$9|4d20', 'A$AA$4r R4.94'],
                           ['$56.64', '$33.59', '$29.49', '$94.20', '$44.94']),
                          (['%$$$%$9p2. 42', '"e"$1..5o.4/d9', '$29.29', ',$,.59,.,25', 'E$5.0O0'],
                           ['$92.42', '$15.49', '$29.29', '$59.25', '$5.00']),
                          (['#...$0...00'"'", '$44.59', '{}$$$$.$$...92))().f8f3', '$41.a11',
                            '$about3.50'], ['$0.00', '$44.59', '$92.83', '$41.11', '$3.50']),
                          (['###77###'], ['$0.77'])])
def test_remove_char(test_input, expected):
    """
    Tests the Remove Unnecessary Characters from Items in List solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove_char(test_input) == expected
