"""
Unit tests for Double Trouble challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.double_trouble import trouble


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 3, 5, 6, 7, 4, 3], 7, [1, 3, 5, 6, 7, 4]),
                          ([4, 1, 1, 1, 4], 2, [4, 1, 4]), ([2, 6, 2], 8, [2, 2]),
                          ([2, 2, 2, 2, 2, 2], 4, [2])])
def test_trouble(test_input1, test_input2, expected):
    """
    Tests the Double Trouble solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert trouble(test_input1, test_input2) == expected
