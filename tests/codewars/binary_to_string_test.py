"""
Unit tests for Binary to String challenge

:Author: BJ Peter Dela Cruz
"""
from string import ascii_letters as letters
from codewars.binary_to_string import binary_to_string


def test_binary_to_string():
    """
    Tests the Binary to String solution.

    :return: None
    """
    characters = letters + "0123456789!@#$%^&*()?<>;:'\\|}{[]_-=+ "
    binary_string = ''.join([bin(ord(char)) for char in characters])
    assert binary_to_string(binary_string) == characters
