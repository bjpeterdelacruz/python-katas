"""
Unit tests for Safen User Input: Part 1 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.safen_user_input_part_i_htmlspecialchars import html_special_chars


@pytest.mark.parametrize("test_input, expected",
                         [("<h2>Hello World</h2>", "&lt;h2&gt;Hello World&lt;/h2&gt;"),
                          ("Hello, how would you & I fare?",
                           "Hello, how would you &amp; I fare?"),
                          ('How was "The Matrix"?  Did you like it?',
                           'How was &quot;The Matrix&quot;?  Did you like it?'),
                          ("<script>alert('Website Hacked!');</script>",
                           "&lt;script&gt;alert('Website Hacked!');&lt;/script&gt;")])
def test_html_special_chars(test_input, expected):
    """
    Tests the Safen User Input: Part 1 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert html_special_chars(test_input) == expected
