"""
Unit tests for Thinking and Testing: How Many "Word" challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinking_and_testing_how_many_word import how_many_word


@pytest.mark.parametrize("test_input, expected",
                         [("word", 1), ("hello world", 1), ("I love codewars.", 0),
                          ("My cat waiting for a dog.", 1),
                          ("We often read book, a word hidden in the book.", 2),
                          (
                          "When you in order to do something by a wrong way, "
                          "your heart will missed somewhere.",
                          2),
                          ("This sentence has one word.", 1),
                          ("This sentence has two words, not one word.", 2),
                          ("One word + one word = three words -)", 3),
                          ("Can you find more word for me?", 1)])
def test_how_many_word(test_input, expected):
    """
    Tests the Thinking and Testing: How Many "Word" solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert how_many_word(test_input) == expected
