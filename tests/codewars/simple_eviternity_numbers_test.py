"""
Unit tests for Simple Eviternity Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_eviternity_numbers import solve


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(0, 100, 4),
                          (0, 1000, 14),
                          (0, 10000, 37),
                          (0, 100000, 103),
                          (0, 500000, 148),
                          (90, 139701, 99),
                          (61, 56976, 53)])
def test_solve(test_input1, test_input2, expected):
    """
    Tests the Simple Eviternity Numbers solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert solve(test_input1, test_input2) == expected
