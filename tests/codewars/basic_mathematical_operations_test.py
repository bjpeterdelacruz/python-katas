"""
Unit tests for Basic Mathematical Operations challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.basic_mathematical_operations import basic_op


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [('+', 4, 7, 11),
                          ('-', 15, 18, -3),
                          ('*', 5, 5, 25),
                          ('/', 49, 7, 7)])
def test_basic_op(test_input1, test_input2, test_input3, expected):
    """
    Tests the Basic Mathematical Operations solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert basic_op(test_input1, test_input2, test_input3) == expected


def test_basic_op_invalid_input_1():
    """
    Tests the Basic Mathematical Operations solution with invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        basic_op('a', 5, 6)
        assert False


def test_basic_op_invalid_input_2():
    """
    Tests the Basic Mathematical Operations solution with invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        basic_op('/', 0, 0)
        assert False
