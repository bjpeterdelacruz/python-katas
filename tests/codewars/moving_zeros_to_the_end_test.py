"""
Unit tests for Move Zeros challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.moving_zeros_to_the_end import move_zeros


@pytest.mark.parametrize("test_input, expected",
                         [([False, [], '0', 1, 0.0, 0, 1, 2, 0, 1, 3, "a"],
                           [False, [], '0', 1, 1, 2, 1, 3, "a", 0, 0, 0])])
def test_move_zeros(test_input, expected):
    """
    Tests the Move Zeros solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert move_zeros(test_input) == expected
