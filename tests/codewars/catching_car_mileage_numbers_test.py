"""
Unit tests for Catching Car Mileage Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.catching_car_mileage_numbers import is_interesting


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(1, [], 0), (30, [], 0), (97, [], 0), (98, [], 1), (99, [], 1),
                          (100, [], 2), (7775, [], 1), (7776, [], 1), (7777, [], 2), (7778, [], 0),
                          (2345, [], 2), (2344, [], 1), (2343, [], 1), (7890, [], 2),
                          (7889, [], 1), (7888, [], 1), (112211, [], 2), (1337, [1337], 2),
                          (3100, [], 0), (3100, [3100], 2), (43210, [], 2), (43209, [], 1),
                          (43209, [43209], 2), (43208, [], 1), (50000, [], 2)])
def test_is_interesting(test_input1, test_input2, expected):
    """
    Tests the Catching Car Mileage Numbers solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_interesting(test_input1, test_input2) == expected
