"""
Unit tests for Ultimate Array Reverser challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.ultimate_array_reverser import reverse


@pytest.mark.parametrize("test_input, expected",
                         [(["I", "like", "big", "butts", "and", "I", "cannot", "lie!"],
                           ["!", "eilt", "onn", "acIdn", "ast", "t", "ubgibe", "kilI"]),
                          (
                                  ["?kn", "ipnr", "utotst", "ra", "tsn", "iksr", "uo", "yer",
                                   "ofebta", "eote", "vahu", "oyodpm",
                                   "ir", "hsyn", "amwoH"],
                                  ["How", "many", "shrimp", "do", "you", "have", "to", "eat",
                                   "before", "your",
                                   "skin", "starts", "to", "turn", "pink?"])])
def test_reverse(test_input, expected):
    """
    Tests the Ultimate Array Reverser solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert reverse(test_input) == expected
