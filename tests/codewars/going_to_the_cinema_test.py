"""
Unit tests for Going to the Cinema challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.going_to_the_cinema import movie


@pytest.mark.parametrize("card, ticket, discount, expected_answer", [
    (500, 15, 0.9, 43),
    (100, 10, 0.95, 24),
    (0, 10, 0.95, 2),
    (250, 20, 0.9, 21),
    (500, 20, 0.9, 34),
    (2500, 20, 0.9, 135)
])
def test_movie(card, ticket, discount, expected_answer):
    """
    Tests the Going to the Cinema solution.

    :param card: The price for a membership card
    :param ticket: The price for a ticket
    :param discount: The discount for the ticket
    :param expected_answer: The expected answer given the other three parameters
    :return: None
    """
    assert movie(card, ticket, discount) == expected_answer
