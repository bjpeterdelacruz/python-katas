"""
Unit tests for Roman Numerals Helper challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.roman_numerals_helper import RomanNumerals


@pytest.mark.parametrize("test_input, expected",
                         [(3923, "MMMCMXXIII"), (1990, "MCMXC"), (349, "CCCXLIX"),
                          (1666, "MDCLXVI")])
def test_to_roman(test_input, expected):
    """
    Tests the Roman Numerals Helper solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert RomanNumerals.to_roman(test_input) == expected


@pytest.mark.parametrize("test_input, expected",
                         [("MMMCMXXIII", 3923), ("MCMXC", 1990), ("CCCXLIX", 349),
                          ("MDCLXVI", 1666)])
def test_from_roman(test_input, expected):
    """
    Tests the Roman Numerals Helper solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert RomanNumerals.from_roman(test_input) == expected
