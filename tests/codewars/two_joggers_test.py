"""
Unit tests for Two Joggers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.two_joggers import nbr_of_laps


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(5, 3, (3, 5)), (4, 6, (3, 2)), (5, 5, (1, 1))])
def test_nbr_of_laps(test_input1, test_input2, expected):
    """
    Tests the Two Joggers solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert nbr_of_laps(test_input1, test_input2) == expected
