"""
Unit tests for They're Good Dogs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.theyre_good_dogs import we_rate_dogs


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('This is Tucker. He would like a hug. 3/10 someone hug him', 11,
                           'This is Tucker. He would like a hug. 11/10 someone hug him'),
                          ('This is Charlie. He pouts until he gets to go on the swing. 5/10 '
                           'manipulative af.', 12,
                           'This is Charlie. He pouts until he gets to go on the swing. 12/10 '
                           'manipulative af.')])
def test_we_rate_dogs(test_input1, test_input2, expected):
    """
    Tests the They're Good Dogs solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert we_rate_dogs(test_input1, test_input2) == expected
