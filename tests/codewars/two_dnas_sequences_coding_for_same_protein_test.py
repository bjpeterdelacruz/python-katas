"""
Unit tests for 2 DNAs Sequences: Coding for Same Protein? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.two_dnas_sequences_coding_for_same_protein import code_for_same_protein


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("ATGTCGTCAATTTAA", "ATGTCGTCAATTTAA", True),
                          ("ATGTTTTAA", "ATGTTCTAA", True),
                          ("ATGTTTTAA", "ATGATATAA", False),
                          ("ATGTTTTAA", "ATGATATAA", False),
                          ("ATGTTTGGGAATAATTAAGGGTAA", "ATGTTCGGGAATAATGGGAGGTAA", False),
                          ("ATGTTTTAA", "ATGTTC", False)])
def test_code_for_same_protein(test_input1, test_input2, expected):
    """
    Tests the 2 DNAs Sequences: Coding for Same Protein? solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert code_for_same_protein(test_input1, test_input2) == expected
