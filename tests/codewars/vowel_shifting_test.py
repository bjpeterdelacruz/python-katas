"""
Unit tests for Vowel Shifting challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.vowel_shifting import vowel_shift


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(None, 0, None),
                          ("", 0, ""),
                          ("This is a test!", 0, "This is a test!"),
                          ("This is a test!", 1, "Thes is i tast!"),
                          ("This is a test!", 3, "This as e tist!"),
                          ("This is a test!", 4, "This is a test!"),
                          ("This is a test!", -1, "This as e tist!"),
                          ("This is a test!", -5, "This as e tist!"),
                          ("Brrrr", 99, "Brrrr"),
                          ("AEIOUaeiou", 1, "uAEIOUaeio")])
def test_vowel_shift(test_input1, test_input2, expected):
    """
    Tests the Vowel Shifting solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert vowel_shift(test_input1, test_input2) == expected
