"""
Unit tests for Apparently Modifying Strings challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.apparently_modifying_strings import apparently


@pytest.mark.parametrize("test_input, expected",
                         [("and", "and apparently"), ("and apparently", "and apparently"),
                          ("bbut aand", "bbut aand"),
                          ("You and I but me", "You and apparently I but apparently me"),
                          ("and and but apparently but",
                           "and apparently and apparently but apparently but apparently")])
def test_apparently(test_input, expected):
    """
    Tests the Apparently Modifying Strings solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert apparently(test_input) == expected
