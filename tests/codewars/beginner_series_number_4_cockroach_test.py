"""
Unit tests for Beginner Series #4: Cockroach challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.beginner_series_number_4_cockroach import cockroach_speed


@pytest.mark.parametrize("test_input, expected",
                         [(1.08, 30), (1.09, 30), (0, 0)])
def test_cockroach_speed(test_input, expected):
    """
    Tests the Beginner Series #4: Cockroach solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert cockroach_speed(test_input) == expected
