"""
Unit tests for Adding Remainders to a List challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.adding_remainders_to_a_list import solve


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([2, 7, 5, 9, 100, 34, 32, 0], 3, [4, 8, 7, 9, 101, 35, 34, 0]),
                          ([], 2, []),
                          ([1000, 999, 998, 997], 5, [1000, 1003, 1001, 999]),
                          ([0, 0, 0, 0], 5, [0, 0, 0, 0]),
                          ([4, 3, 2, 1], 5, [8, 6, 4, 2]),
                          ([33, 23, 45, 78, 65], 10, [36, 26, 50, 86, 70]),
                          (list(range(1, 99)), 11, [x + x % 11 for x in list(range(1, 99))]),
                          (list(range(1, 9999)), 3658,
                           [x + x % 3658 for x in list(range(1, 9999))])])
def test_solve(test_input1, test_input2, expected):
    """
    Tests the Adding Remainders to a List solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert solve(test_input1, test_input2) == expected
