"""
Unit tests for Arithmetic Sequence challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.arithmetic_sequence import nthterm


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(0, 2, 5, 10), (0, 5, 0, 0), (-100, 5, 3, -85), (-10, 4, 5, 10)])
def test_nthterm(test_input1, test_input2, test_input3, expected):
    """
    Tests the Arithmetic Sequence solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert nthterm(test_input1, test_input2, test_input3) == expected
