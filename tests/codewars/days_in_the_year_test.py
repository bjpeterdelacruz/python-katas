"""
Unit tests for Days in the Year challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.days_in_the_year import year_days


@pytest.mark.parametrize("test_input, expected",
                         [(0, '0 has 366 days'),
                          (-64, '-64 has 366 days'),
                          (2016, '2016 has 366 days'),
                          (1974, '1974 has 365 days'),
                          (-10, '-10 has 365 days'),
                          (777, '777 has 365 days'),
                          (1857, '1857 has 365 days'),
                          (2000, '2000 has 366 days'),
                          (-300, '-300 has 365 days'),
                          (-1, '-1 has 365 days')])
def test_year_days(test_input, expected):
    """
    Tests the Days in the Year solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert year_days(test_input) == expected
