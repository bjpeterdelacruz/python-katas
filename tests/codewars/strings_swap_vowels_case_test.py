"""
Unit tests for Swap Vowels Case challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.strings_swap_vowels_case import swap_vowel_case

test_inputs = ("C is alive!", "", " ", "a", "A.", "to", "The",
               "in", "to", "of", "was", "enough", "version", "notably",
               "42", "Unix", "Unix's", "Master", "simply", "ALGOL", "different",
               "instances", "portable")

expected_results = ("C Is AlIvE!", "", " ", "A", "a.", "tO", "ThE",
                    "In", "tO", "Of", "wAs", "EnOUgh", "vErsIOn", "nOtAbly",
                    "42", "unIx", "unIx's", "MAstEr", "sImply", "aLGoL", "dIffErEnt",
                    "InstAncEs", "pOrtAblE")


def test_swap_vowel_case():
    """
    Tests the Swap Vowels Case solution.

    :return: None
    """
    for test_input, expected in zip(test_inputs, expected_results):
        assert swap_vowel_case(test_input) == expected
