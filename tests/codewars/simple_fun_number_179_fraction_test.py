"""
Unit tests for Simple Fun #179: Fraction challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_fun_number_179_fraction import fraction


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(5, 10, 3), (100, 1000, 11), (3, 15, 6)])
def test_fraction(test_input1, test_input2, expected):
    """
    Tests the Simple Fun #179: Fraction solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert fraction(test_input1, test_input2) == expected
