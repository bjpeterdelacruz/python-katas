"""
Unit tests for Exponential-Golomb Decoder challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exponential_golomb_decoder import decoder


@pytest.mark.parametrize("test_input, expected",
                         [("01001100100", [1, 2, 3]),
                          ("00000101001000011111000101100110", [40, 30, 10, 5]),
                          ("111000000000110010000100000000011001000010000000001100100001111",
                           [0, 0, 0, 800, 800, 800, 0, 0, 0]),
                          ("0000001000000", [63]),
                          ("1", [0]),
                          ("0101010101011010010", [1, 0, 1, 0, 1, 0, 0, 1, 1]),
                          ("111111111111000000010000000",
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 127])])
def test_decoder(test_input, expected):
    """
    Tests the Exponential-Golomb Decoder solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert decoder(test_input) == expected
