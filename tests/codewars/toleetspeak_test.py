"""
Unit tests for ToLeetSpeak challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.toleetspeak import to_leet_speak


@pytest.mark.parametrize("test_input, expected",
                         [("LEET", "1337"),
                          ("CODEWARS", "(0D3W@R$"),
                          ("HELLO WORLD", "#3110 W0R1D"),
                          ("LOREM IPSUM DOLOR SIT AMET", "10R3M !P$UM D010R $!7 @M37"),
                          ("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
                           "7#3 QU!(K 8R0WN F0X JUMP$ 0V3R 7#3 1@2Y D06")])
def test_to_leet_speak(test_input, expected):
    """
    Tests the ToLeetSpeak solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert to_leet_speak(test_input) == expected
