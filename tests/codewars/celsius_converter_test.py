"""
Unit tests for Celsius Converter challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.celsius_converter import weather_info


@pytest.mark.parametrize("test_input, expected",
                         [(56, "13.333333333333334 is above freezing temperature"),
                          (23, "-5.0 is freezing temperature"),
                          (33, "0.5555555555555556 is above freezing temperature"),
                          (5, "-15.0 is freezing temperature"),
                          (0, "-17.77777777777778 is freezing temperature")])
def test_weather_info(test_input, expected):
    """
    Tests the Celsius Converter solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert weather_info(test_input) == expected
