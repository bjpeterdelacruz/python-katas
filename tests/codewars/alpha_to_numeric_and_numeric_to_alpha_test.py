"""
Unit tests for Alpha to Numeric and Numeric to Alpha challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.alpha_to_numeric_and_numeric_to_alpha import alpha_num_num_alpha


@pytest.mark.parametrize("test_input, expected",
                         [('a262bc', '1zb23'),
                          ('25abcd26', 'y1234z'),
                          ('18zyz14', 'r262526n'),
                          ('a1b2c3d4', '1a2b3c4d'),
                          ('5a8p17', 'e1h16q'),
                          ('w6aa4ct24m5', '23f11d320x13e'),
                          ('17dh', 'q48'),
                          ('25gj8sk6r17', 'y710h1911f18q'),
                          ('17dh', 'q48'),
                          ('25abcd26', 'y1234z'),
                          ('18zzz14', 'r262626n'),
                          ('y17kg5et11', '25q117e520k'),
                          ('abcdefghijklmnopqrstuvwxyz',
                           '1234567891011121314151617181920212223242526'),
                          ('1a2b3c4d5e6f7g8h9i10j11k12l13m14n15o16p17q18r19s20t21u22v23w24x25y26z',
                           'a1b2c3d4e5f6g7h8i9j10k11l12m13n14o15p16q17r18s19t20u21v22w23x24y25z26'),
                          ('h15q4pc6yw23nmx19y', '8o17d163f2523w141324s25'),
                          ('p16k11o25x7m6m20ct9', '16p11k15y24g13f13t320i'),
                          ('nv15u19d5fq8', '1422o21s4e617h'),
                          ('4n3fk22en17ekve', 'd14c611v514q511225'),
                          ('iwantamemewar', '92311420113513523118'),
                          ('7h15cc9s23l11k10sd5', 'g8o33i19w12k11j194e'),
                          ('hd2gd14gf2sg5lm8wfv9bb13', '84b74n76b197e1213h23622i22m'),
                          ('13', 'm'),
                          ('7k7k7sg3jvh16d', 'g11g11g197c10228p4'),
                          ('6h19r21a8b', 'f8s18u1h2'),
                          ('youaredonegoodforyou', '251521118541514571515461518251521')])
def test_alpha_num_num_alpha(test_input, expected):
    """
    Tests the Alpha to Numeric and Numeric to Alpha solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert alpha_num_num_alpha(test_input) == expected
