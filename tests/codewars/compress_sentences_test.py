"""
Unit tests for Compress Sentences challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.compress_sentences import compress


@pytest.mark.parametrize("test_input, expected",
                         [("The bumble bee", "012"),
                          ("SILLY LITTLE BOYS silly little boys", "012012"),
                          (
                          "Ask not what your COUNTRY can do for you ASK WHAT YOU CAN DO FOR YOUR "
                          "country",
                          "01234567802856734"),
                          ("The number 0 is such a strange number Strangely it has zero meaning",
                           "012345617891011")])
def test_compress(test_input, expected):
    """
    Tests the Compress Sentences solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert compress(test_input) == expected
