"""
Unit tests for Linked Lists: Length and Count challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.linked_lists_length_and_count import length, count
from utils.linked_list import Node, push, build_one_two_three


def test_length():
    """
    Assert that the correct length of a linked list is returned.

    :return: None
    """
    assert length(None) == 0
    assert length(Node(99)) == 1
    assert length(build_one_two_three()) == 3


def test_count():
    """
    Assert that the correct count is returned for the given data in a linked list.

    :return: None
    """
    assert count(build_one_two_three(), 1) == 1
    assert count(build_one_two_three(), 2) == 1
    assert count(build_one_two_three(), 3) == 1
    assert count(build_one_two_three(), 99) == 0
    assert count(None, 1) == 0

    assert count(push(push(push(push(push(None, 1), 1), 1), 2), 1), 1) == 4
