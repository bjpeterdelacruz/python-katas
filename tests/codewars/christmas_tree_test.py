"""
Unit tests for Christmas Tree challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.christmas_tree import christmas_tree


@pytest.mark.parametrize("test_input, expected",
                         [(0, ''), (1, '*'), (2, ' * \n***'), (3, '  *  \n *** \n*****'),
                          (4, '   *   \n  ***  \n ***** \n*******'),
                          (5, '    *    \n   ***   \n  *****  \n ******* \n*********'),
                          (6, '     *     \n    ***    \n   *****   \n  *******  '
                              '\n ********* \n***********'),
                          (7, '      *      \n     ***     \n    *****    \n   *******   '
                              '\n  *********  \n *********** \n*************'),
                          (8, '       *       \n      ***      \n     *****     \n    *******    '
                              '\n   *********   \n  ***********  \n ************* '
                              '\n***************'),
                          (9, '        *        \n       ***       \n      *****      '
                              '\n     *******     \n    *********    \n   ***********   '
                              '\n  *************  \n *************** \n*****************'),
                          (10, '         *         \n        ***        \n       *****       '
                               '\n      *******      \n     *********     \n    ***********    '
                               '\n   *************   \n  ***************  \n ***************** '
                               '\n*******************')])
def test_christmas_tree(test_input, expected):
    """
    Tests the Christmas Tree solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert christmas_tree(test_input) == expected
