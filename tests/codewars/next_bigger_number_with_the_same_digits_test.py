"""
Unit tests for Next Bigger Number with Same Digits challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.next_bigger_number_with_the_same_digits import next_bigger


@pytest.mark.parametrize("test_input, expected",
                         [(60682961796395, 60682961796539), (809047860, 809048067), (83559, 83595),
                          (532, -1)])
def test_next_bigger(test_input, expected):
    """
    Tests the Next Bigger Number with Same Digits solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert next_bigger(test_input) == expected
