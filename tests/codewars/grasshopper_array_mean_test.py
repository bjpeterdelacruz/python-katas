"""
Unit tests for Grasshopper: Array Mean challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.grasshopper_array_mean import find_average


@pytest.mark.parametrize("test_input, expected",
                         [([], 0), ([1, 2, 3], 2), ([5, 5, 4, 4, 2], 4)])
def test_find_average(test_input, expected):
    """
    Tests the Grasshopper: Array Mean solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_average(test_input) == expected
