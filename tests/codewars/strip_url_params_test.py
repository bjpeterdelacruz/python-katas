"""
Unit tests for Strip URL Params challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.strip_url_params import strip_url_params


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("www.codewars.com?a=1&b=2", None, "www.codewars.com?a=1&b=2"),
                          ("www.codewars.com?a=1&b=2&a=1&b=3", None, "www.codewars.com?a=1&b=2"),
                          ("www.codewars.com?a=1&b=2&a=1&b=3", ["b"], "www.codewars.com?a=1"),
                          ("www.codewars.com", ["b"], "www.codewars.com"),
                          ("www.codewars.com?a=1&b=2", ["a", "b"], "www.codewars.com")])
def test_strip_url_params(test_input1, test_input2, expected):
    """
    Tests the Strip URL Params solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    if test_input2:
        assert strip_url_params(test_input1, test_input2) == expected
    else:
        assert strip_url_params(test_input1) == expected
