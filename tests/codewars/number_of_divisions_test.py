"""
Unit tests for Number of Divisions challenge

:Author: BJ Peter Dela Cruz
"""
from math import log
from random import randint, randrange
from codewars.number_of_divisions import divisions


def test_divisions():
    """
    Tests the Number of Divisions solution.

    :return: None
    """
    tests = [(randint(10, 10 ** 100), randrange(2, 6)) for _ in range(1000)]
    for exponent, base in tests:
        assert divisions(exponent, base) == int(log(exponent, base))
