"""
Unit tests for Compute Cube as Sums challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.compute_cube_as_sums import find_summands


@pytest.mark.parametrize("test_input, expected",
                         [(1, [1]), (3, [7, 9, 11]), (4, [13, 15, 17, 19])])
def test_find_summands(test_input, expected):
    """
    Tests the Compute Cube as Sums solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_summands(test_input) == expected
