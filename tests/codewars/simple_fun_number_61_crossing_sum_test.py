"""
Unit tests for Crossing Sum challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_fun_number_61_crossing_sum import crossing_sum


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [([[1, 1, 1, 1], [2, 2, 2, 2], [3, 3, 3, 3]], 1, 3, 12),
                          ([[1, 1], [3, 3], [1, 1], [2, 2]], 3, 0, 9),
                          ([[100]], 0, 0, 100),
                          ([[1, 2, 3, 4, 5]], 0, 0, 15),
                          ([[1], [2], [3], [4], [5]], 0, 0, 15)])
def test_crossing_sum(test_input1, test_input2, test_input3, expected):
    """
    Tests the Crossing Sum solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert crossing_sum(test_input1, test_input2, test_input3) == expected
