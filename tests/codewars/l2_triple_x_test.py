"""
Unit tests for L2: Triple X challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.l2_triple_x import triple_x


@pytest.mark.parametrize("test_input, expected",
                         [("", False),
                          ("there's no XXX here", False),
                          ("abraxxxas", True),
                          ("xoxotrololololololoxxx", False),
                          ("soft kitty, warm kitty, xxxxx", True),
                          ("softx kitty, warm kitty, xxxxx", False),
                          ("Xabraxxxas", True),
                          ("xoXotrololololololoxxx", False),
                          ("softXxxx kitty, warm kitty, xxxxx", True),
                          ("softxXxxx kitty, warm kitty, xxxxx", False)])
def test_triple_x(test_input, expected):
    """
    Tests the L2: Triple X solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert triple_x(test_input) == expected
