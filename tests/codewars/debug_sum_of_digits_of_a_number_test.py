"""
Unit tests for Debug Sum of Digits of a Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.debug_sum_of_digits_of_a_number import get_sum_of_digits


@pytest.mark.parametrize("test_input, expected",
                         [(0, 0), (12, 3), (999, 27)])
def test_get_sum_of_digits(test_input, expected):
    """
    Tests the Debug Sum of Digits of a Number solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_sum_of_digits(test_input) == expected
