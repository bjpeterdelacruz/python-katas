"""
Unit tests for Simple Substitution Cipher Helper challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.simple_substitution_cipher_helper import Cipher


def test_cipher():
    """
    Tests the Simple Substitution Cipher Helper solution.

    :return: None
    """
    cipher = Cipher("abcdefghijklmnopqrstuvwxyz", "etaoinshrdlucmfwypvbgkjqxz")
    assert cipher.encode("abc_?_def") == "eta_?_oin"
    assert cipher.decode("eta_?_oin") == "abc_?_def"
    assert cipher.decode(cipher.encode("/**ae|i|ou**/")) == "/**ae|i|ou**/"
