"""
Unit tests for Can We Divide It? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.can_we_divide_it import is_divide_by


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(8, 2, 4, True),
                          (12, -3, 4, True),
                          (8, 3, 4, False),
                          (48, 2, -5, False),
                          (-100, -25, 10, True),
                          (10000, 5, -3, False),
                          (4, 4, 2, True),
                          (5, 2, 3, False),
                          (-96, 25, 17, False),
                          (33, 1, 33, True)])
def test_is_divide_by(test_input1, test_input2, test_input3, expected):
    """
    Tests the Can We Divide It? solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_divide_by(test_input1, test_input2, test_input3) == expected
