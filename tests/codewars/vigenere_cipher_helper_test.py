"""
Unit tests for Vigenère Cipher Helper challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.vigenere_cipher_helper import VigenereCipher


def test_cipher_ascii():
    """
    Tests the Vigenère Cipher Helper solution using ASCII characters.

    :return: None
    """
    cipher = VigenereCipher("password", "abcdefghijklmnopqrstuvwxyz")

    assert cipher.encode("codewars") == "rovwsoiv"
    assert cipher.decode("rovwsoiv") == "codewars"

    assert cipher.encode("cOdEwArS") == "rOvEsAiS"
    assert cipher.decode("rOvEsAiS") == "cOdEwArS"

    assert cipher.encode("CODEWARS") == "CODEWARS"
    assert cipher.decode("CODEWARS") == "CODEWARS"


def test_cipher_unicode():
    """
    Tests the Vigenère Cipher Helper solution using Unicode (Katakana) characters.

    :return: None
    """
    katakana = 'アイウエオァィゥェォカキクケコサシスセソタチツッテトナニヌネノハヒフヘホマミムメモヤャユュヨョ' \
               'ラリルレロワヲンー'
    cipher = VigenereCipher('カタカナ', katakana)
    assert cipher.decode("ドオカセガヨゴザキアニ") == "ドモアリガトゴザイマス"
