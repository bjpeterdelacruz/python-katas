"""
Unit tests for Triple Trouble challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.triple_trouble import triple_trouble


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [("aaa", "bbb", "ccc", "abcabcabc"),
                          ("aaaaaa", "bbbbbb", "cccccc", "abcabcabcabcabcabc"),
                          ("burn", "reds", "roll", "brrueordlnsl"),
                          ("Bm", "aa", "tn", "Batman"),
                          ("LLh", "euo", "xtr", "LexLuthor")])
def test_triple_trouble(test_input1, test_input2, test_input3, expected):
    """
    Tests the Triple Trouble solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert triple_trouble(test_input1, test_input2, test_input3) == expected
