"""
Unit tests for Delete Occurrences challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.delete_occurrences_of_an_element_if_it_occurs_more_than_n_times import delete_nth


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([20, 37, 20, 21], 1, [20, 37, 21]),
                          ([1, 1, 3, 3, 7, 2, 2, 2, 2], 3, [1, 1, 3, 3, 7, 2, 2, 2])])
def test_delete_nth(test_input1, test_input2, expected):
    """
    Tests the Delete Occurrences solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert delete_nth(test_input1, test_input2) == expected
