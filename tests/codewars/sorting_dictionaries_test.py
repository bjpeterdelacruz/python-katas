"""
Unit tests for Sorting Dictionaries challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sorting_dictionaries import sort_dict


@pytest.mark.parametrize("test_input, expected",
                         [({3: 1, 2: 2, 1: 3}, [(1, 3), (2, 2), (3, 1)]),
                          ({1: 2, 2: 4, 3: 6}, [(3, 6), (2, 4), (1, 2)]),
                          ({'a': 6, 'b': 2, 'c': 4}, [('a', 6), ('c', 4), ('b', 2)])])
def test_sort_dict(test_input, expected):
    """
    Tests the Sorting Dictionaries solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sort_dict(test_input) == expected
