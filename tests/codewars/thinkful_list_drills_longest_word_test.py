"""
Unit tests for Thinkful: Longest Word challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_list_drills_longest_word import longest


@pytest.mark.parametrize("test_input, expected",
                         [(['abc', 'a', 'Johnny', '', 'hello'], 6)])
def test_longest(test_input, expected):
    """
    Tests the Thinkful: Longest Word solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert longest(test_input) == expected
