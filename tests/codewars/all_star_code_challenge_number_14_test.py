"""
Unit tests for All Star Code Challenge #14 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.all_star_code_challenge_number_14 import median


@pytest.mark.parametrize("test_input, expected",
                         [([3, 1, 2], 2), ([4, 4, 2, 2, 1, 3], 2.5)])
def test_median(test_input, expected):
    """
    Tests the All Star Code Challenge #14 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert median(test_input) == expected
