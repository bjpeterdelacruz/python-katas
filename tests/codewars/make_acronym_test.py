"""
Unit tests for Make Acronym challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.make_acronym import to_acronym


@pytest.mark.parametrize("test_input, expected",
                         [("Big Joe", "BJ"), ("", ""), ("Alpha Bravo Charlie Delta", "ABCD")])
def test_to_acronym(test_input, expected):
    """
    Tests the Make Acronym solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert to_acronym(test_input) == expected
