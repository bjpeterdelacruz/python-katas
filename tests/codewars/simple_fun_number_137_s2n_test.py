"""
Unit tests for S2N challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_fun_number_137_s2n import s2n


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(2, 3, 20),
                          (3, 5, 434),
                          (10, 9, 1762344782),
                          (1, 1, 3),
                          (0, 0, 1),
                          (300, 2, 9090501),
                          (567, 2, 61083856),
                          (37, 4, 15335280),
                          (36, 4, 13409059)])
def test_s2n(test_input1, test_input2, expected):
    """
    Tests the S2N solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert s2n(test_input1, test_input2) == expected
