"""
Unit tests for Vaporcode challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.v_a_p_o_r_c_o_d_e import vaporcode


@pytest.mark.parametrize("test_input, expected",
                         [("Lets go to the movies",
                           "L  E  T  S  G  O  T  O  T  H  E  M  O  V  I  E  S"),
                          ("Why isn't my code working?",
                           "W  H  Y  I  S  N  '  T  M  Y  C  O  D  E  W  O  R  K  I  N  G  ?")])
def test_vaporcode(test_input, expected):
    """
    Tests the Vaporcode solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert vaporcode(test_input) == expected
