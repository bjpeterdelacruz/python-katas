"""
Unit tests for Sum of All the Multiples of 3 or 5 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_of_all_the_multiples_of_3_or_5 import find


@pytest.mark.parametrize("test_input, expected",
                         [(5, 8), (10, 33), (100, 2418), (1000, 234168)])
def test_find(test_input, expected):
    """
    Tests the Sum of All the Multiples of 3 or 5 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find(test_input) == expected
