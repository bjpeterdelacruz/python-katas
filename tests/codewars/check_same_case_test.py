"""
Unit tests for Check Same Case challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.check_same_case import same_case


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('a', 'b', 1), ('Z', 'Y', 1), ('-', 'a', -1), ('Z', '0', -1),
                          ('a', 'Z', 0), ('M', 'n', 0)])
def test_same_case(test_input1, test_input2, expected):
    """
    Tests the Check Same Case solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert same_case(test_input1, test_input2) == expected
