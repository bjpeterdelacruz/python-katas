"""
Unit tests for Simple Decrypt Algo challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_decrypt_algo import decrypt


@pytest.mark.parametrize("test_input, expected",
                         [('$aaaa#bbb*ccfff!z', '43200300000000000000000001'),
                          ('z$aaa#ccc%eee1234567890', '30303000000000000000000001')])
def test_decrypt(test_input, expected):
    """
    Tests the Simple Decrypt Algo solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert decrypt(test_input) == expected
