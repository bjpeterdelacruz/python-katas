"""
Unit tests for Valid Number to Two Decimal Places challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.valid_number_to_2_decimal_places import valid_number


@pytest.mark.parametrize("test_input, expected",
                         [("0.00", True),
                          (".00", True),
                          ("-.00", True),
                          ("+.20", True),
                          ("-.20", True),
                          ("+10.20", True),
                          ("-10.20", True),
                          (".99", True),
                          (".30", True),
                          ("0.40", True),
                          ("34443.33", True),
                          (".000", False),
                          (".0a", False),
                          ("1.00.", False),
                          (".+00", False),
                          ("100", False),
                          ("100.", False),
                          ("10000.8", False),
                          ("w.00", False),
                          ("..00", False),
                          ("1,00", False),
                          ("6.00a", False),
                          (".110", False),
                          (".00-", False)])
def test_valid_number(test_input, expected):
    """
    Tests the Valid Number to Two Decimal Places solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert valid_number(test_input) == expected
