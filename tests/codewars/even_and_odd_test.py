"""
Unit tests for Even and Odd challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.even_and_odd import even_and_odd


@pytest.mark.parametrize("test_input, expected",
                         [(126453, (264, 153)),
                          (3012, (2, 31)),
                          (4628, (4628, 0)),
                          (2134563, (246, 1353)),
                          (513513515, (0, 513513515)),
                          (1326459980031245, (26480024, 13599315)),
                          (1035636314685115613, (664686, 135331511513)),
                          (1111111359, (0, 1111111359)),
                          (22220468, (22220468, 0)),
                          (1032469007531245, (2460024, 13975315)),
                          (0, (0, 0))])
def test_even_and_odd(test_input, expected):
    """
    Tests the Even and Odd solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert even_and_odd(test_input) == expected
