"""
Unit tests for Playing with Passphrases challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.playing_with_passphrases import play_pass


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("I LOVE YOU!!!", 1, "!!!vPz fWpM J"),
                          ("i love you!!!", 1, "!!!vPz fWpM J"),
                          ("MY GRANMA CAME FROM NY ON THE 23RD OF APRIL 2015", 2,
                           "4897 NkTrC Hq fT67 GjV Pq aP OqTh gOcE CoPcTi aO")])
def test_play_pass(test_input1, test_input2, expected):
    """
    Tests the Playing with Passphrases solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert play_pass(test_input1, test_input2) == expected
