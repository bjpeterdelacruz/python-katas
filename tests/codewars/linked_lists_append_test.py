"""
Unit tests for Linked Lists: Append challenge

:Author: BJ Peter Dela Cruz
"""
from utils.linked_list import Node, build_one_two, build_two_one, build_one_two_three, push
from codewars.linked_lists_append import append


def build_four_five_six():
    """
    This function builds the following linked list: 4 -> 5 -> 6 -> None

    :return: A linked list with three elements
    """
    head = None
    head = push(head, 6)
    head = push(head, 5)
    head = push(head, 4)
    return head


def build_one_two_three_four_five_six():
    """
    This function builds the following linked list: 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> None

    :return: A linked list with six elements
    """
    head = None
    head = push(head, 6)
    head = push(head, 5)
    head = push(head, 4)
    head = push(head, 3)
    head = push(head, 2)
    head = push(head, 1)
    return head


def build_four_five_six_one_two_three():
    """
    This function builds the following linked list: 4 -> 5 -> 6 -> 1 -> 2 -> 3 -> None

    :return: A linked list with six elements
    """
    head = None
    head = push(head, 3)
    head = push(head, 2)
    head = push(head, 1)
    head = push(head, 6)
    head = push(head, 5)
    head = push(head, 4)
    return head


def test_append_both_empty():
    """
    Assert that None is returned if no lists are passed in.

    :return: None
    """
    assert append(None, None) is None


def test_append_one_is_empty():
    """
    Assert that if only one list is passed in, that same list is returned.

    :return: None
    """
    one_list = build_one_two_three()
    assert append(None, one_list) == one_list
    assert append(one_list, None) == one_list


def assert_linked_list_equals(first_list, second_list):
    """
    Assert that both lists are equal by checking their contents.

    :return: None
    """
    while first_list is not None and second_list is not None:
        assert first_list.data == second_list.data
        first_list = first_list.next
        second_list = second_list.next
    assert first_list is None
    assert second_list is None


def test_append_non_empty_lists():
    """
    Assert that passing in two non-emtpy lists will return a concatenated list.

    :return: None
    """
    assert_linked_list_equals(append(Node(1), Node(2)), build_one_two())
    assert_linked_list_equals(append(Node(2), Node(1)), build_two_one())
    assert append(Node(2), Node(1)).next.next is None

    assert_linked_list_equals(append(build_one_two_three(), build_four_five_six()),
                              build_one_two_three_four_five_six())
    assert_linked_list_equals(append(build_four_five_six(), build_one_two_three()),
                              build_four_five_six_one_two_three())
    assert append(build_four_five_six(),
                  build_one_two_three()).next.next.next.next.next.next is None
