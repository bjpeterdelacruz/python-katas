"""
Unit tests for Where My Anagrams At? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.where_my_anagrams_at import anagrams


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('abba', ['aabb', 'abcd', 'bbaa', 'dada'], ["aabb", "bbaa"])])
def test_valid_parentheses(test_input1, test_input2, expected):
    """
    Tests the Where My Anagrams At? solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert anagrams(test_input1, test_input2) == expected
