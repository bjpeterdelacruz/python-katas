"""
Unit tests for All Star Code Challenge #24 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.all_star_code_challenge_number_24 import hypotenuse, leg


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(4, 3, 5)])
def test_hypotenuse(test_input1, test_input2, expected):
    """
    Tests the All Star Code Challenge #24 solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert hypotenuse(test_input1, test_input2) == expected


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(5, 3, 4)])
def test_leg(test_input1, test_input2, expected):
    """
    Tests the All Star Code Challenge #24 solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert leg(test_input1, test_input2) == expected
