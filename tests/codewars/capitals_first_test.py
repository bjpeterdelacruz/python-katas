"""
Unit tests for Capitals First! challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.capitals_first import capitals_first


@pytest.mark.parametrize("test_input, expected",
                         [("hey You, Sort me Already", "You, Sort Already hey me"),
                          ("sense Does to That Make you?", "Does That Make sense to you?"),
                          ("i First need Thing In coffee The Morning",
                           "First Thing In The Morning i need coffee"),
                          ("123 baby You and Me", "You Me baby and"),
                          ("Life gets Sometimes pretty !Hard", "Life Sometimes gets pretty")])
def test_capitals_first(test_input, expected):
    """
    Tests the Capitals First! solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert capitals_first(test_input) == expected
