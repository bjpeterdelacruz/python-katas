"""
Unit tests for Wilson Primes challenge

:Author: BJ Peter Dela Cruz
"""
import random
from codewars.wilson_primes import am_i_wilson


def test_am_i_wilson():
    """
    Tests the Wilson Primes solution.

    :return: None
    """
    for _ in range(100):
        number = random.randint(0, 100_000)
        if number in [5, 13, 563]:
            assert am_i_wilson(number) is True
        else:
            assert am_i_wilson(number) is False
