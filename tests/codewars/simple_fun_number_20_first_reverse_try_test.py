"""
Unit tests for First Reverse Try challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_fun_number_20_first_reverse_try import first_reverse_try


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 3, 4, 5], [5, 2, 3, 4, 1]),
                          ([3, 5, 6, 2], [2, 5, 6, 3]),
                          ([], []),
                          ([111], [111]),
                          ([6, 4, 2, 5, 7], [7, 4, 2, 5, 6]),
                          (['one', 'two', 'three'], ['three', 'two', 'one']),
                          (['Codewars'], ['Codewars']),
                          ([111, 45], [45, 111])])
def test_first_reverse_try(test_input, expected):
    """
    Tests the First Reverse Try solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert first_reverse_try(test_input) == expected
