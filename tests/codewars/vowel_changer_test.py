"""
Unit tests for Vowel Changer challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.vowel_changer import vowel_change


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("hannah hannah bo-bannah banana fanna fo-fannah fee, fy, mo-mannah. "
                           "hannah!", 'i', 'hinnih hinnih bi-binnih binini finni fi-finnih fii, '
                                           'fy, mi-minnih. hinnih!'),
                          ('adira wants to go to the park', 'o', 'odoro wonts to go to tho pork')])
def test_vowel_change(test_input1, test_input2, expected):
    """
    Tests the Vowel Changer solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert vowel_change(test_input1, test_input2) == expected
