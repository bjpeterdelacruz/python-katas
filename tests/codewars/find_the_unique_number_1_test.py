"""
Unit tests for Find the Unique Element challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_the_unique_number_1 import find_uniq


@pytest.mark.parametrize("test_input, expected",
                         [([0, 1, 1, 1, 1, 1], 0), ([1, 1, 1, 2, 1, 1], 2),
                          ([0, 0, 0.55, 0, 0], 0.55), ([3, 10, 3, 3, 3], 10)])
def test_find_uniq(test_input, expected):
    """
    Tests the Find the Unique Element solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_uniq(test_input) == expected
