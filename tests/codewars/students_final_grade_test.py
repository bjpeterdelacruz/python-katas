"""
Unit tests for Student's Final Grade challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.students_final_grade import final_grade


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(100, 12, 100),
                          (99, 0, 100),
                          (10, 15, 100),
                          (85, 5, 90),
                          (55, 3, 75),
                          (55, 0, 0),
                          (20, 2, 0)])
def test_final_grade(test_input1, test_input2, expected):
    """
    Tests the Student's Final Grade solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert final_grade(test_input1, test_input2) == expected
