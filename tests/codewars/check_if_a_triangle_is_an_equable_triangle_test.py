"""
Unit tests for Check if a Triangle is an Equable Triangle challenge

:Author: BJ Peter Dela Cruz
"""
from random import randint, random
from codewars.check_if_a_triangle_is_an_equable_triangle import equable_triangle


def test_equable_triangle():
    """
    Tests the Check if a Triangle is an Equable Triangle solution.

    :return: None
    """
    base = [[9, 10, 17], [6, 25, 29], [7, 15, 20], [6, 8, 10], [5, 12, 13]]

    for _ in range(1000):
        arr = sorted(base[randint(0, len(base) - 1)], key=lambda a: random()) if randint(0, 1) \
            else generate_sides()
        assert equable_triangle(*arr[:]) == solution(*arr)


def solution(*sides):
    """
    This function returns True if the given sides form an equable triangle, False otherwise.

    :param sides: The sides of a triangle
    :return: True if sides form an equable triangle, False otherwise.
    """
    return sorted(sides) in [[9, 10, 17], [6, 25, 29], [7, 15, 20], [6, 8, 10],
                             [5, 12, 13]]


def generate_sides():
    """
    Generates a list of three integers, each representing the side of a triangle.

    :return: A list of three integers generated randomly.
    """
    side_1 = randint(2, 100)
    side_2 = randint(2, 100)
    return sorted([side_1, side_2, randint(*sorted([side_1, side_2]))])
