"""
Unit tests for Some (But Not All) challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.some_but_not_all import some_but_not_all


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('abcdefg&%$', str.isalpha, True),
                          ('&%$=', str.isalpha, False),
                          ('abcdefg', str.isalpha, False),
                          ([4, 1], lambda x: x > 3, True),
                          ([1, 1], lambda x: x > 3, False),
                          ([4, 4], lambda x: x > 3, False),
                          ('a', str.isalpha, False),
                          ('', str.isalpha, False),
                          (':f', str.isalpha, True),
                          ('f:', str.isalpha, True),
                          ([], lambda x: x > 3, False),
                          ([1], lambda x: x > 3, False),
                          ([1] * 20, lambda x: x > 3, False),
                          ([4] * 20, lambda x: x > 3, False)])
def test_some_but_not_all(test_input1, test_input2, expected):
    """
    Tests the Some (But Not All) solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert some_but_not_all(test_input1, test_input2) == expected
