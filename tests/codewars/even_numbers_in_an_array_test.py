"""
Unit tests for Even Numbers in an Array challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.even_numbers_in_an_array import even_numbers


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([2, 3, 4, 5, 6, 7, 8, 9, 10], 3, [6, 8, 10]),
                          ([-4, 2, -6, 0], 2, [-6, 0])])
def test_even_numbers(test_input1, test_input2, expected):
    """
    Tests the Even Numbers in an Array solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert even_numbers(test_input1, test_input2) == expected
