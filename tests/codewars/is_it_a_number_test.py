"""
Unit tests for Is It a Number? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.is_it_a_number import is_digit


@pytest.mark.parametrize("test_input, expected",
                         [("s2324", False),
                          ("-234.4", True),
                          ("3 4", False),
                          ("3-4", False),
                          ("3 4   ", False),
                          ("34.65", True),
                          ("-0", True),
                          ("0.0", True),
                          ("", False),
                          (" ", False)])
def test_is_digit(test_input, expected):
    """
    Tests the Is It a Number? solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_digit(test_input) == expected
