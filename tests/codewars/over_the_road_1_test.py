"""
Unit tests for Over the Road challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.over_the_road_1 import over_the_road


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(1, 3, 6), (3, 3, 4), (2, 3, 5), (3, 5, 8), (7, 11, 16),
                          (23633656673, 310027696726, 596421736780),
                          (596421736780, 310027696726, 23633656673)])
def test_over_the_road(test_input1, test_input2, expected):
    """
    Tests the Over the Road solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert over_the_road(test_input1, test_input2) == expected
