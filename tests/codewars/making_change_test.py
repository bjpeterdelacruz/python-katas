"""
Unit tests for Making Change challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.making_change import make_change


@pytest.mark.parametrize("test_input, expected",
                         [(0, {}), (1, {"P": 1}), (5, {"N": 1}),
                          (43, {"Q": 1, "D": 1, "N": 1, "P": 3}),
                          (91, {"H": 1, "Q": 1, "D": 1, "N": 1, "P": 1}), (101, {"H": 2, "P": 1}),
                          (239, {"H": 4, "Q": 1, "D": 1, "P": 4})])
def test_make_change(test_input, expected):
    """
    Tests the Making Change solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert make_change(test_input) == expected
