"""
Unit tests for Rotate the Letters of Each Element challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.rotate_the_letters_of_each_element import group_cities


@pytest.mark.parametrize("test_input, expected",
                         [(['Tokyo', 'Faaa', 'Aafa', 'Afaa', 'London', 'Rome', 'Donlon', 'Kyoto',
                            'Paris', 'Okyot'],
                           [['Aafa', 'Afaa', 'Faaa'],
                            ['Kyoto', 'Okyot', 'Tokyo'], ['Donlon', 'London'],
                            ['Paris'], ['Rome']]),
                          (['Tokyo', 'London', 'Rome', 'Donlon'],
                           [['Donlon', 'London'], ['Rome'], ['Tokyo']]),
                          (['Rome', 'Rome', 'Rome', 'Donlon', 'London'],
                           [['Donlon', 'London'], ['Rome']]), (['Ab', 'Aa'], [['Aa'], ['Ab']]),
                          ([], [])])
def test_group_cities(test_input, expected):
    """
    Tests the Rotate the Letters of Each Element solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert group_cities(test_input) == expected
