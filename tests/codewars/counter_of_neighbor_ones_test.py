"""
Unit tests for Count of Neighbor Ones challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.counter_of_neighbor_ones import ones_counter


@pytest.mark.parametrize("test_input, expected",
                         [([0], []),
                          ([1, 0, 1, 1], [1, 2]),
                          ([0, 0, 0, 0, 0, 0, 0, 0], []),
                          ([1, 1, 1, 1, 1], [5]),
                          ([1, 1, 1, 0, 0, 1, 0, 1, 1, 0], [3, 1, 2]),
                          ([0, 0, 0, 1, 0, 0, 1, 1], [1, 2]),
                          ([1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1], [1, 2, 4, 1])])
def test_ones_counter(test_input, expected):
    """
    Tests the Count of Neighbor Ones solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert ones_counter(test_input) == expected
