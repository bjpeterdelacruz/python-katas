"""
Unit tests for Showing X to Y of Z Products challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.showing_x_to_y_of_z_products import pagination_text


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(4, 9, 36, "Showing 28 to 36 of 36 Products."),
                          (1, 10, 30, "Showing 1 to 10 of 30 Products."),
                          (3, 10, 26, "Showing 21 to 26 of 26 Products."),
                          (1, 10, 8, "Showing 1 to 8 of 8 Products."),
                          (2, 30, 350, "Showing 31 to 60 of 350 Products."),
                          (1, 23, 30, "Showing 1 to 23 of 30 Products."),
                          (2, 23, 30, "Showing 24 to 30 of 30 Products."),
                          (43, 15, 3456, "Showing 631 to 645 of 3456 Products.")])
def test_pagination_text(test_input1, test_input2, test_input3, expected):
    """
    Tests the Decreasing Inputs solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert pagination_text(test_input1, test_input2, test_input3) == expected
