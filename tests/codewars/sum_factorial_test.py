"""
Unit tests for Sum Factorial challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_factorial import sum_factorial


@pytest.mark.parametrize("test_input, expected",
                         [([4, 6], 744), ([5, 4, 1], 145)])
def test_sum_factorial(test_input, expected):
    """
    Tests the Sum Factorial solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_factorial(test_input) == expected
