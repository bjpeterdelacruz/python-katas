"""
Unit tests for Polybius Square Cipher challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.polybius_square_cipher_encode import polybius


@pytest.mark.parametrize("test_input, expected",
                         [('A', "11"), ("IJ", "2424"), ("CODEWARS", "1334141552114243"),
                          ("POLYBIUS SQUARE CIPHER", "3534315412244543 434145114215 132435231542")])
def test_play_pass(test_input, expected):
    """
    Tests the Polybius Square Cipher solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert polybius(test_input) == expected
