"""
Unit tests for EAN Validation challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.ean_validation import validate_ean


@pytest.mark.parametrize("test_input, expected",
                         [("9783815820865", True), ("9783815820864", False),
                          ("9783827317100", True), ("4003301018398", True),
                          ("9783827317101", False), ("4003301018392", False),
                          ("0000000000017", True), ("0000000000010", False),
                          ("9783815820865", True), ("9783815820864", False),
                          ("9783827317100", True), ("978382731710", False)])
def test_validate_ean(test_input, expected):
    """
    Tests the Determine the Date solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert validate_ean(test_input) == expected
