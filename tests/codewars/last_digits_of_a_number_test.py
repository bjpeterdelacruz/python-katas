"""
Unit tests for Last Digits of a Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.last_digits_of_a_number import solution


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(1, 1, [1]),
                          (123767, 4, [3, 7, 6, 7]),
                          (0, 1, [0]),
                          (34625647867585, 10, [5, 6, 4, 7, 8, 6, 7, 5, 8, 5]),
                          (1234, 0, []),
                          (24134, -4, []),
                          (1343, 5, [1, 3, 4, 3])])
def test_solution(test_input1, test_input2, expected):
    """
    Tests the Last Digits of a Number solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert solution(test_input1, test_input2) == expected
