"""
Unit tests for Filter Long Words challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.filter_long_words import filter_long_words


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("The quick brown fox jumps over the lazy dog", 4,
                           ['quick', 'brown', 'jumps'])])
def test_filter_long_words(test_input1, test_input2, expected):
    """
    Tests the Filter Long Words solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert filter_long_words(test_input1, test_input2) == expected
