"""
Unit tests for Calculating with Functions challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.calculating_with_functions import zero, one, two, three, four, five, six, seven, eight
from codewars.calculating_with_functions import nine, plus, minus, times, divided_by


def test_calculating_with_functions():
    """
    Tests the Calculating with Functions solution by performing some math operations.

    :return: None
    """
    assert zero(plus(one())) == 1
    assert two(minus(three())) == -1
    assert four(times(five())) == 20
    assert six(divided_by(four())) == 1
    assert seven(plus(eight())) == 15
    assert nine(minus(nine())) == 0
    assert eight(times(two())) == 16
    assert six(divided_by(three())) == 2
