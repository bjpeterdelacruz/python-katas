"""
Unit tests for Are You Available challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.are_you_available import check_availability


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([['09:30', '10:15'], ['12:20', '15:50']], '10:00', '10:15'),
                          ([['09:30', '10:15'], ['12:20', '15:50']], '11:00', True)])
def test_check_availability(test_input1, test_input2, expected):
    """
    Tests the "Are You Available" solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert check_availability(test_input1, test_input2) == expected
