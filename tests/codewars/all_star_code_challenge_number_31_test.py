"""
Unit tests for All Star Code Challenge #31 challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.all_star_code_challenge_number_31 import help_jesse, Instruction


def test_help_jesse():
    """
    Tests the All Star Code Challenge #31 solution.

    :return: None
    """
    recipe = [
        Instruction(5, "Sodium Chloride", "Graduated Cylinder"),
        Instruction(250, "Hydrochloric Acid", "Boiling Flask"),
        Instruction(100, "Water", "Erlenmeyer Flask", "Do NOT mess this step up, Jesse!")
    ]
    test_recipe = recipe[:]
    assert help_jesse(recipe) == ["Pour 5 mL of Sodium Chloride into a Graduated Cylinder",
                                  "Pour 250 mL of Hydrochloric Acid into a Boiling Flask",
                                  "Pour 100 mL of Water into a Erlenmeyer Flask (Do NOT mess this "
                                  "step up, Jesse!)"]
    assert recipe[0] == test_recipe[0]
    assert recipe[1] == test_recipe[1]
    assert recipe[2] == test_recipe[2]
    assert recipe == test_recipe
