"""
Unit tests for Stanton Measure challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.stanton_measure import stanton_measure


@pytest.mark.parametrize("test_input, expected",
                         [([1, 4, 3, 2, 1, 2, 3, 2], 3), ([1, 1, 1, 3, 3, 3, 3, 3], 5)])
def test_stanton_measure(test_input, expected):
    """
    Tests the Stanton Measure solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert stanton_measure(test_input) == expected
