"""
Unit tests for Sum of Intervals challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_of_minimums import sum_of_minimums


@pytest.mark.parametrize("test_input, expected",
                         [([[1, 2, 3], [0, -2, 2]], -1), ([[9, 6, 3], [4, 8, 12]], 7)])
def test_sum_of_minimums(test_input, expected):
    """
    Tests the Sum of Intervals solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_of_minimums(test_input) == expected
