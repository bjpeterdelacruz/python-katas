"""
Unit tests for Grid Index challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.grid_index import grid_index


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([['m', 'y', 'e'], ['x', 'a', 'm'], ['p', 'l', 'e']],
                           [1, 2, 3, 4, 5, 6, 7, 8, 9],
                           "myexample"),
                          ([['m', 'y', 'e'], ['x', 'a', 'm'], ['p', 'l', 'e']], [1, 5, 6],
                           "mam"),
                          ([['m', 'y', 'e'], ['x', 'a', 'm'], ['p', 'l', 'e']], [1, 3, 7, 8],
                           "mepl"),
                          ([['h', 'e', 'l', 'l'], ['o', 'c', 'o', 'd'], ['e', 'w', 'a', 'r'],
                            ['r', 'i', 'o', 'r']], [5, 7, 9, 3, 6, 6, 8, 8, 16, 13],
                           "ooelccddrr")])
def test_grid_index(test_input1, test_input2, expected):
    """
    Tests the Grid Index solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert grid_index(test_input1, test_input2) == expected
