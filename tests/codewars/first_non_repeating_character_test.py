"""
Unit tests for First Non-Repeating Character challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.first_non_repeating_character import first_non_repeating_letter


@pytest.mark.parametrize("test_input, expected",
                         [('', ''), ('aa', ''), ('a', 'a'),
                          ('Go hang a salami, I\'m a lasagna hog!', ','), ('sTress', 'T')])
def test_first_non_repeating_letter(test_input, expected):
    """
    Tests the First Non-Repeating Character solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert first_non_repeating_letter(test_input) == expected
