"""
Unit tests for Convert to Binary challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.convert_to_binary import to_binary


@pytest.mark.parametrize("test_input, expected",
                         [(1, 1), (5, 101), (11, 1011)])
def test_to_binary(test_input, expected):
    """
    Tests the Convert to Binary solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert to_binary(test_input) == expected
