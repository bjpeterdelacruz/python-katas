"""
Unit tests for Formatting Decimal Places #1 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.formatting_decimal_places_1 import two_decimal_places


@pytest.mark.parametrize("test_input, expected",
                         [(1.232, 1.23), (1.459, 1.45), (10.1289767789, 10.12),
                          (-7488.83485834983, -7488.83), (4.653725356, 4.65)])
def test_two_decimal_places(test_input, expected):
    """
    Tests the Formatting Decimal Places #1 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert two_decimal_places(test_input) == expected
