"""
Unit tests for Thinkful: Quotable challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_string_drills_quotable import quotable


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('Gary', 'Hello World', 'Gary said: "Hello World"')])
def test_quotable(test_input1, test_input2, expected):
    """
    Tests the Thinkful: Quotable solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert quotable(test_input1, test_input2) == expected
