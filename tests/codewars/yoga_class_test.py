"""
Unit tests for Yoga Class challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.yoga_class import yoga


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([[3, 2, 1, 3], [1, 3, 2, 1], [1, 1, 1, 2]],
                           [1, 7, 5, 9, 10, 21, 4, 3], 68),
                          ([[7, 2, 1, 0], [1, 3, 2, 2], [1, 9, 1, 2]],
                           [1000, 20, 3, 105, 66, 204, 4, 1, 22, 86], 38),
                          ([[0, 0], [0, 0]], [1, 1, 0, 1, 2, 3, 0, 1, 5], 8),
                          ([], [], 0), ([], [1, 3, 4], 0), ([[0, 0], [0, 0]], [], 0)])
def test_yoga(test_input1, test_input2, expected):
    """
    Tests the Yoga Class solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert yoga(test_input1, test_input2) == expected
