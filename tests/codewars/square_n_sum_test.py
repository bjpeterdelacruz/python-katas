"""
Unit tests for Square(n) Sum challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.square_n_sum import square_sum


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2], 5),
                          ([0, 3, 4, 5], 50),
                          ([], 0),
                          ([-1, -2], 5),
                          ([-1, 0, 1], 2)])
def test_square_sum(test_input, expected):
    """
    Tests the Square(n) Sum solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert square_sum(test_input) == expected
