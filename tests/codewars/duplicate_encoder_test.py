"""
Unit tests for Duplicate Encode challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.duplicate_encoder import duplicate_encode


@pytest.mark.parametrize("test_input, expected",
                         [("din", "((("), ("recede", "()()()"), ("Success", ")())())"),
                          ("(( @", "))(("), ("!ueGHy", "((((((")])
def test_duplicate_encode(test_input, expected):
    """
    Tests the Duplicate Encode solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert duplicate_encode(test_input) == expected
