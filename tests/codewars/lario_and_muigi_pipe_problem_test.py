"""
Unit tests for Lario and Muigi Pipe Problem challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.lario_and_muigi_pipe_problem import pipe_fix


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 3, 5, 6, 8, 9], [1, 2, 3, 4, 5, 6, 7, 8, 9]),
                          ([1, 2, 3, 12], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
                          ([6, 9], [6, 7, 8, 9]),
                          ([-1, 4], [-1, 0, 1, 2, 3, 4]),
                          ([1, 2, 3], [1, 2, 3]),
                          ([2], [2])])
def test_pipe_fix(test_input, expected):
    """
    Tests the Lario and Muigi Pipe Problem solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert pipe_fix(test_input) == expected
