"""
Unit tests for esreveR challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.esrever import reverse


@pytest.mark.parametrize("test_input, expected",
                         [([], []), ([1, 2, 3], [3, 2, 1]),
                          (["a", 0, True, None], [None, True, 0, "a"])])
def test_reverse(test_input, expected):
    """
    Tests the esreveR solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert reverse(test_input) == expected
