"""
Unit tests for Number to Bytes challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.number_to_bytes import to_bytes


@pytest.mark.parametrize("test_input, expected",
                         [(0, ['00000000']), (12, ['00001100']), (256, ['00000001', '00000000']),
                          (0b1_00101100_10101110, ['00000001', '00101100', '10101110'])])
def test_to_bytes(test_input, expected):
    """
    Tests the Number to Bytes solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert to_bytes(test_input) == expected
