"""
Unit tests for Simple Multiplication challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_multiplication import simple_multiplication


@pytest.mark.parametrize("test_input, expected",
                         [(3, 27), (4, 32), (0, 0)])
def test_simple_multiplication(test_input, expected):
    """
    Tests the Simple Multiplication solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert simple_multiplication(test_input) == expected
