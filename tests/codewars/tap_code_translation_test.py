"""
Unit tests for Tap Code Translation challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.tap_code_translation import tap_code_translation


@pytest.mark.parametrize("test_input, expected",
                         [("test", ".... .... . ..... .... ... .... ...."),
                          ("total", ".... .... ... .... .... .... . . ... ."),
                          ("break", ". .. .... .. . ..... . . . ..."),
                          ("something",
                           ".... ... ... .... ... .. . ..... .... .... .. ... .. .... ... ... "
                           ".. .."),
                          ("final", ".. . .. .... ... ... . . ... .")])
def test_tap_code_translation(test_input, expected):
    """
    Tests the Tap Code Translation solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert tap_code_translation(test_input) == expected
