"""
Unit tests for Words to Sentence challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.words_to_sentence import words_to_sentence


@pytest.mark.parametrize("test_input, expected",
                         [(['bacon', 'is', 'delicious'], 'bacon is delicious')])
def test_words_to_sentence(test_input, expected):
    """
    Tests the Words to Sentence solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert words_to_sentence(test_input) == expected
