"""
Unit tests for Build a Square challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.build_a_square import generate_shape


@pytest.mark.parametrize("test_input, expected",
                         [(8, '++++++++\n++++++++\n++++++++\n++++++++\n++++++++\n++++++++\n'
                              '++++++++\n++++++++'),
                          (3, '+++\n+++\n+++')])
def test_generate_shape(test_input, expected):
    """
    Tests the Buddy Pairs solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert generate_shape(test_input) == expected
