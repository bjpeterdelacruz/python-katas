"""
Unit tests for Exclamation Marks Series #14 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_14_convert_the_exclamation_marks_and_question_marks_to_a_prime_number import convert  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [("!!", 2),
                          ("!??", 3),
                          ("!???", 13),
                          ("!!!??", 2),
                          ("!!!???", 11),
                          ("!???!!", 11),
                          ("!????!!!?", 53),
                          ("!!!!!!!???????", 11)])
def test_convert(test_input, expected):
    """
    Tests the Exclamation Marks Series #14 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert convert(test_input) == expected
