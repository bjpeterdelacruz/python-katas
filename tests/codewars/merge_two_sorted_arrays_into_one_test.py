"""
Unit tests for Merge Two Sorted Arrays Into One challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.merge_two_sorted_arrays_into_one import merge_arrays


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3, 4], [5, 6, 7, 8], [1, 2, 3, 4, 5, 6, 7, 8]),
                          ([10, 8, 6, 4, 2], [9, 7, 5, 3, 1], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
                          ([-20, 35, 36, 37, 39, 40], [-10, -5, 0, 6, 7, 8, 9, 10, 25, 38, 50, 62],
                           [-20, -10, -5, 0, 6, 7, 8, 9, 10, 25, 35, 36, 37, 38, 39, 40, 50, 62]),
                          ([5, 6, 7, 8, 9, 10], [20, 18, 15, 14, 13, 12, 11, 4, 3, 2],
                           [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 18, 20]),
                          ([45, 30, 20, 15, 12, 5], [9, 10, 18, 25, 35, 50],
                           [5, 9, 10, 12, 15, 18, 20, 25, 30, 35, 45, 50]),
                          ([-8, -3, -2, 4, 5, 6, 7, 15, 42, 90, 134],
                           [216, 102, 74, 32, 8, 2, 0, -9, -13],
                           [-13, -9, -8, -3, -2, 0, 2, 4, 5, 6, 7, 8, 15, 32, 42, 74, 90, 102, 134,
                            216]),
                          ([-100, -27, -8, 5, 23, 56, 124, 325],
                           [-34, -27, 6, 12, 25, 56, 213, 325, 601],
                           [-100, -34, -27, -8, 5, 6, 12, 23, 25, 56, 124, 213, 325, 601]),
                          ([18, 7, 2, 0, -22, -46, -103, -293],
                           [-300, -293, -46, -31, -5, 0, 18, 19, 74, 231],
                           [-300, -293, -103, -46, -31, -22, -5, 0, 2, 7, 18, 19, 74, 231]),
                          ([105, 73, -4, -73, -201], [-201, -73, -4, 73, 105],
                           [-201, -73, -4, 73, 105])])
def test_merge_arrays(test_input1, test_input2, expected):
    """
    Tests the Merge Two Sorted Arrays Into One solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert merge_arrays(test_input1, test_input2) == expected
