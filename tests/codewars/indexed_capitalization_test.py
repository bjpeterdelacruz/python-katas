"""
Unit tests for Indexed Capitalization challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.indexed_capitalization import capitalize


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("abcdef", [1, 2, 5], 'aBCdeF'),
                          ("abcdef", [1, 2, 5, 100], 'aBCdeF',),
                          ("codewars", [1, 3, 5, 50], 'cOdEwArs'),
                          ("abracadabra", [2, 6, 9, 10], 'abRacaDabRA'),
                          ("codewarriors", [5], 'codewArriors'),
                          ("indexinglessons", [0], 'Indexinglessons')])
def test_capitalize(test_input1, test_input2, expected):
    """
    Tests the Indexed Capitalization solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert capitalize(test_input1, test_input2) == expected
