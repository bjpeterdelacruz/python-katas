"""
Unit tests for All Star Code Challenge #29 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.all_star_code_challenge_number_29 import reverse_sentence


@pytest.mark.parametrize("test_input, expected",
                         [("Hello !Nhoj Want to have lunch?", "olleH johN! tnaW ot evah ?hcnul"),
                          ("1 2 3 4 5", "1 2 3 4 5"),
                          ("Codewars", "srawedoC"),
                          ("CodeWars rules!", "sraWedoC !selur"),
                          ("", "")])
def test_add_arrays(test_input, expected):
    """
    Tests the All Star Code Challenge #29 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert reverse_sentence(test_input) == expected
