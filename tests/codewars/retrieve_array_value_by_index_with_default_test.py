"""
Unit tests for Retrieve Array Value by Index with Default Value challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.retrieve_array_value_by_index_with_default import solution


def test_solution():
    """
    Tests the Retrieve Array Value by Index with Default Value solution.

    :return: None
    """
    test_list = [1, 2, 3]
    assert solution(test_list, 0, 'a') == 1
    assert solution(test_list, 1, 'a') == 2
    assert solution(test_list, 2, 'a') == 3
    assert solution(test_list, 3, 'a') == 'a'
    assert solution(test_list, -1, 'a') == 3
    assert solution(test_list, -2, 'a') == 2
    assert solution(test_list, -3, 'a') == 1
    assert solution(test_list, -4, 'a') == 'a'

    assert solution(range(1, 6), 10, 'a') == 'a'
    assert solution([None, None], 0, 'a') is None
