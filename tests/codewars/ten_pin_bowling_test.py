"""
Unit tests for Tem-Pin Bowling challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.ten_pin_bowling import bowling_score


@pytest.mark.parametrize("test_input, expected",
                         [("X 11 9/ 80 X X 90 8/ 7/ 44", 136), ("X X 9/ 80 X X 90 8/ 7/ 44", 171),
                          ("9/ 9/ 9/ 9/ 9/ 9/ 9/ 9/ 9/ 9/X", 191),
                          ("9/ 9/ 9/ 9/ 8/ 9/ 9/ 9/ 9/ X9/", 191),
                          ("11 11 11 11 11 11 11 11 11 11", 20),
                          ("11 11 11 11 11 11 11 11 X 9/X", 56), ("X X X X X X X X X XXX", 300),
                          ("X X X X X X X X X X11", 273), ("X X X X X X X X X 72", 265),
                          ("X X X X X X X X X XX5", 295)])
def test_bowling_score(test_input, expected):
    """
    Tests the Ten-Pin Bowling solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert bowling_score(test_input) == expected
