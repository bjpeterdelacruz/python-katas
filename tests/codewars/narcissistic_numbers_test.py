"""
Unit tests for Narcissistic Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.narcissistic_numbers import is_narcissistic


@pytest.mark.parametrize("test_input, expected",
                         [(153, True), (370, True), (371, True), (407, True),
                          (152, False), (369, False), (372, False), (406, False)])
def test_is_narcissistic(test_input, expected):
    """
    Tests the Narcissistic Numbers solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_narcissistic(test_input) == expected
