"""
Unit tests for Simple Fun #319: Number and IP Address challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_fun_number_319_number_and_ip_address import number_and_ip_address


@pytest.mark.parametrize("test_input, expected",
                         [("10.0.3.193", "167773121"), ("167773121", "10.0.3.193")])
def test_number_and_ip_address(test_input, expected):
    """
    Tests the Simple Fun #319: Number and IP Address solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert number_and_ip_address(test_input) == expected
