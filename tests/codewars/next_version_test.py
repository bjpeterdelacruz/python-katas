"""
Unit tests for Next Version challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.next_version import next_version


@pytest.mark.parametrize("test_input, expected",
                         [("1.2.3", "1.2.4"), ("0.9.9", "1.0.0"), ("1", "2"),
                          ("1.2.3.4.5.6.7.8", "1.2.3.4.5.6.7.9"), ("9.9", "10.0"),
                          ("0.9.9.9.9.9.9.9.9.9.9.9.9.9.9.9", "1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0"),
                          ("9", "10"), ("10", "11"), ("9.9.9.9.9.9.9.8", "9.9.9.9.9.9.9.9"),
                          ("10.9.9.9.9.9", "11.0.0.0.0.0"), ("99.9", "100.0"), ("999.9", "1000.0"),
                          ("0.0.1", "0.0.2")])
def test_next_version(test_input, expected):
    """
    Tests the Next Version solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert next_version(test_input) == expected
