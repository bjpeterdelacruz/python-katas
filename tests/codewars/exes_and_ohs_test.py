"""
Unit tests for Exes and Ohs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exes_and_ohs import xo


@pytest.mark.parametrize("test_input, expected",
                         [("xoxo", True), ("xxxoo", False), ("oooxx", False), ("aaabbc", True)])
def test_xo(test_input, expected):
    """
    Tests the Exes and Ohs solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert xo(test_input) == expected
