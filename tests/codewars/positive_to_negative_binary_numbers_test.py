"""
Unit tests for Positive to Negative Binary Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.positive_to_negative_binary_numbers import positive_to_negative


@pytest.mark.parametrize("test_input, expected",
                         [([0, 1, 0, 0, 1], [1, 0, 1, 1, 1]),
                          ([0, 0, 0, 0], [0, 0, 0, 0]),
                          ([0, 0, 1, 0], [1, 1, 1, 0]),
                          ([0, 0, 1, 1], [1, 1, 0, 1]),
                          ([0, 1, 0, 0], [1, 1, 0, 0]),
                          ([0, 1, 0, 0], [1, 1, 0, 0])])
def test_positive_to_negative(test_input, expected):
    """
    Tests the Positive to Negative Binary Numbers solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert positive_to_negative(test_input) == expected
