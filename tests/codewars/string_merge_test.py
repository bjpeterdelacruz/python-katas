"""
Unit tests for String Merge! challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.string_merge import string_merge


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [('hello', 'world', 'l', 'held'),
                          ('jason', 'jacksonville', 's', 'jasonville')])
def test_string_merge(test_input1, test_input2, test_input3, expected):
    """
    Tests the String Merge! solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert string_merge(test_input1, test_input2, test_input3) == expected
