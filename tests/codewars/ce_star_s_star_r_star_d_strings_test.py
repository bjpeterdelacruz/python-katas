"""
Unit tests for Censored Strings challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.ce_star_s_star_r_star_d_strings import uncensor


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('*h*s *s v*ry *tr*ng*', 'Tiiesae', 'This is very strange'),
                          ('A**Z*N*', 'MAIG', 'AMAZING'),
                          ('xyz', '', 'xyz'),
                          ('', '', ''),
                          ('***', 'abc', 'abc')])
def test_uncensor(test_input1, test_input2, expected):
    """
    Tests the Censored Strings solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert uncensor(test_input1, test_input2) == expected
