"""
Unit tests for FizzBuzz Backwards challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.fizzbuzz_backwards import reverse_fizz_buzz


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, "Fizz", 4, "Buzz"], (3, 5)),
                          ([1, "Fizz", "Buzz", "Fizz", 5, "FizzBuzz"], (2, 3)),
                          ([1, "FizzBuzz", 3, "FizzBuzz", 5, "FizzBuzz"], (2, 2)),
                          (["Fizz", "Fizz", "Fizz", "Fizz", "Fizz", "FizzBuzz"], (1, 6)),
                          ([1, "Buzz", 3, "FizzBuzz", 5, "Buzz", 7, "FizzBuzz"], (4, 2))])
def test_count_bits(test_input, expected):
    """
    Tests the FizzBuzz Backwards solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert reverse_fizz_buzz(test_input) == expected
