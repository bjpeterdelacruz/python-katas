"""
Unit tests for Grasshopper: Basic Function Fixer challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.grasshopper_basic_function_fixer import add_five


@pytest.mark.parametrize("test_input, expected",
                         [(5, 10), (0, 5), (-5, 0)])
def test_add_five(test_input, expected):
    """
    Tests the Grasshopper: Basic Function Fixer solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert add_five(test_input) == expected
