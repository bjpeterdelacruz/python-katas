"""
Unit tests for Identical Elements challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.identical_elements import duplicate_elements


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3], [4, 5, 6], False), ([1, 2, 3], [4, 1], True)])
def test_duplicate_elements(test_input1, test_input2, expected):
    """
    Tests the Identical Elements solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert duplicate_elements(test_input1, test_input2) == expected
