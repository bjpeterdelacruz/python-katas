"""
Unit tests for Paul's Misery challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.pauls_misery import paul


@pytest.mark.parametrize("test_input, expected",
                         [(['life', 'eating', 'life'], "Super happy!"),
                          (['Petes kata', 'Petes kata', 'eating', 'Petes kata', 'Petes kata',
                            'eating'], "Happy!"),
                          (["Petes kata"] * 8, "Sad!"), (["Petes kata"] * 11, "Miserable!")])
def test_paul(test_input, expected):
    """
    Tests the Paul's Misery solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert paul(test_input) == expected
