"""
Unit tests for Alternate Logic challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.alternate_logic import alt_or

# pylint: disable=too-many-statements


def test_alt_or():
    """
    Tests the Alternate Logic solution.

    :return: None
    """
    assert alt_or([]) is None
    assert alt_or([False, False, False, False, False, False]) is False
    assert alt_or([False, False, False, False, False, True]) is True
    assert alt_or([False, False, False, False, True, False]) is True
    assert alt_or([False, False, False, False, True, True]) is True
    assert alt_or([False, False, False, True, False, False]) is True
    assert alt_or([False, False, False, True, False, True]) is True
    assert alt_or([False, False, False, True, True, False]) is True
    assert alt_or([False, False, False, True, True, True]) is True
    assert alt_or([False, False, True, False, False, False]) is True
    assert alt_or([False, False, True, False, False, True]) is True
    assert alt_or([False, False, True, False, True, False]) is True
    assert alt_or([False, False, True, False, True, True]) is True
    assert alt_or([False, False, True, True, False, False]) is True
    assert alt_or([False, False, True, True, False, True]) is True
    assert alt_or([False, False, True, True, True, False]) is True
    assert alt_or([False, False, True, True, True, True]) is True
    assert alt_or([False, True, False, False, False, False]) is True
    assert alt_or([False, True, False, False, False, True]) is True
    assert alt_or([False, True, False, False, True, False]) is True
    assert alt_or([False, True, False, False, True, True]) is True
    assert alt_or([False, True, False, True, False, False]) is True
    assert alt_or([False, True, False, True, False, True]) is True
    assert alt_or([False, True, False, True, True, False]) is True
    assert alt_or([False, True, False, True, True, True]) is True
    assert alt_or([False, True, True, False, False, False]) is True
    assert alt_or([False, True, True, False, False, True]) is True
    assert alt_or([False, True, True, False, True, False]) is True
    assert alt_or([False, True, True, False, True, True]) is True
    assert alt_or([False, True, True, True, False, False]) is True
    assert alt_or([False, True, True, True, False, True]) is True
    assert alt_or([False, True, True, True, True, False]) is True
    assert alt_or([False, True, True, True, True, True]) is True
    assert alt_or([True, False, False, False, False, False]) is True
    assert alt_or([True, False, False, False, False, True]) is True
    assert alt_or([True, False, False, False, True, False]) is True
    assert alt_or([True, False, False, False, True, True]) is True
    assert alt_or([True, False, False, True, False, False]) is True
    assert alt_or([True, False, False, True, False, True]) is True
    assert alt_or([True, False, False, True, True, False]) is True
    assert alt_or([True, False, False, True, True, True]) is True
    assert alt_or([True, False, True, False, False, False]) is True
    assert alt_or([True, False, True, False, False, True]) is True
    assert alt_or([True, False, True, False, True, False]) is True
    assert alt_or([True, False, True, False, True, True]) is True
    assert alt_or([True, False, True, True, False, False]) is True
    assert alt_or([True, False, True, True, False, True]) is True
    assert alt_or([True, False, True, True, True, False]) is True
    assert alt_or([True, False, True, True, True, True]) is True
    assert alt_or([True, True, False, False, False, False]) is True
    assert alt_or([True, True, False, False, False, True]) is True
    assert alt_or([True, True, False, False, True, False]) is True
    assert alt_or([True, True, False, False, True, True]) is True
    assert alt_or([True, True, False, True, False, False]) is True
    assert alt_or([True, True, False, True, False, True]) is True
    assert alt_or([True, True, False, True, True, False]) is True
    assert alt_or([True, True, False, True, True, True]) is True
    assert alt_or([True, True, True, False, False, False]) is True
    assert alt_or([True, True, True, False, False, True]) is True
    assert alt_or([True, True, True, False, True, False]) is True
    assert alt_or([True, True, True, False, True, True]) is True
    assert alt_or([True, True, True, True, False, False]) is True
    assert alt_or([True, True, True, True, False, True]) is True
    assert alt_or([True, True, True, True, True, False]) is True
    assert alt_or([True, True, True, True, True, True]) is True
    assert alt_or([False, False, False, False, False]) is False
    assert alt_or([False, False, False, False, True]) is True
    assert alt_or([False, False, False, True, False]) is True
    assert alt_or([False, False, False, True, True]) is True
    assert alt_or([False, False, True, False, False]) is True
    assert alt_or([False, False, True, False, True]) is True
    assert alt_or([False, False, True, True, False]) is True
    assert alt_or([False, False, True, True, True]) is True
    assert alt_or([False, True, False, False, False]) is True
    assert alt_or([False, True, False, False, True]) is True
    assert alt_or([False, True, False, True, False]) is True
    assert alt_or([False, True, False, True, True]) is True
    assert alt_or([False, True, True, False, False]) is True
    assert alt_or([False, True, True, False, True]) is True
    assert alt_or([False, True, True, True, False]) is True
    assert alt_or([False, True, True, True, True]) is True
    assert alt_or([True, False, False, False, False]) is True
    assert alt_or([True, False, False, False, True]) is True
    assert alt_or([True, False, False, True, False]) is True
    assert alt_or([True, False, False, True, True]) is True
    assert alt_or([True, False, True, False, False]) is True
    assert alt_or([True, False, True, False, True]) is True
    assert alt_or([True, False, True, True, False]) is True
    assert alt_or([True, False, True, True, True]) is True
    assert alt_or([True, True, False, False, False]) is True
    assert alt_or([True, True, False, False, True]) is True
    assert alt_or([True, True, False, True, False]) is True
    assert alt_or([True, True, False, True, True]) is True
    assert alt_or([True, True, True, False, False]) is True
    assert alt_or([True, True, True, False, True]) is True
    assert alt_or([True, True, True, True, False]) is True
    assert alt_or([True, True, True, True, True]) is True
    assert alt_or([False, False, False, False]) is False
    assert alt_or([False, False, False, True]) is True
    assert alt_or([False, False, True, False]) is True
    assert alt_or([False, False, True, True]) is True
    assert alt_or([False, True, False, False]) is True
    assert alt_or([False, True, False, True]) is True
    assert alt_or([False, True, True, False]) is True
    assert alt_or([False, True, True, True]) is True
    assert alt_or([True, False, False, False]) is True
    assert alt_or([True, False, False, True]) is True
    assert alt_or([True, False, True, False]) is True
    assert alt_or([True, False, True, True]) is True
    assert alt_or([True, True, False, False]) is True
    assert alt_or([True, True, False, True]) is True
    assert alt_or([True, True, True, False]) is True
    assert alt_or([True, True, True, True]) is True
    assert alt_or([False, False, False]) is False
    assert alt_or([False, False, True]) is True
    assert alt_or([False, True, False]) is True
    assert alt_or([False, True, True]) is True
    assert alt_or([True, False, False]) is True
    assert alt_or([True, False, True]) is True
    assert alt_or([True, True, False]) is True
    assert alt_or([True, True, True]) is True
    assert alt_or([False, False]) is False
    assert alt_or([False, True]) is True
    assert alt_or([True, False]) is True
    assert alt_or([True, True]) is True
    assert alt_or([False]) is False
    assert alt_or([True]) is True
