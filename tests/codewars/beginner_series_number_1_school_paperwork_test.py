"""
Unit tests for Beginner Series #1: School Paperwork challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.beginner_series_number_1_school_paperwork import paperwork


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(5, 5, 25), (5, -5, 0), (-5, -5, 0), (-5, 5, 0), (5, 0, 0)])
def test_paperwork(test_input1, test_input2, expected):
    """
    Tests the Beginner Series #1: School Paperwork solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert paperwork(test_input1, test_input2) == expected
