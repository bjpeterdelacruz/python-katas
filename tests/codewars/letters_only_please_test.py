"""
Unit tests for Letters Only, Please challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.letters_only_please import remove_chars


@pytest.mark.parametrize("test_input, expected",
                         [('co_ol f0un%(c)t-io"n', "cool function"),
                          ("test for error!", "test for error"),
                          (".tree1", 'tree'),
                          ("that's a pie&ce o_f p#ie!", 'thats a piece of pie')])
def test_remove_chars(test_input, expected):
    """
    Tests the Letters Only, Please solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove_chars(test_input) == expected
