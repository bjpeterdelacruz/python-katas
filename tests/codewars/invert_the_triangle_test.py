"""
Unit tests for Invert the Triangle challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.invert_the_triangle import invert_triangle

# pylint: disable=trailing-whitespace


def test_invert_triangle():
    """
    Tests the Invert the Triangle solution.

    :return: None
    """
    triangle = """#########  #########
########    ########
#######      #######
######        ######
#####          #####
####            ####
###              ###
##                ##
#                  #"""

    actual = invert_triangle(triangle)
    expected = """ ################## 
  ################  
   ##############   
    ############    
     ##########     
      ########      
       ######       
        ####        
         ##         """

    assert actual == expected

    triangle = """    #    
   ###   
  #####  
 ####### 
#########"""

    actual = invert_triangle(triangle)
    expected = """         
#       #
##     ##
###   ###
#### ####"""

    assert actual == expected
