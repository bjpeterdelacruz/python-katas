"""
Unit tests for Separate Basic Types challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.separate_basic_types import separate_types


@pytest.mark.parametrize("test_input, expected",
                         [(['a', 1, 2, False, 'b'],
                           {int: [1, 2],  str: ['a', 'b'], bool: [False]})])
def test_separate_types(test_input, expected):
    """
    Tests the Separate Basic Types solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert separate_types(test_input) == expected
