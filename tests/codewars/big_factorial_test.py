"""
Unit tests for Big Factorial challenge

:Author: BJ Peter Dela Cruz
"""
import sys
from codewars.big_factorial import factorial


def test_factorial():
    """
    Tests the Big Factorial solution.

    :return: None
    """
    # Limiting integer string conversion length offers a practical way to avoid CVE-2020-10735.
    # Setting maxdigits to zero disables this limitation. For more information, refer to this page:
    # https://docs.python.org/3/library/stdtypes.html#integer-string-conversion-length-limitation
    sys.set_int_max_str_digits(maxdigits=0)
    with open("tests/codewars/big_factorial/test_data.txt", "r", encoding="utf-8") as file:
        lines = file.readlines()
    for line in lines:
        test_input, expected = line.strip().split(",")
        assert factorial(int(test_input)) == int(expected)
    assert factorial(-25) is None
    # Reset back to default value of 4300.
    sys.set_int_max_str_digits(maxdigits=4300)
