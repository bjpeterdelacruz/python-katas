"""
Unit tests for Sum of Pairs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_of_pairs import sum_pairs

l9 = [1] * 10000000
l9[(len(l9) // 2) - 1] = 6
l9[(len(l9) // 2)] = 7
l9[len(l9) - 2] = 8
l9[len(l9) - 1] = -3


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([l9, 13, [6, 7]]), ([1], 13, None), ([20, -13, 40], 17, None)])
def test_sum_pairs(test_input1, test_input2, expected):
    """
    Tests the Sum of Pairs solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_pairs(test_input1, test_input2) == expected
