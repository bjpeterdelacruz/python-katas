"""
Unit tests for Find the Second Largest Integer in Array challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_the_2nd_largest_integer_in_array import find_2nd_largest


@pytest.mark.parametrize("test_input, expected",
                         [([1, 1, 1, 1, 1], None), ([1, 2, 1, 2, 1], 1),
                          (['a', 'b', 'c', 1, 2.3], None), ([6, 4, 5, 3, 1, 2], 5)])
def test_find_2nd_largest(test_input, expected):
    """
    Tests the Find the Second Largest Integer in Array solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_2nd_largest(test_input) == expected
