"""
Unit tests for Simple Encryption #1: Alternating Split challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_encryption_number_1_alternating_split import encrypt, decrypt


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("This is a test!", 0, "This is a test!"),
                          ("This is a test!", 1, "hsi  etTi sats!"),
                          ("This is a test!", 2, "s eT ashi tist!"),
                          ("This is a test!", 3, " Tah itse sits!"),
                          ("This is a test!", 4, "This is a test!"),
                          ("This is a test!", -1, "This is a test!"),
                          ("This kata is very interesting!", 1, "hskt svr neetn!Ti aai eyitrsig"),
                          ("", 0, ""),
                          (None, 0, None)])
def test_encrypt(test_input1, test_input2, expected):
    """
    Tests the encryption process of the Simple Encryption #1: Alternating Split solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert encrypt(test_input1, test_input2) == expected


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("This is a test!", 0, "This is a test!"),
                          ("hsi  etTi sats!", 1, "This is a test!"),
                          ("s eT ashi tist!", 2, "This is a test!"),
                          (" Tah itse sits!", 3, "This is a test!"),
                          ("This is a test!", 4, "This is a test!"),
                          ("This is a test!", -1, "This is a test!"),
                          ("hskt svr neetn!Ti aai eyitrsig", 1, "This kata is very interesting!"),
                          ("", 0, ""),
                          (None, 0, None)])
def test_decrypt(test_input1, test_input2, expected):
    """
    Tests the decryption process of the Simple Encryption #1: Alternating Split solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert decrypt(test_input1, test_input2) == expected
