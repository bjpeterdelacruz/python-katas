"""
Unit tests for Next Perfect Square challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.nextperfectsquare import next_perfect_square


@pytest.mark.parametrize("test_input, expected",
                         [(6, 9), (36, 49), (0, 1), (-5, 0), (-100, 0)])
def test_next_perfect_square(test_input, expected):
    """
    Tests the Next Perfect Square solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert next_perfect_square(test_input) == expected
