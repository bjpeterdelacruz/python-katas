"""
Unit tests for Sum Even Fibonacci Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_even_fibonacci_numbers import sum_even_fibonacci


@pytest.mark.parametrize("test_input, expected",
                         [(0, 0), (1, 0), (2, 2), (7, 2), (8, 10), (111111, 60696),
                          (123456789000000, 154030760585064)])
def test_sum_even_fibonacci(test_input, expected):
    """
    Tests the Sum Even Fibonacci Numbers solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_even_fibonacci(test_input) == expected
