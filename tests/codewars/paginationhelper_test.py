"""
Unit tests for PaginationHelper challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.paginationhelper import PaginationHelper


def test_pagination_helper1():
    """
    Tests the PaginationHelper solution.

    :return: None
    """
    helper = PaginationHelper(['a', 'b', 'c', 'd', 'e', 'f'], 4)
    assert helper.page_count() == 2
    assert helper.item_count() == 6
    assert helper.page_item_count(-1) == -1
    assert helper.page_item_count(0) == 4
    assert helper.page_item_count(1) == 2
    assert helper.page_item_count(2) == -1

    assert helper.page_index(5) == 1
    assert helper.page_index(2) == 0
    assert helper.page_index(20) == -1
    assert helper.page_index(-10) == -1


def test_pagination_helper2():
    """
    Tests the PaginationHelper solution.

    :return: None
    """
    helper = PaginationHelper(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'], 2)
    assert helper.page_count() == 4
    assert helper.item_count() == 8
    assert helper.page_item_count(-1) == -1
    assert helper.page_item_count(0) == 2
    assert helper.page_item_count(1) == 2
    assert helper.page_item_count(2) == 2
    assert helper.page_item_count(3) == 2
    assert helper.page_item_count(4) == -1

    assert helper.page_index(5) == 2
    assert helper.page_index(2) == 1
