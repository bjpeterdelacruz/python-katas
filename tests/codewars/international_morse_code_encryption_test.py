"""
Unit tests for International Morse Code Encryption challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.international_morse_code_encryption import encryption


@pytest.mark.parametrize("test_input, expected",
                         [('RNDEbu7LLvX', '.-. -. -.. . -... ..- --... .-.. .-.. ...- -..-'),
                          ('QcXQUoym', '--.- -.-. -..- --.- ..- --- -.-- --'),
                          ('QcX QUoym', '--.- -.-. -..-   --.- ..- --- -.-- --'),
                          ('QcX  QUoym', '--.- -.-. -..-     --.- ..- --- -.-- --'),
                          ('QcX   QUoym', '--.- -.-. -..-       --.- ..- --- -.-- --'),
                          ("HELLO WORLD", ".... . .-.. .-.. ---   .-- --- .-. .-.. -.."),
                          ("SOS", "... --- ..."),
                          ("1836", ".---- ---.. ...-- -...."),
                          ("THE QUICK BROWN FOX",
                           "- .... .   --.- ..- .. -.-. -.-   -... .-. --- .-- -.   ..-. --- -..-"),
                          ("JUMPED OVER THE", ".--- ..- -- .--. . -..   --- ...- . .-.   - .... ."),
                          ("LAZY DOG", ".-.. .- --.. -.--   -.. --- --."),
                          ("WOLFRAM ALPHA 1",
                           ".-- --- .-.. ..-. .-. .- --   .- .-.. .--. .... .-   .----"),
                          (
                          "CodeWars Rocks", "-.-. --- -.. . .-- .- .-. ...   .-. --- -.-. -.- ..."),
                          ("", ""),
                          ("Final basic test",
                           "..-. .. -. .- .-..   -... .- ... .. -.-.   - . ... -")
                          ])
def test_encryption(test_input, expected):
    """
    Tests the International Morse Code Encryption solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert encryption(test_input) == expected
