"""
Unit tests for Penultimate challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.penultimate import penultimate


@pytest.mark.parametrize("test_input, expected",
                         [("Python is fun", "u"), ([1, 2, 3, 4], 3)])
def test_penultimate(test_input, expected):
    """
    Tests the Penultimate solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert penultimate(test_input) == expected
