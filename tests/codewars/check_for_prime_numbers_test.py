"""
Unit tests for Check for Prime Numbers challenge

:Author: BJ Peter Dela Cruz
"""
from random import randint
import pytest
from codewars.check_for_prime_numbers import is_prime


@pytest.mark.parametrize("test_input, expected",
                         [(0, False), (1, False), (2, True), (3, True), (11, True), (12, False),
                          (61, True), (571, True), (573, False), (25, False), (5, True)])
def test_is_prime(test_input, expected):
    """
    Tests the Check for Prime Numbers solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_prime(test_input) == expected


def sol(num):
    """
    Kata author's solution

    :param num: Number to test whether it is prime
    :return: True if the number is prime, False otherwise
    """
    return num == 2 or num % 2 and num > 2 and all(
        num % curr for curr in range(3, int(num ** .5) + 1, 2))


def test_is_prime2():
    """
    Tests the Check for Prime Numbers solution.

    :return: None
    """
    for _ in range(250):
        num = randint(0, 10000)
        assert is_prime(num) == sol(num)
