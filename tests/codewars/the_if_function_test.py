"""
Unit tests for The 'if' Function challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.the_if_function import _if


def test_func1():
    """
    Tests the _if function using a falsy value.

    :return: None
    """
    func1_value = func2_value = False

    def func1():
        """
        Sets a flag that this function was called to True.

        :return: None
        """
        nonlocal func1_value
        func1_value = True

    def func2():
        """
        Sets a flag that this function was called to True.

        :return: None
        """
        nonlocal func2_value
        func2_value = True

    _if(True, func1, func2)
    assert func1_value is True
    assert func2_value is False


def test_func2():
    """
    Tests the _if function using a falsy value.

    :return: None
    """
    func1_value = func2_value = False

    def func1():
        """
        Sets a flag that this function was called to True.

        :return: None
        """
        nonlocal func1_value
        func1_value = True

    def func2():
        """
        Sets a flag that this function was called to True.

        :return: None
        """
        nonlocal func2_value
        func2_value = True

    _if(False, func1, func2)
    assert func1_value is False
    assert func2_value is True
