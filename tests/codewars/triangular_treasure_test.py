"""
Unit tests for Triangular Treasure challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.triangular_treasure import triangular


@pytest.mark.parametrize("test_input, expected",
                         [(2, 3),
                          (7, 28),
                          (12, 78),
                          (25, 325),
                          (50, 1275),
                          (1000, 500500),
                          (5000, 12502500),
                          (10000, 50005000),
                          (0, 0),
                          (-1, 0),
                          (-5, 0)])
def test_triangular(test_input, expected):
    """
    Tests the Triangular Treasure solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert triangular(test_input) == expected
