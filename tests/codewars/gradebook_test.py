"""
Unit tests for Gradebook challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.gradebook import get_grade


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(95, 90, 93, "A"),
                          (100, 85, 96, "A"),
                          (92, 93, 94, "A"),
                          (100, 100, 100, "A"),
                          (70, 70, 100, "B"),
                          (82, 85, 87, "B"),
                          (84, 79, 85, "B"),
                          (70, 70, 70, "C"),
                          (75, 70, 79, "C"),
                          (60, 82, 76, "C"),
                          (65, 70, 59, "D"),
                          (66, 62, 68, "D"),
                          (58, 62, 70, "D"),
                          (44, 55, 52, "F"),
                          (48, 55, 52, "F"),
                          (58, 59, 60, "F"),
                          (0, 0, 0, "F")])
def test_get_grade(test_input1, test_input2, test_input3, expected):
    """
    Tests the Gradebook solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_grade(test_input1, test_input2, test_input3) == expected
