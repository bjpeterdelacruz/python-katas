"""
Unit tests for Number Manipulation 1 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.number_manipulation_i_easy import manipulate


@pytest.mark.parametrize("test_input, expected",
                         [(321, 300), (4298, 4200), (12, 10), (98765432, 98760000)])
def test_manipulate(test_input, expected):
    """
    Tests the Number Manipulation 1 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert manipulate(test_input) == expected
