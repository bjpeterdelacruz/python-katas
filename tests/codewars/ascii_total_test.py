"""
Unit tests for ASCII Total challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.ascii_total import uni_total


@pytest.mark.parametrize("test_input, expected",
                         [("a", 97),
                          ("b", 98),
                          ("c", 99),
                          ("", 0),
                          ("aaa", 291),
                          ("abc", 294),
                          ("Mary Had A Little Lamb", 1873),
                          ("Mary had a little lamb", 2001),
                          ("CodeWars rocks", 1370),
                          ("And so does Strive", 1661)])
def test_uni_total(test_input, expected):
    """
    Tests the ASCII Total solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert uni_total(test_input) == expected
