"""
Unit tests for Scrolling Text challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.scrolling_text import scrolling_text


@pytest.mark.parametrize("test_input, expected",
                         [("codewars",
                           ['CODEWARS', 'ODEWARSC', 'DEWARSCO', 'EWARSCOD', 'WARSCODE', 'ARSCODEW',
                            'RSCODEWA', 'SCODEWAR']),
                          ("Hello world!", ['HELLO WORLD!', 'ELLO WORLD!H', 'LLO WORLD!HE',
                                            'LO WORLD!HEL', 'O WORLD!HELL', ' WORLD!HELLO',
                                            'WORLD!HELLO ', 'ORLD!HELLO W', 'RLD!HELLO WO',
                                            'LD!HELLO WOR', 'D!HELLO WORL', '!HELLO WORLD']),
                          ("123456789", ['123456789', '234567891', '345678912', '456789123',
                                         '567891234', '678912345', '789123456', '891234567',
                                         '912345678'])])
def test_scrolling_text(test_input, expected):
    """
    Tests the Scrolling Text solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert scrolling_text(test_input) == expected
