"""
Unit tests for Define a Card Suit challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.define_a_card_suit import define_suit


@pytest.mark.parametrize("test_input, expected",
                         [("3C", "clubs"), ("JD", "diamonds"), ("8S", "spades"), ("10H", "hearts")])
def test_define_suit(test_input, expected):
    """
    Tests the Define a Card Suit solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert define_suit(test_input) == expected
