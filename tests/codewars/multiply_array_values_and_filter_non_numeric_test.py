"""
Unit tests for Multiply and Filter challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.multiply_array_values_and_filter_non_numeric import multiply_and_filter


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3, 4], 1.5, [1.5, 3, 4.5, 6]),
                          ([1, 2, 3], 0, [0, 0, 0]),
                          ([0, 0, 0], 2, [0, 0, 0]),
                          (
                          [1, None, lambda x: x, 2.5, 'string', 10, None, {}, []], 3, [3, 7.5, 30]),
                          ([1, None, lambda x: x, 2.5, 'string', 10, None, {}, [], True, False], 3,
                           [3, 7.5, 30])])
def test_multiply_and_filter(test_input1, test_input2, expected):
    """
    Tests the Multiply and Filter solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert multiply_and_filter(test_input1, test_input2) == expected
