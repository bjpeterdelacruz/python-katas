"""
Unit tests for Sum of Intervals challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_of_intervals import sum_of_intervals


@pytest.mark.parametrize("test_input, expected",
                         [([(1, 5)], 4), ([(1, 5), (6, 10)], 8), ([(1, 5), (1, 5)], 4),
                          ([(1, 4), (7, 10), (3, 5)], 7), ([(1, 5), (5, 10)], 9),
                          ([(6, 8), (1, 10), (4, 7), (3, 5)], 9),
                          ([(6, 8), (1, 10), (4, 7), (3, 11)], 10)])
def test_sum_of_intervals(test_input, expected):
    """
    Tests the Sum of Intervals solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_of_intervals(test_input) == expected
