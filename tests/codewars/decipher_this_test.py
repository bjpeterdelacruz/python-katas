"""
Unit tests for Decipher This! challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.decipher_this import decipher_this


@pytest.mark.parametrize("test_input, expected",
                         [
                             ("65 119esi 111dl 111lw 108dvei 105n 97n 111ka",
                              "A wise old owl lived in an oak"),
                             ("84eh 109ero 104e 115wa 116eh 108sse 104e 115eokp",
                              "The more he saw the less he spoke"),
                             ("84eh 108sse 104e 115eokp 116eh 109ero 104e 104dare",
                              "The less he spoke the more he heard"),
                             ("87yh 99na 119e 110to 97ll 98e 108eki 116tah 119esi 111dl 98dri",
                              "Why can we not all be like that wise old bird"),
                             ("84kanh 121uo 80roti 102ro 97ll 121ruo 104ple",
                              "Thank you Piotr for all your help")
                         ])
def test_decipher_this(test_input, expected):
    """
    Tests the Decipher This! solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert decipher_this(test_input) == expected
