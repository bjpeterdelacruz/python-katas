"""
Unit tests for Milk and Cookies for Santa challenge

:Author: BJ Peter Dela Cruz
"""
from datetime import date

import pytest
from codewars.milk_and_cookies_for_santa import time_for_milk_and_cookies


@pytest.mark.parametrize("test_input, expected",
                         [(date(2021, 12, 24), True), (date(2019, 10, 28), False),
                          (date(2072, 12, 24), True), (date(2029, 3, 17), False)])
def test_time_for_milk_and_cookies(test_input, expected):
    """
    Tests the Milk and Cookies for Santa solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert time_for_milk_and_cookies(test_input) == expected
