"""
Unit tests for Get Character from ASCII Value challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.get_character_from_ascii_value import get_char


@pytest.mark.parametrize("test_input, expected",
                         [(65, 'A'), (33, '!'), (34, '"'), (35, '#'), (36, '$'), (37, '%'),
                          (38, '&')])
def test_get_char(test_input, expected):
    """
    Tests the Get Character from ASCII Value solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_char(test_input) == expected
