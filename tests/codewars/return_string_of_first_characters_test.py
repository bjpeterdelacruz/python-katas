"""
Unit tests for Return String of First Characters challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.return_string_of_first_characters import make_string


@pytest.mark.parametrize("test_input, expected",
                         [("The quick brown fox", "Tqbf"), ("", ""), ("T", "T"), ("The", "T")])
def test_make_string(test_input, expected):
    """
    Tests the Return String of First Characters solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert make_string(test_input) == expected
