"""
Unit tests for Duplicate Sandwich challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.duplicate_sandwich import duplicate_sandwich


@pytest.mark.parametrize("test_input, expected",
                         [([0, 1, 2, 3, 4, 5, 6, 1, 7, 8], [2, 3, 4, 5, 6]),
                          (['None', 'Hello', 'Example', 'hello', 'None', 'Extra'],
                           ['Hello', 'Example', 'hello']),
                          ([0, 0], []),
                          ([True, False, True], [False]),
                          (['e', 'x', 'a', 'm', 'p', 'l', 'e'],
                           ['x', 'a', 'm', 'p', 'l'])])
def test_duplicate_sandwich(test_input, expected):
    """
    Tests the Duplicate Sandwich solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert duplicate_sandwich(test_input) == expected
