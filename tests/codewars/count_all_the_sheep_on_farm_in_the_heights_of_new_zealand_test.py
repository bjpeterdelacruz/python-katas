"""
Unit tests for Count All the Sheep on Farm in the Heights of New Zealand challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.count_all_the_sheep_on_farm_in_the_heights_of_new_zealand import lost_sheep


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [([1, 2], [3, 4], 15, 5),
                          ([3, 1, 2], [4, 5], 21, 6),
                          ([5, 1, 4], [5, 4], 29, 10),
                          ([11, 23, 3, 4, 15], [7, 14, 9, 21, 15], 300, 178),
                          ([2, 7, 13, 17], [23, 56, 44, 12, 1, 2, 1, ], 255, 77),
                          ([2, 5, 8], [11, 23, 3, 4, 15, 112, 12, 4], 355, 156),
                          ([1, 1, 1, 2, 1, 2], [2, 1, 2, 1, 2, 1], 30, 13),
                          ([5, 10, 15], [11, 23, 3, 4, 15], 89, 3),
                          ([3, 6, 9, 12], [3, 2, 1, 2, 3, 1], 44, 2)])
def test_lost_sheep(test_input1, test_input2, test_input3, expected):
    """
    Tests the Count All the Sheep on Farm in the Heights of New Zealand solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert lost_sheep(test_input1, test_input2, test_input3) == expected
