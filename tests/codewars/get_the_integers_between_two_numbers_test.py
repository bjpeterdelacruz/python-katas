"""
Unit tests for Get Integers Between Two Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.get_the_integers_between_two_numbers import function


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(2, 6, [3, 4, 5]), (3, 3, []), (4, 2, [])])
def test_function(test_input1, test_input2, expected):
    """
    Tests the Get Integers Between Two Numbers solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert function(test_input1, test_input2) == expected
