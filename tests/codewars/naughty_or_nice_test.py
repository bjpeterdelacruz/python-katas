"""
Unit tests for Naughty or Nice challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.naughty_or_nice import get_naughty_names, get_nice_names


naughty = [{'name': 'xDranik', 'was_nice': False}]
nice = [{'name': 'Santa', 'was_nice': True},
        {'name': 'Warrior reading this kata', 'was_nice': True}]


def test_get_naughty_names():
    """
    Tests the get_naughty_names function for the Naughty or Nice solution.

    :return: None
    """
    assert get_naughty_names(nice) == []
    assert get_naughty_names(naughty) == ['xDranik']


def test_get_nice_names():
    """
    Tests the get_nice_names function for the Naughty or Nice solution.

    :return: None
    """
    assert get_nice_names(nice) == ['Santa', 'Warrior reading this kata']
    assert get_nice_names(naughty) == []
