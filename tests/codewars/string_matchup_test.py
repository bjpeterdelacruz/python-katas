"""
Unit tests for String Matchup challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.string_matchup import solve


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(['abc', 'abc', 'xyz', 'abcd', 'cde'], ['abc', 'cde', 'uap'], [2, 1, 0]),
                          (['abc', 'xyz', 'abc', 'xyz', 'cde'], ['abc', 'cde', 'xyz'], [2, 1, 2]),
                          (['quick', 'brown', 'fox', 'is', 'quick'], ['quick', 'abc', 'fox'],
                           [2, 0, 1])])
def test_solve(test_input1, test_input2, expected):
    """
    Tests the String Matchup solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert solve(test_input1, test_input2) == expected
