"""
Unit tests for Middle Me challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.middle_me import middle_me


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(3, "a", "b", "a"), (2, "a", "b", "bab"), (10, "c", "k", "kkkkkckkkkk")])
def test_middle_me(test_input1, test_input2, test_input3, expected):
    """
    Tests the Middle Me solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert middle_me(test_input1, test_input2, test_input3) == expected
