"""
Unit tests for Return Two Highest Values in List challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.return_two_highest_values_in_list import two_highest


@pytest.mark.parametrize("test_input, expected",
                         [([], []), ([15], [15]), ([15, 20, 20, 17], [20, 17])])
def test_two_highest(test_input, expected):
    """
    Tests the Return Two Highest Values in List solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert two_highest(test_input) == expected
