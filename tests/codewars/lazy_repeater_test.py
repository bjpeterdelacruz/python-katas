"""
Unit tests for Lazy Repeater challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.lazy_repeater import make_looper


def test_make_looper():
    """
    Tests the Lazy Repeater solution.

    :return: None
    """
    abc = make_looper("abc")
    assert abc() == "a"
    assert abc() == "b"
    assert abc() == "c"
    assert abc() == "a"
    assert abc() == "b"
    assert abc() == "c"
    assert abc() == "a"
    assert abc() == "b"
    assert abc() == "c"
    assert abc() == "a"
