"""
Unit tests for Caesar Encryption challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.caeser_encryption import caesar


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("This is a message", 0, "THIS IS A MESSAGE"),
                          ("who are you?", 18, "OZG SJW QGM?"),
                          ("..5tyu..", 25, "..5SXT.."),
                          ("..#$%^..", 0, "..#$%^.."),
                          ("..#$%^..", 26, "..#$%^.."),
                          ("final one", 9, "ORWJU XWN")])
def test_caesar(test_input1, test_input2, expected):
    """
    Tests the Caesar Encryption solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert caesar(test_input1, test_input2) == expected
