"""
Unit tests for Fun with Lists: Length challenge

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-many-function-args
from codewars.fun_with_lists_length import Node, length


def test_length():
    """
    Tests the Formatting Fun with Lists: Length solution.

    :return: None
    """
    assert length(None) == 0
    assert length(Node(5)) == 1
    assert length(Node(1, Node(2, Node(3, Node(4))))) == 4
