"""
Unit tests for Aerial Firefighting challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.aerial_firefighting import waterbombs


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("xxxxxYxYx", 2, 5), ("xxYxx", 3, 2), ("xxYxx", 1, 4),
                          ("YYxxYxxYxYYxxYxxxx", 3, 6)])
def test_waterbombs(test_input1, test_input2, expected):
    """
    Tests the Aerial Firefighting solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert waterbombs(test_input1, test_input2) == expected
