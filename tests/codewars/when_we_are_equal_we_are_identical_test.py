"""
Unit tests for Make Identical challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.when_we_are_equal_we_are_identical import make_identical

# pylint: disable=literal-comparison


def test_make_identical():
    """
    Tests the Make Identical solution.

    :return: None
    """
    assert make_identical("hello"[1:4]) is "ell"
    assert make_identical("hello"[1:5]) is not "ell"
