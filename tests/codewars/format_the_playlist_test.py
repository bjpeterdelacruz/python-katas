"""
Unit tests for Format the Playlist challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.format_the_playlist import format_playlist


@pytest.mark.parametrize("test_input, expected",
                         [([
                               ('In Da Club', '3:13', '50 Cent'),
                               ('Candy Shop', '3:45', '50 Cent'),
                               ('One', '4:36', 'U2'),
                               ('Wicked', '2:53', 'Future'),
                               ('Cellular', '2:58', 'King Krule'),
                               ('The Birthday Party', '4:45', 'The 1975'),
                               ('In The Night Of Wilderness', '5:26', 'Blackmill'),
                               ('Pull Up', '3:35', 'Playboi Carti'),
                               ('Cudi Montage', '3:16', 'KIDS SEE GHOSTS'),
                               ('What Up Gangsta', '2:58', '50 Cent')],
                           """+----------------------------+------+-----------------+
| Name                       | Time | Artist          |
+----------------------------+------+-----------------+
| Candy Shop                 | 3:45 | 50 Cent         |
| In Da Club                 | 3:13 | 50 Cent         |
| What Up Gangsta            | 2:58 | 50 Cent         |
| In The Night Of Wilderness | 5:26 | Blackmill       |
| Wicked                     | 2:53 | Future          |
| Cudi Montage               | 3:16 | KIDS SEE GHOSTS |
| Cellular                   | 2:58 | King Krule      |
| Pull Up                    | 3:35 | Playboi Carti   |
| The Birthday Party         | 4:45 | The 1975        |
| One                        | 4:36 | U2              |
+----------------------------+------+-----------------+"""), ([],
                                                              """+------+------+--------+
| Name | Time | Artist |
+------+------+--------+"""
                                                              )])
def test_format_playlist(test_input, expected):
    """
    Tests the Format the Playlist solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert format_playlist(test_input) == expected
