"""
Unit tests for Array Info challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.array_info import array_info


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 3.33, 4, 5.01, 'bass', 'kick', ' '], [[8], [3], [2], [2], [1]]),
                          ([0.001, 2, ' '], [[3], [1], [1], [None], [1]]),
                          ([], 'Nothing in the array!'),
                          ([' '], [[1], [None], [None], [None], [1]]),
                          ([' ', ' '], [[2], [None], [None], [None], [2]]),
                          (['jazz'], [[1], [None], [None], [1], [None]]),
                          ([4], [[1], [1], [None], [None], [None]]),
                          ([3.1416], [[1], [None], [1], [None], [None]]),
                          ([4], [[1], [1], [None], [None], [None]]),
                          ([11, 22, 33.33, 44.44, 'hasan', 'ahmad'], [[6], [2], [2], [2], [None]]),
                          (['a', 'b', 'c', 'd', ' '], [[5], [None], [None], [4], [1]]),
                          ([1, 2, 3, 4, 5, 6, 7, 8, 9], [[9], [9], [None], [None], [None]]),
                          ([1, 2.23, 'string', ' '], [[4], [1], [1], [1], [1]]),
                          ([' ', ' '], [[2], [None], [None], [None], [2]])])
def test_array_info(test_input, expected):
    """
    Tests the Array Info solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert array_info(test_input) == expected
