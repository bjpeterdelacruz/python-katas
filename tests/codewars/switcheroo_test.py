"""
Unit tests for Switcheroo challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.switcheroo import switcheroo


@pytest.mark.parametrize("test_input, expected",
                         [("abc", "bac"), ("aaabcccbaaa", "bbbacccabbb"), ("ccccc", "ccccc"),
                          ("abababababababab", "babababababababa"), ("aaaaa", "bbbbb")])
def test_switcheroo(test_input, expected):
    """
    Tests the Switcheroo solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert switcheroo(test_input) == expected
