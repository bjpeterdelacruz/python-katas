"""
Unit tests for Slope of a Line challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.slope_of_a_line import get_slope


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2], [1, 3], None),
                          ([3, 4], [3, 4], None),
                          ([2, 1], [3, 2], 1), ([4, 8], [1, 2], 2.0)
                          ])
def test_get_slope(test_input1, test_input2, expected):
    """
    Tests the Slope of a Line solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_slope(test_input1, test_input2) == expected
