"""
Unit tests for Thinking and Testing: Something Capitalized challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinking_and_testing_something_capitalized import something_capitalized


@pytest.mark.parametrize("test_input, expected",
                         [("", ""), ("a", "A"), ("b", "B"), ("a a", "A A"), ("a b", "A B"),
                          ("a b c", "A B C"), ("aa", "aA"), ("ab ab", "aB aB"),
                          ("this is a string", "thiS iS A strinG")])
def test_something_capitalized(test_input, expected):
    """
    Tests the Thinking and Testing: Something Capitalized solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert something_capitalized(test_input) == expected
