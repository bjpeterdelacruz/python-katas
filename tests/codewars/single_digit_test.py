"""
Unit tests for Single Digit challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.single_digit import single_digit


def test_single_digit():
    """
    Tests the Single Digit solution.

    :return: None
    """
    assert single_digit(5) == 5
    assert single_digit(999) == 8
    assert single_digit(1234444123) == 1
    assert single_digit(443566) == 2
    assert single_digit(565656565) == 3
    assert single_digit(4868872) == 8
    assert single_digit(234234235) == 2
    assert single_digit(567448) == 7
    assert single_digit(10000000000) == 3
