"""
Unit tests for Binary Representation challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.binary_representation_of_an_integer import show_bits


@pytest.mark.parametrize("test_input, expected",
                         [(701,
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
                            1, 0, 1, 1, 1, 1, 0, 1]),
                          (-245,
                           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            0, 0, 0, 0, 1, 0, 1, 1]),
                          (12336,
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
                            0, 0, 1, 1, 0, 0, 0, 0])])
def test_show_bits(test_input, expected):
    """
    Tests the Binary Representation solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert show_bits(test_input) == expected
