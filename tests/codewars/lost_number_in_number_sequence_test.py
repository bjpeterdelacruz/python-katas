"""
Unit tests for Lost Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.lost_number_in_number_sequence import find_deleted_number


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3, 4, 5, 6, 7, 8, 9], [5, 7, 9, 4, 8, 1, 2, 3], 6),
                          ([1, 2, 3, 4, 5, 6, 7], [2, 3, 6, 1, 5, 4, 7], 0),
                          ([1, 2, 3, 4, 5, 6, 7, 8, 9], [5, 7, 6, 9, 4, 8, 1, 2, 3], 0),
                          ([1], [], 1), ([], [], 0)])
def test_find_deleted_number(test_input1, test_input2, expected):
    """
    Tests the Lost Number solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_deleted_number(test_input1, test_input2) == expected
