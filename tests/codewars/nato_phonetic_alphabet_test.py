"""
Unit tests for NATO Phonetic Alphabet challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.nato_phonetic_alphabet import nato


@pytest.mark.parametrize("test_input, expected",
                         [("Hello", "Hotel Echo Lima Lima Oscar"),
                          ("GOOD", "Golf Oscar Oscar Delta"),
                          ("love", "Lima Oscar Victor Echo")])
def test_nato(test_input, expected):
    """
    Tests the NATO Phonetic Alphabet solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert nato(test_input) == expected
