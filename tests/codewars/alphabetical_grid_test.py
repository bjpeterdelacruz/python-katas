"""
Unit tests for Alphabetical Grid challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.alphabetical_grid import grid


GRID_4 = """a b c d
b c d e
c d e f
d e f g"""

GRID_10 = """a b c d e f g h i j
b c d e f g h i j k
c d e f g h i j k l
d e f g h i j k l m
e f g h i j k l m n
f g h i j k l m n o
g h i j k l m n o p
h i j k l m n o p q
i j k l m n o p q r
j k l m n o p q r s"""


def test_grid():
    """
    Tests the Alphabet Grid solution.

    :return: None
    """
    assert grid(4) == GRID_4
    assert grid(10) == GRID_10
    assert grid(-1) is None
