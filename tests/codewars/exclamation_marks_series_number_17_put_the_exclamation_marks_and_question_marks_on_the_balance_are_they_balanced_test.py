"""
Unit tests for Exclamation Marks Series #17 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_17_put_the_exclamation_marks_and_question_marks_on_the_balance_are_they_balanced import balance  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('!!???', '?!?!?', 'Balance'), ('!!!', '???', 'Right'),
                          ('!!', '?', 'Left'), ('', '', 'Balance'), ('!?!', '??!', 'Right'),
                          ('!!!!!', '?!?', 'Left')])
def test_balance(test_input1, test_input2, expected):
    """
    Tests the Exclamation Marks Series #17 solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert balance(test_input1, test_input2) == expected
