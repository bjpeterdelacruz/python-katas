"""
Unit tests for Well of Ideas: Harder Version challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.well_of_ideas_harder_version import well


@pytest.mark.parametrize("test_input, expected",
                         [([['bad', 'bAd', 'bad'], ['bad', 'bAd', 'bad'], ['bad', 'bAd', 'bad']],
                           "Fail!"),
                          ([['gOOd', 'bad', 'BAD', 'bad', 'bad'], ['bad', 'bAd', 'bad'],
                            ['GOOD', 'bad', 'bad', 'bAd']], "Publish!"),
                          ([['gOOd', 'bAd', 'BAD', 'bad', 'bad', 'GOOD'], ['bad'], ['gOOd', 'BAD']],
                           "I smell a series!")])
def test_well(test_input, expected):
    """
    Tests the Well of Ideas: Harder Version solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert well(test_input) == expected
