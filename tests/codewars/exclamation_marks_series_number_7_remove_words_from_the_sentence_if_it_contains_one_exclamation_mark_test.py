"""
Unit tests for Exclamation Marks Series #7 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_7_remove_words_from_the_sentence_if_it_contains_one_exclamation_mark import remove  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [('This is! is an!! exam!ple hello !world sentence! !!!world!!',
                           'This is an!! hello !!!world!!')])
def test_remove(test_input, expected):
    """
    Tests the Exclamation Marks Series #7 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove(test_input) == expected
