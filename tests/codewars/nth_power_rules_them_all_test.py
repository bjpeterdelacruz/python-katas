"""
Unit tests for Nth Power Rules Them All challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.nth_power_rules_them_all import modified_sum


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3], 3, 30),
                          ([1, 2], 5, 30),
                          ([3, 5, 7], 2, 68),
                          ([1, 2, 3, 4, 5], 3, 210),
                          ([2, 7, 13, 17], 2, 472),
                          ([2, 5, 8], 3, 630),
                          ([2, 4, 6, 8], 6, 312940),
                          ([5, 10, 15], 4, 61220),
                          ([3, 6, 9, 12], 3, 2670)])
def test_modified_sum(test_input1, test_input2, expected):
    """
    Tests the Nth Power Rules Them All solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert modified_sum(test_input1, test_input2) == expected
