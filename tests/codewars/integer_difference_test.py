"""
Unit tests for Format to the 2nd challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.integer_difference import int_diff


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 1, 5, 6, 9, 16, 27], 4, 3),
                          ([1, 1, 3, 3], 2, 4),
                          ([4, 8, 12, 12, 3, 6, 2], 6, 3),
                          ([1, 2, 3, 4, 5, 6, 7], 7, 0),
                          ([1, 6, 2, 3, 7, 8, 7], 0, 1),
                          ([1, 2, 3, 4], 0, 0),
                          ([1, 1, 1, 1], 0, 6)])
def test_int_diff(test_input1, test_input2, expected):
    """
    Tests the Format to the 2nd solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert int_diff(test_input1, test_input2) == expected
