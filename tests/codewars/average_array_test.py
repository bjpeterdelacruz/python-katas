"""
Unit tests for Average Array challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.average_array import avg_array


@pytest.mark.parametrize("test_input, expected",
                         [([[1, 2, 3], [4, 2], [1]], [2.0, 2.0, 3.0]),
                          ([[1, 2, 3], [4, 6, 8]], [2.5, 4.0, 5.5])])
def test_avg_array(test_input, expected):
    """
    Tests the Average Array solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert avg_array(test_input) == expected
