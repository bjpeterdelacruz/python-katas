"""
Unit tests for Isogram Cipher challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.isogram_cipher import isogram_encode, isogram_decode


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(500, "PATHFINDER", "FRR"), ("500", "PATHFINDER", "FRR"),
                          ('Codewars', 'PATHFINDER', 'Incorrect key or input!'),
                          ('', 'PATHFINDER', 'Incorrect key or input!'),
                          (500, 'PATHFIND', 'Incorrect key or input!'),
                          (500, 'PATHFINDEE', 'Incorrect key or input!'),
                          (732242421, "BSDJOIGYQ", 'Incorrect key or input!'),
                          (3558867, "PUIVDHSRW", 'Incorrect key or input!')])
def test_isogram_encode(test_input1, test_input2, expected):
    """
    Tests the Isogram Cipher (encode) solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert isogram_encode(test_input1, test_input2) == expected


def test_isogram_encode_varargs():
    """
    Tests the Isogram Cipher (encode) solution with fewer arguments.

    :return: None
    """
    assert isogram_encode() == 'Incorrect key or input!'
    assert isogram_encode(500) == 'Incorrect key or input!'


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('FRR', 'PATHFINDER', '500'),
                          ('FRR', 'PATHFIND', 'Incorrect key or input!'),
                          (True, 'PATHFINDER', 'Incorrect key or input!'),
                          ("", 'PATHFINDER', 'Incorrect key or input!'),
                          ('LOL', 'PATHFINDER', 'Incorrect key or input!')])
def test_isogram_decode(test_input1, test_input2, expected):
    """
    Tests the Isogram Cipher (decode) solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert isogram_decode(test_input1, test_input2) == expected


def test_isogram_decode_varargs():
    """
    Tests the Isogram Cipher (decode) solution with fewer arguments.

    :return: None
    """
    assert isogram_decode() == 'Incorrect key or input!'
    assert isogram_decode(500) == 'Incorrect key or input!'
