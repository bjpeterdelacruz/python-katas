"""
Unit tests for Comfortable Words challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.comfortable_words import comfortable_word


@pytest.mark.parametrize("test_input, expected",
                         [("cub", True), ("wand", False), ("a", True), ("az", False)])
def test_comfortable_word(test_input, expected):
    """
    Tests the Comfortable Words solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert comfortable_word(test_input) == expected
