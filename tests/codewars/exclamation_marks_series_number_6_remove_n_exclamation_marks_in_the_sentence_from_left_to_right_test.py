"""
Unit tests for Exclamation Marks Series #6 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_6_remove_n_exclamation_marks_in_the_sentence_from_left_to_right import remove  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("Hi!", 1, "Hi"),
                          ("Hi!", 100, "Hi"),
                          ("Hi!!!", 1, "Hi!!"),
                          ("Hi!!!", 100, "Hi"),
                          ("!Hi", 1, "Hi"),
                          ("!Hi!", 1, "Hi!"),
                          ("!Hi!", 100, "Hi"),
                          ("!!!Hi !!hi!!! !hi", 1, "!!Hi !!hi!!! !hi"),
                          ("!!!Hi !!hi!!! !hi", 3, "Hi !!hi!!! !hi"),
                          ("!!!Hi !!hi!!! !hi", 5, "Hi hi!!! !hi"),
                          ("!!!Hi !!hi!!! !hi", 100, "Hi hi hi")])
def test_remove(test_input1, test_input2, expected):
    """
    Tests the Exclamation Marks Series #1 solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove(test_input1, test_input2) == expected
