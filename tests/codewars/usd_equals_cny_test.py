"""
Unit tests for USD => CNY challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.usd_equals_cny import usdcny


@pytest.mark.parametrize("test_input, expected",
                         [(15, "101.25 Chinese Yuan"), (465, "3138.75 Chinese Yuan")])
def test_usdcny(test_input, expected):
    """
    Tests the USD => CNY solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert usdcny(test_input) == expected
