"""
Unit tests for Most Consecutive Zeros challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.most_consecutive_zeros_of_a_binary_number import max_consec_zeros


@pytest.mark.parametrize("test_input, expected",
                         [("7", "Zero"), ("10000", "Four")])
def test_max_consec_zeros(test_input, expected):
    """
    Tests the Most Consecutive Zeros solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert max_consec_zeros(test_input) == expected
