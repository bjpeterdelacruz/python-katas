"""
Unit tests for Alternating Case challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.alternating_case_equals_alternating_case import to_alternating_case


@pytest.mark.parametrize("test_input, expected",
                         [("1a2b3c4d5e", "1A2B3C4D5E"),
                          ("String.prototype.toAlternatingCase",
                           "sTRING.PROTOTYPE.TOaLTERNATINGcASE"), ("HeLLo WoRLD", "hEllO wOrld"),
                          ("hello WORLD", "HELLO world"), ("hello world", "HELLO WORLD"),
                          ("HELLO WORLD", "hello world")])
def test_to_alternating_case(test_input, expected):
    """
    Tests the Alternating Case solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert to_alternating_case(test_input) == expected
