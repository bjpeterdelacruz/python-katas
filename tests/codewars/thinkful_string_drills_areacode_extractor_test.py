"""
Unit tests for Thinkful: Area Codes challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_string_drills_areacode_extractor import area_code


@pytest.mark.parametrize("test_input, expected",
                         [('My phone number is (808) 123-4567', '808'),
                          ('(123) 456-7890', '123')])
def test_area_code(test_input, expected):
    """
    Tests the Thinkful: Area Codes solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert area_code(test_input) == expected
