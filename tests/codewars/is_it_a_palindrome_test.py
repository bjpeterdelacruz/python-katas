"""
Unit tests for Is It a Palindrome? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.is_it_a_palindrome import is_palindrome


@pytest.mark.parametrize("test_input, expected",
                         [('a', True),
                          ('aba', True),
                          ('Abba', True),
                          ('malam', True),
                          ('walter', False),
                          ('kodok', True),
                          ('Kasue', False),
                          ('hello', False),
                          ('Bob', True),
                          ('Madam', True),
                          ('AbBa', True),
                          ('', True),
                          ("LAGrALLyiclOaEowNvmU", False),
                          ("cWalaIYFGucHEhbnEH", False),
                          ("smlWLKQYCrRUcqOLYuGGuYLOqcURrCYQKLWlms", True),
                          ("dRjLeHcvvcHeLjRd", True),
                          ("wvvqHAfrFWkIYygRQHTR", False),
                          ("zuKWoAyeQNvhurRlYlUUlYlRruhvNQeyAoWKuz", True),
                          ("QtFpQMSYPMnnMPYSMQpFtQ", True),
                          ("muNcdggdcNum", True),
                          ("TUkKinLuE", False),
                          ("MaKeRSDisini", False)])
def test_is_palindrome(test_input, expected):
    """
    Tests the Is It a Palindrome? solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_palindrome(test_input) == expected
