"""
Unit tests for Lottery Machine challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.lottery_machine import lottery


@pytest.mark.parametrize("test_input, expected",
                         [("wQ8Hy0y5m5oshQPeRCkG", "805"),
                          ("ffaQtaRFKeGIIBIcSJtg", "One more run!"),
                          ("555", "5"),
                          ("HappyNewYear2020", "20"),
                          ("20191224isXmas", "20194"),
                          ("", "One more run!")])
def test_lottery(test_input, expected):
    """
    Tests the Lottery Machine solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert lottery(test_input) == expected
