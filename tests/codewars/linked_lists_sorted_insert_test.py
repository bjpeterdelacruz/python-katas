"""
Unit tests for Linked Lists: Sorted Insert challenge

:Author: BJ Peter Dela Cruz
"""
from utils.linked_list import build_one_two_three
from codewars.linked_lists_sorted_insert import sorted_insert


def test_sorted_insert():
    """
    Assert that a value is inserted into a linked list at the correct position.

    :return: None
    """
    assert sorted_insert(None, 23).data == 23
    assert sorted_insert(None, 23).next is None

    assert sorted_insert(build_one_two_three(), 0.5).data == 0.5
    assert sorted_insert(build_one_two_three(), 0.5).next.data == 1
    assert sorted_insert(build_one_two_three(), 0.5).next.next.data == 2
    assert sorted_insert(build_one_two_three(), 0.5).next.next.next.data == 3
    assert sorted_insert(build_one_two_three(), 0.5).next.next.next.next is None

    assert sorted_insert(build_one_two_three(), 1.5).data == 1
    assert sorted_insert(build_one_two_three(), 1.5).next.data == 1.5
    assert sorted_insert(build_one_two_three(), 1.5).next.next.data == 2
    assert sorted_insert(build_one_two_three(), 1.5).next.next.next.data == 3
    assert sorted_insert(build_one_two_three(), 1.5).next.next.next.next is None

    assert sorted_insert(build_one_two_three(), 2.5).data == 1
    assert sorted_insert(build_one_two_three(), 2.5).next.data == 2
    assert sorted_insert(build_one_two_three(), 2.5).next.next.data == 2.5
    assert sorted_insert(build_one_two_three(), 2.5).next.next.next.data == 3
    assert sorted_insert(build_one_two_three(), 2.5).next.next.next.next is None

    assert sorted_insert(build_one_two_three(), 3.5).data == 1
    assert sorted_insert(build_one_two_three(), 3.5).next.data == 2
    assert sorted_insert(build_one_two_three(), 3.5).next.next.data == 3
    assert sorted_insert(build_one_two_three(), 3.5).next.next.next.data == 3.5
    assert sorted_insert(build_one_two_three(), 3.5).next.next.next.next is None
