"""
Unit tests for Maximum Sum Between Two Negatives challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.max_sum_between_two_negatives import max_sum_between_two_negatives


@pytest.mark.parametrize("test_input, expected",
                         [([5, 7, 3, 1, -1], -1), ([1, 2, 3, 4], -1),
                          ([-1, 2, 4, -3, 7], 6), ([1, -1, 2, 3, -5, 4, -6], 5)])
def test_strip_url_params(test_input, expected):
    """
    Tests the Maximum Sum Between Two Negatives solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert max_sum_between_two_negatives(test_input) == expected
