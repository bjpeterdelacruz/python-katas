"""
Unit tests for Find Your Villain Name challenge

:Author: BJ Peter Dela Cruz
"""
from datetime import datetime
import pytest
from codewars.find_your_villain_name import get_villain_name

FORMAT_STR = '%d/%m/%Y'


@pytest.mark.parametrize("test_input, expected",
                         [(datetime.strptime("1/1/2000", FORMAT_STR), "The Evil Pickle"),
                          (datetime.strptime("2/2/2000", FORMAT_STR), "The Vile Hood Ornament"),
                          (datetime.strptime("2/12/2000", FORMAT_STR),
                           "The Awkward Hood Ornament")])
def test_get_villain_name(test_input, expected):
    """
    Tests the Find Your Villain Name solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_villain_name(test_input) == expected
