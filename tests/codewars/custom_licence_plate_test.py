"""
Unit tests for Custom Licence Plate challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.custom_licence_plate import licence_plate


@pytest.mark.parametrize("test_input, expected",
                         [("mercedes", "MERCEDES"), ("anter69", "ANTER-69"), ("1st", "1-ST"),
                          ("~c0d3w4rs~", "C-0-D-3"), ("I'm cool!", "I-M-COOL"),
                          ("1337", "not possible"), ("Eat*DiRT!", "EAT-DIRT"),
                          ("1, 2- 3 :gO!", "1-2-3-GO"), ("0 good", "0-GOOD"),
                          ("_9K*w\"HB7+", "9-K-W-HB"), ("! @ x & $", "not possible"),
                          ("+6GMh\\xs", "6-GMH-XS"), ("p![", "not possible"), ("", "not possible"),
                          ("---", "not possible")])
def test_licence_plate(test_input, expected):
    """
    Tests the Custom Licence Plate solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert licence_plate(test_input) == expected
