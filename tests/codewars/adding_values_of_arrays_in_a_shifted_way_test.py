"""
Unit tests for Adding Values of Arrays in a Shifted Way challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.adding_values_of_arrays_in_a_shifted_way import sum_arrays


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([[1, 2, 3, 4, 5, 6], [7, 7, 7, 7, 7, -7]], 0, [8, 9, 10, 11, 12, -1]),
                          ([[1, 2, 3, 4, 5, 6], [7, 7, 7, 7, 7, 7]], 3,
                           [1, 2, 3, 11, 12, 13, 7, 7, 7]),
                          ([[1, 2, 3, 4, 5, 6], [7, 7, 7, -7, 7, 7], [1, 1, 1, 1, 1, 1]], 3,
                           [1, 2, 3, 11, 12, 13, -6, 8, 8, 1, 1, 1])])
def test_sum_arrays(test_input1, test_input2, expected):
    """
    Tests the Adding Values of Arrays in a Shifted Way solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_arrays(test_input1, test_input2) == expected
