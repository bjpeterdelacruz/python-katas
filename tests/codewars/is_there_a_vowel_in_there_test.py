"""
Unit tests for Is There a Vowel in There? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.is_there_a_vowel_in_there import is_vow


@pytest.mark.parametrize("test_input, expected",
                         [([118, 117, 120, 121, 117, 98, 122, 97, 120, 106, 104, 116, 113, 114, 113,
                            120, 106],
                           [118, "u", 120, 121, "u", 98, 122, "a", 120, 106, 104, 116, 113, 114,
                            113, 120, 106]),
                          ([101, 121, 110, 113, 113, 103, 121, 121, 101, 107, 103],
                           ["e", 121, 110, 113, 113, 103, 121, 121, "e", 107, 103]),
                          ([118, 103, 110, 109, 104, 106], [118, 103, 110, 109, 104, 106]),
                          ([107, 99, 110, 107, 118, 106, 112, 102],
                           [107, 99, 110, 107, 118, 106, 112, 102]),
                          ([100, 100, 116, 105, 117, 121], [100, 100, 116, "i", "u", 121])])
def test_is_vow(test_input, expected):
    """
    Tests the Is There a Vowel in There? solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_vow(test_input) == expected
