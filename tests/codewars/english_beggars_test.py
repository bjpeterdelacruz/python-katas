"""
Unit tests for English Beggars challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.english_beggars import beggars


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3, 4, 5], 1, [15]), ([1, 2, 3, 4, 5], 2, [9, 6]),
                          ([1, 2, 3, 4, 5], 3, [5, 7, 3]), ([1, 2, 3, 4, 5], 6, [1, 2, 3, 4, 5, 0]),
                          ([1, 2, 3, 4, 5], 0, []), ([1, 2], 5, [1, 2, 0, 0, 0])])
def test_beggars(test_input1, test_input2, expected):
    """
    Tests the English Beggars solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert beggars(test_input1, test_input2) == expected
