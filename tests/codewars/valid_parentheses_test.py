"""
Unit tests for Valid Parentheses challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.valid_parentheses import valid_parentheses


@pytest.mark.parametrize("test_input, expected",
                         [("(((hello))(((world))))", True), (")(H)(I)(", False)])
def test_valid_parentheses(test_input, expected):
    """
    Tests the Valid Parentheses solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert valid_parentheses(test_input) == expected
