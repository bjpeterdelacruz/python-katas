"""
Unit tests for Find the Capitals challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_the_capitals import capital


@pytest.mark.parametrize("test_input, expected",
                         [([{'state': 'Maine', 'capital': 'Augusta'}],
                           ["The capital of Maine is Augusta"]),
                          ([{'country': 'Spain', 'capital': 'Madrid'}],
                           ["The capital of Spain is Madrid"]),
                          ([{"state": 'Maine', 'capital': 'Augusta'},
                            {'country': 'Spain', "capital": "Madrid"}],
                           ["The capital of Maine is Augusta", "The capital of Spain is Madrid"])])
def test_capital(test_input, expected):
    """
    Tests the Find the Capitals solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert capital(test_input) == expected
