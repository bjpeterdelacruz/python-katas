"""
Unit tests for Short Long Short challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.short_long_short import solution


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("1", "22", "1221"), ("22", "1", "1221"), ("22", "44", "442244")])
def test_solution(test_input1, test_input2, expected):
    """
    Tests the Short Long Short solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert solution(test_input1, test_input2) == expected
