"""
Unit tests for Keyboard Handler challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.keyboard_handler import handler


@pytest.mark.parametrize("test_input, expected",
                         [('a', 'a'), ('1', '1'), (5, 'KeyError'), ('A', 'KeyError'),
                          ('', 'KeyError'), ('abc', 'KeyError'), (['a', 2, 'C'], 'KeyError'),
                          ({'a': 1, 2: 'b'}, 'KeyError')])
def test_handler_1(test_input, expected):
    """
    Tests the Keyboard Handler solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert handler(test_input) == expected


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('a', True, 'A'), ('1', True, '1')])
def test_handler_2(test_input1, test_input2, expected):
    """
    Tests the Keyboard Handler solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert handler(test_input1, test_input2) == expected


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [('a', True, True, 'a'),
                          ('a', False, True, 'A'),
                          ('1', True, True, '!'),
                          ('1', False, True, '!')])
def test_handler_3(test_input1, test_input2, test_input3, expected):
    """
    Tests the Keyboard Handler solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert handler(test_input1, test_input2, test_input3) == expected


def test_handler_4():
    """
    Tests the Keyboard Handler solution.

    :return: None
    """
    chars = "qwertyuiopasdfghjklzxcvbnm1234567890-=[];'\\/.,`"
    shift = 'QWERTYUIOPASDFGHJKLZXCVBNM!@#$%^&*()_+{}:"|?><~'
    for character in zip(chars, shift):
        assert handler(character[0], False, True) == character[1]
