"""
Unit tests for Breadcrumb Generator challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.breadcrumb_generator import generate_bc


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("www.codewars.com/users/GiacomoSorbi?ref=CodeWars", " / ",
                           '<a href="/">HOME</a> / <a href="/users/">USERS</a> '
                           '/ <span class="active">GIACOMOSORBI</span>'),
                          ("www.microsoft.com/important/confidential/docs/index.htm#top", " * ",
                           '<a href="/">HOME</a> * <a href="/important/">IMPORTANT</a> *'
                           ' <a href="/important/confidential/">CONFIDENTIAL</a> '
                           '* <span class="active">DOCS</span>'),
                          ("mysite.com/very-long-url-to-make-a-silly-yet-meaningful-example/"
                           "example.asp", " > ",
                           '<a href="/">HOME</a> > <a href="/very-long-url-to-make-a-silly-yet'
                           '-meaningful-example/">VLUMSYME</a> > <span class="active">EXAMPLE'
                           '</span>'),
                          ("www.very-long-site_name-to-make-a-silly-yet-meaningful-example.com"
                           "/users/giacomo-sorbi", " + ",
                           '<a href="/">HOME</a> + <a href="/users/">USERS</a> + '
                           '<span class="active">GIACOMO SORBI</span>'),
                          ("mysite.com/pictures/holidays.html", " : ",
                           '<a href="/">HOME</a> : <a href="/pictures/">PICTURES</a> : '
                           '<span class="active">HOLIDAYS</span>'),
                          ("www.codewars.com/users/GiacomoSorbi", " / ",
                           '<a href="/">HOME</a> / <a href="/users/">USERS</a> /'
                           ' <span class="active">GIACOMOSORBI</span>'),
                          ("www.microsoft.com/docs/index.htm", " * ",
                           '<a href="/">HOME</a> * <span class="active">DOCS</span>'),
                          ("mysite.com/very-long-url-to-make-a-silly-yet-meaningful-example/"
                           "example.htm", " > ",
                           '<a href="/">HOME</a> > <a href="/very-long-url-to-make-a-silly-yet'
                           '-meaningful-example/">VLUMSYME</a> > <span class="active">EXAMPLE'
                           '</span>'),
                          ("www.very-long-site_name-to-make-a-silly-yet-meaningful-example.com/"
                           "users/giacomo-sorbi", " + ",
                           '<a href="/">HOME</a> + <a href="/users/">USERS</a> +'
                           ' <span class="active">GIACOMO SORBI</span>'),
                          ("https://www.pippi.pi/index.htm#offers?previous=normalSearch&output"
                           "=full", ".", '<span class="active">HOME</span>'),
                          ("http://www.agcpartners.co.uk?favourite=code", " : ",
                           '<span class="active">HOME</span>'),
                          ("http://www.codewars.com/test.html#conclusion", " : ",
                           '<a href="/">HOME</a> : <span class="active">TEST</span>'),
                          ("www.agcpartners.co.uk/", " * ", '<span class="active">HOME</span>')])
def test_generate_bc(test_input1, test_input2, expected):
    """
    Tests the Breadcrumb Generator solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert generate_bc(test_input1, test_input2) == expected
