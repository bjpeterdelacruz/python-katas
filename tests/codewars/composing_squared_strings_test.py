"""
Unit tests for Computing Squared Strings challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.composing_squared_strings import compose


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("byGt\nhTts\nRTFF\nCnnI", "jIRl\nViBu\nrWOb\nNkTB",
                           "bNkTB\nhTrWO\nRTFVi\nCnnIj"),
                          ("HXxA\nTGBf\nIPhg\nuUMD", "Hcbj\nqteH\nGbMJ\ngYPW",
                           "HgYPW\nTGGbM\nIPhqt\nuUMDH"),
                          ("tSrJ\nOONy\nsqPF\nxMkB", "hLqw\nEZuh\nmYFl\nzlYf",
                           "tzlYf\nOOmYF\nsqPEZ\nxMkBh"),
                          ("fn\nlr", "Kz\nmO", "fmO\nlrK"),
                          ("fctRKq\nBCorKQ\nZKGbDO\nbhHohe\nUjyNSg\nPCOiuM",
                           "elSYAB\nLQMYuN\nTzQtaM\nFutqip\nwSAjZX\nuOPhSJ",
                           "fuOPhSJ\nBCwSAjZ\nZKGFutq\nbhHoTzQ\nUjyNSLQ\nPCOiuMe"),

                          ("qtKz\negiP\niOgb\nRqly", "ZUCx\nShBJ\nmybK\neBZA",
                           "qeBZA\negmyb\niOgSh\nRqlyZ"),
                          ("rmNE\naFQJ\nfsNe\ntDtw", "GvqU\noJlZ\ngJxQ\nVQvX",
                           "rVQvX\naFgJx\nfsNoJ\ntDtwG"),
                          ("RCKr\naJwU\nqEyM\nNbdP", "hxYA\nlUtD\nLFmc\nssTy",
                           "RssTy\naJLFm\nqEylU\nNbdPh"),
                          ("lFqaEC\nITEzHC\nqaEPEb\nexhzgU\nxoxRJc\nTxqlDN",
                           "IMpAnn\nktLyDb\nHawiQt\nNVRips\ncrKROc\nJqPpty",
                           "lJqPpty\nITcrKRO\nqaENVRi\nexhzHaw\nxoxRJkt\nTxqlDNI"),
                          ("wEGa\nhICc\nPrvY\nCuSd", "qfYz\nwJfU\noHhO\nNxaV",
                           "wNxaV\nhIoHh\nPrvwJ\nCuSdq")])
def test_compose(test_input1, test_input2, expected):
    """
    Tests the Computing Squared Strings solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert compose(test_input1, test_input2) == expected
