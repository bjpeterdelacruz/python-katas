"""
Unit tests for flatten() challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.flatten import flatten


@pytest.mark.parametrize("test_input, expected",
                         [(['a', ['b', 2], 3, None, [[4], ['c']]], ['a', 'b', 2, 3, None, 4, 'c'])])
def test_flatten(test_input, expected):
    """
    Tests the flatten() solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert flatten(test_input) == expected
