"""
Unit tests for Simple Fun #358: Vertical Histogram Of Letters challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.simple_fun_number_358_vertical_histogram_of_letters import vertical_histogram_of


def test_vertical_histogram_of():
    """
    Tests the Simple Fun #358: Vertical Histogram Of Letters solution.

    :return: None
    """
    assert vertical_histogram_of("XXY YY ZZZ123ZZZ AAA BB C") == \
           """          *
          *
          *
*       * *
* *   * * *
* * * * * *
A B C X Y Z"""

    assert vertical_histogram_of("AAABBC") == '*\n* *\n* * *\nA B C'

    assert vertical_histogram_of("abc123") == ""
