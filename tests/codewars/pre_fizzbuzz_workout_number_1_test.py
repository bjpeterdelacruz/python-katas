"""
Unit tests for Pre-FizzBuzz Workout #1 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.pre_fizzbuzz_workout_number_1 import pre_fizz


@pytest.mark.parametrize("test_input, expected",
                         [(3, [1, 2, 3]), (5, [1, 2, 3, 4, 5])])
def test_pre_fizz(test_input, expected):
    """
    Tests the Pre-FizzBuzz Workout #1 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert pre_fizz(test_input) == expected
