"""
Unit tests for Zero Terminated Sum challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.zero_terminated_sum import largest_sum


@pytest.mark.parametrize("test_input, expected",
                         [("72102450111111090", 11),
                          ("123004560", 15),
                          ("0", 0),
                          ("4755843230", 41),
                          ("3388049750", 25),
                          ("3902864190", 30),
                          ("4098611970", 41),
                          ("5628206860", 23),
                          ("5891002970", 23),
                          ("0702175070", 15)])
def test_largest_sum(test_input, expected):
    """
    Tests the Zero Terminated Sum solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert largest_sum(test_input) == expected
