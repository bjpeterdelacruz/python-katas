"""
Unit tests for Cat and Mouse challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.cat_and_mouse_easy_version import cat_mouse


@pytest.mark.parametrize("test_input, expected",
                         [("C....m", "Escaped!"), ("C..m", "Caught!"), ("C.....m", "Escaped!"),
                          ("C.m", "Caught!"), ("m...C", "Caught!"), ("m...", "Escaped!"),
                          ("..C..", "Escaped!")])
def test_cat_mouse(test_input, expected):
    """
    Tests the Cat and Mouse solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert cat_mouse(test_input) == expected
