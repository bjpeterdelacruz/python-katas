"""
Unit tests for Bob's Short Forms challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.bobs_short_forms import short_form


@pytest.mark.parametrize("test_input, expected",
                         [("typhoid", "typhd"),
                          ("fire", "fre"),
                          ("destroy", "dstry"),
                          ("kata", "kta"),
                          ("codewars", "cdwrs"),
                          ("assert", "assrt"),
                          ("insane", "insne"),
                          ("nice", "nce"),
                          ("amazing", "amzng"),
                          ("incorrigible", "incrrgble"),
                          ("HeEllO", "HllO"),
                          ("inCRediBLE", "inCRdBLE"),
                          ("IMpOsSiblE", "IMpsSblE"),
                          ("UnInTENtiONAl", "UnnTNtNl"),
                          ("AWESOme", "AWSme"),
                          ("rhythm", "rhythm"),
                          ("hymn", "hymn"),
                          ("lynx", "lynx"),
                          ("nymph", "nymph"),
                          ("pygmy", "pygmy")])
def test_short_form(test_input, expected):
    """
    Tests the Bob's Short Forms solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert short_form(test_input) == expected
