"""
Unit tests for Unexpected Parsing challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.unexpected_parsing import get_status


@pytest.mark.parametrize("test_input, expected",
                         [(True, {"status": "busy"}), (False, {"status": "available"})])
def test_get_status(test_input, expected):
    """
    Tests the Unexpected Parsing solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_status(test_input) == expected
