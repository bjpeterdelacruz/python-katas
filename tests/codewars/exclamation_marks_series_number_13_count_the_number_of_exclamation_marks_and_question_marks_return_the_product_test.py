"""
Unit tests for Exclamation Marks Series #13 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_13_count_the_number_of_exclamation_marks_and_question_marks_return_the_product import product  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [('', 0), ('!!', 0), ('???', 0), ('!?', 1), ('!!???', 6),
                          ('!?!?!!!?', 15)])
def test_product(test_input, expected):
    """
    Tests the Exclamation Marks Series #13 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert product(test_input) == expected
