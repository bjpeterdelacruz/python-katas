"""
Unit tests for Fibonacci Factorials challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.fib_factorials import sum_fib


def test_factorial():
    """
    Tests the Fibonacci Factorials solution.

    :return: None
    """
    tests = {
        2: 2,
        3: 3,
        4: 5,
        5: 11,
        6: 131,
        7: 40451,
        8: 6227061251,
        9: 51090942177936501251,
        10: 295232799039604140898709551821456501251,
        11: 12696403353658275925965100847566517254813120091053577660985391821456501251
    }
    for num, expected in tests.items():
        assert sum_fib(num) == expected
