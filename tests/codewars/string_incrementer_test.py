"""
Unit tests for String Incrementer challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.string_incrementer import increment_string


@pytest.mark.parametrize("test_input, expected",
                         [('foo', 'foo1'), ("foobar001", "foobar002"), ("foobar1", "foobar2"),
                          ("foobar00", "foobar01"), ("foobar99", "foobar100"),
                          ("foobar099", "foobar100"), ("", "1"), ("foobar0099", "foobar0100"),
                          ("foobar101", "foobar102"), ("foobar0101", "foobar0102"), ("000", "001"),
                          ("10", "11"),
                          ("x136888.rL3367649988252834", "x136888.rL3367649988252835"),
                          (">;Qa?C+145L7MA^(cn9'46586&}bmEk/B225000001606",
                           ">;Qa?C+145L7MA^(cn9'46586&}bmEk/B225000001607")])
def test_increment_string(test_input, expected):
    """
    Tests the String Incrementer solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert increment_string(test_input) == expected
