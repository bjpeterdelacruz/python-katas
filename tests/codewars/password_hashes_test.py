"""
Unit tests for Password Hashes challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.password_hashes import pass_hash


@pytest.mark.parametrize("test_input, expected",
                         [("password", "5f4dcc3b5aa765d61d8327deb882cf99"),
                          ("abc123", "e99a18c428cb38d5f260853678922e03")])
def test_pass_hash(test_input, expected):
    """
    Tests the Password Hashes solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert pass_hash(test_input) == expected
