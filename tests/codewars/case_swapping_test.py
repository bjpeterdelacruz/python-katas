"""
Unit tests for Case Swapping challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.case_swapping import swap


@pytest.mark.parametrize("test_input, expected",
                         [('HelloWorld', 'hELLOwORLD'),
                          ('CodeWars', 'cODEwARS'),
                          ('ThIs iS A l0NG sENTence witH nUMbERs in IT 123 456',
                           'tHiS Is a L0ng SentENCE WITh NumBerS IN it 123 456'),
                          ('', ''),
                          (' ', ' '),
                          ('  ', '  '),
                          (' 1a1 ', ' 1A1 '),
                          ('H_E_l-l_0 WO|||Rld', 'h_e_L-L_0 wo|||rLD'),
                          ('TeSt', 'tEsT'),
                          ('EeEEeeEEEeee', 'eEeeEEeeeEEE')])
def test_swap(test_input, expected):
    """
    Tests the Case Swapping solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert swap(test_input) == expected
