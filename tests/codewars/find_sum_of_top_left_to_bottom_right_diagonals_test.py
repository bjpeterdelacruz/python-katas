"""
Unit tests for Find Diagonal Sum challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_sum_of_top_left_to_bottom_right_diagonals import diagonal_sum


@pytest.mark.parametrize("test_input, expected",
                         [([[1, 2], [3, 4]], 5),
                          ([[1, 2, 3], [4, 5, 6], [7, 8, 9]], 15),
                          ([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]], 34)])
def test_diagonal_sum(test_input, expected):
    """
    Tests the Find Diagonal Sum solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert diagonal_sum(test_input) == expected
