"""
Unit tests for Substring Fun challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.substring_fun import nth_char


@pytest.mark.parametrize("test_input, expected",
                         [(['yoda', 'best', 'has'], 'yes'),
                          ([], ''),
                          (['X-ray'], 'X'),
                          (['No', 'No'], 'No'),
                          (['Chad', 'Morocco', 'India', 'Algeria', 'Botswana', 'Bahamas',
                            'Ecuador', 'Micronesia'], 'Codewars')])
def test_nth_char(test_input, expected):
    """
    Tests the Substring Fun solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert nth_char(test_input) == expected
