"""
Unit tests for Calculate Average challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.calculate_average import find_average


@pytest.mark.parametrize("test_input, expected",
                         [([], 0), ([1, 2, 3], 2), ([2, 3], 2.5)])
def test_find_average(test_input, expected):
    """
    Tests the Calculate Average solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_average(test_input) == expected
