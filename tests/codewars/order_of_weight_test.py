"""
Unit tests for Order of Weight challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.order_of_weight import arrange


@pytest.mark.parametrize("test_input, expected",
                         [(["200G", "300G", "150G", "100KG"], ["150G", "200G", "300G", "100KG"]),
                          (["400G", "100T", "150KG", "100G"], ["100G", "400G", "150KG", "100T"]),
                          (["4T", "300G", "450G", "900KG"], ["300G", "450G", "900KG", "4T"]),
                          (["400T", "100T", "1T", "100G"], ["100G", "1T", "100T", "400T"]),
                          (["1G", "2KG", "3T", "100KG"], ["1G", "2KG", "100KG", "3T"]),
                          (["100KG", "100G", "150T", "150KG"], ["100G", "100KG", "150KG", "150T"]),
                          (["3T", "2900000G", "2950KG"], ["2900000G", "2950KG", "3T"]),
                          (["3T", "3000001G", "2950KG"], ["2950KG", "3T", "3000001G"]),
                          (["1T"], ["1T"]),
                          ([], [])])
def test_arrange(test_input, expected):
    """
    Tests the Order of Weight solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert arrange(test_input) == expected
