"""
Unit tests for Pete the Baker challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.pete_the_baker import cakes


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [({"flour": 500, "sugar": 200, "eggs": 1},
                           {"flour": 1200, "sugar": 1200, "eggs": 5, "milk": 200}, 2),
                          ({"apples": 3, "flour": 300, "sugar": 150, "milk": 100, "oil": 100},
                           {"sugar": 500, "flour": 2000, "milk": 2000}, 0),
                          ({"flour": 500, "sugar": 200, "eggs": 2},
                           {"flour": 250, "sugar": 100, "eggs": 1, "milk": 200}, 0)
                          ])
def test_cakes(test_input1, test_input2, expected):
    """
    Tests the Pete the Baker solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert cakes(test_input1, test_input2) == expected
