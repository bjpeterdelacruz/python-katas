"""
Moves in Squared Strings 2

:Author: BJ Peter Dela Cruz
"""
from codewars.moves_in_squared_strings_ii import rot, selfie_and_rot, oper


def test_oper():
    """
    Tests the Moves in Squared Strings 2 solution.

    :return: None
    """
    assert oper(rot, "fijuoo\nCqYVct\nDrPmMJ\nerfpBA\nkWjFUG\nCVUfyL")\
           == "LyfUVC\nGUFjWk\nABpfre\nJMmPrD\ntcVYqC\nooujif"
    assert oper(rot, "rkKv\ncofM\nzXkh\nflCB") == "BClf\nhkXz\nMfoc\nvKkr"
    assert oper(selfie_and_rot, "xZBV\njsbS\nJcpN\nfVnP")\
           == "xZBV....\njsbS....\nJcpN....\nfVnP....\n....PnVf\n....NpcJ\n....Sbsj\n....VBZx"
    assert oper(selfie_and_rot, "uLcq\nJkuL\nYirX\nnwMB")\
           == "uLcq....\nJkuL....\nYirX....\nnwMB....\n....BMwn\n....XriY\n....LukJ\n....qcLu"
    assert oper(rot, "abcd\nefgh\nijkl\nmnop") == "ponm\nlkji\nhgfe\ndcba"
    assert oper(selfie_and_rot, "abcd\nefgh\nijkl\nmnop")\
           == "abcd....\nefgh....\nijkl....\nmnop....\n....ponm\n....lkji\n....hgfe\n....dcba"
