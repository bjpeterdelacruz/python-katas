"""
Unit tests for Super Duper Easy challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.super_duper_easy import problem


@pytest.mark.parametrize("test_input, expected",
                         [(20, 1006), ("Hello", "Error"), (None, "Error"), (True, "Error")])
def test_problem(test_input, expected):
    """
    Tests the Super Duper Easy solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert problem(test_input) == expected
