"""
Unit tests for Do You Speak English? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.do_you_speak_english import sp_eng


@pytest.mark.parametrize("test_input, expected",
                         [(None, False), ("aBcEnGlIsHdEf", True), ("__ENG_L_ISH__", False),
                          ("baENGLISHab", True), ("ABCenglishDEF", True)])
def test_sp_eng(test_input, expected):
    """
    Tests the Do You Speak English? solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sp_eng(test_input) == expected
