"""
Unit tests for Descending Order challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.descending_order import descending_order


@pytest.mark.parametrize("test_input, expected",
                         [(15, 51), (231, 321), (54871239, 98754321)])
def test_descending_order(test_input, expected):
    """
    Tests the Descending Order solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert descending_order(test_input) == expected
