"""
Unit tests for IPv4 to int32 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.ipv4_to_int32 import ip_to_int32


@pytest.mark.parametrize("test_input, expected",
                         [("128.114.17.104", 2154959208), ("0.0.0.0", 0),
                          ("128.32.10.1", 2149583361)])
def test_ip_to_int32(test_input, expected):
    """
    Tests the IPv4 to int32 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert ip_to_int32(test_input) == expected
