"""
Unit tests for Square Even, Root Odd challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_square_even_root_odd import sum_square_even_root_odd


@pytest.mark.parametrize("test_input, expected",
                         [([4, 5, 7, 8, 1, 2, 3, 0], 91.61), ([1, 14, 9, 8, 17, 21], 272.71)])
def test_sum_square_even_root_odd(test_input, expected):
    """
    Tests the Square Even, Root Odd solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_square_even_root_odd(test_input) == expected
