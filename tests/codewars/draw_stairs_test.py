"""
Unit tests for Draw Stairs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.draw_stairs import draw_stairs


@pytest.mark.parametrize("test_input, expected",
                         [(3, 'I\n I\n  I'), (5, 'I\n I\n  I\n   I\n    I')])
def test_draw_stairs(test_input, expected):
    """
    Tests the Draw Stairs solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert draw_stairs(test_input) == expected
