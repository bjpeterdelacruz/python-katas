"""
Unit tests for Keyword Cipher challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.keyword_cipher import keyword_cipher


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("Welcome home", "secret", "wticljt dljt"),
                          ("hello", "wednesday", "bshhk"),
                          ("HELLO", "wednesday", "bshhk"),
                          ("HeLlO", "wednesday", "bshhk"),
                          ("WELCOME HOME", "gridlocked", "wlfimhl kmhl"),
                          ("alpha bravo charlie", "delta", "djofd eqdvn lfdqjga"),
                          ("Home Base", "seven", "dlja esqa"),
                          ("basecamp", "covert", "ocprvcil"),
                          ("one two three", "rails", "mks twm tdpss"),
                          ("Test", "unbuntu", "raqr")])
def test_keyword_cipher(test_input1, test_input2, expected):
    """
    Tests the Keyword Cipher solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert keyword_cipher(test_input1, test_input2) == expected
