"""
Unit tests for Data Type Scramble challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.data_type_scramble import make_model_year


@pytest.mark.parametrize("test_input, expected",
                         [([1998, 'ford', ('mustang', 'gt'), False],
                           {'make': 'ford', 'model': 'mustang gt', 'year': 1998, 'new': False}),
                          (['benz', ('motorwagen', 'basic'), False, 1886],
                           {'make': 'benz', 'model': 'motorwagen basic', 'year': 1886,
                            'new': False}),
                          ([('camry', 'basic'), True, 2020, 'toyo'],
                           {'make': 'toyo', 'model': 'camry basic', 'year': 2020, 'new': True})])
def test_make_model_year(test_input, expected):
    """
    Tests the Data Type Scramble solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert make_model_year(test_input) == expected
