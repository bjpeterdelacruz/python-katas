"""
Unit tests for Previous Multiples of Three challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.previous_multiple_of_three import prev_mult_of_three


@pytest.mark.parametrize("test_input, expected",
                         [(1, None), (25, None), (36, 36), (1244, 12), (952406, 9)])
def test_prev_mult_of_three(test_input, expected):
    """
    Tests the Previous Multiples of Three solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert prev_mult_of_three(test_input) == expected
