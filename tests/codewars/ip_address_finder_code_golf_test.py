"""
Unit tests for IP Address Finder (Code Golf) challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.ip_address_finder_code_golf import f


@pytest.mark.parametrize("test_input, expected",
                         [("www.codewars.com", [88, 176, 8, 96]),
                          ("www.starwiki.com", [110, 220, 74, 184]),
                          ("www.winnerss.win", [136, 16, 152, 32])])
def test_f(test_input, expected):
    """
    Tests the IP Address Finder (Code Golf) solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert f(test_input) == expected
