"""
Unit tests for My Head is at the Wrong End challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.my_head_is_at_the_wrong_end import fix_the_meerkat


@pytest.mark.parametrize("test_input, expected",
                         [(["tail", "body", "head"], ["head", "body", "tail"]),
                          (["tails", "body", "heads"], ["heads", "body", "tails"]),
                          (["bottom", "middle", "top"], ["top", "middle", "bottom"]),
                          (["lower legs", "torso", "upper legs"],
                           ["upper legs", "torso", "lower legs"]),
                          (["ground", "rainbow", "sky"], ["sky", "rainbow", "ground"])])
def test_fix_the_meerkat(test_input, expected):
    """
    Tests the My Head is at the Wrong End solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert fix_the_meerkat(test_input) == expected
