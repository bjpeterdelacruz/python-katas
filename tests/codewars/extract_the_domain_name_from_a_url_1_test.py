"""
Unit tests for Extract the Domain Name from a URL challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.extract_the_domain_name_from_a_url_1 import domain_name


@pytest.mark.parametrize("test_input, expected",
                         [("http://google.com", "google"), ("http://google.co.jp", "google"),
                          ("www.xakep.ru", "xakep"), ("https://youtube.com", "youtube"),
                          ("https://yx1fzgvsiie3968lwvti5.co/default.html",
                           "yx1fzgvsiie3968lwvti5"),
                          ("http://www.pqdxy3z9qv66nosw.name/index.php", "pqdxy3z9qv66nosw"),
                          ("http://n5mweqahye74z6c071qictxaaz17.org/default.html",
                           "n5mweqahye74z6c071qictxaaz17"),
                          ("8ly1yn4s7iqwki.de/default.html", "8ly1yn4s7iqwki"),
                          ("http://www.f5875lu8v4m6i.net/index.php", "f5875lu8v4m6i"),
                          ("http://www.codewars.com/kata/", "codewars"),
                          ("http://6slap7ujwnz8cwic3f9jgg.tv/index.php", "6slap7ujwnz8cwic3f9jgg")])
def test_domain_name(test_input, expected):
    """
    Tests the Extract the Domain Name from a URL solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert domain_name(test_input) == expected
