"""
Unit tests for Simple Fun #149: Next Day of Week challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_fun_number_149_next_day_of_week import next_day_of_week


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(4, 42, 6), (6, 42, 2), (7, 42, 2)])
def test_next_day_of_week(test_input1, test_input2, expected):
    """
    Tests the Simple Fun #149: Next Day of Week solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert next_day_of_week(test_input1, test_input2) == expected
