"""
Unit tests for Removing Elements challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.removing_elements import remove_every_other


@pytest.mark.parametrize("test_input, expected",
                         [([], []), ([1], [1]), (['Hello', 'Goodbye'], ['Hello']),
                          (['Yes', 'No', 'Yes', 'No', 'Yes'], ['Yes', 'Yes', 'Yes'])])
def test_remove_every_other(test_input, expected):
    """
    Tests the Removing Elements solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove_every_other(test_input) == expected
