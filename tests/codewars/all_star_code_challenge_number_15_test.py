"""
Unit tests for All Star Code Challenge #15 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.all_star_code_challenge_number_15 import rotate


@pytest.mark.parametrize("test_input, expected",
                         [("Hello", ["elloH", "lloHe", "loHel", "oHell", "Hello"]),
                          (" ", [" "]), ("", []), ("123", ["231", "312", "123"])])
def test_rotate(test_input, expected):
    """
    Tests the All Star Code Challenge #15 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert rotate(test_input) == expected
