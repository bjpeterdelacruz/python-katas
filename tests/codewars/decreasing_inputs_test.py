"""
Unit tests for Decreasing Inputs challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.decreasing_inputs import add


def test_add():
    """
    Tests the Decreasing Inputs solution.

    :return: None
    """
    assert add() == 0
    assert add(100, 200, 300) == 300
    assert add(2) == 2
    assert add(4, -3, -2) == 2
    assert add(-1, -2, -3, -4) == -4
