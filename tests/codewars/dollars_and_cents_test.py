"""
Unit tests for Dollars and Cents challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.dollars_and_cents import format_money


@pytest.mark.parametrize("test_input, expected",
                         [(39.99, "$39.99"), (3, "$3.00"), (3.10, "$3.10"), (314.16, "$314.16")])
def test_format_money(test_input, expected):
    """
    Tests the Dollars and Cents solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert format_money(test_input) == expected
