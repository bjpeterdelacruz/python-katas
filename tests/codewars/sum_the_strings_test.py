"""
Unit tests for Sum the Strings challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_the_strings import sum_str


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("4", "", "4"), ("", "5", "5"), ("4", "5", "9"), ("", "", "0"),
                          ("-1", "1", "0")])
def test_sum_str(test_input1, test_input2, expected):
    """
    Tests the Sum the Strings solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_str(test_input1, test_input2) == expected
