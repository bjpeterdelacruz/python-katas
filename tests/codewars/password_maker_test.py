"""
Unit tests for Password Maker challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.password_maker import make_password


@pytest.mark.parametrize("test_input, expected",
                         [("Give me liberty or give me death", "Gml0gmd"),
                          ("Keep Calm and Carry On", "KCaC0"),
                          ("If You Are the One", "1YAt0"),
                          ("Swimming in the Pacific Ocean", "51tP0")])
def test_make_password(test_input, expected):
    """
    Tests the Password Maker solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert make_password(test_input) == expected
