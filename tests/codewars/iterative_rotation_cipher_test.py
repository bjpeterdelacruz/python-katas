"""
Unit tests for Iterative Rotation Cipher challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.iterative_rotation_cipher import encode, decode


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(10, 'If you wish to make an apple pie from scratch, you must first '
                               'invent the universe.',
                           '10 hu fmo a,ys vi utie mr snehn rni tvte .ysushou teI fwea pmapi '
                           'apfrok rei tnocsclet'),
                          (14, 'True evil is a mundane bureaucracy.',
                           '14 daue ilev is a munbune Traurecracy.'),
                          (22, 'There is nothing more atrociously cruel than an adored child.',
                           '22 tareu oo iucnaTr dled oldthser.hg hiarm nhcn se rliyet oincoa'),
                          (36, 'As I was going up the stair\nI met a man who wasn\'t there!\n'
                               'He wasn\'t there again today,\nOh how I wish he\'d go away!',
                           '36 ws h weA dgIaa ug owh n!asrit git \n msm phw teaI\'e tanantwhe '
                           'reos\ns ther! aHeae \'gwadin\nt haw n htoo ,I\'i sy aohOy'),
                          (29, 'I avoid that bleak first hour of the working day during which '
                               'my still sluggish senses and body make every chore a penance.\n'
                               'I find that in arriving later, the work which I do perform is '
                               'of a much higher quality.',
                           '29 a r.lht niou gwryd aoshg gIsi mk lei adwhfci isd seensn rdohy mo '
                           'kleie oltbyhes a\naneu p.n rndr tehh irnne yifav t eo,raclhtc frpw '
                           'IIti im gwkaidhv aicufh ima doea eruhi y io qshhcoa kr ef l btah '
                           'gtrrse otnvugrt')])
def test_encode(test_input1, test_input2, expected):
    """
    Tests the Iterative Rotation Cipher solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert encode(test_input1, test_input2) == expected
    assert decode(encode(test_input1, test_input2)) == test_input2
