"""
Unit tests for Consecutive Items challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.consecutive_items import consecutive


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [([1, 2, 3], 0, 2, False), ([1, 2, 3], 2, 4, False),
                          ([2, 4, 6, 8], 4, 6, True), ([1, 3, 5, 7], 5, 3, True),
                          ([1, 2, 3, 4, 5], 2, 4, False)])
def test_consecutive(test_input1, test_input2, test_input3, expected):
    """
    Tests the Consecutive Items solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert consecutive(test_input1, test_input2, test_input3) == expected
