"""
Unit tests for Digits Explosion challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.digits_explosion import explode


@pytest.mark.parametrize("test_input, expected",
                         [("234019", "2233344441999999999"), ("0", ""), ("1", "1"),
                          ("123", "122333"), ("70", "7777777")])
def test_explode(test_input, expected):
    """
    Tests the Digits Explosion solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert explode(test_input) == expected
