"""
Unit tests for Is There an Odd Bit? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.is_there_an_odd_bit import any_odd


@pytest.mark.parametrize("test_input, expected",
                         [(2863311530, True),
                          (128, True),
                          (131, True),
                          (2, True),
                          (24082, True),
                          (0, False),
                          (85, False),
                          (1024, False),
                          (1, False),
                          (1365, False)])
def test_any_odd(test_input, expected):
    """
    Tests the Is There an Odd Bit? solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert any_odd(test_input) == expected
