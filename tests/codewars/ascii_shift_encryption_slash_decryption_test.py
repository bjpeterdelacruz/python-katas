"""
Unit tests for ASCII Shift Encryption Decryption challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.ascii_shift_encryption_slash_decryption import ascii_encrypt, ascii_decrypt


def test_encrypt_decrypt():
    """
    Tests the ASCII Shift Encryption Decryption solution.

    :return: None
    """
    assert ascii_encrypt("password") == "pbuv{txk"
    assert ascii_decrypt("pbuv{txk") == "password"
    assert ascii_encrypt("password") == ascii_decrypt(ascii_encrypt(ascii_encrypt("password")))
    assert ascii_decrypt(ascii_encrypt("Hello World!")) == "Hello World!"
