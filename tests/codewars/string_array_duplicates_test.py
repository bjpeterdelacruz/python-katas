"""
Unit tests for String Array Duplicates challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.string_array_duplicates import dup


@pytest.mark.parametrize("test_input, expected",
                         [(["ccooddddddewwwaaaaarrrrsssss", "piccaninny", "hubbubbubboo"],
                           ['codewars', 'picaniny', 'hubububo']),
                          (["abracadabra", "allottee", "assessee"],
                           ['abracadabra', 'alote', 'asese']),
                          (["kelless", "keenness"], ['keles', 'kenes']),
                          (["Woolloomooloo", "flooddoorroommoonlighters", "chuchchi"],
                           ['Wolomolo', 'flodoromonlighters', 'chuchchi']),
                          (["adanac", "soonness","toolless", "ppellee"],
                           ['adanac', 'sones', 'toles', 'pele']),
                          (["callalloo", "feelless", "heelless"], ['calalo', 'feles', 'heles']),
                          (["putteellinen", "keenness"], ['putelinen', 'kenes']),
                          (["kelless", "voorraaddoosspullen", "achcha"],
                           ['keles', 'voradospulen', 'achcha'])])
def test_dup(test_input, expected):
    """
    Tests the String Array Duplicates solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert dup(test_input) == expected
