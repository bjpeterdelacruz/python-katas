"""
Unit tests for Shifter Words challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.shifter_words import shifter


@pytest.mark.parametrize("test_input, expected",
                         [("WHO IS WHO", 2), ("WHO IS SHIFTER AND WHO IS NO", 3),
                          ("SOS IN THE HOME", 2), ("TASK", 0), ("", 0)])
def test_shifter(test_input, expected):
    """
    Tests the Shifter Words solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert shifter(test_input) == expected
