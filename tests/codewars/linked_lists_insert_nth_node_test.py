"""
Unit tests for Linked Lists: Insert Nth Node challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from utils.linked_list import build_one_two_three
from codewars.linked_lists_insert_nth_node import insert_nth


def test_insert_nth_empty_list():
    """
    Assert that a linked list is created when adding data to index 0 of a non-existing linked list.

    :return: None
    """
    assert insert_nth(None, 0, 12).data == 12
    assert insert_nth(None, 0, 12).next is None


def test_insert_nth_nonempty_list():
    """
    Assert that data is correctly added at the given index in the linked list.

    :return: None
    """
    assert insert_nth(build_one_two_three(), 0, 23).data == 23
    assert insert_nth(build_one_two_three(), 0, 23).next.data == 1
    assert insert_nth(build_one_two_three(), 0, 23).next.next.data == 2
    assert insert_nth(build_one_two_three(), 0, 23).next.next.next.data == 3
    assert insert_nth(build_one_two_three(), 0, 23).next.next.next.next is None

    assert insert_nth(build_one_two_three(), 1, 23).data == 1
    assert insert_nth(build_one_two_three(), 1, 23).next.data == 23
    assert insert_nth(build_one_two_three(), 1, 23).next.next.data == 2
    assert insert_nth(build_one_two_three(), 1, 23).next.next.next.data == 3
    assert insert_nth(build_one_two_three(), 1, 23).next.next.next.next is None

    assert insert_nth(build_one_two_three(), 2, 23).data == 1
    assert insert_nth(build_one_two_three(), 2, 23).next.data == 2
    assert insert_nth(build_one_two_three(), 2, 23).next.next.data == 23
    assert insert_nth(build_one_two_three(), 2, 23).next.next.next.data == 3
    assert insert_nth(build_one_two_three(), 2, 23).next.next.next.next is None

    assert insert_nth(build_one_two_three(), 3, 23).data == 1
    assert insert_nth(build_one_two_three(), 3, 23).next.data == 2
    assert insert_nth(build_one_two_three(), 3, 23).next.next.data == 3
    assert insert_nth(build_one_two_three(), 3, 23).next.next.next.data == 23
    assert insert_nth(build_one_two_three(), 3, 23).next.next.next.next is None


def test_fail_1():
    """
    Assert that an error is raised if attempting to add data past the end of the linked list.

    :return: None
    """
    with pytest.raises(ValueError):
        insert_nth(build_one_two_three(), 4, 23)
        assert False


def test_fail_2():
    """
    Assert that an error is raised if attempting to add data to an index not equal to zero for a
    linked list that does not exist.

    :return: None
    """
    with pytest.raises(ValueError):
        insert_nth(None, 1, 23)
        assert False
