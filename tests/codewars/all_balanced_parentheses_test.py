"""
Unit tests for Balanced Parentheses challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.all_balanced_parentheses import balanced_parens


@pytest.mark.parametrize("test_input, expected",
                         [(0, [""]), (1, ["()"]), (2, ["()()", "(())"]),
                          (3, ["()()()", "()(())", "(())()", "(()())", "((()))"])])
def test_balanced_parens(test_input, expected):
    """
    Tests the Balanced Parentheses solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert list(reversed(balanced_parens(test_input))) == expected
