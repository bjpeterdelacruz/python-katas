"""
Unit tests for Ensure Question challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.ensure_question import ensure_question


@pytest.mark.parametrize("test_input, expected",
                         [("", "?"), ("How are you?", "How are you?"), ("Yes", "Yes?")])
def test_ensure_question(test_input, expected):
    """
    Tests the Ensure Question solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert ensure_question(test_input) == expected
