"""
Unit tests for ISBN-10 Validation challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.isbn_10_validation import valid_isbn10


@pytest.mark.parametrize("test_input, expected",
                         [("1111", False), ("12345X7890", False), ("12345678AX", False),
                          ("1234554321", True), ("111222333X", False), ("111222333A", False)])
def test_valid_isbn10(test_input, expected):
    """
    Tests the ISBN-10 Validation solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert valid_isbn10(test_input) == expected
