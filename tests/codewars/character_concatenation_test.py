"""
Unit tests for Character Concatenation challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.character_concatenation import char_concat


@pytest.mark.parametrize("test_input, expected",
                         [("abc def", 'af1be2cd3'),
                          ("CodeWars", 'Cs1or2da3eW4'),
                          ("CodeWars Rocks", 'Cs1ok2dc3eo4WR5a 6rs7'),
                          ("1234567890", '101292383474565'),
                          ("$'D8KB)%PO@s", "$s1'@2DO38P4K%5B)6")])
def test_char_concat(test_input, expected):
    """
    Tests the Character Concatenation solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert char_concat(test_input) == expected
