"""
Unit tests for Word to Binary challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.word_to_binary import word_to_bin


@pytest.mark.parametrize("test_input, expected",
                         [('man', ['01101101', '01100001', '01101110']),
                          ('AB', ['01000001', '01000010']),
                          ('wecking',
                           ['01110111', '01100101', '01100011', '01101011', '01101001', '01101110',
                            '01100111']),
                          ('Ruby', ['01010010', '01110101', '01100010', '01111001']),
                          ('Yosemite',
                           ['01011001', '01101111', '01110011', '01100101', '01101101', '01101001',
                            '01110100', '01100101'])])
def test_word_to_bin(test_input, expected):
    """
    Tests the Word to Binary solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert word_to_bin(test_input) == expected
