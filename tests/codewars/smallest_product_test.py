"""
Unit tests for Smallest Product challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.smallest_product import smallest_product


@pytest.mark.parametrize("test_input, expected",
                         [([[3, 2], [1, 2, 1], [7, 8]], 2),
                          ([[10], [3, 0], [-1, -6, 2]], 0),
                          ([[-1], [5, 0], [3]], -1)])
def test_smallest_product(test_input, expected):
    """
    Tests the Smallest Product solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert smallest_product(test_input) == expected
