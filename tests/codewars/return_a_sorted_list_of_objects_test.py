"""
Unit tests for Return a Sorted List of Objects challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.return_a_sorted_list_of_objects import sort_list


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("x", [], []), ("b", [
                             {"a": 2, "b": 2},
                             {"a": 3, "b": 40},
                             {"a": 1, "b": 12}
                         ], [{"a": 3, "b": 40},
                             {"a": 1, "b": 12},
                             {"a": 2, "b": 2}
                             ]),
                          ("a", [
                              {"a": 4, "b": 3},
                              {"a": 2, "b": 2},
                              {"a": 3, "b": 40},
                              {"a": 1, "b": 12}
                          ], [{"a": 4, "b": 3},
                              {"a": 3, "b": 40},
                              {"a": 2, "b": 2},
                              {"a": 1, "b": 12}
                              ])])
def test_sort_list(test_input1, test_input2, expected):
    """
    Tests the Return a Sorted List of Objects solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert sort_list(test_input1, test_input2) == expected
