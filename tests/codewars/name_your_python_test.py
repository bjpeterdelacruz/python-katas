"""
Unit tests for Name Your Python challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.name_your_python import Python


def test_name_your_python():
    """
    Tests the Name Your Python solution.

    :return: None
    """
    python = Python("this is a test")
    assert python.name == "this is a test"
