"""
Unit tests for Weird IPv6 Hex String Parsing challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.weird_ipv6_hex_string_parsing import parse_ipv6


@pytest.mark.parametrize("test_input, expected",
                         [("1234:5678:9ABC:D00F:1111:2222:3333:4445", "10264228481217"),
                          ("5454:FBFD:9ABC:AAAA:FFFF:2222:FBDE:0101", "18544240608532"),
                          ("0000:0000:0000:0000:0000:0000:0000:0000", "00000000"),
                          ("FFFF:FFFF:BBBB:CCCC:1212:AABC:0000:1111", "6060444864304"),
                          ("ACDD-0101-9ABC-AAAA-FFFF-2222-FBDE-ACCC", "48242406085346"),
                          ("5454rFBFDr9ABCrAA0ArFAFFr2222rFBDEr0101", "18544230558532"),
                          ("F234#5678#9ABC#D00F#1111#2222#3333#4485", "24264228481221")])
def test_parse_ipv6(test_input, expected):
    """
    Tests the Weird IPv6 Hex String Parsing solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert parse_ipv6(test_input) == expected
