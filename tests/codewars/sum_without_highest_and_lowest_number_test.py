"""
Unit tests for Sum without Highest and Lowest Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_without_highest_and_lowest_number import sum_array


@pytest.mark.parametrize("test_input, expected",
                         [(None, 0),
                          ([], 0),
                          ([3], 0),
                          ([-3], 0),
                          ([3, 5], 0),
                          ([-3, -5], 0),
                          ([6, 2, 1, 8, 10], 16),
                          ([6, 0, 1, 10, 10], 17),
                          ([-6, -20, -1, -10, -12], -28),
                          ([-6, 20, -1, 10, -12], 3)])
def test_sum_array(test_input, expected):
    """
    Tests the Sum without Highest and Lowest Number solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_array(test_input) == expected
