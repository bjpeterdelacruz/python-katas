"""
Unit tests for Alien Accent challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.alien_accent import convert


@pytest.mark.parametrize("test_input, expected",
                         [('Hello', 'Hellu'), ('Alaska', 'Olosko'), ('Orlando', 'Urlondu')])
def test_convert(test_input, expected):
    """
    Tests the Alien Accent solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert convert(test_input) == expected
