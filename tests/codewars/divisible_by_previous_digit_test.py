"""
Unit tests for Divisible by Previous Digit challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.divisible_by_previous_digit import divisible_by_last


@pytest.mark.parametrize("test_input, expected",
                         [(73312, [False, False, True, False, True]),
                          (2026, [False, True, False, True]),
                          (635, [False, False, False])])
def test_divisible_by_last(test_input, expected):
    """
    Tests the Divisible by Previous Digit solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert divisible_by_last(test_input) == expected
