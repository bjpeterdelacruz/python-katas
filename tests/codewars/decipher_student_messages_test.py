"""
Unit tests for Decipher Student Messages challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.decipher_student_messages import decipher_message


@pytest.mark.parametrize("test_input, expected",
                         [("Answer to Number 5 Part b", "ArNran u rstm5twob  e ePb")])
def test_decipher_message(test_input, expected):
    """
    Tests the Decipher Student Messages solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert decipher_message(test_input) == expected
