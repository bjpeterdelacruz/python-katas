"""
Unit tests for The Feast of Many Beasts challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.the_feast_of_many_beasts import feast


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("great blue heron", "garlic naan", True),
                          ("chickadee", "chocolate cake", True),
                          ("brown bear", "bear claw", False),
                          ("marmot", "mulberry tart", True),
                          ("porcupine", "pie", True),
                          ("cat", "yogurt", False),
                          ("electric eel", "lasagna", False),
                          ("slow loris", "salsas", True),
                          ("ox", "orange lox", True),
                          ("blue-footed booby", "blueberry", True)])
def test_feast(test_input1, test_input2, expected):
    """
    Tests the solution for this kata.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert feast(test_input1, test_input2) == expected
