"""
Unit tests for Make UpperCase challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.makeuppercase import make_upper_case


@pytest.mark.parametrize("test_input, expected",
                         [("abc123", "ABC123"), ("@#$_1a2b3c", "@#$_1A2B3C")])
def test_make_upper_case(test_input, expected):
    """
    Tests the Make UpperCase solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert make_upper_case(test_input) == expected
