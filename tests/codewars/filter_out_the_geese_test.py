"""
Unit tests for Filter Out the Geese challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.filter_out_the_geese import goose_filter


@pytest.mark.parametrize("test_input, expected",
                         [(["Mallard", "Hook Bill", "African", "Crested", "Pilgrim", "Toulouse",
                            "Blue Swedish"],
                           ["Mallard", "Hook Bill", "Crested", "Blue Swedish"]),
                          (["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"],
                           ["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"]),
                          (["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"], [])])
def test_goose_filter(test_input, expected):
    """
    Tests the Filter Out the Geese solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert goose_filter(test_input) == expected
