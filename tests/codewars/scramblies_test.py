"""
Unit tests for Scrambles challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.scramblies import scramble


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("katas", "steak", False), ("scriptingjava", "javascript", True)])
def test_scramble(test_input1, test_input2, expected):
    """
    Tests the Scrambles solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert scramble(test_input1, test_input2) == expected
