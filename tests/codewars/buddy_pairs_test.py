"""
Unit tests for Buddy Pairs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.buddy_pairs import buddy


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(10, 50, [48, 75]), (48, 50, [48, 75]), (271, 5128, [1050, 1925]),
                          (2177, 4357, "Nothing")])
def test_buddy(test_input1, test_input2, expected):
    """
    Tests the Buddy Pairs solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert buddy(test_input1, test_input2) == expected
