"""
Unit tests for After Midnight challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.after_midnight import day_and_time


@pytest.mark.parametrize("test_input, expected",
                         [(0, 'Sunday 00:00'),
                          (-3, 'Saturday 23:57'),
                          (45, 'Sunday 00:45'),
                          (759, 'Sunday 12:39'),
                          (1236, 'Sunday 20:36'),
                          (1447, 'Monday 00:07'),
                          (7832, 'Friday 10:32'),
                          (18876, 'Saturday 02:36'),
                          (259180, 'Thursday 23:40'),
                          (-349000, 'Tuesday 15:20')])
def test_day_and_time(test_input, expected):
    """
    Tests the After Midnight solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert day_and_time(test_input) == expected
