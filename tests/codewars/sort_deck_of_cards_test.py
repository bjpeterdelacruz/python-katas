"""
Unit tests for Sort Deck of Cards challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.sort_deck_of_cards import sort_cards

TEST_CARDS = [
    'A', '3', 'T', 'T824Q', 'QKJ6932', 'J69327A8', 'J679J7327A8',
    'TA8AAA24AQA', 'AAAAAAAAAAAAA', '39A5T824Q7J6K', 'Q286JK395A47T',
    '54TQKJ69327A8', 'Q286JK395A47TQ286JK395A47T',
    'Q286JKKKKK395AAA47TQ286JK395A47T',
    'AAAAAAAAAAAAAQ286JK395A47TQ286JK395A47T'
]


def test_sort_cards():
    """
    Tests the Sort Deck of Cards solution.

    :return: None
    """
    for cards in TEST_CARDS:
        assert sort_cards(cards) == sorted(cards, key='A23456789TJQK'.index)
