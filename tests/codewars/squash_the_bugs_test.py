"""
Unit tests for Squash the Bugs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.squash_the_bugs import find_longest


@pytest.mark.parametrize("test_input, expected",
                         [("The quick white fox jumped around the massive dog", 7),
                          ("Take me to tinseltown with you", 10),
                          ("Sausage chops", 7),
                          ("Wind your body and wiggle your belly", 6),
                          ("Lets all go on holiday", 7)])
def test_find_longest(test_input, expected):
    """
    Tests the Squash the Bugs solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_longest(test_input) == expected
