"""
Unit tests for Remove First and Last Character (Part 2) challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.remove_first_and_last_character_part_two import array


@pytest.mark.parametrize("test_input, expected",
                         [('', None),
                          ('1', None),
                          ('1, 3', None),
                          ('1,2,3', '2'),
                          ('1,2,3,4', '2 3')])
def test_array(test_input, expected):
    """
    Tests the Remove First and Last Character (Part 2) solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert array(test_input) == expected
