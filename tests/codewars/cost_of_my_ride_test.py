"""
Unit tests for Cost of My Ride challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.cost_of_my_ride import insurance


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(27, "economy", 5, 250), (32, "medium", 6, 360),
                          (18, "truck", -1, 0), (24, "medium", 0, 0), (24, "BMW", 4, 300)])
def test_insurance(test_input1, test_input2, test_input3, expected):
    """
    Tests the Cost of My Ride solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert insurance(test_input1, test_input2, test_input3) == expected
