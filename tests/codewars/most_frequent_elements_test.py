"""
Unit tests for Most Frequent Elements challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.most_frequent_elements import find_most_frequent


@pytest.mark.parametrize("test_input, expected",
                         [([1, 1, 2, 3], {1}),
                          ([1, 1, 2, 2, 3], {1, 2}),
                          ([], set()),
                          ([1, 1, '2', '2', 3], {1, '2'}),
                          ([None, None], {None})])
def test_find_most_frequent(test_input, expected):
    """
    Tests the Most Frequent Elements solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_most_frequent(test_input) == expected
