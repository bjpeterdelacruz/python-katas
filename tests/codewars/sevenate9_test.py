"""
Unit tests for Seven Ate 9 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sevenate9 import seven_ate9


@pytest.mark.parametrize("test_input, expected",
                         [("7979797", "7777"), ("79712312", "7712312")])
def test_seven_ate9(test_input, expected):
    """
    Tests the Seven Ate 9 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert seven_ate9(test_input) == expected
