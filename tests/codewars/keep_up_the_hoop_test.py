"""
Unit tests for Keep Up the Hoop challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.keep_up_the_hoop import hoop_count


@pytest.mark.parametrize("test_input, expected",
                         [(6, "Keep at it until you get it"), (10, "Great, now move on to tricks"),
                          (22, "Great, now move on to tricks")])
def test_hoop_count(test_input, expected):
    """
    Tests the Keep Up the Hoop solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert hoop_count(test_input) == expected
