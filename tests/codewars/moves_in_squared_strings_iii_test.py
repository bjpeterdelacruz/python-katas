"""
Unit tests for Moves in Squared Strings 3 challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.moves_in_squared_strings_iii import rot_90_clock, diag_1_sym, selfie_and_diag1, oper


def test_oper():
    """
    Tests the Moves in Squared Strings 3 solution.

    :return: None
    """
    assert oper(rot_90_clock, "rgavce\nvGcEKl\ndChZVW\nxNWgXR\niJBYDO\nSdmEKb") == \
           "Sixdvr\ndJNCGg\nmBWhca\nEYgZEv\nKDXVKc\nbORWle"
    assert oper(diag_1_sym, "wuUyPC\neNHWxw\nehifmi\ntBTlFI\nvWNpdv\nIFkGjZ") == \
           "weetvI\nuNhBWF\nUHiTNk\nyWflpG\nPxmFdj\nCwiIvZ"
    assert oper(selfie_and_diag1, "NJVGhr\nMObsvw\ntPhCtl\nsoEnhi\nrtQRLK\nzjliWg") == \
           "NJVGhr|NMtsrz\nMObsvw|JOPotj\ntPhCtl|VbhEQl" \
           "\nsoEnhi|GsCnRi\nrtQRLK|hvthLW\nzjliWg|rwliKg"
