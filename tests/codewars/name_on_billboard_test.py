"""
Unit tests for Name on Billboard challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.name_on_billboard import billboard


@pytest.mark.parametrize("test_input, expected",
                         [('', 0), ('BJ', 60), ('a b c', 150)])
def test_billboard_1(test_input, expected):
    """
    Tests the Name on Billboard solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert billboard(test_input) == expected


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('', 10, 0), ('BJ', 25, 50), ('a b c', 100, 500)])
def test_billboard_2(test_input1, test_input2, expected):
    """
    Tests the Name on Billboard solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert billboard(test_input1, test_input2) == expected
