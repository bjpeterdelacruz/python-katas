"""
Unit tests for Number of People in the Bus challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.number_of_people_in_the_bus import number


@pytest.mark.parametrize("test_input, expected",
                         [([[10, 0], [3, 5], [5, 8]], 5),
                          ([[3, 0], [9, 1], [4, 10], [12, 2], [6, 1], [7, 10]], 17),
                          ([[3, 0], [9, 1], [4, 8], [12, 2], [6, 1], [7, 8]], 21),
                          ([[0, 0]], 0)])
def test_number(test_input, expected):
    """
    Tests the Number of People in the Bus solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert number(test_input) == expected
