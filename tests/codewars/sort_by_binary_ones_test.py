"""
Unit tests for Sort by Binary Ones challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sort_by_binary_ones import sort_by_binary_ones


@pytest.mark.parametrize("test_input, expected",
                         [([1, 3], [3, 1]),
                          ([1, 2, 3, 4], [3, 1, 2, 4]),
                          ([1, 2, 3, 4], [3, 1, 2, 4]),
                          ([1, 3], [3, 1]),
                          ([1, 15, 7, 3, 5], [15, 7, 3, 5, 1]),
                          ([80, 21], [21, 80]),
                          ([0, 1024, 33], [33, 1024, 0]),
                          ([2, 2048, 3], [3, 2, 2048]),
                          ([5, 2049, 3], [3, 5, 2049]),
                          ([1, 5, 21, 7, 44, 99, 50, 51, 49, 80, 33, 25],
                           [51, 99, 7, 21, 25, 44, 49, 50, 5, 33, 80, 1])])
def test_sort_by_binary_ones(test_input, expected):
    """
    Tests the Sort by Binary Ones solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sort_by_binary_ones(test_input) == expected
