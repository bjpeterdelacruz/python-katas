"""
Unit tests for Strings Starts With challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.strings_starts_with import starts_with


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("hello world!", "hello", True),
                          ("hello world!", "HELLO", False),
                          ("nowai", "nowaisir", False),
                          ("", "", True),
                          ("abc", "", True),
                          ("", "abc", False)])
def test_starts_with(test_input1, test_input2, expected):
    """
    Tests the Strings Starts With solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert starts_with(test_input1, test_input2) == expected
