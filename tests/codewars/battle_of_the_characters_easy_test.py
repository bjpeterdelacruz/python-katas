"""
Unit tests for Battle of the Characters challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.battle_of_the_characters_easy import battle


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("AAA", "Z", "Z"),
                          ("ONE", "TWO", "TWO"),
                          ("ONE", "NEO", "Tie!"),
                          ("FOUR", "FIVE", "FOUR"),
                          ("QWERTY", "ASDFGH", "QWERTY"),
                          ("QAZWSX", "VFREDC", "QAZWSX"),
                          ("SKDUFR", "QWEVPD", "QWEVPD"),
                          ("PLOKIJ", "ZNMASK", "ZNMASK"),
                          ("FREDAS", "TSOBES", "TSOBES")])
def test_battle(test_input1, test_input2, expected):
    """
    Tests the Battle of the Characters solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert battle(test_input1, test_input2) == expected
