"""
Unit tests for Thinkful: Lists of Lists challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_list_and_loop_drills_lists_of_lists import process_data


@pytest.mark.parametrize("test_input, expected",
                         [([[2, 1], [5, 3], [7, 4], [10, 6]], 24), ([[5, 4], [6, 4]], 2),
                          ([[2, 9], [2, 4], [7, 5]], 28), ([[2, 5], [3, 4], [8, 7]], 3)])
def test_process_data(test_input, expected):
    """
    Tests the Thinkful: Lists of Lists solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert process_data(test_input) == expected
