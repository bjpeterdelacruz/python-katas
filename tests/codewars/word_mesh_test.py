"""
Unit tests for Word Mesh challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.word_mesh import word_mesh


@pytest.mark.parametrize("test_input, expected",
                         [(["allow", "lowering", "ringmaster", "terror"], "lowringter"),
                          (["kingdom", "dominator", "notorious", "usual", "allegory"],
                           "failed to mesh"),
                          (["beacon", "condominium", "umbilical", "california"], "conumcal"),
                          (["abandon", "donation", "onion", "ongoing"], "dononon"),
                          (["jamestown", "ownership", "hippocampus", "pushcart", "cartographer",
                            "pheromone"], "ownhippuscartpher"),
                          (['xinxgea', 'terrainmap', 'rooseveltmappingtheamazon',
                            'zonedforhousing'], "failed to mesh")])
def test_valid_parentheses(test_input, expected):
    """
    Tests the Word Mesh solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert word_mesh(test_input) == expected
