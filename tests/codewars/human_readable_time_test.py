"""
Unit tests for Human Readable Time challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.human_readable_time import make_readable


@pytest.mark.parametrize("test_input, expected",
                         [(359999, "99:59:59"), (86400, "24:00:00"), (3599, "00:59:59")])
def test_make_readable(test_input, expected):
    """
    Tests the Human Readable Time solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert make_readable(test_input) == expected
