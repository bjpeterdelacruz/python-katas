"""
Unit tests for ROT13 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.rot13 import rot13


@pytest.mark.parametrize("test_input, expected",
                         [("EBG13 rknzcyr.", "ROT13 example."),
                          ("This is my first ROT13 exercise!", "Guvf vf zl svefg EBG13 rkrepvfr!")])
def test_rot13(test_input, expected):
    """
    Tests the ROT13 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert rot13(test_input) == expected
