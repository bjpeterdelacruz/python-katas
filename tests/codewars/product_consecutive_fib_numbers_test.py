"""
Unit tests for Product of Consecutive Fibonacci Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.product_consecutive_fib_numbers import product_fib


@pytest.mark.parametrize("test_input, expected",
                         [(4895, [55, 89, True]), (4995, [89, 144, False])])
def test_product_fib(test_input, expected):
    """
    Tests the Product of Consecutive Fibonacci Numbers solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert product_fib(test_input) == expected
