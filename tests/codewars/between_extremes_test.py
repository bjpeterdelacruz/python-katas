"""
Unit tests for Between Extremes challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.between_extremes import between_extremes


@pytest.mark.parametrize("test_input, expected",
                         [([1, 1], 0),
                          ([-1, -1], 0),
                          ([1, -1], 2),
                          ([21, 34, 54, 43, 26, 12], 42),
                          ([-1, -41, -77, -100], 99)])
def test_between_extremes(test_input, expected):
    """
    Tests the Between Extremes solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert between_extremes(test_input) == expected
