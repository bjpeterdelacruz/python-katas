"""
Unit tests for Calculate Two People's Individual Ages challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.calculate_two_peoples_individual_ages import get_ages


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(24, 4, (14, 10)),
                          (30, 6, (18, 12)),
                          (70, 10, (40, 30)),
                          (18, 4, (11, 7)),
                          (63, 14, (38.5, 24.5)),
                          (80, 80, (80, 0)),
                          (63, -14, None),
                          (-22, 15, None)])
def test_get_ages(test_input1, test_input2, expected):
    """
    Tests the Calculate Two People's Individual Ages solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_ages(test_input1, test_input2) == expected
