"""
Unit tests for Calculate Meal Total challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.calculate_meal_total import calculate_total


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(5.00, 5, 10, 5.75),
                          (36.97, 7, 15, 45.10),
                          (0.00, 6, 18, 0.00),
                          (80.94, 0, 20, 97.13),
                          (54.96, 8, 0, 59.36)])
def test_calculate_total(test_input1, test_input2, test_input3, expected):
    """
    Tests the Calculate Meal Total solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert calculate_total(test_input1, test_input2, test_input3) == expected
