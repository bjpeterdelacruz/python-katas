"""
Unit tests for Count the Digits challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.count_the_digit import nb_dig


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(5750, 0, 4700),
                          (11011, 2, 9481),
                          (12224, 8, 7733),
                          (11549, 1, 11905),
                          (14550, 7, 8014),
                          (8304, 7, 3927),
                          (10576, 9, 7860),
                          (12526, 1, 13558),
                          (7856, 4, 7132),
                          (14956, 1, 17267)])
def test_alphabet_war(test_input1, test_input2, expected):
    """
    Tests the Count the Digits solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert nb_dig(test_input1, test_input2) == expected
