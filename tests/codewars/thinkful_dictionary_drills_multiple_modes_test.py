"""
Unit tests for Thinkful: Multiple Modes challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_dictionary_drills_multiple_modes import modes


@pytest.mark.parametrize("test_input, expected",
                         [("tomato", ["o", "t"]), ([1, 3, 3, 7], [3]), ("redder", [])])
def test_modes(test_input, expected):
    """
    Tests the Thinkful: Multiple Modes solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert modes(test_input) == expected
