"""
Unit tests for Thinkful: Vectors challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.thinkful_object_drills_vectors import Vector


def test_vector():
    """
    Tests the Thinkful: Vectors solution.

    :return: None
    """
    vector1 = Vector(1, 2)
    assert vector1.x == 1
    assert vector1.y == 2

    vector2 = Vector(3, 5)
    assert vector1.add(vector2) == Vector(4, 7)
