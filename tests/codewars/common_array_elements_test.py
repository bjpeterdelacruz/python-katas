"""
Unit tests for Common Array Elements challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.common_array_elements import common


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [([1, 2, 3], [5, 3, 2], [7, 3, 2], 5),
                          ([1], [1], [2], 0),
                          ([2, 2, 3], [1, 2, 2], [2, 2, 4], 4)])
def test_common(test_input1, test_input2, test_input3, expected):
    """
    Tests the Common Array Elements solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert common(test_input1, test_input2, test_input3) == expected
