"""
Unit tests for Happy Birthday challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.happy_birthday import wrap


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(17, 32, 11, 162), (13, 13, 13, 124), (1, 3, 1, 32)])
def test_wrap(test_input1, test_input2, test_input3, expected):
    """
    Tests the Happy Birthday solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert wrap(test_input1, test_input2, test_input3) == expected
