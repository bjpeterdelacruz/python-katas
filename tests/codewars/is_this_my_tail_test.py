"""
Unit tests for Is This My Tail? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.is_this_my_tail import correct_tail


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("lion", "n", True), ("fox", "c", False)])
def test_correct_tail(test_input1, test_input2, expected):
    """
    Tests the Is This My Tail? solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert correct_tail(test_input1, test_input2) == expected
