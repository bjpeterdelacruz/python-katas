"""
Unit tests for ■□ Pattern □■ : Wave challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.number_number_pattern_number_number_wave import draw


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 3, 4], "\n".join(["□□□■", "□□■■", "□■■■", "■■■■"])),
                          ([1, 2, 3, 3, 2, 1], "\n".join(["□□■■□□", "□■■■■□", "■■■■■■"])),
                          ([1, 2, 3, 3, 2, 1, 1, 2, 3, 4, 5, 6, 7], "\n".join(
                              ["□□□□□□□□□□□□■", "□□□□□□□□□□□■■", "□□□□□□□□□□■■■", "□□□□□□□□□■■■■",
                               "□□■■□□□□■■■■■", "□■■■■□□■■■■■■", "■■■■■■■■■■■■■"])),
                          ([5, 3, 1, 2, 4, 6, 5, 4, 2, 3, 5, 2, 1], "\n".join(
                              ["□□□□□■□□□□□□□", "■□□□□■■□□□■□□", "■□□□■■■■□□■□□", "■■□□■■■■□■■□□",
                               "■■□■■■■■■■■■□", "■■■■■■■■■■■■■"])),
                          ([1, 0, 1, 0, 1, 0, 1, 0], "■□■□■□■□")])
def test_draw(test_input, expected):
    """
    Tests the ■□ Pattern □■ : Wave solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert draw(test_input) == expected
