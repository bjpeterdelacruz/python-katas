"""
Unit tests for Filter Coffee challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.filter_coffee import search


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(3, [6, 1, 2, 9, 2], "1,2,2"),
                          (14, [7, 3, 23, 9, 14, 20, 7], "3,7,7,9,14"),
                          (0, [6, 1, 2, 9, 2], ""),
                          (10, [], ""),
                          (24, [24, 0, 100, 2, 5], "0,2,5,24"),
                          (24, [2.7, 0, 100.9, 1, 5.5], "0,1,2.7,5.5"),
                          (-1, [1, 2, 3, 4], ""),
                          (-1, [-1, 0, 1, 2, 3, 4], "-1"),
                          (14, [17, 33, 23, 19, 19, 20, 17], ""),
                          (14, [13, 15, 14, 14, 15, 13], "13,13,14,14")])
def test_search(test_input1, test_input2, expected):
    """
    Tests the Filter Coffee solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert search(test_input1, test_input2) == expected
