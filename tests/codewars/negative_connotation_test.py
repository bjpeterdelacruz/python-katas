"""
Unit tests for Negative Connotation challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.negative_connotation import connotation


@pytest.mark.parametrize("test_input, expected",
                         [("A big brown fox caught a bad bunny", True),
                          ("Xylophones can obtain Xenon.", False),
                          ("CHOCOLATE MAKES A GREAT SNACK", True),
                          ("All FOoD tAsTEs NIcE for someONe", True),
                          ("Is  this the  best  Kata?", True),
                          ("Coats and jackets make good garments.", True),
                          (
                          "Cranberries and newly obtained salted caramel tastes wonderful.", False),
                          ("Water is often required on school trips!", False),
                          ("Humour intrigues and astounds Data.", True)])
def test_connotation(test_input, expected):
    """
    Tests the Negative Connotation solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert connotation(test_input) == expected
