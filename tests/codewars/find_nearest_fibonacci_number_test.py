"""
Unit tests for Find the Nearest Fibonacci Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_nearest_fibonacci_number import nearest_fibonacci


@pytest.mark.parametrize("test_input, expected",
                         [(100, 89), (120, 144), (17, 13), (0, 0)])
def test_count_bits(test_input, expected):
    """
    Tests the Find the Nearest Fibonacci Number solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert nearest_fibonacci(test_input) == expected
