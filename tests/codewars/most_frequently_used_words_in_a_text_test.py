"""
Unit tests for Most Frequently Used Words challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.most_frequently_used_words_in_a_text import top_3_words


@pytest.mark.parametrize("test_input, expected",
                         [("""In a village of La Mancha, the name of which I have no desire to call
to mind, there lived not long since one of those gentlemen that keep a lance
in the lance-rack, an old buckler, a lean hack, and a greyhound for
coursing. An olla of rather more beef than mutton, a salad on most
nights, scraps on Saturdays, lentils on Fridays, and a pigeon or so extra
on Sundays, made away with three-quarters of his income.""", ['a', 'of', 'on']),
                          ("a a a  b  c c  d d d d  e e e e e", ["e", "d", "a"]),
                          ("e e e e DDD ddd DdD: ddd ddd aa aA Aa, bb cc cC e e e",
                           ["e", "ddd", "aa"]),
                          ("  //wont won't won't ", ["won't", "wont"]), ("  , e   .. ", ["e"]),
                          ("  ...  ", []), ("  '  ", []), ("  '''  ", []),
                          ("'vC,bdnaYaWPI-dKdndTn/:!:dKdndTn .;bdnaYaWPI/;?_.dKdndTn.;,__'vC;"
                           "?_bdnaYaWPI:bdnaYaWPI-? -dKdndTn.__'vC/dKdndTn_;;,.dKdndTn,/./ bd"
                           "naYaWPI,.?!.'vC :,?bdnaYaWPI!-?./'vC-:?/bdnaYaWPI,:.:dKdndTn;,:dK"
                           "dndTn_/'vC.,:bdnaYaWPI ;/ /dKdndTn/,:'vC,  ?bdnaYaWPI-.'vC_!_/?dK"
                           "dndTn.?dKdndTn;_dKdndTn_./ :bdnaYaWPI_,,!?dKdndTn?'vC/bdnaYaWPI!_"
                           ":!bdnaYaWPI-!_/.'vC.!,/;bdnaYaWPI-??_,dKdndTn!!.'vC .-dKdndTn- :;"
                           ".dKdndTn?'vC?bdnaYaWPI dKdndTn?dKdndTn  ?.dKdndTn; .'vC/dKdndTn_,"
                           ".;bdnaYaWPI!? !:bdnaYaWPI_dKdndTn!/dKdndTn/ .,dKdndTn :_/-bdnaYaW"
                           "PI_-?'vC.'vC :/?'vC. /?/'vC;?bdnaYaWPI;_?'vC_ bdnaYaWPI_bdnaYaWPI"
                           ",;dKdndTn?bdnaYaWPI;?,?bdnaYaWPI.? !!bdnaYaWPI ?dKdndTn.",
                           ['dkdndtn', 'bdnayawpi', "'vc"])])
def test_top_3_words(test_input, expected):
    """
    Tests the Most Frequently Used Words solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert top_3_words(test_input) == expected
