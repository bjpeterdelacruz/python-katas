"""
Unit tests for Linked Lists: Get Nth Node challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from utils.linked_list import Node, build_one_two_three
from codewars.linked_lists_get_nth_node import get_nth


def test_get_nth():
    """
    Assert that the correct value is returned for a given index.

    :return: None
    """
    assert get_nth(Node(99), 0).data == 99
    assert get_nth(build_one_two_three(), 0).data == 1
    assert get_nth(build_one_two_three(), 1).data == 2
    assert get_nth(build_one_two_three(), 2).data == 3


def test_fail_1():
    """
    Assert that an error is raised if an index is passed in for a non-existing list.

    :return: None
    """
    with pytest.raises(ValueError):
        get_nth(None, 0)
        assert False


def test_fail_2():
    """
    Assert that an error is raised if the index is negative.

    :return: None
    """
    with pytest.raises(ValueError):
        get_nth(build_one_two_three(), -1)
        assert False


def test_fail_3():
    """
    Assert that an error is raised if the index is equal to the length of the list.

    :return: None
    """
    with pytest.raises(ValueError):
        get_nth(build_one_two_three(), 3)
        assert False


def test_fail_4():
    """
    Assert that an error is raised if the index goes past the end of the list.

    :return: None
    """
    with pytest.raises(ValueError):
        get_nth(build_one_two_three(), 99)
        assert False
