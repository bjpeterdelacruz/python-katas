"""
Unit tests for Scratch Lottery 1 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_fun_number_320_scratch_lottery_i import scratch


@pytest.mark.parametrize("test_input, expected",
                         [(["tiger tiger tiger 100", "rabbit dragon snake 100", "rat ox pig 1000",
                            "dog cock sheep 10", "horse monkey rat 5", "dog dog dog 1000"], 1100)
                          ])
def test_scratch(test_input, expected):
    """
    Tests the Scratch Lottery 1 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert scratch(test_input) == expected
