"""
Unit tests for Excel Sheet Column Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.excel_sheet_column_numbers import title_to_number


@pytest.mark.parametrize("test_input, expected",
                         [('A', 1),
                          ('Z', 26),
                          ('AA', 27),
                          ('AZ', 52),
                          ('BA', 53),
                          ('CODEWARS', 28779382963),
                          ('ZZZTOP', 321268054),
                          ('OYAJI', 7294985),
                          ('LONELINESS', 68400586976949),
                          ('UNFORGIVABLE', 79089429845931757)])
def test_title_to_number(test_input, expected):
    """
    Tests the Excel Sheet Column Numbers solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert title_to_number(test_input) == expected
