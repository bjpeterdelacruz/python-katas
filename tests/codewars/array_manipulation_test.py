"""
Unit tests for Array Manipulation challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.array_manipulation import array_manip


@pytest.mark.parametrize("test_input, expected",
                         [([8, 58, 71, 18, 31, 32, 63, 92, 43, 3, 91, 93, 25, 80, 28],
                           [18, 63, 80, 25, 32, 43, 80, 93, 80, 25, 93, -1, 28, -1, -1]),
                          ([2, 4, 18, 16, 7, 3, 9, 13, 18, 10],
                           [3, 7, -1, 18, 9, 9, 10, 18, -1, -1])])
def test_array_manip(test_input, expected):
    """
    Tests the Array Manipulation solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert array_manip(test_input) == expected
