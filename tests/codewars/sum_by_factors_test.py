"""
Unit tests for Sum by Factors challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_by_factors import sum_by_factors


@pytest.mark.parametrize("test_input, expected",
                         [([107, 158, 204, 100, 118, 123, 126, 110, 116, 100],
                           [[2, 1032], [3, 453], [5, 310], [7, 126], [11, 110], [17, 204],
                            [29, 116], [41, 123], [59, 118], [79, 158], [107, 107]])])
def test_buddy(test_input, expected):
    """
    Tests the Sum by Factors solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_by_factors(test_input) == expected
