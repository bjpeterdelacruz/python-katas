"""
Unit tests for Triangular Range challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.triangular_range import triangular_range


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(1, 3, {1: 1, 2: 3}), (5, 16, {3: 6, 4: 10, 5: 15})])
def test_triangular_range(test_input1, test_input2, expected):
    """
    Tests the Unexpected Parsing solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert triangular_range(test_input1, test_input2) == expected
