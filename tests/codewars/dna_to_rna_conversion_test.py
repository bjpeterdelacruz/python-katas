"""
Unit tests for DNA to RNA Conversion challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.dna_to_rna_conversion import dna_to_rna


@pytest.mark.parametrize("test_input, expected",
                         [('TTTT', 'UUUU'), ('GCAT', 'GCAU'), ('CACAG', 'CACAG')])
def test_dna_to_rna(test_input, expected):
    """
    Tests the DNA to RNA Conversion solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert dna_to_rna(test_input) == expected
