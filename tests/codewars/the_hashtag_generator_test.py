"""
Unit tests for Hashtag Generator challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.the_hashtag_generator import generate_hashtag


@pytest.mark.parametrize("test_input, expected",
                         [('', False), ("Codewars", "#Codewars"),
                          ("Codewars Is Nice", "#CodewarsIsNice"),
                          ("codewars is nice", "#CodewarsIsNice"),
                          ("Codewars      ", "#Codewars"), ("CodeWars is nice", "#CodewarsIsNice"),
                          ("c i n", "#CIN"), ("codewars  is  nice", "#CodewarsIsNice"),
                          ("Loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
                           "ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
                           "oooooooooooooong Cat", False)])
def test_generate_hashtag(test_input, expected):
    """
    Tests the Hashtag Generator solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert generate_hashtag(test_input) == expected
    if expected:
        assert expected[0] == '#'
