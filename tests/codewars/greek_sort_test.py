"""
Unit tests for Greek Sort challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.greek_sort import greek_comparator


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("alpha", "beta", -1), ("chi", "chi", 0), ("upsilon", "rho", 1)])
def test_greek_comparator(test_input1, test_input2, expected):
    """
    Tests the Greek Sort solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert greek_comparator(test_input1, test_input2) == expected
