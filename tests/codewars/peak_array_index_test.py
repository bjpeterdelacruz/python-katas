"""
Unit tests for Peak Array Index challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.peak_array_index import peak


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 3, 5, 3, 2, 1], 3), ([1, 12, 3, 3, 6, 3, 1], 2),
                          ([10, 20, 30, 40], -1)])
def test_peak(test_input, expected):
    """
    Tests the Peak Array Index solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert peak(test_input) == expected
