"""
Unit tests for Mexican Wave challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.mexican_wave import wave


@pytest.mark.parametrize("test_input, expected",
                         [('hello', ["Hello", "hEllo", "heLlo", "helLo", "hellO"]),
                          ("it is", ["It is", "iT is", "it Is", "it iS"]),
                          (" gap ", [" Gap ", " gAp ", " gaP "]), ("", [])])
def test_wave(test_input, expected):
    """
    Tests the Mexican Wave solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert wave(test_input) == expected
