"""
Unit tests for Remove Duplicates from List challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.remove_duplicates_from_list import distinct


@pytest.mark.parametrize("test_input, expected",
                         [([1], [1]),
                          ([1, 2], [1, 2]),
                          ([1, 1, 2], [1, 2]),
                          ([1, 1, 1, 2, 3, 4, 5], [1, 2, 3, 4, 5]),
                          ([1, 2, 2, 3, 3, 4, 4, 5, 6, 7, 7, 7], [1, 2, 3, 4, 5, 6, 7])])
def test_distinct(test_input, expected):
    """
    Tests the Remove Duplicates from List solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert distinct(test_input) == expected
