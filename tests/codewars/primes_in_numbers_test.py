"""
Unit tests for Primes in Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.primes_in_numbers import prime_factors


@pytest.mark.parametrize("test_input, expected",
                         [(100, "(2**2)(5**2)"), (10, "(2)(5)"), (11, "(11)"), (28, "(2**2)(7)")])
def test_prime_factors(test_input, expected):
    """
    Tests the Primes in Numbers solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert prime_factors(test_input) == expected
