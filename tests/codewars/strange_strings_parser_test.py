"""
Unit tests for Strange Strings Parser challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.strange_strings_parser import word_splitter


@pytest.mark.parametrize("test_input, expected",
                         [("56&SHORT!BEACH+WEST=HOUSE", ['56', 'SHORT', 'BEACH', 'WEST', 'HOUSE']),
                          ("320000;56C:7200RPM#MODE%65>LATCH?ON",
                           ['320000', '56C', '7200RPM', 'MODE', '65', 'LATCH', 'ON']),
                          ("32.0500;-6C:PITCH=-72#ROLL!21.3*REGISTER:90.345689&ARMED?-25",
                           ['32.0500', '-6C', 'PITCH', '-72', 'ROLL', '21.3', 'REGISTER',
                            '90.345689', 'ARMED', '-25'])])
def test_word_splitter(test_input, expected):
    """
    Tests the Strange Strings Parser solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert word_splitter(test_input) == expected
