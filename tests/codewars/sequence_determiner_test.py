"""
Unit tests for Sequence Determiner challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sequence_determiner import determine_sequence


@pytest.mark.parametrize("test_input, expected",
                         [([2, 5, 8, 11, 14], 0),
                          ([1, 2, 4, 8, 16], 1),
                          ([1, 2, 1, 3, 4, 5], -1),
                          ([1, 1, 1, 1, 1], 2),
                          ([2, 5, 8, 10, 11], -1),
                          ([1, 2, 4, 7, 16], -1),
                          ([1, 2, 4, 8, 15], -1),
                          ([1, 2, 4, 8, 25], -1),
                          ([2, 5, 8, 20, 14], -1),
                          ([2, 2, 2, 8, 16], -1),
                          ([0, 0, 0, 0, 0], 0),
                          ([5, 5, 5, 5, 5], 2),
                          ([47, 47, 47, 47, 47], 2)])
def test_determine_sequence(test_input, expected):
    """
    Tests the Sequence Determiner solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert determine_sequence(test_input) == expected
