"""
Unit tests for Check Digit challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.check_digit_1 import check_digit


@pytest.mark.parametrize("test_input1, test_input2, test_input3, test_input4, expected",
                         [(1234567, 1, 0, 1, True), (1234567, 0, 1, 2, True),
                          (67845123654, 4, 2, 4, True), (6668844536485, 0, 0, 6, True),
                          (9999999999, 2, 5, 1, False)])
def test_check_digit(test_input1, test_input2, test_input3, test_input4, expected):
    """
    Tests the Check Digit solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param test_input4: Test input
    :param expected: Expected result
    :return: None
    """
    assert check_digit(test_input1, test_input2, test_input3, test_input4) == expected
