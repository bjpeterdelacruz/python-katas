"""
Unit tests for Hello, Name or World! challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.hello_name_or_world import hello


@pytest.mark.parametrize("test_input, expected",
                         [('', 'World'), ('Ayaka', 'Ayaka'), ('e', 'E')])
def test_hello_1(test_input, expected):
    """
    Tests the Hello, Name or World! solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert hello(test_input) == f"Hello, {expected}!"


def test_hello_2():
    """
    Tests the Hello, Name or World! solution.

    :return: None
    """
    assert hello() == "Hello, World!"
