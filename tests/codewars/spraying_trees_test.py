"""
Unit tests for Spraying Trees challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.spraying_trees import task


@pytest.mark.parametrize("test_input1, test_input2, test_input3",
                         [("Monday", 5, 2), ("Tuesday", 4, 3), ("Wednesday", 3, 4),
                          ("Thursday", 2, 5), ("Friday", 0, 1)])
def test_task(test_input1, test_input2, test_input3):
    """
    Tests the Spraying Trees solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :return: None
    """
    people = {'Monday': 'James', 'Tuesday': 'John', 'Wednesday': 'Robert',
              'Thursday': 'Michael', 'Friday': 'William'}
    expected = f'It is {test_input1} today, {people[test_input1]}, you have to work, ' \
               f'you must spray {test_input2} trees and you need ' \
               f'{test_input2 * test_input3} dollars to buy liquid'
    assert task(test_input1, test_input2, test_input3) == expected
