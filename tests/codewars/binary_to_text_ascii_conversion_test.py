"""
Unit tests for Binary to Text (ASCII) Conversion challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.binary_to_text_ascii_conversion import binary_to_string


@pytest.mark.parametrize("test_input, expected",
                         [("", ""), (None, ""), ("00110001001100000011000100110001", "1011"),
                          ("0100100001100101011011000110110001101111", "Hello")])
def test_beeramid(test_input, expected):
    """
    Tests the Binary to Text (ASCII) Conversion solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert binary_to_string(test_input) == expected
