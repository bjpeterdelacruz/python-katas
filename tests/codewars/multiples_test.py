"""
Unit tests for Multiples challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.multiples import multiple


@pytest.mark.parametrize("test_input, expected",
                         [(30, "BangBoom"),
                          (3, "Bang"),
                          (98, "Miss"),
                          (65, "Boom"),
                          (23, "Miss"),
                          (15, "BangBoom"),
                          (4, "Miss"),
                          (3, "Bang"),
                          (2, "Miss"),
                          (45, "BangBoom"),
                          (90, "BangBoom"),
                          (21, "Bang"),
                          (7, "Miss"),
                          (6, "Bang"),
                          (3, "Bang"),
                          (10003823, "Miss"),
                          (41535, "BangBoom"),
                          (712, "Miss"),
                          (985, "Boom"),
                          (164523, "Bang")])
def test_multiple(test_input, expected):
    """
    Tests the Multiples solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert multiple(test_input) == expected
