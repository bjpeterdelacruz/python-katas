"""
Unit tests for Find the Index of the Second Occurrence of a Letter in a String challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_the_index_of_the_second_occurrence import second_symbol


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("hello", "l", 3),
                          ("llama", "l", 1),
                          ("yell", "l", 3),
                          ("how", "w", -1),
                          ("How are you?", "o", 9),
                          ("How are you?", "y", -1)])
def test_second_symbol(test_input1, test_input2, expected):
    """
    Tests the Find the Index of the Second Occurrence of a Letter in a String solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert second_symbol(test_input1, test_input2) == expected
