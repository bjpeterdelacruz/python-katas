"""
Unit tests for Arithmetic List! challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.arithmetic_list import seqlist


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(0, 2, 5, [0, 2, 4, 6, 8]), (0, 5, 0, []),
                          (-100, -5, 3, [-100, -105, -110]), (-10, 4, 5, [-10, -6, -2, 2, 6])])
def test_seqlist(test_input1, test_input2, test_input3, expected):
    """
    Tests the Arithmetic List! solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert seqlist(test_input1, test_input2, test_input3) == expected


def test_seqlist_invalid_input():
    """
    Tests the Arithmetic List! solution with invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        seqlist(0, 5, -1)
