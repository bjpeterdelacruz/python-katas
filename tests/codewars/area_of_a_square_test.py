"""
Unit tests for Area of a Square challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.area_of_a_square import square_area


@pytest.mark.parametrize("test_input, expected",
                         [(2, 1.62), (0, 0), (14.05, 80), (1, 0.41), (100, 4052.85)])
def test_square_area(test_input, expected):
    """
    Tests the Area of a Square solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert square_area(test_input) == expected
