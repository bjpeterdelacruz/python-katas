"""
Unit tests for parseInt() Reloaded challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.parseint_reloaded import parse_int


@pytest.mark.parametrize("test_input, expected",
                         [("one hundred seventy thousand", 170000),
                          ("seven hundred thousand and thirty-five", 700035),
                          ("seven hundred thirty thousand one hundred and thirty-five", 730135)])
def test_parse_int(test_input, expected):
    """
    Tests the parseInt() Reloaded solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert parse_int(test_input) == expected
