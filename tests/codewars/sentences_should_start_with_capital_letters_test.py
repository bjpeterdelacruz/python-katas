"""
Unit tests for Sentences Should Start with Capital Letters challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sentences_should_start_with_capital_letters import fix, to_upper


@pytest.mark.parametrize("test_input, expected",
                         [('hi', 'Hi'), ('  hello world', '  Hello world')])
def test_to_upper(test_input, expected):
    """
    Tests the to_upper function for the Sentences Should Start with Capital Letters solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert to_upper(test_input) == expected


@pytest.mark.parametrize("test_input, expected",
                         [('a quick brown fox .  . hello world.',
                           'A quick brown fox .  . Hello world.'),
                          ('asdfqwerty. iloveyou.', 'Asdfqwerty. Iloveyou.'),
                          ('hello. my name is BJ.', 'Hello. My name is BJ.')])
def test_fix(test_input, expected):
    """
    Tests the fix function for the Sentences Should Start with Capital Letters solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert fix(test_input) == expected
