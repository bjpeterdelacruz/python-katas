"""
Unit tests for Count the Characters challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.count_the_characters import count_char


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("Fancy fifth fly aloof", "f", 5), ("fizzbuzz", "z", 4)])
def test_count_char(test_input1, test_input2, expected):
    """
    Tests the Count the Characters solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert count_char(test_input1, test_input2) == expected
