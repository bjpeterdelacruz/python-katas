"""
Unit tests for ORing Arrays challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.oring_arrays import or_arrays


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3], [1, 2, 3], [1, 2, 3]), ([1, 2, 3], [4, 5, 6], [5, 7, 7]),
                          ([1, 2, 3], [1, 2], [1, 2, 3]), ([1, 2], [1, 2, 3], [1, 2, 3])])
def test_or_arrays(test_input1, test_input2, expected):
    """
    Tests the ORing Arrays solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert or_arrays(test_input1, test_input2) == expected


def test_or_arrays_default_param():
    """
    Tests the ORing Arrays solution using the third default parameter.

    :return: None
    """
    assert or_arrays([9, 2], [1, 2, 3, 4], 7) == [9, 2, 7, 7]
    assert or_arrays([9, 2], [1, 2, 3], 5) == [9, 2, 7]
