"""
Unit tests for N-th Power challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.nth_power import index


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3, 4], 2, 9),
                          ([1, 3, 10, 100], 3, 1000000),
                          ([0, 1], 0, 1),
                          ([0, 1], 1, 1),
                          ([1, 2], 2, -1),
                          ([1, 2], 3, -1),
                          ([0], 0, 1),
                          ([1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 9, 1),
                          ([1, 1, 1, 1, 1, 1, 1, 1, 1, 100], 9, 1000000000000000000),
                          ([29, 82, 45, 10], 3, 1000),
                          ([6, 31], 3, -1),
                          ([75, 68, 35, 61, 9, 36, 89, 0, 30], 10, -1)])
def test_index(test_input1, test_input2, expected):
    """
    Tests the N-th Power solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert index(test_input1, test_input2) == expected
