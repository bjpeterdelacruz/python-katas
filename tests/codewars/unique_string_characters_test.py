"""
Unit tests for Unique String Characters challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.unique_string_characters import solve


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("xyab", "xzca", "ybzc"), ("xyabb", "xzca", "ybbzc"),
                          ("abcd", "xyz", "abcdxyz"), ("xxx", "xzca", "zca")])
def test_solve(test_input1, test_input2, expected):
    """
    Tests the Unique String Characters solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert solve(test_input1, test_input2) == expected
