"""
Unit tests for Remove String Spaces challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.remove_string_spaces import no_space


@pytest.mark.parametrize("test_input, expected",
                         [("a b  c   d    ", "abcd"), ("  a  b  c  ", "abc")])
def test_no_space(test_input, expected):
    """
    Tests the Remove String Spaces solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert no_space(test_input) == expected
