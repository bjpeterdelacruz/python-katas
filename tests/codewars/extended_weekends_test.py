"""
Unit tests for Extended Weekends challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.extended_weekends import solve


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(2016, 2020, ("Jan", "May", 5)),
                          (1900, 1950, ("Mar", "Dec", 51)),
                          (1800, 2500, ("Aug", "Oct", 702))])
def test_solve(test_input1, test_input2, expected):
    """
    Tests the Extended Weekends solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert solve(test_input1, test_input2) == expected
