"""
Unit tests for Calendar Week challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.calendar_week import get_calendar_week


@pytest.mark.parametrize("test_input, expected",
                         [("2017-01-01", 52), ("2018-12-24", 52), ("2018-12-31", 1),
                          ("2019-01-01", 1), ("2016-02-29", 9), ("2015-12-29", 53),
                          ("2024-12-31", 1), ("2025-01-05", 1), ("2025-01-06", 2),
                          ("1995-12-31", 52), ("1996-01-01", 1), ("1999-12-31", 52),
                          ("2000-01-02", 52), ("2000-01-03", 1), ("2016-12-25", 51),
                          ("2016-12-26", 52), ("2017-01-01", 52), ("2017-01-02", 1),
                          ("2017-01-09", 2), ("2017-12-31", 52), ("2018-01-01", 1),
                          ("2018-12-31", 1), ("2019-01-01", 1)])
def test_evaluate(test_input, expected):
    """
    Tests the Calendar Week solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_calendar_week(test_input) == expected
