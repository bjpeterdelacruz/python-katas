"""
Unit tests for Counting Duplicates Across Multiple Lists challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.counting_duplicates_across_multiple_lists import count_duplicates


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [([], [], [], 0),
                          (['a', 'a', 'a', 'a', 'a'], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1], 4),
                          (['2fr43'] * 1000000, [4531] * 1000000, [321] * 1000000, 999999),
                          (['John', 'Beth', 'Beth', 'Bill'], [37, 23, 30, 46], [183, 170, 165, 175],
                           0),
                          (['John', 'Beth', 'Beth', 'Beth', 'Bill'], [37, 23, 23, 23, 46],
                           [183, 170, 170, 170, 175], 2),
                          (['Jack', 'Ben', 'Jack', 'Ben', 'Jack', 'Jack'], [25, 25, 34, 25, 25, 34],
                           [180, 180, 200, 180, 180, 200], 3)])
def test_count_duplicates(test_input1, test_input2, test_input3, expected):
    """
    Tests the Counting Duplicates Across Multiple Lists solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert count_duplicates(test_input1, test_input2, test_input3) == expected
