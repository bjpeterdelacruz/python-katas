"""
Unit tests for Find the Squares challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_the_squares import find_squares


@pytest.mark.parametrize("test_input, expected",
                         [(17, '81-64'),
                          (77, '1521-1444'),
                          (187, '8836-8649'),
                          (177, '7921-7744'),
                          (1807, '817216-815409'),
                          (1771, '784996-783225')])
def test_find_squares(test_input, expected):
    """
    Tests the Find the Squares solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_squares(test_input) == expected
