"""
Unit tests for First M Multiples of N challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.first_m_multiples_of_n import multiples


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(3, 10, [10, 20, 30]), (5, 2, [2, 4, 6, 8, 10])])
def test_multiples(test_input1, test_input2, expected):
    """
    Tests the First M Multiples of N solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert multiples(test_input1, test_input2) == expected
