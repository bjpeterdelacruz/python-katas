"""
Unit tests for Baby Count! challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.scholarstem_unit_6_baby_count import count_name


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(["Bob", "Ted", "Amy", "Amy", "Ted", "Amy"], "Amy", 3),
                          (["Bob", "Ted", "Amy", "Amy", "Ted", "Amy"], "Bill", 0),
                          ([], "Amy", 0)])
def test_count_name(test_input1, test_input2, expected):
    """
    Tests the Baby Count! solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert count_name(test_input1, test_input2) == expected
