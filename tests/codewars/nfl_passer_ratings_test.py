"""
Unit tests for NFL Passer Ratings challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.nfl_passer_ratings import passer_rating


def test_passer_rating():
    """
    Tests the NFL Passer Ratings solution.

    :return: None
    """
    assert passer_rating(432, 3554, 291, 28, 2) == 112.2
    assert passer_rating(5, 76, 4, 1, 0) == 158.3
    assert passer_rating(48, 192, 19, 2, 3) == 39.6
    assert passer_rating(1, 2, 1, 1, 0) == 118.8
    assert passer_rating(34, 172, 20, 1, 1) == 69.7
    assert passer_rating(10, 17, 2, 0, 1) == 0.0
