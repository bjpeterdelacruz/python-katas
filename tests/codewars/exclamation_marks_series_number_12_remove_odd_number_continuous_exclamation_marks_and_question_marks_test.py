"""
Unit tests for Exclamation Marks Series #12 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_12_remove_odd_number_continuous_exclamation_marks_and_question_marks import remove  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [("", ""),
                          ("!", "!"),
                          ("!!", "!!"),
                          ("!!!", ""),
                          ("!??", "!??"),
                          ("!???", "!"),
                          ("!!!??", "??"),
                          ("!!!???", ""),
                          ("!??", "!??"),
                          ("!???!!", ""),
                          ("!????!!!?", "!")])
def test_remove(test_input, expected):
    """
    Tests the Exclamation Marks Series #12 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove(test_input) == expected
