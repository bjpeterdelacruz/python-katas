"""
Unit tests for Fix the Loop challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.fix_the_loop import list_animals


@pytest.mark.parametrize("test_input, expected",
                         [(['lion', 'tiger', 'wolf'], "1. lion\n2. tiger\n3. wolf\n")])
def test_list_animals(test_input, expected):
    """
    Tests the Fix the Loop solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert list_animals(test_input) == expected
