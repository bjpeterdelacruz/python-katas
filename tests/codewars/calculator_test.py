"""
Unit tests for Calculator challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.calculator import Calculator


@pytest.mark.parametrize("test_input, expected",
                         [("2 + 6 * (3 + (2 - 3 * 4)) - 7", -47), ("2 + -6 + 8", 4),
                          ("( ( ( ( 1 ) * 2 ) ) )", 2), ("2 * ( 2 * ( 2 * ( 2 * 1 ) ) )", 16),
                          ("1.1 + 2.2 + 3.3", 6.6), ("2 + 3 * 4 / 3 - 6 / 3 * 3 + 8", 8)])
def test_evaluate(test_input, expected):
    """
    Tests the Calculator solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert Calculator.evaluate(test_input) == expected
