"""
Unit tests for No Zeros for Heroes challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.no_zeros_for_heros import no_boring_zeros


@pytest.mark.parametrize("test_input, expected",
                         [(0, 0), (10400, 104), (100, 1)])
def test_no_boring_zeros(test_input, expected):
    """
    Tests the No Zeros for Heroes solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert no_boring_zeros(test_input) == expected
