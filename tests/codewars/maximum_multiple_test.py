"""
Unit tests for Maximum Multiple challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.maximum_multiple import max_multiple


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(2, 10, 10), (2, 9, 8), (3, 59, 57), (3, 60, 60)])
def test_max_multiple(test_input1, test_input2, expected):
    """
    Tests the Maximum Multiple solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert max_multiple(test_input1, test_input2) == expected
