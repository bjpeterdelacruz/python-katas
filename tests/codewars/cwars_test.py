"""
Unit tests for C.Wars challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.cwars import initials


@pytest.mark.parametrize("test_input, expected",
                         [("code wars", "C.Wars"), ("codewars", "Codewars"),
                          ("bj peter dela cruz", "B.P.D.Cruz"), ("BJ Dela Cruz", "B.D.Cruz"),
                          ("", "")])
def test_initials(test_input, expected):
    """
    Tests the C.Wars solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert initials(test_input) == expected
