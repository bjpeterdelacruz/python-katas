"""
Unit tests for Find Multiples of a Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_multiples_of_a_number import find_multiples


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(5, 25, [5, 10, 15, 20, 25]),
                          (1, 2, [1, 2]),
                          (5, 7, [5]),
                          (4, 27, [4, 8, 12, 16, 20, 24]),
                          (11, 54, [11, 22, 33, 44])])
def test_find_multiples(test_input1, test_input2, expected):
    """
    Tests the Find Multiples of a Number solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_multiples(test_input1, test_input2) == expected
