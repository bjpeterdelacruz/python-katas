"""
Unit tests for GA-DE-RY-PO-LU-KI Cypher challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.ga_de_ry_po_lu_ki_cypher import encode, decode


@pytest.mark.parametrize("test_input, expected",
                         [("ABCD", "GBCE"), ("GBCE", "ABCD"), ("Gug hgs g cgt", "Ala has a cat")])
def test_encode(test_input, expected):
    """
    Tests the GA-DE-RY-PO-LU-KI Cypher (encode) solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert encode(test_input) == expected


@pytest.mark.parametrize("test_input, expected",
                         [("agedyropulik", "gaderypoluki"), ("Ala has a cat", "Gug hgs g cgt"),
                          ("gaderypoluki", "agedyropulik")])
def test_decode(test_input, expected):
    """
    Tests the GA-DE-RY-PO-LU-KI Cypher (decode) solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert decode(test_input) == expected
