"""
Unit tests for Reverse List Order challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.reverse_list_order import reverse_list


@pytest.mark.parametrize("test_input, expected",
                         [([], []), ([0], [0]), ([1, 2, 3], ([3, 2, 1]))])
def test_reverse_list(test_input, expected):
    """
    Tests the Reverse List Order solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert reverse_list(test_input) == expected
