"""
Unit tests for Subtract the Sum challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.subtract_the_sum import subtract_sum


def test_subtract_sum():
    """
    Tests the solution for this kata.

    :return: None
    """
    for number in range(10, 10001):
        assert subtract_sum(number) == "apple"
