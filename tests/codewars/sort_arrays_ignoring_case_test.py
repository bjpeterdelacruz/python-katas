"""
Unit tests for Sort Arrays (Ignoring Case) challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sort_arrays_ignoring_case import sortme


@pytest.mark.parametrize("test_input, expected",
                         [(["Hello", "there", "I'm", "fine"], ["fine", "Hello", "I'm", "there"]),
                          (["C", "d", "a", "B"], ["a", "B", "C", "d"]),
                          (["CodeWars"], ["CodeWars"])])
def test_sortme(test_input, expected):
    """
    Tests the Sort Arrays (Ignoring Case) solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert sortme(test_input) == expected
