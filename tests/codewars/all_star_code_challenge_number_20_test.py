"""
Unit tests for All Star Code Challenge #20 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.all_star_code_challenge_number_20 import add_arrays


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2], [4, 5], [5, 7]), (['a'], ['b'], ['ab'])])
def test_add_arrays(test_input1, test_input2, expected):
    """
    Tests the All Star Code Challenge #20 solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert add_arrays(test_input1, test_input2) == expected


def test_fail():
    """
    Tests whether a ValueError is raised on invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        add_arrays([1, 2], [3])
        assert False
