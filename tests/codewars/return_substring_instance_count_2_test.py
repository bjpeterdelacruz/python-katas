"""
Unit tests for Return Substring Instance Count - 2 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.return_substring_instance_count_2 import search_substr


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('aa_bb_cc_dd_bb_e', 'bb', 2), ('aaabbbcccc', 'bbb', 1),
                          ('aaacccbbbcccc', 'cc', 5), ('aaa', 'aa', 2),
                          ('a', '', 0), ('', 'a', 0), ('', '', 0)])
def test_search_substr_1(test_input1, test_input2, expected):
    """
    Tests the Return Substring Instance Count - 2 solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert search_substr(test_input1, test_input2) == expected


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [('aaa', 'aa', False, 1), ('aaabbbaaa', 'bb', False, 1),
                          ('', '', False, 0)])
def test_search_substr_2(test_input1, test_input2, test_input3, expected):
    """
    Tests the Return Substring Instance Count - 2 solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert search_substr(test_input1, test_input2, test_input3) == expected
