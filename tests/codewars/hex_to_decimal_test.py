"""
Unit tests for Hex to Decimal challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.hex_to_decimal import hex_to_dec


@pytest.mark.parametrize("test_input, expected",
                         [("1", 1), ("a", 10), ("10", 16)])
def test_hex_to_decimal(test_input, expected):
    """
    Tests the Hex to Decimal solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert hex_to_dec(test_input) == expected
