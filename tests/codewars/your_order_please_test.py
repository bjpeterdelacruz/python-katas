"""
Unit tests for Your Order, Please challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.your_order_please import order


@pytest.mark.parametrize("test_input, expected",
                         [("4of Fo1r pe6ople g3ood th5e the2", "Fo1r the2 g3ood 4of th5e pe6ople"),
                          ("", "")])
def test_order(test_input, expected):
    """
    Tests the Your Order, Please solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert order(test_input) == expected
