"""
Unit tests for Turn with a Compass challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.turn_with_a_compass import direction


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("S", 180, "N"),
                          ("SE", -45, "E"),
                          ("W", 495, "NE")])
def testdirection(test_input1, test_input2, expected):
    """
    Tests the Triangular Treasure solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert direction(test_input1, test_input2) == expected
