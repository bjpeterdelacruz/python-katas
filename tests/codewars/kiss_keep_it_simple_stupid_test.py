"""
Unit tests for Keep It Simple Stupid challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.kiss_keep_it_simple_stupid import is_kiss


@pytest.mark.parametrize("test_input, expected",
                         [('Joe had a bad day', "Good work Joe!"),
                          ('Joe had some bad days', "Good work Joe!"),
                          ('Joe is having no fun', "Keep It Simple Stupid"),
                          ('Sometimes joe cries for hours', "Keep It Simple Stupid"),
                          ('Joe is having lots of fun', "Good work Joe!"),
                          ('Joe is working hard a lot', "Keep It Simple Stupid"),
                          ('Joe listened to the noise and it was an onamonapia', "Good work Joe!"),
                          ('Joe listened to the noises and there were some onamonapias',
                           "Keep It Simple Stupid")])
def test_is_kiss(test_input, expected):
    """
    Tests the Keep It Simple Stupid solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_kiss(test_input) == expected
