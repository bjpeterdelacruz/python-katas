"""
Unit tests for Next Smaller Number with Same Digits challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.next_smaller_number_with_the_same_digits import next_smaller


@pytest.mark.parametrize("test_input, expected",
                         [(21, 12), (907, 790), (531, 513), (1027, -1), (441, 414),
                          (123456798, 123456789), (513, 351), (135, -1),
                          (9, -1), (100, -1), (1207, 1072), (99999999, -1),
                          (432891583025794012233445578899, 432891583025793998875544432210),
                          (26558370720223556778, 26558370708776553222),
                          (2349288197071931601112233467788999, 2349288197071931499988776633221110),
                          (38650201679672709120001122235666777899,
                           38650201679672709119987776665322221000)])
def test_next_smaller(test_input, expected):
    """
    Tests the Next Smaller Number with Same Digits solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert next_smaller(test_input) == expected
