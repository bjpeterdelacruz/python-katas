"""
Unit tests for Making Copies challenge

:Author: BJ Peter Dela Cruz
"""
import pytest

from codewars.making_copies import copy_list


def test_copy_list():
    """
    Tests the Making Copies solution.

    :return: None
    """
    lst = [1, 2, 3, 4]
    lst_copy = copy_list(lst)

    assert lst == lst_copy

    lst_copy[1] = 5
    assert lst != lst_copy

    with pytest.raises(ValueError):
        copy_list("Hello World")
        assert False
