"""
Unit tests for ATM challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.atm import solve


@pytest.mark.parametrize("test_input, expected",
                         [(770, 4), (550, 2), (10, 1), (1250, 4), (125, -1), (777, -1), (42, -1)])
def test_solve(test_input, expected):
    """
    Tests the ATM solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert solve(test_input) == expected
