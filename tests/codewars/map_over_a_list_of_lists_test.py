"""
Unit tests for Map Over List of Lists challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.map_over_a_list_of_lists import grid_map


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([[1, 2, 3], [4], [], [5, 6, 7, 8, 9]], lambda x: x * 2,
                           [[2, 4, 6], [8], [], [10, 12, 14, 16, 18]])])
def test_grid_map(test_input1, test_input2, expected):
    """
    Tests the Map Over List of Lists solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert grid_map(test_input1, test_input2) == expected
