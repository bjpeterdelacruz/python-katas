"""
Unit tests for Message Validator challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.message_validator import is_a_valid_message


@pytest.mark.parametrize("test_input, expected",
                         [("CHVRBMJBAZLEFAYFISKXX4JCUB192", False), ("6RILQY5IJNMWH2QJ", False),
                          ("hello5", False), ("3hey5hello2hi5", False), ("3hey5hello2hi", True),
                          ("", True)])
def test_is_a_valid_message(test_input, expected):
    """
    Tests the Message Validator solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_a_valid_message(test_input) == expected
