"""
Unit tests for Words to Hex challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.words_to_hex import words_to_hex


@pytest.mark.parametrize("test_input, expected",
                         [("Hello, my name is Gary and I like cheese.",
                           ['#48656c', '#6d7900', '#6e616d', '#697300', '#476172', '#616e64',
                            '#490000', '#6c696b', '#636865']),
                          ("0123456789", ['#303132']),
                          ("ThisIsOneLongSentenceThatConsistsOfWords", ['#546869']),
                          ("Blah blah blah blaaaaaaaaaaaah",
                           ['#426c61', '#626c61', '#626c61', '#626c61']),
                          ("&&&&& $$$$$ ^^^^^ @@@@@ ()()()()(",
                           ['#262626', '#242424', '#5e5e5e', '#404040', '#282928'])])
def test_words_to_hex(test_input, expected):
    """
    Tests the Words to Hex solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert words_to_hex(test_input) == expected
