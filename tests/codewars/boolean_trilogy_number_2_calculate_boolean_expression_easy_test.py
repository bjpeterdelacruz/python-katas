"""
Unit tests for Boolean Trilogy #2: Calculate Boolean Expression (Easy) challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.boolean_trilogy_number_2_calculate_boolean_expression_easy import calculate


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("A & B & C", {"A": 0, "B": 1, "C": 0}, 0),
                          ("A | B | C", {"A": 0, "B": 0, "C": 0}, 0),
                          ("A & C & B", {"A": 1, "B": 1, "C": 1}, 1),
                          ("B & A | C", {"A": 1, "B": 0, "C": 0}, 0), ("D", {"D": 1}, 1),
                          ("A & A | A", {"A": 0}, 0)])
def test_calculate(test_input1, test_input2, expected):
    """
    Tests the Boolean Trilogy #2: Calculate Boolean Expression (Easy) solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert calculate(test_input1, test_input2) == expected
