"""
Unit tests for Reverse or Rotate? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.reverse_or_rotate import revrot


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("123456987654", 6, "234561876549"), ("123456987653", 6, "234561356789"),
                          ("66443875", 4, "44668753"), ("66443875", 8, "64438756"),
                          ("664438769", 8, "67834466"), ("123456779", 8, "23456771"),
                          ("", 8, ""), ("123456779", 0, ""),
                          ("563000655734469485", 4, "0365065073456944")])
def test_revrot(test_input1, test_input2, expected):
    """
    Tests the Reverse or Rotate? solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert revrot(test_input1, test_input2) == expected
