"""
Unit tests for Always Perfect challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.always_perfect import check_root


@pytest.mark.parametrize("test_input, expected",
                         [('4,5,6,7', '841, 29'),
                          ('3,s,5,6', 'incorrect input'),
                          ('11,13,14,15', 'not consecutive'),
                          ('10,11,12,13,15', 'incorrect input'),
                          ('10,11,12,13', '17161, 131'),
                          ('ad,d,q,tt,v', 'incorrect input'),
                          ('//,;;,/,..,', 'incorrect input'),
                          ('1,2,3,4', '25, 5'),
                          ('1015,1016,1017,1018', '1067648959441, 1033271'),
                          ('555,777,444,111', 'not consecutive'),
                          ('20,21,22,24', 'not consecutive'),
                          ('9,10,10,11', 'not consecutive'),
                          ('11254,11255,11256,11258', 'not consecutive'),
                          ('25000,25001,25002,25003', '390718756875150001, 625075001'),
                          ('2000000,2000001,2000002,2000003',
                           '16000048000044000012000001, 4000006000001'),
                          ('4,5,6,q', 'incorrect input'),
                          ('5,6,7', 'incorrect input'),
                          ('3,5,6,7', 'not consecutive'),
                          ('-4,-3,-2,-1', '25, 5'),
                          ('-1,0,1,2', '1, 1')])
def test_check_root(test_input, expected):
    """
    Tests the Always Perfect solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert check_root(test_input) == expected
