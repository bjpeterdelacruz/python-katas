"""
Unit tests for Maximum and Minimum Difference (Challenge Version) challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.the_maximum_and_minimum_difference_challenge_version import max_and_min


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([3, 10, 5], [20, 7, 15, 8], (17, 2))])
def test_max_and_min(test_input1, test_input2, expected):
    """
    Tests the Maximum and Minimum Difference (Challenge Version) solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert max_and_min(test_input1, test_input2) == expected
