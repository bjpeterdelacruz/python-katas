"""
Unit tests for Median Fun Fun challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.median_fun_fun import median


@pytest.mark.parametrize("test_input, expected",
                         [([3, 1, 4, 2], 2.5), ([4, 2, 7], 4), ([], None)])
def test_median(test_input, expected):
    """
    Tests the Median Fun Fun solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert median(test_input) == expected
