"""
Unit tests for Calculate Variance challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.calculate_variance import variance


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 2, 3], 0.5)])
def test_variance(test_input, expected):
    """
    Tests the Calculate Variance solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert variance(test_input) == expected
