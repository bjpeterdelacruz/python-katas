"""
Unit tests for Permutations challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.permutations import permutations


@pytest.mark.parametrize("test_input, expected",
                         [("aabb", ["aabb", "abab", "abba", "baab", "baba", "bbaa"])])
def test_permutations(test_input, expected):
    """
    Tests the Permutations solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert permutations(test_input) == expected
