"""
Unit tests for Running Out of Space challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.running_out_of_space import spacey


@pytest.mark.parametrize("test_input, expected",
                         [(["I"], ["I"]),
                          (["I", "have", "no", "spaces"],
                           ["I", "Ihave", "Ihaveno", "Ihavenospaces"])])
def test_spacey(test_input, expected):
    """
    Tests the Running Out of Space solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert spacey(test_input) == expected
