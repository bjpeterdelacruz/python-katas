"""
Unit tests for Drink About challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.drink_about import people_with_age_drink


@pytest.mark.parametrize("test_input, expected",
                         [(13, 'drink toddy'),
                          (0, 'drink toddy'),
                          (17, 'drink coke'),
                          (15, 'drink coke'),
                          (14, 'drink coke'),
                          (20, 'drink beer'),
                          (18, 'drink beer'),
                          (22, 'drink whisky'),
                          (21, 'drink whisky')])
def test_people_with_age_drink(test_input, expected):
    """
    Tests the Drink About solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert people_with_age_drink(test_input) == expected
