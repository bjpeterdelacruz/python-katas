"""
Unit tests for Airport Arrivals/Departures #1 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.airport_arrivals_slash_departures_number_1 import flap_display


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(["CAT"], [[1, 13, 27]], ["DOG"]),
                          (["HELLO "], [[15, 49, 50, 48, 43, 13]], ["WORLD!"]),
                          (["CODE"], [[20, 20, 28, 0]], ["WARS"]),
                          (["CAT", "HELLO ", "CODE"],
                           [[1, 13, 27], [15, 49, 50, 48, 43, 13], [20, 20, 28, 0]],
                           ["DOG", "WORLD!", "WARS"])])
def test_flap_display(test_input1, test_input2, expected):
    """
    Tests the Airport Arrivals/Departures #1 solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert flap_display(test_input1, test_input2) == expected
