"""
Unit tests for Monotone Travel challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.monotone_travel import is_monotone


@pytest.mark.parametrize("test_input, expected",
                         [(list(range(1, 100)), True), ([5, 5, 5, 5, 5], True), ([], True),
                          ([1], True), (list(range(100, 0, -1)), False), ([1, 2, 3, 2, 5], False)])
def test_is_monotone(test_input, expected):
    """
    Tests the Monotone Travel solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_monotone(test_input) == expected
