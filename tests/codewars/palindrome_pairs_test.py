"""
Unit tests for Palindrome Pairs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.palindrome_pairs import palindrome_pairs


@pytest.mark.parametrize("test_input, expected",
                         [(["bat", "tab", "cat"], [[0, 1], [1, 0]]),
                          (["dog", "cow", "tap", "god", "pat"], [[0, 3], [2, 4], [3, 0], [4, 2]]),
                          (["abcd", "dcba", "lls", "s", "sssll"], [[0, 1], [1, 0], [2, 4], [3, 2]]),
                          ([], []),
                          (["adgdfsh", "wertewry", "zxcbxcb", "efveyn"], []),
                          ([5, 2, 'abc', True, [False]], []),
                          ([5777, 'dog', 'god', True, 75], [[0, 4], [1, 2], [2, 1]])])
def test_palindrome_pairs(test_input, expected):
    """
    Tests the Palindrome Pairs solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert palindrome_pairs(test_input) == expected
