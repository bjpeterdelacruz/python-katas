"""
Unit tests for Simple Sentences challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_sentences import make_sentences


@pytest.mark.parametrize("test_input, expected",
                         [(['hello', 'world'], 'hello world.'),
                          (['Quick', 'brown', 'fox', 'jumped', 'over', 'the', 'lazy', 'dog'],
                           'Quick brown fox jumped over the lazy dog.'),
                          (['hello', ',', 'my', 'dear'], 'hello, my dear.'),
                          (['one', ',', 'two', ',', 'three'], 'one, two, three.'),
                          (['One', ',', 'two', 'two', ',', 'three', 'three', 'three', ',', '4',
                            '4', '4', '4'], 'One, two two, three three three, 4 4 4 4.'),
                          (['hello', 'world', '.'], 'hello world.'),
                          (['Bye', '.'], 'Bye.'),
                          (['hello', 'world', '.', '.', '.'], 'hello world.'),
                          (['The', 'Earth', 'rotates', 'around', 'The', 'Sun', 'in', '365', 'days',
                            ',', 'I', 'know', 'that', '.', '.', '.', '.', '.', '.', '.', '.', '.',
                            '.', '.', '.'],
                           'The Earth rotates around The Sun in 365 days, I know that.')])
def test_make_sentences(test_input, expected):
    """
    Tests the Simple Sentences solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert make_sentences(test_input) == expected
