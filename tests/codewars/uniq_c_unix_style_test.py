"""
Unit tests for uniq -c (UNIX style) challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.uniq_c_unix_style import uniq_c


@pytest.mark.parametrize("test_input, expected",
                         [(['a', 'a', 'b', 'b', 'c', 'a', 'b', 'c'],
                           [('a', 2), ('b', 2), ('c', 1), ('a', 1), ('b', 1), ('c', 1)]),
                          ([], [])])
def test_uniq_c(test_input, expected):
    """
    Tests the uniq -c (UNIX style) solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert uniq_c(test_input) == expected
