"""
Unit tests for Calculate Derivative challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.calculate_derivative_number_1_single_integer_equation import get_derivative


@pytest.mark.parametrize("test_input, expected",
                         [('3x^3', '9x^2'), ('3x^2', '6x'), ('3x', '3'), ('3', '0'),
                          ('3x^-1', '-3x^-2'), ('-3x^-1', '3x^-2'), ('-3x^10', '-30x^9'),
                          ('-954x^2', '-1908x')])
def test_get_derivative(test_input, expected):
    """
    Tests the Calculate Derivative solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_derivative(test_input) == expected
