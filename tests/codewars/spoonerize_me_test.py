"""
Unit tests for Spoonerize Me challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.spoonerize_me import spoonerize


@pytest.mark.parametrize("test_input, expected",
                         [("nit picking", "pit nicking"), ("wedding bells", "bedding wells"),
                          ("jelly beans", "belly jeans"), ("pop corn", "cop porn")])
def test_spoonerize(test_input, expected):
    """
    Tests the Spoonerize Me solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert spoonerize(test_input) == expected
