"""
Unit tests for Array Plus Array challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.array_plus_array import array_plus_array


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3], [4, 5, 6], 21), ([1, 2, 3], [], 6), ([], [4, 5, 6], 15)])
def test_array_plus_array(test_input1, test_input2, expected):
    """
    Tests the Array Plus Array solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert array_plus_array(test_input1, test_input2) == expected
