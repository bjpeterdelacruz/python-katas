"""
Unit tests for Thinkful: Grade Calculator challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_list_and_loop_drills_grade_calculator import calculate_grade


@pytest.mark.parametrize("test_input, expected",
                         [([92, 94, 99], 'A'), ([50, 60, 70, 80, 90], 'C'), ([50, 55], 'F'),
                          ([100, 110], 'A'), ([80, 89], 'B'), ([59, 61], 'D')])
def test_calculate_grade(test_input, expected):
    """
    Tests the Thinkful: Grade Calculator solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert calculate_grade(test_input) == expected
