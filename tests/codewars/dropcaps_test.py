"""
Unit tests for Drop Caps challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.dropcaps import drop_cap


@pytest.mark.parametrize("test_input, expected",
                         [('Apple Banana', "Apple Banana"),
                          ('Apple', "Apple"),
                          ('', ""),
                          ('of', "of"),
                          ('Revelation of the contents outraged American public opinion, and then',
                           "Revelation of The Contents Outraged American Public Opinion, And Then"),
                          ('more  than    one space between words',
                           "More  Than    One Space Between Words"),
                          ('  leading spaces', "  Leading Spaces"),
                          ('trailing spaces   ', "Trailing Spaces   "),
                          ('ALL CAPS CRAZINESS', "All Caps Craziness"),
                          ('rAnDoM CaPs CrAzInEsS', "Random Caps Craziness")])
def test_drop_cap(test_input, expected):
    """
    Tests the Drop Caps solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert drop_cap(test_input) == expected
