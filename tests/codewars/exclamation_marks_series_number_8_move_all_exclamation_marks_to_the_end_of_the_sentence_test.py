"""
Unit tests for Exclamation Marks Series #8: Move All Exclamation Marks to the End of the Sentence
challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_8_move_all_exclamation_marks_to_the_end_of_the_sentence import remove  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [('Hi!', 'Hi!'), ('Hi!!!', 'Hi!!!'), ('!Hi', 'Hi!'), ('!Hi!', 'Hi!!'),
                          ('Hi! Hi!', 'Hi Hi!!'), ('Hi', 'Hi'), ('Hi!! !Hi', 'Hi Hi!!!')])
def test_remove(test_input, expected):
    """
    Tests the Exclamation Marks Series #8: Move All Exclamation Marks to the End of the Sentence
    solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove(test_input) == expected
