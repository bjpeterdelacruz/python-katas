"""
Unit tests for Days Represented in a Foreign Country challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.how_many_days_are_we_represented_in_a_foreign_country import days_represented


@pytest.mark.parametrize("test_input, expected",
                         [([[10, 15], [25, 35]], 17), ([[2, 8], [220, 229], [10, 16]], 24),
                          ([[100, 150], [125, 130], [145, 150]], 51)])
def test_beeramid(test_input, expected):
    """
    Tests the Days Represented in a Foreign Country solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert days_represented(test_input) == expected
