"""
Unit tests for Sum of Numbers from 0 to N challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_of_numbers_from_0_to_n import show_sequence


@pytest.mark.parametrize("test_input, expected",
                         [(0, "0=0"), (-1, "-1<0"), (6, "0+1+2+3+4+5+6 = 21")])
def test_show_sequence(test_input, expected):
    """
    Tests the Sum of Numbers from 0 to N solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert show_sequence(test_input) == expected
