"""
Unit tests for Alternate Square Sum challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.alternate_square_sum import alternate_sq_sum


@pytest.mark.parametrize("test_input, expected",
                         [([], 0),
                          ([-1, 0, -3, 0, -5, 3], 0),
                          ([-1, 2, -3, 4, -5], 11),
                          ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 245)])
def test_alternate_sq_sum(test_input, expected):
    """
    Tests the Alternate Square Sum solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert alternate_sq_sum(test_input) == expected
