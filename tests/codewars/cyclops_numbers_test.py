"""
Unit tests for Cyclops Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.cyclops_numbers import cyclops


@pytest.mark.parametrize("test_input, expected",
                         [(1, False), (3, False), (5, True), (11, False), (13, False), (23, False),
                          (27, True)])
def test_licence_plate(test_input, expected):
    """
    Tests the Cyclops Numbers solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert cyclops(test_input) == expected
