"""
Unit tests for Exclamation Marks Series #3 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_3_remove_all_exclamation_marks_from_sentence_except_at_the_end import remove  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [('Hi!', 'Hi!'), ('Hi!!!', 'Hi!!!'), ('!Hi', 'Hi'), ('!Hi!', 'Hi!'),
                          ('Hi! Hi!', 'Hi Hi!'), ('Hi', 'Hi')])
def test_remove(test_input, expected):
    """
    Tests the Exclamation Marks Series #3 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert remove(test_input) == expected
