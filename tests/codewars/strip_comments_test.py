"""
Unit tests for Strip Comments challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.strip_comments import strip_comments


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("apples ! bananas # oranges\nfruits\nveggies # cookies", ["#", "!"],
                           "apples\nfruits\nveggies")])
def test_strip_comments(test_input1, test_input2, expected):
    """
    Tests the Strip Comments solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert strip_comments(test_input1, test_input2) == expected
