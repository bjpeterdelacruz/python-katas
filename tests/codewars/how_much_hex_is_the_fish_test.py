"""
Unit tests for How Much Hex is the Fish challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.how_much_hex_is_the_fish import fish_hex


@pytest.mark.parametrize("test_input, expected",
                         [("pufferfish", 1),
                          ("puffers", 14),
                          ("balloonfish", 14),
                          ("blowfish", 4),
                          ("bubblefish", 10),
                          ("globefish", 10),
                          ("swellfish", 1),
                          ("toadfish", 8),
                          ("toadies", 9),
                          ("honey toads", 9),
                          ("sugar toads", 13),
                          ("sea squab", 5),
                          ("", 0),
                          ("Aeneus corydoras", 1),
                          ("African glass catfish", 0),
                          ("African lungfish", 12),
                          ("Aholehole", 10),
                          ("Airbreathing catfish", 12),
                          ("Airsac catfish", 5),
                          ("Alaska blackfish", 8),
                          ("Albacore", 9),
                          ("Alewife", 5),
                          ("Alfonsino", 5),
                          ("Algae eater", 4),
                          ("Alligatorfish", 15),
                          ("Asian carps", 6),
                          ("Asiatic glassfish", 9),
                          ("Atka mackerel", 6),
                          ("Atlantic cod", 13),
                          ("Atlantic herring", 2),
                          ("Atlantic salmon", 6),
                          ("Atlantic saury", 6),
                          ("Atlantic silverside", 1),
                          ("Australasian salmon", 10),
                          ("Australian grayling", 0),
                          ("Australian herrin", 4),
                          ("Australian lungfish", 5),
                          ("Australian prowfish", 5),
                          ("Ayu", 10), ])
def test_fish_hex(test_input, expected):
    """
    Tests the How Much Hex is the Fish solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert fish_hex(test_input) == expected
