"""
Unit tests for Decode Morse challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.decode_morse import decode


@pytest.mark.parametrize("test_input, expected",
                         [(".... . .-.. .-.. ---  .-- --- .-. .-.. -..", "hello world"),
                          (".---- ... -  .- -. -..  ..--- -. -..", "1st and 2nd"),
                          ("..  .- --  .-  - . ... -", "i am a test"),
                          (
                          ".- -... -.-. -.. . ..-. --. .... .. .--- -.- .-.. -- -. --- .--. --.- "
                          + ".-. ... - ..- ...- .-- -..- -.-- --.. ----- .---- ..--- ...-- ....- "
                          + "..... -.... --... ---.. ----.",
                          "abcdefghijklmnopqrstuvwxyz0123456789"),
                          ("", "")])
def test_decode(test_input, expected):
    """
    Tests the Decode Morse solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert decode(test_input) == expected
