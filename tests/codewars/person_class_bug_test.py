"""
Unit tests for Person Class Bug challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.person_class_bug import Person


def test_person():
    """
    Tests the Person Class Bug solution.

    :return: None
    """
    person = Person('Yukihiro', 'Matsumoto', 47)
    assert person.full_name == "Yukihiro Matsumoto"
    assert person.age == 47
