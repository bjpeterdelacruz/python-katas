"""
Unit tests for Third Angle of a Triangle challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.third_angle_of_a_triangle import other_angle


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(30, 60, 90),
                          (60, 60, 60),
                          (43, 78, 59),
                          (10, 20, 150)])
def test_other_angle(test_input1, test_input2, expected):
    """
    Tests the Third Angle of a Triangle solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert other_angle(test_input1, test_input2) == expected
