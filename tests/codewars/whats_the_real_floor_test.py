"""
Unit tests for What is the Real Floor? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.whats_the_real_floor import get_real_floor


@pytest.mark.parametrize("test_input, expected",
                         [(1, 0),
                          (0, 0),
                          (5, 4),
                          (10, 9),
                          (12, 11),
                          (14, 12),
                          (15, 13),
                          (37, 35),
                          (200, 198),
                          (-2, -2),
                          (-5, -5)])
def test_get_real_floor(test_input, expected):
    """
    Tests the What is the Real Floor? solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_real_floor(test_input) == expected
