"""
Unit tests for Pythagorean Triple challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.pythagorean_triple import pythagorean_triple


@pytest.mark.parametrize("test_input, expected",
                         [([1, 5, 1], False), ([5, 3, 4], True), ([3, 5, 4], True)])
def test_pythagorean_triple(test_input, expected):
    """
    Tests the Pythagorean Triple solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert pythagorean_triple(test_input) == expected
