"""
Unit tests for Product of the Main Diagonal of a Square Matrix challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.product_of_the_main_diagonal_of_a_square_matrix import main_diagonal_product


@pytest.mark.parametrize("test_input, expected",
                         [([[1, 0], [0, 1]], 1), ([[1, 2, 3], [4, 5, 6], [7, 8, 9]], 45)])
def test_main_diagonal_product(test_input, expected):
    """
    Tests the Product of the Main Diagonal of a Square Matrix solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert main_diagonal_product(test_input) == expected
