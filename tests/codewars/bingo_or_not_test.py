"""
Unit tests for Bingo or Not challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.bingo_or_not import bingo


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "LOSE"),
                          ([20, 12, 23, 14, 6, 22, 12, 17, 2, 26], "LOSE"),
                          ([1, 2, 3, 7, 5, 14, 7, 15, 9, 10], "WIN"),
                          ([5, 2, 13, 7, 5, 14, 17, 15, 9, 10], "WIN")])
def test_bingo(test_input, expected):
    """
    Tests the Bingo or Not solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert bingo(test_input) == expected
