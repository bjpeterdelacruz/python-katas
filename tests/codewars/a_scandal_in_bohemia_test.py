"""
Unit tests for A Scandal in Bohemia challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.a_scandal_in_bohemia import key


def test_a_scandal_in_bohemia():
    """
    Tests the A Scandal in Bohemia solution.

    :return: None
    """
    dir_path = "tests/codewars/a_scandal_in_bohemia"
    with open(f"{dir_path}/a_scandal_in_bohemia.txt", "r", encoding="utf-8") as file:
        text = file.readlines()[0]

    with open(f"{dir_path}/a_scandal_in_bohemia_encrypted.txt", "r", encoding="utf-8") as file:
        encrypted = file.readlines()[0]

    filename = f"{dir_path}/a_scandal_in_bohemia_encrypted_random.txt"
    with open(filename, "r", encoding="utf-8") as file:
        encrypted_random = file.readlines()[0]

    assert key(text, encrypted) == "qwertyuiopasdfghjklzxcvbnm"
    assert key(text, encrypted_random) == "wcmapkxvbnetjzqfgdlyihrsuo"
