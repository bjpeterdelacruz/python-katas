"""
Unit tests for Can Santa Save Christmas? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.can_santa_save_christmas import determine_time


@pytest.mark.parametrize("test_input, expected",
                         [(["00:30:00", "02:30:00"], True), (["06:00:00", "18:00:00"], False),
                          (["06:00:00", "17:59:59"], True),
                          (["00:30:00", "02:30:00", "21:00:01"], False)])
def test_determine_time(test_input, expected):
    """
    Tests the Can Santa Save Christmas? solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert determine_time(test_input) == expected
