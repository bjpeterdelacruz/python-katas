"""
Unit tests for Longest Alphabetical Substring challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.longest_alphabetical_substring import longest


@pytest.mark.parametrize("test_input, expected",
                         [("asdfaaaabbbbcttavvfffffdf", "aaaabbbbctt"), (["asd", "as"]),
                          ("nab", "ab"), ("abcdeapbcdef", "abcde"), ("asdfbyfgiklag", "fgikl"),
                          ("z", "z"), ("zyba", "z")])
def test_longest(test_input, expected):
    """
    Tests the Longest Alphabetical Substring solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert longest(test_input) == expected
