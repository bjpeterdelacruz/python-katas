"""
Unit tests for Thinkful: Rømer Temperature challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_number_drills_romer_temperature import celsius_to_romer


@pytest.mark.parametrize("test_input, expected",
                         [(24, 20.1), (8, 11.7), (29, 22.725)])
def test_celsius_to_romer(test_input, expected):
    """
    Tests the Thinkful: Rømer Temperature solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert celsius_to_romer(test_input) == expected
