"""
Unit tests for Sum of Squares Less than Some Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.sum_of_squares_less_than_some_number import get_number_of_squares


@pytest.mark.parametrize("test_input, expected",
                         [(1, 0),
                          (2, 1),
                          (5, 1),
                          (6, 2),
                          (15, 3),
                          (50, 4),
                          (100, 6),
                          (1000, 13),
                          (10000, 30),
                          (100000, 66),
                          (1000000, 143)])
def test_get_number_of_squares(test_input, expected):
    """
    Tests the Sum of Squares Less than Some Number solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_number_of_squares(test_input) == expected
