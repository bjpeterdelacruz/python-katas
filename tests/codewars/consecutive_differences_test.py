"""
Unit tests for Consecutive Differences challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.consecutive_differences import differences


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 3], 0), ([1, 5, 2, 7, 8, 9, 0], 1), ([1.5, 2.5, 1], 0.5),
                          ([-1, 2, 3], 2), ([7], 7)])
def test_differences(test_input, expected):
    """
    Tests the Consecutive Differences solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert differences(test_input) == expected
