"""
Unit tests for Circular List challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.circular_list import CircularList

# pylint: disable=not-callable


def test_circular_list_next_prev():
    """
    Tests the Circular List solution.

    :return: None
    """
    circular_list = CircularList("one", "two", "three")
    assert circular_list.next() == "one"
    assert circular_list.next() == "two"
    assert circular_list.next() == "three"
    assert circular_list.next() == "one"
    assert circular_list.prev() == "three"
    assert circular_list.prev() == "two"
    assert circular_list.prev() == "one"
    assert circular_list.prev() == "three"

    circular_list = CircularList(1, 2, 3, 4, 5)
    assert circular_list.prev() == 5
    assert circular_list.prev() == 4
    assert circular_list.next() == 5
    assert circular_list.next() == 1
    assert circular_list.prev() == 5
    assert circular_list.prev() == 4
    assert circular_list.prev() == 3
    assert circular_list.prev() == 2

    with pytest.raises(ValueError):
        CircularList()
        assert False
