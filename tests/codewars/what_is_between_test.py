"""
Unit tests for What is Between? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.what_is_between import between


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(1, 4, [1, 2, 3, 4]),
                          (-2, 2, [-2, -1, 0, 1, 2]),
                          (-10, 10, list(range(-10, 10 + 1))),
                          (5, 100, list(range(5, 100 + 1)))])
def test_between(test_input1, test_input2, expected):
    """
    Tests the What is Between? solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert between(test_input1, test_input2) == expected
