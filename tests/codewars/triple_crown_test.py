"""
Unit tests for Triple Crown challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.triple_crown import triple_crown


@pytest.mark.parametrize("test_input, expected",
                         [({
                               'Cooper Kupp':
                                   {
                                       'Receiving yards': 1786,
                                       'Receiving touchdowns': 18,
                                       'Receptions': 117
                                   },
                               'Justin Jefferson':
                                   {
                                       'Receiving yards': 1700,
                                       'Receiving touchdowns': 17,
                                       'Receptions': 115
                                   },
                               'Davante Adams':
                                   {
                                       'Receiving yards': 1650,
                                       'Receiving touchdowns': 16,
                                       'Receptions': 110
                                   }
                           }, 'Cooper Kupp'),
                             ({
                                  'Cooper Kupp':
                                      {
                                          'Receiving yards': 1900,
                                          'Receiving touchdowns': 18,
                                          'Receptions': 117
                                      },
                                  'Justin Jefferson':
                                      {
                                          'Receiving yards': 1800,
                                          'Receiving touchdowns': 17,
                                          'Receptions': 116
                                      },
                                  'Davante Adams':
                                      {
                                          'Receiving yards': 1900,
                                          'Receiving touchdowns': 18,
                                          'Receptions': 110
                                      }
                              }, 'None of them'),
                             ({
                                 'Cooper Kupp':
                                     {
                                         'Receiving yards': 1786,
                                         'Receiving touchdowns': 18,
                                         'Receptions': 117
                                     },
                                 'Justin Jefferson':
                                     {
                                         'Receiving yards': 1700,
                                         'Receiving touchdowns': 17,
                                         'Receptions': 115
                                     },
                                 'Davante Adams':
                                     {
                                         'Receiving yards': 1650,
                                         'Receiving touchdowns': 19,
                                         'Receptions': 110
                                     }
                             }, 'None of them')])
def test_triple_crown(test_input, expected):
    """
    Tests the Triple Crown solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert triple_crown(test_input) == expected
