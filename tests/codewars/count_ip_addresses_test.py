"""
Unit tests for Count IP Addresses challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.count_ip_addresses import ips_between


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("10.0.0.0", "10.0.0.50", 50), ("10.0.0.0", "10.0.1.0", 256),
                          ("20.0.0.10", "20.0.1.0", 246)])
def test_ips_between(test_input1, test_input2, expected):
    """
    Tests the Count IP Addresses solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert ips_between(test_input1, test_input2) == expected
