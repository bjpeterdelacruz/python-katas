"""
Unit tests for Name Shuffler challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.name_shuffler import name_shuffler


@pytest.mark.parametrize("test_input, expected",
                         [("John Doe", "Doe John")])
def test_name_shuffler(test_input, expected):
    """
    Tests the Name Shuffler solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert name_shuffler(test_input) == expected
