"""
Unit tests for Thinkful: Graceful Addition challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.thinkful_logic_drills_graceful_addition import my_add


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('1', 2, None), (1, '2', None), (2, 2, 4)])
def test_my_add(test_input1, test_input2, expected):
    """
    Tests the Thinkful: Graceful Addition solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert my_add(test_input1, test_input2) == expected
