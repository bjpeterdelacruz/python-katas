"""
Unit tests for Vowel Back challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.vowels_back import vowel_back


@pytest.mark.parametrize("test_input, expected",
                         [("testcase", "tabtbvba"),
                          ("codewars", "bnaafvab"),
                          ("exampletesthere", "agvvyuatabtqaaa"),
                          ("returnofthespacecamel", "aatpawnftqabyvbabvvau"),
                          ("bringonthebootcamp", "kaiwpnwtqaknntbvvy"),
                          ("weneedanofficedog", "fawaaavwnffibaanp")])
def test_vowel_back(test_input, expected):
    """
    Tests the Vowel Back solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert vowel_back(test_input) == expected
