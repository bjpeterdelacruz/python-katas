"""
Unit tests for Twice as Old challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.twice_as_old import twice_as_old


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(36, 7, 22),
                          (55, 30, 5),
                          (42, 21, 0),
                          (22, 1, 20),
                          (29, 0, 29)])
def test_twice_as_old(test_input1, test_input2, expected):
    """
    Tests the Twice as Old solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert twice_as_old(test_input1, test_input2) == expected
