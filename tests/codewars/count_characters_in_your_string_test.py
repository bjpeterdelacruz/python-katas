"""
Unit tests for Count Characters in Your String challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.count_characters_in_your_string import count


@pytest.mark.parametrize("test_input, expected",
                         [("abcabbccc", {"a": 2, "b": 3, "c": 4})])
def test_count(test_input, expected):
    """
    Tests the Count Characters in Your String solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert count(test_input) == expected
