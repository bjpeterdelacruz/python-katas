"""
Unit tests for Weight for Weight challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.weight_for_weight import order_weight


@pytest.mark.parametrize("test_input, expected",
                         [("56 65 74 100 99 68 86 180 90", "100 180 90 56 65 74 68 86 99")])
def test_order_weight(test_input, expected):
    """
    Tests the Weight for Weight solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert order_weight(test_input) == expected
