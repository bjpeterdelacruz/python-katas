"""
Unit tests for Thinkful: Poem Formatter challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.thinkful_string_drills_poem_formatter import format_poem

tests = [['Beautiful is better than ugly. Explicit is better than implicit. Simple is better than '
          'complex. Complex is better than complicated.', 'Beautiful is better than '
                                                          'ugly.\nExplicit is better than '
                                                          'implicit.\nSimple is better than '
                                                          'complex.\nComplex is better than '
                                                          'complicated.'],
         [
             "Flat is better than nested. Sparse is better than dense. Readability counts. "
             "Special cases aren't special enough to break the rules.",
             "Flat is better than nested.\nSparse is better than dense.\nReadability "
             "counts.\nSpecial cases aren't special enough to break the rules."],
         [
             "Although practicality beats purity. Errors should never pass silently. Unless "
             "explicitly silenced. In the face of ambiguity, refuse the temptation to guess.",
             "Although practicality beats purity.\nErrors should never pass silently.\nUnless "
             "explicitly silenced.\nIn the face of ambiguity, refuse the temptation to guess."]]


def test_format_poem():
    """
    Tests the Thinkful: Poem Formatter solution.

    :return: None
    """
    for test in tests:
        assert format_poem(test[0]) == test[1]
