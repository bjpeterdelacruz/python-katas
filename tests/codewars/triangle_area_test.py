"""
Unit tests for Triangle Area challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.triangle_area import t_area


@pytest.mark.parametrize("test_input, expected",
                         [('\n.\n. .\n', 0.5), ('\n.\n. .\n. . .\n. . . .\n. . . . .\n', 8),
                          ('\n.\n. .\n. . .\n', 2),
                          ('\n.\n. .\n. . .\n. . . .\n. . . . .\n. . . . . .\n. . . . . . .'
                           '\n. . . . . . . .\n. . . . . . . . .\n', 32)])
def test_t_area(test_input, expected):
    """
    Tests the Triangle Area solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert t_area(test_input) == expected
