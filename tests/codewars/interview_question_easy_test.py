"""
Unit tests for Interview Question challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.interview_question_easy import get_strings


@pytest.mark.parametrize("test_input, expected",
                         [("Chicago", "c:**,h:*,i:*,a:*,g:*,o:*"),
                          ("Las Vegas", "l:*,a:**,s:**,v:*,e:*,g:*")])
def test_get_strings(test_input, expected):
    """
    Tests the Interview Question solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_strings(test_input) == expected
