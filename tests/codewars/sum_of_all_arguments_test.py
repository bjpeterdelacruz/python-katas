"""
Unit tests for Sum of All Arguments challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.sum_of_all_arguments import sum_args


def test_sum_args():
    """
    Tests the Sum of All Arguments solution.

    :return: None
    """
    assert sum_args() == 0
    assert sum_args(0) == 0
    assert sum_args(5) == 5
    assert sum_args(-3) == -3
    assert sum_args(1, 3) == 4
    assert sum_args(-1, 3) == 2
    assert sum_args(1, 2, 3) == 6
    assert sum_args(-1, -2, 3) == 0
