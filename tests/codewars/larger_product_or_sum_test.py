"""
Unit tests for Larger Product or Sum challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.larger_product_or_sum import sum_or_product


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 4, "sum"),
                          ([10, 41, 8, 16, 20, 36, 9, 13, 20], 3, "product"),
                          ([10, 20, 3, 30, 5, 4], 3, "same"),
                          ([23, 7, 12, 9, 12, 17, 3, 8, 5, 2, 23], 3, "sum"),
                          ([13, 8, 22, 39, 12, 6, 14, 19, 4, 7, 33], 4, "product"),
                          ([-10, 42, 5, -7, 3, 18], 4, "product"),
                          ([-4, -1, 5, -7, -2, 6, 20, 23, 7], 4, "same"),
                          ([-13, 2, -15, 5, -17, 3], 3, "sum"),
                          ([-100, -70, -50, -20, 10, 40, 70, 100], 6, "product"),
                          ([4, 8, 12, 7, 8, -2, 3, -10], 2, "same")])
def test_sum_or_product(test_input1, test_input2, expected):
    """
    Tests the Larger Product or Sum solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert sum_or_product(test_input1, test_input2) == expected
