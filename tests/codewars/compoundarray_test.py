"""
Unit tests for CompoundArray challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.compoundarray import compound_array


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3, 4, 5, 6], [9, 8, 7, 6], [1, 9, 2, 8, 3, 7, 4, 6, 5, 6]),
                          ([0, 1, 2], [9, 8, 7, 6, 5, 4, 3, 2, 1, 0],
                           [0, 9, 1, 8, 2, 7, 6, 5, 4, 3, 2, 1, 0]),
                          ([11, 12], [21, 22, 23, 24], [11, 21, 12, 22, 23, 24]),
                          ([2147483647, 2147483646, 2147483645, 2147483644, 2147483643], [9],
                           [2147483647, 9, 2147483646, 2147483645, 2147483644, 2147483643]),
                          ([214, 215, 216, 217, 218], [], [214, 215, 216, 217, 218]),
                          ([], [214, 215, 219, 217, 218], [214, 215, 219, 217, 218]),
                          ([], [], [])])
def test_compound_array(test_input1, test_input2, expected):
    """
    Tests the CompoundArray solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert compound_array(test_input1, test_input2) == expected
