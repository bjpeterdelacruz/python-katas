"""
Unit tests for Club Doorman challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.club_doorman import pass_the_door_man


@pytest.mark.parametrize("test_input, expected",
                         [("lettuce", 60), ("hill", 36), ("apple", 48)])
def test_pass_the_door_man(test_input, expected):
    """
    Tests the Club Doorman solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert pass_the_door_man(test_input) == expected
