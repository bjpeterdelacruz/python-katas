"""
Unit tests for Arrh, Grabscrab! challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.arrh_grabscrab import grabscrab


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("trisf", ["first"], ["first"]),
                          ("oob", ["bob", "baobab"], []),
                          ("ainstuomn", ["mountains", "hills", "mesa"], ["mountains"]),
                          ("oolp", ["donkey", "pool", "horse", "loop"], ["pool", "loop"]),
                          ("ortsp", ["sport", "parrot", "ports", "matey"], ["sport", "ports"]),
                          ("ourf", ["one","two","three"], [])])
def test_grabscrab(test_input1, test_input2, expected):
    """
    Tests the Arrh, Grabscrab! solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert grabscrab(test_input1, test_input2) == expected
