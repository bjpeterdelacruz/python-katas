"""
Unit tests for Title Case challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.title_case import title_case


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [('', None, ''), ('a clash of KINGS', 'a an the of', 'A Clash of Kings'),
                          ('THE WIND IN THE WILLOWS', 'The In', 'The Wind in the Willows'),
                          ('    THE WIND IN THE WILLOWS', 'The In', '    The Wind in the Willows'),
                          ('the quick brown fox', None, 'The Quick Brown Fox')])
def test_title_case(test_input1, test_input2, expected):
    """
    Tests the Title Case solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    if test_input2:
        assert title_case(test_input1, test_input2) == expected
    else:
        assert title_case(test_input1) == expected
