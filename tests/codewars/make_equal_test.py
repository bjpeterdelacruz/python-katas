"""
Unit tests for Make Equal challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.make_equal import count


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [([11, 5, 3], 7, 2, 3), ([-4, 6, 8], -7, -3, 2),
                          ([2, 4, 6, 8], 4, 0, 1)])
def test_count(test_input1, test_input2, test_input3, expected):
    """
    Tests the Make Equal solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert count(test_input1, test_input2, test_input3) == expected
