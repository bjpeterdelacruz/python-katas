"""
Unit tests for IQ Test challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.iq import iq_test


@pytest.mark.parametrize("test_input, expected",
                         [("2 4 7 8 10", 3), ("1 2 1 1", 2), ("1 1 1 1 2 1 1", 5), ("1 2 2", 1)])
def test_f(test_input, expected):
    """
    Tests the IQ Test solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert iq_test(test_input) == expected
