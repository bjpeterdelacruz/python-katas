"""
Unit tests for The reject() Function challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.the_reject_function import reject


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3, 4, 5], lambda x: x % 2 == 0, [1, 3, 5]),
                          ([1, 'a', 'b', 4.5, 6], lambda x: isinstance(x, str), [1, 4.5, 6])])
def test_reject(test_input1, test_input2, expected):
    """
    Tests the reject function.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert reject(test_input1, test_input2) == expected
