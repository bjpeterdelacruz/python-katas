"""
Unit tests for Complete Series challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.complete_series import complete_series


@pytest.mark.parametrize("test_input, expected",
                         [([0, 1], [0, 1]), ([3, 4, 6], [0, 1, 2, 3, 4, 5, 6]),
                          ([3, 4, 5], [0, 1, 2, 3, 4, 5]), ([2, 1], [0, 1, 2]),
                          ([1, 4, 4, 6], [0])])
def test_complete_series(test_input, expected):
    """
    Tests the Complete Series solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert complete_series(test_input) == expected
