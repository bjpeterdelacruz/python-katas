"""
Unit tests for Case Sensitive challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.case_sensitive_1 import case_sensitive


@pytest.mark.parametrize("test_input, expected",
                         [("asd", [True, []]), ("cOdEwArS", [False, ["O", "E", "A", "S"]]),
                          ("z", [True, []]), ("", [True, []])])
def test_case_sensitive(test_input, expected):
    """
    Tests the Case Sensitive solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert case_sensitive(test_input) == expected
