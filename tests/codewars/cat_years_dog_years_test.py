"""
Unit tests for Cat Years, Dog Years challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.cat_years_dog_years import human_years_cat_years_dog_years


@pytest.mark.parametrize("test_input, expected",
                         [(1, [1, 15, 15]), (2, [2, 24, 24]), (10, [10, 56, 64])])
def test_human_years_cat_years_dog_years(test_input, expected):
    """
    Tests the Cat Years, Dog Years solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert human_years_cat_years_dog_years(test_input) == expected
