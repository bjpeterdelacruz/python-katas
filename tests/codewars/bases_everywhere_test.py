"""
Unit tests for Bases Everywhere challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.bases_everywhere import base_finder


@pytest.mark.parametrize("test_input, expected",
                         [(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'], 10),
                          (['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'], 10),
                          (['1', '2', '3', '4', '5', '6', '10', '11', '12', '13'], 7),
                          (['301', '302', '303', '304', '305', '310', '311', '312', '313', '314'],
                           6),
                          (['50', '51', '61', '53', '54', '60', '52', '62', '55', '56'], 7)])
def test_base_finder(test_input, expected):
    """
    Tests the Bases Everywhere solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert base_finder(test_input) == expected
