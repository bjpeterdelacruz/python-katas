"""
Unit tests for Even Binary Sorting challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.even_binary_sorting import even_binary


@pytest.mark.parametrize("test_input, expected",
                         [("101 111 100 001 010", "101 111 010 001 100")])
def test_even_binary(test_input, expected):
    """
    Tests the Even Binary Sorting solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert even_binary(test_input) == expected
