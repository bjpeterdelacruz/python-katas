"""
Unit tests for FIRE and FURY challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.fire_and_fury import fire_and_fury


@pytest.mark.parametrize("test_input, expected",
                         [("AAFIREBBFURYCC", "Fake tweet."),
                          ("FIREYYFURYYFURYYFURRYFURYYYFIRE",
                           "You are fired! I am really really furious. You are fired!"),
                          ("FURYYYFIREYYFIREYYFIREYYYFIRE",
                           "I am furious. You and you and you and you are fired!"),
                          ("ABCFURRYDEFFIIRE", "Fake tweet."),
                          ("FIIREYYYFURYUUU", "I am furious.")])
def test_waterbombs(test_input, expected):
    """
    Tests the FIRE and FURY solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert fire_and_fury(test_input) == expected
