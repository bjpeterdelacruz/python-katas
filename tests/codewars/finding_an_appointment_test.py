"""
Unit tests for Finding an Appointment challenge

:Author: BJ Peter Dela Cruz
"""
from random import shuffle
from codewars.finding_an_appointment import get_start_time


def test_get_start_time():
    """
    Tests the Finding an Appointment solution.

    :return: None
    """
    schedules = [
        [['09:00', '11:30'], ['13:30', '16:00'], ['16:00', '17:30'], ['17:45', '19:00']],
        [['09:15', '12:00'], ['14:00', '16:30'], ['17:00', '17:30']],
        [['11:30', '12:15'], ['15:00', '16:30'], ['17:45', '19:00']]
    ]
    assert get_start_time(schedules, 60) == "12:15"
    assert get_start_time(schedules, 75) == "12:15"

    schedules = [
        [['09:00', '19:00']],
        [],
        [],
        []
    ]
    assert get_start_time(schedules, 1) is None

    schedules = [
        [['10:00', '13:00'], ['14:00', '17:00'], ['18:00', '19:00']],
        [['10:00', '11:00'], ['12:00', '13:00'], ['14:00', '15:00'], ['16:00', '17:00'],
         ['18:00', '19:00']]
    ]
    shuffle(schedules)
    assert get_start_time(schedules, 60) == "09:00"

    schedules = [
      [['09:37', '11:19'], ['11:27', '13:37'], ['16:29', '17:41']],
      [['10:07', '10:39'], ['10:41', '11:03'], ['12:21', '12:22'], ['15:49', '16:11'],
       ['17:29', '17:54']],
      [['09:48', '12:26'], ['15:41', '15:59'], ['18:50', '18:57']],
      [['11:21', '12:42'], ['12:51', '13:20'], ['17:51', '17:53'], ['18:07', '18:11']],
      [['09:41', '09:57'], ['10:03', '10:14'], ['10:32', '10:39'], ['10:56', '11:17'],
       ['11:23', '11:41'], ['11:59', '12:03'], ['12:28', '12:45'], ['17:19', '17:27'],
       ['18:56', '18:57']]
    ]
    shuffle(schedules)
    assert get_start_time(schedules, 37) == "09:00"
    assert get_start_time(schedules, 38) == "13:37"
    assert get_start_time(schedules, 124) == "13:37"
    assert get_start_time(schedules, 125) is None

    schedules = [
      [['09:09', '11:27'], ['12:14', '13:41'], ['15:16', '17:17'], ['17:32', '18:50']],
      [['10:38', '12:06'], ['13:39', '15:08'], ['17:23', '17:26'], ['18:02', '18:26']]
    ]
    shuffle(schedules)
    assert get_start_time(schedules, 9) == "09:00"
    assert get_start_time(schedules, 10) == "18:50"
    assert get_start_time(schedules, 11) is None
