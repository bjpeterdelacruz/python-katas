"""
Unit tests for Homogenous Arrays challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.homogenous_arrays import filter_homogenous


@pytest.mark.parametrize("test_input, expected",
                         [([[1, 5, 4], ['a', 3, 5], ['b'], [], ['1', 2, 3]], [[1, 5, 4], ['b']]),
                          ([[123, 234, 432], ['', 'abc'], [''], ['', 1], ['', '1'], []],
                           [[123, 234, 432], ['', 'abc'], [''], ['', '1']]),
                          (
                          [[1, 2, 3], ["1", "2", "3"], ["1", 2, 3]], [[1, 2, 3], ["1", "2", "3"]])])
def test_is_kiss(test_input, expected):
    """
    Tests the Homogenous Arrays solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert filter_homogenous(test_input) == expected
