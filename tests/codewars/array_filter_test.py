"""
Unit tests for Array Filter challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.array_filter import get_even_numbers


@pytest.mark.parametrize("test_input, expected",
                         [([], []), ([1], []), ([1, 2], [2]), ([2, 3, 4, 5, 6], [2, 4, 6])])
def test_get_even_numbers(test_input, expected):
    """
    Tests the Array Filter solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_even_numbers(test_input) == expected
