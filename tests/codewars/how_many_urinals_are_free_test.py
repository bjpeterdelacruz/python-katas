"""
Unit tests for How Many Urinals Are Free challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.how_many_urinals_are_free import get_free_urinals


@pytest.mark.parametrize("test_input, expected",
                         [("10001", 1), ("1001", 0), ("00000", 3), ("0000", 2), ("01000", 1),
                          ("00010", 1), ("10000", 2), ("1", 0), ("0", 1), ("10", 0), ("110", -1),
                          ("1011000001", -1)])
def test_get_free_urinals(test_input, expected):
    """
    Tests the How Many Urinals Are Free solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_free_urinals(test_input) == expected
