"""
Unit tests for Moving Average challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.moving_average import moving_average


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([40, 30, 50, 46, 39, 44], 4, [41.5, 41.25, 44.75]),
                          ([40, 30, 50, 46, 39, 44], 2, [35.0, 40.0, 48.0, 42.5, 41.5]),
                          ([40, 30, 50, 46, 39, 44], 3, [40.0, 42.0, 45.0, 43.0]),
                          ([40, 30, 50, 46, 39, 44], 0, None),
                          ([0, 0, 0, 0, 0, 0, 0], 3, [0.0, 0.0, 0.0, 0.0, 0.0]),
                          ([50, 51, 52, 53, 54], 5, [52.0]),
                          ([40, 30, 50, 46, 39, 44], 7, None)])
def test_moving_average(test_input1, test_input2, expected):
    """
    Tests the Moving Average solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert moving_average(test_input1, test_input2) == expected
