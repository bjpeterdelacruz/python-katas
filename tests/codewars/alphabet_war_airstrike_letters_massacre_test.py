"""
Unit tests for Alphabet War Airstrike challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.alphabet_war_airstrike_letters_massacre import alphabet_war


@pytest.mark.parametrize("test_input, expected",
                         [("z", "Right side wins!"), ("****", "Let's fight again!"),
                          ("z*dq*mw*pb*s", "Let's fight again!"),
                          ("zdqmwpbs", "Let's fight again!"), ("zz*zzs", "Right side wins!"),
                          ("sz**z**zs", "Left side wins!"), ("z*z*z*zs", "Left side wins!"),
                          ("*wwwwww*z*", "Left side wins!"), ("w****z" , "Let's fight again!"),
                          ("mb**qwwp**dm" , "Let's fight again!")])
def test_alphabet_war(test_input, expected):
    """
    Tests the Alphabet War Airstrike solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert alphabet_war(test_input) == expected
