"""
Unit tests for Convert the Score challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.convert_the_score import scoreboard


@pytest.mark.parametrize("test_input, expected",
                         [("The score is four nil", [4, 0]),
                          ("new score: two three", [2, 3]),
                          ("two two", [2, 2]),
                          ("The team just kicked another field goal, three nil", [3, 0]),
                          ("The final score is zero five", [0, 5])])
def test_scoreboard(test_input, expected):
    """
    Tests the Convert the Score solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert scoreboard(test_input) == expected
