"""
Unit tests for Exclamation Marks Series #15 challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.exclamation_marks_series_number_15_replace_the_pair_of_exclamation_marks_and_question_marks_to_spaces import replace  # pylint: disable=line-too-long


@pytest.mark.parametrize("test_input, expected",
                         [("", ""),
                          ("!!", "!!"),
                          ("!??", "!??"),
                          ("!?", "  "),
                          ("!!??", "    "),
                          ("!!!????", "!!!????"),
                          ("!??!!", "!    "),
                          ("!????!!!?", " ????!!! "),
                          ("!?!!??!!!?", "      !!!?"),
                          ("!!?!!?!!?!!?!!!??!!!??!!!??", "  ?  ?  ?!!?!!!  !!!  !!!  "),
                          ("??!??!??!??!???!!???!!???!!", "  !  !  !??!???  ???  ???  "),
                          ("?!!?!!?!!?!!!??!!!??!!!??!!!??!!!??",
                           "?  ?  ?  ?!!!  !!!  !!!  !!!??!!!??")])
def test_replace(test_input, expected):
    """
    Tests the Exclamation Marks Series #15 solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert replace(test_input) == expected
