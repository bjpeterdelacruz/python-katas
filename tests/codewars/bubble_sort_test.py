"""
Unit tests for Bubble Sort challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.bubble_sort import bubble


def test_bubble():
    """
    Tests the Bubble Sort solution.

    :return: None
    """
    lst = [1, 2, 4, 3]
    sol = [[1, 2, 3, 4]]
    assert bubble(lst) == sol

    lst = [2, 1, 4, 3]
    sol = [[1, 2, 4, 3], [1, 2, 3, 4]]
    assert bubble(lst) == sol

    lst = [1, 4, 3, 6, 7, 9, 2, 5, 8]
    sol = [[1, 3, 4, 6, 7, 9, 2, 5, 8],
           [1, 3, 4, 6, 7, 2, 9, 5, 8],
           [1, 3, 4, 6, 7, 2, 5, 9, 8],
           [1, 3, 4, 6, 7, 2, 5, 8, 9],
           [1, 3, 4, 6, 2, 7, 5, 8, 9],
           [1, 3, 4, 6, 2, 5, 7, 8, 9],
           [1, 3, 4, 2, 6, 5, 7, 8, 9],
           [1, 3, 4, 2, 5, 6, 7, 8, 9],
           [1, 3, 2, 4, 5, 6, 7, 8, 9],
           [1, 2, 3, 4, 5, 6, 7, 8, 9]]
    assert bubble(lst) == sol

    assert not bubble([])

    assert not bubble([1, 2, 3, 4, 5, 6, 7, 8, 9])

    assert bubble([1, 3, 3, 7, 4, 2]) == [[1, 3, 3, 4, 7, 2], [1, 3, 3, 4, 2, 7],
                                          [1, 3, 3, 2, 4, 7], [1, 3, 2, 3, 4, 7],
                                          [1, 2, 3, 3, 4, 7]]
