"""
Unit tests for Find the Odd Int challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_the_odd_int import find_it


@pytest.mark.parametrize("test_input, expected",
                         [([20, 1, -1, 2, -2, 3, 3, 5, 5, 1, 2, 4, 20, 4, -1, -2, 5], 5),
                         ([], None), ([10], 10), ([1, 1, 2, -2, 5, 2, 4, 4, -1, -2, 5], -1),
                         ([20, 1, 1, 2, 2, 3, 3, 5, 5, 4, 20, 4, 5], 5),
                         ([1, 1, 1, 1, 1, 1, 10, 1, 1, 1, 1], 10),
                         ([5, 4, 3, 2, 1, 5, 4, 3, 2, 10, 10], 1)])
def test_find_it(test_input, expected):
    """
    Tests the Find the Odd Int solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_it(test_input) == expected
