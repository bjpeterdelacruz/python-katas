"""
Unit tests for Detect Pangram challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.detect_pangram import is_pangram


@pytest.mark.parametrize("test_input, expected",
                         [("The quick, brown fox jumps over the lazy dog!", True),
                          ("abcdefg", False)])
def test_is_pangram(test_input, expected):
    """
    Tests the Detect Pangram solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_pangram(test_input) == expected
