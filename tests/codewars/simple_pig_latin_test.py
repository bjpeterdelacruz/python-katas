"""
Unit tests for Simple Pig Latin challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.simple_pig_latin import pig_it


@pytest.mark.parametrize("test_input, expected",
                         [("Hello world!!!", "elloHay orldway!!!"),
                          ("This is a test !", "hisTay siay aay esttay !")])
def test_scramble(test_input, expected):
    """
    Tests the Simple Pig Latin solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert pig_it(test_input) == expected
