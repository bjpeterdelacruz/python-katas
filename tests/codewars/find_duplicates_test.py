"""
Unit tests for Find Duplicates challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.find_duplicates import duplicates


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 3, "1", 1, 3, 1, 2], [1, 3, 2]), ([1, 2, 3, 4, 5], [])])
def test_duplicates(test_input, expected):
    """
    Tests the Find Duplicates solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert duplicates(test_input) == expected
