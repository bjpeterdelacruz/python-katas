"""
Unit tests for Loose Change challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.loose_change import loose_change


@pytest.mark.parametrize("test_input, expected",
                         [(29, {'Nickels': 0, 'Pennies': 4, 'Dimes': 0, 'Quarters': 1}),
                          (91, {'Nickels': 1, 'Pennies': 1, 'Dimes': 1, 'Quarters': 3}),
                          (0, {'Nickels': 0, 'Pennies': 0, 'Dimes': 0, 'Quarters': 0}),
                          (-2, {'Nickels': 0, 'Pennies': 0, 'Dimes': 0, 'Quarters': 0}),
                          (3.9, {'Nickels': 0, 'Pennies': 3, 'Dimes': 0, 'Quarters': 0}),
                          (56, {'Nickels': 1, 'Pennies': 1, 'Dimes': 0, 'Quarters': 2}),
                          (-435, {'Nickels': 0, 'Pennies': 0, 'Dimes': 0, 'Quarters': 0}),
                          (4.935, {'Nickels': 0, 'Pennies': 4, 'Dimes': 0, 'Quarters': 0})])
def test_loose_change(test_input, expected):
    """
    Tests the Loose Change solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert loose_change(test_input) == expected
