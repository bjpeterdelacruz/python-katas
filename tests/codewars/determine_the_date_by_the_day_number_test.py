"""
Unit tests for Determine the Date challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.determine_the_date_by_the_day_number import get_day


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(1, False, "January, 1"),
                          (32, False, "February, 1"),
                          (41, False, "February, 10"),
                          (59, False, "February, 28"),
                          (60, False, "March, 1"),
                          (365, False, "December, 31"),
                          (364, False, "December, 30"),
                          (1, True, "January, 1"),
                          (59, True, "February, 28"),
                          (60, True, "February, 29"),
                          (61, True, "March, 1"),
                          (62, True, "March, 2"),
                          (365, True, "December, 30"),
                          (366, True, "December, 31"),
                          (32, True, "February, 1")])
def test_get_day(test_input1, test_input2, expected):
    """
    Tests the Determine the Date solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert get_day(test_input1, test_input2) == expected


def test_get_day_invalid_input():
    """
    Tests the Determine the Date solution with invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        get_day(0, False)
        assert False

    with pytest.raises(ValueError):
        get_day(400, True)
        assert False

    with pytest.raises(ValueError):
        get_day(366, False)
        assert False
