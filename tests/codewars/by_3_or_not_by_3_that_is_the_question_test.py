"""
Unit tests for Divisible by Three challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.by_3_or_not_by_3_that_is_the_question import divisible_by_three


@pytest.mark.parametrize("test_input, expected",
                         [("123", True), ("8409", True), ("100853", False), ("33333333", True),
                          ("7", False)])
def test_divisible_by_three(test_input, expected):
    """
    Tests the Divisible by Three solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert divisible_by_three(test_input) == expected
