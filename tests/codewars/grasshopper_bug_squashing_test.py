"""
Unit tests for Grasshopper: Bug Squashing challenge

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-many-arguments

from unittest.mock import Mock, patch, call
import codewars.grasshopper_bug_squashing


@patch('codewars.grasshopper_bug_squashing.roll_dice')
@patch('codewars.grasshopper_bug_squashing.move')
@patch('codewars.grasshopper_bug_squashing.combat')
@patch('codewars.grasshopper_bug_squashing.get_coins')
@patch('codewars.grasshopper_bug_squashing.buy_health')
@patch('codewars.grasshopper_bug_squashing.print_status')
def test_something(print_status_mock, buy_health_mock, get_coins_mock, combat_mock, move_mock,
                   roll_dice_mock):
    """
    Tests that the functions in the Grasshopper: Bug Squashing challenge are called in a given
    order.

    :param print_status_mock: The mock for the print_status function
    :param buy_health_mock: The mock for the buy_health function
    :param get_coins_mock: The mock for the get_coins function
    :param combat_mock: The mock for the combat function
    :param move_mock: The mock for the move function
    :param roll_dice_mock: The mock for the roll_dice function
    :return: None
    """
    manager = Mock()
    manager.attach_mock(roll_dice_mock, 'roll_dice')
    manager.attach_mock(move_mock, 'move')
    manager.attach_mock(combat_mock, 'combat')
    manager.attach_mock(get_coins_mock, 'get_coins')
    manager.attach_mock(buy_health_mock, 'buy_health')
    manager.attach_mock(print_status_mock, 'print_status')

    codewars.grasshopper_bug_squashing.main()

    expected_calls = [call.roll_dice(), call.move(), call.combat(), call.get_coins(),
                      call.buy_health(), call.print_status()]
    assert manager.mock_calls == expected_calls
