"""
Unit tests for Convert a Hex String to RGB challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.hex_string_to_rgb import hex_string_to_rgb


@pytest.mark.parametrize("test_input, expected",
                         [("#FF9933", {'r': 255, 'g': 153, 'b': 51}),
                          ("#beaded", {'r': 190, 'g': 173, 'b': 237}),
                          ("#000000", {'r': 0, 'g': 0, 'b': 0}),
                          ("#111111", {'r': 17, 'g': 17, 'b': 17}),
                          ("#Fa3456", {'r': 250, 'g': 52, 'b': 86})])
def test_hex_string_to_rgb(test_input, expected):
    """
    Tests the Convert a Hex String to RGB solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert hex_string_to_rgb(test_input) == expected
