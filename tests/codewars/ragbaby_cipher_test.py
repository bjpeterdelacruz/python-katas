"""
Unit tests for Ragbaby Cipher challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.ragbaby_cipher import encode, decode


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("cipher", "cipher", "ihrbfj"),
                          ("cipher", "cccciiiiippphheeeeerrrrr", "ihrbfj"),
                          ("This is an example.", "cipher", "Urew pu bq rzfsbtj."),
                          ("This.tHis.thIs.thiS...", "cipher", "Urew.uRew.urEw.ureW...")])
def test_encode(test_input1, test_input2, expected):
    """
    Tests the Ragbaby Cipher solution (encode).

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert encode(test_input1, test_input2) == expected


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("ihrbfj", "cipher", "cipher"),
                          ("Urew pu bq rzfsbtj.", "cipher", "This is an example."),
                          ("Urew.uRew.urEw.ureW...", "cipher", "This.tHis.thIs.thiS...")])
def test_decode(test_input1, test_input2, expected):
    """
    Tests the Ragbaby Cipher solution (decode).

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert decode(test_input1, test_input2) == expected


def test_encode_then_decode():
    """
    Tests the Ragbaby Cipher solution (decode then encode, and vice versa).

    :return: None
    """
    assert encode(decode("This is an example.", "secretkey"),"secretkey") == "This is an example."
    assert decode(encode("This is an example.", "secretkey"),"secretkey") == "This is an example."

    random_text = "BNdJf WsRImd AOl bKnA l.bn o iKwJQLuol  swvtSIETDgcl" \
                  "  rkECoIKDSmYBOfWijJWAqzwT nzprGF"
    assert encode(decode(random_text, "qlmhqzygmjaipzxjm"), "qlmhqzygmjaipzxjm") == random_text
