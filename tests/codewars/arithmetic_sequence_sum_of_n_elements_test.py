"""
Unit tests for Arithmetic Sequence - Sum of N Elements challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.arithmetic_sequence_sum_of_n_elements import arithmetic_sequence_sum


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [(0, 2, 5, 20), (0, 5, 0, 0), (-100, -5, 3, -315), (-10, 4, 5, -10)])
def test_arithmetic_sequence_sum(test_input1, test_input2, test_input3, expected):
    """
    Tests the Arithmetic Sequence - Sum of N Elements solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert arithmetic_sequence_sum(test_input1, test_input2, test_input3) == expected


def test_seqlist_invalid_input():
    """
    Tests the Arithmetic Sequence - Sum of N Elements solution with invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        arithmetic_sequence_sum(0, 5, -1)
