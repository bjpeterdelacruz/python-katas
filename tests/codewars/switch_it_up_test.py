"""
Unit tests for Switch It Up! challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.switch_it_up import switch_it_up


@pytest.mark.parametrize("test_input, expected",
                         [(0, "Zero"),
                          (1, "One"),
                          (2, "Two"),
                          (3, "Three"),
                          (4, "Four"),
                          (5, "Five"),
                          (6, "Six"),
                          (7, "Seven"),
                          (8, "Eight"),
                          (9, "Nine")])
def test_switch_it_up(test_input, expected):
    """
    Tests the Switch It Up! solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert switch_it_up(test_input) == expected
