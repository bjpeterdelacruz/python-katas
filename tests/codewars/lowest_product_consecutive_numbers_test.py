"""
Unit tests for Lowest Product of Four Consecutive Numbers challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.lowest_product_consecutive_numbers import lowest_product


@pytest.mark.parametrize("test_input, expected",
                         [("123456789", 24), (["234567899", 120]), ("123", "Number is too small")])
def test_lowest_product(test_input, expected):
    """
    Tests the Lowest Product of Four Consecutive Numbers solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert lowest_product(test_input) == expected
