"""
Unit tests for Is Integer Array? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.is_integer_array import is_int_array


@pytest.mark.parametrize("test_input, expected",
                         [([], True), ([1, 2, 3, 4], True), ([-11, -12, -13, -14], True),
                          ([1.0, 2.0, 3.0], True), ([1, 2, None], False), (None, False),
                          ("", False), ([None], False), ([1.0, 2.0, 3.0001], False),
                          (["-1"], False), ([1, 2, 3.0], True)])
def test_is_int_array(test_input, expected):
    """
    Tests the Is Integer Array? solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_int_array(test_input) == expected
