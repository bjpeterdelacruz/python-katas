"""
Unit tests for Is He Gonna Survive? challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.is_he_gonna_survive import hero


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(10, 5, True),
                          (7, 4, False),
                          (4, 5, False),
                          (100, 40, True),
                          (1500, 751, False),
                          (0, 1, False)])
def test_hero(test_input1, test_input2, expected):
    """
    Tests the Is He Gonna Survive? solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert hero(test_input1, test_input2) == expected
