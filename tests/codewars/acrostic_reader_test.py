"""
Unit tests for Acrostic Reader challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.acrostic_reader import read_out


@pytest.mark.parametrize("test_input, expected",
                         [(["Big", "Joe"], "BJ"), ("", "")])
def test_read_out(test_input, expected):
    """
    Tests the Acrostic Reader solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert read_out(test_input) == expected
