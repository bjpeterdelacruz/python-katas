"""
Unit tests for Double Every Other challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.double_every_other import double_every_other


@pytest.mark.parametrize("test_input, expected",
                         [([1, 3, 2, 4, 3, 5], [1, 6, 2, 8, 3, 10]), ([], []), ([1], [1])])
def test_double_every_other(test_input, expected):
    """
    Tests the Double Every Other solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert double_every_other(test_input) == expected
