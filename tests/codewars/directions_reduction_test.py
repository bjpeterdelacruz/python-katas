"""
Unit tests for Direction Reduction challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.directions_reduction import direction_reduction


@pytest.mark.parametrize("test_input, expected",
                         [(['NORTH', 'EAST', 'EAST', 'NORTH', 'SOUTH', 'NORTH', 'WEST', 'SOUTH',
                            'SOUTH', 'EAST', 'SOUTH', 'WEST', 'WEST', 'EAST'],
                           ['NORTH', 'EAST', 'EAST', 'NORTH', 'WEST', 'SOUTH', 'SOUTH', 'EAST',
                            'SOUTH', 'WEST']),
                          (['EAST', 'SOUTH', 'EAST', 'SOUTH', 'WEST', 'EAST', 'NORTH', 'SOUTH',
                            'SOUTH', 'SOUTH', 'NORTH', 'EAST', 'EAST', 'EAST', 'WEST', 'EAST',
                            'SOUTH', 'NORTH', 'NORTH', 'SOUTH', 'EAST', 'EAST', 'WEST', 'NORTH'],
                           ['EAST', 'SOUTH', 'EAST', 'SOUTH', 'SOUTH', 'EAST', 'EAST', 'EAST',
                            'EAST', 'NORTH']),
                          (["NORTH", "WEST", "SOUTH", "EAST"], ["NORTH", "WEST", "SOUTH", "EAST"])])
def test_direction_reduction(test_input, expected):
    """
    Tests the Direction Reduction solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert direction_reduction(test_input) == expected
