"""
Unit tests for Center Yourself challenge

:Author: BJ Peter Dela Cruz
"""
from codewars.center_yourself_says_the_monk import center


def test_center():
    """
    Tests the Center Yourself solution.

    :return: None
    """
    check_center("", "", 0, '_')
    check_center("_", "", 1, '_')
    check_center("__", "", 2, '_')
    check_center("a", "a", 0, '_')
    check_center("a", "a", 1, '_')
    check_center("_a", "a", 2, '_')
    check_center("_a_", "a", 3, '_')
    check_center("__a_", "a", 4, '_')
    check_center("__a__", "a", 5, '_')
    check_center("ab", "ab", 0, '_')
    check_center("ab", "ab", 1, '_')
    check_center("ab", "ab", 2, '_')
    check_center("_ab", "ab", 3, '_')
    check_center("_ab_", "ab", 4, '_')
    check_center("__ab_", "ab", 5, '_')
    check_center("__ab__", "ab", 6, '_')
    check_center("abc", "abc", 0, '_')
    check_center("abc", "abc", 1, '_')
    check_center("abc", "abc", 2, '_')
    check_center("abc", "abc", 3, '_')
    check_center("_abc", "abc", 4, '_')
    check_center("_abc_", "abc", 5, '_')
    check_center("__abc_", "abc", 6, '_')
    check_center("__abc__", "abc", 7, '_')
    check_center("abcd", "abcd", 0, '_')
    check_center("abcd", "abcd", 1, '_')
    check_center("abcd", "abcd", 2, '_')
    check_center("abcd", "abcd", 3, '_')
    check_center("abcd", "abcd", 4, '_')
    check_center("_abcd", "abcd", 5, '_')
    check_center("_abcd_", "abcd", 6, '_')
    check_center("__abcd_", "abcd", 7, '_')
    check_center("__abcd__", "abcd", 8, '_')

    check_center(" a ", "a", 3)
    check_center("  a ", "a", 4)
    check_center("   abc   ", "abc", 9)
    check_center("    abc   ", "abc", 10)
    check_center("abcdefgh", "abcdefgh", 2)
    check_center("abcdefgh", "abcdefgh", 8)


def check_center(expected: str, test_input1: str, test_input2: int, test_input3: str = ' '):
    """
    Tests the Center Yourself solution.

    :param expected: The expected output
    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :return: None
    """
    assert center(test_input1, test_input2, test_input3) == expected
