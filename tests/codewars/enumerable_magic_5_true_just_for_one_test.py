"""
Unit tests for Enumerable Magic #5: True Just for One challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.enumerable_magic_5_true_just_for_one import one


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3, 4, 5], lambda x: x < 3, False),
                          ([1, 2, 3, 4, 5], lambda x: x < 2, True),
                          ([1, 2, 3, 4, 5], lambda x: x < 1, False),
                          ([1, 2, 3, 4, 5], lambda x: x == 3, True),
                          ([1, 2, 3, 3, 5], lambda x: x == 3, False)])
def test_one(test_input1, test_input2, expected):
    """
    Tests the Enumerable Magic #5: True Just for One solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert one(test_input1, test_input2) == expected
