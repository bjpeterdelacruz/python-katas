"""
Unit tests for Alternate Case challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.alternate_case import alternate_case


@pytest.mark.parametrize("test_input, expected",
                         [("hElLo -- wORLd", "HeLlO -- WorlD")])
def test_alternate_case(test_input, expected):
    """
    Tests the Alternate Case solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert alternate_case(test_input) == expected
