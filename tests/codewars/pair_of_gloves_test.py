"""
Unit tests for Pairs of Gloves challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.pair_of_gloves import number_of_pairs


@pytest.mark.parametrize("test_input, expected",
                         [(["red", "green", "red", "blue", "blue"], 2),
                          (["red", "red"], 1), (["red", "green", "blue"], 0), ([], 0),
                          (["gray", "black", "purple", "purple", "gray", "black"], 3),
                          (["red", "green", "blue", "blue", "red", "green", "red", "red", "red"],
                           4)])
def test_number_of_pairs(test_input, expected):
    """
    Tests the Pairs of Gloves solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert number_of_pairs(test_input) == expected
