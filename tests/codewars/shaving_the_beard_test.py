"""
Unit tests for Shaving the Beard challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.shaving_the_beard import trim


@pytest.mark.parametrize("test_input, expected",
                         [([["J", '|'], ["J", '|'], ["...", '|']],
                           [["|", '|'], ["|", '|'], ["...", '...']]),
                          ([["J", "|", "J"], ["J", "|", "|"], ["...", "|", "J"]],
                           [["|", "|", "|"], ["|", "|", "|"], ["...", "...", "..."]]),
                          ([["J", "|", "J", "J"], ["J", "|", "|", "J"], ["...", "|", "J", "|"]],
                           [["|", "|", "|", "|"], ["|", "|", "|", "|"],
                            ["...", "...", "...", "..."]])])
def test_trim(test_input, expected):
    """
    Tests the Shaving the Beard solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert trim(test_input) == expected
