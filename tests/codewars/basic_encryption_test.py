"""
Unit tests for Basic Encryption challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.basic_encryption import encrypt


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("", 1, ""), ("a", 2, "c"),
                          ("please encrypt me", 2, "rngcug\"gpet{rv\"og")])
def test_basic_encryption(test_input1, test_input2, expected):
    """
    Tests the Basic Encryption solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert encrypt(test_input1, test_input2) == expected
