"""
Unit tests for Dbftbs Djqifs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codewars.dbftbs_djqifs import encryptor


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(13, '', ''), (13, 'Caesar Cipher', 'Pnrfne Pvcure'),
                          (-5, 'Hello World!', 'Czggj Rjmgy!'),
                          (27, 'Whoopi Goldberg', 'Xippqj Hpmecfsh')])
def test_encryptor(test_input1, test_input2, expected):
    """
    Tests the Dbftbs Djqifs solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert encryptor(test_input1, test_input2) == expected
