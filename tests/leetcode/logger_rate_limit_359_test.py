"""
Unit tests for 359. Logger Rate Limit challenge

:Author: BJ Peter Dela Cruz
"""
from leetcode.logger_rate_limit_359 import Logger


def test_maxstack():
    """
    Tests the 359. Logger Rate Limit solution.

    :return: None
    """
    logger = Logger()
    assert logger.should_print_message(4, "Testing 1, 2, 3...") is True
    assert logger.should_print_message(8, "Testing 1, 2, 3...") is False
    assert logger.should_print_message(7, "Testing 4, 5, 6...") is True
    assert logger.should_print_message(13, "Testing 1, 2, 3...") is False
    assert logger.should_print_message(14, "Testing 1, 2, 3...") is True
    assert logger.should_print_message(15, "Testing 1, 2, 3...") is False
    assert logger.should_print_message(15, "Testing 4, 5, 6...") is False
    assert logger.should_print_message(16, "Testing 4, 5, 6...") is False
    assert logger.should_print_message(17, "Testing 4, 5, 6...") is True
    assert logger.should_print_message(18, "Testing 4, 5, 6...") is False
