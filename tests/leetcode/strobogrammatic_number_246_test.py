"""
Unit tests for 246. Strobogrammatic Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from leetcode.strobogrammatic_number_246 import is_strobogrammatic


@pytest.mark.parametrize("test_input, expected",
                         [("6", False), ("9", False), ("11", True), ("808", True), ("18181", True),
                          ("69", True), ("160", False), ("66099", True), ("66029", False),
                          ("232", False), ("699601109669", True)])
def test_is_strobogrammatic(test_input, expected):
    """
    Tests the 246. Strobogrammatic Number solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_strobogrammatic(test_input) == expected
