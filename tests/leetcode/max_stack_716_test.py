"""
Unit tests for 716. Max Stack challenge

:Author: BJ Peter Dela Cruz
"""
from leetcode.max_stack_716 import MaxStack


def test_maxstack():
    """
    Tests the 716. Max Stack solution.

    :return: None
    """
    stack = MaxStack()
    assert not stack.stack
    stack.push(5)
    stack.push(1)
    stack.push(-5)
    assert stack.stack == [5, 1, -5]
    assert stack.pop() == -5
    assert stack.top() == 1
    assert stack.peek_max() == 5
    assert stack.pop_max() == 5
    assert stack.stack == [1]
    assert stack.peek_max() == 1
    assert stack.pop_max() == 1
    assert not stack.stack
    stack.push(10)
    stack.push(1)
    stack.push(10)
    stack.push(5)
    assert stack.pop_max() == 10
    assert stack.stack == [10, 1, 5]
