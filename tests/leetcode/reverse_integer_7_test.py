"""
Unit tests for 7. Reverse Integer challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from leetcode.reverse_integer_7 import reverse


@pytest.mark.parametrize("test_input, expected",
                         [(1234, 4321), (-321, -123), (2_147_483_648, 0), (-2_147_483_649, 0)])
def test_reverse(test_input, expected):
    """
    Tests the 7. Reverse Integer solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert reverse(test_input) == expected
