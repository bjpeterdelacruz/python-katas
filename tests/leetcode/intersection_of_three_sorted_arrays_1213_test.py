"""
Unit tests for 1213. Intersection of Three Sorted Arrays challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from leetcode.intersection_of_three_sorted_arrays_1213 import arrays_intersection


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [([1, 2, 3], [4, 5, 6], [7, 8, 9], []), ([1, 2], [1, 3], [2, 3], []),
                          ([1, 2, 3], [2, 3], [3], [3]),
                          ([1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3])])
def test_arrays_intersection(test_input1, test_input2, test_input3, expected):
    """
    Tests the 1213. Intersection of Three Sorted Arrays solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert arrays_intersection(test_input1, test_input2, test_input3) == expected
