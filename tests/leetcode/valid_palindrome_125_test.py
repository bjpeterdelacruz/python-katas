"""
Unit tests for 125. Valid Palindrome challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from leetcode.valid_palindrome_125 import is_palindrome


@pytest.mark.parametrize("test_input, expected",
                         [("A man, a plan, a canal: Panama", True), ("race a car", False),
                          (" ", True), ("  Anna ", True)])
def test_is_palindrome(test_input, expected):
    """
    Tests the 125. Valid Palindrome solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_palindrome(test_input) == expected
