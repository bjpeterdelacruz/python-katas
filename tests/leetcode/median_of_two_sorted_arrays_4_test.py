"""
Unit tests for 4. Median of Two Sorted Arrays challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from leetcode.median_of_two_sorted_arrays_4 import find_median_sorted_arrays


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [([1, 2, 3], [4, 5, 6], 3.5), ([1, 2, 3, 4], [], 2.5),
                          ([], [3, 6, 9], 6), ([1, 2], [2, 3, 4], 2)])
def test_find_median_sorted_arrays(test_input1, test_input2, expected):
    """
    Tests the 4. Median of Two Sorted Arrays solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_median_sorted_arrays(test_input1, test_input2) == expected
