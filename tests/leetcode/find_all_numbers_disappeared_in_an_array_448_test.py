"""
Unit tests for 448. Find All Numbers Disappeared in an Array challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from leetcode.find_all_numbers_disappeared_in_an_array_448 import find_disappeared_numbers


@pytest.mark.parametrize("test_input, expected",
                         [([1, 1, 1], [2, 3]), ([1, 2, 3], []), ([5, 3, 2, 4], [1])])
def test_find_disappeared_numbers(test_input, expected):
    """
    Tests the 448. Find All Numbers Disappeared in an Array solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_disappeared_numbers(test_input) == expected
