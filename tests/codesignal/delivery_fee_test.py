"""
Unit tests for Delivery Fee challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codesignal.delivery_fee import delivery_fee


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [([0, 10, 22], [1, 3, 1],
                           [[8, 15], [12, 21], [15, 48], [20, 17], [23, 43]], True),
                          ([0, 10, 22], [1, 3, 1], [[8, 15], [12, 21], [15, 48], [20, 17]], False),
                          ([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                            20, 21, 22, 23],
                            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                             20, 21, 22, 23],
                           [[0, 32], [1, 58], [2, 10], [3, 23], [4, 59], [5, 4], [6, 36],
                            [7, 52], [8, 38], [9, 7], [10, 43], [11, 54], [12, 7], [13, 15],
                            [14, 12], [15, 29], [16, 48], [17, 1], [18, 47], [19, 21], [20, 13],
                            [21, 51], [22, 7], [23, 20]], False)])
def test_delivery_fee(test_input1, test_input2, test_input3, expected):
    """
    Tests the Delivery Fee solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert delivery_fee(test_input1, test_input2, test_input3) == expected
