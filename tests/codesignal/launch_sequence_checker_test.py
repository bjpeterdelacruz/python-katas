"""
Unit tests for Launch Sequence Checker challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codesignal.launch_sequence_checker import launch_sequence_checker


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(["Falcon 9", "Falcon 9", "Falcon 9", "Falcon 9", "Falcon 9", "Falcon 9"],
                           [1, 3, 5, 7, 7, 9], False),
                          (["stage_1", "stage_2", "dragon", "stage_1", "stage_2", "dragon"],
                           [1, 10, 11, 2, 12, 111], True)])
def test_launch_sequence_checker(test_input1, test_input2, expected):
    """
    Tests the Launch Sequence Checker solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert launch_sequence_checker(test_input1, test_input2) == expected
