"""
Unit tests for Find Middle Element challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codesignal.find_middle_element import ListNode, find_middle_element

node_1 = ListNode(1)
node_2 = ListNode(2)
node_3 = ListNode(3)
node_4 = ListNode(4)
node_5 = ListNode(5)
node_6 = ListNode(6)


@pytest.fixture(autouse=True)
def run_around_tests():
    """
    Initializes the nodes for the singly-linked list under test.

    :return: None
    """
    node_1.next = node_2
    node_2.next = node_3
    node_3.next = node_4
    node_4.next = node_5


def test_find_middle_element_even_list():
    """
    Tests the Find Middle Element solution using a singly-linked list that has an even number of
    nodes.

    :return: None
    """
    node_5.next = node_6

    assert find_middle_element(node_1) == 4


def test_find_middle_element_odd_list():
    """
    Tests the Find Middle Element solution using a singly-linked list that has an odd number of
    nodes.

    :return: None
    """
    node_5.next = None

    assert find_middle_element(node_1) == 3
