"""
Unit tests for Fare Estimator challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codesignal.fare_estimator import fare_estimator


@pytest.mark.parametrize("test_input1, test_input2, test_input3, test_input4, expected",
                         [(30, 7, [0.2, 0.35, 0.4, 0.45], [1.1, 1.8, 2.3, 3.5],
                           [13.7, 23.1, 28.1, 38.0])])
def test_fare_estimator(test_input1, test_input2, test_input3, test_input4, expected):
    """
    Tests the Fare Estimator solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param test_input4: Test input
    :param expected: Expected result
    :return: None
    """
    assert fare_estimator(test_input1, test_input2, test_input3, test_input4) == expected
