"""
Unit tests for Company Bot Strategy challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codesignal.company_bot_strategy import company_bot_strategy


@pytest.mark.parametrize("test_input, expected",
                         [([[3, 1], [6, 1], [4, 1], [5, 1]], 4.5),
                          ([[0, 0], [0, 0], [0, 0]], 0)])
def test_company_bot_strategy(test_input, expected):
    """
    Tests the Company Bot Strategy solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert company_bot_strategy(test_input) == expected
