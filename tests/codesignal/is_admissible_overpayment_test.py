"""
Unit tests for Is Admissible Overpayment challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codesignal.is_admissible_overpayment import is_admissible_overpayment


@pytest.mark.parametrize("test_input1, test_input2, test_input3, expected",
                         [([110, 95, 70],
                           ["10.0% higher than in-store", "5.0% lower than in-store",
                            "Same as in-store"], 5, True),
                          ([40, 40, 40, 40],
                           ["0.001% higher than in-store", "0.0% lower than in-store",
                            "0.0% higher than in-store", "0.0% lower than in-store"], 0, False),
                          ([35000, 35000], ["35000.0% higher than in-store",
                                            "10000.0% lower than in-store"], 150, False),
                          ([48, 165], ["20.0% lower than in-store", "10.0% higher than in-store",
                                       "Same as in-store"], 2, False)])
def test_is_admissible_overpayment(test_input1, test_input2, test_input3, expected):
    """
    Tests the Is Admissible Overpayment solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param test_input3: Test input
    :param expected: Expected result
    :return: None
    """
    assert is_admissible_overpayment(test_input1, test_input2, test_input3) == expected
