"""
Unit tests for Shopping Cart challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codesignal.shopping_cart import shopping_cart


@pytest.mark.parametrize("test_input, expected",
                         [(["add : milk",
                            "add : pickles",
                            "remove : milk",
                            "add : milk",
                            "quantity_upd : pickles : +4"], ["pickles : 5", "milk : 1"]),
                          (["add : milk",
                            "add : pickles",
                            "add : fruitz",
                            "add : vegetables",
                            "add : computer",
                            "add : whattheawesomeshopisit",
                            "quantity_upd : computer : +2",
                            "remove : computer",
                            "remove : milk",
                            "add : computer",
                            "quantity_upd : fruitz : +100",
                            "add : computer mouse",
                            "add : computer monitor",
                            "quantity_upd : computer mouse : +3",
                            "quantity_upd : computer mouse : +5",
                            "quantity_upd : computer : +3",
                            "quantity_upd : fruitz : -50",
                            "add : fruitz seed"], ["pickles : 1",
                                                   "fruitz : 51",
                                                   "vegetables : 1",
                                                   "whattheawesomeshopisit : 1",
                                                   "computer : 4",
                                                   "computer mouse : 9",
                                                   "computer monitor : 1",
                                                   "fruitz seed : 1"]),
                          (["add : milk",
                            "add : pickles",
                            "add : fruits",
                            "checkout",
                            "checkout",
                            "checkout",
                            "checkout"], []), (["add : milk",
                                                "add : pickles",
                                                "remove : milk",
                                                "checkout",
                                                "add : milk",
                                                "quantity_upd : milk : +3",
                                                "quantity_upd : milk : -2"], ["milk : 2"])])
def test_shopping_cart(test_input, expected):
    """
    Tests the Shopping Cart solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert shopping_cart(test_input) == expected
