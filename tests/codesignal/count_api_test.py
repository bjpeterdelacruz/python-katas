"""
Unit tests for Count API challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from codesignal.count_api import count_api


@pytest.mark.parametrize("test_input, expected",
                         [([
                               "/project1/subproject1/method1",
                               "/project2/subproject1/method1",
                               "/project1/subproject1/method1",
                               "/project1/subproject2/method1",
                               "/project1/subproject1/method2",
                               "/project1/subproject2/method1",
                               "/project2/subproject1/method1",
                               "/project1/subproject2/method1"
                           ], [
                               "--project1 (6)",
                               "----subproject1 (3)",
                               "------method1 (2)",
                               "------method2 (1)",
                               "----subproject2 (3)",
                               "------method1 (3)",
                               "--project2 (2)",
                               "----subproject1 (2)",
                               "------method1 (2)"
                           ])])
def test_count_api(test_input, expected):
    """
    Tests the Count API solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert count_api(test_input) == expected
