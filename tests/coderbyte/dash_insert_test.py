"""
Unit tests for Dash Insert challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.dash_insert import dash_insert


@pytest.mark.parametrize("test_input, expected",
                         [("99946", "9-9-946"),
                          ("56730", "567-30"),
                          ("567", "567")])
def test_dash_insert(test_input, expected):
    """
    Tests the Dash Insert solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert dash_insert(test_input) == expected
