"""
Unit tests for Permutation Step challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.permutation_step import permutation_step


@pytest.mark.parametrize("test_input, expected",
                         [(41352, 41523),
                          (11121, 11211)])
def test_permutation_step(test_input, expected):
    """
    Tests the Permutation Step solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert permutation_step(test_input) == expected
