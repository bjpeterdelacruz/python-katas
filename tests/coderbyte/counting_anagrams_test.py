"""
Unit tests for Counting Anagrams challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.counting_anagrams import counting_anagrams


@pytest.mark.parametrize("test_input, expected",
                         [("aa aa odg dog gdo", 2),
                          ("a c b c run urn urn", 1),
                          ("cars are very cool so are arcs and my os", 2)])
def test_counting_anagrams(test_input, expected):
    """
    Tests the Counting Anagrams solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert counting_anagrams(test_input) == expected
