"""
Unit tests for Binary Reversal challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.binary_reversal import binary_reversal


@pytest.mark.parametrize("test_input, expected",
                         [("213", 171),
                          ("47", 244)])
def test_binary_reversal(test_input, expected):
    """
    Tests the Binary Reversal solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert binary_reversal(test_input) == expected
