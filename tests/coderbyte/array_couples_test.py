"""
Unit tests for Array Couples challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.array_couples import array_couples


@pytest.mark.parametrize("test_input, expected",
                         [([2, 1, 1, 2, 3, 3], "3,3"),
                          ([6, 2, 2, 6, 5, 14, 14, 1], "5,14,14,1"),
                          ([1, 2, 3, 4, 5, 6], "1,2,3,4,5,6"),
                          ([1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 1], "yes"),
                          ([1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 10], "1,2,2,10"),
                          ([1, 2, 2, 4], "1,2,2,4"),
                          ([3, 3, 1, 2, 3, 3, 2, 1], "yes")])
def test_array_couples(test_input, expected):
    """
    Tests the Array Couples solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert array_couples(test_input) == expected


@pytest.mark.parametrize("test_input",
                         [([2, 1, 1, 2, 3])])
def test_array_couples_fail(test_input):
    """
    Tests whether the Array Couples solution raises an error given invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        array_couples(test_input)
        assert False
