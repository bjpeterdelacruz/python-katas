"""
Unit tests for Largest Four challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.largest_four import largest_four


@pytest.mark.parametrize("test_input, expected",
                         [([2, 3, 5], 10), ([1, 1, 1, -5], -2), ([0, 1, 2, 3, 7, 1], 13)])
def test_largest_four(test_input, expected):
    """
    Tests the Largest Four solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert largest_four(test_input) == expected
