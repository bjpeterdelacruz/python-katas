"""
Unit tests for Second GreatLow challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.second_greatlow import second_greatlow


@pytest.mark.parametrize("test_input, expected",
                         [([1, 42, 42, 100], "42 42"),
                          ([4, 90], "90 4"),
                          ([8], "8 8"),
                          ([7, 7, 12, 98, 106], "12 98")])
def test_second_greatlow(test_input, expected):
    """
    Tests the Second GreatLow solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert second_greatlow(test_input) == expected
