"""
Unit tests for Matching Couples challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.matching_couples import matching_couples


@pytest.mark.parametrize("test_input, expected",
                         [([10, 5, 4], 900),
                          ([5, 5, 4], 200),
                          ([2, 2, 2], 4),
                          ([5, 3, 2], 15),
                          ([5, 5, 2], 25),
                          ([6, 6, 4], 450),
                          ([10, 10, 4], 4050),
                          ([10, 10, 6], 86400),
                          ([15, 10, 6], 327600)])
def test_matching_couples(test_input, expected):
    """
    Tests the Matching Couples solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert matching_couples(test_input) == expected


@pytest.mark.parametrize("invalid_input",
                         [([5, 5, 10]),
                         ([10, 5, 3]),
                         ([0, 2, 2]),
                         ([2, 0, 2])])
def test_matching_couples_fail(invalid_input):
    """
    Tests whether the Matching Couples solution raises an error given invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        matching_couples(invalid_input)
        assert False
