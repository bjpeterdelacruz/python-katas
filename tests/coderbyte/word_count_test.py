"""
Unit tests for Word Count challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.word_count import word_count


@pytest.mark.parametrize("test_input, expected",
                         [("hello world", 2),
                          ("thisisatest", 1),
                          ("this is  a   test", 4)])
def test_word_count(test_input, expected):
    """
    Tests the Word Count solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert word_count(test_input) == expected
