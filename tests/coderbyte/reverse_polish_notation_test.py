"""
Unit tests for Reverse Polish Notation challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.reverse_polish_notation import reverse_polish_notation


@pytest.mark.parametrize("test_input, expected",
                         [("1 2 + 3 * 1 +", 10),
                          ("2 2 * 1 / 10 +", 14)])
def test_reverse_polish_notation(test_input, expected):
    """
    Tests the Reverse Polish Notation solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert reverse_polish_notation(test_input) == expected


@pytest.mark.parametrize("test_input",
                         ["2 2 * 0 / 10 +"])
def test_simple_mode_fail(test_input):
    """
    Tests whether the Reverse Polish Notation solution raises an error given invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        reverse_polish_notation(test_input)
        assert False
