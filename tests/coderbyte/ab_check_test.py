"""
Unit tests for AB Check challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.ab_check import ab_check


@pytest.mark.parametrize("test_input, expected",
                         [("lane borrowed", "true"),
                          ("after badly", "false"),
                          ("Laura sobs", "true"),
                          ("bzzza", "true")])
def test_ab_check(test_input, expected):
    """
    Tests the AB Check solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert ab_check(test_input) == expected
