"""
Unit tests for Three Sum challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.three_sum import three_sum


@pytest.mark.parametrize("test_input, expected",
                         [([8, 2, 1, 4, 10, 5, -1, -1], "true"),
                          ([7, 1, 2, 3], "false")])
def test_three_sum(test_input, expected):
    """
    Tests the Three Sum solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert three_sum(test_input) == expected
