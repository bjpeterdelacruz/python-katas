"""
Unit tests for Missing Digit II challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.missing_digit_ii import missing_digit_ii


@pytest.mark.parametrize("test_input, expected",
                         [("56? * 106 = 5?678", "3 9"),
                          ("18?1 + 9 = 189?", "8 0")])
def test_midding_digit_ii(test_input, expected):
    """
    Tests the Missing Digit II solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert missing_digit_ii(test_input) == expected
