"""
Unit tests for Wildcard Characters challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.wildcard_characters import wildcard_characters


@pytest.mark.parametrize("test_input, expected",
                         [("+ a", "true"),
                          ("* ttt", "true"),
                          ("****{4}+++ fffbbbeeerrrrmko", "true"),
                          ("*{1}*{1} rt", "true"),
                          ("+ abc", "false"),
                          ("*{4} abbb", "false")])
def test_wildcard_characters(test_input, expected):
    """
    Tests the Wildcard Characters solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert wildcard_characters(test_input) == expected
