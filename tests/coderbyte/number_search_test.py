"""
Unit tests for Number Search challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.number_search import number_search


@pytest.mark.parametrize("test_input, expected",
                         [("3ko6", 5),
                          ("Hello6 9World 2, Nic8e D7ay!", 2)])
def test_number_search(test_input, expected):
    """
    Tests the Number Search solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert number_search(test_input) == expected
