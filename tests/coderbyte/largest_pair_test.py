"""
Unit tests for Largest Pair challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.largest_pair import largest_pair


@pytest.mark.parametrize("test_input, expected",
                         [(453857, 85), (100220, 22)])
def test_largest_pair(test_input, expected):
    """
    Tests the Largest Pair solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert largest_pair(test_input) == expected
