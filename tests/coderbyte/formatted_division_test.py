"""
Unit tests for Formatted Division challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.formatted_division import formatted_division


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(2, 3, "0.6667"), (123456789, 10000, "12,345.6789")])
def test_formatted_division(test_input1, test_input2, expected):
    """
    Tests the Formatted Division solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert formatted_division(test_input1, test_input2) == expected
