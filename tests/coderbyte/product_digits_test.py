"""
Unit tests for Product Digits challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.product_digits import product_digits


@pytest.mark.parametrize("test_input, expected",
                         [(90, 3), (6, 2), (23, 3)])
def test_product_digits(test_input, expected):
    """
    Tests the Product Digits solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert product_digits(test_input) == expected
