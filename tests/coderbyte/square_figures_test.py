"""
Unit tests for Square Figures challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.square_figures import square_figures


@pytest.mark.parametrize("test_input, expected",
                         [(6, 317), (1, 0), (2, 4)])
def test_square_figures(test_input, expected):
    """
    Tests the Square Figures solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert square_figures(test_input) == expected
