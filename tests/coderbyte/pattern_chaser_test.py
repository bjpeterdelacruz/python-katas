"""
Unit tests for Pattern Chaser challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.pattern_chaser import pattern_chaser


@pytest.mark.parametrize("test_input, expected",
                         [("sskfssbbb9bbb", "yes bbb"),
                          ("aa2bbbaacbbb", "yes bbb"),
                          ("123224", "no null"),
                          ("458933896893", "yes 893"),
                          ("aaaakkdnrjsnur998aaaaks", "yes aaaak")])
def test_pattern_chaser(test_input, expected):
    """
    Tests the Pattern Chaser solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert pattern_chaser(test_input) == expected
