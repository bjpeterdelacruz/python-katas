"""
Unit tests for Power Set Count challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.power_set_count import power_set_count


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 3, 4], 16), ([1, 2, 3], 8)])
def test_largest_pair(test_input, expected):
    """
    Tests the Power Set Count solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert power_set_count(test_input) == expected
