"""
Unit tests for Bitwise Two challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.bitwise_two import bitwise_two


@pytest.mark.parametrize("test_input, expected",
                         [(["101", "000"], "000"),
                          (["10110", "11111"], "10110"),
                          (["100", "100"], "100")])
def test_bitwise_two(test_input, expected):
    """
    Tests the Bitwise Two solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert bitwise_two(test_input) == expected


@pytest.mark.parametrize("test_input",
                         [(["00", "101"]),
                          (["101", "00"])])
def test_bitwise_two_fail(test_input):
    """
    Tests whether the Bitwise Two solution raises an error given invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        bitwise_two(test_input)
        assert False
