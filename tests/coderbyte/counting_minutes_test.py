"""
Unit tests for Counting Minutes challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.counting_minutes import counting_minutes


@pytest.mark.parametrize("test_input, expected",
                         [("1:00pm-11:00am", 1320),
                          ("1:00am-11:00am", 600),
                          ("1:30pm-10:20am", 1250),
                          ("1:10pm-10:20am", 1270),
                          ("1:23am-1:08am", 1425)])
def test_counting_minutes(test_input, expected):
    """
    Tests the Counting Minutes solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert counting_minutes(test_input) == expected
