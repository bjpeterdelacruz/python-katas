"""
Unit tests for Distinct Characters challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.distinct_characters import distinct_characters


@pytest.mark.parametrize("test_input, expected",
                         [("abc123kkmmmm?", "false"),
                          ("12334bbmma:=6", "true"),
                          ("eeeemmmmmmmmm1000", "false")])
def test_distinct_characters(test_input, expected):
    """
    Tests the Distinct Characters solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert distinct_characters(test_input) == expected
