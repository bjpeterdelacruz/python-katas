"""
Unit tests for Binary Converter challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.binary_converter import binary_converter


@pytest.mark.parametrize("test_input, expected",
                         [("100101", 37),
                          ("011", 3)])
def test_binary_converter(test_input, expected):
    """
    Tests the Binary Converter solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert binary_converter(test_input) == expected
