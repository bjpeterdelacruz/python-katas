"""
Unit tests for String Reduction challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.string_reduction import string_reduction


@pytest.mark.parametrize("test_input, expected",
                         [("abcabc", 2),
                          ("cccc", 4)])
def test_string_reduction(test_input, expected):
    """
    Tests the String Reduction solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert string_reduction(test_input) == expected
