"""
Unit tests for Closest Enemy challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.closest_enemy import closest_enemy


@pytest.mark.parametrize("test_input, expected",
                         [([1, 0, 0, 0, 2, 2, 2], 4),
                          ([2, 0, 0, 0, 2, 2, 1, 0], 1),
                          ([0, 0, 1, 0, 0], 0)])
def test_closest_enemy(test_input, expected):
    """
    Tests the Closest Enemy solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert closest_enemy(test_input) == expected


def test_closest_enemy_fail():
    """
    Tests whether the Closest Enemy solution raises an error given invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        closest_enemy([2, 0, 0, 0, 2])
        assert False
