"""
Unit tests for Caesar Cipher challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.caesar_cipher import caesar_cipher


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("ABC", 0, "ABC"),
                          ("ABC , xyz , XYZ , abc", 2, "CDE , zab , ZAB , cde"),
                          ("I love you!", 3, "L oryh brx!"),
                          ("*** You are AWESOME, dude! ***", 10, "*** Iye kbo KGOCYWO, neno! ***")])
def test_caesar_cipher(test_input1, test_input2, expected):
    """
    Tests the Caesar Cipher solution.

    :param test_input1: Test input 1, a string
    :param test_input2: Test input 2, an integer
    :param expected: Expected result
    :return: None
    """
    assert caesar_cipher(test_input1, test_input2) == expected
