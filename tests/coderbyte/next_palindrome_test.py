"""
Unit tests for Next Palindrome challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.next_palindrome import next_palindrome


@pytest.mark.parametrize("test_input, expected",
                         [(100, 101), (2, 3), (12, 22)])
def test_next_palindrome(test_input, expected):
    """
    Tests the Next Palindrome solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert next_palindrome(test_input) == expected
