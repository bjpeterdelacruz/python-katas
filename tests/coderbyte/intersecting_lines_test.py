"""
Unit tests for Intersecting Lines challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.intersecting_lines import intersecting_lines


@pytest.mark.parametrize("test_input, expected",
                         [(["(9, -2)", "(-2, 9)", "(3, 4)", "(10, 11)"], "(3,4)"),
                          (["(1, 2)", "(3, 4)", "(-5, -6)", "(-7,- 8)"], "no intersection"),
                          (["(4, 3)", "(2, 1)", "(-7, -6)", "(-4, -1)"], "(-10,-11)"),
                          (["(3, 0)", "(1, 4)", "(0, -3)", "(2, 3)"], "(9/5,12/5)"),
                          (["(0, 15)", "(3, -12)", "(2, 1)", "(13, 7)"], "(166/105,27/35)"),
                          (["(10, 11)", "(12, 16)", "(6, 7)", "(18, -4)"], "(318/41,221/41)"),
                          (["(0, 0)", "(0, -1)", "(1, 1)", "(0, 1)"], "(0,1)"),
                          (["(1, -1)", "(1, 1)", "(-5, -5)", "(-7, -7)"], "(1,1)"),
                          (["(1, 1)", "(0, 1)", "(0, 0)", "(0, -1)"], "(0,1)")])
def test_intersecting_lines(test_input, expected):
    """
    Tests the Intersecting Lines solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert intersecting_lines(test_input) == expected
