"""
Unit tests for Alphabet Searching challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.alphabet_searching import alphabet_searching


@pytest.mark.parametrize("test_input, expected",
                         [("abcdefghijklmnopqrstuvwxyz", "true"),
                          ("abcdefghijklmnopqrstuvwxyyyy", "false"),
                          ("abc123456kmo", "false")])
def test_alphabet_searching(test_input, expected):
    """
    Tests the Alphabet Searching solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert alphabet_searching(test_input) == expected
