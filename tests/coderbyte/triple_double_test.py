"""
Unit tests for Triple Double challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.triple_double import triple_double


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [(465555, 5579, 1),
                          (67844, 66237, 0)])
def test_triple_double(test_input1, test_input2, expected):
    """
    Tests the Triple Double solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert triple_double(test_input1, test_input2) == expected
