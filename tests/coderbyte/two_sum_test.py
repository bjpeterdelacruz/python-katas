"""
Unit tests for Two Sum challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.two_sum import two_sum


@pytest.mark.parametrize("test_input, expected",
                         [([17, 4, 5, 6, 10, 11, 4, -3, -5, 3, 15, 2, 7], "6,11 10,7 15,2"),
                          ([10, 2, 5, 7], "-1")])
def test_two_sum(test_input, expected):
    """
    Tests the Two Sum solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert two_sum(test_input) == expected
