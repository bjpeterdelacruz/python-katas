"""
Unit tests for Dash Insert II challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.dash_insert_ii import dash_insert_ii


@pytest.mark.parametrize("test_input, expected",
                         [(99946, "9-9-94*6"),
                          (56647304, "56*6*47-304")])
def test_dash_insert_ii(test_input, expected):
    """
    Tests the Dash Insert II solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert dash_insert_ii(test_input) == expected
