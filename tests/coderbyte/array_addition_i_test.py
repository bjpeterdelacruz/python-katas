"""
Unit tests for Array Addition I challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.array_addition_i import array_addition_i


@pytest.mark.parametrize("test_input, expected",
                         [([5, 7, 16, 1, 2], "false"),
                          ([3, 5, -1, 8, 15], "true")])
def test_array_addition_i(test_input, expected):
    """
    Tests the Array Addition I solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert array_addition_i(test_input) == expected
