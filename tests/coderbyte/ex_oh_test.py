"""
Unit tests for Ex Oh challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.ex_oh import ex_oh


@pytest.mark.parametrize("test_input, expected",
                         [("xooxxo", "true"),
                          ("x", "false")])
def test_ex_oh(test_input, expected):
    """
    Tests the Ex Oh solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert ex_oh(test_input) == expected
