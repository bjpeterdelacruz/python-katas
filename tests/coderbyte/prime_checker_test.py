"""
Unit tests for Prime Checker challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.prime_checker import prime_checker


@pytest.mark.parametrize("test_input, expected",
                         [(11, 1),
                          (910, 1)])
def test_prime_checker(test_input, expected):
    """
    Tests the Prime Checker solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert prime_checker(test_input) == expected
