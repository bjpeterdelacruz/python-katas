"""
Unit tests for Swap Case challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.swap_case import swap_case


@pytest.mark.parametrize("test_input, expected",
                         [("hElLo wOrLd", "HeLlO WoRlD"),
                          (" 123abcDEF456 ", " 123ABCdef456 ")])
def test_swap_case(test_input, expected):
    """
    Tests the Swap Case solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert swap_case(test_input) == expected
