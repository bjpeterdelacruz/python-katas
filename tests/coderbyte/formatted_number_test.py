"""
Unit tests for Formatted Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.formatted_number import formatted_number


@pytest.mark.parametrize("test_input, expected",
                         [(["1,234,56.789"], "false"),
                          ([".123"], "false"),
                          (["1,234,567.89"], "true"),
                          (["1,234.567.89"], "false"),
                          (["1,234,567.898,76"], "false"),
                          (["0.123"], "true"),
                          (["123abc"], "false"),
                          (["1234,567"], "false")])
def test_formatted_number(test_input, expected):
    """
    Tests the Formatted Number solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert formatted_number(test_input) == expected
