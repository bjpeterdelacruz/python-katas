"""
Unit tests for Find Intersection challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.find_intersection import find_intersection


@pytest.mark.parametrize("test_input, expected",
                         [(["1, 3, 4, 7, 13", "1, 2, 4, 13, 15"], "1,4,13"),
                          (["1, 3, 9, 10, 17", "1, 4, 9, 10"], "1,9,10"),
                          (["1, 3, 9, 10, 17", "2, 4, 6, 8"], "false")])
def test_find_intersection(test_input, expected):
    """
    Tests the Find Intersection solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert find_intersection(test_input) == expected
