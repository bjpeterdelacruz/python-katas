"""
Unit tests for Longest Word challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.longest_word import longest_word


@pytest.mark.parametrize("test_input, expected",
                         [("fun&!! time", "time"),
                          ("I\tlove\tdogs!", "love"),
                          ("I   wrote\ta   Hello\tWorld   app.", "wrote"),
                          ("!\tI   wrote\ta   Hello\tWorld   script\tin   Python.", "script"),
                          ("123456789 98765432", "123456789")])
def test_longest_word(test_input, expected):
    """
    Tests the Longest Word solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert longest_word(test_input) == expected
