"""
Unit tests for Vowel Count challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.vowel_count import vowel_count


@pytest.mark.parametrize("test_input, expected",
                         [("hello", 2), ("coderbyte", 3)])
def test_vowel_count(test_input, expected):
    """
    Tests the Vowel Count solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert vowel_count(test_input) == expected
