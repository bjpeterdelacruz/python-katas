"""
Unit tests for Serial Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.serial_number import serial_number


@pytest.mark.parametrize("test_input, expected",
                         [("123.456", "false"), ("4.1.1", "false"), ("10.1.1", "false"),
                          ("11.13.14", "false"), ("11.124.667", "false"),
                          ("114.568.112", "true")])
def test_serial_number(test_input, expected):
    """
    Tests the Dash Insert solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert serial_number(test_input) == expected
