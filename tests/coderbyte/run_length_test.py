"""
Unit tests for Run Length challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.run_length import run_length


@pytest.mark.parametrize("test_input, expected",
                         [("aabbcde", "2a2b1c1d1e"),
                          ("wwwbbbw", "3w3b1w")])
def test_run_length(test_input, expected):
    """
    Tests the Run Length solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert run_length(test_input) == expected
