"""
Unit tests for Pentagonal Number challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.pentagonal_number import pentagonal_number


@pytest.mark.parametrize("test_input, expected",
                         [(1, 1),
                          (2, 6),
                          (5, 51)])
def test_pentagonal_number(test_input, expected):
    """
    Tests the Pentagonal Number solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert pentagonal_number(test_input) == expected
