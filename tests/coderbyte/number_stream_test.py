"""
Unit tests for Number Stream challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.number_stream import number_stream


@pytest.mark.parametrize("test_input, expected",
                         [("6539923335", "true"),
                          ("5556293383563665", "false"),
                          ("5788888888882339999", "true")])
def test_number_stream(test_input, expected):
    """
    Tests the Number Stream solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert number_stream(test_input) == expected
