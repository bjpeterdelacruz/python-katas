"""
Unit tests for Noughts Determiner challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.noughts_determiner import noughts_determiner


@pytest.mark.parametrize("test_input, expected",
                         [(["X", "-", "-", "<>", "-", "O", "-", "<>", "O", "X", "-"], 2),
                          (["X", "-", "-", "<>", "X", "O", "-", "<>", "-", "X", "-"], 8),
                          (["X", "O", "-", "<>", "-", "X", "-", "<>", "O", "-", "-"], 10),
                          (["X", "X", "-", "<>", "-", "O", "-", "<>", "-", "O", "-"], 2),
                          (["X", "O", "-", "<>", "-", "-", "-", "<>", "O", "-", "X"], 5)])
def test_noughts_determiner(test_input, expected):
    """
    Tests the Noughts Determiner solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert noughts_determiner(test_input) == expected
