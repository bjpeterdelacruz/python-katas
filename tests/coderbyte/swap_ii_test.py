"""
Unit tests for Swap II challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.swap_ii import swap_ii


@pytest.mark.parametrize("test_input, expected",
                         [("Hello -5lol6", "hELLO -6LOL5"),
                          ("25 6 du54de", "25 6 DU54DE")])
def test_swap_ii(test_input, expected):
    """
    Tests the Swap II solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert swap_ii(test_input) == expected
