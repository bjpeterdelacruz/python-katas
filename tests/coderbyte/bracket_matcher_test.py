"""
Unit tests for Bracket Matcher challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.bracket_matcher import bracket_matcher


@pytest.mark.parametrize("test_input, expected",
                         [("(coder)(byte))", 0),
                          ("(c(oder)) b(yte)", 1),
                          ("dogs and cats", 1),
                          ("the color re(d))()(()", 0)])
def test_bracket_matcher(test_input, expected):
    """
    Tests the Bracket Matcher solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert bracket_matcher(test_input) == expected
