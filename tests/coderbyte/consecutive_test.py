"""
Unit tests for Consecutive challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.consecutive import consecutive


@pytest.mark.parametrize("test_input, expected",
                         [([5, 10, 15], 8),
                          ([-2, 10, 4], 10)])
def test_consecutive(test_input, expected):
    """
    Tests the Consecutive solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert consecutive(test_input) == expected


@pytest.mark.parametrize("test_input",
                         [([5])])
def test_consecutive_fail(test_input):
    """
    Tests whether the Consecutive solution raises an error given invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        consecutive(test_input)
        assert False
