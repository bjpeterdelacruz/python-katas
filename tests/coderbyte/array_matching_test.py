"""
Unit tests for Array Matching challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.array_matching import array_matching


@pytest.mark.parametrize("test_input, expected",
                         [(["[2, 4, 6, 8]", "[1, 3]"], "3-7-6-8"),
                          (["[1, 3, 9, 27]", "[2, 4, 10, 28]"], "3-7-19-55"),
                          (["[-1, 1]", "[3]"], "2-1")])
def test_array_matching(test_input, expected):
    """
    Tests the Array Matching solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert array_matching(test_input) == expected
