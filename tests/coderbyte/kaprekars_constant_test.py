"""
Unit tests for Kaprekar's Constant challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.kaprekars_constant import kaprekars_constant


@pytest.mark.parametrize("test_input, expected",
                         [(2111, 5), (3524, 3), (9831, 7)])
def test_kaprekars_constant(test_input, expected):
    """
    Tests the Kaprekar's Constant solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert kaprekars_constant(test_input) == expected
