"""
Unit tests for Non-repeating Character challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.nonrepeating_character import nonrepeating_character


@pytest.mark.parametrize("test_input, expected",
                         [("agettkgaeee", "k"),
                          ("abcdef", "a"),
                          ("hello world hi hey", "w")])
def test_nonrepeating_character(test_input, expected):
    """
    Tests the Non-repeating Character solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert nonrepeating_character(test_input) == expected


def test_nonrepeating_character_fail():
    """
    Tests whether the Non-repeating Character solution raises an error given invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        nonrepeating_character("")
        assert False
