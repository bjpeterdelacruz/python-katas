"""
Unit tests for Alphabet Soup challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.alphabet_soup import alphabet_soup


@pytest.mark.parametrize("test_input, expected",
                         [("coderbyte", "bcdeeorty")])
def test_alphabet_soup(test_input, expected):
    """
    Tests the Alphabet Soup solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert alphabet_soup(test_input) == expected
