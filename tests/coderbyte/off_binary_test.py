"""
Unit tests for Off Binary challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.off_binary import off_binary


@pytest.mark.parametrize("test_input, expected",
                         [(["5624", "0010111111001"], 2),
                          (["44", "111111"], 3)])
def test_off_binary(test_input, expected):
    """
    Tests the Off Binary solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert off_binary(test_input) == expected
