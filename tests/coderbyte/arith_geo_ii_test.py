"""
Unit tests for Arith Geo II challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.arith_geo_ii import arith_geo_ii


@pytest.mark.parametrize("test_input, expected",
                         [([2, 4, 6, 8], "Arithmetic"),
                          ([1, 3, 9, 27], "Geometric"),
                          ([-1, 1, 3, 4, 10], "-1")])
def test_arith_geo_ii(test_input, expected):
    """
    Tests the Arith Geo II solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert arith_geo_ii(test_input) == expected
