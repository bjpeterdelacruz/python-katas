"""
Unit tests for Even Pairs challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.even_pairs import even_pairs


@pytest.mark.parametrize("test_input, expected",
                         [("3gy41d216", "true"),
                          ("f09r27i8e67", "false")])
def test_even_pairs(test_input, expected):
    """
    Tests the Even Pairs solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert even_pairs(test_input) == expected
