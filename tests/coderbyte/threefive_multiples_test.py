"""
Unit tests for ThreeFive Multiples challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.threefive_multiples import threefive_multiples


@pytest.mark.parametrize("test_input, expected",
                         [(10, 23), (1, 0), (31, 225), (17, 60)])
def test_threefive_multiples(test_input, expected):
    """
    Tests the ThreeFive Multiples solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert threefive_multiples(test_input) == expected
