"""
Unit tests for HTML Elements challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.html_elements import html_elements


@pytest.mark.parametrize("test_input, expected",
                         [("<div><div><b></b></div></p>", "div"),
                          ("<div>abc</div><p><em><i>test test test</b></em></p>", "i"),
                          ("<p><p><em></em><p></p><i></p>", "i"),
                          ("<div>", "div")])
def test_html_elements(test_input, expected):
    """
    Tests the HTML Elements solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert html_elements(test_input) == expected
