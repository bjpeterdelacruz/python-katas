"""
Unit tests for Blackjack Highest challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.blackjack_highest import blackjack_highest


@pytest.mark.parametrize("test_input, expected",
                         [(["four", "four", "four", "four", "four", "ace"], "blackjack four"),
                          (["two", "three", "four", "five", "six", "ace"], "blackjack six"),
                          (["queen", "ace", "ten"], "blackjack queen"),
                          (["four", "ace", "ten"], "below ten"),
                          (["ace", "queen"], "blackjack ace"),
                          (["four", "king", "ten"], "above king")])
def test_blackjack_highest(test_input, expected):
    """
    Tests the Blackjack Highest solutions.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert blackjack_highest(test_input) == expected
