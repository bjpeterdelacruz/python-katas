"""
Unit tests for Simple Mode challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.simple_mode import simple_mode


@pytest.mark.parametrize("test_input, expected",
                         [([5, 5, 2, 2, 1], 5),
                          ([5, 10, 10, 6, 5], 5),
                          ([3, 4, 1, 6, 10], -1)])
def test_simple_mode(test_input, expected):
    """
    Tests the Simple Mode solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert simple_mode(test_input) == expected


@pytest.mark.parametrize("test_input",
                         [([])])
def test_simple_mode_fail(test_input):
    """
    Tests whether the Simple Mode solution raises an error given invalid input.

    :return: None
    """
    with pytest.raises(ValueError):
        simple_mode(test_input)
        assert False
