"""
Unit tests for Overlapping Ranges challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.overlapping_ranges import overlapping_ranges


@pytest.mark.parametrize("test_input, expected",
                         [([5, 11, 1, 5, 1], "true"),
                          ([10, 15, 16, 20, 3], "false")])
def test_overlapping_ranges(test_input, expected):
    """
    Tests the Overlapping Ranges solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert overlapping_ranges(test_input) == expected
