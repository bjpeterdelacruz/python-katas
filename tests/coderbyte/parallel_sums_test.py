"""
Unit tests for Parallel Sums challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.parallel_sums import parallel_sums


@pytest.mark.parametrize("test_input, expected",
                         [([16, 22, 35, 8, 20, 1, 21, 11], "1,11,20,35,8,16,21,22"),
                          ([1, 2, 3, 4], "1,4,2,3"),
                          ([1, 1, 1, 1], "1,1,1,1"),
                          ([1, 2, 3, 4, 5, 6], "-1"),
                          ([2, 3, 1, 9, 3, 4, 4, 4], "1,2,3,9,3,4,4,4")])
def test_parallel_sums(test_input, expected):
    """
    Tests the Parallel Sums solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert parallel_sums(test_input) == expected
