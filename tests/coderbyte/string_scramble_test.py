"""
Unit tests for String Scramble challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.string_scramble import string_scramble


@pytest.mark.parametrize("test_input1, test_input2, expected",
                         [("cdore", "coder", "true"),
                          ("h3llko", "hello", "false"),
                          ("coodrebtqqkye", "coderbyte", "true"),
                          ("heloooolwrdlla", "helloworld", "true")])
def test_string_scramble(test_input1, test_input2, expected):
    """
    Tests the String Scramble solution.

    :param test_input1: Test input
    :param test_input2: Test input
    :param expected: Expected result
    :return: None
    """
    assert string_scramble(test_input1, test_input2) == expected
