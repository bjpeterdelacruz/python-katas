"""
Unit tests for Tree Constructor challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.tree_constructor import tree_constructor


@pytest.mark.parametrize("test_input, expected",
                         [(["(1,2)", "(2,4)", "(5,7)", "(7,2)", "(9,5)"], "true"),
                          (["(1,2)", "(3,2)", "(2,12)", "(5,2)"], "false"),
                          (["(1,2)", "(2,4)", "(3,2)", "(5,4)", "(4,6)"], "true"),
                          (["(1,2)", "(2,4)", "(3,2)", "(5,6)"], "false"),
                          (["(1,2)"], "true"),
                          (["(2,3)", "(1,2)", "(4,9)", "(9,3)", "(12,9)", "(6,4)"], "true")])
def test_tree_constructor(test_input, expected):
    """
    Tests the Tree Constructor solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert tree_constructor(test_input) == expected


@pytest.mark.parametrize("invalid_input",
                         [([]), ([""])])
def test_tree_constructor_fail(invalid_input):
    """
    Tests whether the Tree Constructor solution raises an error given invalid input.

    :return: None.
    """
    with pytest.raises(ValueError):
        tree_constructor(invalid_input)
        assert False
