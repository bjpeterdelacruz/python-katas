"""
Unit tests for Third Greatest challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.third_greatest import third_greatest


@pytest.mark.parametrize("test_input, expected",
                         [(["hello", "world", "before", "all"], "world"),
                          (["hello", "world", "after", "all"], "after")])
def test_third_greatest(test_input, expected):
    """
    Tests the Third Greatest solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert third_greatest(test_input) == expected
