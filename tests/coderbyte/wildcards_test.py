"""
Unit tests for Wildcards challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.wildcards import wildcards


@pytest.mark.parametrize("test_input, expected",
                         [("+$+ a7b", "true"),
                          ("**$ tttsss0", "true"),
                          ("****{4}+++ fffbbbeeerrrrmko", "true"),
                          ("*{1}*{1} rt", "true"),
                          ("+ abc", "false"),
                          ("*{4} abbb", "false"),
                          ("$**+*{2} 9mmmrrrkbb", "true"),
                          ("$**+*{2}$ 9mmmrrrkbbz", "false")])
def test_wildcards(test_input, expected):
    """
    Tests the Wildcards solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert wildcards(test_input) == expected
