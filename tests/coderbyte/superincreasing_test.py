"""
Unit tests for Superincreasing challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.superincreasing import superincreasing


@pytest.mark.parametrize("test_input, expected",
                         [([1, 2, 3, 4], "false"),
                          ([1, 2, 5, 10], "true")])
def test_superincreasing(test_input, expected):
    """
    Tests the Superincreasing solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert superincreasing(test_input) == expected
