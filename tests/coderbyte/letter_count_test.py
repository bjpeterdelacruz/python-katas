"""
Unit tests for Letter Count challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.letter_count import letter_count


@pytest.mark.parametrize("test_input, expected",
                         [("Hello apple pie", "Hello"),
                          ("No words", "-1")])
def test_letter_count(test_input, expected):
    """
    Tests the Letter Count solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert letter_count(test_input) == expected
