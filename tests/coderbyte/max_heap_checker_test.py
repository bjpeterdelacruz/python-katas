"""
Unit tests for Max Heap Checker challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.max_heap_checker import max_heap_checker


@pytest.mark.parametrize("test_input, expected",
                         [([73, 74, 75, 7, 2, 107], "74,75,107"),
                          ([1, 5, 10, 2, 3, 10, 1], "5,10"),
                          ([100, 99, 98, 4, 3, 2, 1], "max")])
def test_max_heap_checker(test_input, expected):
    """
    Tests the Max Heap Checker solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert max_heap_checker(test_input) == expected
