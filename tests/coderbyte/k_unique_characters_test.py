"""
Unit tests for K Unique Characters challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.k_unique_characters import k_unique_characters


@pytest.mark.parametrize("test_input, expected",
                         [("3aabacbebebe", "cbebebe"),
                          ("2aabbaaccbbaaccaabb", "aabbaa"),
                          ("1aabb", "aa"),
                          ("4aaffaacccerrfffaacca", "aaffaaccce")])
def test_k_unique_characters(test_input, expected):
    """
    Tests the K Unique Characters solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert k_unique_characters(test_input) == expected
