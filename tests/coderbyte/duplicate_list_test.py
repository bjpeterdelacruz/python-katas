"""
Unit tests for Duplicate List challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.duplicate_list import duplicate_list


@pytest.mark.parametrize("test_input, expected",
                         [([0, -2, -2, 5, 5, 5], 3),
                          ([100, 2, 101, 4], 0)])
def test_duplicate_list(test_input, expected):
    """
    Tests the Duplicate List solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert duplicate_list(test_input) == expected
