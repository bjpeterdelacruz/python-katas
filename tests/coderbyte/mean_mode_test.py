"""
Unit tests for Mean Mode challenge

:Author: BJ Peter Dela Cruz
"""
import pytest
from coderbyte.mean_mode import mean_mode


@pytest.mark.parametrize("test_input, expected",
                         [([4, 4, 4, 6, 2], 1),
                          ([1, 2, 3], 0),
                          ([1, 1, 1], 1)])
def test_mean_mode(test_input, expected):
    """
    Tests the Mean Mode solution.

    :param test_input: Test input
    :param expected: Expected result
    :return: None
    """
    assert mean_mode(test_input) == expected
