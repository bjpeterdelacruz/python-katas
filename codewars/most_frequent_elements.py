"""
Most Frequent Elements

:Author: BJ Peter Dela Cruz
"""
from collections import Counter


def find_most_frequent(lst):
    """
    This function returns a set containing one or more elements that appear most frequently in the
    given list. If the list is empty, an empty set is returned.

    :param lst: The input list
    :return: A set containing one or more elements that appear most frequently in :attr:`lst`
    """
    if not lst:
        return set()
    sorted_counts = sorted(Counter(lst).items(), key=lambda item: item[1])
    maximum = sorted_counts[-1][1]
    return set(map(lambda item: item[0], filter(lambda item: item[1] == maximum, sorted_counts)))
