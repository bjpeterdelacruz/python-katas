"""
Thinkful: Quarks

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class Quark:
    """
    This class represents a quark, which is a type of elementary particle and a fundamental
    constituent of matter.
    """
    def __init__(self, color: str, flavor: str):
        """
        Initializes this quark with the given color and flavor. Valid colors are red, blue, or
        green, and valid flavors are up, down, strange, charm, top, and bottom. If an invalid value
        is passed for either color or flavor, a ValueError is raised.

        :param color: The color for this quark
        :param flavor: The flavor or type of this quark
        """
        if color not in ["red", "blue", "green"]:
            raise ValueError
        if flavor not in ["up", "down", "strange", "charm", "top", "bottom"]:
            raise ValueError
        self.color = color
        self.flavor = flavor
        self.baryon_number = 1.0 / 3

    def interact(self, other_quark) -> None:
        """
        Switches the color of this quark with the other quark when both quarks interact with each
        other.

        :param other_quark: The other quark
        :return: None
        """
        self.color, other_quark.color = other_quark.color, self.color
