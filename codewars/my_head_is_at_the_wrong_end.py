"""
My Head is at the Wrong End

:Author: BJ Peter Dela Cruz
"""


def fix_the_meerkat(arr: list[str]) -> list[str]:
    """
    This function puts the last string in :attr:`arr` at the front of the list and the first
    string in :attr:`arr` at the end of the list.

    :param arr: The input list of strings
    :return: The same list
    """
    arr[0], arr[-1] = arr[-1], arr[0]
    return arr
