"""
Rotate for a Max

Author: BJ Peter Dela Cruz
"""
from collections import deque


def max_rot(number: int) -> int:
    """
    This function rotates a digit starting from the far left to the left one digit all the way until
    the last digit on the far right is reached. For example, given "1234", this function will
    perform the following rotations: "2341", "2413", and "2431". Finally, the maximum out of all
    the numbers (which include the original number--in the example, "1234") is returned (in the
    example, "2431" is the maximum).

    :param number: The input integer
    :return: The maximum that is produced after rotating each digit
    """
    num_deque = deque(str(number))
    numbers = [str(number)]
    prefix = ''
    idx = 0
    for _ in range(0, len(str(number)) - 1):
        num_deque.rotate(-1)
        numbers.append(f"{prefix}{''.join(num_deque)}")
        num_deque = deque(list(num_deque)[1:])
        idx += 1
        prefix = str(numbers[-1])[0:idx]
    return max(map(int, numbers))
