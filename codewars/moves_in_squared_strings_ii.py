"""
Moves in Squared Strings 2

:Author: BJ Peter Dela Cruz
"""


def rot(string: list[str]) -> str:
    """
    This helper function rotates the given list of string clockwise 180 degrees.

    :param string: The list of strings to transform
    :return: A newline-delimited list of strings that have been rotated clockwise 180 degrees
    """
    return "\n".join("".join(list(reversed(current))) for current in reversed(string))


def selfie_and_rot(string: list[str]) -> str:
    """
    This helper function returns the original list of strings plus the same strings rotated
    clockwise 180 degrees.

    :param string: The list of strings to transform
    :return: A newline-delimited list that contain the original strings and strings that have been
        rotated clockwise 180 degrees
    """
    min_length = min(len(current) for current in string)
    new_strings = [current + ("." * min_length) for current in string]
    new_strings.extend([("." * min_length) + current for current in rot(string).split("\n")])
    return "\n".join(new_strings)


def oper(func, string: str) -> str:
    """
    This function delegates the string transformation to the appropriate given function. All the
    helper functions operate on a string with N lines, each string containing N characters.

    :param func: The function that will apply either the rotate or the selfie and rotate string
        transformation
    :param string: The input string to transform
    :return: The string after a transformation has been applied
    """
    return func(string.split("\n"))
