"""
Triangle Area

:Author: BJ Peter Dela Cruz
"""


def t_area(triangle: str):
    """
    This function calculates the area of a triangle that is represented by a list of dots and
    spaces. Each space between a dot, both horizontal and vertical, represents a length of one.

    :param triangle: The input string representing a triangle
    :return: The area of the triangle
    """
    lines = triangle.split('\n')[1:-1]
    leg_a = len(lines) - 1
    leg_b = len(lines[-1].split('.')[1:-1])
    return leg_a * leg_b / 2
