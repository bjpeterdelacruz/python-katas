"""
Fibonacci FizzBuzz

:Author: BJ Peter Dela Cruz
"""


def fibs_fizz_buzz(number: int) -> list:
    """
    This function produces a list of numbers that represent the Fibonacci sequence. But all numbers
    that are divisible by 3 are replaced with the string 'Fizz', all numbers that are divisible by 5
    are replaced with the string 'Buzz', and all numbers that are divisible by both 3 and 5 are
    replaced with the string 'FizzBuzz'.

    :param number: The length of the list of Fibonacci numbers.
    :return: A list representing the Fibonacci sequence but with numbers that are divisible by
    either 3 or 5 replaced with strings ("Fizz", "Buzz", or "FizzBuzz").
    """
    if number == 1:
        return [1]
    arr = [1, 1]
    while len(arr) < number:
        arr.append(arr[-2] + arr[-1])
    for idx, _ in enumerate(arr):
        if arr[idx] % 3 == 0 and arr[idx] % 5 == 0:
            arr[idx] = 'FizzBuzz'
        elif arr[idx] % 3 == 0:
            arr[idx] = 'Fizz'
        elif arr[idx] % 5 == 0:
            arr[idx] = 'Buzz'
    return arr
