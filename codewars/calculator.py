"""
Calculator

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class Calculator:
    """
    This class contains one static method that will solve a basic arithmetic equation. The equation
    is assumed to only contain (, ), asterisk, /, +, and -.
    """
    @staticmethod
    def evaluate(string):
        """
        This method solves the equation in :attr:`string`, keeping in mind the order of operations,
        that is, parentheses first, followed by multiplication and division, and finally addition
        and subtraction.

        :param string: An equation
        :return: A float or an int, the result of solving the equation
        """
        open_parens_indexes = []
        equation = ''.join(string.split())
        idx = 0
        while True:
            if '(' not in equation:
                break
            if equation[idx] == '(':
                open_parens_indexes.append(idx)
                idx += 1
            elif equation[idx] == ')':
                start_idx = open_parens_indexes.pop()
                end_idx = idx
                including_parens = equation[start_idx:end_idx + 1]
                inside_parens = including_parens[1:-1]
                equation = equation.replace(including_parens, solve(inside_parens))
                open_parens_indexes = []
                idx = 0
            else:
                idx += 1
        result = solve(equation)
        return int(float(result)) if float(result).is_integer() else float(result)


def solve(equation: str):
    """
    This helper function formats the given equation before it is solved by :func:`perform_math_ops`,
    namely, making sure that the minus sign, if it comes after another math operator such as the
    multiplication operator, has no space after it, i.e. making the number following the minus sign
    negative.

    :param equation: The equation
    :return: The result of performing all the math operations in :attr:`equation`
    """
    return perform_math_ops(' '.join(str_to_list(equation)).replace("* - ", "* -")
                            .replace("/ - ", "/ -").replace("+ - ", "- ").replace("- - ", "+ "))


def perform_math_ops(equation: str):
    """
    This helper function solves the given :attr:`equation`, first performing multiplication and
    division from left to right and then addition and subtraction also from left to right

    :param equation: The basic arithmetic equation to solve
    :return: A number, the result of solving the equation
    """
    temp = equation.split()
    idx = 0
    while True:
        if idx == len(temp):
            break
        if temp[idx] == '*' or temp[idx] == '/':
            equation = math_op(equation, idx, temp)
            temp = equation.split()
            idx = 0
        else:
            idx += 1
    idx = 0
    while True:
        if idx == len(temp):
            break
        if temp[idx] == '+' or temp[idx] == '-':
            equation = math_op(equation, idx, temp)
            temp = equation.split()
            idx = 0
        else:
            idx += 1
    return equation


def math_op(equation, idx, equation_list):
    """
    Using the operator in :attr:`equation_list`, this helper function performs the appropriate math
    operation.

    :param equation: The equation
    :param idx: The index of the math operator in :attr:`equation_list`
    :param equation_list: A list containing the math operator and operands
    :return: A string representing the result of the math operation
    """
    left = equation_list[idx - 1]
    right = equation_list[idx + 1]
    if equation_list[idx] == '*':
        result = float(left) * float(right)
    elif equation_list[idx] == '/':
        result = float(left) / float(right)
    elif equation_list[idx] == '+':
        result = float(left) + float(right)
    else:
        result = float(left) - float(right)
    return equation.replace(f"{left} {equation_list[idx]} {right}", f"{result}")


def str_to_list(string: str) -> list[str]:
    """
    This helper function converts the given string to a list. If a number has more than one digit,
    the entire number will be concatenated together before being put in the resulting list.

    :param string: A string containing numbers and math operators
    :return: A list representation of the string
    """
    temp = []
    numbers = []
    for character in string:
        if character.isnumeric() or character == '.':
            numbers.append(character)
        else:
            if numbers:
                temp.append(''.join(numbers))
            temp.append(character)
            numbers = []
    if numbers:
        temp.append(''.join(numbers))
    return temp
