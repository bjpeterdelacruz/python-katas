"""
Always Perfect

:Author: BJ Peter Dela Cruz
"""
import functools


def check_root(string: str) -> str:
    """
    This function adds 1 to the product of all the integers in the given string and then takes the
    square root of the result, which is always a perfect square. If there are more than or less than
    four integers in the list, "incorrect input" is returned. If there are other characters besides
    numbers in the list (excluding the commas that separate each integer), "incorrect input" is
    raised. If the numbers in the string are not consecutive, "not consecutive" is returned.
    Otherwise, the sum and the square root are returned. For example, given "4,5,6,7", the string
    "841, 29" is returned.

    :param string: The input string containing integers separated by commas
    :return: "incorrect input", "not consecutive", or the sum and the square root, as described
        above
    """
    try:
        numbers = sorted(map(int, string.split(',')))
        if len(numbers) != 4:
            raise ValueError
    except ValueError:
        return 'incorrect input'
    if list(range(numbers[0], numbers[-1] + 1)) != numbers:
        return 'not consecutive'
    result = functools.reduce(lambda a, b: a * b, numbers) + 1
    return f"{result}, {int(result ** 0.5)}"
