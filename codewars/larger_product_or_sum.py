"""
Larger Product or Sum

:Author: BJ Peter Dela Cruz
"""
from functools import reduce


def sum_or_product(array: list[int], num_elements: int) -> str:
    """
    This function returns the string "sum" if the :attr:`num_elements` largest integers in the list
    is greater than the product of the :attr:`num_elements` smallest integers in the list. If the
    product is greater than the sum, the string "product" is returned. If both values are the same,
    the string "same" is returned.

    :param array: The input list of integers
    :param num_elements: The number of elements to use in the calculation of the sum and the product
    :return: A string: "sum", "product", or "same"
    """
    sorted_array = sorted(array)
    sum_elements = sum(list(reversed(sorted_array))[:num_elements])
    product_elements = reduce(lambda x, y: x * y, sorted_array[:num_elements])
    if sum_elements > product_elements:
        return "sum"
    return "product" if product_elements > sum_elements else "same"
