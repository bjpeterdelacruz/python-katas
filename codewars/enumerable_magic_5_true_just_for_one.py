"""
Enumerable Magic #5: True Just for One

:Author: BJ Peter Dela Cruz
"""


def one(seq, fun) -> bool:
    """
    This function returns True if and only if there is exactly one element in :attr:`seq` that
    satisfies the condition in the given function :attr:`fun`.

    :param seq: The input sequence
    :param fun: The function to apply to each element in :attr:`seq`
    :return: True if there is exactly one element in :attr:`seq` that satisfies the condition in
        :attr:`fun`
    """
    return len(list(filter(fun, seq))) == 1
