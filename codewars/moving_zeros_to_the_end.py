"""
Move Zeros

:Author: BJ Peter Dela Cruz
"""


def move_zeros(int_arr: list) -> list:
    """
    This function moves all the zeros in :attr:`int_arr` to the end of the list. The order of the
    other elements in the list is preserved.

    :param int_arr: The input list, which can consist of anything
    :return: The same list but with all zeros moved to the end of it
    """
    arr = list(reversed(int_arr))
    for idx, value in enumerate(arr):
        try:
            if str(value) != "False" and value != '0' and int(value) == 0:
                arr.insert(0, int(arr.pop(idx)))
        except (ValueError, TypeError):
            pass
    return list(reversed(arr))
