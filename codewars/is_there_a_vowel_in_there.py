"""
Is There a Vowel in There?

:Author: BJ Peter Dela Cruz
"""


def is_vow(arr: list[int]) -> list:
    """
    This function accepts and returns a list of integers, or code points. If a code point represents
    a vowel, then the integer is replaced by the vowel. Otherwise, it is left in place in the list
    that is returned.

    :param arr: The input list of integers
    :return: A list containing integers and/or vowels
    """
    return [code if chr(code).lower() not in 'aeiou' else chr(code) for code in arr]
