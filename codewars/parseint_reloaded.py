"""
parseInt() Reloaded

:Author: BJ Peter Dela Cruz
"""
numbers_map = {"zero": 0, "one": 1, "two": 2, "three": 3, "four": 4, "five": 5, "six": 6,
               "seven": 7, "eight": 8, "nine": 9, "ten": 10, "eleven": 11, "twelve": 12,
               "thirteen": 13, "fourteen": 14, "fifteen": 15, "sixteen": 16, "seventeen": 17,
               "eighteen": 18, "nineteen": 19, "twenty": 20, "thirty": 30, "forty": 40, "fifty": 50,
               "sixty": 60, "seventy": 70, "eighty": 80, "ninety": 90, "hundred": 100,
               "thousand": 1000, "million": 1000000}

# pylint: disable=eval-used
# pylint: disable=too-many-branches


def parse_int(string: str) -> int:
    """
    This function converts a string into an integer. The string represents the integer in words. For
    example, if :attr:`string` is "seven hundred thousand and thirty-five", this function will
    return 700035.

    :param string: The input string that represents an integer
    :return: The integer
    """
    temp = string.replace(" and ", " ")
    numbers = []
    for token in temp.split():
        if "-" in token:
            first, second = token.split("-")
            numbers.append(str(numbers_map[first] + numbers_map[second]))
        elif numbers_map[token] < 100:
            numbers.append(str(numbers_map[token]))
        else:
            numbers.append(token)
    temp = ' '.join(numbers)
    hundred_before_thousand = False
    if "hundred thousand" in temp:
        temp = temp.replace("hundred thousand", "100000")
    elif "hundred" in temp and "thousand" in temp:
        if temp.index("hundred") < temp.index("thousand"):
            hundred_before_thousand = True
    numbers = []
    for token in temp.split():
        if token in numbers_map:
            if token == "hundred" and hundred_before_thousand:
                numbers.append("100000+")
                hundred_before_thousand = False
            else:
                numbers.append(f"{numbers_map[token]}+")
        elif int(token) == 100000:
            numbers.append(f"{token}+")
        else:
            numbers.append(f"{token}*")
    result = ' '.join(numbers)
    if result[-1] == "+":
        result = f"{result}0"
    elif result[-1] == "*":
        result = f"{result}1"
    return eval(result)
