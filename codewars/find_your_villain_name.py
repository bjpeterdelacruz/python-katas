"""
Find Your Villain Name

:Author: BJ Peter Dela Cruz
"""
from datetime import datetime


def get_villain_name(birthdate: datetime) -> str:
    """
    This function generates a villain name using the month and day of the given birth date. The
    month is used as the index for the list that contains the first part of the villain name, and
    the last digit of the day is used as the index for the list that contains the second part of the
    villain name. The two parts are concatenated together and separated by a single space.

    :param birthdate: A person's birth date
    :return: A string representing a villain name
    """
    first = ["The Evil", "The Vile", "The Cruel", "The Trashy", "The Despicable",
             "The Embarrassing", "The Disreputable", "The Atrocious", "The Twirling", "The Orange",
             "The Terrifying", "The Awkward"]
    last = ["Mustache", "Pickle", "Hood Ornament", "Raisin", "Recycling Bin", "Potato", "Tomato",
            "House Cat", "Teaspoon", "Laundry Basket"]
    return f'{first[birthdate.month - 1]} {last[int(str(birthdate.day)[-1])]}'
