"""
Sum of All the Multiples of 3 or 5

:Author: BJ Peter Dela Cruz
"""


def find(number: int) -> int:
    """
    This function sums all the multiples of either three or five from three (inclusive) to
    :attr:`number` (inclusive). If an integer is a multiple of both, it is included only once.

    :param number: The input integer, the last number in the range
    :return: The sum of all the multiples of either three or five from three to :attr:`number`
    """
    return sum(num for num in range(3, number + 1) if num % 3 == 0 or num % 5 == 0)
