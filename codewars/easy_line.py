"""
Easy Line

:Author: BJ Peter Dela Cruz
"""
from math import factorial


def easyline(row: int) -> int:
    """
    This function calculates the sum of the squares of each cell in the given row of a Pascal's
    triangle.

    :param row: A row (an integer) in a Pascal's triangle
    :return: The sum of the squares of each cell in the row
    """
    return sum(calculate_cell(row, cell) ** 2 for cell in range(0, row + 1))


def calculate_cell(row: int, cell: int) -> int:
    """
    This helper function calculates the value for the given cell in the given row of a Pascal's
    triangle. The formula used is: row! ÷ (cell! * (row - cell)!)

    :param row: The row
    :param cell: The cell
    :return: The value for the cell
    """
    return factorial(row) // (factorial(cell) * factorial(row - cell))
