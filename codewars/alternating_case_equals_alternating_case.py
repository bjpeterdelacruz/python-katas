"""
Alternating Cases

:Author: BJ Peter Dela Cruz
"""
from string import ascii_uppercase as upper


def to_alternating_case(string: str) -> str:
    """
    This function converts each lowercase character in the given string to an uppercase character,
    and vice versa. Non-alphabetic characters (i.e. numbers, symbols) are left in the string.

    :param string: The input string
    :return: A string that contains the same characters as the input string but the cases of the
        alphabetic characters are swapped (lowercase to uppercase, and vice versa)
    """
    return ''.join(char.lower() if char in upper else char.upper() for char in string)
