"""
All Star Code Challenge #29

Author: BJ Peter Dela Cruz
"""


def reverse_sentence(sentence: str) -> str:
    """
    This function reverses all the words of a sentence. All space characters are kept in their
    place. For example, given "The   quick brown   fox", this function will return
    "ehT   kciuq nworb   xof".

    :param sentence: The input string that represents a sentence
    :return: The same sentence but with all words reversed
    """
    arr = []
    current_word = []
    for character in sentence:
        if character.isspace():
            if current_word:
                arr.append(''.join(reversed(current_word)))
                current_word = []
            arr.append(character)
        else:
            current_word.append(character)
    arr.append(''.join(reversed(current_word)))
    return ''.join(arr)
