"""
Hashtag Generator

:Author: BJ Peter Dela Cruz
"""


def generate_hashtag(string: str) -> str | bool:
    """
    This function generates a hashtag using the given string. If :attr:`string` is None or empty, or
    the length of the generated hashtag is greater than 140 characters, including the hashtag
    symbol, False is returned.

    :param string: The input string
    :return: A hashtag, or False if :attr:`string` is None or empty, or the hashtag contains more
        than 140 characters
    """
    if not string:
        return False
    hashtag = "#" + ''.join(list(map(lambda x: f"{x[0].upper()}{x[1:].lower()}",
                                     string.strip().split())))
    return hashtag if len(hashtag) <= 140 else False
