"""
Calculate Variance

:Author: BJ Peter Dela Cruz
"""
from statistics import mean


def variance(numbers: list[int]):
    """
    This function calculates the variance for the given list of numbers by subtracting the mean
    from every value, squaring the results, summing them all together, and dividing by the number of
    elements.

    :param numbers: The input list of integers
    :return: The variance for :attr:`numbers`
    """
    average = mean(numbers)
    return sum(map(lambda number: (number - average) ** 2, numbers)) / len(numbers)
