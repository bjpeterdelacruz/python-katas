"""
Find Duplicates

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict


def duplicates(array: list) -> list:
    """
    This function returns a list of all elements that are duplicates. Note that a number and the
    same number as a string do not count.

    :param array: The input list, may contain numbers, strings, or both strings and numbers
    :return: A list of all duplicates in the same order as they appear in :attr:`array`
    """
    values = defaultdict(int)
    result = []
    for number in array:
        if number in values and values[number] == 1:
            result.append(number)
        values[number] += 1
    return result
