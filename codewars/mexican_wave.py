"""
Mexican Wave

:Author: BJ Peter Dela Cruz
"""


def wave(people: str) -> list[str]:
    """
    This function turns a string into a Mexican Wave. For example, given "hello", this function will
    return ["Hello", "hEllo", "heLlo", "helLo", "hellO"]. If the string has spaces in between each
    word, they are ignored, and the next letter will be turned into uppercase. For example, given
    "It is", this function will return ["It is", "iT is", "it Is", "it iS"]. The uppercase letter
    represents a person standing up while the lowercase letters represent people sitting down.

    :param people: The input string that represents people
    :return: The string turned into a Mexican Wave as described above
    """
    if not people:
        return []
    arr = [people] * len(''.join(people.split()))
    word_idx = 0
    for arr_idx, word in enumerate(arr):
        while True:
            if word[word_idx] != ' ':
                break
            word_idx += 1
        current_word = [char.upper() if current_idx == word_idx else char.lower()
                        for current_idx, char in enumerate(word)]
        arr[arr_idx] = ''.join(current_word)
        word_idx += 1
    return arr
