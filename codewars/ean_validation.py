"""
EAN Validation

:Author: BJ Peter Dela Cruz
"""


def validate_ean(code: str) -> bool:
    """
    This function validates the given European Article Number (EAN) barcode. The barcode is thirteen
    digits long. Each digit in the even position of the 12-digit data portion of the barcode is
    multiplied by 1, and each digit in the odd position is multiplied by 3. The two results are
    summed together. If the sum is a multiple of 10, the checksum is zero. Otherwise, the checksum
    is 10 - (sum modulo 10). If the checksum matches the last (13th) digit in the barcode, True is
    returned. If they do not match or the barcode is not thirteen digits long, False is returned.

    :param code: The EAN barcode to validate
    :return: True if the barcode is valid, False otherwise
    """
    if len(code) != 13:
        return False
    even_positions = sum(int(code[idx]) * 1 for idx in range(0, 12) if idx % 2 == 0)
    odd_positions = sum(int(code[idx]) * 3 for idx in range(1, 13) if idx % 2 == 1)
    remainder = (even_positions + odd_positions) % 10
    checksum = 0 if remainder == 0 else 10 - remainder
    return checksum == int(code[-1])
