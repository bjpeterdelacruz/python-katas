"""
Sum of Minimums

:Author: BJ Peter Dela Cruz
"""


def sum_of_minimums(numbers: list[list[int]]) -> int:
    """
    This function gathers the minimum value in each sublist of the given list and sums them
    together.

    :param numbers: The input list containing a list of integers
    :return: The sum of all the minimum values
    """
    return sum(min(row) for row in numbers)
