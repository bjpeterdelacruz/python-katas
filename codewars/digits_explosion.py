"""
Digits Explosion

:Author: BJ Peter Dela Cruz
"""


def explode(string: str) -> str:
    """
    This function repeats each digit in :attr:`string` and returns a new string. For example, given
    "35207", the string "33355555227777777" is returned.

    :param string:
    :return:
    """
    return ''.join(char * int(char) for char in string)
