"""
Roman Numerals Helper

:Author: BJ Peter Dela Cruz
"""
num_to_roman = {1: "I", 5: "V", 10: "X", 50: "L", 100: "C", 500: "D", 1000: "M", 4: "IV",
                9: "IX", 40: "XL", 90: "XC", 400: "CD", 900: "CM"}
roman_to_num = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000, "IV": 4,
                "IX": 9, "XL": 40, "XC": 90, "CD": 400, "CM": 900}


class RomanNumerals:
    """
    This class contains static methods that will convert numbers to Roman numerals and vice versa.
    """

    @staticmethod
    def to_roman(integer: int) -> str:
        """
        This static function converts a number to Roman numerals.

        :param integer: The integer to convert
        :return: A string of Roman numerals
        """
        temp = []
        string = str(integer)
        for idx, char in enumerate(string):
            count = len(string) - (idx + 1)
            temp.append(int(char + ("0" * count)))
        strings = []
        for number in temp:
            if number in num_to_roman:
                strings.append(num_to_roman[number])
            else:
                if number > 999:
                    num = number // 1000
                    strings.append(num_to_roman[1000] * num)
                elif number > 99:
                    num = number // 100
                    if num > 5:
                        strings.append(num_to_roman[500] + (num_to_roman[100] * (num - 5)))
                    else:
                        strings.append(num_to_roman[100] * num)
                elif number > 9:
                    num = number // 10
                    if num > 5:
                        strings.append(num_to_roman[50] + (num_to_roman[10] * (num - 5)))
                    else:
                        strings.append(num_to_roman[10] * num)
                elif number > 5:
                    strings.append(num_to_roman[5] + (num_to_roman[1] * (number - 5)))
                else:
                    strings.append(num_to_roman[1] * number)
        return ''.join(strings)

    @staticmethod
    def from_roman(string: str) -> int:
        """
        This static function converts a string of Roman numerals to a number.

        :param string: A string of Roman numerals
        :return: An integer
        """
        temp = []
        idx = 1
        while True:
            if idx == len(string):
                temp.append(roman_to_num[string[idx - 1]])
                break
            if string[idx - 1] + string[idx] in roman_to_num:
                temp.append(roman_to_num[string[idx - 1] + string[idx]])
                idx += 1
                if idx == len(string):
                    break
            else:
                temp.append(roman_to_num[string[idx - 1]])
            idx += 1
        return sum(temp)
