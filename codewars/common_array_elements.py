"""
Common Array Elements

:Author: BJ Peter Dela Cruz
"""


def common(array1: list[int], array2: list[int], array3: list[int]) -> int:
    """
    This function sums all the elements that are common in :attr:`array1`, :attr:`array2`, and
    :attr:`array3`.

    :param array1: The first input list of integers
    :param array2: The second input list of integers
    :param array3: The third input list of integers
    :return: The sum of all the elements that are common in all three lists
    """
    dict1 = {}
    for number in array1:
        if number in dict1:
            dict1[number].append(number)
        else:
            dict1[number] = [number]

    dict2 = {}
    for number in array2:
        if number in dict2:
            dict2[number].append(number)
        else:
            dict2[number] = [number]

    dict3 = {}
    for number in array3:
        if number in dict3:
            dict3[number].append(number)
        else:
            dict3[number] = [number]

    common_keys = dict1.keys() & dict2.keys() & dict3.keys()
    total = 0
    for key in common_keys:
        minimum = min(len(dict1[key]), len(dict2[key]), len(dict3[key]))
        total += (key * minimum)
    return total
