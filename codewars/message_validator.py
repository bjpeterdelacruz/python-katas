"""
Message Validator

:Author: BJ Peter Dela Cruz
"""
import re


def is_a_valid_message(message: str) -> bool:
    """
    This function validates that the given :attr:`message` is in the following format: length of
    word followed by word, length of word followed by word, etc. For example, given "3hey5hello2hi",
    this function will return True because "hey" is 3 characters long, "hello" is 5 characters long,
    and "hi" is 2 characters long.

    :param message: The input string, the message to validate
    :return: True if the message passes validation, False otherwise
    """
    if not message:
        return True
    if not message[0].isnumeric():
        return False
    if message[-1].isnumeric():
        return False
    matches = re.findall(r"[0-9]+[A-Za-z]+", message)
    for current_string in matches:
        tup = re.findall(r"(\d+)([A-Za-z]+)", current_string)
        length = int(tup[0][0])
        word = tup[0][1]
        if length != len(word):
            return False
    return True
