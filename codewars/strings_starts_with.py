"""
Strings: Starts With

:Author: BJ Peter Dela Cruz
"""


def starts_with(string: str, prefix: str) -> bool:
    """
    This function is similar to the startswith function in the string class. It will return True if
    :attr:`string` starts with :attr:`prefix`.

    :param string: The input string
    :param prefix: A series of characters, may be an empty string
    :return: True if :attr:`string` starts with :attr:`prefix`, False otherwise
    """
    try:
        return string.index(prefix) == 0
    except ValueError:
        return False
