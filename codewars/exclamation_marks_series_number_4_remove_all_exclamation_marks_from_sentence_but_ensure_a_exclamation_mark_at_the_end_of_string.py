"""
Exclamation Marks Series #4: Remove All Exclamation Marks from Sentences but Ensure an Exclamation
Mark at the End of String

:Author: BJ Peter Dela Cruz
"""


def remove(string: str) -> str:
    """
    This function removes all exclamation marks in :attr:`string` and then adds an exclamation mark
    at the end of it.

    :param string: The input string
    :return: A string that contains only one exclamation mark, which is at the end of the string
    """
    return f'{string.replace("!", "")}!'
