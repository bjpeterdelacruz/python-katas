from collections import defaultdict


def duplicates(arr):
    pairs = defaultdict(int)
    for number in arr:
        pairs[number] += 1
    counts = 0
    for count in pairs.values():
        while count % 2 == 1:
            count -= 1
        counts += count
    return counts // 2
