"""
Thinkful: Graceful Addition

:Author: BJ Peter Dela Cruz
"""
from numbers import Number


def my_add(val1, val2) -> Number | None:
    """
    This function adds the given values together if they are both numbers (integers or floats). If
    either is not a number, None is returned.

    :param val1: The input number
    :param val2: The input number
    :return: The sum if both values are numbers, or None if either is not a number
    """
    return val1 + val2 if isinstance(val1, Number) and isinstance(val2, Number) else None
