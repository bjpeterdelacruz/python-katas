"""
Censored Strings

:Author: BJ Peter Dela Cruz
"""


def uncensor(infected: str, discovered: str) -> str:
    """
    This function replaces all the asterisk characters in :attr:`infected` with all the characters
    in :attr:`discovered`. It is assumed that the number of asterisks is equal to the number of
    characters in :attr:`discovered`.

    :param infected: The input string containing asterisks
    :param discovered: The input string containing characters to use in the replacement process
    :return: A string with all the asterisks replaced
    """
    arr = []
    idx = 0
    for character in infected:
        if character == '*':
            arr.append(discovered[idx])
            idx += 1
        else:
            arr.append(character)
    return ''.join(arr)
