"""
Exclamation Marks Series #8: Move All Exclamation Marks to the End of the Sentence

:Author: BJ Peter Dela Cruz
"""


def remove(string: str) -> str:
    """
    This function moves all the exclamation marks in :attr:`string` to the end of it.

    :param string: The input string
    :return: A string with all the exclamation marks moved to the end of it
    """
    return string.replace("!", "") + ("!" * string.count('!'))
