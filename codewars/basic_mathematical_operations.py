"""
Basic Mathematical Operations

:Author: BJ Peter Dela Cruz
"""


def basic_op(operator: str, value1, value2):
    """
    This function performs simple arithmetic operations (addition, subtraction, multiplication, or
    division) and then returns the result. If :attr:`operator` is not '+', '-', '*', or '/', a
    ValueError is raised. If :attr:`operator` is '/' and :attr:`value2` is zero, a ValueError is
    raised (division by zero is not allowed).

    :param operator: The input string, a character representing an arithmetic operation
    :param value1: The first number
    :param value2: The second number
    :return: The result of performing the arithmetic operation on :attr:`value1` and :attr:`value2`
    """
    if operator == '+':
        return value1 + value2
    if operator == '-':
        return value1 - value2
    if operator == '*':
        return value1 * value2
    if operator == '/':
        if value2 == 0:
            raise ValueError("Cannot divide by zero!")
        return value1 / value2
    raise ValueError("Operator must be '+', '-', '*', or '/'")
