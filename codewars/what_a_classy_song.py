"""
What a Classy Song

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class Song:
    """
    This class represents a song that is sung by an artist. A set in this class keeps track of the
    people who listened to a song.
    """
    def __init__(self, title: str, artist: str):
        """
        Initializes this song with a title and the artist who sang it.

        :param title: The title of this song
        :param artist: The artist who sang this song
        """
        self.title = title
        self.artist = artist
        self.listeners = set()

    def how_many(self, listeners: list[str]) -> int:
        """
        Returns the number of people who listened to this song in a day. Each call to this method
        represents a day. Only unique listeners are counted throughout the lifetime of this song.
        For example, "John" and "JOHN" are considered the same person, but "John" and "Johnny" are
        two different people.

        :param listeners: A list representing people who listened to this song
        :return: The number of people who listened to this song in a day
        """
        count = 0
        for listener in listeners:
            if listener.lower() not in self.listeners:
                self.listeners.add(listener.lower())
                count += 1
        return count
