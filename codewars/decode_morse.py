"""
Decode Morse

:Author: BJ Peter Dela Cruz
"""
import re


TOME = {'...': 's', '..-': 'u', '-.': 'n', '-..-': 'x', '.----': '1', '-': 't', '.--.': 'p',
        '.---': 'j', '....-': '4', '.-.': 'r', '.....': '5', '-----': '0', '-...': 'b', '-..': 'd',
        '----.': '9', '-....': '6', '--.-': 'q', '--..': 'z', '.--': 'w', '---': 'o', '..---': '2',
        '.-': 'a', '..': 'i', '-.-.': 'c', '...--': '3', '-.--': 'y', '....': 'h', '---..': '8',
        '...-': 'v', '--...': '7', '--.': 'g', '.': 'e', '--': 'm', '.-..': 'l', '..-.': 'f',
        '-.-': 'k'}


def decode(string: str) -> str:
    """
    This function decodes the given message that is written in Morse code. Letters are separated by
    one space, and words are separated by two spaces.

    :param string: The input string, a message written in Morse code
    :return: The decoded message
    """
    words = re.split(r'\s{2,}', string)
    result = []
    for word in words:
        result.append(''.join([TOME[character] for character in word.split()]))
    return ' '.join(result)
