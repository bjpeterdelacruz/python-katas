"""
Filling an Array (Part 1)

:Author: BJ Peter Dela Cruz
"""


def arr(end: int = 0) -> list:
    """
    This function returns a list containing integers from 0 (inclusive) to :attr:`end` (exclusive).
    If no input parameter is supplied, an empty list is returned.

    :param end: The input integer, the number of elements in the list that is returned
    :return: A list of integers from 0 to :attr:`end` - 1, or an empty list
    """
    return list(range(0, end))
