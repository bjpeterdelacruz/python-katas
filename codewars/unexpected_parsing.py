"""
Unexpected Parsing

:Author: BJ Peter Dela Cruz
"""


def get_status(is_busy: bool) -> dict[str, str]:
    """
    This function simply returns a dictionary with "status" as the key and a value of "busy" (if the
    input parameter is True) or "available" (if the input parameter is False).

    :param is_busy: Represents whether someone or a process is busy.
    :return: A dictionary with "status" as the key and a value of either "busy" or "available"
    """
    return {"status": "busy" if is_busy else "available"}
