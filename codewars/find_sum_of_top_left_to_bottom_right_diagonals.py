"""
Find Diagonal Sum

:Author: BJ Peter Dela Cruz
"""


def diagonal_sum(array: list[list[int]]) -> int:
    """
    This function sums the left-right diagonal of the given two-dimensional list of integers.

    :param array: A list that contains lists of integers
    :return: The sum of the left-right diagonal, e.g. array[0][0], array[1][1], array[2][2], etc.
    """
    return sum(row[idx] for idx, row in enumerate(array))
