"""
Sum of Pairs

:Author: BJ Peter Dela Cruz
"""


def sum_pairs(numbers, num):
    """
    This function returns the first pair of integers that sum to :attr:`num`.

    :param numbers: The input list of integers
    :param num: The sum
    :return: A pair of integers from :attr:`numbers` that sum to :attr:`num`
    """
    if len(numbers) < 2:
        return None
    temp = set()
    temp.add(numbers[0])
    for idx in range(1, len(numbers)):
        if num - numbers[idx] in temp:
            return [num - numbers[idx], numbers[idx]]
        temp.add(numbers[idx])
    return None
