"""
Array Plus Array

:Author: BJ Peter Dela Cruz
"""


def array_plus_array(arr1: list, arr2: list):
    """
    This function sums all the elements in :attr:`arr1`, sums all the elements in :attr:`arr2`, and
    then sums the two results together.

    :param arr1: The first input list
    :param arr2: The second input list
    :return: The result of summing together both sums
    """
    return sum(arr1) + sum(arr2)
