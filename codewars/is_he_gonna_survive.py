"""
Is He Gonna Survive?

:Author: BJ Peter Dela Cruz
"""


def hero(bullets: int, dragons: int) -> bool:
    """
    This function returns True if the hero has enough bullets to kill all the dragons. Each dragon
    takes two bullets to kill.

    :param bullets: The number of bullets
    :param dragons: The number of dragons
    :return: True if the hero has enough bullets to kill all the dragons, False otherwise
    """
    return bullets >= dragons * 2
