"""
Aerial Firefighting

:Author: BJ Peter Dela Cruz
"""


def waterbombs(fire: str, water: int) -> int:
    """
    This function counts the number of water bombs needed to extinguish the fires that are in
    :attr:`fire`. A single building is represented as Y and is used as a delimiter for splitting
    :attr:`fire`, and :attr:`water` represents the radius of a water bomb.

    :param fire: The input string containing fires and buildings
    :param water: The input integer, the radius of a water bomb, i.e. the number of fires that can
        be put out by a single water bomb
    :return: The number of water bombs needed to extinguish all the fires in :attr:`fire`
    """
    fires = fire.split('Y')
    total = 0
    for current_fire in fires:
        if current_fire:
            while len(current_fire) > water:
                current_fire = current_fire[water:]
                total += 1
            total += 1
    return total
