"""
DNA to RNA Conversion

:Author: BJ Peter Dela Cruz
"""


def dna_to_rna(dna: str) -> str:
    """
    This function converts a string representing a DNA sequence to a string representing an RNA
    sequence. RNA does not contain Thymine (T). Instead, it contains Uracil (U). The other three
    molecules (Guanine (G), Cytosine (C), and Adenine (A)) are present in both DNA and RNA.

    :param dna: A string representing a DNA sequence
    :return: A string representing an RNA sequence
    """
    return ''.join('U' if char == 'T' else char for char in dna)
