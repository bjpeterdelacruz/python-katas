"""
Make the Deadfish Swim

:Author: BJ Peter Dela Cruz
"""


def parse(data: str) -> list[int]:
    """
    This function parses commands written in the Deadfish esoteric language. "i" increments the
    value in the accumulator (starting value is 0), "d" decrements the value in the accumulator, "s"
    squares the value in the accumulator, and "o" appends the value in the accumulator to a list
    that is outputted when the function is returned.

    :param data: The input string representing commands, each command being one character long
    :return: A list of values from the accumulator
    """
    results = []
    value = 0
    for char in data:
        if char == "i":
            value += 1
        elif char == "d":
            value -= 1
        elif char == "s":
            value *= value
        elif char == "o":
            results.append(value)
    return results
