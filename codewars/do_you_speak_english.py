"""
Do You Speak English?

:Author: BJ Peter Dela Cruz
"""


def sp_eng(sentence) -> bool:
    """
    This function returns True if the given input is a string and the word "english" is found in the
    string. The search is case-insensitive.

    :param sentence: The input
    :return: True if :attr:`sentence` is a string and "english" (regardless of case) is found in it,
        False otherwise
    """
    return isinstance(sentence, str) and "english" in sentence.lower()
