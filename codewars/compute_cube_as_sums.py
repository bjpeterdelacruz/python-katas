"""
Compute Cube as Sums

:Author: BJ Peter Dela Cruz
"""


def find_summands(number: int) -> list[int]:
    """
    This function finds all the summands that equal to the cube of the given number.

    :param number: The input integer
    :return: A list of integers whose sum equals the cube of :attr:`number`
    """
    if number == 1:
        return [1]
    lower_bound = 2 * (((number ** 2) - number) // 2 + 1) - 1
    upper_bound = 2 * (((number ** 2) + number) // 2) - 1
    current = lower_bound + 2
    arr = [lower_bound]
    while current <= upper_bound:
        arr.append(current)
        current += 2
    return arr
