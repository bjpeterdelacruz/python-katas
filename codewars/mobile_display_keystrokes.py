"""
Mobile Display Keyboard

:Author: BJ Peter Dela Cruz
"""
characters = [
    ["1", "*", "0", "#", "2", "3", "4", "5", "6", "7", "8", "9"],
    ["a", "d", "g", "j", "m", "p", "t", "w"],
    ["b", "e", "h", "k", "n", "q", "u", "x"],
    ["c", "f", "i", "l", "o", "r", "v", "y"],
    ["s", "z"]
]


def mobile_keyboard(string: str) -> int:
    """
    This function counts the number of keystrokes for the given string. On a mobile device keyboard,
    a user must press a button one or more times to input a character. For example, to input the
    character "b", the user must press the 2 button three times, which is the number of keystrokes.

    :param string: A lowercase string containing alphanumeric characters and/or the symbols "*"/"#"
    :return: The total number of keystrokes to input :attr:`string`
    """
    total = 0
    for char in string:
        for idx, arr in enumerate(characters):
            if char in arr:
                total += idx + 1
                break
    return total
