"""
Next Version

:Author: BJ Peter Dela Cruz
"""


def next_version(version: str) -> str:
    """
    This function calculates the next version number after :attr:`version`. For example, given
    "1.2.3" and "0.9.9", this function will return "1.2.4" and "1.0.0", respectively.

    :param version: The input string representing a version number separated by dots
    :return: The next version number as a string
    """
    num_periods = len(version.split('.')) - 1
    original_version_str = ''.join(version.split('.'))
    version_str = str(int(original_version_str) + 1)
    while len(version_str) < len(original_version_str):
        version_str = f"0{version_str}"
    reversed_list = list(reversed(version_str))
    idx = 1
    while num_periods > 0:
        reversed_list.insert(idx, '.')
        idx += 2
        num_periods -= 1
    return ''.join(reversed(reversed_list))
