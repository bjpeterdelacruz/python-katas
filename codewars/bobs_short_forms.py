"""
Bob's Short Form

:Author: BJ Peter Dela Cruz
"""


def short_form(string: str) -> str:
    """
    This function removes all vowels in the given string unless the vowel appears at the start or
    end of the string.

    :param string: The input string
    :return: A string with all vowels removed except those at the beginning or end of the string
    """
    copy = list(string)
    for idx in range(1, len(copy) - 1):
        if copy[idx].lower() in "aeiou":
            copy[idx] = ''
    return ''.join(copy)
