"""
Exclamation Marks Series #17: Put the Exclamation Marks and Question Marks on the Balance - Are They
Balanced?

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def balance(left: str, right: str) -> str:
    """
    This function compares two strings that only contain exclamation marks and/or question marks.
    An exclamation mark has a weight of two, and a question mark has a weight of three. The sum of
    all the weights in one string is compared with the sum of all the weights in the other string.
    If :attr:`left` weighs more, "Left" is returned. If :attr:`right` weighs more, "Right" is
    returned. If both strings weigh the same, "Balance" is returned.

    :param left: The input string
    :param right: The input string
    :return: "Left", "Right", or "Balance"
    """
    totals = [sum(len(list(grp)) * 2 if key == '!' else len(list(grp)) * 3
                  for key, grp in groupby(group)) for group in [left, right]]
    return "Left" if totals[0] > totals[1] else "Right" if totals[0] < totals[1] else "Balance"
