"""
Cyclops Numbers

:Author: BJ Peter Dela Cruz
"""


def cyclops(number: int) -> bool:
    """
    This function returns True if the given number is a cyclops number, that is, the number is odd,
    and there is one and only one 0, and it is in the middle of the binary representation of the
    number.

    :param number: The input integer
    :return: True if the number is a cyclops number, False otherwise
    """
    binary = f"{number:0b}"
    return len(binary) % 2 == 1 and binary[len(binary) // 2] == "0" and binary.count("0") == 1
