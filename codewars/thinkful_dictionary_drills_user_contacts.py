"""
Thinkful: User Contacts

:Author: BJ Peter Dela Cruz
"""


def user_contacts(data: list[list]) -> dict:
    """
    This function converts the given list that contains contact information for users into a
    dictionary. If a user does not have contact information, the key is assigned the value of None
    in the dictionary.

    :param data: The input list of lists, each sublist representing contact information for a user
    :return: A dictionary containing contact information for all users listed in :attr:`data`
    """
    return {contact[0]: contact[1] if len(contact) == 2 else None for contact in data}
