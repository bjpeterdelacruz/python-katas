"""
Receiving Integers as Bytes

:Author: BJ Peter Dela Cruz
"""


def interpret_as_int32(stream: list) -> list[int]:
    """
    This function converts a list of bytes to 32-bit signed integers.

    :param stream: The input list of bytes
    :return: A list of 32-bit integers
    """
    current_bytes = []
    count = 1
    bytes_string = b''.join(stream)
    integers = []
    for byte in bytes_string:
        current_bytes.append(f"{byte:08b}")
        if count == 4:
            byte_string = ''.join(current_bytes)
            integers.append(twos_complement(int(byte_string, 2), len(byte_string)))
            current_bytes = []
            count = 0
        count += 1
    return integers


def twos_complement(value: int, number_of_bits: int):
    """
    This helper function returns the two's complement of the given value.

    :param value: The input integer
    :param number_of_bits: The number of bits that the integer uses
    :return: The two's complement of :attr:`value`
    """
    if (value & (1 << (number_of_bits - 1))) != 0:
        return value - (1 << number_of_bits)
    return value
