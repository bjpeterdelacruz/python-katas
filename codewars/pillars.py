"""
Pillars

:Author: BJ Peter Dela Cruz
"""


def pillars(count: int, dist: int, width: int) -> int:
    """
    This function calculates the distance in centimeters between the first and last pillars (without
    the widths of both pillars).

    :param count: The number of pillars
    :param dist: The distance between each pillar (in meters)
    :param width: The width of each pillar (in centimeters)
    :return: The distance between the first and last pillar
    """
    return (count - 2) * width + (count - 1) * dist * 100 if count > 1 else 0
