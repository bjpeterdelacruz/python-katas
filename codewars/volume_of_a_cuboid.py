"""
Volume of a Cuboid

:Author: BJ Peter Dela Cruz
"""


def get_volume_of_cuboid(length, width, height):
    """
    This function returns the volume of a cuboid, which is length * width * height.

    :param length: The length of a cuboid
    :param width: The width of a cuboid
    :param height: The height of a cuboid
    :return: The volume of a cuboid
    """
    return length * width * height
