"""
Binary Pyramid 101

:Author: BJ Peter Dela Cruz
"""


def binary_pyramid(start: int, end: int) -> str:
    """
    This function converts all the integers from :attr:`start` to :attr:`end`, inclusive, to binary
    strings, sums together all the binary strings as if they were in base 10, and then converts the
    result to a binary string, which is returned.

    :param start: The input integer, the starting value
    :param end: The input integer, the ending value
    :return: The sum of all the numbers from :attr:`start` to :attr:`end`, inclusive
    """
    if start > end:
        start, end = end, start
    return bin(sum(int(bin(num)[2:]) for num in range(start, end + 1)))[2:]
