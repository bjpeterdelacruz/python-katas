"""
Array Mash

:Author: BJ Peter Dela Cruz
"""


def array_mash(arr1: list, arr2: list) -> list:
    """
    This function interleaves the elements in :attr:`arr1` with the elements in :attr:`arr2`. For
    example, given [1, 3, 5] and [2, 4, 6], [1, 2, 3, 4, 5, 6] is returned. It is assumed that both
    lists contain the same number of elements.

    :param arr1: The first input list
    :param arr2: The second input list
    :return: A list containing elements from both lists
    """
    return [element for lst in zip(arr1, arr2) for element in lst]
