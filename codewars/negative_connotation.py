"""
Negative Connotation

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase, ascii_uppercase


def connotation(string: str) -> bool:
    """
    This function returns True if there are more words in the given string that begin with a letter
    that belongs in the first half of the alphabet (A-M) or False if there are more words that begin
    with a letter that belongs in the second half (N-Z).

    :param string: The input string consisting of words separated by one or more whitespaces
    :return: True if there are more words that begin with a letter between A-M, or False if there
        are more words that begin with a letter between N-Z.
    """
    first_half = f'{ascii_lowercase[:13]}{ascii_uppercase[:13]}'
    second_half = f'{ascii_lowercase[13:]}{ascii_uppercase[13:]}'
    words = string.split()
    first = sum(1 for letter in words if letter[0] in first_half)
    second = sum(1 for letter in words if letter[0] in second_half)
    return first >= second
