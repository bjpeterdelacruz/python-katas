def calculator(txt: str) -> str:
    value_1, operator, value_2 = txt.split()
    if operator == "+":
        return "." * (len(value_1) + len(value_2))
    if operator == "-":
        return "." * (len(value_1) - len(value_2))
    if operator == "*":
        return "." * (len(value_1) * len(value_2))
    return "." * (len(value_1) // len(value_2))
