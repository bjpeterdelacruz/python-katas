"""
Get Character from ASCII Value

:Author: BJ Peter Dela Cruz
"""


def get_char(value: int) -> str:
    """
    This function returns the Unicode character associated with the given integer value.

    :param value: An integer
    :return: A Unicode character
    """
    return chr(value)
