"""
Perfect Squares, Perfect Fun

:Author: BJ Peter Dela Cruz
"""


def square_it(digits: int) -> str:
    """
    This function checks whether the number of digits in :attr:`digits` is a perfect square. If it
    is, then a string representing a square block is returned. The length of each side, i.e. the
    number of digits on each line, is the square root of the number of digits in :attr:`digits`. If
    the number of digits is not a perfect square, "Not a perfect square!" is returned.

    :param digits: The input integer
    :return: A square block (newline-delimited list of strings), or "Not a perfect square!"
    """
    digits_str = str(digits)
    square_root = len(digits_str) ** 0.5
    if int(square_root) ** 2 != len(digits_str):
        return "Not a perfect square!"
    square_root = int(square_root)
    return '\n'.join([digits_str[idx:idx + square_root]
                      for idx in range(0, len(digits_str), square_root)])
