"""
Cut Rope

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict


def cut_rope(length: int, first_cut: int, second_cut: int):
    """
    Given a rope of :attr:`length` cm, make a mark on the rope every :attr:`first_cut` cm from the
    beginning all the way to the end. Then make a second mark on the rope every :attr:`second_cut`
    cm starting from the beginning again. Finally, cut the rope at each mark, and then return the
    number of pieces of rope grouped by their lengths.

    :param length: The length of a rope
    :param first_cut: The first series of marks
    :param second_cut: The second series of marks
    :return: A dictionary containing rope lengths and the number of pieces of each length
    """
    rope = ['-'] * length
    for cut in [first_cut, second_cut]:
        counter = 0
        for idx, char in enumerate(rope):
            if char != '-':
                continue
            counter += 1
            if counter % cut == 0:
                if idx + 1 < len(rope) and rope[idx + 1] != '.':
                    rope.insert(idx + 1, '.')
                counter = 0
    counts = defaultdict(int)
    rope = ''.join(rope).split('.')
    for cut in rope:
        counts[f'{len(cut)}cm'] += 1
    return counts
