"""
They're Good Dogs

:Author: BJ Peter Dela Cruz
"""


def we_rate_dogs(string: str, rating: int) -> str:
    """
    This function sets the appropriate rating for a dog in the given string.

    :param string: The input string that contains a rating that needs to be changed
    :param rating: The new rating for a dog
    :return: A string that contains the new rating
    """
    return ' '.join(f'{rating}/10' if word.endswith("/10") else word for word in string.split())
