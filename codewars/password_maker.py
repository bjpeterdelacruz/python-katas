"""
Password Maker

:Author: BJ Peter Dela Cruz
"""


def make_password(phrase: str) -> str:
    """
    This function creates a password using the first word of each letter in the given string (each
    word is separated by one or more spaces). If the letter is an 'i' or 'I', the number 1 is used
    instead. If the letter is an 'o' or 'O', the number 0 is used, and if the letter is an 's' or
    'S', the number 5 is used.

    :param phrase: The input string
    :return: A password string
    """
    passphrase = []
    for word in phrase.split():
        letter = word[0]
        if letter in ['i', 'I']:
            letter = '1'
        elif letter in ['o', 'O']:
            letter = '0'
        elif letter in ['s', 'S']:
            letter = '5'
        passphrase.append(letter)
    return ''.join(passphrase)
