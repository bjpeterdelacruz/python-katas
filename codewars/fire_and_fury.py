"""
FIRE and FURY

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def fire_and_fury(tweet: str) -> str:
    """
    This function converts the words "FIRE" and "FURY", if found in the given :attr:`tweet`, to
    "You are fired!" and "I am furious.", respectively. If either word is not found or there are
    characters in :attr:`tweet` besides E, F, I, R, U, and Y, then "Fake tweet." is returned. If
    "FIRE" appears more than once, then "and you" will be inserted between "You" and "are". If
    "FURY" appears more than once, then "really" will be inserted between "am" and "furious." The
    number of times these two substrings appear is equal to the number of times "FIRE" or "FURY"
    appears in :attr:`tweet` minus 1.

    :param tweet: The input string representing a tweet by a former U.S. President that got mangled
    :return: The President's tweet after clearing up all the noise in :attr:`tweet`
    """
    if "FIRE" not in tweet and "FURY" not in tweet:
        return "Fake tweet."
    for letter in tweet:
        if letter not in ["E", "F", "I", "R", "U", "Y"]:
            return "Fake tweet."
    words = []
    while "FIRE" in tweet or "FURY" in tweet:
        # Only one word is in tweet
        if "FIRE" in tweet and "FURY" not in tweet:
            words.append("FIRE")
            tweet = tweet.replace("FIRE", "", 1)
        elif "FIRE" not in tweet and "FURY" in tweet:
            words.append("FURY")
            tweet = tweet.replace("FURY", "", 1)
        # Both words are in tweet
        elif tweet.index("FIRE") < tweet.index("FURY"):
            words.append("FIRE")
            tweet = tweet.replace("FIRE", "", 1)
        else:
            words.append("FURY")
            tweet = tweet.replace("FURY", "", 1)
    groups = [list(group) for _, group in groupby(words)]
    new_tweet = []
    for group in groups:
        count = len(group) - 1
        if group[0] == "FIRE":
            arr = ['You'] + (['and you'] * count) + ['are', 'fired!']
        else:
            arr = ['I', 'am'] + (['really'] * count) + ['furious.']
        new_tweet.append(' '.join(arr))
    return ' '.join(new_tweet)
