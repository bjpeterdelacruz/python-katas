"""
RGB To Hex Conversion

:Author: BJ Peter Dela Cruz
"""


def rgb(red: int, green: int, blue: int) -> str:
    """
    This function converts an RGB integer value into a 6-character hexadecimal string. For example,
    given (255, 0, 10), this function will return "FF000A".

    :param red: The input integer representing the value for red
    :param green: The input integer representing the value for green
    :param blue: The input integer representing the value for blue
    :return: A six-character hexadecimal string
    """
    red = 255 if red > 255 else 0 if red < 0 else red
    green = 255 if green > 255 else 0 if green < 0 else green
    blue = 255 if blue > 255 else 0 if blue < 0 else blue
    return f"{red:02x}{green:02x}{blue:02x}".upper()
