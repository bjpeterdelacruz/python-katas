"""
Keyboard Handler

:Author: BJ Peter Dela Cruz
"""
from string import ascii_uppercase, ascii_lowercase


def handler(key: str, is_caps: bool = False, is_shift: bool = False) -> str:
    """
    This function returns a key from a QWERTY keyboard. If Caps Lock is on and Shift is not pressed,
    the uppercase version of an alphabet character is returned. If Caps Lock is off and Shift is
    pressed, the uppercase version of an alphabet character or a symbol from the shift_keys map is
    returned. If Caps Lock is on and Shift is pressed, the lowercase version of an alphabet
    character or a character from the shift_keys map is returned. If Caps Lock is off and Shift is
    not pressed, the key that was pressed is simply returned. If :attr:`key` is an uppercase letter,
    a string that contains two or more characters, or is not a string, the string "KeyError" is
    returned.

    :param key: The key that was pressed on a QWERTY keyboard
    :param is_caps: True if Caps Lock is on, False otherwise
    :param is_shift: True if Shift is pressed at the same time as :attr:`key`, False otherwise
    :return: A character or string as described above
    """
    if not isinstance(key, str) or len(key) != 1 or key in ascii_uppercase:
        return 'KeyError'
    shift_keys = {'1': '!', '2': '@', '3': '#', '4': '$', '5': '%', '6': '^', '7': '&', '8': '*',
                  '9': '(', '0': ')', '-': '_', '=': '+', '[': '{', ']': '}', '\\': '|', ';': ':',
                  '\'': '"', ',': '<', '.': '>', '/': '?', '`': '~'}
    if is_caps and is_shift:
        return key if key in ascii_lowercase else shift_keys[key]
    if is_shift:
        return shift_keys[key] if key in shift_keys else key.upper()
    return key.upper() if is_caps else key
