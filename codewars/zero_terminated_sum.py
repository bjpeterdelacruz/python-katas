"""
Zero Terminated Sum

:Author: BJ Peter Dela Cruz
"""


def largest_sum(string: str) -> int:
    """
    Given a string in which each number is delimited by a zero, this function sums all the digits in
    each number and returns the largest sum.

    :param string: The input string representing a zero-delimited list of numbers
    :return: The largest sum
    """
    arr = [sum(int(char) for char in digits) for digits in string.split('0') if digits]
    return max(arr) if arr else 0
