"""
Find Next Highest Number with Same Bits

:Author: BJ Peter Dela Cruz
"""


def next_higher(value: int):
    """
    This function returns the next higher number greater than :attr:`value` with the same number of
    bits set, i.e. same number of 1's in the binary representation of :attr:`value`.

    :param value: The input integer
    :return: The next higher number greater than :attr:`value` with the same number of bits set
    """
    num_bits = f"{value:0b}".count("1")
    counter = value + 1
    while True:
        if f"{counter:0b}".count("1") == num_bits:
            return counter
        counter += 1
