"""
Finish Guess the Number Game

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class Guesser:
    def __init__(self, number: int, lives: int):
        self.number = number
        self.lives = lives

    def guess(self, user_guess: int) -> bool:
        if self.lives == 0:
            raise ValueError
        if user_guess == self.number:
            return True
        self.lives -= 1
        return False
