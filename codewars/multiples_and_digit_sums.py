"""
Multiples and Digit Sums

:Author: BJ Peter Dela Cruz
"""


def procedure(number: int) -> int:
    """
    This function finds all the multiples of :attr:`number` up to and including 100. Then for each
    multiple, it calculates the digit sum, e.g. 45 -> 4 + 5 = 9. Finally, it sums all the digit sums
    together and returns the result.

    :param number: The input integer
    :return: The result of summing all the digit sums together
    """
    return sum(sum(map(int, str(current_number))) for current_number in range(number, 101)
               if current_number % number == 0)
