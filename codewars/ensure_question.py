"""
Ensure Question

:Author: BJ Peter Dela Cruz
"""


def ensure_question(string: str) -> str:
    """
    This function adds a question mark to the end of the given string if one does not already exist.

    :param string: The input string
    :return: A string that contains a question mark at the end of it
    """
    return '?' if not string else string if string[-1] == '?' else f'{string}?'
