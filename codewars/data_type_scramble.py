"""
Data Type Scramble

:Author: BJ Peter Dela Cruz
"""


def make_model_year(data: list) -> dict:
    """
    This function takes all the attributes of a car from the given list and puts them into a
    dictionary. Each attribute has a different data type.

    :param data: The input list of attributes of a car
    :return: A dictionary that contains the same attributes
    """
    car_description = {'make': '', 'model': '', 'year': 0, 'new': False}
    for item in data:
        # bool is a subclass of int for historical reasons, so perform this comparison first
        if isinstance(item, bool):
            car_description['new'] = item
        elif isinstance(item, int):
            car_description['year'] = item
        elif isinstance(item, tuple):
            car_description['model'] = f'{item[0]} {item[1]}'
        else:
            car_description['make'] = item
    return car_description
