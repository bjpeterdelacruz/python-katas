"""
Exclamation Marks Series #12: Remove Odd Number Continuous Exclamation Marks and Question Marks

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def remove(string: str) -> str:
    """
    This function removes groups that contain an odd number of continuous exclamation marks or
    question marks. Groups are removed from left to right. After the rightmost group is removed, the
    process starts over from the beginning of the new string and is repeated until there are no more
    groups. A group must contain three or more characters; a single character does not count.

    :param string: The input string that contains only exclamation marks and/or question marks
    :return: A string without groups that contain an odd number of continuous exclamation marks or
        question marks
    """
    has_odd = True
    while has_odd:
        temp = []
        has_odd = False
        for key, group in groupby(string):
            length = len(list(group))
            if length == 1 or length % 2 == 0:
                temp.append(key * length)
            else:
                has_odd = True
        string = ''.join(temp)
    return string
