"""
GA-DE-RY-PO-LU-KI Cypher

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase

KEY = {'G': 'A', 'A': 'G',
       'D': 'E', 'E': 'D',
       'R': 'Y', 'Y': 'R',
       'P': 'O', 'O': 'P',
       'L': 'U', 'U': 'L',
       'K': 'I', 'I': 'K'}


def substitute(message: str) -> str:
    """
    This helper function replaces 'G', 'D', 'R', 'P', 'L', and 'K' in :attr:`message` with 'A', 'E',
    'Y', 'O', 'U', and 'I', respectively, and vice versa. All other letters are left in place in
    :attr:`message`.

    :param message: A string
    :return: A string with the aforementioned letters replaced with their respective replacements
    """
    new_message = []
    for letter in message:
        if letter.upper() in KEY:
            if letter in ascii_lowercase:
                new_message.append(KEY[letter.upper()].lower())
            else:
                new_message.append(KEY[letter])
        else:
            new_message.append(letter)
    return ''.join(new_message)


def encode(message: str) -> str:
    """
    This function encodes the given message using a key. The encoding process is delegated to the
    :attr:`substitute` function.

    :param message: The input string to encode
    :return: An encoded string
    """
    return substitute(message)


def decode(message: str) -> str:
    """
    This function decodes the given message using a key. The decoding process is delegated to the
    :attr:`substitute` function.

    :param message: The input string to decode
    :return: A decoded string
    """
    return substitute(message)
