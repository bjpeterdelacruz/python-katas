"""
How Many Urinals Are Free

:Author: BJ Peter Dela Cruz
"""


def get_free_urinals(urinals: str) -> int:
    """
    This function counts the number of urinals that can be used based on an unspoken rule that two
    urinals side by side cannot be occupied at the same time. A urinal can be used only if each side
    of it is unoccupied. An occupied urinal is represented by 1 in the binary string, and an
    unoccupied urinal is represented by 0. An occupied urinal cannot have another occupied urinal
    next to it, e.g. "11". If this case is found, -1 is returned.

    :param urinals: The input string, a binary string representing urinals that are occupied and
        unoccupied
    :return: The number of urinals that can be used based on the unspoken rule, or -1 if two urinals
        are occupied and next to each other
    """
    if "11" in urinals:
        return -1
    if len(urinals) == 1:
        return 1 if urinals == "0" else 0
    arr = list(urinals)
    count = 0
    for idx, char in enumerate(arr):
        if char == "1":
            continue
        if idx == 0 and arr[1] == "0":
            count += 1
            arr[idx] = "1"
        elif idx == len(arr) - 1 and arr[-2] == "0":
            count += 1
        elif arr[idx - 1] == "0" and arr[idx + 1] == "0":
            count += 1
            arr[idx] = "1"
    return count
