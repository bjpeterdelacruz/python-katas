"""
Letters Only, Please!

:Author: BJ Peter Dela Cruz
"""
from string import ascii_letters


def remove_chars(string: str) -> str:
    """
    This function filters out all characters in :attr:`string` except for letters (both lowercase
    and uppercase) and spaces without using regular expressions.

    :param string: The input string
    :return: A string that only contains letters and spaces
    """
    return ''.join(char for char in string if char in f'{ascii_letters} ')
