"""
Sort Deck of Cards

:Author: BJ Peter Dela Cruz
"""


def sort_cards(cards: list[str]) -> list[str]:
    """
    This function sorts the given list of cards in ascending order by rank. Ace is the lowest card,
    and King is the highest card. The list may contain one or more cards of the same rank, for
    example, three Aces.

    :param cards: The input list of strings representing card ranks from ace (1) to king (13)
    :return: A sorted list of cards in ascending order by rank
    """
    ranks = {'A': 1, 'T': 10, 'J': 11, 'Q': 12, 'K': 13,
             1: 'A', 10: 'T', 11: 'J', 12: 'Q', 13: 'K'}
    cards_list = [ranks[card] if card in ranks else int(card) for card in cards]
    return [ranks[card] if card in ranks else str(card) for card in sorted(cards_list)]
