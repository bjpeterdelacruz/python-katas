"""
Positive to Negative Binary Numbers

:Author: BJ Peter Dela Cruz
"""


def positive_to_negative(binary: list[int]) -> list[int]:
    """
    This function calculates the two's complement of the given integer, which is represented as a
    list of zeros and ones. Each bit in the list is flipped (zero to one, or one to zero), and then
    one is added to the result. The output list, which is the same length as the input list, is
    returned.

    :param binary: The input list representing an integer in binary format
    :return: The two's complement of the integer
    """
    number = str(bin(int(''.join('0' if str(bit) == '1' else '1' for bit in binary), 2) + 1))[2:]
    return [int(bit) for bit in number[::-1][:len(binary)][::-1]]
