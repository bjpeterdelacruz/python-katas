"""
Calculate Derivatives

:Author: BJ Peter Dela Cruz
"""


def get_derivative(string: str) -> str:
    """
    This function calculates the derivative of the given equation that is of the form ax^b.

    :param string: The input string representing an equation
    :return: The derivative of the equation
    """
    if "^" in string:
        numbers = list(map(int, string.split("x^")))
        coefficient, exponent = numbers[0] * numbers[1], numbers[1] - 1
        return f"{coefficient}x" if exponent == 1 else f"{coefficient}x^{exponent}"
    return string.split("x")[0] if "x" in string else "0"
