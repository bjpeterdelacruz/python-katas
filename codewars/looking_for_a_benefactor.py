"""
Looking for a Benefactor

:Author: BJ Peter Dela Cruz
"""
from math import ceil


def new_avg(arr: list, new_average: int) -> float:
    """
    This function returns a number that after it is summed with the numbers in :attr:`arr`, the
    calculated average will be equal to :attr:`new_average`. If the number is less than or equal to
    0, a ValueError will be raised. The return value is rounded up to the nearest integer.

    :param arr: The input list of numbers
    :param new_average: The average
    :return: The number that needs to be added to all the numbers in :attr:`arr` to generate an
        average value equal to :attr:`new_average`
    """
    result = ceil(new_average * (len(arr) + 1) - sum(arr))
    if result <= 0:
        raise ValueError
    return result
