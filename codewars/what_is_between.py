"""
What is Between?

:Author: BJ Peter Dela Cruz
"""


def between(start: int, end: int) -> list[int]:
    """
    This function returns a list that contains all the integers between :attr:`start` (inclusive)
    and :attr:`end` (inclusive).

    :param start: The first integer in the list of integers
    :param end: The last integer in the list of integers
    :return: A list of integers from :attr:`start` to :attr:`end`
    """
    return list(range(start, end + 1))
