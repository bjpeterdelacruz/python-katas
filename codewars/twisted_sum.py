"""
Twisted Sum

:Author: BJ Peter Dela Cruz
"""


def compute_sum(limit: int):
    """
    This function sums all the digits in each number from 1 to :attr:`limit`, inclusive. For
    example, given 10, the result will be 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + (1 + 0) = 46.

    :param limit: The input integer
    :return: The sum of all the digits in each number from 1 to :attr:`limit`, inclusive
    """
    return sum(int(character) for curr_num in range(1, limit + 1) for character in str(curr_num))
