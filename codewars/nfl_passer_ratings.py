"""
NFL Passer Ratings

:Author: BJ Peter Dela Cruz
"""
MAXIMUM = 2.375


def passer_rating(attempts: int, yards: int, completions: int, touchdowns: int, interceptions: int)\
        -> float:
    """
    This function calculates an NFL player's passer rating using the number of passing attempts,
    passing yards, completions, touchdown passes, and interceptions.

    :param attempts: Number of passing attempts
    :param yards: Number of passing yards
    :param completions: Number of completions
    :param touchdowns: Number of touchdown passes
    :param interceptions: Number of interceptions
    :return: The NFL player's passer rating
    """
    a_calc = calculate(((completions / attempts) - 0.3) * 5)
    b_calc = calculate(((yards / attempts) - 3) * 0.25)
    c_calc = calculate((touchdowns / attempts) * 20)
    d_calc = calculate(MAXIMUM - ((interceptions / attempts) * 25))
    result = ((a_calc + b_calc + c_calc + d_calc) / 6) * 100
    return round(result * 10) / 10


def calculate(value: float) -> int:
    """
    This helper function returns 0 if value is negative, 2.375 (MAXIMUM) if value is greater than
    MAXIMUM, or value.

    :param value: A calculated value
    :return: 0 if value is negative, 2.375 (MAXIMUM) if value is greater than MAXIMUM, or value
    """
    return 0 if value < 0 else MAXIMUM if value > MAXIMUM else value
