"""
E-mail Address Obfuscator

:Author: BJ Peter Dela Cruz
"""


def obfuscate(email: str) -> str:
    """
    This function replaces the @ symbol in the given e-mail address with " [at] " and each period
    with " [dot] ". There is one space before each opening bracket and one space after each closing
    bracket in the obfuscated e-mail address.

    :param email: The input string, the e-mail address to obfuscate
    :return: An obfuscated e-mail address
    """
    email = ' [at] '.join(email.split('@'))
    return ' [dot] '.join(email.split('.'))
