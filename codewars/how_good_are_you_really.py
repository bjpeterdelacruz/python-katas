"""
How Good Are You Really?

:Author: BJ Peter Dela Cruz
"""


def better_than_average(class_points: list[int], your_points: int) -> bool:
    """
    This function returns True if your score on a test is greater than the class average, which
    includes your score.

    :param class_points: A list of all the scores on the test, excluding :attr:`your_points`
    :param your_points: Your score on the test
    :return: True if your score on the test is better than the class average, False otherwise
    """
    all_points = list(class_points)
    all_points.append(your_points)
    return your_points > sum(all_points) / len(all_points)
