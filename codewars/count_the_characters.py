"""
Count the Characters

:Author: BJ Peter Dela Cruz
"""
from collections import Counter


def count_char(string: str, character: str) -> int:
    """
    This function counts the number of times :attr:`character` appears in :attr:`string`. The count
    is case-insensitive.

    :param string: The input string
    :param character: The character to count in :attr:`string`
    :return: The number of times :attr:`character` appears in :attr:`string`
    """
    return Counter(string.lower())[character.lower()]
