"""
Dollars and Cents

:Author: BJ Peter Dela Cruz
"""


def format_money(amount) -> str:
    """
    This function formats the given number to two decimal places and adds the U.S. dollar symbol ($)
    to the front of the string that is returned.

    :param amount: The number to format
    :return: A formatted string
    """
    return '${:.2f}'.format(amount)  # pylint: disable=consider-using-f-string
