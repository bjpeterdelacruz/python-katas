"""
No Zeros for Heros

:Author: BJ Peter Dela Cruz
"""


def no_boring_zeros(number: int) -> int:
    """
    This function removes all trailing zeros from the given number and returns a new number. If
    :attr:`number` is zero, then zero is returned.

    :param number: The input integer
    :return: An integer with no trailing zeros, or zero if :attr:`number` is zero
    """
    number_str = str(number)
    while number_str[-1] == '0' and len(number_str) > 1:
        number_str = number_str[0:-1]
    return int(number_str)
