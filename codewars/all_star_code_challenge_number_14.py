"""
All Star Code Challenge #14

Author: BJ Peter Dela Cruz
"""


def median(array: list[int]) -> int:
    """
    This function returns the median of the given list of integers. If the list has an even number
    of integers, then the average of the two integers that are in the middle is returned. To find
    the median, the list must be sorted first, but a copy of the list will be made before it is
    sorted, leaving the original list untouched.

    :param array: The input list of integers
    :return: The median of the list
    """
    sorted_array = sorted(array)
    idx = len(sorted_array) // 2
    middle = sorted_array[idx]
    return middle if len(sorted_array) % 2 == 1 else (sorted_array[idx - 1] + middle) / 2
