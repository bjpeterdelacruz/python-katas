"""
How Old Will I be in 2099

:Author: BJ Peter Dela Cruz
"""


def calculate_age(year_of_birth: int, current_year: int) -> str:
    """
    This function returns Philip's age given his birth year and the current year (if the current
    year is greater than his birth year) or the number of years until he would be born (if his
    birth year is greater than the current year). If both years are the same, the string
    "You were born this very year!" is returned.

    :param year_of_birth: Philip's birth year
    :param current_year: The current year
    :return: e.g. "You are 4 years old.", "You will be born in 10 years.", or
        "You were born this very year!"
    """
    diff = abs(year_of_birth - current_year)
    word = "year" if diff == 1 else "years"
    if current_year < year_of_birth:
        return f"You will be born in {diff} {word}."
    if current_year > year_of_birth:
        return f"You are {diff} {word} old."
    return "You were born this very year!"
