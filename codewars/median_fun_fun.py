"""
Median Fun Fun

:Author: BJ Peter Dela Cruz
"""


def median(arr: list[int]) -> int | float | None:
    """
    This function returns the median of the given list of integers.

    :param arr: The input list of integers
    :return: The median number, may be an integer or a float (if the length of the list is even)
    """
    if not arr:
        return None
    sorted_list = sorted(arr)
    length = len(sorted_list)
    if length % 2 == 0:
        first, second = (length // 2) - 1, (length // 2)
        return (sorted_list[first] + sorted_list[second]) / 2
    return sorted_list[length // 2]
