"""
Make Identical

:Author: BJ Peter Dela Cruz
"""
from sys import intern


def make_identical(string: str) -> str:
    """
    This function interns the given string, i.e. adds it to the global table of interned strings,
    similar to how strings are interned in Java.

    :param string: The string to intern
    :return: The string itself or the previously interned string
    """
    return intern(string)
