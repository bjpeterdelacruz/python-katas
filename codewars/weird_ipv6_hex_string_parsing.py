"""
Weird IPv6 Hex String Parsing

:Author: BJ Peter Dela Cruz
"""


def parse_ipv6(ipv6: str) -> str:
    """
    This function separates each octet in the given IPv6 string, sums all the digits in each octet,
    and concatenates all the sums together.

    :param ipv6: The input string, a valid IPv6 address
    :return: The sum of each octet concatenated together into a single string
    """
    hex_chars = '0123456789abcdefABCDEF'
    characters = []
    results = []
    for letter in ipv6:
        if letter in hex_chars:
            characters.append(letter)
        else:
            results.append(str(sum(int(character, 16) for character in characters)))
            characters = []
    if characters:
        results.append(str(sum(int(character, 16) for character in characters)))
    return ''.join(results)
