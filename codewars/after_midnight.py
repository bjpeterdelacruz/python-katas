"""
After Midnight

:Author: BJ Peter Dela Cruz
"""
from datetime import datetime, timedelta


def day_and_time(minutes: int) -> str:
    """
    This function returns the day and time of the week given the number of minutes before or after
    Sunday midnight.

    :param minutes: The input integer
    :return: The day and time of the week
    """
    new_date = datetime(2021, 1, 3, 0, 0, 0, 0) + timedelta(minutes=minutes)
    return new_date.strftime('%A %H:%M')
