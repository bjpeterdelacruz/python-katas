"""
Linked Lists: Insert Sort

:Author: BJ Peter Dela Cruz
"""
from utils.linked_list import Node
from codewars.linked_lists_sorted_insert import sorted_insert


def insert_sort(head):
    """
    This function creates a new linked list from the given linked list such that the former will be
    in sorted order when it is returned. If :attr:`head` is None, None is returned.

    :param head: The linked list
    :return: The same linked list but with the elements in sorted order
    """
    if not head:
        return None
    if not head.next:
        return head
    new_ll = Node(head.data)
    next_node = head.next
    while next_node:
        new_ll = sorted_insert(new_ll, next_node.data)
        next_node = next_node.next
    return new_ll
