"""
Decreasing Inputs

:Author: BJ Peter Dela Cruz
"""


def add(*args: int) -> int:
    """
    This function divides each integer argument by its position in the argument list--index 0 is
    position 1, index 1 is position 2, etc.--and then returns a sum of all the results. The sum is
    rounded to the nearest integer.

    :param args: A variable number of integer arguments
    :return: A sum that is rounded to the nearest integer
    """
    return round(sum(value / (idx + 1) for idx, value in enumerate(args)))
