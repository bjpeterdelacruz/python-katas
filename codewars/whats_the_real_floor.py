"""
What is the Real Floor?

:Author: BJ Peter Dela Cruz
"""


def get_real_floor(floor: int) -> int:
    """
    This function returns the real floor of a building. In the United States, the first floor is
    actually the ground floor, and there is no thirteenth floor. If :attr:`floor` is between 1 and
    12, inclusive, then :attr:`floor` - 1 is returned. If :attr:`floor` is greater than 13, then
    :attr:`floor` - 2 is returned. Basements (negatives) stay the same.

    :param floor: The input integer representing the floor of a building in the United States
    :return: The real floor of the building
    """
    if floor <= 0:
        return floor
    if floor > 13:
        return floor - 2
    return floor - 1
