"""
Search for Letters

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase


def change(string: str) -> str:
    """
    This function returns a binary string of length 26, each position representing an ASCII letter
    from A to Z. If an ASCII letter appears in :attr:`string`, the bit in the binary string that
    corresponds to the letter will be 1. Otherwise, the bit will be 0.

    :param string: The input string
    :return: A binary string of length 26 representing the ASCII letters that are present in
        :attr:`string`
    """
    return ''.join(['1' if char in string.lower() else '0' for char in ascii_lowercase])
