"""
Finding an Appointment

:Author: BJ Peter Dela Cruz
"""
from datetime import datetime, timedelta


def get_all_available_time_slots(schedules: list[list[list[str]]], duration):
    """
    This helper function gathers all the available time slots from the given list of schedules.

    :param schedules: The input 3-dimensional list of strings that represent business people's
        schedules for the day
    :param duration: The input integer representing the appointment duration in minutes
    :return: A list of all the available start and end times for each business person
    """
    start_time = datetime.strptime("09:00", "%H:%M")
    end_time = datetime.strptime("19:00", "%H:%M")
    all_available_time_slots = []
    for schedule in schedules:
        if not schedule:
            return None
        times = []
        available_times = []
        for appointment in schedule:
            times.append(datetime.strptime(appointment[0], "%H:%M"))
            times.append(datetime.strptime(appointment[1], "%H:%M"))
        if times[0] != start_time:
            available_times.append((start_time, times[0]))
        times.pop(0)
        while times:
            end = times.pop(0)
            if not times:
                if end != end_time:
                    available_times.append((end, end_time))
                break
            start = times.pop(0)
            if start != end:
                available_times.append((end, start))
        all_available_time_slots.append(available_times)

    # Remove all available time slots that are shorter than the appointment duration
    for available_times in all_available_time_slots:
        for current in available_times:
            diff = current[1] - current[0]
            minutes = diff.seconds // 60
            if duration > minutes:
                available_times.remove(current)
    return all_available_time_slots


def get_start_time(schedules: list[list[list[str]]], duration: int):
    """
    This function returns the earliest appointment start time that everyone in the given list can
    attend. If at least one person cannot attend, then None is returned.

    :param schedules: The input 3-dimensional list of strings that represent business people's
        schedules for the day
    :param duration: The input integer representing the appointment duration in minutes
    :return: The earliest appointment start time, or None if the appointment cannot fit in
        everyone's schedule
    """
    all_available_time_slots = get_all_available_time_slots(schedules, duration)
    if not all_available_time_slots:
        return None

    # Gather all the possible time slots for the appointment. For example, if a person is available
    # from 12:00 to 14:00 and the appointment duration is 1 hour, then the available time slots for
    # that person are 12:00 to 13:00 and 13:00 to 14:00.
    possible_time_slots = []
    for available_times in all_available_time_slots:
        for available_between in available_times:
            end = available_between[0] + timedelta(minutes=duration)
            possible_time_slots.append((available_between[0], end))
            while end + timedelta(minutes=duration) <= available_between[1]:
                possible_time_slots.append((end, end + timedelta(minutes=duration)))
                end += timedelta(minutes=duration)

    appointments = []
    schedule = []
    # Gather all the possible time slots that fit in each business person's schedule.
    for available_time_slots in all_available_time_slots:
        for available_between in available_time_slots:
            for time_slot in possible_time_slots:
                if time_slot[0] >= available_between[0] and time_slot[1] <= available_between[1]:
                    schedule.append(time_slot)
        appointments.append(schedule)
        schedule = []

    appointment_time_slots = set(appointments[0])
    # Find all the time slots that fit everyone's schedule
    for idx in range(1, len(appointments)):
        appointment_time_slots = appointment_time_slots.intersection(appointments[idx])
    if not appointment_time_slots:
        return None
    # Get the earliest available appointment start time
    appointment_time_slots = sorted(list(appointment_time_slots), key=lambda time: time[0])
    return appointment_time_slots[0][0].strftime("%H:%M")
