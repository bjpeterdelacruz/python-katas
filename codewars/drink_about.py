"""
Drink About

:Author: BJ Peter Dela Cruz
"""


def people_with_age_drink(age: int) -> str:
    """
    This function returns a string that informs the user what people should drink given their age.

    :param age: The input integer, the age of a person
    :return: The type of drink that the person should drink
    """
    if age < 14:
        return "drink toddy"
    if age < 18:
        return "drink coke"
    return "drink beer" if age < 21 else "drink whisky"
