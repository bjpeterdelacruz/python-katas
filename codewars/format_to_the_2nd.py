def print_nums(*numbers: int) -> str:
    if not numbers:
        return ''
    maximum = len(str(max(numbers)))
    strings = []
    for number in numbers:
        temp = str(number)
        while len(temp) < maximum:
            temp = '0' + temp
        strings.append(temp)
    return '\n'.join(strings)
