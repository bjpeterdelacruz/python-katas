"""
Simple Sentences

:Author: BJ Peter Dela Cruz
"""


def make_sentences(parts: list[str]) -> str:
    """
    This function creates a sentence from the given list of strings. The list may contain zero, one,
    or more periods at the end of it, but the sentence will have one and only one period. Also,
    there will only be a space to the right of a comma but not to the left of it.

    :param parts: The input list of strings
    :return: A string representing a sentence with one or more commas but only one period at the end
    """
    idx = 0
    sentence = []
    while idx < len(parts):
        if idx + 1 < len(parts):
            if parts[idx + 1] == ",":
                sentence.append(f"{parts[idx]},")
                idx += 1
            elif parts[idx + 1] == ".":
                sentence.append(f"{parts[idx]}.")
                break
            else:
                sentence.append(parts[idx])
        else:
            sentence.append(parts[idx])
        idx += 1
    sentence = ' '.join(sentence)
    return sentence if sentence.endswith(".") else f"{sentence}."
