"""
Thinking and Testing #01: A and B?

:Author: BJ Peter Dela Cruz
"""


def a_and_b(value1: int, value2: int) -> int:
    """
    This function performs a bitwise OR operation on the given integers.

    :param value1: The input integer
    :param value2: The input integer
    :return: The result of the bitwise OR operation on both :attr:`value1` and :attr:`value2`
    """
    return value1 | value2
