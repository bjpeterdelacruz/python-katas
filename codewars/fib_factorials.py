"""
Fibonacci Factorials

:Author: BJ Peter Dela Cruz
"""
from math import factorial


def sum_fib(number: int) -> int:
    """
    This function generates :attr:`number` Fibonacci numbers and then returns the sum of the
    factorial of each Fibonacci number.

    :param number: The input integer, the number of Fibonacci numbers to generate
    :return: The sum of the factorial of each Fibonacci number
    """
    fibs = [0, 1]
    while len(fibs) != number:
        fibs.append(fibs[-2] + fibs[-1])
    return sum(factorial(num) for num in fibs)
