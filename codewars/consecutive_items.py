"""
Consecutive Items

:Author: BJ Peter Dela Cruz
"""


def consecutive(arr: list[int], element_a: int, element_b: int) -> bool:
    """
    This function returns True if both elements are consecutive in the given list. In other words,
    one of the elements is at position i in the list, and the other is at position i + 1.

    :param arr: The input list that contains :attr:`element_a` and :attr:`element_b`
    :param element_a: An element in :attr:`arr`
    :param element_b: Another element in :attr:`arr`
    :return: True if the two elements are consecutive in :attr:`arr`, False otherwise
    """
    return element_a in arr and element_b in arr and\
        (arr.index(element_a) == arr.index(element_b) + 1 or
            arr.index(element_a) + 1 == arr.index(element_b))
