"""
Duplicates. Duplicates Everywhere

:Author: BJ Peter Dela Cruz
"""


def remove_duplicate_ids(objects: dict[str, list[str]]):
    """
    This function removes duplicates in each list of the given map. If a duplicate string appears in
    the same list, only the first instance is kept. If a duplicate string appears in two or more
    lists, only the string that is in the list belonging to a key that is the highest integer value
    is kept.

    :param objects: A dict of string keys with list of strings as values
    :return: A dict with all duplicate strings removed in each list
    """
    new_objects = {}
    # first remove duplicates from each list
    for key, value in objects.items():
        new_objects[int(key)] = []
        for val in value:
            if val not in new_objects[int(key)]:
                new_objects[int(key)].append(val)
    sorted_keys = reversed(sorted(new_objects.keys()))
    letters = set()
    # then remove duplicates if they appear across lists
    for key in sorted_keys:
        idx = 0
        values = new_objects[key]
        while idx < len(values):
            if values[idx] in letters:
                values.pop(idx)
                idx -= 1
            else:
                letters.add(values[idx])
            idx += 1
    return {str(key): value for key, value in new_objects.items()}
