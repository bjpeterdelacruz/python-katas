"""
English Beggars

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict


def beggars(values: list[int], num_beggars: int) -> list[int]:
    """
    This function returns an array with the sum of what each beggar brings home, assuming they take
    regular turns, from the first to the last. For example, given [1, 2, 3, 4, 5] and three beggars,
    the first beggar takes home [1, 4], the second beggar takes home [2, 5], and the last beggar
    takes home [3], so the result will be [5, 7, 3].

    :param values: The input list of integers
    :param num_beggars: The number of beggars
    :return: The sum of what each beggar takes home
    """
    if num_beggars == 0:
        return []
    if num_beggars == 1:
        return [sum(values)]
    all_beggars = defaultdict(list)
    current_beggar = 1
    for value in values:
        all_beggars[current_beggar].append(value)
        current_beggar += 1
        if current_beggar > num_beggars:
            current_beggar = 1
    totals = [sum(values) for values in all_beggars.values()]
    while len(totals) < num_beggars:
        totals.append(0)
    return totals
