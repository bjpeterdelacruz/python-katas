"""
Which String is Worth More?

:Author: BJ Peter Dela Cruz
"""


def highest_value(string_a: str, string_b: str) -> str:
    """
    This function calculates the sum of each given string's ASCII codepoint indexes and returns the
    string that has the greatest sum. If both sums are equal, the first string is returned.

    :param string_a: The input string
    :param string_b: The input string
    :return: The first string if its sum is greater than or equal to that of the second string, else
        the second string
    """
    sum_a = sum(ord(char) for char in string_a)
    sum_b = sum(ord(char) for char in string_b)
    return string_a if sum_a >= sum_b else string_b
