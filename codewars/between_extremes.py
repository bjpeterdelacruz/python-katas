"""
Between Extremes

:Author: BJ Peter Dela Cruz
"""


def between_extremes(numbers: list[int]) -> int:
    """
    This function returns the difference between the maximum element in the list of integers and the
    minimum element.

    :param numbers: The input list of integers
    :return: The difference between the maximum and minimum elements in the list
    """
    return max(numbers) - min(numbers)
