"""
Reverse List Order

:Author: BJ Peter Dela Cruz
"""


def reverse_list(lst: list) -> list:
    """
    This function returns a new list that consists of the elements in :attr:`lst` in reversed order.

    :param lst: The input list
    :return: A new list consisting of the elements in :attr:`lst` in reversed order
    """
    return list(reversed(lst))
