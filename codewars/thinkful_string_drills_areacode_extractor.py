"""
Thinkful: Area Codes

:Author: BJ Peter Dela Cruz
"""


def area_code(text: str) -> str:
    """
    This function returns the area code for a phone number that is found in the given string. The
    area code is found between the open and close parentheses.

    :param text: The input string
    :return: The area code
    """
    return text[text.index('(') + 1:text.index(')')]
