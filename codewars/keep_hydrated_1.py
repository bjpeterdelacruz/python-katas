"""
Keep Hydrated!

:Author: BJ Peter Dela Cruz
"""
from math import floor


def litres(time) -> int:
    """
    This function returns the amount of water that Nathan drinks while going cycling. He drinks a
    half a liter of water per hour of cycling. The amount is rounded down to the smallest integer.

    :param time: The number of hours Nathan spends cycling
    :return: The amount of water that he drinks in liters
    """
    return floor(time * 0.5)
