"""
The Feast of Many Beasts

:Author: BJ Peter Dela Cruz
"""


def feast(beast: str, dish: str) -> bool:
    """
    This function returns True if the first letter of :attr:`dish` and the first letter of
    :attr:`beast` match and the last letter of :attr:`dish` and the last letter of :attr:`beast`
    also match.

    :param beast: The first input string, an animal
    :param dish: The second input string, a food item
    :return: True if the first letters match and the last letters match, False otherwise
    """
    return dish[0] == beast[0] and dish[-1] == beast[-1]
