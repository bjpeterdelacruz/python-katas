"""
Numbers to Objects

:Author: BJ Peter Dela Cruz
"""


def num_obj(numbers: list[int]) -> list[dict]:
    """
    This function converts the given list of integers (ASCII codes) into a list of dictionaries. The
    key will be an integer from the list as a string, and the value will be the character
    represented by the ASCII code. All integers in :attr:`numbers` are valid ASCII codes for
    lowercase characters.

    :param numbers: The input list of integers
    :return: A list of dictionaries
    """
    return [{str(number): chr(number)} for number in numbers]
