"""
Milk and Cookies for Santa

:Author: BJ Peter Dela Cruz
"""
from datetime import date


def time_for_milk_and_cookies(today_dt: date) -> bool:
    """
    This function returns True if the given date is Christmas Eve, i.e. December 24.

    :param today_dt: The input date
    :return: True if the given date is Christmas Eve, False otherwise
    """
    return today_dt.month == 12 and today_dt.day == 24
