"""
Ragbaby Cipher

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase, ascii_uppercase


def generate_key(key: str) -> str:
    """
    This helper function generates the key used to encode or decode a string. For example, if
     :attr:`key` is "cipher", this function will generate the key "cipherabdfgjklmnoqstuvwxyz".

    :param key: The key used to generate the complete key
    :return: The key used for encoding or decoding a string
    """
    cipher = []
    for char in key:
        if char not in cipher:
            cipher.append(char)
    for char in ascii_lowercase:
        if char not in cipher:
            cipher.append(char)
    return ''.join(cipher)


def encode(text: str, key: str) -> str:
    """
    This helper function simply calls :func:`change_text` to encode the given :attr:`text`.

    :param text: The text to encode
    :param key: The key used to generate the complete key
    :return: An encoded string
    """
    return change_text(text, key, True)


def decode(text: str, key: str) -> str:
    """
    This helper function simply calls :func:`change_text` to decode the given :attr:`text`.

    :param text: The text to decode
    :param key: The key used to generate the complete key
    :return: A decoded string
    """
    return change_text(text, key, False)


def change_text(text: str, key: str, do_encode: bool) -> str:
    """
    This function either encodes or decodes the given :attr:`text` using the Ragbaby Cipher. The
    positions of the characters in the key that is generated using :attr:`key` are used for the
    encoding and decoding processes.

    :param text: The input string, the text to either encode or decode
    :param key: The key used to generate the complete key
    :param do_encode: True to encode, False to decode
    :return: An encoded or decoded string
    """
    cipher = generate_key(key)
    encoded_text = []
    idx = 1
    for string in text:
        if string.isalpha():
            char_index = cipher.index(string.lower())
            if do_encode:
                char_index += idx
            else:
                char_index -= idx
            char = cipher[char_index % len(ascii_lowercase)]
            if string in ascii_uppercase:
                char = char.upper()
            encoded_text.append(char)
            idx += 1
        else:
            encoded_text.append(string)
            idx = 1
    return ''.join(encoded_text)
