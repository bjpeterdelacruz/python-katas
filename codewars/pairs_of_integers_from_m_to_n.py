def generate_pairs(start: int, end: int) -> list[tuple[int, int]]:
    return [(first, second) for first in range(start, end + 1) for second in range(first, end + 1)]
