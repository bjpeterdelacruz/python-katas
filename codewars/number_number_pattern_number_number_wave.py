"""
■□ Pattern □■ : Wave

:Author: BJ Peter Dela Cruz
"""


def draw(waves: list[int]) -> str:
    """
    This function returns a string that represents a histogram that contains sound waves. Each ■
    character represents a sound wave, and each □ character represents an empty spot in the
    histogram.

    :param waves: The input list of integers for the histogram
    :return: A histogram that contains sound waves
    """
    sound_waves = []
    for current_row in range(max(waves), 0, -1):
        row = []
        for idx, wave in enumerate(waves):
            if wave == current_row:
                row.append("■")
                waves[idx] -= 1
            else:
                row.append("□")
        sound_waves.append(''.join(row))
    return '\n'.join(sound_waves)
