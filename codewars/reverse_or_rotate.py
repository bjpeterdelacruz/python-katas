"""
Reverse or Rotate?

Author: BJ Peter Dela Cruz
"""
from collections import deque


def revrot(string: str, size: int) -> str:
    """
    This function divides a string that represents an integer into chunks of size :attr:`size`. If
    the sum of cube of each digit in a chunk is an even number, the chunk will be reversed.
    Otherwise, the chunk will be rotated to the left one digit. If the last chunk is not of size
    :attr:`size`, it is ignored. If :attr:`size` is zero or greater than the length of the string,
    or the string is empty or None, an empty string is returned. Finally, the chunks are
    concatenated together into a single string.

    :param string: The input string representing an integer
    :param size: The input integer, the size of each chunk
    :return: A concatenated string with each chunk either reversed or rotated
    """
    if size <= 0 or size > len(string) or not string:
        return ''
    chunks = []
    current_chunk = []
    for char in string:
        current_chunk.append(char)
        if len(current_chunk) == size:
            chunks.append(''.join(current_chunk))
            current_chunk = []
    result = []
    for chunk in chunks:
        total = sum(int(char) ** 3 for char in chunk)
        if total % 2 == 0:
            result.append(''.join(list(reversed(chunk))))
        else:
            temp = deque(chunk)
            temp.rotate(-1)
            result.append(''.join(temp))
    return ''.join(result)
