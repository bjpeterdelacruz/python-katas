"""
Homogenous Arrays

:Author: BJ Peter Dela Cruz
"""


def filter_homogenous(arrays: list[list]) -> list[list]:
    """
    This function returns a list of all the lists from the given 2-dimensional list that contain
    only one type, i.e. a list of integers but not a list that contains both characters and
    integers. Empty lists are filtered out and not included in the 2-dimensional list that is
    returned.

    :param arrays: The input 2-dimensional list of lists
    :return: A 2-dimensional list containing all lists that contain only one type
    """
    result = []
    for array in arrays:
        types = {type(element) for element in array}
        if len(types) == 1:
            result.append(array)
    return result
