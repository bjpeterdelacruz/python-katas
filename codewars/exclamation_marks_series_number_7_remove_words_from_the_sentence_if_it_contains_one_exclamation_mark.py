"""
Exclamation Marks Series #7: Remove Words from the Sentence if It Contains One Exclamation Mark

:Author: BJ Peter Dela Cruz
"""


def remove(string: str) -> str:
    """
    This function removes all words from :attr:`string` that have exactly one exclamation mark in
    them.

    :param string: The input string
    :return: A string without words that contain exactly one exclamation mark.
    """
    return ' '.join(word for word in string.split() if word.count('!') != 1)
