"""
Linked Lists: Length and Count

:Author: BJ Peter Dela Cruz
"""
from utils.linked_list import Node


def length(node: Node) -> int:
    """
    This function returns the length of the given linked list, i.e. the number of nodes in it.

    :param node: The linked list
    :return: The length or number of nodes in the linked list
    """
    if not node:
        return 0
    counter = 1
    while node.next:
        counter += 1
        node = node.next
    return counter


def count(node: Node, data) -> int:
    """
    This function counts the number of nodes that contain the given data.

    :param node: The linked list
    :param data: The data that is in the linked list
    :return: The number of nodes that contain :attr:`data`
    """
    if not node:
        return 0
    counter = 0
    while True:
        if node.data == data:
            counter += 1
        node = node.next
        if not node:
            break
    return counter
