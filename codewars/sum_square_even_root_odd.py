"""
Sum: Square Even, Root Odd

:Author: BJ Peter Dela Cruz
"""


def sum_square_even_root_odd(nums: list[int]) -> float:
    """
    This function squares all even numbers in the given list and takes the square root of all odd
    numbers, sums the results together, and then rounds the sum to two decimal places.

    :param nums: The input list of integers
    :return: The sum rounded to two decimal places
    """
    return round(sum(map(lambda x: x ** 2 if x % 2 == 0 else x ** 0.5, nums)), 2)
