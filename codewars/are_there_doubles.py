"""
Are There Doubles?

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def double_check(string: str) -> bool:
    """
    This function checks whether there are characters in :attr:`string` that appear two or more
    times side by side. For example, given "aabc", True is returned, and given "abca", False is
    returned.

    :param string: The input string
    :return: True if there are characters in the string that appear two or more times side by side,
        False otherwise
    """
    for _, value in groupby(string.lower(), lambda x: x):
        if len(list(value)) >= 2:
            return True
    return False
