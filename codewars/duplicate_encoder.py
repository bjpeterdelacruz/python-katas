"""
Duplicate Encoder

:Author: BJ Peter Dela Cruz
"""


def duplicate_encode(word: str) -> str:
    """
    This function encodes the given string. If a letter in the string appears only once, an open
    parenthesis is added to the output string. If a letter in the string appears more than once, a
    closed parenthesis is added to the same output string, which is as long as the input string.

    :param word: The input string to encode
    :return: An encoded string that uses parentheses to represent the number of times a character
        appears in :attr:`word`
    """
    word_lower = word.lower()
    counts = {letter: word_lower.count(letter) for letter in word_lower}
    return ''.join(['(' if counts[letter] == 1 else ')' for letter in word_lower])
