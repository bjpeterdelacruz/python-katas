"""
Caesar Encryption

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase as letters


def index(character, key):
    """
    This helper function gets the index of the character to use in the encrypted message given the
    index of :attr:`character` in the alphabet and :attr:`key`, the number of elements to shift
    right.

    :param character: A character from the plaintext message
    :param key: The number of characters to shift right in the English alphabet
    :return: A character to use in the encrypted message
    """
    return (letters.index(character) + key) % len(letters)


def caesar(message, key):
    """
    This function encrypts the given message using the famous Caesar Cipher, a substitution cipher.
    Characters that do not appear in the English alphabet (A-Z) are left as is in the encrypted
    string.

    :param message: The message to encrypt
    :param key: The number of characters to shift right in the English alphabet to encrypt
        :attr:`message`
    :return: An encrypted message
    """
    return ''.join([letters[index(char, key)] if char in letters else char
                    for char in message]).upper()
