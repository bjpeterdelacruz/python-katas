grid = ['top left',    'top middle',    'top right',
        'middle left', 'center',        'middle right',
        'bottom left', 'bottom middle', 'bottom right']


def fire(row: int, column: int) -> str:
    return grid[row + column * 3]
