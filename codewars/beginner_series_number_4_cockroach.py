"""
Beginner Series #4: Cockroach

:Author: BJ Peter Dela Cruz
"""


def cockroach_speed(kph):
    """
    This function converts a cockroach's speed from kilometers per hour to centimeters per second.

    :param kph: The input speed in kilometers per hour
    :return: The same speed in centimeters per second
    """
    return int(kph * 100_000 / 3_600)
