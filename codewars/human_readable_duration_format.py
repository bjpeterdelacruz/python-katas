"""
Human Readable Duration Format

:Author: BJ Peter Dela Cruz
"""


def format_duration(seconds: int) -> str:
    """
    This function converts the time duration :attr:`seconds` into a human-readable format. For
    example, given 31546921, this function will return "1 year, 3 hours, 2 minutes and 1 second". If
    :attr:`seconds` is zero, "now" is returned.

    :param seconds: The input integer representing the number of seconds
    :return: The duration in a human-readable format
    """
    if seconds == 0:
        return "now"
    strings = []
    append_to_list(strings, seconds // (60 * 60 * 24 * 365), "year")
    append_to_list(strings, seconds // (60 * 60 * 24) % 365, "day")
    append_to_list(strings, seconds // (60 * 60) % 24, "hour")
    append_to_list(strings, seconds // 60 % 60, "minute")
    append_to_list(strings, seconds % 60, "second")
    if len(strings) == 1:
        return strings[0]
    strings.insert(len(strings) - 1, "and")
    return ', '.join(strings).replace(", and, ", " and ")


def append_to_list(strings, value, word):
    """
    This helper function appends the correct word (e.g. minute vs minutes) based on :attr:`value` to
    the :attr:`strings` list.

    :param strings: The list of strings
    :param value: The value
    :param word: The word ("year", "day", "hour", "minute", or "second")
    :return: None
    """
    if value == 1:
        strings.append(f"1 {word}")
    elif value > 1:
        strings.append(f"{value} {word}s")
