"""
Order of Weight

:Author: BJ Peter Dela Cruz
"""


def arrange(arr: list[str]) -> list[str]:
    """
    This function sorts the given list of weights expressed as grams (G), kilograms (KG), and
    tonnes (T) from light to heavy. The weights are converted to tonnes only for sorting, so the
    list that is returned retains the original units of measurement.

    :param arr: The input list of strings representing weights
    :return: The same list of strings sorted from light to heavy
    """
    new_arr = []
    for weight in arr:
        if "K" in weight:
            value = int(weight.split("K")[0]) / 1000
        elif "G" in weight:
            value = int(weight.split("G")[0]) / 1_000_000
        else:
            value = int(weight.split("T")[0])
        new_arr.append((weight, value))
    return [weight[0] for weight in sorted(new_arr, key=lambda x: x[1])]
