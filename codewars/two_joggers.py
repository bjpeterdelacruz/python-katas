"""
Two Joggers

:Author: BJ Peter Dela Cruz
"""


def nbr_of_laps(bob, charlie):
    """
    This function returns the number of laps that :attr;`bob` and :attr:`charlie` need to make in
    order to meet each other again at the start line.

    :param bob: The first jogger
    :param charlie: The second jogger
    :return: The number of laps for both joggers
    """
    return count(bob, charlie), count(charlie, bob)


def count(first_jogger, second_jogger):
    """
    This helper function calculates the number of laps that :attr:`first_jogger` needs to make in
    order to meet :attr:`second_jogger` at the start line, i.e. the former is a multiple of the
    latter. For example, given 3 and 5, this function will return 5 because 15 mod 5 equals 0.

    :param first_jogger: The first jogger, an integer that is a multiple of :attr:`second_jogger`
    :param second_jogger: The second jogger
    :return: The number of laps for :attr:`first_jogger`
    """
    counter = 0
    total = first_jogger
    while True:
        counter += 1
        if total % second_jogger == 0:
            break
        total += first_jogger
    return counter
