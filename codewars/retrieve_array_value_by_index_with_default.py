"""
Retrieve Array Value by Index with Default Value

:Author: BJ Peter Dela Cruz
"""


def solution(items, index, default_value):
    """
    This function retrieves a value at the given index in :attr:`items`. If :attr:`index` is out of
    bounds, :attr:`default_value` is returned.

    :param items: The input list
    :param index: The index in the list
    :param default_value: The default value to return if :attr:`index` is out of bounds
    :return: A value in the list if :attr:`index` is valid, or the default value
    """
    try:
        return items[index]
    except IndexError:
        return default_value
