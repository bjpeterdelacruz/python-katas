"""
Bubblesort Once

:Author: BJ Peter Dela Cruz
"""


def bubblesort_once(lst: list[int]) -> list[int]:
    """
    This function sorts the given list of integers using the bubble sort algorithm. However, after
    the first iteration, the partially-sorted list is returned.

    :param lst: The input list of integers
    :return: The list after performing one iteration of the bubble sort algorithm
    """
    if not lst:
        return lst
    copy = lst.copy()
    for idx in range(0, len(copy) - 1):
        if copy[idx] > copy[idx + 1]:
            copy[idx], copy[idx + 1] = copy[idx + 1], copy[idx]
    return copy
