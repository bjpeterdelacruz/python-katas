"""
Your Order, Please

:Author: BJ Peter Dela Cruz
"""
import re


def order(sentence: str) -> str:
    """
    This function returns the words in :attr:`sentence` in sorted ascending order based on the
    numbers in each word in the string.

    :param sentence: The input string that contains words with a one-digit number in each word
    :return: The words sorted in ascending order based on the number in each word
    """
    if sentence == "":
        return ""
    strings = sentence.split()
    tuples = [(int(re.findall("([0-9])", string)[0]), string) for string in strings]
    tuples = sorted(tuples, key=lambda x: x[0])
    return ' '.join([tup[1] for tup in tuples])
