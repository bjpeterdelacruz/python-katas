"""
Word to Binary

:Author: BJ Peter Dela Cruz
"""


def word_to_bin(word: str) -> list[str]:
    """
    This function returns a list of ASCII codes as binary strings, each representing a character in
    :attr:`word`.

    :param word: The input string
    :return: A list of strings, each representing an ASCII code in an 8-digit binary format
    """
    return [f'{ord(char):08b}' for char in word]
