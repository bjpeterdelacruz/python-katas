"""
Formatting Decimal Places #1

:Author: BJ Peter Dela Cruz
"""


def two_decimal_places(number: float) -> float:
    """
    This function formats the given floating point number to two decimal places. The number is not
    rounded.

    :param number: The input floating point number
    :return: A floating point number formatted to two decimal places
    """
    number = str(number)
    return float(number[0:number.index(".") + 3])
