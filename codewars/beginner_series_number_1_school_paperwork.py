"""
Beginner Series #1: School Paperwork

:Author: BJ Peter Dela Cruz
"""


def paperwork(num_classmates: int, num_papers: int) -> int:
    """
    This function returns the number of blank papers needed to copy :attr:`num_papers` papers
    for :attr:`num_classmates` classmates.

    :param num_classmates: An integer, the number of classmates
    :param num_papers: An integer, the number of papers for each classmate
    :return: The total number of papers, or zero if either input parameter is less than zero
    """
    return 0 if num_classmates < 0 or num_papers < 0 else num_classmates * num_papers
