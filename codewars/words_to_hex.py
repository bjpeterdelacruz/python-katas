"""
Words to Hex

:Author: BJ Peter Dela Cruz
"""


def words_to_hex(words: str) -> list[str]:
    """
    This function converts each word in :attr:`words`, delimited by whitespace character(s), to a
    hexadecimal string that represents a color. Only the first three characters of each word are
    converted to a hexadecimal representation of their ASCII values. If a word consists of only one
    or two characters, "0000" or "00" is appended to the end of the hexadecimal string,
    respectively.

    :param words: The input string containing a whitespace-delimited list of words
    :return: A list of hexadecimal values, each representing a color
    """
    color_codes = []
    for word in words.split():
        if len(word) >= 3:
            color_codes.append(f"#{to_hex(word[0])}{to_hex(word[1])}{to_hex(word[2])}")
        elif len(word) == 2:
            color_codes.append(f"#{to_hex(word[0])}{to_hex(word[1])}00")
        else:
            color_codes.append(f"#{to_hex(word[0])}0000")
    return color_codes


def to_hex(letter: str) -> str:
    """
    This helper function converts the given letter (a single character) to a hexadecimal ASCII
    value (as a string).

    :param letter: A character
    :return: The ASCII value of the given letter as a hexadecimal string
    """
    return hex(ord(letter))[2:]
