"""
ASCII Shift Encryption Decryption

:Author: BJ Peter Dela Cruz
"""


def ascii_encrypt(plaintext: str) -> str:
    """
    This function encrypts the given plaintext message using the index of each character in the
    string as the number of characters to shift right in the set of Unicode code points.

    :param plaintext: The plaintext message to encrypt
    :return: An encrypted message, the ciphertext
    """
    return ''.join([chr(ord(char) + idx) for idx, char in enumerate(plaintext)])


def ascii_decrypt(ciphertext: str) -> str:
    """
    This function decrypts the given plaintext message using the index of each character in the
    string as the number of characters to shift left in the set of Unicode code points.

    :param ciphertext: The ciphertext message to decrypt
    :return: A decrypted message, the plaintext
    """
    return ''.join([chr(ord(char) - idx) for idx, char in enumerate(ciphertext)])
