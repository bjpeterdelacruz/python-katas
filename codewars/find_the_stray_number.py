"""
Find the Stray Number

:Author: BJ Peter Dela Cruz
"""
from collections import Counter


def stray(arr: list[int]) -> int:
    """
    This function finds the integer that does not belong in the given list, i.e. all the integers in
    the list are the same except for one.

    :param arr: The input list of integer
    :return: The integer that does not belong in :attr:`arr`
    """
    return Counter(arr).most_common()[-1][0]
