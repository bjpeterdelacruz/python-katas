"""
Convert the Score

:Author: BJ Peter Dela Cruz
"""


def scoreboard(string: str) -> list[int]:
    """
    This function converts a game's score into a list of integers.

    :param string: The input string
    :return: A list of integers representing a game's score
    """
    nums = 'nil,0,zero,0,one,1,two,2,three,3,four,4,five,5,six,6,seven,7,eight,8,nine,9'.split(',')
    return [int(nums[nums.index(word) + 1]) for word in string.split() if word in nums]
