"""
Longest Alphabetical Substring

:Author: BJ Peter Dela Cruz
"""


def longest(string: str) -> str:
    """
    This function returns the longest substring that is in alphabetical order in :attr:`string`. For
    example, given "asdfaaaabbbbcttavvfffffdf", this function will return "aaaabbbbctt".

    :param string: The input string
    :return: The longest substring that is in alphabetical order
    """
    idx = 1
    temp = string
    longest_substring = temp[0]
    while idx != len(temp):
        if ord(temp[idx]) < ord(temp[idx - 1]):
            if len(temp[:idx]) > len(longest_substring):
                longest_substring = temp[:idx]
            temp = temp[idx:]
            idx = 0
        idx += 1
    return temp if len(temp) > len(longest_substring) else longest_substring
