"""
Break camelCase

:Author: BJ Peter Dela Cruz
"""


def solution(camel_case: str) -> str:
    """
    This function simply adds a space before words in :attr:`camel_case` that are capitalized.

    :param camel_case: The input string that is in camel case
    :return: The same string but with a space added before words that are capitalized
    """
    return ''.join([f" {char}" if char.isupper() else char for char in camel_case])
