"""
I'm Everywhere

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase


def i(word: str) -> str:
    """
    This function appends the letter i to the front of the given word if the word does not start
    with the uppercase letter I or a lowercase letter. Also, the number of vowels in the word must
    not be equal to or greater than the number of consonants. If any of these conditions are not
    met, "Invalid word" is returned.

    :param word: The input string
    :return: The word with the letter i appended to the front, or the string "Invalid word"
    """
    if not word or word[0] in ascii_lowercase or word[0] == "I":
        return "Invalid word"
    consonant_counts = 0
    vowels_counts = 0
    for letter in word:
        if letter.lower() in 'aeiou':
            vowels_counts += 1
        else:
            consonant_counts += 1
    return "Invalid word" if vowels_counts >= consonant_counts else f'i{word}'
