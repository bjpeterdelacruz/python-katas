"""
ASCII Total

:Author: BJ Peter Dela Cruz
"""


def uni_total(letters: str) -> int:
    """
    This function sums all the ASCII values of each character in :attr:`letters`.

    :param letters: The input string
    :return: The sum of all the ASCII values of each character in :attr:`letters`
    """
    return sum(ord(letter) for letter in letters)
