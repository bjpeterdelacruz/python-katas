"""
Populate Hash with Default Value

:Author: BJ Peter Dela Cruz
"""


def populate_dict(keys, default):
    """
    This function creates a dictionary using the given list of keys and assigns the given default
    value to each key.

    :param keys: The input list of keys
    :param default: The default value to assign to each key
    :return: A dictionary of keys, each with the same default value
    """
    return {key: default for key in keys}
