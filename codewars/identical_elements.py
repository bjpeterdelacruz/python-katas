"""
Identical Elements

:Author: BJ Peter Dela Cruz
"""


def duplicate_elements(arr_1: list, arr_2: list) -> int:
    """
    This function returns True if an element in one of the lists also exists in the other list.

    :param arr_1: The first list
    :param arr_2: The second list
    :return: True if at least one element that exists in one list also exists in the other list,
        False otherwise
    """
    return len(set(arr_1 + arr_2)) != len(arr_1) + len(arr_2)
