"""
Monotone Travel

:Author: BJ Peter Dela Cruz
"""


def is_monotone(heights: list[int]) -> bool:
    """
    This function returns True if each integer in the given list is less than or equal to the one
    that follows it, i.e. the list is sorted in ascending order. Otherwise, False is returned.

    :param heights: The input list of integers
    :return: True if the list is sorted in ascending order, False otherwise
    """
    for idx in range(1, len(heights)):
        if heights[idx - 1] > heights[idx]:
            return False
    return True
