"""
Extended Weekends

:Author: BJ Peter Dela Cruz
"""
import datetime


def solve(start_year: int, end_year: int) -> tuple[str, str, int]:
    """
    This function counts the number of months that have five Fridays, five Saturdays, and five
    Sundays. Only months with 31 days are considered because February and months with only 30 days
    will not have five Sundays. The number of months as well as the name of the first month and the
    name of the last month between the start and end years (both ends inclusive) are returned.

    :param start_year: The first year in the range
    :param end_year: The last year in the range
    :return: The name of the first month, the name of the last month, and the total number of months
    """
    date = datetime.date(start_year, 1, 1)
    total_weekends = 0
    months = []
    while date.year <= end_year:
        if date.strftime('%A') == "Friday" and date.month in [1, 3, 5, 7, 8, 10, 12]:
            months.append(date.strftime('%b'))
            total_weekends += 1
        month = date.month + 1 if date.month < 12 else 1
        year = date.year if month > 1 else date.year + 1
        date = datetime.date(year, month, 1)
    return months[0], months[-1], total_weekends
