"""
Simple Multiplication

:Author: BJ Peter Dela Cruz
"""


def simple_multiplication(number: int | float) -> int | float:
    """
    This function multiplies a number by eight if it is an even number or by nine if it is odd.

    :param number: The input number
    :return: The number multiplied by eight if the number is even, or the number multiplied by nine
        if it is odd
    """
    return number * 8 if number % 2 == 0 else number * 9
