"""
Vowel Changer

:Author: BJ Peter Dela Cruz
"""


def vowel_change(text: str, vowel: str) -> str:
    """
    This function replaces all the lowercase vowels in :attr:`text` with :attr:`vowel`. It is
    assumed that :attr:`text` only contains lowercase ASCII characters.

    :param text: The input string
    :param vowel: The vowel used to replace all the other vowels in :attr:`text`
    :return: A string that contains :attr:`vowel` and no other vowels
    """
    return ''.join(vowel if char in 'aeiou' else char for char in text)
