"""
Narcissistic Numbers

:Author: BJ Peter Dela Cruz
"""


def is_narcissistic(number: int) -> bool:
    """
    This function returns True if the given number is narcissistic, that is, the sum of each digit
    to the power of the length of the number is equal to the number itself.

    :param number: The input integer
    :return: True if the number is narcissistic, False otherwise
    """
    return number == sum(int(num) ** len(str(number)) for num in str(number))
