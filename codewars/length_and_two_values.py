"""
Length and Two Values

:Author: BJ Peter Dela Cruz
"""


def alternate(length: int, first_value, second_value) -> list:
    """
    This function returns a list of size :attr:`length` that contains the two given values. The
    values in the list will alternate. For example, alternate(5, 1, 2) will return [1, 2, 1, 2, 1].

    :param length: The size of the list that is returned.
    :param first_value: The value that will appear first in the list.
    :param second_value: The value that will appear next in the list.
    :return: A list of size :attr:`length` that will contain alternating values.
    """
    lst = [first_value, second_value] * length
    return lst[:length]
