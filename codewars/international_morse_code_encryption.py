from itertools import groupby

CHAR_TO_MORSE = {'A': '.-', 'B': '-...', 'C': '-.-.',
                 'D': '-..', 'E': '.', 'F': '..-.',
                 'G': '--.', 'H': '....', 'I': '..',
                 'J': '.---', 'K': '-.-', 'L': '.-..',
                 'M': '--', 'N': '-.', 'O': '---',
                 'P': '.--.', 'Q': '--.-', 'R': '.-.',
                 'S': '...', 'T': '-', 'U': '..-',
                 'V': '...-', 'W': '.--', 'X': '-..-',
                 'Y': '-.--', 'Z': '--..',

                 '0': '-----', '1': '.----', '2': '..---',
                 '3': '...--', '4': '....-', '5': '.....',
                 '6': '-....', '7': '--...', '8': '---..',
                 '9': '----.'}


def encryption(string: str) -> str:
    morse_code = []
    word = []
    chars = []
    for key, value in groupby(string):
        values = list(value)
        if key == ' ':
            chars.append(' ' * (len(values) * 2 + 1))
        else:
            chars.extend(key * len(values))
    for char in chars:
        if char.upper() in CHAR_TO_MORSE:
            word.append(CHAR_TO_MORSE[char.upper()])
        else:
            morse_code.append(' '.join(word))
            morse_code.append(char)
            word = []
    if word:
        morse_code.append(' '.join(word))
    return ''.join(morse_code)
