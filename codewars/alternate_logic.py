"""
Alternate Logic

:Author: BJ Peter Dela Cruz
"""


def alt_or(lst):
    """
    This function returns True if at least one True is found in the given list, False otherwise.
    Note that the or operator is not used.

    :param lst: The input list of booleans
    :return: None if the list is empty or None, True if True is found in the list, or False
    """
    return None if not lst else bool(True in lst)
