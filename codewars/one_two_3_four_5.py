"""
1 Two 3 Four 5

:Author: BJ Peter Dela Cruz
"""
NUMBERS = {"1": "one", "2": "two", "3": "three", "4": "four", "5": "five",
           "6": "six", "7": "seven", "8": "eight", "9": "nine", "0": "zero"}


def conv(num: int) -> str:
    """
    This function converts the given number to a string of numbers and words using the following
    rules: if the length of the number is even, all odd digits are left as is, and all even digits
    are converted to words, e.g. "2" to "two". The position of the digit is the length that the word
    must be, which means that the word is repeated until the length is reached. Each word in the
    string alternates between lowercase and uppercase (lowercase first). If the length of the number
    is odd, all even digits are left as is, all odd digits are converted to words, and the case
    for the words starts with uppercase.

    :param num: The input integer
    :return: A string containing numbers and words
    """
    arr = []
    for idx, char in enumerate(str(num)):
        if len(str(num)) % 2 == 0:
            arr.append(char if int(char) % 2 == 1 else num_to_word(char, idx + 1, True))
        else:
            arr.append(char if int(char) % 2 == 0 else num_to_word(char, idx + 1, False))
    return ''.join(arr)


def num_to_word(number: str, length: int, lowercase_first: bool) -> str:
    """
    This helper function converts the given number to a word, e.g. "1" to "one". The word will be
    repeated until the length of the concatenated string is equal to or greater than :attr:`length`.
    If the length is greater than :attr:`length`, the string will be truncated up to :attr:`length`
    characters. The case of all characters in each word in the concatenated string will alternate
    between lowercase and uppercase, e.g. "oneONEone" and "TWOtwoTW".

    :param number: The number to convert to a word
    :param length: The length of the concatenated string that is returned
    :param lowercase_first: True if the case must start with lowercase first, False if the case must
        start with uppercase first
    :return: A concatenated string containing one or more repeated words
    """
    words = [NUMBERS[number]]
    while len(''.join(words)) < length:
        words.append(NUMBERS[number])
    rem = 1 if lowercase_first else 0
    for idx, word in enumerate(words):
        if idx % 2 == rem:
            words[idx] = word.upper()
    return ''.join(words)[:length]
