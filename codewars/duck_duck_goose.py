"""
Duck Duck Goose

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class Player:
    """
    This class represents a person who is playing the Duck Duck Goose game.
    """
    def __init__(self, name: str):
        """
        Initializes this Player object with the given name.

        :param name: The name of a person playing Duck Duck Goose
        """
        self.name = name


def duck_duck_goose(players: list[Player], goose: int) -> str:
    """
    This function returns the name of the player who is chosen in the game of Duck Duck Goose.

    :param players: A list of people who are playing the game
    :param goose: The 1-based index of the player who is chosen
    :return: The name of the player who is chosen
    """
    return players[goose % len(players) - 1].name
