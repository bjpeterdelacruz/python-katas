def find_children(santas_list: list[str], children: list[str]) -> list[str]:
    return sorted(set(child for child in children if child in santas_list))
