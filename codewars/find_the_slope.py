"""
Find the Slope

:Author: BJ Peter Dela Cruz
"""


def find_slope(points: list[int]) -> str:
    """
    This function finds the slope of a line given a list representing two points on the line. The
    first two integers represent X and Y for the first point, and the last two integers represent X
    and Y for the second point. If the X values for both points are the same, then "undefined" is
    returned.

    :param points: A list of integers representing two points on a line
    :return: The slope of the line as a string or "undefined"
    """
    x_1, y_1, x_2, y_2 = points[0], points[1], points[2], points[3]
    return "undefined" if x_1 == x_2 else str((y_2 - y_1) // (x_2 - x_1))
