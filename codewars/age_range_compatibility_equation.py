"""
Age Range Compatibility Equation

:Author: BJ Peter Dela Cruz
"""
from math import floor


def dating_range(age: int) -> str:
    """
    This function implements a dating rule for people of all ages. For people who are fifteen years
    old or older, the minimum age is: (:attr:`age` / 2) + 7, and the maximum age is:
    (:attr:`age` - 7) * 2. For people who are fourteen years old or younger, a different algorithm
    is used: :attr:`age` +/- 10% of :attr:`age`.

    :param age: The input integer, a person's age
    :return: The socially acceptable age range of the dating partner
    """
    if age <= 14:
        minimum_age = age - (0.1 * age)
        maximum_age = age + (0.1 * age)
    else:
        minimum_age = (age / 2) + 7
        maximum_age = (age - 7) * 2
    return f'{floor(minimum_age)}-{floor(maximum_age)}'
