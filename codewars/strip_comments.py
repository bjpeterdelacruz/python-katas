"""
Strip Comments

:Author: BJ Peter Dela Cruz
"""


def strip_comments(string: str, markers: list[str]) -> str:
    """
    This function removes all the characters that follow each marker in :attr:`markers` on each line
    in :attr:`string`. All trailing whitespaces are removed on each line after the comments
    following a marker have been removed.

    :param string: The input string, each line is separated by a newline character
    :param markers: The list of markers that represent an inline code comment
    :return: The same string but with all comments and trailing whitespaces removed
    """
    strings = string.split('\n')
    results = []
    for current_string in strings:
        temp = current_string
        for marker in markers:
            if marker in temp:
                temp = temp[:temp.index(marker)].strip()
        results.append(temp)
    return '\n'.join(results)
