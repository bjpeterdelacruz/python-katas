"""
Divisible by Previous Digit

:Author: BJ Peter Dela Cruz
"""


def divisible_by_last(number: int) -> list[bool]:
    """
    This function iterates through each digit in :attr:`number` and outputs a list that contains
    boolean values that represent whether a digit is divisible by the digit before it. The first
    element in the list will always be False because no digit ever comes before the first digit.

    :param number: The input integer
    :return: A list containing boolean values
    """
    result = [False]
    num_as_string = str(number)
    for idx in range(1, len(num_as_string)):
        if num_as_string[idx - 1] == "0":
            result.append(False)
        else:
            result.append(int(num_as_string[idx]) % int(num_as_string[idx - 1]) == 0)
    return result
