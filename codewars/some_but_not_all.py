"""
Some (But Not All)

:Author: BJ Peter Dela Cruz
"""


def some_but_not_all(seq, pred) -> bool:
    """
    This function returns True if some of the elements in :attr:`seq` satisfies the predicate
    :attr:`pred`. False is returned if none or all of the elements satisfy the predicate.

    :param seq: The input list
    :param pred: The predicate to apply to each element in :attr:`seq`
    :return: True if some of the elements satisfy the predicate, False if none or all of the
        elements satisfy the predicate
    """
    found = len(list(filter(pred, seq)))
    return found > 0 and found != len(seq)
