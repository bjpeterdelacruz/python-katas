"""
Excel Sheet Column Numbers

:Author: BJ Peter Dela Cruz
"""
from string import ascii_uppercase


def title_to_number(title: str) -> int:
    """
    This function converts a column title as it appears in an Excel spreadsheet to its corresponding
    column number. For example, "A" is 1, "Z" is 26, and "AB" is 28.

    :param title: A column title
    :return: A column number
    """
    title = title[::-1]
    return sum((ascii_uppercase.index(char) + 1) * (26 ** exp) for exp, char in enumerate(title))
