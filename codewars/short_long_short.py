"""
Short Long Short

:Author: BJ Peter Dela Cruz
"""


def solution(str_1: str, str_2: str) -> str:
    """
    This function concatenates :attr:`str_1` and :attr:`str_2` together and returns the result. The
    shortest string will come first, followed by the longest string, and then the shortest string.

    :param str_1: The first input string
    :param str_2: The second input string
    :return: A string that is the concatenation of :attr:`str_1` and :attr:`str_2`
    """
    return f'{str_1}{str_2}{str_1}' if len(str_1) < len(str_2) else f'{str_2}{str_1}{str_2}'
