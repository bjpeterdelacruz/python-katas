"""
Linked Lists: Sorted Insert

:Author: BJ Peter Dela Cruz
"""
from utils.linked_list import Node


def sorted_insert(head, data) -> Node:
    """
    This function inserts the given data at the appropriate position in the linked list. It is
    assumed that the list is already sorted.

    :param head: The linked list
    :param data: The data to insert
    :return: The linked list with the data inserted at the appropriate position such that the list
        remains in sorted order
    """
    if not head:
        return Node(data)
    if data < head.data:
        new_node = Node(data)
        new_node.next = head
        return new_node
    prev = head
    next_node = head.next
    while next_node:
        if next_node.data > data:
            new_node = Node(data)
            prev.next = new_node
            new_node.next = next_node
            return head
        prev = next_node
        next_node = next_node.next
    prev.next = Node(data)
    return head
