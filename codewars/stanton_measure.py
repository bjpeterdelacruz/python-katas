"""
Stanton Measure

:Author: BJ Peter Dela Cruz
"""


def stanton_measure(arr: list[int]) -> int:
    """
    This function calculates the Stanton measure of a list. The Stanton measure is calculated by
    counting the number of times that the number 1 appears in the list and then counting the number
    of times the count appears in the list. For example, given [1, 2, 3, 2, 1, 2, 5, 2], the Stanton
    measure is 4.

    :param arr: The input list of integers
    :return: The Stanton measure of :attr:`arr`
    """
    return arr.count(arr.count(1))
