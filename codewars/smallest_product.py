"""
Smallest Product

:Author: BJ Peter Dela Cruz
"""


def smallest_product(arr: list[list[int]]) -> int:
    """
    This function multiplies all the elements of each sublist together and then returns the smallest
    product.

    :param arr: The input list that contains lists of integers
    :return: The smallest product after multiplying all the elements of each sublist
    """
    products = []
    for current_arr in arr:
        product = 1
        for element in current_arr:
            product *= element
        products.append(product)
    return min(products)
