"""
Christmas Tree

:Author: BJ Peter Dela Cruz
"""


def christmas_tree(height: int) -> str:
    """
    This function returns a string representing a Christmas tree. The formula (2 x height) - 1 is
    used to draw the leaves (represented by asterisks) on the tree.

    :param height: The input integer, the height of the tree
    :return: An ASCII art of a Christmas tree
    """
    tree = []
    line_length = 2 * height - 1
    while height > 0:
        num_stars = 2 * height - 1
        num_spaces = (line_length - num_stars) // 2
        tree.append(''.join([' ' * num_spaces, '*' * num_stars, ' ' * num_spaces]))
        height -= 1
    tree.reverse()
    return '\n'.join(tree)
