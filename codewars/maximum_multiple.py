"""
Maximum Multiple

:Author: BJ Peter Dela Cruz
"""


def max_multiple(divisor: int, bound: int) -> int:
    """
    This function finds a number that is divisible by the given divisor and is the highest value
    that is below or equal to :attr:`bound`.

    :param divisor: The input integer
    :param bound: The input integer, the starting value that may or may not be divisible by
        :attr:`divisor`
    :return: A number that is divisible by :attr:`divisor`
    """
    current_number = bound
    while True:
        if current_number % divisor == 0:
            return current_number
        current_number -= 1
