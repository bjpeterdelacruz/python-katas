"""
Return Substring Instance Count - 2

:Author: BJ Peter Dela Cruz
"""


def search_substr(full_text: str, search_text: str, allow_overlap: bool = True) -> int:
    """
    This function counts the number of :attr:`search_text` in :attr:`full_text`. If overlapping is
    allowed, the :attr:`full_text` is searched starting from the index of the character next to the
    one that is the beginning of :attr:`search_text` if the latter is found.

    :param full_text: The input string
    :param search_text: The input string, the text to search for in :attr:`full_text`
    :param allow_overlap: (Optional) True to include overlapping substrings, False otherwise
    :return: The total number of :attr:`search_text` in :attr:`full_text`
    """
    if not search_text:
        return 0
    count = 0
    while search_text in full_text:
        count += 1
        idx = full_text.index(search_text)
        idx += 1 if allow_overlap else len(search_text)
        full_text = full_text[idx:]
    return count
