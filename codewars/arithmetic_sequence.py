"""
Arithmetic Sequence

:Author: BJ Peter Dela Cruz
"""


def nthterm(first: int, index: int, constant: int) -> int:
    """
    This function produces an arithmetic sequence of integers, i.e. the difference between one
    element in a list and the next element is constant, and returns the last element in the
    sequence.

    :param first: The first integer that will be in the arithmetic sequence
    :param index: The index of the last element in the arithmetic sequence
    :param constant: The difference between one element in the sequence and the next
    :return: The last element in the sequence
    """
    return [first + (constant * counter) for counter in range(0, index + 1)][-1]
