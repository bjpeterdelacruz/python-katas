"""
Is There an Odd Bit?

:Author: BJ Peter Dela Cruz
"""


def any_odd(number: int) -> int:
    """
    This function returns 1 if there is at least one bit that is turned on at an odd index in the
    binary representation of the given number. The index is zero-based and starts from the least
    significant bit (i.e. the bit that determines whether a number is even or odd).

    :param number: The input integer
    :return: 1 if there is at least one bit that is turned on at an odd index, 0 otherwise
    """
    bit_string = ''.join(reversed(bin(number)[2:]))
    bits = [bit for idx, bit in enumerate(bit_string) if idx % 2 == 1 and bit == "1"]
    return 1 if len(bits) >= 1 else 0
