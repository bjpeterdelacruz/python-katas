"""
Acrostic Reader

:Author: BJ Peter Dela Cruz
"""


def read_out(acrostic: str) -> str:
    """
    This function returns the word that is spelled out in an acrostic (the first letter of each line
    spells out a word).

    :param acrostic: The input string, an acrostic
    :return: The word that is spelled out in the acrostic
    """
    return ''.join([word[0] for word in acrostic])
