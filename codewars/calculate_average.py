"""
Calculate Average

:Author: BJ Peter Dela Cruz
"""


def find_average(numbers: list[int]) -> int | float:
    """
    This function finds the average of a list of numbers. If the list is empty, 0 is returned.

    :param numbers: The input list of numbers
    :return: The average of all the numbers, or 0 if the list is empty
    """
    return 0 if len(numbers) == 0 else sum(numbers) / len(numbers)
