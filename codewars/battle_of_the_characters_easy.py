"""
Battle of the Characters

:Author: BJ Peter Dela Cruz
"""
from string import ascii_uppercase as upper


def battle(group_one: str, group_two: str) -> str:
    """
    This function sums the indexes at which a character appears in the English alphabet. The string
    that produces the greatest sum is returned. If the sums are equal, then "Tie!" is returned.

    :param group_one: The first input string
    :param group_two: The second input string
    :return: The string that produces the greatest sum, or "Tie!" if the sums of the indexes are
        equal
    """
    sum_one = sum(upper.index(letter) + 1 for letter in group_one)
    sum_two = sum(upper.index(letter) + 1 for letter in group_two)
    return group_one if sum_one > sum_two else group_two if sum_two > sum_one else "Tie!"
