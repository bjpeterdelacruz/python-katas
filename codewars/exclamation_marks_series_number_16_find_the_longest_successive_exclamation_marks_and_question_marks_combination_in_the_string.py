"""
Exclamation Marks Series #16: Find the Longest Successive Exclamation Marks and Question Marks
Combination in the String

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def find(string: str) -> str:
    """
    This function finds the longest combination of exclamation marks and question marks. The
    combination must contain only two parts: a string consisting of "!" and a string consisting of
    "?". Both must be adjacent. If two or more combinations share the same length, the first one
    that is found is returned. If no combinations are found, an empty string is returned.

    :param string: The input string consisting of only exclamation marks and question marks
    :return: A combination consisting of exclamation marks and question marks, or an empty string
    """
    lengths = [(key, len(list(group))) for key, group in groupby(string)]
    substrings = []
    for idx in range(1, len(lengths)):
        first, second = lengths[idx - 1], lengths[idx]
        substrings.append((first[0] * first[1] + second[0] * second[1], first[1] + second[1]))
    return max(substrings, key=lambda pair: pair[1])[0] if substrings else ""
