"""
Name Your Python

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class Python:
    """
    Initializes an instance of this class with a name.
    """
    def __init__(self, name):
        self.name = name
