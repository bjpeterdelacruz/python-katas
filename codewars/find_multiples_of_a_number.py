"""
Find Multiples of a Number

:Author: BJ Peter Dela Cruz
"""


def find_multiples(integer: int, limit: int) -> list[int]:
    """
    This function returns a list of integers from 1 to :attr:`limit` (both inclusive) that are
    multiples of :attr:`integer`.

    :param integer: The number that is used to generate a list of multiples
    :param limit: The maximum integer that must be in the list that is returned if it is a multiple
        of :attr:`integer`
    :return: A list of integers from 1 to :attr:`limit` (both inclusive) that are multiples of
        :attr:`integer`
    """
    return list(filter(lambda x: x % integer == 0, range(1, limit + 1)))
