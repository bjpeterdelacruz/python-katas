def moving_average(values: list[int], window_size: int) -> list[float] | None:
    if window_size == 0 or window_size > len(values):
        return None
    result = []
    for idx, _ in enumerate(values):
        if idx + window_size > len(values):
            break
        result.append(sum(values[idx:idx + window_size]) / len(values[idx:idx + window_size]))
    return result
