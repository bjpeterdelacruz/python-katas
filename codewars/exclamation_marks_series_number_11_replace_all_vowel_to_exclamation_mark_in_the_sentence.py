"""
Exclamation Marks Series #11: Replace All Vowel to Exclamation Mark in the Sentence

:Author: BJ Peter Dela Cruz
"""


def replace_exclamation(string: str) -> str:
    """
    This function replaces each vowel in :attr:`string`, regardless of case, with an exclamation
    mark.

    :param string: The input string
    :return: A string without any vowels
    """
    return ''.join('!' if char in "aeiouAEIOU" else char for char in string)
