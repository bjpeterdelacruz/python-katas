"""
Boolean Trilogy #2: Calculate Boolean Expression (Easy)

:Author: BJ Peter Dela Cruz
"""


def calculate(expr: str, values: dict[str, int]) -> int:
    """
    This function calculates the value of a boolean expression consisting of AND and OR operators.
    The eval function is not used, and the AND operator has a higher precedence than the OR
    operator. The symbols in the boolean expression are replaced by zeros and ones from the given
    dictionary.

    :param expr: The input string representing of a boolean expression
    :param values: A dictionary of values (zeros and ones) to use in the boolean expression
    :return: The value of the boolean expression, either a zero or one
    """
    for key, value in values.items():
        expr = expr.replace(key, str(value))
    expr_arr = expr.split('|')
    or_arr = []
    for expression in expr_arr:
        and_arr = expression.strip().split()
        while len(and_arr) != 1:
            first = int(and_arr.pop(0))
            and_arr.pop(0)
            second = int(and_arr.pop(0))
            and_arr.insert(0, str(first & second))
        or_arr.append(and_arr.pop())
    or_arr = ' | '.join(or_arr).split()
    while len(or_arr) != 1:
        first = int(or_arr.pop(0))
        or_arr.pop(0)
        second = int(or_arr.pop(0))
        or_arr.insert(0, str(first | second))
    return int(or_arr.pop())
