"""
String Array Duplicates

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def dup(array: list[str]) -> list[str]:
    """
    This function goes through each string in :attr:`array` and replaces all substrings of length 2
    or more that contain the same character with just one character. For example, given
    ["Mississippi", "Tennessee"], this function will return ["Misisipi", "Tenese"].

    :param array: The input list of strings
    :return: The same list of strings but with all substrings replaced with a single character
    """
    return [(''.join([key for key, _ in groupby(string)])) for string in array]
