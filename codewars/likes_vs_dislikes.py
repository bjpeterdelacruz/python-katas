"""
Likes vs. Dislikes

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=unnecessary-pass
# pylint: disable=too-few-public-methods


class Like:
    """
    This class represents a Like button.
    """
    pass


class Dislike:
    """
    This class represents a Dislike button.
    """
    pass


class Nothing:
    """
    This class represents an undo of a button press, e.g. pressing the Like button two times.
    """
    pass


def like_or_dislike(lst: list):
    """
    This function iterates through a list of buttons that were pressed and returns the final state.
    If the Like or Dislike button is pressed twice in a row, the current state will be Nothing. If
    an empty list is passed to the function, Nothing is returned. If the list contains only one
    button, the final state will be represented by that button. If the Like button is pressed and
    then the Dislike button is pressed, or vice versa, then the current state will be the last
    button that was pressed.

    :param lst: A list of Like or Dislike buttons.
    :return: Like, Dislike, or Nothing
    """
    if not lst:
        return Nothing
    if len(lst) == 1:
        return lst[0]
    states = [Nothing if lst[0] == lst[1] else lst[1]]
    idx = 2
    while idx < len(lst):
        states.append(Nothing if states[-1] == lst[idx] else lst[idx])
        idx += 1
    return states[-1]
