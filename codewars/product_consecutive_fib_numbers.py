"""
Product of Consecutive Fibonacci Numbers

:Author: BJ Peter Dela Cruz
"""


def product_fib(prod: int) -> list:
    """
    This function returns the two Fibonacci numbers F(n) and F(n-1) whose product is equal to
    :attr:`prod`. If no product is found, this function returns the two Fibonacci numbers whose
    product is greater than :attr:`prod`.

    :param prod: The input integer representing the product of two Fibonacci numbers
    :return: Two Fibonacci numbers and either True if the product is found, False otherwise
    """
    is_found = False
    fibonacci = [0, 1]
    while True:
        fib_2 = fibonacci[-1]
        fib_1 = fibonacci[-2]
        if fib_1 * fib_2 == prod:
            is_found = True
            break
        if fib_1 * fib_2 > prod:
            break
        fibonacci.append(fib_1 + fib_2)
    return [fib_1, fib_2, is_found]
