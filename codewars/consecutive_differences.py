"""
Consecutive Differences

:Author: BJ Peter Dela Cruz
"""


def differences(integers: list[int]) -> int:
    """
    This function calculates the positive difference between each consecutive pair of numbers in the
    given list and puts the results into a new list. Then the function repeats this process for each
    pair of numbers in the new list of differences until the list consists of only one element. That
    element is returned.

    :param integers: The input list of integers
    :return: The final difference
    """
    if len(integers) == 1:
        return integers[0]
    temp = integers
    diffs = []
    while len(diffs) != 1:
        if len(diffs) > 0:
            temp = diffs
        diffs = [abs(temp[idx - 1] - temp[idx]) for idx in range(1, len(temp))]
    return diffs[0]
