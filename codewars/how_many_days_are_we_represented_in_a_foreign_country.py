"""
Days Represented in a Foreign Country

:Author: BJ Peter Dela Cruz
"""


def days_represented(trips: list[list[int]]) -> int:
    """
    This function returns the number of days that a company has been represented in a foreign
    country. The given list of tuples represent the start and end dates. Note that the start and end
    dates may overlap in which case the dates are only counted once.

    :param trips: The input list of tuples that contain start and end dates
    :return: The number of days that a company has been represented in a foreign country
    """
    return len({day for trip in trips for day in range(trip[0], trip[1] + 1)})
