"""
Spoonerize Me

:Author: BJ Peter Dela Cruz
"""


def spoonerize(words: str) -> str:
    """
    This function takes two words, swaps the first letter of each word, and returns the result. For
    example, given "jelly beans", the function will return "belly jeans".

    :param words: A space-delimited list of two words
    :return: The same space-delimited list but with the first letter of each word swapped
    """
    first_word, second_word = words.split()
    return f"{second_word[0]}{first_word[1:]} {first_word[0]}{second_word[1:]}"
