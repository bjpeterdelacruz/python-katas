"""
Number of People in the Bus

:Author: BJ Peter Dela Cruz
"""


def number(bus_stops: list[list[int]]) -> int:
    """
    This function calculates the number of people remaining on a bus given a list that contains both
    the number of people who get on the bus and the number of people who get off the bus.

    :param bus_stops: A list of bus stops at which people get on the bus and people get off the bus
    :return: The total number of people remaining on the bus after the last stop
    """
    total = 0
    for stop in bus_stops:
        total += stop[0] - stop[1]
    return total
