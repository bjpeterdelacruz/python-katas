"""
Case Swapping

:Author: BJ Peter Dela Cruz
"""
from string import ascii_uppercase as upper


def swap(string: str) -> str:
    """
    This function swaps the case of each alphabetical letter in the given string, i.e. all lowercase
    letters are turned into uppercase letters, and vice versa.

    :param string: The input string
    :return: The same string but with the case of each alphabetical character swapped
    """
    return ''.join([char.lower() if char in upper else char.upper() for char in string])
