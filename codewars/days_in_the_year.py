"""
Days in the Year

:Author: BJ Peter Dela Cruz
"""


def year_days(year: int) -> str:
    """
    This function returns the number of days in a year. If the year is a century, there are 366 days
    only if the year is divisible by 400. Otherwise, there are 365 days. If the year is not a
    century, there are 366 days only if the year is divisible by 4. Otherwise, there are 365 days.
    Negative numbers and the number zero are valid inputs.

    :param year: The input integer, a year
    :return: A string, e.g. "2000 has 366 days" or "2001 has 365 days"
    """
    if year % 100 == 0:
        return f'{year} has 366 days' if year % 400 == 0 else f'{year} has 365 days'
    return f'{year} has 366 days' if year % 4 == 0 else f'{year} has 365 days'
