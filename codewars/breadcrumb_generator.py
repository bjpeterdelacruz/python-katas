"""
Breadcrumb Generator

:Author: BJ Peter Dela Cruz
"""


ignore_words = ["the", "of", "in", "from", "by", "with", "and", "or", "for", "to", "at", "a"]
ignore_chars = ["#", "?", "."]


def shorten(string: str) -> str:
    """
    This helper function shortens the given URL string if it contains more than 30 characters. If it
    does contain more than 30 characters, the first letter of each word that is not in the
    ignore_words list is used to generate the display text for the breadcrumb. If the URL string
    contains 30 characters or less, all hyphens are replaced by spaces, and all letters in each word
    are turned to uppercase.

    :param string: The URL string to shorten
    :return: A shortened URL string if it originally contained more than 30 characters
    """
    if len(string) <= 30:
        return string.upper().replace('-', ' ')
    words = string.split("-")
    new_string = []
    for word in words:
        if word.lower() not in ignore_words:
            new_string.append(word[0].upper())
    return ''.join(new_string)


def generate_anchor(link: str, string: str) -> str:
    """
    This helper function creates an anchor (<a>) HTML tag.

    :param link: The link to use in the href attribute
    :param string: The anchor text
    :return: An anchor HTML tag
    """
    return f'<a href="{link}">{shorten(string)}</a>'


def generate_span(string: str) -> str:
    """
    This helper function creates a span (<span>) HTML tag.

    :param string: The text to display
    :return: A span HTML tag
    """
    return f'<span class="active">{shorten(string)}</span>'


def ignore_characters(string: str) -> str:
    """
    This helper function returns a new URL string with everything following and including the #, ?,
    and . characters ignored.

    :param string: The URL string that contains #, ?, or .
    :return: The same URL string but with everything following and including the aforementioned
        characters ignored
    """
    new_string = string
    for char in ignore_chars:
        if char in new_string:
            new_string = new_string[:new_string.index(char)]
    return new_string


def generate_bc(url: str, separator: str) -> str:
    """
    This function generates a breadcrumb that can be used on HTML pages using the given :attr:`url`.
    The :attr:`separator` string is used to separate different levels in the breadcrumb.

    :param url: The input string, a URL for which to generate a breadcrumb
    :param separator: A string used to separate different levels in the breadcrumb
    :return: A breadcrumb for the given URL
    """
    breadcrumb = []
    url_list = url.replace("http://", "").replace("https://", "").split("/")
    if url_list[-1] == '':
        url_list = url_list[:-1]
    if len(url_list) == 1:
        return generate_span("HOME")
    if len(url_list) > 2:
        breadcrumb = ['<a href="/">HOME</a>']
    href = ['/']
    for idx in range(1, len(url_list)):
        if idx + 1 == len(url_list):
            breadcrumb.append(generate_span(ignore_characters(url_list[idx])))
        elif idx + 1 == len(url_list) - 1 and "index." in url_list[idx + 1]:
            breadcrumb.append(generate_span(url_list[idx]))
            break
        else:
            href.append(url_list[idx])
            href.append('/')
            breadcrumb.append(generate_anchor(''.join(href), url_list[idx]))
    if len(breadcrumb) == 1 and "span" in breadcrumb[0]:
        if "INDEX" in breadcrumb[0]:
            return generate_span("HOME")
        breadcrumb.insert(0, '<a href="/">HOME</a>')
    return f"{separator}".join(breadcrumb)
