"""
Club Doorman

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase
from collections import Counter


def pass_the_door_man(word: str) -> int:
    """
    This function finds the first letter in the given string that appears two times in a row. One is
    added to the index at which the letter appears in the English alphabet. Finally, the sum is
    multiplied by three and returned. It is assumed that the string will always contain at least two
    letters in a row.

    :param word: The input string
    :return: A numeric result
    """
    counts = Counter(word)
    letter = [letter for letter, count in counts.items() if count == 2 and letter * 2 in word][0]
    return (ascii_lowercase.index(letter) + 1) * 3
