"""
V  A  P  O  R  C  O  D  E

:Author: BJ Peter Dela Cruz
"""


def vaporcode(string: str) -> str:
    """
    This function converts all characters in the given string to uppercase and inserts two spaces
    between each character. All whitespace characters that are in the given string are removed
    before the spaces are inserted.

    :param string: The input string
    :return: A string that contains uppercase characters and two spaces between each character
    """
    return '  '.join(''.join(string.upper().split()))
