"""
Exponential-Golomb Decoder

:Author: BJ Peter Dela Cruz
"""


def decoder(sequence: str) -> list[int]:
    """
    This function decodes the given binary string that was encoded using an exponential-Golomb code.
    First, get the number of preceding zeros. Then a substring (that starts with 1) of the binary
    string that contains that number of bits is converted to a base-10 representation of an integer
    and lastly decremented by one. This process is repeated until all bits in the encoded binary
    string are processed.

    :param sequence: The input string, encoded using an exponential-Golomb code
    :return: The decoded string
    """
    count = 0
    idx = 0
    result = []
    while idx < len(sequence):
        if sequence[idx] == '0':
            count += 1
            idx += 1
        else:
            bit_string = sequence[idx:idx + count + 1]
            result.append(int(bit_string, 2) - 1)
            count = 0
            idx += len(bit_string)
    return result
