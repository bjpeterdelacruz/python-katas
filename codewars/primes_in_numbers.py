"""
Primes in Numbers

:Author: BJ Peter Dela Cruz
"""
from collections import OrderedDict
from codewars.simple_fun_number_115_factor_sum import get_prime_factors


def prime_factors(integer: int) -> str:
    """
    This function returns all the prime factors of :attr:`integer` in the following form:
    "(p1**n1)(p2**n2)...(pk**nk)". The prime factors are displayed in ascending order. If
    n1,n2,...nk is 1, only the prime factor will be displayed in the string.

    :param integer: The input integer for which to generate all the prime factors
    :return: All the prime factors displayed as "(p1**n1)(p2**n2)...(pk**nk)"
    """
    factors = get_prime_factors(integer)
    counts = OrderedDict()
    for factor in factors:
        if factor in counts:
            counts[factor] += 1
        else:
            counts[factor] = 1
    strings = [f"({key})" if value == 1 else f"({key}**{value})" for key, value in counts.items()]
    return ''.join(strings)
