"""
uniq -c (UNIX style)

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def uniq_c(seq):
    """


    :param seq:
    :return:
    """
    return [(key, len(list(value))) for key, value in groupby(seq, key=lambda x: x)]
