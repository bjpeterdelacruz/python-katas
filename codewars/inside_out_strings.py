"""
Inside Out Strings

:Author: BJ Peter Dela Cruz
"""


def inside_out(string: str) -> str:
    """
    This function turns each word in the given string inside out by reversing all characters in the
    word up to the midpoint or middle character (if the word has an odd number of characters). For
    example, "taxi" and "taxis" will become "atix" and "atxsi", respectively.

    :param string: The input string containing words separated by space character(s)
    :return: A string with each word separated by a space and turned inside out
    """
    arr = []
    for word in string.split():
        if len(word) % 2 == 0:
            end_1 = start_2 = len(word) // 2
            middle_char = ''
        else:
            end_1 = len(word) // 2
            start_2 = end_1 + 1
            middle_char = word[end_1]
        arr.append(f'{word[0:end_1][::-1]}{middle_char}{word[start_2:][::-1]}')
    return ' '.join(arr)
