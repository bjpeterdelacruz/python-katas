"""
Fun with Lists: Length

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class Node:
    """
    This class represents a node in a singly-linked list.
    """
    def __init__(self, value: int, next_node=None):
        """
        Initializes a node in a singly-linked list.

        :param value: The value for this node
        :param next_node: The node that follows this one in the list
        """
        self.value = value
        self.next = next_node


def length(head: Node | None) -> int:
    """
    This function counts the number of nodes in the given linked list.

    :param head: A node, the head of the linked list
    :return: The number of nodes in the linked list, or 0 if :attr:`head` is None
    """
    count = 0
    while head:
        count += 1
        head = head.next
    return count
