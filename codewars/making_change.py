"""
Making Change

:Author: BJ Peter Dela Cruz
"""


def make_change(amount: int) -> dict[str, int]:
    """
    This function returns a dictionary containing the least number of coins needed to make change.
    Any coins that are not used to make change are not included in the dictionary.

    :param amount: The input amount
    :return: A dictionary containing the number of coins of each type (half-dollar, quarter, dime,
        nickel, penny)
    """
    amount, half_dollar = count_change(amount, 50)
    amount, quarter = count_change(amount, 25)
    amount, dime = count_change(amount, 10)
    amount, nickel = count_change(amount, 5)
    amount, penny = count_change(amount, 1)
    change = [("H", half_dollar), ("Q", quarter), ("D", dime), ("N", nickel), ("P", penny)]
    return dict(filter(lambda x: x[1] > 0, change))


def count_change(amount: int, coin: int) -> tuple[int, int]:
    """
    This helper function counts the minimum number of coins of a certain type, e.g. nickel, that is
    needed to make change. The count as well as the amount leftover are returned.

    :param amount: The amount
    :param coin: The type of coin, e.g. quarter (25), nickel (5), etc.
    :return: The amount leftover and the number of coins of type :attr:`coin`
    """
    count = 0
    while amount >= coin:
        amount -= coin
        count += 1
    return amount, count
