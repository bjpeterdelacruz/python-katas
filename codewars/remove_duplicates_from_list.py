"""
Remove Duplicates from List

:Author: BJ Peter Dela Cruz
"""


def distinct(seq):
    """
    This function removes all duplicate values from the given list. The order of the elements in the
    list that is returned must be the same as the order of the elements in the input list.

    :param seq: The input list
    :return: A list of unique elements
    """
    unique = set()
    new_list = []
    for item in seq:
        if item not in unique:
            unique.add(item)
            new_list.append(item)
    return new_list
