def score_test(tests: list[int], right: int, omit: int, wrong: int) -> int:
    return right * tests.count(0) + omit * tests.count(1) - wrong * tests.count(2)
