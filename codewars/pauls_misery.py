"""
Paul's Misery

:Author: BJ Peter Dela Cruz
"""


def paul(events: list[str]) -> str:
    """
    This function determines whether Paul who likes to complete katas on Codewars is miserable. Each
    string in the given list is worth zero, one, five, or ten points. If his score (the sum of all
    the points) is above 100, he is miserable. If his score is greater than or equal to 70 and less
    than 100, he is sad. If his score is greater than or equal to 40 and less than 70, he is happy.
    Finally, if his score is below 40, he is super happy.

    :param events: The input list of strings representing events in Paul's life
    :return: "Super happy!", "Happy!", "Sad!", or "Miserable!"
    """
    scores = {'kata': 5, 'Petes kata': 10, 'life': 0, 'eating': 1}
    total_score = sum(scores[event] for event in events)
    if total_score < 40:
        return 'Super happy!'
    if 40 <= total_score < 70:
        return 'Happy!'
    return 'Sad!' if 70 <= total_score < 100 else 'Miserable!'
