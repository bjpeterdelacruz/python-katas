"""
Polybius Square Cipher (Encode)

:Author: BJ Peter Dela Cruz
"""
from string import ascii_uppercase


def polybius(text: str) -> str:
    """
    This function encodes the given text using a polybius square cipher. Only uppercase letters are
    encoded. All other characters except for spaces are ignored. The cipher encodes I and J as 24.

    :param text: The input string to encode
    :return: An encoded string
    """
    cipher = {' ': ' '}
    row = col = 0
    for char in ascii_uppercase:
        cipher[char] = f"{row + 1}{col + 1}"
        if char == 'J':
            cipher[char] = cipher['I']
            col -= 1
        col += 1
        if col % 5 == 0:
            col = 0
            row += 1
    return ''.join([cipher[letter] for letter in text if letter in cipher])
