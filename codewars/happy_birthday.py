"""
Happy Birthday

:Author: BJ Peter Dela Cruz
"""


def wrap(height: int, width: int, length: int) -> int:
    """
    This function finds the total length in centimeters of a ribbon that is used to decorate a box.
    The ribbon is crossed on the side of the box with the largest area. On the other side, which
    has the same area as the side with the largest area, the ribbon is bound to create a bow, which
    is 20 centimeters long.

    :param height: The height in centimeters of the box
    :param width: The width in centimeters of the box
    :param length: The length in centimeters of the box
    :return: The total length in centimeters of the ribbon
    """
    measurements = sorted([height, width, length])
    return measurements[0] * 4 + (measurements[1] + measurements[2]) * 2 + 20
