"""
Return String of First Characters

:Author: BJ Peter Dela Cruz
"""


def make_string(string: str) -> str:
    """
    This function concatenates the first letter of each word in :attr:`string` into a new string.
    Each word in :attr:`string` is separated by one or more spaces.

    :param string: The input string (a whitespace-delimited list of strings)
    :return: A string consisting of the first letter of each word in :attr:`string` concatenated
        together
    """
    return ''.join(word[0] for word in string.split())
