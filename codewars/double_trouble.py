"""
Double Trouble

:Author: BJ Peter Dela Cruz
"""


def trouble(arr: list[int], target: int) -> list[int]:
    """
    This function checks whether two consecutive integers in :attr:`arr` sum to :attr:`target`. If
    they do, the second integer is removed. If the second integer is removed, the next integer in
    the list that replaces it and the first integer are checked, and the process repeats until the
    end of the list is reached.

    :param arr: The input list of integers
    :param target: The sum
    :return: A list that does not contain two consecutive integers that sum to :attr:`target`
    """
    idx = 1
    while idx < len(arr):
        if sum(arr[idx - 1:idx + 1]) == target:
            arr = arr[0:idx] + arr[idx + 1:]
            idx -= 1
        idx += 1
    return arr
