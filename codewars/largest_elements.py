"""
Largest Elements

:Author: BJ Peter Dela Cruz
"""


def largest(top_n: int, numbers: list[int]) -> list[int]:
    """
    This function returns the top N integers in the given list of integers, where N is equal to
    :attr:`top_n`.

    :param top_n: The number of integers to return
    :param numbers: The input list of integers
    :return: The top N integers in :attr:`numbers`
    """
    return list(sorted(numbers))[-top_n:]
