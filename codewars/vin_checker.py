"""
VIN Checker

:Author: BJ Peter Dela Cruz
"""
table = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8,
         'J': 1, 'K': 2, 'L': 3, 'M': 4, 'N': 5,
         'P': 7,
         'R': 9,
         'S': 2, 'T': 3, 'U': 4, 'V': 5, 'W': 6, 'X': 7, 'Y': 8, 'Z': 9}


def check_vin(number: str) -> bool:
    """
    This function checks whether the given VIN is valid. A vehicle identification number (VIN) must
    be seventeen characters long. If 'I', 'O', or 'Q' appears in the VIN, it is invalid. Each
    alphabetic character in the VIN is converted to a number. Then each number is multiplied by a
    weight. All the products are summed together. The modulus 11 of the sum is taken. If the ninth
    character in the VIN is an 'X', the result of the modulus operation must be 10. Otherwise, the
    result of the modulus operation must be equal to the ninth character. If they are not equal, the
    VIN is invalid.

    :param number: A string representing a valid or invalid VIN
    :return: True if the VIN is valid, False otherwise
    """
    if len(number) != 17:
        return False
    try:
        numbers = [table[char] if char in table else int(char) for char in number]
    except ValueError:
        return False
    weights = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2]
    total = sum(weights[idx] * number for idx, number in enumerate(numbers))
    modulus = total % 11
    try:
        return (modulus == 10 and number[8] == 'X') or (modulus == int(number[8]))
    except ValueError:
        return False
