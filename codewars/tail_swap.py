"""
Tail Swap

:Author: BJ Peter Dela Cruz
"""


def tail_swap(strings: list[str]) -> list[str]:
    """
    This function swaps the substring that comes after a colon in the first string in the given list
    of two strings with the substring that comes after a colon in the second string. There will
    always be only one colon in each string, and it will never be at the beginning or end of both
    strings, i.e. the substrings before and after the colon will at least have one character.

    :param strings: The input list containing two strings
    :return: A list containing two strings
    """
    colon_1_idx = strings[0].index(':')
    colon_2_idx = strings[1].index(':')
    head_1 = strings[0][:colon_1_idx]
    head_2 = strings[1][:colon_2_idx]
    tail_1 = strings[0][colon_1_idx + 1:]
    tail_2 = strings[1][colon_2_idx + 1:]
    return [f'{head_1}:{tail_2}', f'{head_2}:{tail_1}']
