"""
Interview Question

:Author: BJ Peter Dela Cruz
"""
from collections import Counter


def get_strings(city: str) -> str:
    """
    This function counts the number of characters in :attr:`city` and returns a string in which the
    count is represented by asterisks, e.g. 5 = *****. The counting is case-insensitive.

    :param city: The input string
    :return: A string containing the count for each letter, each appearing in the order that it
        appears in :attr:`city`
    """
    counts = Counter([character.lower() for character in city if character.isalpha()])
    return ','.join([f"{letter}:{'*' * count}" for letter, count in counts.items()])
