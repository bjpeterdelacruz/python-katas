"""
Comfortable Words

:Author: BJ Peter Dela Cruz
"""


def comfortable_word(word: str) -> bool:
    """
    This function checks whether the given word is "comfortable". That is, the hands alternate when
    a word is typed on a QWERTY keyboard. For example, "cub" is a comfortable word because the left
    hand types "c", then the right hand types "u", and finally the left hand types "b", but "wand"
    is not a comfortable word because the left hand types the first two characters.

    :param word: The input string
    :return: True if :attr:`word` is a comfortable word as described above or False otherwise
    """
    left = 'qwertasdfgzxcvb'
    hands = ['left' if char in left else 'right' for char in word]
    current_hand = hands[0]
    for idx in range(1, len(hands)):
        if current_hand == hands[idx]:
            return False
        current_hand = hands[idx]
    return True
