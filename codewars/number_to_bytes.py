"""
Number to Bytes

:Author: BJ Peter Dela Cruz
"""


def to_bytes(number: int) -> list[str]:
    """
    This function converts an unsigned integer to a list of bytes. Each binary string in the list
    represents a byte, which is eight digits long.

    :param number: An unsigned integer
    :return: A list of eight-digit binary strings (bytes) that represents :attr:`number`
    """
    num_binary = bin(number)[2:]
    while len(num_binary) % 8 != 0:
        num_binary = '0' + num_binary
    arr = []
    idx = 0
    while idx < len(num_binary):
        arr.append(num_binary[idx:idx + 8])
        idx += 8
    return arr
