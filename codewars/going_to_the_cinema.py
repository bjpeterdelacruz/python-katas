"""
Going to the Cinema

:Author: BJ Peter Dela Cruz
"""
from math import ceil


def movie(card, ticket, percent):
    """
    This function returns the number of times that John has to go to the cinema such that the price
    of buying tickets with a membership card will cost less than just buying tickets without one.
    With a membership card, the number of times that John goes to the cinema is the number of times
    the discount is applied to a ticket. For example, if the price of a ticket is $10, the discount
    for a ticket is 10%, and John goes to the cinema three times, then the total price of a ticket,
    including a membership card that costs $500, is 500 + 10 * (0.9 ^ 3) = $507.29.

    :param card: The price for a membership card.
    :param ticket: The price for a single ticket, with or without a membership card.
    :param percent: The discount as a percentage applied to a ticket if one has a membership card.
    :return: The number of times that John has to go to the cinema such that the total cost of a
    membership card and tickets rounded up to the nearest dollar will cost less than just tickets.
    """
    total_card = card + (ticket * percent)
    total_ticket = ticket
    count = 1
    while ceil(total_card) >= total_ticket:
        count += 1
        total_card += (ticket * percent ** count)
        total_ticket += ticket
    return count
