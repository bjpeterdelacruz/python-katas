"""
Strip URL Params

:Author: BJ Peter Dela Cruz
"""


def strip_url_params(url: str, params_to_strip: list[str] = None):
    """
    This function strips all duplicate query parameters from :attr:`str`, i.e. if the parameter 'a'
    appears twice in the URL, then the second 'a' is ignored. Also, any parameters that appear in
    :attr:`params_to_strip` will be removed from the URL entirely.

    :param url: The input string, the URL from which to strip query parameters
    :param params_to_strip: The input list of strings of query parameters to remove from :attr:`url`
    :return: The URL with some, all, or no parameters stripped
    """
    if "?" not in url:
        return url
    query_params = url[url.index("?") + 1:].split("&")
    queries = {}
    for param in query_params:
        str1, str2 = param.split("=")
        if str1 not in queries:
            queries[str1] = str2
    if params_to_strip is not None:
        for param in params_to_strip:
            queries.pop(param, None)
    if not queries:
        return url[:url.index("?")]
    query_params = '&'.join([f"{key}={value}" for key, value in queries.items()])
    return url[:url.index("?") + 1] + query_params
