"""
The 'if' Function

:Author: BJ Peter Dela Cruz
"""


def _if(boolean: bool, func1, func2):
    """
    This function calls the function :attr:`func1` if :attr:`boolean` is truthy. Otherwise, the
    function :attr:`func2` is called.

    :param boolean: A truthy or falsy value
    :param func1: A function to call if :attr:`boolean` is truthy
    :param func2: A function to call if :attr:`boolean` is falsy
    :return: None
    """
    if boolean:
        func1()
    else:
        func2()
