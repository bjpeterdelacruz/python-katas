"""
Address Book by State

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict

US_STATES = {
    'AK': 'Alaska',
    'AL': 'Alabama',
    'AR': 'Arkansas',
    'AS': 'American Samoa',
    'AZ': 'Arizona',
    'CA': 'California',
    'CO': 'Colorado',
    'CT': 'Connecticut',
    'DC': 'District of Columbia',
    'DE': 'Delaware',
    'FL': 'Florida',
    'GA': 'Georgia',
    'GU': 'Guam',
    'HI': 'Hawaii',
    'IA': 'Iowa',
    'ID': 'Idaho',
    'IL': 'Illinois',
    'IN': 'Indiana',
    'KS': 'Kansas',
    'KY': 'Kentucky',
    'LA': 'Louisiana',
    'MA': 'Massachusetts',
    'MD': 'Maryland',
    'ME': 'Maine',
    'MI': 'Michigan',
    'MN': 'Minnesota',
    'MO': 'Missouri',
    'MP': 'Northern Mariana Islands',
    'MS': 'Mississippi',
    'MT': 'Montana',
    'NA': 'National',
    'NC': 'North Carolina',
    'ND': 'North Dakota',
    'NE': 'Nebraska',
    'NH': 'New Hampshire',
    'NJ': 'New Jersey',
    'NM': 'New Mexico',
    'NV': 'Nevada',
    'NY': 'New York',
    'OH': 'Ohio',
    'OK': 'Oklahoma',
    'OR': 'Oregon',
    'PA': 'Pennsylvania',
    'PR': 'Puerto Rico',
    'RI': 'Rhode Island',
    'SC': 'South Carolina',
    'SD': 'South Dakota',
    'TN': 'Tennessee',
    'TX': 'Texas',
    'UT': 'Utah',
    'VA': 'Virginia',
    'VI': 'Virgin Islands',
    'VT': 'Vermont',
    'WA': 'Washington',
    'WI': 'Wisconsin',
    'WV': 'West Virginia',
    'WY': 'Wyoming'
}


def by_state(string: str) -> str:
    """
    This function sorts a list of addresses in alphabetical order by name and then in alphabetical
    order by state. The state abbreviation in the address is replaced by the full name of the state.

    :param string: A newline-delimited list of addresses
    :return: A string with the addresses sorted by name under each state with the latter sorted in
        alphabetical order
    """
    addresses = []
    for address in sorted(string.split("\n")):
        addr = address.split(",")
        if len(addr) > 1:
            addresses.append(addr)
    states = defaultdict(list)
    for address in addresses:
        addr = address[-1].split(" ")
        city, name = ' '.join(addr[:-1]), US_STATES[addr[-1]]
        address[-1] = f"{city} {name}"
        states[name].append("".join(address))
    state_names = sorted(states.keys())
    result = [f"\r\n {state}\r\n..... " + "\r\n..... ".join(states[state]) for state in state_names]
    return ''.join(result).strip()
