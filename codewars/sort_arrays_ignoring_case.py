"""
Sort Arrays (Ignoring Case)

:Author: BJ Peter Dela Cruz
"""


def sortme(words: list[str]) -> list[str]:
    """
    This function sorts the given list of words in case-insensitive alphabetical order.

    :param words: The input list of strings to sort
    :return: A case-insensitive sorted list
    """
    return sorted(words, key=lambda x: x.lower())
