"""
Thinking and Testing #04: Something Capitalized

:Author: BJ Peter Dela Cruz
"""


def something_capitalized(string: str) -> str:
    """
    This function returns a string with all but the last character in lowercase.

    :param string: The input string
    :return: The same string but with all except the last character in lowercase
    """
    return ' '.join([capitalize(word) for word in string.split()])


def capitalize(word: str) -> str:
    """
    This helper function capitalizes the last letter in the given string and turns all the other
    letters into lowercase.

    :param word: The string
    :return: The same string but with all except the last character in lowercase
    """
    return word.upper() if len(word) == 1 else word[:-1].lower() + word[-1].upper()
