"""
Loose Change

:Author: BJ Peter Dela Cruz
"""
from math import floor


def loose_change(cents):
    """
    This function counts the least amount of quarters, dimes, nickels, and pennies needed to
    represent :attr:`cents` and returns a map of coins and their counts.

    :param cents: The input integer, the loose change
    :return: A map containing the number of quarters, dimes, nickels, and pennies
    """
    change = {}
    cents = int(floor(cents))
    change, cents = make_change(cents, 25, change)
    change, cents = make_change(cents, 10, change)
    change, cents = make_change(cents, 5, change)
    change, _ = make_change(cents, 1, change)
    return {'Nickels': change[5], 'Pennies': change[1], 'Dimes': change[10], 'Quarters': change[25]}


def make_change(cents: int, value: int, change: dict[int, int]) -> tuple[dict[int, int], int]:
    """
    This helper function counts the number of coins of a particular face value that can be used to
    represent :attr:`cents`.

    :param cents: The loose change (amount)
    :param value: The face value of a coin
    :param change: A map containing the counts for each coin
    :return: A tuple containing :attr:`change` and the number of cents remaining
    """
    count = 0
    while cents >= value:
        count += 1
        cents -= value
    change[value] = count
    return change, cents
