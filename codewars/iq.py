"""
IQ Test

:Author: BJ Peter Dela Cruz
"""


def iq_test(numbers: str) -> int:
    """
    This function returns the index (1-based) of the number that is not like all the other numbers.
    For example, given "2 4 7 8 10", this function will return 3 because the third number is odd
    while the others are even, and given "11 13 15 16 17", this function will return 4 because the
    fourth number is even while the others are odd.

    :param numbers: The input string representing a space-delimited list of numbers
    :return: The index (1-based) of the number that is either odd or even
    """
    odd = []
    even = []
    for idx, number in enumerate(map(int, numbers.split())):
        if number % 2 == 0:
            even.append((idx + 1, number))
        else:
            odd.append((idx + 1, number))
        if (len(odd) == 1 and len(even) > 1) or (len(odd) > 1 and len(even) == 1):
            break
    return odd[0][0] if len(odd) == 1 else even[0][0]
