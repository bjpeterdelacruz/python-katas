"""
All Star Code Challenge #16

:Author: BJ Peter Dela Cruz
"""
from collections import Counter


def no_repeat(string: str) -> str:
    """
    This function returns the first character in :attr:`string` that appears only once.

    :param string: The input string
    :return: The first character in :attr:`string` that appears only once
    """
    return [letter_count for letter_count in Counter(string).items() if letter_count[1] == 1][0][0]
