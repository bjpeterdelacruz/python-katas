"""
Alternate Case

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase as lowercase


def alternate_case(string: str) -> str:
    """
    This function switches the case in the given string, i.e. all lowercase letters are turned to
    uppercase letters, and vice versa.

    :param string: The input string
    :return: The same string but with the cases of its letters switched
    """
    return ''.join([char.upper() if char in lowercase else char.lower() for char in string])
