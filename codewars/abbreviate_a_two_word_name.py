"""
Abbreviate Two Word Name

:Author: BJ Peter Dela Cruz
"""


def abbrev_name(name: str) -> str:
    """
    This function abbreviates the first name and last name of a person.

    :param name: A string that contains a first name and last name separated by spaces
    :return: An abbreviation
    """
    first, second = name.upper().split()
    return f'{first[0]}.{second[0]}'
