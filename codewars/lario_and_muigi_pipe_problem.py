"""
Lario and Muigi Pipe Problem

:Author: BJ Peter Dela Cruz
"""


def pipe_fix(nums: list[int]) -> list[int]:
    """
    This function generates a list of integers that starts with the lowest number in :attr:`nums`
    and ends with the highest number in :attr:`nums`. The step value for the increment is one. It is
    assumed that :attr:`nums` is already sorted in ascending order.

    :param nums: The input list of integers
    :return: A list of integers from the lowest number to the highest number, both ends inclusive
    """
    return list(range(nums[0], nums[-1] + 1))
