"""
Decipher Student Messages

:Author: BJ Peter Dela Cruz
"""
from math import sqrt


def decipher_message(message: str) -> str:
    """
    This function decodes the given message that was encoded using a grid whose length and width is
    the square root of the length of :attr:`message`. The original message was written from left to
    right starting from the top left of the grid. The message wrapped to the next line until the
    last character of the message is at the bottom right of the grid. The message was encoded by
    concatenating the characters together starting from the top to the bottom one column at a time.

    :param message: The input string, the message to decode
    :return: The decoded message
    """
    length = int(sqrt(len(message)))
    deciphered_message = []
    outer = 0
    while outer < length:
        inner = outer
        while inner < len(message):
            deciphered_message.append(message[inner])
            inner += length
        outer += 1
    return ''.join(deciphered_message)
