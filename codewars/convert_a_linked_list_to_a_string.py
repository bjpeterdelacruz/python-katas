"""
Convert Linked List to a String

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class Node:
    """
    This class represents a node in a singly linked list.
    """
    def __init__(self, data, next_node=None):
        """
        Initializes this node with data and optionally sets the node that will come after this node.

        :param data: The data for this node
        :param next_node: The node that will come after this node
        """
        self.data = data
        self.next = next_node


def stringify(node):
    """
    This function returns a string representation of the given linked list.

    :param node: The input node, the head of a linked list
    :return: A string representation of the linked list
    """
    arr = []
    while node:
        arr.append(str(node.data))
        node = node.next
    arr.append(str(node))
    return ' -> '.join(arr)
