"""
Thinkful: Order Filler

:Author: BJ Peter Dela Cruz
"""


def fillable(stock: dict, merchandise: str, count: int) -> bool:
    """
    This function returns True if the given merchandise is in stock and there is enough of the
    merchandise available to order, or False otherwise.

    :param stock: A dictionary representing all the merchandises in stock and their total quantity
    :param merchandise: The merchandise to order
    :param count: The quantity of the merchandise to order
    :return: True if there is enough of the merchandise in stock, False otherwise
    """
    return merchandise in stock and stock[merchandise] >= count
