"""
Adding Values of Arrays in a Shifted Way

:Author: BJ Peter Dela Cruz
"""


def sum_arrays(arrays: list[list], shift: int) -> list:
    """
    This function shifts all the integers in each sublist of :attr:`arrays` :attr:`shift` * N spaces
    to the right where N is the index (starting at 0) of the sublist in :attr:`arrays`. After all
    the integers in a sublist are shifted, they are summed with the integers in the list that is
    returned. It is assumed that each sublist contains the same number of integers.

    :param arrays: A list containing lists of integers
    :param shift: The value to shift all the integers in each sublist
    :return: A list containing sums
    """
    sum_array = [0] * (len(arrays[0]) + shift * (len(arrays) - 1))
    for row_idx, row in enumerate(arrays):
        for col_idx, cell in enumerate(row):
            sum_array[row_idx * shift + col_idx] += cell
    return sum_array
