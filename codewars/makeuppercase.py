"""
Make UpperCase

:Author: BJ Peter Dela Cruz
"""


def make_upper_case(string: str) -> str:
    """
    This function converts all the characters in the given string to uppercase.

    :param string: The input string
    :return: The same string but with all characters converted to uppercase
    """
    return string.upper()
