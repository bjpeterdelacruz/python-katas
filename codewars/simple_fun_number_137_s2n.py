"""
Simple Fun #137: S2N

:Author: BJ Peter Dela Cruz
"""


def s2n(base_limit: int, exp_limit: int) -> int:
    """
    This function finds the sum for the range 0 to :attr:`base_limit`, inclusive, for all exponents
    from 0 to :attr:`exp_limit`, inclusive. For example, if :attr:`base_limit` is 2 and
    :attr:`exp_limit` is 3, the function will perform the following sum:
    (0^0 + 0^1 + 0^2 + 0^3) + (1^0 + 1^1 + 1^2 + 1^3) + (2^0 + 2^1 + 2^2 + 2^3) = 20

    :param base_limit: The input integer, the last base number in the range
    :param exp_limit: The input integer, the last exponent in the range
    :return: The sum from 0 to :attr:`base_limit`, inclusive, for all exponents from 0 to
        :attr:`exp_limit`, inclusive
    """
    return sum(base ** exponent for exponent in range(0, exp_limit + 1)
                for base in range(0, base_limit + 1))
