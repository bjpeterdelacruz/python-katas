"""
Integer Difference

:Author: BJ Peter Dela Cruz
"""


def int_diff(arr: list[int], diff: int) -> int:
    """
    This function counts all the pairs of integers in the given list that have a difference equal to
    :attr:`diff`.

    :param arr: The input list of integers
    :param diff: The difference
    :return: The number of pairs that have a difference equal to :attr:`diff`
    """
    count = 0
    for idx, value in enumerate(arr):
        for idx2 in range(idx + 1, len(arr)):
            if abs(value - arr[idx2]) == diff:
                count += 1
    return count
