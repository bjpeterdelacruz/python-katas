"""
String Matchup

:Author: BJ Peter Dela Cruz
"""
from collections import Counter


def solve(first: list[str], second: list[str]) -> list[int]:
    """
    This function counts the number of times each string in :attr:`first` appears in :attr:`second`.

    :param first: The first list of strings
    :param second: The second list of strings
    :return: A list containing the number of times that each string in :attr:`first` appears in
        :attr:`second`
    """
    counts = Counter(first)
    return [counts[string] if string in counts else 0 for string in second]
