"""
Next Smaller Number with Same Digits

:Author: BJ Peter Dela Cruz
"""


def next_smaller(integer: int) -> int:
    """
    This function finds the next smallest number using only the same digits in :attr:`integer`.

    :param integer: The input integer
    :return: The next number that is smaller than :attr:`integer`
    """
    string = str(integer)
    if string == ''.join(sorted(string)):
        return -1
    idx = len(string) - 2
    while string[idx:len(string)] == ''.join(sorted(string[idx:len(string)])):
        idx -= 1
    suffix = ''.join(sorted(string[idx + 1:], reverse=True))
    digit = str(int(string[idx]) - 1)
    while digit not in suffix:
        digit = str(int(digit) - 1)
    prefix = string[:idx] + digit
    suffix = suffix.replace(digit, string[idx], 1)
    smaller_number = prefix + suffix
    return -1 if len(str(int(smaller_number))) < len(string) else int(smaller_number)
