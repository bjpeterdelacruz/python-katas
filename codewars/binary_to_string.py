"""
Binary to String

:Author: BJ Peter Dela Cruz
"""


def binary_to_string(binary: str) -> str:
    """
    This function converts the given binary string to a string containing ASCII/Unicode characters.

    :param binary: The input string, a binary string containing zeros and ones
    :return: A string containing ASCII/Unicode characters
    """
    return ''.join(map(lambda x: chr(int(x, 2)), binary.split('0b')[1:]))
