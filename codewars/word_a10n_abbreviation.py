"""
Word a10n (abbreviation)

:Author: BJ Peter Dela Cruz
"""
from string import ascii_letters


def abbreviate(sentence: str) -> str:
    """
    This function abbreviates all words in the given string that contain more than three characters.
    Words do not have to be delimited by spaces, e.g. "inter-continental" will become "i3r-c9l".

    :param sentence: The input string
    :return: A string containing abbreviated words
    """
    result = []
    abbreviation = []
    for character in sentence:
        if character in ascii_letters:
            abbreviation.append(character)
        else:
            if abbreviation:
                result.append(abbreviate_word(''.join(abbreviation)))
                abbreviation = []
            result.append(character)
    if abbreviation:
        result.append(abbreviate_word(''.join(abbreviation)))
    return ''.join(result)


def abbreviate_word(word: str) -> str:
    """
    This function abbreviates :attr:`word` if it contains more than three characters. Otherwise, the
    word is returned.

    :param word: The word to abbreviate
    :return: The abbreviated word if it contains more than three characters
    """
    return f'{word[0]}{len(word) - 2}{word[-1]}' if len(word) > 3 else word
