"""
Person Class Bug

:Author: BJ Peter Dela Cruz
"""

# pylint: disable=too-few-public-methods


class Person:
    """
    This class represents a person with a first name, last name, and age.
    """
    def __init__(self, first_name: str, last_name: str, age: int):
        """
        Correctly initializes an instance of a Person class.

        :param first_name: The first name of a person
        :param last_name: The last name of a person
        :param age: The person's age
        """
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.full_name = f"{self.first_name.strip()} {self.last_name.strip()}"
