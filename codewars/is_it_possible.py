"""
Check That the Situation is Correct

:Author: BJ Peter Dela Cruz
"""


def is_it_possible(field: str) -> bool:
    """
    This function checks whether the positions of all the X's and 0's on the given Tic-Tac-Toe board
    are legal assuming that X goes first.

    :param field: The input string, a Tic-Tac-Toe board with zero, one, or more X's and 0's
    :return: True if the positions of all the X's and 0's are correct, False otherwise
    """
    if len(field) != 9:
        return False
    if field.count("_") == 9:
        return True
    if field.count("X") < field.count("0") or field.count("X") - field.count("0") > 1:
        return False
    row_1 = field[0:3]
    row_2 = field[3:6]
    row_3 = field[6:9]
    diag_1 = field[0] + field[4] + field[8]
    diag_2 = field[2] + field[4] + field[6]
    col_1 = field[0] + field[3] + field[6]
    col_2 = field[1] + field[4] + field[7]
    col_3 = field[2] + field[5] + field[8]
    three_in_a_row = [row_1, row_2, row_3, diag_1, diag_2, col_1, col_2, col_3]
    for three_x in three_in_a_row:
        if three_x == "XXX":
            if field.count("X") != field.count("0") + 1:
                return False
    for three_o in three_in_a_row:
        if three_o == "000":
            if field.count("X") != field.count("0"):
                return False
    return True
