"""
Number of Divisions

:Author: BJ Peter Dela Cruz
"""


def divisions(number: int, divisor: int) -> int:
    """
    This function calculates the number of times that :attr:`number` can be divided by
    :attr:`divisor`, ignoring all remainders. When the remainder is less than :attr:`divisor`, then
    the total count is returned.

    :param number: The dividend
    :param divisor: The divisor
    :return: The number of times that :attr:`number` can be divided by :attr:`divisor`
    """
    count = 0
    while number >= divisor:
        number /= divisor
        count += 1
    return count
