"""
Simple Encryption #1: Alternating Split

:Author: BJ Peter Dela Cruz
"""


def decrypt(ciphertext: str, num: int) -> str:
    """
    This function decrypts the given ciphertext by first finding the midpoint in the ciphertext.
    Next, a character from the midpoint is added to an empty list, then a character from the
    beginning of the string is added. Then the next character after the midpoint is added to the
    list followed by the next character after the first character in the string. This process is
    repeated until the end of the string and the midpoint are reached.

    :param ciphertext: The input string to decrypt
    :param num: The number of times to repeat the aforementioned process
    :return: A plaintext, or the ciphertext if the string is empty or :attr:`num` is less than or
        equal to zero
    """
    if not ciphertext or num <= 0:
        return ciphertext
    plaintext = ciphertext
    midpoint = len(ciphertext) // 2
    for _ in range(0, num):
        temp = []
        offset = 0
        while midpoint + offset < len(plaintext):
            temp.append(plaintext[midpoint + offset])
            if offset < midpoint:
                temp.append(plaintext[offset])
            offset += 1
        plaintext = ''.join(temp)
    return plaintext


def encrypt(plaintext: str, num: int) -> str:
    """
    This function encrypts the given plaintext by taking all the characters at odd indexes in the
    plaintext and concatenating them with all the characters at even indexes in the same plaintext.
    This process is repeated :attr:`num` times.

    :param plaintext: The input string to encrypt
    :param num: The number of times to repeat the concatenation process
    :return: A ciphertext, or the plaintext if the string is empty or :attr:`num` is less than or
        equal to zero
    """
    if not plaintext or num <= 0:
        return plaintext
    ciphertext = plaintext
    for _ in range(0, num):
        ciphertext = take_chars(ciphertext, False) + take_chars(ciphertext)
    return ciphertext


def take_chars(text: str, even: bool = True) -> str:
    """
    This helper function produces a string using the characters at even indexes in :attr:`text` if
    :attr:`even` is True. If :attr:`even` is False, the characters at odd indexes are used to
    produce a new string.

    :param text: The input string
    :param even: True to use the characters at even indexes in :attr:`text` to produce a new string,
        False to use characters at odd indexes
    :return: A new string using characters from :attr:`text`
    """
    return ''.join([char for idx, char in enumerate(text) if idx % 2 == (0 if even else 1)])
