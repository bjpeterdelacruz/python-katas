"""
Exclamation Marks Series #5: Remove All Exclamation Marks from the End of Words

:Author: BJ Peter Dela Cruz
"""


def remove(string: str) -> str:
    """
    This function removes all the exclamation marks at the end of each word in :attr:`string`.

    :param string: The input string
    :return: A string that does not contain exclamation marks at the end of each word in it
    """
    temp = []
    for word in string.split():
        while word[-1] == '!':
            word = word[0:-1]
        temp.append(word)
    return ' '.join(temp)
