"""
Crossing Sum

:Author: BJ Peter Dela Cruz
"""


def crossing_sum(matrix: list[list[int]], row: int, col: int) -> int:
    """
    This function sums all the integers in a row in the given 2-dimensional list. It then adds all
    the integers in the given column to the sum. Note that the integer at the intersection of the
    row and column is not added twice.

    :param matrix: The input 2-dimensional list of integers
    :param row: The row in the list to add
    :param col: The column in the list to add
    :return: The sum of the row and column
    """
    total = sum(matrix[row])
    for current_row, _ in enumerate(matrix):
        if current_row != row:
            total += matrix[current_row][col]
    return total
