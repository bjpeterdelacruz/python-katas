"""
Sum of Multiples

:Author: BJ Peter Dela Cruz
"""


def sum_mul(number: int, maximum: int) -> int | str:
    """
    This function finds the sum of all multiples of :attr:`number` below :attr:`maximum`. The word
    "INVALID" is returned if :attr:`multiple` is less than 1 or :attr:`maximum` is less than 1.

    :param number: The number for which to find all multiples
    :param maximum: The maximum
    :return: The sum of all multiples of :attr:`number` below :attr:`maximum`
    """
    if number < 1 or maximum < 1:
        return "INVALID"
    total = 0
    number = 1
    while number * number < maximum:
        total += number * number
        number += 1
    return total
