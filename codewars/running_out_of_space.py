"""
Running Out of Space

:Author: BJ Peter Dela Cruz
"""


def spacey(array: list[str]) -> list[str]:
    """
    This function returns a list in which each element is a concatenation of all the previous
    elements and itself. For example, given ["I", "have", "no", "space", this function will return
    ["I", "Ihave", "Ihaveno", "Ihavenospace"].

    :param array: The input list of strings
    :return: A list in which element is a concatenation of all the previous elements and itself
    """
    new_array = [array[0]]
    for idx in range(1, len(array)):
        new_array.append(f"{new_array[-1]}{array[idx]}")
    return new_array
