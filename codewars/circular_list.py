"""
Circular List

:Author: BJ Peter Dela Cruz
"""


class CircularList:
    """
    This class represents a circular list. A call to next() when the cursor is on the last element
    will move the cursor to the first element, and a call to prev() when the cursor is on the first
    element will move the cursor to the last element.
    """
    def __init__(self, *arr):
        """
        Initializes the cursor to point to a position before the first element in the list. A call
        to next() will return the first element, and a call to prev() will return the last element.
        If :attr:`arr` is empty, a ValueError is raised.

        :param arr: The input list, must not be empty
        """
        if len(arr) == 0:
            raise ValueError("list must not be empty")
        self.arr = arr
        self.curr_idx = -1
        self.size = len(self.arr)

    def next(self):
        """
        Returns the next element in the list. If the cursor is on the last element, the first
        element is returned.

        :return: The next element in the list
        """
        self.curr_idx += 1
        if self.curr_idx == self.size:
            self.curr_idx = 0
        return self.arr[self.curr_idx]

    def prev(self):
        """
        Returns the previous element in the list. If the cursor is on the first element, the last
        element is returned.

        :return: The previous element in the list
        """
        self.curr_idx -= 1
        if self.curr_idx < 0:
            self.curr_idx = self.size - 1
        return self.arr[self.curr_idx]
