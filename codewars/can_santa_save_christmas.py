"""
Can Santa Save Christmas?

:Author: BJ Peter Dela Cruz
"""
from datetime import datetime


def determine_time(times: list[str]) -> bool:
    """
    This function returns True if the sum of all the durations in :attr:`times` is less than twenty-
    four hours. Each duration is represented as hours, minutes, and seconds in the format HH:MM:SS.

    :param times: The input list of strings, each string representing a duration or the amount of
        time it takes for Santa Claus to deliver a present
    :return: True if the sum of all durations is less than 24 hours, False otherwise
    """
    hours = 0
    for time in times:
        present = datetime.strptime(time, "%H:%M:%S")
        hours += present.hour + (present.minute / 60) + (present.second / (60 * 60))
    return hours < 24
