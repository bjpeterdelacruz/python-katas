"""
Yoga Class

:Author: BJ Peter Dela Cruz
"""


def yoga(classroom: list[list[int]], poses: list[int]) -> int:
    """
    This function returns the total number of students who can complete a yoga pose in the given
    list of poses. A student can complete a pose if the sum of the skill levels of all the students
    in the row that contains the student and the student's skill level is equal to or greater than
    the skill level of the pose.

    :param classroom: The input list of list of integers, each row representing students in a
        classroom
    :param poses: The input list of integers, each representing the skill level needed to accomplish
        a pose
    :return: The sum of all students who can accomplish each pose in the list of poses
    """
    return sum(1 for pose in poses for row in classroom for per in row if per + sum(row) >= pose)
