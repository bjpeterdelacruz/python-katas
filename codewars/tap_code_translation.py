"""
Tap Code Translation

:Author: BJ Peter Dela Cruz
"""


def tap_code_translation(text: str) -> str:
    """
    This function turns the given string of lowercase ASCII letters into a string containing dots
    and spaces. All instances of 'k' will be replaced by 'c' before the translation takes place.
    Each character in :attr:`text` will be converted to one or more dots, separated by a space, and
    finally followed by one or more dots. The first set of dots represents the index of the row
    (starting from 1) in a 2-dimensional list in which the character is found, and the second set of
    dots represents the index of the column (also starting from 1) in which the character is found.
    Finally, each translated character is separated by a space in the string that is returned.

    :param text: The input string containing lowercase ASCII letters
    :return: A string containing a series of dots and spaces
    """
    if 'k' in text:
        text = 'c'.join(text.split('k'))
    matrix = [[1, 'a', 'b', 'c', 'd', 'e'],
              [2, 'f', 'g', 'h', 'i', 'j'],
              [3, 'l', 'm', 'n', 'o', 'p'],
              [4, 'q', 'r', 's', 't', 'u'],
              [5, 'v', 'w', 'x', 'y', 'z']]
    dots = []
    for character in text:
        for row in matrix:
            if character in row:
                dots.append(f"{'.' * row[0]} {'.' * row.index(character)}")
                break
    return ' '.join(dots)
