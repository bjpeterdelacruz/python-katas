"""
Factor Sum

:Author: BJ Peter Dela Cruz
"""


def factor_sum(integer: int) -> int:
    """
    This function takes a positive integer and replaces it with the sum of its prime factors. It
    then takes that sum and replaces it with the sum of its prime factors. It continues this process
    until a sum is already found in the list of the sums computed so far. In this case, the most
    recent sum that was added to the list is returned.

    :param integer: The input integer
    :return: The final sum
    """
    numbers = []
    result = integer
    while True:
        result = sum(get_prime_factors(result))
        if result in numbers:
            return numbers.pop()
        numbers.append(result)


def get_prime_factors(integer: int) -> list[int]:
    """
    This helper function returns a list of all the prime factors of :attr:`integer`.

    :param integer: The integer for which to find its prime factors
    :return: A list of all the prime factors of :attr:`integer`
    """
    factors = []
    number = 2
    while True:
        if integer % number == 0:
            factors.append(number)
            integer /= number
        else:
            number += 1
        if integer == 1:
            break
    return factors
