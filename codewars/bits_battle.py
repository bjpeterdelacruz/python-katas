"""
Bits Battle

:Author: BJ Peter Dela Cruz
"""


def bits_battle(numbers: list[int]) -> str:
    """
    This function counts the number of zeros in all even numbers and the number of ones in all odd
    numbers. The number zero is not counted. Evens win if the number of ones is greater than the
    number of zeros, odds win if the number of zeros is greater than the number of ones, and a tie
    if both numbers are the same.

    :param numbers: The input list of integers
    :return: A string stating the winner or a tie
    """
    groups = {0: [], 1: []}
    for number in numbers:
        if number != 0:
            groups[number % 2].append(str(bin(number))[2:])
    evens = sum(number.count('0') for number in groups[0])
    odds = sum(number.count('1') for number in groups[1])
    if odds > evens:
        return "odds win"
    return "evens win" if evens > odds else "tie"
