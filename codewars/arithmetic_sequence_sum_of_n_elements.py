"""
Arithmetic Sequence - Sum of N Elements

:Author: BJ Peter Dela Cruz
"""


def arithmetic_sequence_sum(number: int, constant: int, max_length: int) -> int:
    """
    This function produces an arithmetic sequence of integers, i.e. the difference between one
    element in a list and the next element is constant. The list that is returned will be of size
    :attr:`max_length`. A ValueError is raised if :attr:`max_length` is less than zero. Each element
    in the list is the sum of :attr:`number` and the number of occurrences of :attr:`constant` (the
    number of occurrences is equal to the element's position in the list, and position numbers start
    with 0).

    :param number: An integer
    :param constant: The integer to add zero or more times with :attr:`number`
    :param max_length: The maximum number of integers in the arithmetic sequence
    :return: The sum of the arithmetic sequence
    """
    if max_length < 0:
        raise ValueError
    return sum(number + (constant * count) for count in range(0, max_length))
