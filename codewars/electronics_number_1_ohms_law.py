"""
Electronics #1: Ohm's Law

:Author: BJ Peter Dela Cruz
"""


def ohms_law(input_string: str) -> str:
    """
    This function calculates the voltage in volts (V), current in amps (A), or resistance in ohms
    (R) using Ohm's Law, which is V = I * R.

    :param input_string: A string that contains two values: A and R, A and V, or R and V
    :return: The voltage (if A and R are given), current (if R and V are given), or resistance (if A
        and V are given) rounded to six decimal places
    """
    arr = sorted(input_string.split(), key=lambda x: x[-1])
    values = list(map(lambda x: float(x[0:-1]), arr))
    if arr[0][-1] == 'A':
        if arr[1][-1] == 'R':
            return f'{round(values[0] * values[1], 6)}V'
        return f'{round(values[1] / values[0], 6)}R'
    return f'{round(values[1] / values[0], 6)}A'
