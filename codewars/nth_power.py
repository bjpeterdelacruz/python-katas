"""
N-th Power

:Author: BJ Peter Dela Cruz
"""


def index(array: list[int], power: int) -> int:
    """
    This function raises an integer at index :attr:`power` in :attr:`array` to the power
    :attr:`power`. If :attr:`power` is greater than or equal to the length of :attr:`array`, -1 is
    returned.

    :param array: The input list of integers
    :param power: The exponent
    :return: An integer raised to the power :attr:`power`, or -1 if :attr:`power` is greater than or
        equal to the length of :attr:`array`
    """
    return -1 if len(array) <= power else array[power] ** power
