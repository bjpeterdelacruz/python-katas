"""
Middle Me

:Author: BJ Peter Dela Cruz
"""


def middle_me(number: int, key: str, text: str) -> str:
    """
    This function takes :attr:`key` and puts it in the middle of :attr:`text` that is repeated
    :attr:`number` times. If :attr:`number` is odd, then :attr:`key` is simply returned.

    :param number: The number of times to repeat :attr:`text`
    :param key: The string to put in the middle of :attr:`text` after it is repeated :attr:`number`
        times
    :param text: The string to repeat :attr:`number` times
    :return: A string
    """
    return key if number % 2 == 1 else f'{text * (number // 2)}{key}{text * (number // 2)}'
