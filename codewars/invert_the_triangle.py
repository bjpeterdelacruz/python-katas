"""
Invert Triangle

:Author: BJ Peter Dela Cruz
"""


def invert_triangle(triangle: str) -> str:
    """
    This function inverts the given triangle, which is represented as a string, and then replaces
    each space character with a pound sign, and vice versa.

    :param triangle: A newline-delimited string that represents a triangle
    :return: An inverted triangle
    """
    inverted = reversed(triangle.split('\n'))
    result = []
    for line in inverted:
        result.append(''.join(['#' if character == ' ' else ' ' for character in line]))
    return '\n'.join(result)
