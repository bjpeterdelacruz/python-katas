"""
Float Precision

:Author: BJ Peter Dela Cruz
"""


def solution(number: float) -> float:
    """
    This function rounds the given floating point number to two decimal places. For example, given
    123.456, the function will return 123.46.

    :param number: The input floating point number
    :return: The number rounded to two decimal places
    """
    return round(number * 100) / 100
