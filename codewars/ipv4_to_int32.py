"""
IPv4 to int32

:Author: BJ Peter Dela Cruz
"""


def ip_to_int32(ip_addr: str) -> int:
    """
    This function converts the given IP address string to a 32-bit integer.

    :param ip_addr: The input string, a valid IP address
    :return: The 32-bit integer representing the IP address
    """
    return int(''.join(map(lambda x: bin(int(x))[2:].rjust(8, '0'), ip_addr.split('.'))), 2)
