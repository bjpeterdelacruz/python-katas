"""
Even Numbers in an Array

:Author: BJ Peter Dela Cruz
"""


def even_numbers(arr: list[int], number: int) -> list[int]:
    """
    This function retrieves the last :attr:`number` even numbers from the given list. The even
    numbers that are returned will be in the same order that they appear in :attr:`arr`.

    :param arr: The input list of integers
    :param number: The number of even numbers to retrieve
    :return: A list of even integers
    """
    count = 0
    result = []
    idx = -1
    while count < number:
        if arr[idx] % 2 == 0:
            result.insert(0, arr[idx])
            count += 1
        idx -= 1
    return result
