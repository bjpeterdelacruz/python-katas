"""
Sum of Digits

:Author: BJ Peter Dela Cruz
"""


def sum_of_digits(integer) -> str:
    """
    This function sums all the digits or numeric characters in :attr:`integer` and returns a string
    that is an equation that includes the sum. If :attr:`integer` contains a non-numeric character,
    an empty string is returned.

    :param integer: The input integer or string
    :return: An equation that sums all the digits in :attr:`integer`, or an empty string if
        non-numeric characters are found in :attr:`integer`
    """
    try:
        integers = [int(character) for character in str(integer)]
        equation = ' + '.join(map(str, integers))
        return f'{equation} = {sum(integers)}'
    except ValueError:
        return ""
