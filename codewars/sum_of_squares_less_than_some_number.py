"""
Sum of Squares Less Than Some Number

:Author: BJ Peter Dela Cruz
"""


def get_number_of_squares(number: int) -> int:
    """
    This function calculates the square of each integer from 1 until the sum of the squares is
    greater than or equal to :attr:`number`. The last integer in the sequence is returned. For
    example: if :attr:`number` is 15, then 1^2 + 2^2 + 3^2 = 1 + 4 + 9 = 14 < 15, so 3 is returned.

    :param number: The integer which the sum must not be greater than or equal to
    :return: The last integer in the sequence before it is squared
    """
    base = total = 0
    while total < number:
        base += 1
        total += (base ** 2)
    return base - 1
