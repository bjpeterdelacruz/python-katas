"""
Find the Second Largest Integer in Array

:Author: BJ Peter Dela Cruz
"""


def find_2nd_largest(arr: list):
    """
    This function finds the second largest integer in the given list. All non-integer elements are
    filtered out of the list before the search for the second largest element proceeds.

    :param arr: The input list, may contain integers as well as non-integers
    :return: The second largest integer in :attr:`arr`
    """
    filtered_list = list(filter(lambda x: isinstance(x, int), arr))
    if len(set(filtered_list)) == 1:
        return None
    largest = max(filtered_list)
    return max(filter(lambda x: x != largest, filtered_list))
