"""
Check Digit

:Author: BJ Peter Dela Cruz
"""


def check_digit(number: int, index1: int, index2: int, digit: int) -> bool:
    """
    This function returns True if :attr:`digit` is found in :attr:`number` between the two indexes,
    inclusive.

    :param number: A number
    :param index1: The start index
    :param index2: The end index
    :param digit: The digit to find in :attr:`number`
    :return: True if :attr:`digit` is found in :attr:`number` between the two indexes, inclusive
    """
    if index2 < index1:
        index1, index2 = index2, index1
    return str(digit) in str(number)[index1:index2 + 1]
