"""
Direction Reduction

:Author: BJ Peter Dela Cruz
"""


def direction_reduction(directions: list[str]) -> list[str]:
    """
    This function reduces the given list of directions such that the directions that are directly
    opposite each other are cancelled out.

    :param directions: The input list of directions ("NORTH", "SOUTH", "EAST", and "WEST")
    :return: A simplified list of directions
    """
    string = ' '.join(directions)
    while True:
        string = string.replace("NORTH SOUTH", "").replace("SOUTH NORTH", "")\
            .replace("EAST WEST", "").replace("WEST EAST", "")
        if "NORTH SOUTH" not in string and "SOUTH NORTH" not in string \
                and "EAST WEST" not in string and "WEST EAST" not in string:
            break
    return string.split()
