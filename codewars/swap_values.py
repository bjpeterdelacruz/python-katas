"""
Swap Values

:Author: BJ Peter Dela Cruz
"""


def swap_values(args):
    """
    This function swaps the first two elements of the given list. It is assumed that the list
    contains at least two elements.

    :param args: The input list
    :return: None, the input list itself is modified
    """
    args[0], args[1] = args[1], args[0]
