"""
Sum Even Fibonacci Numbers

:Author: BJ Peter Dela Cruz
"""


def sum_even_fibonacci(limit: int) -> int:
    """
    This function returns the sum of all even Fibonacci numbers from 2 up to and including
    :attr:`limit`.

    :param limit: The input integer, the limit for the Fibonacci sequence (inclusive)
    :return: The sum of all even Fibonacci numbers up to and including :attr:`limit`
    """
    if limit <= 1:
        return 0
    fibonacci = [1, 2]
    sum_even = 2
    while True:
        next_fib = fibonacci[-2] + fibonacci[-1]
        fibonacci.append(next_fib)
        if next_fib <= limit and next_fib % 2 == 0:
            sum_even += next_fib
        if next_fib >= limit:
            break
    return sum_even
