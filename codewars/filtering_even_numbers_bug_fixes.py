"""
Filtering Even Numbers (Bug Fixes)

:Author: BJ Peter Dela Cruz
"""


def kata_13_december(lst: list[int]) -> list[int]:
    """
    This function filters out all the even numbers in the given list of integers.

    :param lst: The input list of integers
    :return: A list containing only odd numbers
    """
    return [number for number in lst if number % 2 == 1]
