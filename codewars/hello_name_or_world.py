"""
Hello, Name or World!

BJ Peter Dela Cruz
"""


def hello(*args: str) -> str:
    """
    This function returns "Hello, World!" if a string is not provided or the string is empty.
    Otherwise, the first letter of the string is capitalized, and all the other characters are
    turned into lowercase. Then the string is returned as part of a greeting.

    :param args: The input string (optional)
    :return: A greeting
    """
    if not args or not args[0]:
        return 'Hello, World!'
    name = args[0]
    if len(name) > 1:
        return f'Hello, {name[0].upper() + name[1:].lower()}!'
    return f'Hello, {name[0].upper()}!'
