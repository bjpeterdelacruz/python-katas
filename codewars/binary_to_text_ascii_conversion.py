"""
Binary to Text (ASCII) Conversion

:Author: BJ Peter Dela Cruz
"""


def binary_to_string(binary: str) -> str:
    """
    This function converts the given binary string to ASCII text. The binary string is assumed to be
    valid, i.e. its length is a multiple of 8. If the length is zero, an empty string is returned.

    :param binary: The input string containing binary numbers (0's and 1's)
    :return: A string containing ASCII text
    """
    if not binary:
        return ""
    ascii_characters = []
    binary_characters = []
    for character in binary:
        binary_characters.append(character)
        if len(binary_characters) == 8:
            ascii_characters.append(chr(int(''.join(binary_characters), 2)))
            binary_characters.clear()
    return ''.join(ascii_characters)
