"""
Vigenere Cipher Helper

:Author: BJ Peter Dela Cruz
"""


class VigenereCipher:
    """
    This class will encode and decode a string using the Vigenere Cipher.
    """
    def __init__(self, key: str, alphabet: str):
        """
        Initializes the Vigenere table using the given :attr:`alphabet`.

        :param key: The key that will be repeated for encoding plaintext and decoding ciphertext
        :param alphabet: The set of characters used to generate the Vigenere table
        """
        self._vigenere_table = {}
        self._key = key
        self._alphabet = alphabet
        self._setup_vigenere_table()

    def encode(self, plaintext: str) -> str:
        """
        Encodes the given :attr:`plaintext` and returns a ciphertext.

        :param plaintext: The plaintext to encode
        :return: A ciphertext
        """
        new_key = self._generate_key(plaintext)
        ciphertext = []
        idx = 0
        for character in plaintext:
            if character in self._vigenere_table:
                ciphertext.append(self._vigenere_table[character][new_key[idx]])
            else:
                ciphertext.append(character)
            idx += 1
        return ''.join(ciphertext)

    def decode(self, ciphertext: str) -> str:
        """
        Decodes the given :attr:`ciphertext` and returns a plaintext.

        :param ciphertext: The ciphertext to decode
        :return: A plaintext
        """
        new_key = self._generate_key(ciphertext)
        plaintext = []
        idx = 0
        for character in ciphertext:
            selected_row = self._vigenere_table[new_key[idx]]
            is_found = False
            for key, value in selected_row.items():
                if value == ciphertext[idx]:
                    plaintext.append(key)
                    is_found = True
                    break
            if not is_found:
                plaintext.append(character)
            idx += 1
        return ''.join(plaintext)

    def _setup_vigenere_table(self) -> None:
        shift = 0
        for plaintext in self._alphabet:
            self._vigenere_table[plaintext] = {}
            for key in self._alphabet:
                self._vigenere_table[plaintext][key] = self._shift_character(key, shift)
            shift += 1

    def _generate_key(self, text: str) -> str:
        key = self._key
        while len(key) < len(text):
            key += self._key
        return key[:len(text)]

    def _shift_character(self, key: str, shift: int) -> str:
        return self._alphabet[(self._alphabet.index(key) + shift) % len(self._alphabet)]
