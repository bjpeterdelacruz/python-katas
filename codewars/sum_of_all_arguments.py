"""
Sum of All Arguments

:Author: BJ Peter Dela Cruz
"""


def sum_args(*args):
    """
    This function sums together all the elements in the given list by delegating to the internal
    sum function.

    :param args: The input list
    :return: The sum of all the elements in :attr:`args`
    """
    return sum(args)
