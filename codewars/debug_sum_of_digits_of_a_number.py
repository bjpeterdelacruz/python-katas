"""
Debug Sum of Digits of a Number

:Author: BJ Peter Dela Cruz
"""


def get_sum_of_digits(number: int) -> int:
    """
    This function sums all the digits in the given integer and returns the result.

    :param number: The input integer
    :return: The sum of all the digits in :attr:`number`
    """
    return sum(int(digit) for digit in str(number))
