def remove_nth_element(arr: list, index: int) -> list:
    return [number for idx, number in enumerate(arr) if idx != index]
