"""
Zip With

:Author: BJ Peter Dela Cruz
"""


def zip_with(func, list_1: list, list_2: list) -> list:
    """
    This function zips the given lists together using the built-in zip function and then applies
    the given function :attr:`func` to each element from both lists.

    :param func: The function to apply to each element from both :attr:`list_1` and :attr:`list_2`
    :param list_1: The first input list
    :param list_2: The second input list
    :return: A list of results after zipping both lists and then applying :attr:`func` to each
        element in both lists
    """
    return [func(res[0], res[1]) for res in zip(list_1, list_2)]
