"""
Ciphers #1 - The 01 Cipher

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase as lc


def encode(string: str) -> str:
    """
    This function converts all the ASCII characters in the given string to ones if the index in
    which they appear in the alphabet is odd and zeros if the index is even. All non-ASCII
    characters are not touched.

    :param string: The input string
    :return: An encoded string containing zeros, ones, and non-ASCII characters (if present)
    """
    return ''.join([char if char not in lc else "1" if lc.index(char) % 2 == 1 else "0"
                    for char in string.lower()])
