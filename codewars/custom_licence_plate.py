"""
Custom Licence Plate

:Author: BJ Peter Dela Cruz
"""
import re


def licence_plate(string: str) -> str:
    """
    This function creates a custom licence plate from the given string. The license plate has the
    following characteristics: it must be between 2-8 characters long and contain only letters
    [A-Z], numbers [0-9], and dashes [-]; the letters and numbers are separated by a dash; it does
    not start or end with a dash and does not contain two or more consecutive dashes; and it is not
    made up of only numbers.

    :param string: The input string from which to create a custom licence plate
    :return: The custom licence plate, or "not possible" if it is not possible to create a licence
        plate that satisfies the aforementioned requirements
    """
    if string.isnumeric() or not string:
        return "not possible"
    string_list = list(string.upper())
    for idx, char in enumerate(string_list):
        if not char.isalnum():
            string_list[idx] = '-'
    lic_plate = '-'.join([char for char in re.split(r'(\d+)', ''.join(string_list)) if char != ''])
    while "--" in lic_plate:
        lic_plate = lic_plate.replace("--", "-")
    if lic_plate == "-":
        return "not possible"
    while "-" in lic_plate[0]:
        lic_plate = lic_plate[1:]
    if len(lic_plate) > 8:
        lic_plate = lic_plate[:8]
    while "-" in lic_plate[-1]:
        lic_plate = lic_plate[:-1]
    return lic_plate if len(lic_plate) >= 2 and not lic_plate.isnumeric() else "not possible"
