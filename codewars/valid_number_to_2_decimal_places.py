"""
Valid Number to 2 Decimal Places

:Author: BJ Peter Dela Cruz
"""
import re


def valid_number(number: str) -> bool:
    """
    This function returns True if the given string represents a valid number with two decimal
    places, False otherwise. Examples of valid input include: "+1.23", "-1.00", "-.50", "0.01".
    Examples of invalid input include: "5", "-1.0", "+9.999", "hello 123.45".

    :param number: The input string representing a number
    :return: True if the string represents a valid number with two decimal places, False otherwise
    """
    return re.search("^[-+]?\\d*\\.\\d\\d$", number) is not None
