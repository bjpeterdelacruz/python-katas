"""
Find the Index of the Second Occurrence of a Letter in a String

:Author: BJ Peter Dela Cruz
"""


def second_symbol(string: str, symbol: str) -> int:
    """
    This function returns the index of the second occurrence of :attr:`symbol` in :attr:`string`. If
    :attr:`symbol` does not occur or occurs only once in the string, then -1 is returned.

    :param string: The string in which to search for :attr:`symbol`
    :param symbol: The string to find in :attr:`string`
    :return: The index of the second occurrence of :attr:`symbol`
    """
    try:
        idx = string.index(symbol)
        idx2 = string[idx + 1:].index(symbol)
    except ValueError:
        return -1
    return idx + idx2 + 1
