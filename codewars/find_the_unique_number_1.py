"""
Find the Unique Element

:Author: BJ Peter Dela Cruz
"""


def find_uniq(arr):
    """
    This function finds the first element that is unique in the given list in O(n) time.

    :param arr: The input list
    :return: The unique element in :attr:`arr`
    """
    elements = set()
    elements.add(arr[0])
    elements.add(arr[1])
    elements.add(arr[2])
    if len(elements) > 1:
        if arr[0] == arr[1]:
            return arr[2]
        if arr[0] == arr[2]:
            return arr[1]
        return arr[0]
    elem = None
    for element in arr:
        if element not in elements:
            elem = element
            break
    return elem
