"""
Sum Without Highest and Lowest Number

:Author: BJ Peter Dela Cruz
"""


def sum_array(arr):
    """
    This function sums all the numbers in :attr:`arr` except the largest and smallest numbers.

    :param arr: The input list of numbers
    :return: The sum of all the numbers in the input list except the largest and smallest numbers
    """
    if not arr or len(arr) == 1:
        return 0
    sorted_arr = sorted(arr)
    return sum(arr) - sorted_arr[0] - sorted_arr[-1]
