"""
Complete Series

:Author: BJ Peter Dela Cruz
"""


def complete_series(seq: list[int]) -> list[int]:
    """
    This function returns a list of integers from 0 to the highest integer in the given list. If the
    list does not contain unique integers, a list with the integer 0 will be returned.

    :param seq: The input list of integers
    :return: A list of integers from 0 to N, inclusive; or a list containing only 0
    """
    return list(range(0, sorted(seq)[-1] + 1)) if len(seq) == len(set(seq)) else [0]
