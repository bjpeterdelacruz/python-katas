"""
Take the Derivative

:Author: BJ Peter Dela Cruz
"""


def derive(coefficient: int, exponent: int) -> str:
    """
    This function returns the derivative of the function Nx^M where N is :attr:`coefficient` and M
    is :attr:`exponent`. For example, given :attr:`coefficient` = 5 and :attr:`exponent` = 4, the
    string "20x^3" is returned.

    :param coefficient: The coefficient of the function
    :param exponent: The exponent of the function
    :return: The derivative of the function
    """
    return f'{exponent * coefficient}x^{exponent - 1}'
