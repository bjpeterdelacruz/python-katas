"""
Find the Capitals

:Author: BJ Peter Dela Cruz
"""


def capital(capitals: list[dict[str, str]]) -> list[str]:
    """
    This function returns a list of strings describing the capitals of U.S. states and/or countries.

    :param capitals: A map containing U.S. state and/or country names and their capitals
    :return: A list of strings describing the capitals of U.S. states and/or countries
    """
    return [f'The capital of '
            f'{region["state"] if "state" in region else region["country"]} is {region["capital"]}'
            for region in capitals]
