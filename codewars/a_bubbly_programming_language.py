"""
Bubbly Programming Language

:Author: BJ Peter Dela Cruz
"""
stack = []


def start(func):
    """
    This function represents the start of a program written in the Bubbly programming language. This
    function clears the stack used in the program.

    :param func: The function to call after this function
    :return: The function to call
    """
    stack.clear()
    return func


def end():
    """
    This function represents the end of a program.

    :return: The top of the stack
    """
    return stack[0]


def push(integer: int):
    """
    This function pushes the given integer to the top of the stack.

    :param integer: The integer to put on the top of the stack
    :return: A lambda function that represents the next function to call
    """
    stack.append(integer)
    return lambda x: x


def add(func):
    """
    This function pops the top two items from the top of the stack, sums them together, and then
    puts the result on the top of the stack.

    :param func: The function to call after this function
    :return: The function to call. If the function to call is :func:`end`, the result of that
        function is returned.
    """
    stack.append(stack.pop() + stack.pop())
    if func.__name__ == "end":
        return func()
    return func


def sub(func):
    """
    This function pops the top two items from the top of the stack, subtracts the second one that
    was popped from the first one, and then puts the result on the top of the stack.

    :param func: The function to call after this function
    :return: The function to call. If the function to call is :func:`end`, the result of that
        function is returned.
    """
    stack.append(stack.pop() - stack.pop())
    if func.__name__ == "end":
        return func()
    return func


def mul(func):
    """
    This function pops the top two items from the top of the stack, multiples them together, and
    then puts the result on the top of the stack.

    :param func: The function to call after this function
    :return: The function to call. If the function to call is :func:`end`, the result of that
        function is returned.
    """
    stack.append(stack.pop() * stack.pop())
    if func.__name__ == "end":
        return func()
    return func


def div(func):
    """
    This function pops the top two items from the top of the stack, divides the first one that was
    popped by the second one, and then puts the result on the top of the stack. It is assumed that
    the second item is not equal to zero.

    :param func: The function to call after this function
    :return: The function to call. If the function to call is :func:`end`, the result of that
        function is returned.
    """
    stack.append(stack.pop() // stack.pop())
    if func.__name__ == "end":
        return func()
    return func
