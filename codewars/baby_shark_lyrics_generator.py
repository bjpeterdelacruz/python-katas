"""
Baby Shark Lyrics Generator

:Author: BJ Peter Dela Cruz
"""

# pylint: disable=bad-indentation
# pylint: disable=invalid-name


def baby_shark_lyrics():
 """
 This function generates the lyrics to the Baby Shark song in under 300 characters.

 :return: The lyrics to the Baby Shark song
 """
 s = "shark"
 f = [f"Baby {s}",f"Mommy {s}",f"Daddy {s}",f"Grandma {s}",f"Grandpa {s}","Let's go hunt"]
 d = ' '.join(["doo"]*6)
 l = []
 for t in f:
  l.extend([f"{t}, {d}"]*4)
  l[-1] = f"{t}!"
 l.append("Run away,…")
 return '\n'.join(l)
