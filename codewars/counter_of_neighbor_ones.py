"""
Counter of Neighbor Ones

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def ones_counter(arr: list[int]) -> list[int]:
    """
    This function groups together continuous ones from the list of ones and zeros, and returns a
    list that contains the number of ones in each group.

    :param arr: The input list of integers
    :return: A list containing the number of ones in each group of continuous ones
    """
    return [len(list(group)) for key, group in groupby(arr) if key == 1]
