"""
Make Acronym

:Author: BJ Peter Dela Cruz
"""


def to_acronym(string: str) -> str:
    """
    This function creates an acronym from the given string, which consists of words separated by one
    or more whitespace characters. All the letters in the acronym are capitalized before it is
    returned.

    :param string: The input string
    :return: An acronym
    """
    return ''.join([word[0].upper() for word in string.split()])
