"""
Filter Long Words

:Author: BJ Peter Dela Cruz
"""


def filter_long_words(sentence: str, minimum: int) -> list[str]:
    """
    This function returns a list of words that contain more than :attr:`minimum` characters.

    :param sentence: The input string
    :param minimum: The input integer
    :return: A list of words in :attr:`sentence` that contain more than :attr:`minimum` characters
    """
    return [word for word in sentence.split() if len(word) > minimum]
