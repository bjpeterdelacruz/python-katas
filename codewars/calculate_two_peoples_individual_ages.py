"""
Calculate Two People's Individual Ages

:Author: BJ Peter Dela Cruz
"""


def get_ages(sum_of_ages: int, difference: int):
    """
    This function calculates the ages of two people given the sum of their ages and the difference
    between their ages.

    :param sum_of_ages: The sum of the ages of two people
    :param difference: The difference between the two ages
    :return: The ages of two people (oldest first), or None if the ages could not be calculated
    """
    if sum_of_ages < 0 or difference < 0:
        return None
    oldest = difference
    youngest = 0
    while oldest + youngest < sum_of_ages:
        oldest += 0.5
        youngest += 0.5
    return None if oldest + youngest > sum_of_ages else (oldest, youngest)
