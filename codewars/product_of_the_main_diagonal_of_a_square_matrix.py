"""
Product of the Main Diagonal of a Square Matrix

:Author: BJ Peter Dela Cruz
"""
from functools import reduce
from operator import mul


def main_diagonal_product(matrix: list[list[int]]) -> int:
    """
    This function calculates the product of all the integers in the main diagonal (upper left to
    bottom right).

    :param matrix: A two-dimensional list of integers
    :return: The product of all the integers in the main diagonal
    """
    return reduce(mul, [row[idx] for idx, row in enumerate(matrix)], 1)
