"""
Refactored Greeting

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class Person:
    """
    This class represents a person.
    """
    def __init__(self, name: str):
        """
        Sets the name for this Person object.

        :param name: A person's name
        """
        self.name = name

    def greet(self, other_person_name: str) -> str:
        """
        Returns a greeting that contains another person's name

        :param other_person_name: The name of another person
        :return: A string representing a greeting
        """
        return f'Hello {other_person_name}, my name is {self.name}'
