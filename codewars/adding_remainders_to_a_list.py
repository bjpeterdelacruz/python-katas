"""
Adding Remainders to a List

:Author: BJ Peter Dela Cruz
"""


def solve(nums: list[int], div: int) -> list[int]:
    """
    This function divides each integer in :attr:`nums` by :attr:`div` and adds the remainder to it.
    A new list is returned. The original list is not modified.

    :param nums: The input list of integers
    :param div: The divisor
    :return: A new list of integers
    """
    return [number + (number % div) for number in nums]
