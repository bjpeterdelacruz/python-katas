"""
Duplicate Sandwich

:Author: BJ Peter Dela Cruz
"""

# pylint: disable=inconsistent-return-statements


def duplicate_sandwich(arr: list) -> list:
    """
    This function returns a list of all the elements in the given list that are between the element
    that appears twice (all the elements in the list are unique except one). It is assumed that the
    elements in the list are hashable.

    :param arr: The input list
    :return: All the elements between
    """
    unique = set()
    for element in arr:
        if element in unique:
            sub_arr = arr[arr.index(element) + 1:]
            return sub_arr[:sub_arr.index(element)]
        unique.add(element)
