"""
Hex to Decimal

:Author: BJ Peter Dela Cruz
"""


def hex_to_dec(hex_string: str) -> int:
    """
    This function converts the given hexadecimal (base 16) string to a decimal (base 10) number.

    :param hex_string: A hexadecimal string
    :return: A decimal number
    """
    return int(hex_string, 16)
