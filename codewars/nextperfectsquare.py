"""
Next Perfect Square

:Author: BJ Peter Dela Cruz
"""


def next_perfect_square(number: int) -> int:
    """
    This function finds the next number that is greater than :attr:`number` and is a perfect square.

    :param number: The input integer
    :return: The next number that is greater than :attr:`number` and is a perfect square
    """
    if number < 0:
        return 0
    next_number = number + 1
    while not (next_number ** 0.5).is_integer():
        next_number += 1
    return next_number
