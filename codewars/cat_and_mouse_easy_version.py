"""
Cat and Mouse

:Author: BJ Peter Dela Cruz
"""


def cat_mouse(string: str) -> str:
    """
    This function determines whether a cat can catch a mouse. A cat can only jump over at most three
    characters, i.e. if there are three or fewer dots between the cat and the mouse, the mouse will
    be caught. Otherwise, the mouse will escape.

    :param string: The input string representing the position of a cat and a mouse
    :return: "Escaped!" if the mouse escaped, "Caught!" otherwise
    """
    if 'C' in string and 'm' in string:
        return "Escaped!" if abs(string.index('C') - string.index('m')) > 4 else "Caught!"
    return "Escaped!"
