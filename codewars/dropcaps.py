from itertools import groupby


def drop_cap(string: str) -> str:
    arr = []
    for isspace, characters in groupby(string, lambda x: x.isspace()):
        word = ''.join(list(characters))
        arr.append(word if isspace or len(word) < 3 else word[0].upper() + word[1:].lower())
    return ''.join(arr)
