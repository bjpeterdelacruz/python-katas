"""
Swap Items in a Dictionary

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict


def switch_dict(dictionary: dict) -> dict:
    """
    This function swaps the keys and values of a dictionary. The values in :attr:`dictionary` will
    become keys in the dictionary that is returned, and the keys in :attr:`dictionary` will be
    appended to a list, which will be stored as a value in the same dictionary.

    :param dictionary: The input dictionary
    :return: A new dictionary
    """
    switch = defaultdict(list)
    for key, value in dictionary.items():
        switch[value].append(key)
    return switch
