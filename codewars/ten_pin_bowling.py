"""
Ten-Pin Bowling

:Author: BJ Peter Dela Cruz
"""


def calculate_score(first_frame: str, second_frame: str, third_frame: str) -> int:
    """
    This helper function calculates the score for :attr:`first_frame` using the number of pins
    knocked down in :attr:`second_frame` (if :attr:`first_frame` is a strike or a spare) and
    :attr:`third_frame` (if :attr:`first_frame` and :attr:`second_frame` are strikes).

    :param first_frame: The first frame
    :param second_frame: The second frame
    :param third_frame: The third frame
    :return: The score for the first frame
    """
    if first_frame == "X" and second_frame == "X" and third_frame[0] == "X":
        return 30
    if first_frame == "X" and second_frame == "X":
        return 20 + int(third_frame[0])
    if first_frame == "X":
        score = 10 + int(second_frame[0])
        return 20 if second_frame[1] == "/" else score + int(second_frame[1])
    if first_frame[1] == "/":
        return 20 if second_frame == "X" else 10 + int(second_frame[0])
    return int(first_frame[0]) + int(first_frame[1])


def calculate_score_last_two_frames(nine: str, ten: str) -> int:
    """
    This helper function calculates the score for the ninth frame using the number of pins knocked
    down in the ninth and tenth frames.

    :param nine: The ninth frame
    :param ten: The tenth frame
    :return: The score for the ninth frame
    """
    if nine == "X":
        if ten[:2] == "XX":
            return 30
        if ten[0] == "X":
            return 20 + int(ten[1])
        if ten[1] == "/":
            return 20
        return 10 + int(ten[0]) + int(ten[1])
    if nine[1] == "/" and ten[0] == "X":
        return 20
    return 10 + int(ten[0]) if nine[1] == "/" else int(nine[0]) + int(nine[1])


def calculate_score_last_frame(ten: str) -> int:
    """
    This helper function calculates the score for the last (tenth) frame.

    :param ten: The tenth frame
    :return: The score for the tenth frame
    """
    if ten == "XXX":
        return 30
    if ten[:2] == "XX":
        return 20 + int(ten[2])
    if ten[0] == "X":
        return 20 if ten[2] == "/" else 10 + int(ten[1]) + int(ten[2])
    if ten[1] == "/":
        return 20 if ten[2] == "X" else 10 + int(ten[2])
    return int(ten[0]) + int(ten[1])


def bowling_score(frames: str) -> int:
    """
    This function calculates a bowler's final score in a ten-pin bowling game.

    :param frames: The input string, a space-delimited list of ten frames in a bowling game
    :return: The bowler's final score
    """
    all_frames = frames.split()
    score = 0
    for idx in range(0, 8):   # frames 1 - 8
        score += calculate_score(all_frames[idx], all_frames[idx + 1], all_frames[idx + 2])
    score += calculate_score_last_two_frames(all_frames[8], all_frames[9])
    return score + calculate_score_last_frame(all_frames[9])
