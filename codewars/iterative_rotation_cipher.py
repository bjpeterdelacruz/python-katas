"""
Iterative Rotation Cipher

:Author: BJ Peter Dela Cruz
"""
from collections import deque


def encode(number: int, string: str) -> str:
    """
    This function encodes the given string using the following algorithm. 1. All spaces (not
    newline characters) are stripped off the string. 2. The entire string is rotated to the right
    :attr:`number` times. 3. The spaces are put back in the same positions. 4. Each word in the
    string is rotated to the right :attr:`number` times. Steps 1 through 4 are repeated
    :attr:`number` times.

    :param number: The input integer, the number of times to rotate the entire string and each word
    :param string: The input string to encode
    :return: The encoded string
    """
    spaces_indexes = [(idx, char) for idx, char in enumerate(string) if char == ' ']
    for _ in range(0, number):
        characters = deque([char for word in string for char in word if char != ' '])
        characters.rotate(number)
        for pair in spaces_indexes:
            characters.insert(pair[0], pair[1])
        words = []
        for word in ''.join(characters).split(' '):
            word_deque = deque(word)
            word_deque.rotate(number)
            words.append(''.join(word_deque))
        characters = list(''.join(words))
        for pair in spaces_indexes:
            characters.insert(pair[0], pair[1])
        string = ''.join(characters)
    return f'{number} {string}'


def decode(string: str) -> str:
    """
    This function decodes the given string that was encoded using :func:`encode`. To decode the
    string: 1. Get the number N to rotate the entire string and each word from the beginning of the
    string. 2. After removing the number and any leading whitespaces, rotate each word in the string
    to the left N times. 3. Strip off the spaces (not newline characters) between each word. 4.
    Rotate the entire string to the left N times. Steps 2 through 4 are repeated N times.

    :param string: The input string to decode
    :return: The decoded string
    """
    strings = string.split()
    number, string = int(strings[0]), string.replace(strings[0], '', 1).lstrip()
    spaces_indexes = [(idx, char) for idx, char in enumerate(string) if char == ' ']
    for _ in range(0, number):
        words = []
        for word in string.split(' '):
            word_deque = deque(word)
            word_deque.rotate(-number)
            words.append(''.join(word_deque))
        characters = deque(''.join(words))
        characters.rotate(-number)
        for pair in spaces_indexes:
            characters.insert(pair[0], pair[1])
        string = ''.join(characters)
    return string
