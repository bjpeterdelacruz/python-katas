"""
Alien Accent

:Author: BJ Peter Dela Cruz
"""


def convert(string: str) -> str:
    """
    This function converts all a's and o's (lowercase and uppercase) in :attr:`string` to o's and
    u's, respectively.

    :param string: The input string
    :return: A string with the aforementioned characters replaced
    """
    letters = {'a': 'o', 'o': 'u', 'A': 'O', 'O': 'U'}
    return ''.join([letters[letter] if letter in letters else letter for letter in string])
