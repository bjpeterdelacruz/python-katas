"""
Exclamation Marks Series #13: Count the Number of Exclamation Marks and Question Marks, Return the
Product

:Author: BJ Peter Dela Cruz
"""


def product(string: str) -> int:
    """
    This function counts the number of exclamation marks and exclamation marks in :attr:`string` and
    returns the product of both counts.

    :param string: The input string
    :return: The result of multiplying the number of exclamation marks with the number of question
        marks
    """
    return string.count('!') * string.count('?')
