"""
Bingo or Not

:Author: BJ Peter Dela Cruz
"""
from string import ascii_uppercase as letters


def bingo(array: list[int]) -> str:
    """
    This function returns "WIN" if any of the numbers in the given list of indexes, each
    representing a position of a letter in the alphabet, can spell "BINGO". The letters can be in
    any order. If the letters do not spell "BINGO", "LOSE" is returned.

    :param array: The input list of integers
    :return: "WIN" or "LOSE"
    """
    bingo_numbers = list(map(lambda x: letters.index(x) + 1, "BINGO"))
    length = len(set(filter(lambda x: x in bingo_numbers, array)))
    return "WIN" if length == len(bingo_numbers) else "LOSE"
