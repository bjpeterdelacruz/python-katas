"""
Thinkful: Quotable

:Author: BJ Peter Dela Cruz
"""


def quotable(name: str, quote: str) -> str:
    """
    This function returns a quote that the given person said using a formatted string.

    :param name: The name of the person who said :attr:`quote`
    :param quote: The quote
    :return: A string with the person saying the given quote
    """
    return f'{name} said: "{quote}"'
