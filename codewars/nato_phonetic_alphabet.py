"""
NATO Alphabet

:Author: BJ Peter Dela Cruz
"""


letters = {
    "A": "Alpha", "B": "Bravo", "C": "Charlie",
    "D": "Delta", "E": "Echo", "F": "Foxtrot",
    "G": "Golf", "H": "Hotel", "I": "India",
    "J": "Juliett", "K": "Kilo", "L": "Lima",
    "M": "Mike", "N": "November", "O": "Oscar",
    "P": "Papa", "Q": "Quebec", "R": "Romeo",
    "S": "Sierra", "T": "Tango", "U": "Uniform",
    "V": "Victor", "W": "Whiskey", "X": "X-ray",
    "Y": "Yankee", "Z": "Zulu"
}


def nato(word: str) -> str:
    """
    This function spells out the given word using the NATO alphabet. For example, given "Bob", the
    string "Bravo Oscar Bravo" is returned.

    :param word: The input string, a word that contains only alphabetic characters
    :return: The word spelled out using the NATO alphabet
    """
    return ' '.join(letters[letter.upper()] for letter in word)
