"""
Find All Occurrences of an Element in an Array

:Author: BJ Peter Dela Cruz
"""


def find_all(array: list[int], number: int) -> list[int]:
    """
    This function finds all the occurrences of :attr:`number` in :attr:`array` and returns a list
    of index positions of :attr:`number` in :attr:`array`.

    :param array: The input list of integers
    :param number: The number to find in :attr:`array`
    :return: A list of index positions of :attr:`number` in :attr:`array`
    """
    return [idx for idx, num in enumerate(array) if num == number]
