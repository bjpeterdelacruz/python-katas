"""
Arrh, Grabscrab!

:Author: BJ Peter Dela Cruz
"""


def grabscrab(word: str, possible_words: list[str]) -> list[str]:
    """
    This function returns a list of possible words that a pirate might have meant by the given word.

    :param word: The input string, a word spoken by a pirate
    :param possible_words: A list of possible words that the pirate might have meant
    :return: A list of possible words that the pirate might have meant using all the letters in
        :attr:`word`
    """
    word = sorted(list(word))
    matches = []
    for match in possible_words:
        temp = sorted(list(match))
        if word == temp:
            matches.append(match)
    return matches
