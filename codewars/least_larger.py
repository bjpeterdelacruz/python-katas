"""
Least Larger

:Author: BJ Peter Dela Cruz
"""


def least_larger(arr: list[int], idx: int) -> int:
    """
    This function finds the smallest number in the given list that is greater than the number at the
    given index and returns the index of that number.

    :param arr: The input list of integers
    :param idx: The index of an integer in the list
    :return: The index of the smallest number that is greater than the number at :attr:`idx`
    """
    results = [(index, number) for index, number in enumerate(arr) if number > arr[idx]]
    if not results:
        return -1
    results = sorted(results, key=lambda x: x[1])
    return results[0][0]
