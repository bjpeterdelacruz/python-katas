"""
Alphabetical Grid

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase


def grid(number: int) -> str | None:
    """
    This function returns a square grid of size :attr:`number`. Each cell in the grid contains an
    ASCII character between 'a' and 'z', inclusive. The order goes from 'a', 'b', 'c', ... in the
    first row; 'b', 'c', 'd', ... in the second row; etc. After 'z', the sequence starts over from
    'a'. If :attr:`number` is negative, None is returned.

    :param number: The size of the square grid, i.e. the number of characters in each row and column
    :return: A grid of size :attr:`number` by :attr:`number` that contains ASCII characters that are
        in alphabetical order starting with 'a', or None if :attr:`number` is negative
    """
    if number < 0:
        return None
    alpha_grid = []
    for idx in range(0, number):
        row = [ascii_lowercase[idx_2 % len(ascii_lowercase)] for idx_2 in range(idx, idx + number)]
        alpha_grid.append(' '.join(row))
    return '\n'.join(alpha_grid)
