"""
Number of Trailing Zeros of N!

:Author: BJ Peter Dela Cruz
"""


def zeros(factorial: int) -> int:
    """
    This function counts the number of trailing zeros in N! without actually performing the
    factorial.

    :param factorial: The factorial for which to get the number of trailing zeros
    :return: The number of trailing zeros
    """
    num_five = 5
    count = 0
    while True:
        if factorial / num_five < 1:
            break
        count += factorial // num_five
        num_five *= 5
    return count
