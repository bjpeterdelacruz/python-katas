"""
Composing Squared Strings

:Author: BJ Peter Dela Cruz
"""


def compose(string_1: str, string_2: str) -> str:
    """
    This function generates a string from two squared strings. Both :attr:`string_1` and
    :attr:`string_2` are squared strings. A squared string contains the same number of lines as the
    number of characters on each line. This function generates a string that contains N lines, each
    line containing N + 1 characters. To generate this string, the first line will contain the first
    character from :attr:`string_1` and all the characters from :attr:`string_2`, the second line
    will contain the first two characters from :attr:`string_1` and all the characters of the
    penultimate line of :attr:`string_2` except the last character, and so on until the last line
    will contain all the characters from :attr:`string_1` and the first character of the first line
    of :attr:`string_2`.

    :param string_1: The first input string, a squared string of size N
    :param string_2: The second input string, a squared string of size N
    :return: A string that contains N lines, each line containing N + 1 characters, as described
        above
    """
    arr_1 = string_1.split()
    arr_2 = string_2.split()
    return '\n'.join([f'{arr_1[idx - 1][:idx]}{arr_2[len(arr_2) - idx][:len(arr_2) - (idx - 1)]}'
                      for idx in range(1, len(arr_1) + 1)])
