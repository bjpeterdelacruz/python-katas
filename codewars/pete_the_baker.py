"""
Pete the Baker

:Author: BJ Peter Dela Cruz
"""


def cakes(recipe: dict[str, int], available: dict[str, int]) -> int:
    """
    This function calculates the number of cakes for a given recipe that Pete the baker can make
    given the ingredients he has on hand.

    :param recipe: A dictionary of the ingredients and amount for a particular cake
    :param available: A dictionary of the ingredients and amount that Pete has
    :return: The amount of cakes that Pete can make
    """
    for ingredient, count in recipe.items():
        if ingredient not in available or count > available[ingredient]:
            return 0
    return min(available[ingredient] // recipe[ingredient] for ingredient in recipe.keys())
