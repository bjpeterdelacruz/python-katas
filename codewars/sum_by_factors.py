"""
Sum by Factors

:Author: BJ Peter Dela Cruz
"""
from codewars.simple_fun_number_115_factor_sum import get_prime_factors


def sum_by_factors(integers: list[int]) -> list[list[int, int]]:
    """
    This function returns a list containing a list of two elements. The first element in the sublist
    will be a prime number, and the second element will be the sum of all the numbers in
    :attr:`integers` of which the prime number is a prime factor. The list of lists will be sorted
    in ascending order by prime number.

    :param integers: The input list of integers
    :return: A sorted list of lists containing a prime number and the sum of all the numbers in
        :attr:`integers` of which the prime number is a prime factor
    """
    factors = {integer: get_prime_factors(abs(integer)) for integer in integers}
    all_primes = sorted({prime for numbers in factors.values() for prime in numbers})
    return [list((prime, sum(num for num in integers if prime in factors[num]))) for prime in
            all_primes]
