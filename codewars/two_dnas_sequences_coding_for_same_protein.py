"""
Two DNAs Sequences: Coding for Same Protein?

:Author: BJ Peter Dela Cruz
"""
codons = {'TTC': 'F', 'TTT': 'F', 'TTA': 'L', 'TTG': 'L', 'CTT': 'L', 'CTC': 'L', 'CTA': 'L',
          'CTG': 'L', 'ATT': 'I', 'ATC': 'I', 'ATA': 'I', 'ATG': 'M', 'GTT': 'V', 'GTC': 'V',
          'GTA': 'V', 'GTG': 'V', 'TCT': 'S', 'TCC': 'S', 'TCA': 'S', 'TCG': 'S', 'AGT': 'S',
          'AGC': 'S', 'CCT': 'P', 'CCC': 'P', 'CCA': 'P', 'CCG': 'P', 'ACT': 'T', 'ACC': 'T',
          'ACA': 'T', 'ACG': 'T', 'GCT': 'A', 'GCC': 'A', 'GCA': 'A', 'GCG': 'A', 'TAT': 'Y',
          'TAC': 'Y', 'CAT': 'H', 'CAC': 'H', 'CAA': 'Q', 'CAG': 'Q', 'AAT': 'N', 'AAC': 'N',
          'AAA': 'K', 'AAG': 'K', 'GAT': 'D', 'GAC': 'D', 'GAA': 'E', 'GAG': 'E', 'TGT': 'C',
          'TGC': 'C', 'TGG': 'W', 'CGT': 'R', 'CGC': 'R', 'CGA': 'R', 'CGG': 'R', 'AGA': 'R',
          'AGG': 'R', 'GGT': 'G', 'GGC': 'G', 'GGA': 'G', 'GGG': 'G', 'TAA': '*', 'TGA': '*',
          'TAG': '*'}


def code_for_same_protein(seq1: str, seq2: str) -> bool:
    """
    This function returns True if the given DNA sequences code for the same protein. If the DNA
    sequences do not contain the same number of characters, False is returned.

    :param seq1: The first DNA sequence, a string
    :param seq2: The second DNA sequence, a string
    :return: True if the two DNA sequences code for the same protein, False otherwise
    """
    if len(seq1) != len(seq2):
        return False
    idx = 0
    while idx < len(seq1):
        if codons[seq1[idx:idx + 3]] != codons[seq2[idx:idx + 3]]:
            return False
        idx += 3
    return True
