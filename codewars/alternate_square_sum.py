"""
Alternate Square Sum

:Author: BJ Peter Dela Cruz
"""


def alternate_sq_sum(arr: list[int]) -> int:
    """
    This function squares each integer that is at an odd index in :attr:`arr` before summing
    together each result with the rest of the integers, i.e. the integers at even indexes, in the
    list.

    :param arr: The input list of integers
    :return: The sum of the square of each integer at an odd index in :attr:`arr` with the integers
        at even indexes
    """
    return sum(element ** 2 if idx % 2 == 1 else element for idx, element in enumerate(arr))
