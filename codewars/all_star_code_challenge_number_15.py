"""
All Star Code Challenge #15

Author: BJ Peter Dela Cruz
"""
from collections import deque


def rotate(str_: str) -> list[str]:
    """
    This function generates a list of strings that are generated after rotating the given string one
    character to the left one time for a total of N times, where N is the length of the given
    string.

    :param str_: The input string to rotate
    :return: A list of all possible strings after rotating :attr:`str_` N times
    """
    str_deque = deque(str_)
    arr = []
    for _ in str_:
        str_deque.rotate(-1)
        arr.append(''.join(str_deque))
    return arr
