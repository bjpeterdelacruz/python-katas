def is_divisible(*args: int) -> bool:
    for number in range(1, len(args)):
        if args[0] % args[number] != 0:
            return False
    return True
