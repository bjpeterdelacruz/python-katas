"""
Can We Divide It?

:Author: BJ Peter Dela Cruz
"""


def is_divide_by(number: int, first_number: int, second_number: int) -> bool:
    """
    This function returns True if :attr:`number` is divisible by both :attr:`first_number` and
    :attr:`second_number`.

    :param number: The input integer
    :param first_number: The first divisor
    :param second_number: The second divisor
    :return: True if :attr:`number` is divisible by the latter two integers, False otherwise
    """
    return number % first_number == 0 and number % second_number == 0
