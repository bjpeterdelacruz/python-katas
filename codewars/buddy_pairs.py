"""
Buddy Pairs

:Author: BJ Peter Dela Cruz
"""
from math import sqrt


def buddy(start, limit):
    """
    This function returns the first buddy pair (n m) such that n is a positive integer between
    :attr:`start` (inclusive) and :attr:`limit` (inclusive), m can be greater than :attr:`limit`,
    and m must be greater than n. If no buddy pair is found, "Nothing" is returned.

    :param start: The lower limit (inclusive)
    :param limit: The upper limit (inclusive)
    :return: The first buddy pair (n m)
    """
    for num in range(start, limit + 1):
        sum_1 = sum_divisors(num)
        if sum_1 - 1 > num:
            sum_2 = sum_divisors(sum_1 - 1)
            if sum_2 - 1 == num:
                return list((sum_2 - 1, sum_1 - 1))
    return "Nothing"


def sum_divisors(number: int):
    """
    This helper function sums all the proper divisors of :attr:`number`.

    :param number: The integer for which to sum its proper divisors
    :return: The sum of the proper divisors of :attr:`number`
    """
    divisors = [1]
    for num in range(2, int(sqrt(number)) + 1):
        if number % num == 0:
            if number / num == num:
                divisors.append(num)
            else:
                divisors.append(num)
                divisors.append(number // num)
    return sum(divisors)
