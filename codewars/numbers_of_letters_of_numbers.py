"""
Numbers of Letters of Numbers

:Author: BJ Peter Dela Cruz
"""


def numbers_of_letters(number: int) -> list[str]:
    """
    This function converts each digit in :attr:`number` into a word, e.g. 99 becomes "ninenine", and
    then adds the word to a list. Then it converts the length of the word into a word, e.g.
    "ninenine" has eight characters, so "ninenine" becomes "eight", and adds the word to a list.
    This process of converting a word's length into a word continues until the word starts to
    repeat. Then the list is returned. For example, given 99, a list containing "ninenine", "eight",
    "five", and "four" is returned. Converting the length of "four" to a word also produces "four",
    so the aforementioned list of four strings is returned.

    :param number: The input integer
    :return: A list of strings
    """
    letters_to_numbers = {"0": "zero", "1": "one", "2": "two", "3": "three", "4": "four",
                          "5": "five", "6": "six", "7": "seven", "8": "eight", "9": "nine"}

    arr = [''.join([letters_to_numbers[digit] for digit in str(number)])]
    letters = str(len(arr[-1]))
    while letters not in letters_to_numbers or letters_to_numbers[letters] != arr[-1]:
        if letters not in letters_to_numbers:
            arr.append(''.join([letters_to_numbers[digit] for digit in letters]))
        else:
            arr.append(letters_to_numbers[letters])
        letters = str(len(arr[-1]))
    return arr
