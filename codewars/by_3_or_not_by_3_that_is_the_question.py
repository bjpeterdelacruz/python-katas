"""
Divisible by Three

:Author: BJ Peter Dela Cruz
"""


def divisible_by_three(string: str) -> bool:
    """
    This function returns True if the given number is divisible by three without using the modulo
    operator. All the digits in the number are summed together. If the length of the sum is greater
    than one, the process is repeated. The original number is divisible by 3 if the final sum is 3,
    6, or 9.

    :param string: The number as a string
    :return: True if the original number is divisible by 3, False otherwise
    """
    while len(string) != 1:
        string = str(sum(map(int, string)))
    return string in ["3", "6", "9"]
