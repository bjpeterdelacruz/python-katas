"""
Lowest Product of Four Consecutive Numbers

:Author: BJ Peter Dela Cruz
"""
from math import inf


def lowest_product(string: str):
    """
    This function multiplies sets of four consecutive numbers in :attr:`string` and returns the
    lowest product.

    :param string: The input string that contains numbers
    :return: The lowest product of four consecutive numbers
    """
    if len(string) < 4:
        return "Number is too small"
    idx = 0
    substring = string[0:4]
    lowest = inf
    while True:
        temp = 1
        for character in substring:
            temp *= int(character)
        if temp < lowest:
            lowest = temp
        idx += 1
        substring = string[idx:idx + 4]
        if len(substring) < 4:
            break
    return lowest
