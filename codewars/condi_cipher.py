"""
Condi Cipher

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase
from collections import OrderedDict


def generate_key_dicts(cipher_key: str) -> tuple[OrderedDict, OrderedDict]:
    """
    This helper function generates two OrderedDict objects: one that maps characters to positions
    and another that maps positions to characters.

    :param cipher_key: The key used to generate the two dictionaries
    :return: A tuple of two dictionaries
    """
    char_to_pos = OrderedDict()
    pos_to_char = OrderedDict()
    idx = 1
    for char in cipher_key:
        if char not in char_to_pos:
            char_to_pos[char] = idx
            idx += 1
    for char in ascii_lowercase:
        if char not in char_to_pos:
            char_to_pos[char] = idx
            idx += 1
    for key, value in char_to_pos.items():
        pos_to_char[value] = key
    return char_to_pos, pos_to_char


def get_shift(characters, initial_shift: int, idx: int, char_to_pos: OrderedDict) -> int:
    """
    This helper function returns the number of characters to shift.

    :param characters: A list of characters
    :param initial_shift: The number of characters to shift for the first character
    :param idx: The index for :attr:`characters`
    :param char_to_pos: An OrderedDict of characters (the keys) to positions
    :return: The number of characters to shift
    """
    prev_position = idx - 1
    while prev_position > -1 and characters[prev_position] not in char_to_pos:
        prev_position -= 1
    return initial_shift if prev_position == -1 else char_to_pos[characters[prev_position]]


def encode(message: str, key: str, initial_shift: int) -> str:
    """
    This function encodes :attr:`string` and returns a ciphertext.

    :param message: The input string, the plaintext
    :param key: The key used for encryption/decryption
    :param initial_shift: The number of characters to shift for the first character
    :return: The encrypted message (ciphertext)
    """
    char_to_pos, pos_to_char = generate_key_dicts(key)
    encoded_message = []
    idx = 0
    while idx < len(message):
        if message[idx] not in char_to_pos:
            encoded_message.append(message[idx])
            idx += 1
            continue
        shift = get_shift(message, initial_shift, idx, char_to_pos)
        position = (char_to_pos[message[idx]] + shift) % 26
        if position == 0:
            position = 26
        encoded_message.append(pos_to_char[position])
        idx += 1
    return ''.join(encoded_message)


def decode(message: str, key: str, initial_shift: int) -> str:
    """
    This function decodes :attr:`message` and returns the original message.

    :param message: The input string, a ciphertext
    :param key: The key used for encryption/decryption
    :param initial_shift: The number of characters to shift for the first character
    :return: The original message (plaintext)
    """
    char_to_pos, pos_to_char = generate_key_dicts(key)
    decoded_message = []
    idx = 0
    while idx < len(message):
        if message[idx] not in char_to_pos:
            decoded_message.append(message[idx])
            idx += 1
            continue
        shift = get_shift(decoded_message, initial_shift, idx, char_to_pos)
        position = (char_to_pos[message[idx]] - shift) % 26
        if position == 0:
            position = 26
        decoded_message.append(pos_to_char[position])
        idx += 1
    return ''.join(decoded_message)
