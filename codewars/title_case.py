"""
Title Case

:Author: BJ Peter Dela Cruz
"""


def title_case(title: str, minor_words: str = '') -> str:
    """
    This function converts the given title into title case, that is, the first letter of each word
    in :attr:`title` will be converted to uppercase and the rest of the word converted to lowercase.
    If a word in :attr:`title` appears in :attr:`minor_words`, regardless of case, then that word
    will be converted to all lowercase.

    :param title: The input string, the title to convert
    :param minor_words: A space-delimited list of words to ignore in :attr:`title`, i.e. words to
        make all lowercase
    :return: The title in title case
    """
    if not title:
        return title
    new_title = list(title[0].upper() + title[1:].lower())
    for idx in range(1, len(new_title) - 1):
        if new_title[idx + 1].isalpha() and new_title[idx].isspace():
            new_title[idx + 1] = new_title[idx + 1].upper()
    new_title = ''.join(new_title)
    idx = 0
    while new_title[idx].isspace():
        idx += 1
    for minor_word in minor_words.split():
        word = minor_word[0].upper() + minor_word[1:].lower()
        if word in new_title[idx + 1:]:
            new_title = new_title[:idx + 1] + new_title[idx + 1:].replace(word, word.lower())
    return new_title
