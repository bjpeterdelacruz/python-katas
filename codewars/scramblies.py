"""
Scrambles

:Author: BJ Peter Dela Cruz
"""
from collections import Counter


def scramble(str1: str, str2: str) -> bool:
    """
    This function determines whether the characters in :attr:`str` can be used to make the string in
    :attr:`str2`.

    :param str1: The input string
    :param str2: The input string
    :return: True if any of the characters in :attr:`str1` can be used to make :attr:`str2`, False
        otherwise
    """
    counts1 = Counter(str1)
    counts2 = Counter(str2)
    for key, count in counts2.items():
        if counts1[key] < count:
            return False
    return True
