"""
Big Factorial

:Author: BJ Peter Dela Cruz
"""


def factorial(number: int) -> int | None:
    """
    This function calculates the factorial for the given integer without using recursion or any
    built-in functions.

    :param number: The input integer
    :return: The factorial of :attr:`number` or None if :attr:`number` is negative
    """
    if number < 0:
        return None
    if number <= 1:
        return 1
    result = 1
    for num in range(2, number + 1):
        result *= num
    return result
