"""
Ultimate Array Reversal

:Author: BJ Peter Dela Cruz
"""


def reverse(arr: list[str]) -> list[str]:
    """
    This function concatenates all the strings in the given list, reverses the concatenated string,
    and then returns a list that contains strings that form the reversed string when concatenated
    together. The list will contain the same number of strings as the original list, and each
    string in the list will contain the same number of characters as the one that is in the same
    position in the original list.

    :param arr: The input list of strings
    :return: A list of strings that contains the same number of elements
    """
    lengths = [len(string) for string in arr]
    reversed_string = ''.join(reversed(''.join(arr)))
    new_arr = []
    for length in lengths:
        new_arr.append(reversed_string[:length])
        reversed_string = reversed_string[length:]
    return new_arr
