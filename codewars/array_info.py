"""
Array Info

:Author: BJ Peter Dela Cruz
"""


def array_info(lst: list) -> str | list:
    """
    This function counts all the integers, floats, strings, and whitespace-only strings that are in
    the given list. The counts, as well as the length of the list, are returned in a five-element
    list.

    :param lst: The input list containing various data types
    :return: A five-element list containing the length of the list and the total number of integers,
        floats, strings, and whitespace-only strings
    """
    if not lst:
        return "Nothing in the array!"
    int_count = none_or_count(len(list(filter(lambda x: isinstance(x, int), lst))))
    float_count = none_or_count(len(list(filter(lambda x: isinstance(x, float), lst))))
    str_count, whitespace_count = str_whitespace_counts(lst)
    return [[len(lst)], int_count, float_count, str_count, whitespace_count]


def none_or_count(count: int) -> list:
    """
    This helper function returns a one-element list containing either a count if it is greater than
    zero or None if the count is equal to zero.

    :param count: The count
    :return: A one-element list containing either a count or None
    """
    return [None] if count == 0 else [count]


def str_whitespace_counts(lst: list) -> tuple[list, list]:
    """
    This helper function returns a tuple containing two counts, one of all the strings in the given
    list and another of all the whitespace-only strings in the same list.

    :param lst: The input list that may or may not contain strings and other data types
    :return: A tuple containing two counts, one for strings and another for whitespace-only strings
    """
    counts = [0, 0]
    for element in lst:
        if isinstance(element, str):
            if element.strip():
                counts[0] += 1
            else:
                counts[1] += 1
    return none_or_count(counts[0]), none_or_count(counts[1])
