"""
Pre-FizzBuzz Workout #1

:Author: BJ Peter Dela Cruz
"""


def pre_fizz(number: int) -> list[int]:
    """
    This function generates a list of integers from 1 to :attr:`number`, inclusive.

    :param number: The last number in the range to include in the list
    :return: A list of integers from 1 to :attr:`number`, inclusive
    """
    return list(range(1, number + 1))
