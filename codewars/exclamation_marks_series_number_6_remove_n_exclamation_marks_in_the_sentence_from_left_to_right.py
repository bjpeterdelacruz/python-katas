"""
Exclamation Marks Series #6: Remove N Exclamation Marks in the Sentence from Left to Right

:Author: BJ Peter Dela Cruz
"""


def remove(string: str, count: int) -> str:
    """
    This function removes the first :attr:`count` exclamation marks in :attr:`string`.

    :param string: The input string that may or may not contain exclamation marks
    :param count: The number of exclamation marks to remove
    :return: A string that contains :attr:`count` fewer exclamation marks than :attr:`string`
    """
    return string.replace("!", "", count)
