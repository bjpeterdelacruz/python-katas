"""
Triangular Treasure

:Author: BJ Peter Dela Cruz
"""


def triangular(nth: int) -> int:
    """
    This function calculates a triangular number given :attr:`nth`. A triangular number is the
    number of objects arranged in an equilateral triangle. For example, given :attr:`nth` = 4, the
    triangular number 10 will be returned, because (4 * (4 + 1)) / 2 = 10.

    :param nth: An integer
    :return: A triangular number
    """
    return (nth * (nth + 1)) // 2 if nth > 0 else 0
