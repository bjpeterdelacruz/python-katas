"""
Spraying Trees

Author: BJ Peter Dela Cruz
"""


def task(weekday: str, number: int, cost: int) -> str:
    """
    This function returns a string with the following information: what day is today, who has to
    work, how many trees the person has to spray, and how much money it costs to buy the liquid to
    spray the tree(s).

    :param weekday: What day is today
    :param number: The number of trees to spray
    :param cost: How much it costs to spray one or more trees
    :return: A string with information about who has to work today
    """
    people = {'Monday': 'James', 'Tuesday': 'John', 'Wednesday': 'Robert',
              'Thursday': 'Michael', 'Friday': 'William'}
    return f'It is {weekday} today, {people[weekday]}, you have to work, ' \
           f'you must spray {number} trees and you need {number * cost} dollars to buy liquid'
