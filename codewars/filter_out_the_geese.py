"""
Filter Out the Geese

:Author: BJ Peter Dela Cruz
"""


def goose_filter(birds: list[str]) -> list[str]:
    """
    This function filters out all the geese that appear in the given list of birds.

    :param birds: The input list of strings
    :return: A list that contains only bird names
    """
    geese = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]
    return [bird for bird in birds if bird not in geese]
