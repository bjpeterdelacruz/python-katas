"""
Remove String Spaces

:Author: BJ Peter Dela Cruz
"""


def no_space(string: str) -> str:
    """
    This function removes all whitespace characters from the given string.

    :param string: The input string
    :return: The same string but without whitespace characters in it
    """
    return ''.join(string.split())
