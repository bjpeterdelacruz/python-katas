"""
Linked Lists: Insert Nth Node

:Author: BJ Peter Dela Cruz
"""
from utils.linked_list import Node
from codewars.linked_lists_length_and_count import length


def insert_nth(head, index, data):
    """
    This function inserts the given data at the given index in the linked list. If head is None, a
    new linked list is returned with only one node with the given data, but if index is not equal to
    zero, a ValueError is raised. If head is not None, :attr:`index` must be between 0 and the
    length of the linked list, both inclusive. If :attr:`index` equals the length, the data is
    inserted at the end of the list. If :attr:`index` is greater than the length, a ValueError is
    raised.

    :param head: The linked list
    :param index: The index at which to insert the data
    :param data: The data to insert
    :return: The head of the linked list with the given data inserted into it
    """
    if not head:
        if index != 0:
            raise ValueError
        return Node(data)
    length_of_ll = length(head)
    if index > length_of_ll:
        raise ValueError
    if index == 0:
        new_head = Node(data)
        new_head.next = head
        return new_head
    prev_node = head
    next_node = head.next
    idx = 1
    while next_node:
        if index == idx:
            new_node = Node(data)
            prev_node.next = new_node
            new_node.next = next_node
            return head
        idx += 1
        prev_node = next_node
        next_node = next_node.next
    prev_node.next = Node(data)
    return head
