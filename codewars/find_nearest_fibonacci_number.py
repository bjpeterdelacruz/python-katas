"""
Find Nearest Fibonacci Number

:Author: BJ Peter Dela Cruz
"""
from math import log

# pylint: disable=dangerous-default-value,invalid-name


def fast_fib(number, cache={0: 0, 1: 1}):
    """
    This helper function uses the matrix form to recursively find the nearest Fibonacci numbers
    closest to :attr:`number`.

    :param number: The number for which to find the closest Fibonacci number
    :param cache: A cache that contains Fibonacci numbers that have already been calculated
    :return: The nearest Fibonacci number
    """
    if number in cache:
        return cache[number]
    m = (number + 1) // 2
    a, b = fast_fib(m - 1), fast_fib(m)
    fib = a * a + b * b if number & 1 else (2 * a + b) * b
    cache[number] = fib
    return fib


def nearest_fibonacci(number):
    """
    This function finds the Fibonacci number that is closest to :attr:`number`. Note that this
    function with the help of :func:`fast_fib` finds the Fibonacci number in less than 200 ms.

    :param number: The input integer
    :return: The Fibonacci number closest to :attr:`number`. If two Fibonacci numbers are the same
        distance to :attr:`number`, the lowest Fibonacci number is returned.
    """
    if number == 0:
        return 0
    # Approximate by inverting the large term of Binet's formula
    log_root_5 = log(5) / 2
    log_phi = log((1 + 5 ** 0.5) / 2)
    y = int((log(number) + log_root_5) / log_phi)
    lo = fast_fib(y)
    hi = fast_fib(y + 1)
    return lo if number - lo <= hi - number else hi
