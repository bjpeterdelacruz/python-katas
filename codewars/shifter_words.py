"""
Shifter Words

:Author: BJ Peter Dela Cruz
"""


def shifter(string: str) -> int:
    """
    This function returns the number of shifter words that are found in the given string. A shifter
    word is a word that, when written on paper and after turning the paper 180 degrees, can still be
    read. For example, "SOS" is a shifter word.

    :param string: The input string
    :return: The number of shifter words found in :attr:`string`
    """
    shifter_characters = ['H', 'I', 'N', 'O', 'S', 'X', 'Z', 'M', 'W']
    words_found = set()
    for word in string.split():
        is_found = True
        for character in word:
            if character not in shifter_characters:
                is_found = False
                break
        if is_found:
            words_found.add(word)
    return len(words_found)
