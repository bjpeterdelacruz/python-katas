"""
The reject() Function

:Author: BJ Peter Dela Cruz
"""


def reject(seq: list, predicate):
    """
    This function filters out all the elements in the given sequence that satisfy the given
    predicate.

    :param seq: The input list
    :param predicate: The predicate
    :return: A list that contains elements that do not satisfy :attr:`predicate`
    """
    return [element for element in seq if not predicate(element)]
