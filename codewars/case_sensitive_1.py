"""
Case Sensitive

:Author: BJ Peter Dela Cruz
"""
from string import ascii_uppercase


def case_sensitive(string: str) -> list[bool, list[str]]:
    """
    This function returns a count of the number of uppercase letters in the given string and a list
    of all the uppercase letters in the order that they appear in the string.

    :param string: The input string
    :return: A list containing a count and a list of all the uppercase letters in :attr:`string`
    """
    uppercase = [letter for letter in string if letter in ascii_uppercase]
    return [len(uppercase) == 0, uppercase]
