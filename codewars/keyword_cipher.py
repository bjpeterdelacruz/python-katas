"""
Keyword Cipher

:Author: BJ Peter Dela Cruz
"""


def keyword_cipher(msg: str, keyword: str):
    """
    To encrypt :attr:`msg`, first drop all the letters in :attr:`keyword` that appear more than
    once. For example, if "wednesday" is the keyword, the result will be "wednsay". Then the rest of
    the letters of the English alphabet that do not appear in :attr:`keyword` are appended to the
    end of the result, so the encryption key of the previous example will become
    "wednsaybcfghijklmopqrtuvxz". Finally, the characters in :attr:`msg` will be substituted using a
    dict with characters of the English alphabet as the keys and the characters in the encryption
    key as the values. Characters such as numbers and symbols are left as is in the encrypted
    string.

    :param msg: The message to encrypt
    :param keyword: The string that is used to generate the encryption key
    :return: An encrypted string
    """
    keys = list(dict.fromkeys(keyword.lower()))  # https://stackoverflow.com/a/17016257/3640269
    keys.extend(chr(code) for code in range(ord('a'), ord('z') + 1) if chr(code) not in keys)
    chars = dict(zip([chr(code) for code in range(ord('a'), ord('z') + 1)], keys))
    return ''.join(chars[char] if char in keys else char for char in msg.lower())
