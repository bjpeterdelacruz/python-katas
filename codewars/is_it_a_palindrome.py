"""
Is It a Palindrome?

:Author: BJ Peter Dela Cruz
"""


def is_palindrome(string: str) -> bool:
    """
    This function returns True if the given string is a palindrome, or False otherwise. Case is
    ignored.

    :param string: The input string
    :return: True if :attr:`string` is a palindrome, False otherwise
    """
    return ''.join(reversed(string.lower())) == string.lower()
