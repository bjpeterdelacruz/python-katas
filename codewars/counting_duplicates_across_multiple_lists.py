"""
Counting Duplicates Across Multiple Lists

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict


def count_duplicates(name: list[str], age: list[int], height: list[int]) -> int:
    """
    This function gathers the name, age, and height of a person at each index--e.g. name[0], age[0],
    and height[0] make up one entry--and returns a count of the number of duplicate entries, that
    is, the number of entries that have the same name, age, and height as the original entry. For
    example, given the inputs ["BJ", "BJ", "BJ"], [35, 35, 35], and [67, 67, 67], a count of 2 is
    returned.

    :param name: A list of people's names
    :param age: A list of people's ages
    :param height: A list of people's heights
    :return: The number of duplicate entries that have the same three attributes as the original
        entry
    """
    counts = defaultdict(int)
    for element in list(zip(name, age, height)):
        counts[element] += 1
    return sum(value - 1 for value in counts.values())
