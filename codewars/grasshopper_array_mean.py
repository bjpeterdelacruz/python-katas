"""
Grasshopper: Array Mean

:Author: BJ Peter Dela Cruz
"""


def find_average(nums: list):
    """
    This function returns the mean (average) of the given list of numbers.

    :param nums: The input list of numbers
    :return: The average
    """
    return 0 if len(nums) == 0 else sum(nums) / len(nums)
