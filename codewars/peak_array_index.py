"""
Peak Array Index

:Author: BJ Peter Dela Cruz
"""


def peak(arr: list[int]) -> int:
    """
    This function returns the index in the given list such that the sum of all the integers to the
    left of the index equals the sum of all the integers to the right of the index. If no such index
    exists, -1 is returned.

    :param arr: The input list of integers
    :return: The index such that the sum of the left side equals the sum of the right side, or -1
        if no such index exists
    """
    for idx in range(0, len(arr)):
        if sum(arr[0:idx]) == sum(arr[idx + 1:]):
            return idx
    return -1
