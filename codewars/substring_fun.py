"""
Substring Fun

:Author: BJ Peter Dela Cruz
"""


def nth_char(words: list[str]) -> str:
    """
    This function creates a string from the given list of strings. A letter from each string in the
    list is used to create the string, and the index of a string in the list is used to extract
    a letter from that string. For example, given ["yoda", "best", "has"], "yes" will be returned,
    because words[0][0] + words[1][1] + words[2][2] = "yes".

    :param words: The input list of strings
    :return: A string created from one letter of each string in :attr:`words`
    """
    return ''.join(words[idx][idx] for idx in range(0, len(words)))
