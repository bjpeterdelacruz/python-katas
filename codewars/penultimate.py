"""
Penultimate

:Author: BJ Peter Dela Cruz
"""


def penultimate(arr):
    """
    This function returns the second to last (penultimate) element in the given sequence. It is
    assumed that the sequence contains two or more elements.

    :param arr: The input sequence (a list, a string, etc.)
    :return: The second to last element
    """
    return arr[-2]
