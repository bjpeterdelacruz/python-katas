"""
USD => CNY

:Author: BJ Peter Dela Cruz
"""


def usdcny(usd: int) -> str:
    """
    This function converts U.S. dollars to Chinese yuan. The result is formatted to two decimal
    places.

    :param usd: The amount in U.S. dollars to convert
    :return: The amount in Chinese yuan
    """
    return f"{format(usd * 6.75, '.2f')} Chinese Yuan"
