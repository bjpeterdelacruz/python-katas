"""
Broken Sequence

:Author: BJ Peter Dela Cruz
"""


def find_missing_number(sequence: str) -> int:
    """
    This function checks whether the given sequence of integers contains non-numeric characters or
    is missing a number. If the sequence of integers, starting from 1 and increasing by 1, is not
    missing a number or the sequence is empty, 0 is returned. If non-numeric characters are present,
    1 is returned. If the sequence is missing numbers, the lowest number that is missing is
    returned. For example, given "1 2 6", the missing numbers are 3, 4, and 5, so 3 is returned.

    :param sequence: A whitespace-delimited sequence of integers
    :return: 0, 1, or N where N is the lowest number that is missing in :attr:`sequence`
    """
    if not sequence:
        return 0
    characters = sequence.split()
    for char in characters:
        if not char.isnumeric():
            return 1
    sorted_list = sorted(map(int, characters))
    expected_list = list(range(1, sorted_list[-1] + 1))
    if sorted_list == expected_list:
        return 0
    intersection = sorted(set(sorted_list) ^ set(expected_list))
    return intersection[0]
