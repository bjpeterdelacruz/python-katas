"""
Simple Fun #149: Next Day of Week

:Author: BJ Peter Dela Cruz
"""


def next_day_of_week(current_day: int, available_week_days: int) -> int:
    """
    This function finds the next day of the current week that an alarm will sound. If the alarm will
    not sound for the remainder of the week, the first day of the next week for which the alarm is
    configured is returned. Each bit that is turned on in :attr:`available_week_days` represents a
    day that the alarm will sound. It is assumed that an alarm will sound at least once in a week.

    :param current_day: The current day (today)
    :param available_week_days: An integer whose bits in the binary representation represent days of
        a week
    :return: The next day that the alarm will sound (0 is Sunday, 7 is Saturday)
    """
    days = [idx + 1 for idx, bit in enumerate(reversed(bin(available_week_days)[2:])) if bit == '1']
    next_days = list(filter(lambda x: x > current_day, days))
    return next_days[0] if next_days else days[0]
