"""
Over the Road

:Author: BJ Peter Dela Cruz
"""


def over_the_road(address: int, street_length: int) -> int:
    """
    This function determines the house number opposite the given address. If address is odd, the
    house number on the other side of the street is even, and vice versa. Odd-numbered houses are
    lined up in ascending order from top to bottom while even-numbered houses are lined up in
    descending order from top to bottom.

    :param address: The input street, a number representing an address that is either even or odd
    :param street_length: The length of the street with odd-numbered houses on one side and even-
        numbered houses on the other side
    :return: The house number opposite the given address
    """
    if address % 2 == 1:
        pos = (address - 1) // 2
        return (street_length - pos) * 2
    pos = address // 2
    return 2 * (street_length - pos) + 1
