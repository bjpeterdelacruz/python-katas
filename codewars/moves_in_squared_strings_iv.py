"""
Moves in Squared Strings 4

:Author: BJ Peter Dela Cruz
"""


def transform(string: list[str], rotate: bool = False) -> str:
    """
    This helper function applies the appropriate string transformation based on the value of
    :attr:`rotate`. If :attr:`rotate` is True, the rotate transformation is applied. (See
    :func:`rot_90_counter`.) Otherwise, the diagonal transformation is applied. (See
    :func:`diag_2_sym`.)

    :param string: The list of strings to transform
    :param rotate: True to apply the rotate transformation
    :return: A newline-delimited list of strings that is the result of the appropriate
        transformation
    """
    col_idx = len(string) - 1
    row_idx = 0 if rotate else len(string) - 1
    result = []
    while col_idx > -1:
        word = []
        if rotate:
            while row_idx < len(string):
                word.append(string[row_idx][col_idx])
                row_idx += 1
        else:
            while row_idx > -1:
                word.append(string[row_idx][col_idx])
                row_idx -= 1
        result.append(''.join(word))
        col_idx -= 1
        row_idx = 0 if rotate else len(string) - 1
    return "\n".join(result)


def diag_2_sym(string: list[str]) -> str:
    """
    This helper function delegates the diagonal transformation to the :func:`transform` function,
    which will return strings that are symmetric with respect to the cross diagonal.

    :param string: The list of strings to transform
    :return: A newline-delimited list of strings that are symmetric with respect to the cross
        diagonal
    """
    return transform(string)


def rot_90_counter(string: list[str]) -> str:
    """
    This helper function delegates the rotate transformation to the :func:`transform` function,
    which will return the strings rotated 90 degrees counterclockwise.

    :param string: The list of strings to transform
    :return: A newline-delimited list of strings that have been rotated 90 degrees counterclockwise
    """
    return transform(string, True)


def selfie_diag2_counterclock(string: list[str]) -> str:
    """
    This helper function returns the original list of strings, strings that are symmetric with
    respect to the cross diagonal, and strings that are rotated 90 degrees counterclockwise.

    :param string: The list of strings to transform
    :return: A newline-delimited list that contains the original strings, strings that are
        symmetric with respect to the cross diagonal, and strings that are rotated 90 degrees
        counterclockwise
    """
    diag2 = diag_2_sym(string).split("\n")
    rot90 = rot_90_counter(string).split("\n")
    return "\n".join([f"{current}|{diag2[idx]}|{rot90[idx]}" for idx, current in enumerate(string)])


def oper(func, string: str) -> str:
    """
    This function delegates the string transformation to the appropriate given function. All the
    helper functions operate on a string with N lines, each string containing N characters.

    :param func: The function that will apply a transformation to :attr:`string`
    :param string: The input string to transform
    :return: The string after a transformation has been applied
    """
    return func(string.split("\n"))
