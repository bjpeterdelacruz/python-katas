"""
Fundamentals: Return

:Author: BJ Peter Dela Cruz
"""


def add(num_1, num_2):
    """
    This function adds two numbers together and returns the result.

    :param num_1: The input number
    :param num_2: The input number
    :return: The result of adding :attr:`num_1` and :attr:`num_2` together
    """
    return num_1 + num_2


def multiply(num_1, num_2):
    """
    This function multiplies two numbers together and returns the result.

    :param num_1: The input number
    :param num_2: The input number
    :return: The result of multiplying :attr:`num_1` and :attr:`num_2` together
    """
    return num_1 * num_2


def divide(num_1, num_2):
    """
    This function divides :attr:`num_1` by :attr:`num_2` and returns the result.

    :param num_1: The input number
    :param num_2: The input number
    :return: The result of dividing :attr:`num_1` by :attr:`num_2`
    """
    return num_1 / num_2


def mod(num_1, num_2):
    """
    This function divides :attr:`num_1` by :attr:`num_2` and returns the remainder.

    :param num_1: The input number
    :param num_2: The input number
    :return: The remainder after dividing :attr:`num_1` by :attr:`num_2`
    """
    return num_1 % num_2


def exponent(num_1, num_2):
    """
    This function raises :attr:`num_1` to the :attr:`num_2`-th power and returns the result.

    :param num_1: The input number
    :param num_2: The input number
    :return: The result after raising :attr:`num_1` to the :attr:`num_2`-th power
    """
    return num_1 ** num_2


def subt(num_1, num_2):
    """
    This function subtracts :attr:`num_2` from :attr:`num_1` and returns the result.

    :param num_1: The input number
    :param num_2: The input number
    :return: The result after subtracting :attr:`num_2` from :attr:`num_1`
    """
    return num_1 - num_2
