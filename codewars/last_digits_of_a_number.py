"""
Last Digits of a Number

:Author: BJ Peter Dela Cruz
"""


def solution(number: int, num_digits: int) -> list[int]:
    """
    This function returns the last :attr:`num_digits` digits of the given integer. If
    :attr:`num_digits` is less than or equal to zero, an empty list is returned.

    :param number: The input integer
    :param num_digits: The number of digits to retrieve from the end of the integer
    :return: A list of digits
    """
    if num_digits <= 0:
        return []
    return list(map(int, reversed(list(reversed(str(number)))[:num_digits])))
