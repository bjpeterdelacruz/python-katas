"""
Exclamation Mark Series #3

:Author: BJ Peter Dela Cruz
"""


def remove(string: str) -> str:
    """
    This function removes all exclamation marks from the given string except those that are at the
    end of the string. For example, given "Hi!!! Hi!!!", this function will return "Hi Hi!!!".

    :param string: The input string that may or may not contain exclamation marks
    :return: A string with all but the exclamation marks at the end of the original string removed
    """
    count = 0
    idx = -1
    while string[idx] == '!':
        count += 1
        idx -= 1
    new_string = ''.join(list(filter(lambda x: x != '!', string)))
    return f"{new_string}{'!' * count}"
