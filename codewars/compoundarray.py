"""
CompoundArray

:Author: BJ Peter Dela Cruz
"""


def compound_array(arr1: list, arr2: list) -> list:
    """
    This function returns a list in which the elements of one list are interleaved with the elements
    in another list. For example, given [1, 2, 3, 4, 5] and [9, 8, 7], [1, 9, 2, 8, 3, 7, 4, 5] is
    returned. Each element in :attr:`arr2` is inserted at an odd index in the list that is returned.

    :param arr1: The first input list
    :param arr2: The second input list
    :return: A list that contains elements from both :attr:`arr1` and :attr:`arr2`
    """
    if not arr1:
        return arr2
    if not arr2:
        return arr1
    arr = []
    min_length = min([len(arr1), len(arr2)])
    for idx in range(0, min_length):
        arr.append(arr1[idx])
        arr.append(arr2[idx])
        if idx == min_length - 1:
            if min_length == len(arr1):
                arr.extend(arr2[idx + 1:])
            else:
                arr.extend(arr1[idx + 1:])
    return arr
