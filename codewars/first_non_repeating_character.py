"""
First Non-Repeating Character

:Author: BJ Peter Dela Cruz
"""
from collections import OrderedDict


def first_non_repeating_letter(string: str) -> str:
    """
    This function returns the first non-repeating character in :attr:`string`. Lowercase and
    uppercase letters are treated as the same character. This function will return the correct case
    of the initial letter.

    :param string: The input string
    :return: The first lowercase or uppercase non-repeating character in :attr:`string`
    """
    counts = OrderedDict()
    for char in string:
        if char.lower() in counts:
            counts[char.lower()] += 1
        else:
            counts[char.lower()] = 1
    first_char = None
    for char, count in counts.items():
        if count == 1:
            first_char = char
            break
    if first_char is None:
        return ''
    return first_char.upper() if first_char.upper() in string else first_char
