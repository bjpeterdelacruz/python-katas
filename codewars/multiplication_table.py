"""
Multiplication Table

:Author: BJ Peter Dela Cruz
"""


def multiplication_table(size: int) -> list[list[int]]:
    """
    This function returns a list of lists that represents a multiplication table.

    :param size: The input integer, the size of the table
    :return: A multiplication table up to :attr:`size`
    """
    return [[row * col for col in range(1, size + 1)] for row in range(1, size + 1)]
