"""
Greek Sort

:Author: BJ Peter Dela Cruz
"""


def greek_comparator(lhs: str, rhs: str) -> int:
    """
    This function compares two strings, each representing a Greek letter. The function returns 0 if
    both strings are the same, -1 if the first Greek letter (:attr:`lhs`) comes before the second
    Greek letter (:attr:`rhs`), or 1 if :attr:`rhs` comes before :attr:`lhs`.

    :param lhs: A string, the first Greek letter
    :param rhs: A string, the second Greek letter
    :return: -1, 0, or -1
    """
    greek_alphabet = (
        'alpha', 'beta', 'gamma', 'delta', 'epsilon', 'zeta',
        'eta', 'theta', 'iota', 'kappa', 'lambda', 'mu',
        'nu', 'xi', 'omicron', 'pi', 'rho', 'sigma',
        'tau', 'upsilon', 'phi', 'chi', 'psi', 'omega')
    lhs_idx = greek_alphabet.index(lhs)
    rhs_idx = greek_alphabet.index(rhs)
    return 0 if lhs_idx == rhs_idx else -1 if lhs_idx < rhs_idx else 1
