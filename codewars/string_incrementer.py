"""
String Incrementer

:Author: BJ Peter Dela Cruz
"""

# pylint: disable=too-many-branches


def increment_string(string: str) -> str:
    """
    This function increments the number at the end of :attr:`string`. For example, given "foobar01",
    this function will return "foobar02".

    :param string: The input string
    :return: The same string but with the number at the end of the string incremented by 1
    """
    if not string or not string[-1].isnumeric():
        return f"{string}1"
    numeric = ""
    for idx in range(len(string) - 1, -1, -1):
        if string[idx].isnumeric():
            numeric = f"{numeric}{string[idx]}"
        else:
            break
    numeric = ''.join(reversed(numeric))
    alpha = string[:string.rindex(numeric)]
    if '0' not in numeric or numeric[0] != '0':
        return f"{alpha}{int(numeric) + 1}"
    zeros = ""
    for char in numeric:
        if char.isnumeric():
            if char == '0':
                zeros = f"{zeros}0"
            else:
                break
    numbers = ""
    for idx, char in enumerate(numeric):
        if char.isnumeric() and char != '0':
            numbers = numeric[idx:]
            break
    if '9' * len(numbers) in numbers:
        zeros = zeros[:-1]
        if not numbers:
            numbers = "0"
    return f"{alpha}{zeros}{int(numbers) + 1}"
