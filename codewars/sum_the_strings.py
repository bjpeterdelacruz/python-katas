"""
Sum the Strings

:Author: BJ Peter Dela Cruz
"""


def sum_str(num_a: str, num_b: str) -> str:
    """
    This function sums two integers that are represented as strings. An empty string is treated as
    a zero.

    :param num_a: The first integer as a string
    :param num_b: The second integer as a string
    :return: The sum of both integers returned as a string
    """
    number_a = int(num_a) if num_a else 0
    number_b = int(num_b) if num_b else 0
    return str(number_a + number_b)
