"""
Decipher This!

:Author: BJ Peter Dela Cruz
"""
import re


def decipher_this(string: str):
    """
    To decipher the given string, the second letter and the last letter are switched, and a letter's
    character code is replaced by the letter itself. For example, "72olle" can be deciphered to
    "hello".

    :param string: The string to decipher
    :return: The deciphered version of the string
    """
    regex = re.compile("([0-9]+)([a-zA-Z]*)")
    deciphered_message = []
    for word in string.split():
        groups = regex.match(word).groups()
        if groups[-1]:
            word = list(word.replace(groups[0], chr(int(groups[0]))))
            word[1], word[-1] = word[-1], word[1]
            deciphered_message.append(''.join(word))
        else:
            deciphered_message.append(chr(int(groups[0])))
    return ' '.join(deciphered_message)
