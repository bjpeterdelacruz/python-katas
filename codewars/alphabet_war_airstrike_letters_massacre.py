"""
Alphabet War Airstrike

:Author: BJ Peter Dela Cruz
"""


def alphabet_war(fight: str) -> str:
    """
    This function replaces letters in the given string with underscores if they are next to a bomb
    that is represented by an asterisk. The letter immediately before and after the asterisk are
    each replaced by an underscore. If two asterisks are next to each other, the one on the right
    will not be replaced by an underscore. Asterisks are processed from left to right.

    :param fight: The input string representing a fight between two factions of letters
    :return: A string declaring which side won the fight or a stalemate
    """
    if len(fight) > 1:
        if fight[0] == '*' and fight[1] != '*':
            fight = "__" + fight[2:]
        if fight[-1] == '*' and fight[-2] != '*':
            fight = fight[:-2] + "__"
        for idx in range(1, len(fight)):
            if fight[idx] == '*':
                fight = fight[:idx - 1] + "__" + fight[idx + 1:]
                if idx + 1 < len(fight) and fight[idx + 1] != '*':
                    fight = fight[:idx + 1] + "_" + fight[idx + 2:]
    return count_sides(fight)


def count_sides(fight: str) -> str:
    """
    This helper function counts the number of powers of each side and determines whether there is a
    victor or stalemate.

    :param fight: The fight after an airstrike
    :return: A string declaring which side won the fight or a stalemate
    """
    left = {"w": 4, "p": 3, "b": 2, "s": 1}
    right = {"m": 4, "q": 3, "d": 2, "z": 1}

    totals = (0, 0)
    for character in fight:
        if character in left:
            totals = totals[0] + left[character], totals[1]
        elif character in right:
            totals = totals[0], totals[1] + right[character]

    if totals[0] > totals[1]:
        return "Left side wins!"
    return "Right side wins!" if totals[0] < totals[1] else "Let's fight again!"
