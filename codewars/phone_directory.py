"""
Phone Directory

:Author: BJ Peter Dela Cruz
"""


def phone(strings: str, num: str) -> str:
    """
    This function finds the phone number, address, and name of an individual given a phone number.
    If a phone number is associated with two or more individuals, an error string is returned. If
    the phone number is not found in the input list of strings, an error string is returned.

    :param strings: The input list of strings containing phone numbers, addresses, and names of
        people
    :param num: The input string, a phone number
    :return: A string containing the phone number, address, and name of an individual, or an error
        string
    """
    lines = strings.split("\n")
    found = [line for line in lines if num in line]
    if len(found) > 1:
        return f"Error => Too many people: {num}"
    if not found:
        return f"Error => Not found: {num}"
    record = found[0]
    name = record[record.index('<') + 1:record.index('>')].strip()
    phone_number = record[record.index('+') + 1:]
    idx = phone_number.index(' ') if ' ' in phone_number else len(phone_number)
    phone_number = phone_number[:idx]
    phone_number = ''.join(char for char in phone_number if char.isnumeric() or char == '-')
    address = record.replace(name, '').replace(phone_number, '')
    address = list(address)
    for idx, char in enumerate(address):
        if not char.isalnum() and char not in [' ', '.', '-']:
            address[idx] = ' '
    address = ' '.join(''.join(address).split())
    return f"Phone => {phone_number}, Name => {name}, Address => {address}"
