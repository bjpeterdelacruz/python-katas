"""
Words to Sentence

:Author: BJ Peter Dela Cruz
"""


def words_to_sentence(words: list[str]) -> str:
    """
    This function creates a string from a list of strings, each of which is separated by a space.

    :param words: The input list of strings
    :return: A string containing all the strings in :attr:`words`, separated by a space
    """
    return ' '.join(words)
