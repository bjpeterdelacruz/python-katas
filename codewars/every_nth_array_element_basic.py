def every(array: list, interval: int = 1, start_index: int = 0) -> list:
    result = []
    for idx, element in enumerate(array):
        if idx >= start_index and (idx - start_index) % interval == 0:
            result.append(element)
    return result
