"""
Slope of a Line

:Author: BJ Peter Dela Cruz
"""


def get_slope(point_1: list, point_2: list):
    """
    This function calculates the slope of a line. If the X values for both points are the same or
    :attr:`point_1` and :attr:`point_2` refer to the same point, None is returned. Otherwise, the
    slope is returned. The slope is the difference of the two Y values divided by the difference of
    the two X values.

    :param point_1: A list that represents the first point and contains two values, X and Y.
    :param point_2: A list that represents the second point and contains two values, X and Y.
    :return: The slope.
    """
    if point_1[0] == point_2[0] or point_1 == point_2:
        return None
    return (point_2[1] - point_1[1]) / (point_2[0] - point_1[0])
