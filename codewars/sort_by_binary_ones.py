"""
Sort by Binary Ones

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict


def sort_by_binary_ones(num_list: list[int]) -> list[int]:
    """
    This function sorts the given list of integers according to the number of ones each integer has.
    If the number of ones in two integers are the same, the integer whose binary string
    representation is the shortest goes first. If the number of ones in two integers are the same
    and the lengths of the binary string representations are the same, the integer that is the least
    goes first.

    :param num_list: The input list of integers
    :return: A sorted list of integers based on the number of ones in their binary representations
    """
    groups = defaultdict(list)
    for number in num_list:
        binary_number = bin(number)[2:]
        groups[binary_number.count("1")].append(binary_number)
    sorted_keys = reversed(sorted(groups.keys()))
    numbers = []
    for key in sorted_keys:
        numbers.extend(map(lambda x: int(x, 2), sorted(groups[key], key=int)))
    return numbers
