"""
All Star Code Challenge #24

Author: BJ Peter Dela Cruz
"""


def hypotenuse(side_a, side_b):
    """
    This function finds the hypotenuse of a triangle given two sides. It is assumed that the sides
    form a valid triangle.

    :param side_a: The first side of a triangle
    :param side_b: The second side of a triangle
    :return: The hypotenuse
    """
    return ((side_a * side_a) + (side_b * side_b)) ** 0.5


def leg(side_c, side_a):
    """
    This function finds one of the sides of a triangle given a side and a hypotenuse. It is assumed
    that the hypotenuse and the side form a valid triangle.

    :param side_c: The side of a triangle
    :param side_a: The hypotenuse of a triangle
    :return: The other side
    """
    return ((side_c * side_c) - (side_a * side_a)) ** 0.5
