"""
The Maximum and Minimum Difference (Challenge Version)

:Author: BJ Peter Dela Cruz
"""
from math import inf


def max_and_min(seq1, seq2):
    """
    This function returns a pair of numbers: the maximum and minimum differences between one number
    in :attr:`seq1` and another number in :attr:`seq2`. For example, given [3, 10, 5] and
    [20, 7, 15, 8], this function will return (17, 2) because 20 - 3 = 17 (maximum difference) and
    10 - 8 = 2 (minimum difference).

    :param seq1: The input list of integers
    :param seq2: The input list of integers
    :return: A pair of numbers that represent the maximum and minimum differences
    """
    seq1.sort()
    seq2.sort()

    maximum = max(seq1[-1] - seq2[0], seq2[-1] - seq1[0])

    minimum = inf
    index_1 = index_2 = 0
    while index_1 < len(seq1) and index_2 < len(seq2):
        minimum = min(minimum, abs(seq1[index_1] - seq2[index_2]))
        if seq1[index_1] < seq2[index_2]:
            index_1 += 1
        else:
            index_2 += 1

    return maximum, minimum
