"""
Turn with a Compass

:Author: BJ Peter Dela Cruz
"""


def direction(facing: str, turn: int) -> str:
    """
    This function returns the direction (north, northeast, east, southeast, south, southwest, west,
    or northwest) after turning left or right :attr:`turn` degrees from the initial direction
    :attr:`facing`.

    :param facing: One of eight directions that someone is initially facing
    :param turn: The number of degrees to turn in increments of 45
    :return: The new direction after turning left or right :attr:`turn` degrees
    """
    directions = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
    idx = ((turn // 45) + directions.index(facing)) % len(directions)
    return directions[idx]
