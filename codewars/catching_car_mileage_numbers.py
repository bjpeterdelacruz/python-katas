"""
Catching Car Mileage Numbers

:Author: BJ Peter Dela Cruz
"""


def is_interesting(number: int, awesome_phrases: list[int]) -> int:
    """
    This function returns 2 if :attr:`number`, which represents the mileage on an odometer, is an
    "interesting number", 1 if the "interesting number" is a mile or two miles ahead, or 0 if
    :attr:`number` is not an "interesting number". A number is "interesting" if one of the following
    is true: the number has one digit [1-9] followed by all zeros (e.g. 20000), the number contains
    the same digits (e.g. 7777), the digits in the number are increasing (e.g. 1234), the digits in
    the number are decreasing (e.g. 9876), the number is a palindrome (e.g. 12321), or the number
    matches any of the numbers in :attr:`awesome_phrases`. For sequences that are increasing, the
    digit 0 must come after 9, and for sequences that are decreasing, the digit 0 must come after 1.
    Only numbers greater than 99 are considered "interesting".

    :param number: The input integer representing mileage on an odometer
    :param awesome_phrases: A list of integers, may be empty
    :return: 0, 1, or 2
    """
    if number < 100:
        return 1 if number in [98, 99] else 0
    for idx, num in enumerate([number, number + 1, number + 2]):
        number_str = str(num)
        if number_str[1:] == len(number_str[1:]) * '0' or num in awesome_phrases:
            return 2 if idx == 0 else 1
        if ''.join(reversed(number_str)) == number_str or len(set(number_str)) == 1:
            return 2 if idx == 0 else 1
        if number_str in "9876543210" or number_str in "1234567890":
            return 2 if idx == 0 else 1
    return 0
