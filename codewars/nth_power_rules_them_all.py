"""
Nth Power Rules Them All

:Author: BJ Peter Dela Cruz
"""


def modified_sum(arr: list[int], nth: int) -> int:
    """
    This function raises each integer in :attr:`arr` to the :attr:`nth` power. The results are then
    summed together. Finally, the sum of all the integers in :attr:`arr` is subtracted from the
    first sum, and the difference is returned.

    :param arr: The input list of integers
    :param nth: The exponent each integer in :attr:`arr` is raised to
    :return: The difference between the two sums as described above
    """
    return sum(num ** nth for num in arr) - sum(arr)
