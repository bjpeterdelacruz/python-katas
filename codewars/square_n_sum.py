"""
Square(n) Sum

:Author: BJ Peter Dela Cruz
"""


def square_sum(numbers: list[int]) -> int:
    """
    This function squares each integer in the given list and then sums together the results.

    :param numbers: The input list of integers
    :return: The sum of all integers after each has been squared
    """
    return sum(map(lambda num: num ** 2, numbers))
