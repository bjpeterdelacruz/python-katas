"""
FizzBuzz Backwards

:Author: BJ Peter Dela Cruz
"""


def reverse_fizz_buzz(array: list) -> tuple:
    """
    This function finds the multiple for "Fizz" and the multiple for "Buzz". For example, given
    [1, "Fizz", "Buzz", "Fizz", 5, "FizzBuzz"], the function will return (2, 3). Note that "Fizz" or
    "Buzz" may not be present in the list, and the multiple for "Fizz" and the multiple for "Buzz"
    may be equal.

    :param array: The input list containing numbers and the words "Fizz", "Buzz", or "FizzBuzz".
    :return: A tuple containing the multiple for "Fizz" and the multiple for "Buzz"
    """
    fizz = []
    buzz = []
    fizzbuzz = []
    for idx, value in enumerate(array):
        if value == "Fizz":
            fizz.append(idx + 1)
        elif value == "Buzz":
            buzz.append(idx + 1)
        elif value == "FizzBuzz":
            fizzbuzz.append(idx + 1)
    if not fizz and not buzz and fizzbuzz:
        return fizzbuzz[0], fizzbuzz[0]
    if fizz and not buzz and fizzbuzz:
        return fizz[0], fizzbuzz[0]
    if not fizz and buzz and fizzbuzz:
        return fizzbuzz[0], buzz[0]
    return fizz[0], buzz[0]
