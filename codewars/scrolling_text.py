from collections import deque


def scrolling_text(text: str) -> list[str]:
    text_deque = deque(text.upper())
    temp = []
    for _ in text:
        temp.append(''.join(text_deque))
        text_deque.rotate(-1)
    return temp
