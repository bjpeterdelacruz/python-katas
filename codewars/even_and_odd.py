"""
Even and Odd

:Author: BJ Peter Dela Cruz
"""


def even_and_odd(number: int) -> tuple[int, int]:
    """
    This function returns a tuple that contains all the even digits in :attr:`number` concatenated
    together and all the odd digits in :attr:`number` concatenated together.

    :param number: The input integer
    :return: A tuple that contains all the even digits as an integer and all the odd digits also as
        an integer
    """
    return filter_number(number, 0), filter_number(number, 1)


def filter_number(number: int, even_or_odd: int) -> int:
    """
    This helper function returns an integer that consists of all even or odd digits in
    :attr:`number`. If there are no even or odd digits, then zero is returned.

    :param number: The input integer
    :param even_or_odd: 0 to return an integer that consists of all even digits, 1 to return an
        integer that consists of all odd digits
    :return: An integer, or zero if :attr:`number` does not consist of even or odd digits (depending
        on the value of :attr:`even_or_odd`)
    """
    temp = ''.join(filter(lambda x: int(x) % 2 == even_or_odd, str(number)))
    return int(temp) if temp else 0
