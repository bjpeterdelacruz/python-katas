"""
Shared Bit Counter

:Author: BJ Peter Dela Cruz
"""


def shared_bits(first_number: int, second_number: int) -> bool:
    """
    This function returns True if the two numbers share at least two ON bits in the same positions.

    :param first_number: The first input integer
    :param second_number: The second input integer
    :return: True if the two integers share the same ON bits, False otherwise
    """
    return f"{first_number & second_number:0b}".count("1") > 1
