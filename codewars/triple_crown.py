from collections import defaultdict


def triple_crown(receivers: dict[str, dict[str, int]]) -> str:
    statistics = defaultdict(list)
    for player_name, stats in receivers.items():
        for action, value in stats.items():
            statistics[action].append((player_name, value))
    players = set()
    for value in statistics.values():
        value.sort(key=lambda player: player[1])
        if value[-1][-1] == value[-2][-1]:
            return 'None of them'
        players.add(value[-1][0])
    return 'None of them' if len(players) > 1 else list(players)[0]
