from math import ceil


def pagination_text(page_number: int, page_size: int, total_products: int) -> str:
    start = (page_number - 1) * page_size + 1
    if page_number < ceil(total_products / page_size):
        end = page_number * page_size
    else:
        remaining = total_products % page_size
        end = (page_number - 1) * page_size + remaining if remaining > 0 else total_products
    return f'Showing {start} to {end} of {total_products} Products.'
