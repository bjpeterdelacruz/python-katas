"""
Most Frequently Used Words

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict
import re


def top_3_words(text: str) -> list[str]:
    """
    This function counts the number of words in :attr:`text`. A word in this case contains only
    letters (A-Z), and at least one letter, and any number of apostrophes. The top three most
    frequent words are returned in a list. When counting words, case is not taken into
    consideration.

    :param text: The input string
    :return: The top three most frequent words in :attr:`text`
    """
    words = [word for word in re.split("[^'a-zA-Z]", text) if word]
    counts = defaultdict(int)
    for word in words:
        if re.search("[a-zA-Z]+", word):
            counts[word.lower()] += 1
    counts = dict(reversed(sorted(counts.items(), key=lambda item: item[1])))
    return list(counts.keys())[:3]
