"""
Easy Balance Checking

:Author: BJ Peter Dela Cruz
"""


def balance(book):
    """
    This function balances the given checkbook. First, all characters except letters, numbers, dots,
    and spaces are removed. Then each line item in the checkbook is deducted from the given original
    balance. A string containing the total expense, average expense, and the balance at the end of
    each transaction is returned.

    :param book: The input string representing a checkbook
    :return: The checkbook with all invalid characters removed and including the total and average
        expenses
    """
    filtered = []
    for line in book.split("\n"):
        new_line = []
        for character in line:
            if character.isalnum() or character in ['.', ' ']:
                new_line.append(character)
        new_line = ''.join(new_line)
        if len(new_line.strip()) > 0:
            filtered.append(new_line.strip())

    new_balance = float(filtered.pop(0))
    total_expense = 0
    checkbook = [f"Original Balance: {new_balance:.2f}"]
    for line in filtered:
        arr = line.split(" ")
        expense = float(arr[-1])
        arr = arr[:-1]
        arr.append(f"{expense:.2f}")
        total_expense += expense
        new_balance -= expense
        arr.append(f"Balance {new_balance:.2f}")
        checkbook.append(' '.join(arr))
    checkbook.append(f"Total expense  {total_expense:.2f}")
    checkbook.append(f"Average expense  {total_expense / len(filtered):.2f}")
    return '\r\n'.join(checkbook)
