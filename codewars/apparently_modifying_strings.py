"""
Apparently Modifying Strings

:Author: BJ Peter Dela Cruz
"""


def apparently(string: str) -> str:
    """
    This function adds the word "apparently" after the word "and" or "but" only if "apparently" does
    not already come after either word.

    :param string: The input string
    :return: The same string with the word "apparently" added after either "and" or "but" if either
        of these words appears in :attr:`string`
    """
    arr = string.split()
    if 'and' not in arr and 'but' not in arr:
        return string
    if len(arr) == 1:
        return f'{arr[0]} apparently'
    for idx in range(0, len(arr) - 1):
        if arr[idx] in ['and', 'but'] and arr[idx + 1] != 'apparently':
            arr[idx] = f'{arr[idx]} apparently'
    if arr[-1] in ['and', 'but']:
        arr[-1] = f'{arr[-1]} apparently'
    return ' '.join(arr)
