"""
Valid Parentheses

:Author: BJ Peter Dela Cruz
"""


def valid_parentheses(string: str) -> bool:
    """
    This function returns True if the parentheses in :attr:`string` are nested and paired correctly,
    False otherwise.

    :param string: The input string that contains parentheses
    :return: True if the parentheses are nested correctly, False otherwise
    """
    str_copy = ''.join([char for char in string if char in ['(', ')']])
    while "()" in str_copy:
        str_copy = str_copy.replace("()", "")
    return len(str_copy) == 0
