"""
Simple Fun #319: Number and IP Address

:Author: BJ Peter Dela Cruz
"""


def number_and_ip_address(value: str) -> str:
    """
    This function converts :attr:`value` to a number if it is a 32-bit IP address. If :attr:`value`
    represents an integer, it is converted to an IP address.

    :param value: The input string, either an IP address or an integer
    :return: An IP address if :attr:`value` is a number or vice versa
    """
    if "." in value:
        numbers = value.split(".")
        return str(int(''.join([bin(int(number))[2:].zfill(8) for number in numbers]), 2))
    bits = bin(int(value))[2:].zfill(32)
    ranges = [(0, 8), (8, 16), (16, 24), (24, 32)]
    return '.'.join([f'{int(bits[start:end], 2)}' for start, end in ranges])
