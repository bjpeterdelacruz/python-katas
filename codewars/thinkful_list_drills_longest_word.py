"""
Thinkful: Longest Word

:Author: BJ Peter Dela Cruz
"""


def longest(words: list[str]) -> int:
    """
    This function finds the longest word in the given list and returns the number of characters in
    that word.

    :param words: The input list of strings
    :return: The number of characters in the longest word in :attr:`words`
    """
    return max(map(len, words))
