"""
Check if a Triangle is an Equable Triangle

:Author: BJ Peter Dela Cruz
"""


def heronian_triangle_formula(side_a: int, side_b: int, side_c: int) -> float:
    """
    This helper function calculates the area of a Heronian triangle, which is a triangle that has
    side lengths and an area that are all integers. The perimeter of such a triangle is always even,
    and there is at most one side that has an even length.

    :param side_a: The length of side A
    :param side_b: The length of side B
    :param side_c: The length of side C
    :return: The area of a Heronian triangle
    """
    s_value = 0.5 * (side_a + side_b + side_c)
    return (s_value * (s_value - side_a) * (s_value - side_b) * (s_value - side_c)) ** 0.5


def equable_triangle(side_a: int, side_b: int, side_c: int) -> bool:
    """
    This function returns True if the perimeter of a triangle is equal to its area. Such rectangles
    are called equable triangles. Note that the formula for calculating the area of a triangle is
    not 0.5 * :attr:`side_a` * :attr:`side_b`. Instead, it uses the formula for calculating the area
    of Heronian triangles.

    :param side_a: The input integer representing the length of side A
    :param side_b: The input integer representing the length of side B
    :param side_c: The input integer representing the length of side C
    :return: True if the perimeter and area are equal, False otherwise
    """
    return side_a + side_b + side_c == heronian_triangle_formula(side_a, side_b, side_c)
