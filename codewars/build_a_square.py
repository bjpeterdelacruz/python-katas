"""
Build a Square

:Author: BJ Peter Dela Cruz
"""


def generate_shape(number: int) -> str:
    """
    This function returns a newline-delimited string that represents a square of length
    :attr:`number`.

    :param number: The length of each side of the square
    :return: A newline-delimited string that represents a square
    """
    return '\n'.join(['+' * number] * number)
