"""
Define a Card Suit

:Author: BJ Peter Dela Cruz
"""


def define_suit(card: str) -> str:
    """
    This function returns the suit of the given playing card. For example, given "2C", "clubs" is
    returned.

    :param card: A string that is two or three characters long, the last character is the suit and
        the other characters represent the card's value
    :return: The suit name: "clubs", "spades", "diamonds", or "hearts"
    """
    suit = {"C": "clubs", "S": "spades", "D": "diamonds", "H": "hearts"}
    return suit[card[-1]]
