"""
Super Duper Easy

:Author: BJ Peter Dela Cruz
"""


def problem(value: int) -> int | str:
    """
    This function multiplies :attr:`value` by fifty and then increases the result by six. If the
    input parameter is not a number, "Error" is returned.

    :param value: The input number
    :return: A numeric result or "Error" if :attr:`value` is not a number
    """
    try:
        return int(str(value)) * 50 + 6
    except (TypeError, ValueError):
        return "Error"
