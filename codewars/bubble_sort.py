"""
Bubble Sort

:Author: BJ Peter Dela Cruz
"""


def bubble(lst: list[int]) -> list[list[int]]:
    """
    This function sorts the given list of integers using the bubble sort algorithm. After a swap has
    been made, a snapshot of :attr:`lst` is taken and added to a list, which is returned after
    :attr:`lst` is sorted.

    :param lst: The input list of integers to sort
    :return: A list of snapshots (i.e. state of :attr:`lst` after a swap has been made)
    """
    if not lst:
        return []
    snapshots = []
    while True:
        swapped = False
        for idx in range(0, len(lst) - 1):
            if lst[idx] > lst[idx + 1]:
                lst[idx], lst[idx + 1] = lst[idx + 1], lst[idx]
                snapshots.append(lst.copy())
                swapped = True
        if not swapped:
            break
    return snapshots
