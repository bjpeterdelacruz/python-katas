"""
Count All the Sheep on Farm in the Heights of New Zealand

:Author: BJ Peter Dela Cruz
"""


def lost_sheep(friday: list[int], saturday: list[int], total: int) -> int:
    """
    This function counts all the sheep that returned to the farm on Friday and Saturday, and returns
    the number of sheep that have not returned by subtracting the sum from the given total number of
    sheep. Sheep return to the farm in groups on Friday and Saturday, so each integer in both lists
    represents a group of sheep that returned to the farm.

    :param friday: A list of groups of sheep that returned to the farm on Friday
    :param saturday: A list of groups of sheep that returned to the farm on Saturday
    :param total: The total number of sheep
    :return: The number of sheep that have not returned to the farm
    """
    return total - sum(friday) - sum(saturday)
