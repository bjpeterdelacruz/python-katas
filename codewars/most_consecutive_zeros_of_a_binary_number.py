"""
Most Consecutive Zeros of a Binary String

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby
from math import inf

numbers_to_words = {1: "One", 2: "Two", 3: "Three", 4: "Four", 5: "Five", 6: "Six", 7: "Seven",
                    8: "Eight", 9: "Nine", 10: "Ten", 11: "Eleven", 12: "Twelve", 13: "Thirteen",
                    14: "Fourteen", 15: "Fifteen", 16: "Sixteen", 17: "Seventeen", 18: "Eighteen",
                    19: "Nineteen", 20: "Twenty", -inf: "Zero"}


def max_consec_zeros(string: str) -> str:
    """
    This function returns the most number of consecutive zeros in the binary number representation
    of :attr:`string`.

    :param string: The input string
    :return: The most number of consecutive zeros
    """
    most_zeros = -inf
    for key, value in groupby(f"{int(string):0b}"):
        length = len(list(value))
        if key == "0" and most_zeros < length:
            most_zeros = length
    return numbers_to_words[most_zeros]
