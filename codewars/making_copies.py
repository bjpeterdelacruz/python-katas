"""
Making Copies

:Author: BJ Peter Dela Cruz
"""


def copy_list(lst: list) -> list:
    """
    This function makes a copy of the given list. If the input is not a list, a ValueError is
    raised.

    :param lst: The input list to copy
    :return: A copy of :attr:`lst`
    """
    if not isinstance(lst, list):
        raise ValueError
    return lst.copy()
