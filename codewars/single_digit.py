"""
Single Digit

:Author: BJ Peter Dela Cruz
"""


def single_digit(number: int) -> int:
    """
    This function sums all the bits that are turned on in the given integer. If the given integer or
    the sum has two or more digits, repeat the summation until the sum has only one digit.

    :param number: The input integer
    :return: A one-digit sum of all the bits that are turned on, i.e. bit is set to 1
    """
    while len(str(number)) > 1:
        number = sum(1 for char in bin(number)[2:] if char == "1")
    return number
