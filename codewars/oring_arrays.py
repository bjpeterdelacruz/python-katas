"""
ORing Arrays

:Author: BJ Peter Dela Cruz
"""


def or_arrays(arr_1: list[int], arr_2: list[int], default_value: int = 0) -> list[int]:
    """
    This function performs a bitwise OR operation on an element in :attr:`arr_1` with the
    corresponding element in :attr:`arr_2`. If one list is shorter than the other, the former is
    padded with :attr:`default_value` until the lengths are equal.

    :param arr_1: The first input list of integers
    :param arr_2: The second input list of integers
    :param default_value: The value to append to the end of the shorter list
    :return: A list containing the results of the bitwise OR operation on each element in both lists
    """
    while len(arr_1) < len(arr_2):
        arr_1.append(default_value)
    while len(arr_2) < len(arr_1):
        arr_2.append(default_value)
    return [value | arr_2[idx] for idx, value in enumerate(arr_1)]
