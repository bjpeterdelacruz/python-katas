"""
Thinkful: Poem Formatter

:Author: BJ Peter Dela Cruz
"""


def format_poem(poem: str) -> str:
    """
    This function returns a string in which every sentence in the given string is on its own line.
    The string that is returned will not have any trailing whitespace characters.

    :param poem: The input string
    :return: A string in which every sentence is on its own line.
    """
    return poem.replace(". ", ".\n").strip()
