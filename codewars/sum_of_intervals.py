"""
Sum of Intervals

:Author: BJ Peter Dela Cruz
"""


def sum_of_intervals(intervals: list[int]) -> int:
    """
    This function calculates the sum of all the interval lengths in :attr:`intervals`. All intervals
    that overlap are folded into the interval(s) that contain them. For example, given intervals
    [1, 10], [5, 7], and [6, 11], the interval length is 10 because the intervals are treated as
    [1, 11] and 11 - 1 equals 10.

    :param intervals: A list of tuples representing intervals
    :return: The sum of all the interval lengths in :attr:`intervals`
    """
    intervals_list = [list(elem) for elem in intervals]
    intervals_list.sort(key=lambda x: x[0])
    idx = 1
    while idx < len(intervals_list):
        if intervals_list[idx - 1][1] >= intervals_list[idx][0]:
            interval = intervals_list.pop(idx)
            if interval[1] > intervals_list[idx - 1][1]:
                intervals_list[idx - 1][1] = interval[1]
            idx -= 1
        idx += 1
    return sum(interval[1] - interval[0] for interval in intervals_list)
