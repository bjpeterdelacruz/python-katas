"""
Array Manipulation

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=cell-var-from-loop


def array_manip(array: list[int]) -> list[int]:
    """
    This function replaces every element in the given list of integers with one that is to its right
    and is the least element out of all the elements that are greater than it. If there are no
    elements to its right that are greater than it, the element is replaced with -1. For example,
    given [1, 3, 2, 4, 5], [2, 4, -1, 5, -1] is returned.

    :param array: The input list of integers
    :return: A list of integers that are greater than the ones that they replaced
    """
    result = array.copy()
    for idx in range(0, len(array) - 1):
        sorted_subarray = sorted(filter(lambda x: x > array[idx], array[idx + 1:]))
        if not sorted_subarray or sorted_subarray[0] < array[idx]:
            result[idx] = -1
        else:
            result[idx] = sorted_subarray[0]
    result[-1] = -1
    return result
