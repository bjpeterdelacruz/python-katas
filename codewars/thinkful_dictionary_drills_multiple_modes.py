"""
Thinkful: Multiple Modes

:Author: BJ Peter Dela Cruz
"""
from collections import Counter


def modes(data) -> list:
    """
    This function returns a sorted list of all the elements in the given sequence that appear the
    most. For example, given "tomato", the list ["o", "t"] is returned. If all the elements appear
    the same number of times, e.g. "anna", an empty list is returned.

    :param data: The input sequence
    :return: A sorted list containing all the elements that appear the most, or an empty list
    """
    counter = Counter(data)
    counts = sorted(counter.values())
    return [] if len(set(counts)) == 1 else sorted(key for key, val in counter.items()
                                                   if val == counts[-1])
