"""
Simple Decrypt Algo

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase
from collections import defaultdict


def decrypt(test_key: str) -> str:
    """
    This function counts the number of lowercase characters in :attr:`test_key` and returns a
    26-character string with all the counts of each letter from a to z.

    :param test_key: The input string
    :return: A 26-character string representing the counts of each lowercase ASCII letter
    """
    counts = defaultdict(int)
    for char in test_key:
        if char in ascii_lowercase:
            counts[char] += 1
    return ''.join([str(counts[key]) for key in ascii_lowercase])
