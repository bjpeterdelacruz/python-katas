"""
Twice as Old

:Author: BJ Peter Dela Cruz
"""


def twice_as_old(dad_years_old: int, son_years_old: int) -> int:
    """
    This function calculates how many years ago when a father's age was twice the current age of his
    son, or how many years from now when the father's age will be twice his son's current age.

    :param dad_years_old: A father's current age
    :param son_years_old: His son's current age
    :return: The number of years ago when a father's age was twice his son's current age, or the
        number of years until the father's age will be twice his son's current age
    """
    if son_years_old == 0:
        return dad_years_old
    years_until = years_since = 0
    dad_age_until = dad_age_since = dad_years_old
    while son_years_old * 2 != dad_age_until and son_years_old * 2 != dad_age_since:
        dad_age_until += 1
        dad_age_since -= 1
        years_until += 1
        years_since += 1
    return years_until if son_years_old * 2 == dad_age_until else years_since
