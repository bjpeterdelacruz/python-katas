"""
Baby Count!

:Author: BJ Peter Dela Cruz
"""


def count_name(arr: list[str], name: str) -> int:
    """
    This function counts the number of times :attr:`name` appears in :attr:`arr`.

    :param arr: The input list of strings
    :param name: The name to find in :attr:`arr`
    :return: The number of times :attr:`name` appears in :attr:`arr`
    """
    return len(list(filter(lambda x: x == name, arr)))
