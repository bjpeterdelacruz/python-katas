"""
Lottery Machine

:Author: BJ Peter Dela Cruz
"""


def lottery(random_string: str) -> str:
    """
    This function filters out all the non-numeric characters in :attr:`random_string` and returns
    only unique numbers in the same order that they appear in the string. For example, given
    "xy5z6ABC5", this function will return "56".

    :param random_string: The input string containing alphanumeric characters
    :return: A string containing unique numbers
    """
    unique = set()
    arr = []
    for character in random_string:
        if character.isnumeric() and character not in unique:
            arr.append(character)
            unique.add(character)
    return ''.join(arr) if arr else "One more run!"
