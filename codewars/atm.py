"""
ATM

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict


def solve(amount: int) -> int:
    """
    This function returns the least number of currency notes that an ATM should dispense for
    :attr:`amount`. If it is impossible for the ATM to dispense currency notes for :attr:`amount`,
    -1 is returned. The denominations are 10, 20, 50, 100, 200, and 500.

    :param amount: The input integer
    :return: The least number of currency notes or -1 if the ATM cannot dispense currency notes for
        :attr:`amount`
    """
    if amount % 10 != 0:
        return -1
    counts = defaultdict(int)
    for value in [500, 200, 100, 50, 20, 10]:
        while amount >= value:
            counts[value] += 1
            amount -= value
    return sum(counts.values())
