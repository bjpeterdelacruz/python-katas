"""
First Reverse Try

:Author: BJ Peter Dela Cruz
"""


def first_reverse_try(arr: list[int]) -> list[int]:
    """
    This function switches the first element in the given list of integers with the last.

    :param arr: The input list of integers
    :return: The same list of integers but with the first element at the end of the list and the
        last element at the beginning of the list
    """
    if arr:
        arr[0], arr[-1] = arr[-1], arr[0]
    return arr
