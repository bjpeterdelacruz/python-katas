"""
Array Element Parity

:Author: BJ Peter Dela Cruz
"""
from collections import Counter


def solve(arr: list[int]) -> int:
    """
    This function returns the integer from the given list that is either only positive or only
    negative, i.e. the integer does not appear in the list as a negative number and again as a
    positive number.

    :param arr: The input list of integers
    :return: The integer that does not appear in the list twice, once as a negative and again as a
        positive
    """
    unique = set(arr)
    if len(unique) == len(arr):
        return sum(arr)
    return [key for key, value in Counter(arr).items() if value > 1][0]
