"""
How Much Hex is the Fish

:Author: BJ Peter Dela Cruz
"""


def fish_hex(name: str) -> int:
    """
    This function gathers all the hexadecimal digits A through F (regardless of case) in
    :attr:`name` and performs an exclusive OR (XOR) operation on all of them.

    :param name: The input string, the name of a fish
    :return: The result of the XOR operation on each hexadecimal digit found in :attr:`name`
    """
    hex_digits = [f"0x{char.upper()}" for char in name if char in "abcdefABCDEF"]
    result = 0
    for digit in hex_digits:
        result ^= int(digit, 16)
    return result
