"""
Center Yourself

:Author: BJ Peter Dela Cruz
"""


def center(string: str, width: int, fill: str = ' ') -> str:
    """
    This function returns a string in which :attr:`string` is at the center of a string of length
    :attr:`width`. Each side of the generated string is padded with the character in :attr:`fill`.
    If the length of the padding on each side cannot be of equal length, then the padding on the
    left side will be one character longer. If the length of the string is greater than
    :attr:`width`, then the string is simply returned.

    :param string: The string to center
    :param width: The length of the generated string
    :param fill: The character to pad on each side of the generated string
    :return: A string in which :attr:`string` is at the center, padded on each side by :attr:`fill`
    """
    if len(string) >= width:
        return string
    num_chars = width - len(string)
    left, right = num_chars // 2, num_chars // 2
    if num_chars % 2 == 1:
        left += 1
    return f'{fill * left}{string}{fill * right}'
