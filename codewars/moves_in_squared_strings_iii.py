"""
Moves in Squared Strings 3

:Author: BJ Peter Dela Cruz
"""


def transform(string: list[str], rotate: bool = False, selfie: bool = False) -> str:
    """
    This helper function applies the appropriate string transformation based on the values of
    :attr:`rotate` and :attr:`selfie`. If :attr:`rotate` and :attr:`selfie` are both False, the
    diagonal transformation is applied. (See :func:`diag_1_sym`.) If :attr:`rotate` is True, the
    rotate transformation is applied. (See :func:`rot_90_clock`.) If :attr:`selfie` is True, both
    the selfie and diagonal transformations are applied. (See :func:`selfie_and_diag1`.)

    :param string: The list of strings to transform
    :param rotate: True to apply the rotate transformation
    :param selfie: True to apply the selfie and diagonal transformations
    :return: A newline-delimited list of strings that is the result of the appropriate
        transformation
    """
    col_idx = 0
    result = []
    while col_idx < len(string[0]):
        word = []
        for row in string:
            word.append(row[col_idx])
        if rotate:
            result.append(''.join(reversed(word)))
        elif selfie:
            result.append(f"{string[col_idx]}|{''.join(word)}")
        else:
            result.append(''.join(word))
        col_idx += 1
    return '\n'.join(result)


def diag_1_sym(string: list[str]) -> str:
    """
    This helper function delegates the diagonal transformation to the :func:`transform` function,
    which will return strings that are symmetric with respect to the main diagonal.

    :param string: The list of strings to transform
    :return: A newline-delimited list of strings that are symmetric with respect to the main
        diagonal
    """
    return transform(string)


def rot_90_clock(string: list[str]) -> str:
    """
    This helper function delegates the rotate transformation to the :func:`transform` function,
    which will return the strings rotated 90 degrees clockwise.

    :param string: The list of strings to transform
    :return: A newline-delimited list of strings that have been rotated 90 degrees clockwise
    """
    return transform(string, True)


def selfie_and_diag1(string: list[str]) -> str:
    """
    This helper function delegates the selfie and diagonal transformations to the :func:`transform`
    function, which will return the original list of strings plus strings that are symmetric with
    respect to the main diagonal.

    :param string: The list of strings to transform
    :return: A newline-delimited list that contains the original strings and strings that are
        symmetric with respect to the main diagonal
    """
    return transform(string, False, True)


def oper(func, string: str) -> str:
    """
    This function delegates the string transformation to the appropriate given function. All the
    helper functions operate on a string with N lines, each string containing N characters.

    :param func: The function that will apply a transformation to :attr:`string`
    :param string: The input string to transform
    :return: The string after a transformation has been applied
    """
    return func(string.split("\n"))
