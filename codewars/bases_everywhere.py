"""
Bases Everywhere

:Author: BJ Peter Dela Cruz
"""


def base_finder(seq: list[str]) -> int:
    """
    This function returns the radix or base (between 2 and 10, inclusive) based on the numbers in
    the given unsorted list.

    :param seq: The input list of integers
    :return: The base (radix) between 2 and 10, inclusive
    """
    seq = sorted(map(int, seq))
    idx = 1
    while idx <= 9:
        if seq[idx] - seq[idx - 1] > 1:
            return int(str(seq[idx - 1])[-1]) + 1
        idx += 1
    return 10
