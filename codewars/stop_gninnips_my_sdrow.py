"""
Stop Spinning My Words

:Author: BJ Peter Dela Cruz
"""


def spin_words(sentence: str) -> str:
    """
    This function reverses all words in :attr:`sentence` that contain five or more characters.

    :param sentence: The input string
    :return: The same string but with all words that contain five or more characters reversed
    """
    return ' '.join([word[::-1] if len(word) >= 5 else word for word in sentence.split()])
