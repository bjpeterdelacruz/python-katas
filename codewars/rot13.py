"""
ROT13

:Author: BJ Peter Dela Cruz
"""


def rot13(string: str) -> str:
    """
    This function shifts all the alphabet (A through Z) characters in :attr:`string` to the right 13
    times, wrapping around to the beginning if the end is reached. Other characters such as numbers
    and symbols are left alone.

    :param string: The input string
    :return: The encrypted version of :attr:`string`
    """
    return ''.join(shift_character(c, 97) if 97 <= ord(c) <= 122 else c
                   for c in ''.join(shift_character(c, 65) if 65 <= ord(c) <= 90 else c
                                    for c in string))


def shift_character(char: str, offset: int) -> str:
    """
    This helper function shifts the character :attr:`char` to the right 13 times after first
    subtracting :attr:`offset`. Because the ASCII code for lowercase a and uppercase A do not start
    at 0, :attr:`offset` has to be subtracted from the ASCII value of :attr:`char`. After the shift
    is applied, :attr:`offset` is added back.

    :param char: The character to shift
    :param offset: The offset
    :return: A character
    """
    return chr((ord(char) - offset + 13) % 26 + offset)
