"""
Compress Sentences

:Author: BJ Peter Dela Cruz
"""


def compress(sentence: str) -> str:
    """
    This function adds each word in the given sentence to a list (ignoring case and only including
    each word once) and returns a string that contains indexes (starting with zero) at which the
    words from the sentence appear in the list.

    :param sentence: The input string
    :return: A string that contains indexes
    """
    words = [word.lower() for word in sentence.split()]
    distinct = set()
    words_list = []
    indices = []
    for word in words:
        if word in distinct:
            indices.append(words_list.index(word))
        else:
            words_list.append(word)
            indices.append(len(words_list) - 1)
            distinct.add(word)
    return ''.join(map(str, indices))
