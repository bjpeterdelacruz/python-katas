"""
All Star Code Challenge #31

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class Instruction:
    """
    This class represents an instruction for mixing chemicals.
    """
    def __init__(self, amount, solution, instrument, note=None):
        """
        Initializes an instruction with an amount in milliliters, the chemical solution, and the
        instrument to use. A note may or may not be included with this instruction.

        :param amount: The amount in milliliters (mL)
        :param solution: The chemical solution
        :param instrument: The instrument to use
        :param note: A note, may be None
        """
        self.amount = amount
        self.solution = solution
        self.instrument = instrument
        if note:
            self.note = note


def help_jesse(arr: list[Instruction]) -> list[str]:
    """
    This function generates a list of strings, each representing an instruction for pouring a
    chemical solution. For each Instruction object, the function checks whether the note attribute
    is present. If it is, a note is written in parenthesis and appended to the end of an
    instruction.

    :param arr: The input list of Instruction objects
    :return: A list of strings, each representing an instruction for pouring a chemical solution
    """
    instructions = []
    for instruction in arr:
        temp = [f'Pour {instruction.amount} mL of {instruction.solution} into a '
                f'{instruction.instrument}']
        if hasattr(instruction, 'note'):
            temp.append(f'({instruction.note})')
        instructions.append(' '.join(temp))
    return instructions
