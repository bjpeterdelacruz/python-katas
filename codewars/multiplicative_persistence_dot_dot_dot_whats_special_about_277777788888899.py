"""
Multiplicative Persistence... What's Special About 277777788888899?

:Author: BJ Peter Dela Cruz
"""


def per(number: int) -> list[int]:
    """
    This function multiplies all the digits in the given integer and then adds the result to a list.
    If the sum contains two or more digits, the process is repeated until the sum contains only one
    digit, i.e., the sum is less than ten. The list of all the sums is then returned.

    :param number: The input integer
    :return: A list of sums
    """
    arr = []
    result = number
    while result > 9:
        temp = 1
        for digit in str(result):
            temp *= int(digit)
        arr.append(temp)
        result = temp
    return arr
