"""
Prize Draw

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase as chars


def rank(participants: str, weights: list[int], winner_rank: int) -> str:
    """
    This function returns the name of the person with the winning number at position
    :attr:`winner_rank` in the list of participants. The winning number is calculated as follows:
    sum the letters of a participant's name (letters A-Z represent numbers 1-26), add the length
    of the participant's name to the sum, and multiply the result by a weight in :attr:`weights` to
    get the winning number. The participants are sorted in descending order by winning numbers, but
    if two or more winning numbers are the same, those participants are sorted in alphabetical
    order. indexes in the list of participants with winning numbers start at 1.

    :param participants: The input list of strings
    :param weights: The input list of integers
    :param winner_rank: The input integer representing the position in the list of winning
        participants
    :return: The name of the participant at position :attr:`winner_rank` after calculating all the
        winning numbers and sorting the results
    """
    if not participants:
        return "No participants"
    list_participants = participants.split(",")
    if winner_rank > len(list_participants):
        return "Not enough participants"
    winning_numbers = []
    idx = 0
    for first_name in list_participants:
        winning_numbers.append(
            (first_name,
             (len(first_name) + sum(chars.index(char.lower()) + 1 for char in first_name))
             * weights[idx]))
        idx += 1
    winning_numbers.sort(key=lambda tup: tup[0])
    winning_numbers.sort(key=lambda tup: tup[1], reverse=True)
    return winning_numbers[winner_rank - 1][0]
