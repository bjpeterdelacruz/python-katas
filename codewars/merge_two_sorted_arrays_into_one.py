"""
Merge Two Sorted Arrays Into One

:Author: BJ Peter Dela Cruz
"""


def merge_arrays(arr1, arr2):
    """
    This function merges the contents of two lists and returns a sorted list with all duplicates
    removed.

    :param arr1: The first input list
    :param arr2: The second input list
    :return: A sorted list containing the contents of both input lists with all duplicates removed
    """
    new_arr = set(arr1)
    new_arr.update(arr2)
    return sorted(list(new_arr))
