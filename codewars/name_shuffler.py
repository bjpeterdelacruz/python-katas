"""
Name Shuffler

:Author: BJ Peter Dela Cruz
"""


def name_shuffler(name: str) -> str:
    """
    This function swaps a first name with a last name.

    :param name: A string with a first name followed by a last name
    :return: A string with a last name followed by a first name
    """
    first, second = name.split()
    return f'{second} {first}'
