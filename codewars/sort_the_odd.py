"""
Sort the Odd

:Author: BJ Peter Dela Cruz
"""


def sort_the_odd(int_arr: list[int]) -> list[int]:
    """
    This function sorts the odd numbers in the given list in ascending order but does not change the
    order of the even numbers and leaves them in place. For example, given [9, 2, 4, 1, 6, 5], this
    function will return [1, 2, 4, 5, 6, 9].

    :param int_arr: The input list of integers
    :return: The same list but with odd numbers sorted in ascending order and even numbers
        remaining in place
    """
    if len(int_arr) == 0:
        return []
    numbers = []
    odd = []
    for number in int_arr:
        if number % 2 == 0:
            numbers.append(str(number))
        else:
            odd.append(number)
            numbers.append("None")
    odd.sort()
    numbers = ' '.join(numbers)
    for number in odd:
        numbers = numbers.replace("None", str(number), 1)
    return list(map(int, numbers.split()))
