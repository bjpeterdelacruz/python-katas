"""
Sort and Star

:Author: BJ Peter Dela Cruz
"""


def two_sort(array: list[str]) -> str:
    """
    This function sorts the given list of strings and then returns the first string after inserting
    three asterisks between each character in the string.

    :param array: The input list of strings
    :return: The first string in the sorted list with three asterisks between each character
    """
    return '***'.join(sorted(array)[0])
