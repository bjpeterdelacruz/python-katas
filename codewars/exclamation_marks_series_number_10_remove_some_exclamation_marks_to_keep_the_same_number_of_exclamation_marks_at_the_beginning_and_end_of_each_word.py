"""
Exclamation Marks Series #10: Remove Some Exclamation Marks to Keep the Same Number of Exclamation
Marks at the Beginning and End of Each Word

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def remove(string: str) -> str:
    """
    This function removes the minimum number of exclamation marks from the beginning or end of each
    word in :attr:`string` so that the number of exclamation marks on both sides is the same. For
    example, given "!!!Hi! Hello! !!Bye!!!", the string "!Hi! Hello !!Bye!!" is returned.

    :param string: The input string
    :return: A string that contains the same number of exclamation marks on both sides of each word
    """
    temp = []
    for word in string.split():
        counts = [len(list(group)) for key, group in groupby(word) if key == '!']
        word = word.replace("!", "")
        temp.append(f'{"!" * min(counts)}{word}{"!" * min(counts)}' if len(counts) == 2 else word)
    return ' '.join(temp)
