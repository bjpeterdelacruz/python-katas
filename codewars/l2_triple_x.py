"""
L2: Triple X

:Author: BJ Peter Dela Cruz
"""


def triple_x(string: str) -> bool:
    """
    This function returns True if the first instance of "x" is followed by the string "xx". Only
    lowercase x's count, and if there are no x's in the string, False is returned.

    :param string: The input string
    :return: True if "x" is followed by "xx", False otherwise
    """
    for idx, char in enumerate(string):
        if char == 'x':
            if string[idx + 1:idx + 3] == 'xx':
                return True
            return False
    return False
