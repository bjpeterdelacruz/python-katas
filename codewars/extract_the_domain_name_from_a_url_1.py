"""
Extract the Domain Name from a URL

:Author: BJ Peter Dela Cruz
"""


def domain_name(url: str) -> str:
    """
    This function extracts the domain name from the given :attr:`url`. For example, given
    "http://www.bjdelacruz.com", this function will return "bjdelacruz".

    :param url: The input string representing a URL
    :return: The domain name in the URL
    """
    url = url.replace("http://", "").replace("https://", "").replace("www.", "")
    if "/" in url:
        url = url[:url.index("/")]
    if ".com" in url:
        url = url[:url.index(".com")]
    if ".co." in url:
        url = url[:url.index(".co.")]
    url = url.split('.')
    return url[-1] if len(url) < 2 else url[-2]
