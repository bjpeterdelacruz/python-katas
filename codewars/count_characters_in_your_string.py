"""
Count Characters in Your String

:Author: BJ Peter Dela Cruz
"""
from collections import Counter


def count(string):
    """
    This function counts each character in the given string and returns a dictionary with all the
    counts.

    :param string: The input string
    :return: A dictionary containing the number of times each character appears in :attr:`string`
    """
    return dict(Counter(string))
