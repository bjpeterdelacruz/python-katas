"""
Are You Available

:Author: BJ Peter Dela Cruz
"""
from datetime import datetime


def check_availability(schedule: list[list[str]], current_time: str):
    """
    This function returns True if :attr:`current_time` is not in the middle of an appointment,
    which is represented by list containing a start time and an end time in :attr:`schedule`.
    Otherwise, the function will return the next available time, which is the end time of the
    current appointment.

    :param schedule: A list of list of start and end times, which represent appointments
    :param current_time: The time in 24-hour format
    :return: True if :attr:`current_time` is not in the middle of an appointment, or the end time of
        the current appointment
    """
    time = datetime.strptime(current_time, '%H:%M')
    for times in schedule:
        start = datetime.strptime(times[0], '%H:%M')
        end = datetime.strptime(times[1], '%H:%M')
        if start < time < end:
            return end.strftime('%H:%M')
    return True
