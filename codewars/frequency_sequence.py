from collections import defaultdict


def freq_seq(string: str, separator: str) -> str:
    counts = defaultdict(int)
    for char in string:
        counts[char] += 1
    return separator.join(str(counts[char]) for char in string)
