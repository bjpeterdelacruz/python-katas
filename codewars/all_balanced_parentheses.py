"""
Balanced Parentheses

:Author: BJ Peter Dela Cruz
"""


def dfs(parens: list[str], left: int, right: int, string: str) -> None:
    """
    This helper function generates parentheses and adds them to the :attr:`parens` list.
    :attr:`left` and :attr:`right` represent the remaining number of ( and ) that need to be added,
    respectively.

    :param parens: The list of parentheses
    :param left: The remaining number of ( that need to be added
    :param right: The remaining number of ) that need to be added
    :param string: The string to append to each parenthesis, initially an empty string
    :return: None, the parentheses are added to :attr:`parens`
    """
    if left > right:
        return
    if left == 0 and right == 0:
        parens.append(string)
        return
    if left > 0:
        dfs(parens, left - 1, right, f'{string}(')
    if right > 0:
        dfs(parens, left, right - 1, f'{string})')


def balanced_parens(number: int) -> list[str]:
    """
    This function generates balanced (valid) parentheses using the recursive :func:`dfs` function.

    :param number: The input integer, number of pairs of parentheses to generate
    :return: A string of balanced parentheses
    """
    parens = []
    dfs(parens, number, number, "")
    return parens
