"""
Linked Lists: Get Nth Node

:Author: BJ Peter Dela Cruz
"""
from utils.linked_list import Node
from codewars.linked_lists_length_and_count import length


def get_nth(node: Node, index: int) -> Node:
    """
    This function gets the node at the given index in the linked list. If None is passed in,
    index is negative, or index is equal to or greater than the length of the linked list, a
    ValueError is raised.

    :param node: The linked list
    :param index: The index in the linked list
    :return: The node at the given index
    """
    if not node or index < 0 or length(node) <= index:
        raise ValueError
    counter = 0
    while counter != index:
        node = node.next
        counter += 1
    return node
