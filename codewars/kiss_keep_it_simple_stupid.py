"""
Keep It Simple Stupid

:Author: BJ Peter Dela Cruz
"""


def is_kiss(words: str) -> str:
    """
    This function returns "Good work Joe!" if the length of each word is equal to or less than the
    number of words in :attr:`words`. Otherwise, if one word contains more characters than the
    number of words, "Keep It Simple Stupid" is returned.

    :param words: The input string
    :return: "Good work Joe!" or "Keep It Simple Stupid"
    """
    arr = words.split()
    for word in arr:
        if len(word) > len(arr):
            return "Keep It Simple Stupid"
    return "Good work Joe!"
