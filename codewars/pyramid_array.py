"""
Pyramid Array

:Author: BJ Peter Dela Cruz
"""


def pyramid(number: int) -> list[list]:
    """
    This function generates a list of length :attr:`number` that contains zero or more sublists. The
    first sublist will contain one element, the second will contain two, the third will contain
    three, etc. The last sublist will contain :attr:`number` elements.

    :param number: The input integer, the length of the list
    :return: A list containing sublists
    """
    return [[1] * times for times in range(1, number + 1)]
