"""
Separate Basic Types

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict


def separate_types(seq: list) -> dict:
    """
    This function generates a dict of types with a list of values that belong to that type.

    :param seq: An input list that contains various types (integers, booleans, strings, etc.)
    :return: A dict that contains lists of types that belong to a type
    """
    types = defaultdict(list)
    for obj in seq:
        types[type(obj)].append(obj)
    return dict(types)
