"""
Convert to Binary

:Author: BJ Peter Dela Cruz
"""


def to_binary(number: int) -> int:
    """
    This function returns the base-2 representation of the given base-10 integer.

    :param number: The input integer
    :return: The same integer in base-2 representation
    """
    return int(bin(number)[2:])
