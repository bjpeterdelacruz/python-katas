"""
Triangular Numbers

:Author: BJ Peter Dela Cruz
"""


def triangular_range(start: int, stop: int) -> dict[int, int]:
    """
    This function returns a dictionary of all numbers in the range [start, stop] as keys and their
    associated triangular number if the latter exists. If a triangular number for a given number in
    the range does not exist, that number will not be added as a key to the dictionary. A triangular
    number is defined as the number of asterisks that can form an equilateral triangle.

    :param start: The start of the range, inclusive
    :param stop: The end of the range, inclusive
    :return: A dictionary of all numbers in the range [start, stop] as keys and their associated
    triangular numbers
    """
    numbers = {}
    for number in range(start, stop + 1):
        sqrt = (number * 8 + 1) ** 0.5
        if sqrt % 1 == 0:
            numbers[int(sqrt - 1) // 2] = number
    return numbers
