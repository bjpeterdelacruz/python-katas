"""
Alpha to Numeric and Numeric to Alpha

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase
import re


def alpha_num_num_alpha(string: str) -> str:
    """
    This function converts every letter in the given string to a number and vice versa. The letter
    a is converted to 1, b to 2, c to 3, etc.

    :param string: The input string containing letters and numbers
    :return: A string with every letter in :attr:`string` converted to a number and vice versa
    """
    lowercase = ' ' + ascii_lowercase
    characters = [chars for chars in re.split(r'(\d\d?)', string) if chars]
    result = []
    for chars in characters:
        if chars.isnumeric():
            result.append(lowercase[int(chars)])
        else:
            for letter in chars:
                result.append(str(lowercase.index(letter)))
    return ''.join(result)
