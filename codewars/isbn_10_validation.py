"""
ISBN-10 Validation

:Author: BJ Peter Dela Cruz
"""


def valid_isbn10(isbn: str) -> bool:
    """
    This function validates an ISBN 10 identifier, which is ten digits long. The first nine
    characters are digits 0-9. The last digit can be 0-9 or X whose value is 10.

    An ISBN 10 identifier is valid if the sum of the digits multiplied by their position (starting
    with 1) modulo 11 equals zero.

    :param isbn: The input string, the ISBN 10 identifier to validate
    :return: True if the ISBN 10 identifier is valid, False otherwise
    """
    if len(isbn) != 10:
        return False
    if "X" in isbn and isbn[-1] != "X":
        return False
    try:
        values = [int(value) * (idx + 1) for idx, value in enumerate(isbn[:-1])]
    except ValueError:
        return False
    if "X" in isbn:
        values.append(10 * 10)
    elif isbn[-1].isnumeric():
        values.append(int(isbn[-1]) * 10)
    else:
        return False
    return sum(values) % 11 == 0
