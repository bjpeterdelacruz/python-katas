"""
Linked Lists: Append

:Author: BJ Peter Dela Cruz
"""
from codewars.linked_lists_insert_nth_node import insert_nth, length


def append(first_list, second_list):
    """
    This function appends all the elements in :attr:`second_list` to the end of :attr:`first_list`.
    If both lists are None, None is returned. If one of the lists is None, the other list is
    returned.

    :param first_list: The first linked list
    :param second_list: The second linked list
    :return: A concatenated linked list
    """
    if not first_list and not second_list:
        return None
    if not first_list:
        return second_list
    if not second_list:
        return first_list
    next_node = second_list
    while next_node:
        first_list = insert_nth(first_list, length(first_list), next_node.data)
        next_node = next_node.next
    return first_list
