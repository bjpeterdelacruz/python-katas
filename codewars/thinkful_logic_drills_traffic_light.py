"""
Thinkful: Logic Drills - Traffic Light

:Author: BJ Peter Dela Cruz
"""


def update_light(current: str) -> str:
    """
    This function simulates a traffic signal. If the current light is "green", then the light that
    appears next, "yellow", is returned. If the current light is "yellow", then "red" is returned.
    Finally, if the current light is "red", then "green" is returned.

    :param current: The current light: "red", "yellow", or "green"
    :return: The light that will appear next
    """
    return "green" if current == "red" else "yellow" if current == "green" else "red"
