"""
All Star Code Challenge #15

Author: BJ Peter Dela Cruz
"""


def add_arrays(arr_1: list, arr_2: list) -> list:
    """
    This function adds (if numbers) or concatenates (if strings) elements in :attr:`arr_1` with the
    elements in :attr:`arr_2` that are at the same index position. Both lists must be of the same
    length, or an error will be raised if they are not.

    :param arr_1: The first input list
    :param arr_2: The second input list
    :return: A new list with the elements from both lists added or concatenated together
    """
    if len(arr_1) != len(arr_2):
        raise ValueError
    return [value + arr_2[idx] for idx, value in enumerate(arr_1)]
