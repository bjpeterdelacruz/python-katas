"""
Exclamation Marks Series #15: Replace the Pair of Exclamation Marks and Question Marks to Spaces

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def replace(string: str) -> str:
    """
    This function replaces all pairs of exclamation marks and question marks with spaces. A pair
    consists of one or more exclamation marks and an equal number of question marks. Both
    exclamation marks and questions marks do not need to be adjacent to each other. For example,
    given "??!!!?!!???!!", the string "     ?     !!" is returned. In the example, "??" and "!!"
    make up one pair, and "!!!" and "???" make up another pair.

    :param string: The input string
    :return: A string that contains spaces if :attr:`string` contains pairs of exclamation marks and
        question marks
    """
    groups = [[key, len(list(group))] for key, group in groupby(string)]
    for idx, group in enumerate(groups):
        symbol_1, count_1 = group[0], group[1]
        for idx2 in range(idx + 1, len(groups)):
            symbol_2, count_2 = groups[idx2][0], groups[idx2][1]
            if count_1 == count_2 and symbol_1 != symbol_2 and symbol_1 != " " and symbol_2 != " ":
                groups[idx][0] = " "
                groups[idx2][0] = " "
                break
    return ''.join(group[0] * group[1] for group in groups)
