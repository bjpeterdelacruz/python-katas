"""
Permutations

:Author: BJ Peter Dela Cruz
"""
from itertools import permutations as permute


def permutations(string: str) -> list[str]:
    """
    This function returns all permutations of the given string, removing all duplicates if they are
    present.

    :param string: The input string
    :return: A list of all permutations of :attr:`string`, sorted in ascending order
    """
    return sorted({''.join(permutation) for permutation in permute(string)})
