"""
flatten()

:Author: BJ Peter Dela Cruz
"""


def flatten(*array):
    """
    This function flattens all the lists in :attr:`array` into one single list.

    :param array: The input list that contains sublists of multiple levels
    :return: A list with no sublists
    """
    flattened_array = []
    idx = 0
    while idx < len(array):
        if isinstance(array[idx], list):
            flattened_array.extend(flatten(*array[idx]))
        else:
            flattened_array.append(array[idx])
        idx += 1
    return flattened_array
