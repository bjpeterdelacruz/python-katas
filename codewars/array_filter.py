"""
Array Filter

Author: BJ Peter Dela Cruz
"""


def get_even_numbers(arr: list[int]) -> list[int]:
    """
    Given a list of integers, this function returns a list of all integers that are even.

    :param arr: The input list of integers
    :return: A list of all even integers
    """
    return list(filter(lambda x: x % 2 == 0, arr))
