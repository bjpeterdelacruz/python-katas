"""
Removing Elements

:Author: BJ Peter Dela Cruz
"""


def remove_every_other(my_list: list) -> list:
    """
    This function removes all elements in the given list that are at even indexes. For example,
    given ["Hi", "Hello", 2, "World!", 0.5, True], the list ["Hello", "World!", True] is returned.

    :param my_list: The input list
    :return: An empty list or a list that contains fewer elements than :attr:`my_list`
    """
    return [element for idx, element in enumerate(my_list) if idx % 2 == 0]
