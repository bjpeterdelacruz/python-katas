"""
int32 to IPv4

:Author: BJ Peter Dela Cruz
"""


def int32_to_ip(int32: int) -> str:
    """
    This function converts the given 32-bit integer to an IP version 4 address string.

    :param int32: The input integer
    :return: An IP version 4 address
    """
    binary_digits = f"{int32:032b}"
    first = binary_digits[:8]
    second = binary_digits[8:16]
    third = binary_digits[16:24]
    fourth = binary_digits[24:]
    return f"{int(first, 2)}.{int(second, 2)}.{int(third, 2)}.{int(fourth, 2)}"
