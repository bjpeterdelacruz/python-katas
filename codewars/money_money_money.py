"""
Money Money Money

:Author: BJ Peter Dela Cruz
"""


def calculate_years(principal, interest_rate, tax_rate, desired):
    """
    This function calculates the number of years it will take for the given principal to reach the
    given desired amount. Interest on the principal is paid yearly, and only the interest is taxed,
    based on the tax rate. After paying the tax, the new amount is re-invested, which continues
    until the desired amount is reached.

    :param principal: The principal (the amount to invest)
    :param interest_rate: The yearly interest rate
    :param tax_rate: The tax rate (only the interest is taxed)
    :param desired: The desired amount
    :return: The number of years to invest in order to reach the desired amount
    """
    number_of_years = 0
    while principal < desired:
        interest = principal * interest_rate
        tax = interest * tax_rate
        principal += (interest - tax)
        number_of_years += 1
    return number_of_years
