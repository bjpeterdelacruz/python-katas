"""
Airport Arrivals/Departures #1

:Author: BJ Peter Dela Cruz
"""
ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ?!@#&()|<>.:=-+*/0123456789"


def flap_display(lines: list[str], rotors: list[list[int]]) -> list[str]:
    """
    This function rotates each character in each string in :attr:`lines` using a rotor in
    :attr:`rotors`. The result is a list of strings whose characters were rotated to the right.

    :param lines: The input list of strings
    :param rotors: The input list of list of integers, each list representing a rotor for each
        string in :attr:`lines`
    :return: The list of strings after rotating each character in each string
    """
    results = []
    idx = 0
    for line in lines:
        rotors_for_line = rotors[idx]
        rotor_idx = 2
        while rotor_idx <= len(rotors_for_line):
            rotors_for_line[rotor_idx - 1] = sum(rotors_for_line[rotor_idx - 2:rotor_idx])
            rotor_idx += 1
        rotor_idx = 0
        result = []
        for char in line:
            char_idx = ALPHABET.index(char.upper()) + rotors_for_line[rotor_idx]
            char_idx %= len(ALPHABET)
            result.append(ALPHABET[char_idx])
            rotor_idx += 1
        results.append(''.join(result))
        idx += 1
    return results
