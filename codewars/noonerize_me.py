"""
Noonerize Me

:Author: BJ Peter Dela Cruz
"""


def noonerize(numbers: list[int]) -> int | str:
    """
    This function swaps the first digit of the first integer in :attr:`numbers` with the first
    digit of the second integer. Then the absolute value of the difference of both integers is
    returned. If the list does not contain integers, the string "invalid array" is returned.

    :param numbers: The input list of integers
    :return: The absolute value of the difference of two integers after the first digits have been
        swapped
    """
    try:
        first, second = str(numbers[0]), str(numbers[1])
        return abs(int(second[0] + first[1:]) - int(first[0] + second[1:]))
    except ValueError:
        return "invalid array"
