def encrypt(text: str, rule: int):
    return "".join(chr((ord(char) + rule) % 256) for char in text)
