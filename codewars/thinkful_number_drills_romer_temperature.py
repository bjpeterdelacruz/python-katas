"""
Thinkful: Rømer Temperature

:Author: BJ Peter Dela Cruz
"""


def celsius_to_romer(temp: float) -> float:
    """
    This function converts the given temperature from Celsius to Rømer, which is no longer in use
    today.

    :param temp: The input temperature in Celsius
    :return: The temperature in the Rømer scale
    """
    return (temp * 21 / 40) + 7.5
