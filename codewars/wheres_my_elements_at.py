def element_location(begin: int, end: int, index: int, size: int) -> int:
    if index < 0 or (end - begin) / size <= index:
        raise IndexError
    return begin + (index * size)
