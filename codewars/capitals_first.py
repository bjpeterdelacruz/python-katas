"""
Capitals First!

:Author: BJ Peter Dela Cruz
"""
from string import ascii_uppercase as upper
from string import ascii_letters as letters


def capitals_first(text: str) -> str:
    """
    This function returns a string in which all the words that are capitalized in :attr:`text` are
    listed first, followed by words that are not capitalized. If a word starts with a non-alphabetic
    character, that word is ignored in the output string.

    :param text: The input string
    :return: A string in which all words that are capitalized are listed first, followed by those
        that are not capitalized
    """
    ascii_only = [word for word in text.split() if word[0] in letters]
    arr = [word for word in ascii_only if word[0] in upper]
    arr.extend([word for word in ascii_only if word[0] not in upper])
    return ' '.join(arr)
