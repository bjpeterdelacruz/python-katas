"""
Name on Billboard

:Author: BJ Peter Dela Cruz
"""


def billboard(name: str, price: int = 30):
    """
    This function returns the total price for all the characters in :attr:`name` given that each
    character costs the same amount without using the multiplication operator.

    :param name: The input string
    :param price: The amount in British pound sterling each characters costs (default is £30)
    :return: The total amount
    """
    return sum(price for _ in name)
