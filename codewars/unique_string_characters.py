"""
Unique String Characters

:Author: BJ Peter Dela Cruz
"""


def solve(string_a: str, string_b: str) -> str:
    """
    This function returns the characters that are not common in the given strings.

    :param string_a: The first input string
    :param string_b: The second input string
    :return: A string with the characters that are not common in both strings
    """
    chars_a = ''.join([character for character in string_a if character not in string_b])
    chars_b = ''.join([character for character in string_b if character not in string_a])
    return f"{chars_a}{chars_b}"
