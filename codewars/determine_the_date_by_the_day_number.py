"""
Determine the Date

:Author: BJ Peter Dela Cruz
"""
from datetime import date, timedelta


def get_day(day: int, is_leap: bool) -> str:
    """
    This function gets the month and date for the given day of the year.

    :param day: The day of the year (from 1 to 365 or 366)
    :param is_leap: True if the year is a leap year (366 days), False otherwise
    :return: A month and date
    """
    if day < 1:
        raise ValueError("day must be greater than 0")
    if is_leap and day > 366:
        raise ValueError("day must not be greater than 366 for leap years")
    if not is_leap and day > 365:
        raise ValueError("day must not be greater than 365 for non-leap years")
    new_date = date(1992 if is_leap else 2021, 1, 1) + timedelta(days=day - 1)
    return new_date.strftime('%B, %-d')
