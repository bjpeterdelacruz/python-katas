"""
Double Every Other

:Author: BJ Peter Dela Cruz
"""


def double_every_other(lst: list) -> list:
    """
    This function doubles all the elements in the given list that are at an odd index.

    :param lst: The input list
    :return: A list that contains the same elements as :attr:`lst` but with all the elements that
        are at an odd index doubled
    """
    return [element * 2 if idx % 2 == 1 else element for idx, element in enumerate(lst)]
