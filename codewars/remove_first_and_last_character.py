"""
Remove First and Last Character

:Author: BJ Peter Dela Cruz
"""


def remove_char(string: str) -> str:
    """
    This function removes the first and last characters from the given string. It is assumed that
    the string contains at least two characters.

    :param string: The input string
    :return: The same string but with the first and last characters removed from it
    """
    return string[1:-1]
