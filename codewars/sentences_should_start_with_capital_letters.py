"""
Sentences Should Start with Capital Letters

:Author: BJ Peter Dela Cruz
"""
from string import ascii_letters


def to_upper(word: str) -> str:
    """
    This helper function capitalizes the first alphabetic character in the given string. If the
    string does not contain any alphabetic characters, the string is simply returned.

    :param word: The input string
    :return: A string with the first alphabetic character capitalized
    """
    for idx, char in enumerate(word):
        if char in ascii_letters:
            return word[:idx] + char.upper() + word[idx + 1:]
    return word


def fix(paragraph: str) -> str:
    """
    This function capitalizes the first alphabetic character of each sentence in the given string.
    For example, given "hello. how are you? i am fine.", the string "Hello. How are you? I am fine."
    is returned.

    :param paragraph: The input string that contains sentences
    :return: A string that contains sentences, each sentence starts with a capitalized alphabetic
        character
    """
    words = list(paragraph.split('.'))[0:-1]
    return ''.join(f'{to_upper(word)}.' for word in words)
