"""
Seven Ate 9

:Author: BJ Peter Dela Cruz
"""


def seven_ate9(string: str) -> str:
    """
    This function replaces all 9's in the given string that are surrounded by a 7 immediately to
    their left and right.

    :param string: The input string
    :return: A string that does not contain the substring "797"
    """
    while "797" in string:
        string = string.replace("797", "77", 1)
    return string
