"""
Closest to Zero

:Author: BJ Peter Dela Cruz
"""


def closest(lst: list[int]) -> int | None:
    """
    This function returns the integer, positive or negative, that is closest to zero. If a positive
    integer and a negative integer are both the closest to zero, None is returned.

    :param lst: The input list of integers
    :return: The integer that is closest to zero, or None if two integers are both the closest to
        zero
    """
    minimum = abs(lst[0])
    numbers = set()
    for number in lst:
        if abs(number) < minimum:
            minimum = abs(number)
            numbers = {number}
        elif abs(number) == minimum:
            numbers.add(number)
    if len(numbers) > 1:
        return None
    return minimum if minimum in lst else -minimum
