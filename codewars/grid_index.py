"""
Grid Index

:Author: BJ Peter Dela Cruz
"""


def grid_index(grid: list[list[str]], indexes: list[int]) -> str:
    """
    This function gets the characters in the given two-dimensional grid at the given indexes and
    returns the characters as a concatenated string. The index in the grid starts at 1.

    :param grid: A two-dimensional grid of characters
    :param indexes: The list of indexes at which to retrieve the characters in :attr:`grid`
    :return: A concatenated string of characters found in :attr:`grid`
    """
    arr = [element for row in grid for element in row]
    return ''.join([arr[index - 1] for index in indexes])
