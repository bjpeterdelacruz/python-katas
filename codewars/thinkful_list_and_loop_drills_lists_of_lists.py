"""
Thinkful: Lists of Lists

:Author: BJ Peter Dela Cruz
"""
from functools import reduce


def process_data(data: list[list[int]]) -> int:
    """
    This function subtracts the second element in each sublist of the given list from the first
    element in the same sublist. After performing this operation on all sublists, all the
    differences are multiplied together.

    :param data: The input list of list of integers
    :return: The product of all the differences multiplied together
    """
    return reduce(lambda fst, snd: fst * snd, map(lambda lst: lst[0] - lst[1], data))
