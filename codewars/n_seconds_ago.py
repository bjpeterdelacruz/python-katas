"""
N Seconds Ago

:Author: BJ Peter Dela Cruz
"""
from datetime import datetime, timedelta


def seconds_ago(datetime_string: str, seconds: int) -> str:
    """
    This function returns a date and time given the number of seconds before the given date and
    time.

    :param datetime_string: A date amd time
    :param seconds: The number of seconds before :attr:`datetime_string`
    :return: A date and time that comes before :attr:`datetime_string`
    """
    new_date = datetime.strptime(datetime_string, '%Y-%m-%d %H:%M:%S') - timedelta(seconds=seconds)
    return f'{new_date.year:04d}-{new_date.strftime("%m-%d %H:%M:%S")}'
