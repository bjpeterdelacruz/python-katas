"""
Simple Fun #358: Vertical Histogram Of Letters

:Author: BJ Peter Dela Cruz
"""
from string import ascii_uppercase
from collections import defaultdict


def vertical_histogram_of(string: str) -> str:
    """
    This function returns a vertical histogram of all the uppercase letters in :attr:`string`.

    :param string: The input string
    :return: A vertical histogram showing the counts of all the letters in :attr:`string`
    """
    counts = defaultdict(int)
    maximum = -1
    for char in string:
        if char in ascii_uppercase:
            counts[char] += 1
            if counts[char] > maximum:
                maximum = counts[char]
    keys = sorted(counts.keys())
    lines = []
    count = maximum
    while count >= 0:
        line = []
        for key in keys:
            if count == 0:
                line.append(key)
            elif counts[key] >= count:
                line.append('*')
            else:
                line.append(' ')
            line.append(' ')
        lines.append(''.join(line).rstrip() + "\n")
        count -= 1
    return ''.join(lines).rstrip()
