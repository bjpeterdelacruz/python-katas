"""
Multiples

:Author: BJ Peter Dela Cruz
"""


def multiple(number: int) -> str:
    """
    This function returns "Boom" if :attr:`number` is divisible by 5, "Bang" if it is divisible by
    3, "BangBoom" if it is divisible by both 5 and 3, and "Miss" if it is not divisible by either.

    :param number: The input integer
    :return: "Boom", "Bang", "BangBoom", or "Miss"
    """
    if number % 3 == 0 and number % 5 == 0:
        return "BangBoom"
    if number % 5 == 0:
        return "Boom"
    return "Bang" if number % 3 == 0 else "Miss"
