"""
Shorten IPv6 Address

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def shorten(ip_v6: str) -> str:
    """
    This function shortens the given valid IPv6 address. All leading zeros in each quadruplet will
    be removed, and the longest sequence of quadruplets that contain only zeros will be replaced
    with a colon. If there are multiple sequences, only the first sequence will be replaced.

    :param ip_v6: The input string, a valid IPv6 address
    :return: A shortened version of the IPv6 address
    """
    quadruplets = ip_v6.split(":")
    result = []
    # First replace all leading zeros
    for quadruplet in quadruplets:
        while "0" in quadruplet and quadruplet.index("0") == 0 and len(quadruplet) != 1:
            quadruplet = quadruplet.replace("0", "", 1)
        result.append(quadruplet)
    # Get the longest sequence of quadruplets that are zeros
    zeros = [list(value) for key, value in groupby(result, key=lambda chars: chars) if key == "0"]
    if not zeros:
        return ":".join(result)
    zeros.sort(key=len, reverse=True)
    length = len(zeros[0])
    idx = 0
    # Replace the first quadruplet that is just zeros
    while True:
        if result[idx:idx + length] == zeros[0]:
            return ":".join(result[0:idx]) + "::" + ":".join(result[idx + length:])
        idx += 1
