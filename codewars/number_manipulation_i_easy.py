"""
Number Manipulation 1

:Author: BJ Peter Dela Cruz
"""


def manipulate(number: int) -> int:
    """
    This function replaces the second half of the given integer with zeros. If the number of digits
    in :attr:`number` is odd, then the middle digit and all the digits that come after it will be
    replaced with zeros.

    :param number: The input integer
    :return: The same integer but with the second half of its digits replaced with zeros
    """
    num_as_string = str(number)
    middle = num_zeros = len(num_as_string) // 2
    if len(num_as_string) % 2 == 1:
        num_zeros += 1
    return int(f"{num_as_string[:middle]}" + "0" * num_zeros)
