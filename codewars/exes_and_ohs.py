"""
Exes and Ohs

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict

# pylint: disable=invalid-name


def xo(string: str) -> bool:
    """
    This function returns True if the number of X's is equal to the number of O's. The count is
    case-insensitive, and all other characters in the string are ignored.

    :param string: The input string
    :return: True if the number of X's is equal to the number of O's, False otherwise
    """
    counts = defaultdict(int)
    for character in string:
        if character.lower() in ['x', 'o']:
            counts[character.lower()] += 1
    return counts['x'] == counts['o']
