"""
Simple Pig Latin

:Author: BJ Peter Dela Cruz
"""


def pig_it(text: str) -> str:
    """
    This function returns the Pig Latin version of :attr:`text`. In Pig Latin, the first letter of
    a word and "ay" are appended to the end of the word. For example, "Hello" -> "elloHay".
    Punctuation are left in place if they are at the end of the word. For example, "Hello!!!" ->
    "elloHay!!!".

    :param text: The input string to convert to Pig Latin
    :return: The Pig Latin version of :attr:`text`
    """
    substrings = []
    for string in text.split():
        symbols = []
        for character in string[::-1]:
            if not character.isalnum():
                symbols.insert(0, character)
            else:
                break
        substring = string
        if symbols:
            substring = ''.join(substring.rsplit(''.join(symbols), 1))
        if len(substring) == 1 and substring.isalnum():
            substrings.append(f"{substring}ay{''.join(symbols)}")
        elif len(substring) > 1:
            substrings.append(f"{substring[1:]}{substring[0]}ay{''.join(symbols)}")
        else:
            substrings.append(''.join(symbols))
    return ' '.join(substrings)
