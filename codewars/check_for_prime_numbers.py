"""
Check for Prime Numbers

:Author: BJ Peter Dela Cruz
"""
from math import ceil


def is_prime(number: int) -> bool:
    """
    This function returns True if the given number is prime, False otherwise.

    :param number: The input integer
    :return: True if the number is prime, False otherwise
    """
    if number in [2, 3, 5]:
        return True
    if number == 1 or number % 2 == 0 or number % 5 == 0:
        return False
    if sum(int(num) for num in str(number)) % 3 == 0:
        return False
    result = ceil(number ** 0.5)
    for num in range(3, result + 1, 2):
        if number % num == 0:
            return False
    return True
