"""
Thinkful: Repeater Level 2

:Author: BJ Peter Dela Cruz
"""


def repeater(string: str, count: int) -> str:
    """
    This function returns a string that contains the given string repeated :attr:`count` times.

    :param string: The input string to repeat
    :param count: The number of times to repeat the string
    :return: A string that contains the input string repeated :attr:`count` times
    """
    return f'"{string}" repeated {count} times is: "{string * count}"'
