"""
Remove Consecutive Duplicate Words

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def remove_consecutive_duplicates(string: str) -> str:
    """
    This function keeps the first instance of a word if it appears more than once consecutively. For
    example, given the string "hello hello hello world hi hi hi hi bye bye hi hi hello", the string
    "hello world hi bye hi hello" is returned.

    :param string: The input string
    :return: A string that does not contain consecutive, duplicate words
    """
    return ' '.join(key for key, _ in groupby(string.split()))
