"""
Strange Strings Parser

:Author: BJ Peter Dela Cruz
"""
from string import ascii_letters


def word_splitter(string: str) -> list[str]:
    """
    This function splits the given string into a list of strings using all the non-alphanumeric
    characters (except - and .) in the string as delimiters.

    :param string: The input string
    :return: A list of strings
    """
    regular_chars = f'{ascii_letters}0123456789-.'
    special_chars = ''.join({char for char in string if char not in regular_chars})
    start = 0
    arr = []
    for idx, value in enumerate(string):
        if value in special_chars:
            substring = string[start:idx]
            if substring:
                arr.append(substring)
            start = idx + 1
    if string[start:]:
        arr.append(string[start:])
    return arr
