"""
Third Angle of a Triangle

:Author: BJ Peter Dela Cruz
"""


def other_angle(angle_1, angle_2):
    """
    This function calculates the third angle of a triangle given two known angles.

    :param angle_1: The first angle
    :param angle_2: The second angle
    :return: The third angle of the triangle
    """
    return 180 - angle_1 - angle_2
