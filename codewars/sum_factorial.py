"""
Sum Factorial

:Author: BJ Peter Dela Cruz
"""


def sum_factorial(lst: list[int]) -> int:
    """
    This function calculates the factorial of each element in :attr:`lst` and then sums all the
    results together. For example: [3, 4, 2, 5] = 3! + 4! + 2! + 5! = 6 + 24 + 2 + 120 = 152

    :param lst: The input list of integers
    :return: The sum of all the factorials
    """
    products = []
    for maximum in lst:
        product = 1
        for number in range(2, maximum + 1):
            product *= number
        products.append(product)
    return sum(products)
