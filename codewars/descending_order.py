"""
Descending Order

:Author: BJ Peter Dela Cruz
"""


def descending_order(num: int) -> int:
    """
    This function sorts the digits in :attr:`num` in descending order, essentially rearranging the
    digits to create the highest number possible.

    :param num: The input integer
    :return: An integer with the same digits in :attr:`num` that are sorted in descending order
    """
    return int(''.join(reversed(sorted(str(num)))))
