"""
Thinking and Testing 28: How Many "Word"

:Author: BJ Peter Dela Cruz
"""


def how_many_word(string: str) -> int:
    """
    This function counts all the number of times that 'word' appears in the given string. The count
    is case-insensitive, and all the characters between 'w', 'o', 'r', and 'd' in the given string
    are ignored.

    :param string: The input string
    :return: The number of times that "word" appears in :attr:`string`
    """
    index = 0
    letters = []
    for letter in string:
        if letter.lower() in 'word'[index]:
            letters.append(letter.lower())
            index += 1
        if index == 4:
            index = 0
    letters = ''.join(letters)
    count = 0
    while 'word' in letters:
        count += 1
        letters = letters.replace('word', '', 1)
    return count
