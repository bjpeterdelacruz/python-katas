"""
Switcheroo

:Author: BJ Peter Dela Cruz
"""


def switcheroo(string: str) -> str:
    """
    Given a string that contains only the lowercase letters a, b, and c, this function switches all
    a's and b's with each other but leaves all c's in place.

    :param string: The input string
    :return: A string in which all a's and b's switched places with each other
    """
    return ''.join(map(lambda x: 'b' if x == 'a' else 'a' if x == 'b' else 'c', string))
