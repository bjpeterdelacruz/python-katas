"""
Binary Representation

:Author: BJ Peter Dela Cruz
"""
MAX_32_BIT = 0b11111111_11111111_11111111_11111111


def show_bits(number: int) -> list[int]:
    """
    This function converts a 32-bit number into a list of ones and zeros, each representing a bit in
    the number.

    :param number: The input number, a 32-bit signed or unsigned integer
    :return: A binary representation of :attr:`number` as a list of ones and zeros
    """
    return list(map(int, list(bin(number & MAX_32_BIT)[2:].rjust(32, '0'))))
