"""
Evaluate Mathematical Expression

:Author: BJ Peter Dela Cruz
"""


def calc(expression: str):
    """
    This function evaluates the given mathematical expression and returns the result as an integer
    or a float. The expression can consist of addition, subtraction, multiplication, division, and
    parentheses. Numbers may be whole numbers (e.g. 10) or decimal numbers (e.g. 7.25). Operators
    are always evaluated from left to right, and multiplication and division are evaluated before
    addition and subtraction. Multiple levels of parentheses are supported, e.g. ((2 + (((5))))).
    Whitespaces between numbers and operators are also supported. Finally, the minus sign (hyphen)
    used for negating numbers and parentheses will never be separated by whitespaces. For example,
    "6 + -(4)" and "6 + -( -4)" are valid expressions whereas "6 + - (4)" and "6 + -(- 4)" are
    invalid.

    :param expression: The input string, a valid mathematical expression
    :return: The result of the mathematical expression as an integer or a float
    """
    arr = []
    for elem in expression.split():
        num = []
        for character in elem:
            if character.isnumeric() or character == '.':
                num.append(character)
            else:
                if num:
                    arr.append(''.join(num))
                    num = []
                arr.append(character)
        if num:
            arr.append(''.join(num))
    start = idx = 0
    while idx < len(arr):
        if arr[idx] == '(':
            start = idx
        elif arr[idx] == ')':
            end = idx
            arr = arr[:start] + calculate(arr[start + 1:end]) + arr[end + 1:]
            idx = -1
        idx += 1
    result = str(calculate(arr)[0])
    return int(result[:result.index('.')]) if result.endswith(".0") else float(result)


def replace_hyphens(expr: list) -> list:
    """
    This helper function replaces all the minus signs (hyphens) that are by themselves. For example,
    ["-", "5"] and ["5", "-", "-", "4"] will become [-5] and ["5", "+", 4], respectively. Another
    example: ["-", "7", "+", "-", "3", "-", "-1"] will become [-7, "+", -3, "+", 1].

    :param expr: A mathematical expression
    :return: The same expression but with all separate hyphens replaced
    """
    idx = 1
    while idx < len(expr):
        value = expr[idx]
        if expr[idx - 1] == value == '-':
            expr.pop(idx)
            expr[idx - 1] = '+'
            idx -= 1
        else:
            is_number = isinstance(value, (float, int)) or value.isnumeric()
            if expr[idx - 1] == '-' and is_number:
                num = float(value) if isinstance(value, str) else value
                expr[idx] = num * -1
                if idx - 1 == 0:
                    expr.pop(idx - 1)
                else:
                    try:
                        float(expr[idx - 2])
                    except ValueError:
                        expr.pop(idx - 1)
                    else:
                        expr[idx - 1] = '+'
        idx += 1
    return expr


def perform_op(first_number, second_number, operator: str):
    """
    This helper function performs a mathematical operation and returns the result.

    :param first_number: The first number
    :param second_number: The second number
    :param operator: A character representing multiplication ('*'), division ('/'),
        subtraction ('-'), or addition ('+')
    :return: The result of the mathematical operation
    """
    if operator == '*':
        return first_number * second_number
    if operator == '/':
        return first_number / second_number
    if operator == '-':
        return first_number - second_number
    return first_number + second_number


def calculate(expression: list) -> list:
    """
    This helper function evaluates the given mathematical expression and returns the result. The
    expression can contain zero, one, or more mathematical operators. Operator precedence is
    followed.

    :param expression: The mathematical expression to evaluate
    :return: The result of the mathematical expression
    """
    expression = replace_hyphens(expression)
    while len(expression) > 1:
        if '*' in expression and '/' not in expression:
            idx = expression.index('*')
        elif '/' in expression and '*' not in expression:
            idx = expression.index('/')
        elif '*' in expression and '/' in expression:
            div_idx = expression.index('/')
            mul_idx = expression.index('*')
            idx = mul_idx if mul_idx < div_idx else div_idx
        elif '+' in expression and '-' not in expression:
            idx = expression.index('+')
        else:
            add_idx = expression.index('+')
            sub_idx = expression.index('-')
            idx = add_idx if add_idx < sub_idx else sub_idx
        first_number = float(expression[idx - 1])
        second_number = float(expression[idx + 1])
        result = perform_op(first_number, second_number, expression[idx])
        expression[idx - 1] = result
        expression.pop(idx)
        expression.pop(idx)
    return expression
