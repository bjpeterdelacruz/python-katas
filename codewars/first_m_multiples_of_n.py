"""
First M Multiples of N

:Author: BJ Peter Dela Cruz
"""


def multiples(num_multiples: int, number: int) -> list[int]:
    """
    This function generates a list containing multiples of :attr:`number`.

    :param num_multiples: The number of multiples of :attr:`number`
    :param number: The number for which to generate a list of multiples
    :return: A list containing multiples of :attr:`number`
    """
    return [number * current for current in range(1, num_multiples + 1)]
