def pair_zeros(arr: list[int]) -> list[int]:
    new_arr = []
    unique = set()
    for number in arr:
        if number == 0:
            if number in unique:
                unique.remove(number)
            else:
                new_arr.append(number)
                unique.add(number)
        else:
            new_arr.append(number)
    return new_arr
