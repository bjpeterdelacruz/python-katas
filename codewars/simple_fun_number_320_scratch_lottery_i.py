"""
Scratch Lottery 1

:Author: BJ Peter Dela Cruz
"""


def scratch(lottery: list[str]) -> int:
    """
    This function sums the prize amount for each lottery ticket that contains the same animal name
    three times.

    :param lottery: The input list of strings, each representing a lottery ticket that contains
        animal names
    :return: The total prize amount
    """
    total = 0
    for ticket in lottery:
        arr = ticket.split()
        animals, prize = set(arr[0:3]), int(arr[3])
        if len(animals) == 1:
            total += prize
    return total
