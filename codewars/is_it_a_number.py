"""
Is It a Number?

:Author: BJ Peter Dela Cruz
"""


def is_digit(string: str) -> bool:
    """
    This function returns True if the given string represents a valid integer or float, or False
    otherwise.

    :param string: The input string
    :return: True if :attr:`string` represents a valid integer or float, False otherwise
    """
    string = string.strip()
    try:
        int(string)
    except ValueError:
        try:
            float(string)
        except ValueError:
            return False
    return True
