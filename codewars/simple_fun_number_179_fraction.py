"""
Simple Fun #179: Fraction

:Author: BJ Peter Dela Cruz
"""
from fractions import Fraction


def fraction(numerator: int, denominator: int) -> int:
    """
    This function returns the sum of the numerator and the denominator of the given fraction after
    it has been reduced. For example, given 5 and 10, the sum 3 will be returned because 5/10 can be
    reduced to 1/2.

    :param numerator: The input integer representing the numerator of a fraction
    :param denominator: The input integer representing the denominator of a fraction
    :return: The sum of the numerator and the denominator after the fraction has been reduced
    """
    frac = Fraction(numerator / denominator).limit_denominator()
    return frac.numerator + frac.denominator
