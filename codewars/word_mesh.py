"""
Word Mesh

:Author: BJ Peter Dela Cruz
"""


def word_mesh(words: str) -> str:
    """
    This function returns a string in which the characters represent substrings that are at the end
    of one word in the given list of words and begin with the next word in the list. For example,
    given "cat attack kitchen engrave", the function will return "atken" ("at", "k", and "en"). If
    there are no substrings at the end of one word that appear at the beginning of the next word,
    "failed to mesh" is returned.

    :param words: A space-delimited list of words
    :return: A string that contains substrings, or "failed to mesh"
    """
    strings = []
    for idx in range(1, len(words)):
        first = words[idx - 1]
        second = words[idx]
        substring_idx = 0
        while True:
            substring = first[substring_idx:]
            if not substring:
                return "failed to mesh"
            if second.startswith(substring):
                strings.append(substring)
                break
            substring_idx += 1
    return ''.join(strings)
