"""
Sorting Dictionaries

:Author: BJ Peter Dela Cruz
"""


def sort_dict(dictionary: dict[int, int]) -> list:
    """
    This function generates a sorted list of (key, value) tuples from the given dictionary. The list
    is sorted by value from largest to smallest.

    :param dictionary: A dictionary
    :return: A sorted list of (key, value) tuples
    """
    return sorted(dictionary.items(), key=lambda x: -x[1])
