"""
Calendar Week

:Author: BJ Peter Dela Cruz
"""
import datetime


def get_calendar_week(date_string):
    """
    This function returns the ISO 8601 week number for the given date. ISO 8601 defines the first
    week of the year as the week containing the first Thursday.

    :param date_string: The input string representing a date in the format YYYY-MM-DD
    :return: The ISO 8601 week number
    """
    date = date_string.split("-")
    return datetime.datetime(int(date[0]), int(date[1]), int(date[2])).isocalendar()[1]
