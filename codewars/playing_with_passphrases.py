"""
Playing with Passphrases

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase, ascii_uppercase


def play_pass(string: str, num: int):
    """
    This function generates a secure passphrase by performing the following in order: shift right
    each alphabetic character by the given number, replace each digit by its complement to nine,
    keep all non-alphanumeric characters, lowercase all alphabetic characters that are in an odd
    position in the string, uppercase all alphabetic characters that are in an even position, and
    finally reverse the string.

    :param string: The input string from which to generate a passphrase
    :param num: The number to shift right each alphabetic character
    :return: A passphrase
    """
    arr = []
    idx = 0
    for char in string:
        if char.upper() in ascii_uppercase:
            ascii_idx = (ascii_uppercase.index(char.upper()) + num) % len(ascii_uppercase)
            if idx % 2 == 1:
                arr.append(ascii_lowercase[ascii_idx])
            else:
                arr.append(ascii_uppercase[ascii_idx])
        elif char.isnumeric():
            arr.append(str(9 - int(char)))
        else:
            arr.append(char)
        idx += 1
    return ''.join(reversed(''.join(arr)))
