"""
Average Array

:Author: BJ Peter Dela Cruz
"""


def avg_array(lists: list[list]) -> list:
    """
    This function calculates the average of all the elements in each list that are at the same
    index. For example, given [[1, 2, 3], [4, 2], [1]], the function will return [2.0, 2.0, 3.0]
    because 1 + 4 + 1 = 6 / 3 = 2.0, 2 + 2 = 4 / 2 = 2.0, and 3 / 1 = 3.0.

    :param lists: The input list that contains lists of numbers
    :return: A list containing averages
    """
    averages = []
    maximum_length = max(len(arr) for arr in lists)
    for idx in range(0, maximum_length):
        total = sum(lists[row][idx] for row in range(0, len(lists)) if idx < len(lists[row]))
        count = sum(1 for row in range(0, len(lists)) if idx < len(lists[row]))
        averages.append(total / count)
    return averages
