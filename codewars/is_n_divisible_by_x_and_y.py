"""
Is N Divisible by X and Y?

:Author: BJ Peter Dela Cruz
"""


def is_divisible(number: int, first_number: int, second_number: int) -> bool:
    """
    This function returns True if :attr:`number` is divisible by both :attr:`first_number` and
    :attr:`second_number`.

    :param number: The number for which to check divisibility
    :param first_number: The first number
    :param second_number: The second number
    :return: True if :attr:`number` is divisible by both numbers, False otherwise
    """
    return number % first_number == 0 and number % second_number == 0
