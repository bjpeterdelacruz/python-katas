"""
Even Binary Sorting

:Author: BJ Peter Dela Cruz
"""


def even_binary(string: str) -> str:
    """
    This function sorts the even binary numbers in ascending order but does not change the order of
    the odd binary strings and leaves them in place. For example, given "101 111 100 001 010", this
    function will return "101 111 010 001 100".

    :param string: The input string of binary numbers
    :return: The same string but with the even binary numbers sorted in ascending order and the odd
        binary numbers remaining in place
    """
    strings = string.split()
    numbers = []
    even = []
    for binary_string in strings:
        number = int(binary_string, 2)
        if number % 2 == 0:
            even.append(number)
            numbers.append("None")
        else:
            numbers.append(binary_string)
    even.sort()
    numbers = ' '.join(numbers)
    for number in even:
        numbers = numbers.replace("None", f"{number:03b}", 1)
    return numbers
