"""
Lazy Repeater

:Author: BJ Peter Dela Cruz
"""


def make_looper(string: str):
    """
    This function returns another function that will output a letter in :attr:`string`
    when called, wrapping around to the beginning if the end of the string is reached.

    :param string: The input string
    :return: A function
    """
    idx = 0

    def loop():
        nonlocal idx
        if idx % len(string) == 0:
            idx = 0
        character = string[idx]
        idx += 1
        return character

    return loop
