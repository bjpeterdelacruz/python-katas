"""
Password Hashes

:Author: BJ Peter Dela Cruz
"""
from hashlib import md5


def pass_hash(string: str) -> str:
    """
    This function calculates the MD5 hash of the given string.

    :param string: The input string
    :return: An MD5 hash
    """
    return md5(string.encode()).hexdigest()
