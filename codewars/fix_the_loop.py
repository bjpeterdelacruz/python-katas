"""
Fix the Loop

:Author: BJ Peter Dela Cruz
"""


def list_animals(animals: list[str]) -> str:
    """
    This function builds a string that represents a numbered list using the given list of strings.
    The list starts with the number one, and each list item is separated by a newline character.

    :param animals: The input list of strings
    :return: A numbered list that starts with the number one
    """
    return ''.join(f'{idx + 1}. {animal}\n' for idx, animal in enumerate(animals))
