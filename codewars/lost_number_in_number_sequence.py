"""
Lost Number

:Author: BJ Peter Dela Cruz
"""


def find_deleted_number(arr: list[int], mixed_arr: list[int]) -> int:
    """
    This function finds the number that is present in :attr:`arr` but missing in :attr:`mixed_arr`.
    The first list is sorted and contains integers from 1 to N.

    :param arr: The input list of integers
    :param mixed_arr: The input list of integers
    :return: The integer that is present in :attr:`arr` but missing in :attr:`mixed_arr` or 0 if no
        integers are missing in :attr:`mixed_arr`
    """
    mixed_set = set(mixed_arr)
    for num in arr:
        if num not in mixed_set:
            return num
    return 0
