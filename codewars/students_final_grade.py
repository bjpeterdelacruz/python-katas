"""
Student's Final Grade

:Author: BJ Peter Dela Cruz
"""


def final_grade(exam: int, projects: int) -> int:
    """
    This function calculates a student's final grade based on his or her exam score and the number
    of projects that he or she completed.

    :param exam: The input integer, the student's exam score
    :param projects: The input integer, the number of projects that the student completed
    :return: The student's final grade based on the exam score and number of projects completed
    """
    if exam > 90 or projects > 10:
        return 100
    if exam > 75 and projects >= 5:
        return 90
    return 75 if exam > 50 and projects >= 2 else 0
