"""
Shaving the Beard

:Author: BJ Peter Dela Cruz
"""


def trim(beard: list[list[str]]) -> list[list[str]]:
    """
    This function shaves off the hair in the given beard. That is, each curly hair, represented as
    the letter J, is shaved off, i.e. J is turned into the pipe character. The chin, which is the
    last list in the list of lists, is shaved completely, i.e. the J and pipe characters are turned
    into three dot characters.

    :param beard: The input list of list of strings, which represents a beard
    :return: A shaved beard
    """
    shaved = []
    for idx in range(0, len(beard) - 1):
        shaved.append(["|" if hair == "J" else hair for hair in beard[idx]])
    shaved.append(["..." if hair in ["J", "|"] else hair for hair in beard[-1]])
    return shaved
