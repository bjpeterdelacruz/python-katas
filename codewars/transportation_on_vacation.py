"""
Transportation on Vacation

:Author: BJ Peter Dela Cruz
"""


def rental_car_cost(number_of_days: int) -> int:
    """
    This function calculates the amount to pay for a rental car for the given number of days. Each
    day costs $40. If the car is rented for seven or more days, $50 is deducted from the total cost.
    If the car is rented for three or more days but less than seven days, $20 is deducted from the
    total cost.

    :param number_of_days: The number of days to rent a car
    :return: The total cost of the rental car
    """
    cost = number_of_days * 40
    return cost - 50 if number_of_days >= 7 else cost - 20 if number_of_days >= 3 else cost
