"""
Pythagorean Triple

:Author: BJ Peter Dela Cruz
"""


def pythagorean_triple(integers: list[int]) -> bool:
    """
    This function returns True if the three integers in the given list form a Pythagorean triple,
    that is, (A * A) + (B * B) = C * C. The input list may or may not be sorted.

    :param integers: The input list that contains three integers
    :return: True if the three integers form a Pythagorean triple, False otherwise
    """
    ints = sorted(integers)
    return ints[0] ** 2 + ints[1] ** 2 == ints[2] ** 2
