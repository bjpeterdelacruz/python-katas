"""
Is Integer Array?

:Author: BJ Peter Dela Cruz
"""


def is_int_array(arr) -> bool:
    """
    This function determines whether :attr:`arr` contains a list of only integers or floats without
    a decimal place, e.g. [1, 2, 3.0] is a valid list, but [1, 2, 3.0001] is not.

    :param arr: The input list
    :return: True if the list is empty or contains only integers or floats without a decimal place,
        False otherwise
    """
    if not isinstance(arr, list):
        return False
    if not arr:  # empty list
        return True
    for item in arr:
        if not isinstance(item, (int, float)):
            return False
        if isinstance(item, float) and not str(item).endswith(".0"):
            return False
    return True
