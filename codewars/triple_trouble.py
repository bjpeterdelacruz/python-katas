"""
Triple Trouble

:Author: BJ Peter Dela Cruz
"""


def triple_trouble(one: str, two: str, three: str) -> str:
    """
    This function groups together characters from each of the three input strings based on their
    index and returns a concatenated string containing all the groups. It is assumed that all the
    strings have the same length. For example, given "ad", "be", and "cf", the string "abcdef" is
    returned.

    :param one: The first input string
    :param two: The second input string
    :param three: The third input string
    :return: A string containing groups of characters
    """
    return ''.join(f'{one[idx] + two[idx] + three[idx]}' for idx in range(0, len(one)))
