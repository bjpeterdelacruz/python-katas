"""
Count Consonants

:Author: BJ Peter Dela Cruz
"""


def consonant_count(string: str) -> int:
    """
    This function counts the number of consonants in the given string.

    :param string: The input string
    :return: The number of consonants in the string
    """
    return len([char for char in string if char.isalpha() and char.lower() not in "aeiou"])
