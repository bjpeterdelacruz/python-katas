"""
Keep Up the Hoop

:Author: BJ Peter Dela Cruz
"""


def hoop_count(number: int) -> str:
    """
    This function returns the string "Great, now move on to tricks" if the number of times that the
    hula hoop goes around Alex is greater than or equal to 10. Otherwise, the string
    "Keep at it until you get it" is returned.

    :param number: An integer, the number of times that the hula hoop goes around Alex
    :return: An encouraging message
    """
    return "Great, now move on to tricks" if number >= 10 else "Keep at it until you get it"
