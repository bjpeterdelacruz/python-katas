"""
Arithmetic Progression

:Author: BJ Peter Dela Cruz
"""


def arithmetic_sequence_elements(first_number: int, constant: int, max_length: int) -> str:
    """
    This function produces an arithmetic sequence of integers, i.e. the difference between one
    element in a list and the next element is constant. The list that is returned will be of size
    :attr:`max_length`. A ValueError is raised if :attr:`max_length` is less than zero.

    :param first_number: The first integer in the arithmetic sequence
    :param constant: The difference between one element in the sequence and the next
    :param max_length: The maximum number of integers in the arithmetic sequence
    :return: An arithmetic sequence of integers, may be an empty string if :attr:`max_length` is 0
    """
    if max_length < 0:
        raise ValueError
    if max_length == 0:
        return ''
    new_list = [first_number]
    while len(new_list) < max_length:
        new_list.append(new_list[-1] + constant)
    return ', '.join(map(str, new_list))
