"""
Calculating with Functions

:Author: BJ Peter Dela Cruz
"""

# pylint: disable=eval-used


def plus(func):
    """
    Returns a function that will add two numbers together.

    :param func: The number that will be on the right-hand side of the plus sign
    :return: A function that will be used to add two numbers together
    """
    def add():
        return f" + {func}"
    return add


def minus(func):
    """
    Returns a function that will subtract one number from another.

    :param func: The number that will be on the right-hand side of the minus sign
    :return: A function that will be used to subtract one number from another
    """
    def sub():
        return f" - {func}"
    return sub


def times(func):
    """
    Returns a function that will multiply two numbers together.

    :param func: The number that will be on the right-hand side of the multiply sign
    :return: A function that will be used to multiply two numbers together
    """
    def mul():
        return f" * {func}"
    return mul


def divided_by(func):
    """
    Returns a function that will divide two numbers.

    :param func: The number that will be on the right-hand side of the division sign
    :return: A function that will be used to divide two numbers
    """
    def divide():
        return f" // {func}"
    return divide


def zero(func=None):
    """
    Represents the number 0.

    :param func: If not None, represents a mathematical function of which this number is on the
        left-hand side
    :return: The number 0 or the result of the mathematical function
    """
    return eval(f"0 {func()}") if func else 0


def one(func=None):
    """
    Represents the number 1.

    :param func: If not None, represents a mathematical function of which this number is on the
        left-hand side
    :return: The number 1 or the result of the mathematical function
    """
    return eval(f"1 {func()}") if func else 1


def two(func=None):
    """
    Represents the number 2.

    :param func: If not None, represents a mathematical function of which this number is on the
        left-hand side
    :return: The number 2 or the result of the mathematical function
    """
    return eval(f"2 {func()}") if func else 2


def three(func=None):
    """
    Represents the number 3.

    :param func: If not None, represents a mathematical function of which this number is on the
        left-hand side
    :return: The number 3 or the result of the mathematical function
    """
    return eval(f"3 {func()}") if func else 3


def four(func=None):
    """
    Represents the number 4.

    :param func: If not None, represents a mathematical function of which this number is on the
        left-hand side
    :return: The number 4 or the result of the mathematical function
    """
    return eval(f"4 {func()}") if func else 4


def five(func=None):
    """
    Represents the number 5.

    :param func: If not None, represents a mathematical function of which this number is on the
        left-hand side
    :return: The number 5 or the result of the mathematical function
    """
    return eval(f"5 {func()}") if func else 5


def six(func=None):
    """
    Represents the number 6.

    :param func: If not None, represents a mathematical function of which this number is on the
        left-hand side
    :return: The number 6 or the result of the mathematical function
    """
    return eval(f"6 {func()}") if func else 6


def seven(func=None):
    """
    Represents the number 7.

    :param func: If not None, represents a mathematical function of which this number is on the
        left-hand side
    :return: The number 7 or the result of the mathematical function
    """
    return eval(f"7 {func()}") if func else 7


def eight(func=None):
    """
    Represents the number 8.

    :param func: If not None, represents a mathematical function of which this number is on the
        left-hand side
    :return: The number 8 or the result of the mathematical function
    """
    return eval(f"8 {func()}") if func else 8


def nine(func=None):
    """
    Represents the number 9.

    :param func: If not None, represents a mathematical function of which this number is on the
        left-hand side
    :return: The number 9 or the result of the mathematical function
    """
    return eval(f"9 {func()}") if func else 9
