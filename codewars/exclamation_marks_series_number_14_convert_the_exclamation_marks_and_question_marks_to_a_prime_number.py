"""
Exclamation Marks Series #14: Convert the Exclamation Marks and Question Marks to a Prime Number

:Author: BJ Peter Dela Cruz
"""
from math import sqrt, ceil
from itertools import groupby


def is_prime(number: int) -> bool:
    """
    This helper function returns True if the given integer is a prime number, False otherwise.

    :param number: The input integer
    :return: True if the integer is prime, False otherwise
    """
    if number % 2 == 0 and number > 2:
        return False
    for num in range(3, ceil(sqrt(number)), 2):
        if number % num == 0:
            return False
    return True


def convert(string: str) -> int:
    """
    This function converts groups of continuous exclamation marks and question marks to a number. If
    the number is prime, it is returned. Otherwise, the number is divided by the smallest factor.
    This process is repeated until the number is prime. For example, given "!????!!!?", the function
    converts the string to 1431, which is not prime. 1431 // 3 = 477 -> 477 // 3 = 159 ->
    159 // 3 = 53, which is prime, so the function returns 53.

    :param string: The input string
    :return: A prime number
    """
    number = int(''.join(str(len(list(group))) for _, group in groupby(string)))
    while not is_prime(number):
        for num in range(number - 1, 1, -1):
            if number % num == 0:
                number = num
                break
    return number
