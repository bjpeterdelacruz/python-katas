"""
Indexed Capitalization

:Author: BJ Peter Dela Cruz
"""


def capitalize(string: str, indexes: list[int]) -> str:
    """
    This function capitalizes the characters in the given string if the index of the character is in
    the given list.

    :param string: The input string
    :param indexes: The input list of integers representing indexes in the string
    :return: A string that may or may not have capitalized letters
    """
    return ''.join([char if idx not in indexes else char.upper()
                    for idx, char in enumerate(string)])
