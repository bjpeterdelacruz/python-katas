"""
Beeramid

:Author: BJ Peter Dela Cruz
"""


def beeramid(bonus, price) -> int:
    """
    This function returns the height of a pyramid that is made of beers (a.k.a. "beer"-amid).

    :param bonus: The input integer, the referral bonus
    :param price: The input integer, the price of a can of beer
    :return: The height of the "beer"-amid
    """
    total_beers = bonus / price
    beers_per_level = [1]
    idx = 1
    while sum(beers_per_level) < total_beers:
        beers_per_level.append(idx * 2 + 1 + beers_per_level[-1])
        idx += 1
    return len(beers_per_level) if sum(beers_per_level) == total_beers else len(beers_per_level) - 1
