"""
Sequence Determiner

:Author: BJ Peter Dela Cruz
"""
from math import inf


def determine_sequence(series_array: list[int]) -> int:
    """
    This function determines whether the given list of integers represents an arithmetic sequence or
    a geometric sequence. If it is an arithmetic sequence, 0 is returned. If it is a geometric
    sequence, 1 is returned. If it is both, 2 is returned, and if it is neither, -1 is returned.

    :param series_array: The input list of integers
    :return: -1, 0, 1, or 2 as described above
    """
    if set(series_array) == {0}:
        return 0
    arithmetic = set()
    geometric = set()
    for idx in range(1, len(series_array)):
        current, previous = series_array[idx], series_array[idx - 1]
        arithmetic.add(abs(current - previous))
        geometric.add(inf if previous == 0 else current / previous)
        if len(arithmetic) > 1 and len(geometric) > 1:
            return -1
    if len(arithmetic) == 1 and len(geometric) == 1:
        return 2
    return 0 if len(arithmetic) == 1 else 1
