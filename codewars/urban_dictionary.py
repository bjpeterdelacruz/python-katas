"""
Urban Dictionary

:Author: BJ Peter Dela Cruz
"""


class WordDictionary:
    """
    This class represents a dictionary to which one can add words. The dictionary can be searched
    using exact match or a very simple regular expression.
    """
    def __init__(self):
        """
        Initializes an empty dictionary that will contain words.
        """
        self.word_dict = []

    def add_word(self, word: str) -> None:
        """
        Adds the given word to the dictionary.

        :param word: The word to add to the dictionary.
        :return: None
        """
        self.word_dict.append(word)

    def search(self, word: str) -> bool:
        """
        Searches the dictionary for the given word. If the regular expression does not contain a
        period, an exact match is performed. Periods mean "match any character". For example, given
        ".ad" and the dictionary contains [dad, mad, sad, eat, sun, bunny], True will be returned
        because "dad", "mad", and "sad" match that pattern.

        :param word: The word to search for
        :return: True if the word is found or there are words that match the regular expression,
            False otherwise
        """
        if "." not in word:
            return word in self.word_dict
        for current_word in self.word_dict:
            if len(current_word) == len(word):
                found = True
                for idx, char in enumerate(current_word):
                    if word[idx] != "." and char != word[idx]:
                        found = False
                        break
                if found:
                    return True
        return False
