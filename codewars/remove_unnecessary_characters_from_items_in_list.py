"""
Remove Unnecessary Characters from Items in List

:Author: BJ Peter Dela Cruz
"""


def remove_char(array: list[str]) -> list[str]:
    """
    This function removes all non-numeric characters in each string in :attr:`array`. Then each
    number in the list is formatted as US currency. For example, "Ax?7.|.5yZ" becomes "$0.75".

    :param array: The input list of strings
    :return: A list of strings, each string formatted as US currency
    """
    new_array = []
    for item in array:
        temp = ''.join([char for chars in item for char in chars if char in '0123456789'])
        while len(temp) < 3:
            temp = f'0{temp}'
        new_array.append(f'${temp[:-2]}.{temp[-2:]}')
    return new_array
