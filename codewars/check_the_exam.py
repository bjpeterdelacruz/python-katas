"""
Check the Exam

:Author: BJ Peter Dela Cruz
"""


def check_exam(arr1: list[str], arr2: list[str]) -> int:
    """
    This function checks whether a student's submitted answers are correct. If a student's answer is
    correct, four points are awarded. But if it is incorrect, one point is subtracted from the total
    score. If the student did not provide an answer, no points are awarded or subtracted. If the
    total score is less than zero, zero is returned.

    :param arr1: The input list of correct answers
    :param arr2: The input list of a student's answers
    :return: The total score or zero if the total score is less than zero
    """
    score = 0
    for idx, value in enumerate(arr1):
        if value == arr2[idx]:
            score += 4
        elif arr2[idx] != "":
            score -= 1
    return score if score >= 0 else 0
