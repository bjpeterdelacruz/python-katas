"""
Human Readable Time

:Author: BJ Peter Dela Cruz
"""


def make_readable(seconds: int) -> str:
    """
    This function converts the given seconds into a human-readable format, namely "##:##:##", which
    represents hours, minutes, and seconds.

    :param seconds: The input integer representing the number of seconds
    :return: A human-readable string in the ##:##:## format
    """
    hours = seconds // (60 ** 2)
    minutes = seconds // 60 % 60
    seconds = seconds % 60
    return f"{hours:02}:{minutes:02}:{seconds:02}"
