"""
Dbftbs Djqifs

:Author: BJ Peter Dela Cruz
"""
from string import ascii_letters, ascii_lowercase, ascii_uppercase


def encryptor(key: int, message: str) -> str:
    """
    This function encrypts a string using the famous Caesar Cipher. All non-alphabetic characters
    are left as-is.

    :param key: The input integer, the number of characters to shift
    :param message: The input string, the message to encrypt
    :return: An encrypted message
    """
    encrypted_string = []
    length = len(ascii_lowercase)
    for char in message:
        if char not in ascii_letters:
            encrypted_string.append(char)
        elif char in ascii_lowercase:
            encrypted_string.append(ascii_lowercase[(ascii_lowercase.index(char) + key) % length])
        else:
            encrypted_string.append(ascii_uppercase[(ascii_uppercase.index(char) + key) % length])
    return ''.join(encrypted_string)
