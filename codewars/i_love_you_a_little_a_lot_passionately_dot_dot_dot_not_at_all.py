"""
I Love You

:Author: BJ Peter Dela Cruz
"""


def how_much_i_love_you(nb_petals: int) -> str:
    """
    This function returns what a girl would say after she tears off the last petal from a flower
    that has the given number of petals. For example, if a flower has eight petals, after tearing
    off the last petal, she would say, "A little."

    :param nb_petals: The number of petals on a flower
    :return: A phrase that a girl would say after tearing off the last petal
    """
    petals = ["I love you", "a little", "a lot", "passionately", "madly", "not at all"]
    return petals[(nb_petals - 1) % len(petals)]
