"""
Upturn Numeral Triangle

:Author: BJ Peter Dela Cruz
"""


def pattern(number: int) -> str:
    """
    This function returns a string that represents an upside down equilateral triangle. If
    :attr:`number` is greater than 9, only the rightmost digit is outputted in the string.

    :param number: The length of each side of the triangle
    :return: A string that contains numbers from 0 to 9, and represents an upside down equilateral
        triangle
    """
    arr = []
    for num in range(0, number):
        string = ' ' * (num + 1)
        string += f'{str(num + 1)[-1]} ' * (number - num)
        arr.append(f'{string.rstrip()}\n')
    return ''.join(arr).rstrip()
