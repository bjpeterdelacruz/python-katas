"""
Thinkful: Grade Calculator

:Author: BJ Peter Dela Cruz
"""
from math import floor
GRADES = {10: 'A', 9: 'A', 8: 'B', 7: 'C', 6: 'D'}


def calculate_grade(scores: list) -> str:
    """
    This function calculates the average grade for the given list of grades. If the grade is greater
    than or equal to 90% and less than or equal to 100%, 'A' is returned. If the grade is less than
    90% and greater than or equal to 80%, 'B' is returned. If the grade is less than 80% and greater
    than or equal to 70%, 'C' is returned. If the grade is less than 70% and greater than or equal
    to 60%, 'D' is returned. Finally, if the grade is below 60%, 'F' is returned.

    :param scores: The input list of grades
    :return: A letter grade based on the aforementioned formula
    """
    average = floor(sum(scores) / len(scores) / 10)
    return GRADES[average] if average in GRADES else 'F'
