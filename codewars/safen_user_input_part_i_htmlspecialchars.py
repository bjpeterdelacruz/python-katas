"""
Safen User Input: Part 1

:Author: BJ Peter Dela Cruz
"""


def html_special_chars(data: str) -> str:
    """
    This function replaces all ampersand, less than, greater than, and double quote characters with
    "&amp;", "&lt;", "&gt;", and "&quot;" strings, respectively.

    :param data: The input string that may contain the four aforementioned characters
    :return: A string that contains the aforementioned characters encoded as strings that begin with
        an ampersand
    """
    return data.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")\
        .replace('"', "&quot;")
