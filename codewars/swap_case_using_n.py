"""
Swap Case Using N

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase


def swap(string: str, number: int) -> str:
    """
    This function swaps the case of the letters in :attr:`string` if the bit for a letter is 1.
    :attr:`number` is converted to a bit string and repeated until its length is equal to or greater
    than the length of :attr:`string`. Numbers and symbols are ignored, and only ASCII characters
    are checked.

    :param string: The input string containing characters whose cases are either swapped or not
    :param number: The input integer, 1 to swap the case of a letter, 0 to leave as is
    :return: The same string but with the cases of all, some, or no letters swapped
    """
    binary = f"{number:0b}"
    while len(binary) < len(string):
        binary += binary
    result = []
    binary_idx = 0
    for char in string:
        if char.isalpha():
            if binary[binary_idx] == "1":
                result.append(char.upper() if char in ascii_lowercase else char.lower())
            else:
                result.append(char)
            binary_idx += 1
        else:
            result.append(char)
    return ''.join(result)
