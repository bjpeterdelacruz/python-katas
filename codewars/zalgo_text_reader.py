"""
Zalgo Text Reader

:Author: BJ Peter Dela Cruz
"""


def read_zalgo(zalgo_text: str) -> str:
    """
    This function extracts the ASCII characters from the given Zalgo text.

    :param zalgo_text: The input string representing a Zalgo text
    :return: A string consisting only of ASCII characters
    """
    return ''.join([char for char in zalgo_text if 32 <= ord(char) <= 126])
