"""
Boolean Logic from Scratch

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=simplifiable-if-expression


def func_or(first, second):
    """
    This function returns True if either :attr:`first` or :attr:`second` is truthy without using the
    or operator.

    :param first: The first input
    :param second: The second input
    :return: True if either element is truthy, false otherwise
    """
    if first:
        return True
    return True if second else False


def func_xor(first, second):
    """
    This function returns True if one and only one of the two arguments are truthy. If both
    arguments are truthy or falsy, False is returned.

    :param first: The first input
    :param second: The second input
    :return: True if one and only one of the two arguments are truthy, False if both are truthy or
        falsy
    """
    if first and second:
        return False
    return False if not first and not second else True
