"""
Map Over List of Lists

:Author: BJ Peter Dela Cruz
"""


def grid_map(lists: list[list], operation):
    """
    This function performs the given operation on each element in each list of the given list.

    :param lists: The input list of lists
    :param operation: The operation to perform on each element in each list
    :return: A list of lists
    """
    return [list(map(operation, current_list)) for current_list in lists]
