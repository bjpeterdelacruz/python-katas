"""
Multiply and Filter

:Author: BJ Peter Dela Cruz
"""


def multiply_and_filter(seq, multiplier):
    """
    This function first filters out all the non-numeric elements in :attr:`seq` and then multiplies
    each of the numeric elements by :attr:`multiplier`.

    :param seq: A list containing numeric and non-numeric elements
    :param multiplier: The value with which to multiply each numeric element in :attr:`seq`
    :return: A list containing only numeric elements, each multiplied by :attr:`multiplier`
    """
    arr = []
    for element in seq:
        try:
            float(element)
            if str(element) not in ["True", "False"]:
                arr.append(element * multiplier)
        except (TypeError, ValueError):
            pass
    return arr
