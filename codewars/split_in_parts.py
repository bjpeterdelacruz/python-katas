"""
Split in Parts

:Author: BJ Peter Dela Cruz
"""


def split_in_parts(string: str, part_length: int) -> str:
    """
    This function splits the given string into substrings of size :attr:`part_length`. The last
    substring may be the same size as the other substrings or smaller. Then all the substrings are
    concatenated together with a single space, and the resulting string is returned.

    :param string: The input string
    :param part_length: The size of each substring
    :return: A string consisting of substrings separated by a single space
    """
    start = 0
    arr = []
    while start < len(string):
        arr.append(string[start:start + part_length])
        start += part_length
    return ' '.join(arr)
