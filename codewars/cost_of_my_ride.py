"""
Cost of My Ride

:Author: BJ Peter Dela Cruz
"""


def insurance(age: int, size: str, num_of_days: int) -> int:
    """
    This function calculates the total cost of renting a car for one or more days.

    :param age: The age of the person renting a car
    :param size: The size of the rental car
    :param num_of_days: The number of days to rent
    :return: The total cost of renting a car
    """
    if num_of_days <= 0:
        return 0
    total = 50 * num_of_days
    if size == "economy":
        total += 0
    elif size == "medium":
        total += (10 * num_of_days)
    else:
        total += (15 * num_of_days)
    if age < 25:
        total += (10 * num_of_days)
    return total
