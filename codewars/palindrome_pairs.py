"""
Palindrome Pairs

:Author: BJ Peter Dela Cruz
"""


def palindrome_pairs(words: list[str]) -> list[list[int]]:
    """
    This function finds all the pairs of strings in :attr:`words` that form a palindrome and returns
    a list of their indexes.

    :param words: The input list of strings
    :return: A list of indexes of the strings in :attr:`words` that form a palindrome
    """
    return [[idx1, idx2] for idx1, word1 in enumerate(words) for idx2, word2 in enumerate(words)
            if idx1 != idx2 and is_palindrome(word1, word2)]


def is_palindrome(word1: str, word2: str) -> bool:
    """
    This helper function concatenates two strings together and returns True if the concatenated
    string is a palindrome, False otherwise.

    :param word1: The first word
    :param word2: The second word
    :return: True if the concatenation of :attr:`word1` and :attr:`word2` is a palindrome, False
        otherwise
    """
    combined_word = str(word1) + str(word2)
    return combined_word == combined_word[::-1]
