"""
PaginationHelper

:Author: BJ Peter Dela Cruz
"""


class PaginationHelper:
    """
    This class keeps track of items on pages. The items themselves are not relevant.
    """

    def __init__(self, collection, items_per_page: int):
        """
        Sets up this helper for tracking the number of items per page.

        :param collection: The list of items to track
        :param items_per_page: The number of items per page
        """
        self._collection = collection
        self._items_per_page = items_per_page
        self._indexes = {idx: [] for idx in range(0, self.page_count())}
        self.__generate_item_indexes()

    def __generate_item_indexes(self) -> None:
        """
        Populates the indexes dictionary that keeps track of the page on which an item can be found.

        :return: None
        """
        idx = 0
        if self.__last_page_items_remaining() == 0:
            pages = self.page_count()
        else:
            pages = self.page_count() - 1
        for current_page in range(0, pages):
            for _ in range(0, self._items_per_page):
                self._indexes[current_page].append(idx)
                idx += 1
        if self.__last_page_items_remaining() > 0:
            for _ in range(0, self.__last_page_items_remaining()):
                self._indexes[self.page_count() - 1].append(idx)
                idx += 1

    def item_count(self) -> int:
        """
        Returns the number of items in the collection.

        :return: The number of items in the collection
        """
        return len(self._collection)

    def page_count(self) -> int:
        """
        Returns the total number of pages.

        :return: The total number of pages.
        """
        count = self.item_count() // self._items_per_page
        return count if self.__last_page_items_remaining() == 0 else count + 1

    def page_item_count(self, page_index: int) -> int:
        """
        Returns the number of items on the page at the given (zero-based) index.

        :param page_index: The page index
        :return: The number of items on the page
        """
        if page_index < 0:
            return -1
        if page_index >= self.page_count():
            return -1
        if page_index == self.page_count() - 1:
            if self.__last_page_items_remaining() != 0:
                return self.__last_page_items_remaining()
        return self._items_per_page

    def page_index(self, item_index: int) -> int:
        """
        Returns the (zero-based) page index at which the item at the given (zero-based) index in the
        collection can be found. If :attr:`item_index` is negative or greater than or equal to the
        number of items in the collection, -1 is returned.

        :param item_index: The index of the item in the collection
        :return: The page index at which the item can be found, -1 if :attr:`item_index` is out of
            bounds
        """
        for page_index, items in self._indexes.items():
            if item_index in items:
                return page_index
        return -1

    def __last_page_items_remaining(self) -> int:
        """
        Returns the number of items that are on the last page if the number of items modulo items
        per page is not equal to zero, e.g. if number of items is 13 and items per page is 4, then
        the last page will have only one item.

        :return: The number of items that are on the last page if the modulo operation is not zero
        """
        return self.item_count() % self._items_per_page
