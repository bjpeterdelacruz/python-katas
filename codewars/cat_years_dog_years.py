"""
Cat Years, Dog Years

:Author: BJ Peter Dela Cruz
"""


def human_years_cat_years_dog_years(human_years):
    """
    This function calculates a cat's age and a dog's age given a human's age in years.

    :param human_years: The input integer, the age of a human in years
    :return: A list containing the human's age (in years), the cat's age (in years), and the dog's
        age (in years)
    """
    if human_years == 1:
        return [1, 15, 15]
    if human_years == 2:
        return [2, 24, 24]
    years = human_years - 2
    return [human_years, 24 + (years * 4), 24 + (years * 5)]
