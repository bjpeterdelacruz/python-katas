"""
Dominant Array Elements

:Author: BJ Peter Dela Cruz
"""


def solve(arr: list[int]) -> list[int]:
    """
    This function returns a list of dominant array elements that are found in the given list. An
    element is dominant if it is greater than all the elements to its right.

    :param arr: The input list of integers
    :return: A list of integers that are dominant
    """
    if not arr:
        return []
    sol = [elem for idx, elem in enumerate(arr) if idx + 1 < len(arr) and max(arr[idx + 1:]) < elem]
    sol.append(arr[-1])
    return sol
