"""
Delete Occurrences

:Author: BJ Peter Dela Cruz
"""
from collections import defaultdict


def delete_nth(order: list[int], maximum: int) -> list[int]:
    """
    This function keeps up to :attr:`maximum` occurrences of an integer in :attr:`order`. All other
    instances of the same integer will be ignored.

    :param order: The input list of integers
    :param maximum: The maximum number of instances of an integer in :attr:`order` to keep
    :return: A list containing only up to :attr:`maximum` instances of each integer
    """
    counts = defaultdict(int)
    new_order = []
    for num in order:
        counts[num] += 1
        if counts[num] <= maximum:
            new_order.append(num)
    return new_order
