"""
Is It Even?

:Author: BJ Peter Dela Cruz
"""


def is_even(number) -> bool:
    """
    This function returns true if the given number is both an integer and an even number.

    :param number: The input number
    :return: True if :attr:`number` is both an integer and an even number
    """
    return isinstance(number, int) and number % 2 == 0
