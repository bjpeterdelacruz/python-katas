"""
Simple String Expansion

:Author: BJ Peter Dela Cruz
"""


def solve(string: str) -> str:
    """
    This function expands a string given its shortened form. For example, given "3(b(2(c)))", this
    function will return "bccbccbcc". Step by step, this function does the following: 3(b(2(c))) ->
    3(b(cc)) -> 3(bcc) -> bccbccbcc.

    :param string: The input string
    :return: The expanded form of :attr:`string`
    """
    temp = list(string)
    open_parens_indexes = []
    idx = 0
    while True:
        if '(' not in temp:
            break
        if temp[idx] == '(':
            if not temp[idx - 1].isnumeric():
                temp.insert(idx, '1')
                idx += 1
            open_parens_indexes.append(idx)
        elif temp[idx] == ')':
            string = ''.join(temp)
            open_paren_idx = open_parens_indexes.pop()
            repeat = int(string[open_paren_idx - 1])
            for current_char_idx in range(open_paren_idx - 1, -1, -1):
                # support for multi-digit repeats
                if not string[current_char_idx].isnumeric():
                    repeat = int(string[current_char_idx + 1:open_paren_idx])
                    break
            characters = string[open_paren_idx + 1:idx]
            string_to_replace = f"{repeat}({characters})"
            string = string.replace(string_to_replace, characters * repeat)
            temp = list(string)
            idx = 0
        idx += 1
    return string
