"""
Thinkful: Vectors

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=invalid-name
# pylint: disable=too-few-public-methods


class Vector:
    """
    This class represents a vector with a magnitude in the X and Y directions.
    """
    def __init__(self, x, y):
        """
        Initializes a vector with a magnitude in the given X and Y directions.

        :param x: The X direction
        :param y: The Y direction
        """
        self.x = x
        self.y = y

    def add(self, other_vector):
        """
        Returns a new vector whose magnitude is the sum of the components of this vector and the
        given vector.

        :param other_vector: The other vector
        :return: A new vector with a magnitude that is the sum of the components of this vector and
            :attr:`other_vector`
        """
        return Vector(self.x + other_vector.x, self.y + other_vector.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
