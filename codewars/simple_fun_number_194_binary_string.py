"""
Binary String

:Author: BJ Peter Dela Cruz
"""


def bin_str(binary_string: str) -> int:
    """
    This function counts the number of steps needed to turn a binary string into a string containing
    all zeros. If a one is found in the binary string, all the bits to the right of the one and
    including the one is flipped, i.e. one becomes zero, and vice versa. For example, given "0101",
    the function will return 3 because "0101" -> "0010" -> "0001" -> "0000".

    :param binary_string: The binary string representation of a number
    :return: The number of steps needed to turn the binary string into one that contains all zeros
    """
    if not binary_string:
        return 0
    count = 0
    while "1" in binary_string:
        arr = []
        idx = binary_string.index("1") + 1
        while idx < len(binary_string):
            if binary_string[idx] == "0":
                arr.append("1")
            else:
                arr.append("0")
            idx += 1
        binary_string = ''.join(arr)
        count += 1
    return count
