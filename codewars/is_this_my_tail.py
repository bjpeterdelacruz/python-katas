"""
Is This My Tail?

:Author: BJ Peter Dela Cruz
"""


def correct_tail(body: str, tail: str) -> bool:
    """
    This function returns True if the last character of :attr:`body` is equal to :attr:`tail`, or
    False otherwise. It is assumed that :attr:`body` is a non-empty string.

    :param body: A string that contains at least one character
    :param tail: A string
    :return: True if the last character of :attr:`body` is equal to :attr:`tail`, or False otherwise
    """
    return body[-1] == tail
