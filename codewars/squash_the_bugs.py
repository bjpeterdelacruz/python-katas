"""
Squash the Bugs

:Author: BJ Peter Dela Cruz
"""


def find_longest(string: str) -> int:
    """
    This function returns the length of the longest string in the given whitespace-delimited list of
    strings.

    :param string: The input list of strings separated by whitespace(s)
    :return: The length of the longest string in the list
    """
    return len(sorted(string.split(), key=len)[-1])
