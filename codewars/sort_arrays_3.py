"""
Sort Arrays - 3

:Author: BJ Peter Dela Cruz
"""


def sort_me(courses: list[str]) -> list[str]:
    """
    This function sorts strings that are in the following format: <string>-<number>. The strings
    are first sorted by <number> and then by <string> in lexicographical order.

    :param courses: The input list of strings that represent course names in the aforementioned
        format
    :return: The same list of strings but in sorted order first by <number> and then by <string>
    """
    tuples = []
    for course in courses:
        _course = course.split("-")
        tuples.append((_course[0], _course[1]))
    tuples.sort(key=lambda tup: (tup[1], tup[0]))
    return [f"{tup[0]}-{tup[1]}" for tup in tuples]
