"""
Exclamation Marks Series #1: Remove an Exclamation Mark from the End of String

:Author: BJ Peter Dela Cruz
"""


def remove(string: str) -> str:
    """
    This function removes the last exclamation mark at the end of the given string.

    :param string: The input string
    :return: A string from which one exclamation mark was removed
    """
    return string[0:-1] if string and string[-1] == "!" else string
