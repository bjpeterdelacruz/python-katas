"""
Convert a Hex String to RGB

:Author: BJ Peter Dela Cruz
"""


def hex_string_to_rgb(hex_string: str) -> dict[str, int]:
    """
    This function converts the given hexadecimal string representing a color into a dictionary of
    red, green, and blue integer values from 0 to 255.

    :param hex_string: The input string, a hexadecimal number starting with a pound sign
    :return: A dictionary of red, green, and blue integer values
    """
    return {'r': int(hex_string[1:3], 16),
            'g': int(hex_string[3:5], 16),
            'b': int(hex_string[5:], 16)}
