"""
CSV Representation of Array

:Author: BJ Peter Dela Cruz
"""


def to_csv_text(array: list[list]) -> str:
    """
    This function turns a two-dimensional list into a CSV-formatted string. All the elements in each
    row will be separated by a comma, and all the rows will be separated by a newline character.
    (CSV is an abbreviation for comma-separated values.)

    :param array: The input two-dimensional list (a list of lists)
    :return: A CSV-formatted string
    """
    return '\n'.join(','.join(str(char) for char in arr) for arr in array)
