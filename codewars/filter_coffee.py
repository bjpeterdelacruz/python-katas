"""
Filter Coffee

:Author: BJ Peter Dela Cruz
"""


def search(budget: int, prices: list[int]) -> str:
    """
    This function returns a sorted, comma-delimited list of coffee prices that are within budget.

    :param budget: The input integer, the maximum amount to pay for coffee
    :param prices: A list of coffee prices
    :return: A list of coffee prices that are within budget and sorted in ascending order
    """
    return ','.join(map(str, sorted(filter(lambda x: x <= budget, prices))))
