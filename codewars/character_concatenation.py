def char_concat(string: str) -> str:
    middle = len(string) // 2
    if len(string) % 2 == 1:
        string = string[0:middle] + string[middle + 1:]
    arr = []
    end_idx = -1
    for idx, char in enumerate(string):
        arr.append(f'{char}{string[end_idx]}{idx + 1}')
        end_idx -= 1
        if idx + 1 == middle:
            break
    return ''.join(arr)
