"""
Simple Substitution Cipher Helper

:Author: BJ Peter Dela Cruz
"""


class Cipher:
    """
    This class performs a very simple substitution cipher using two strings, one to encode a string
    and another to decode a string.
    """
    def __init__(self, encode_string, decode_string):
        """
        Initializes this class with strings that are used for the encoding and decoding processes.

        :param encode_string: The string used for the encoding process
        :param decode_string: The string used for the decoding process
        """
        self.encode_string = encode_string
        self.decode_string = decode_string

    def process(self, string: str, encode: bool = True) -> str:
        """
        This helper method will either encode or decode the given string based on the value of
        :attr:`encode`.

        :param string: The string to either encode or decode
        :param encode: True to encode, False to decode
        :return: An encoded or decoded string
        """
        first = self.encode_string
        second = self.decode_string
        if not encode:
            first = self.decode_string
            second = self.encode_string
        new_string = []
        for character in string:
            if character in first:
                new_string.append(second[first.index(character)])
            else:
                new_string.append(character)
        return ''.join(new_string)

    def encode(self, string_to_encode: str) -> str:
        """
        This method delegates to :func:`process` to encode the given string.

        :param string_to_encode: The string to encode
        :return: An encoded string
        """
        return self.process(string_to_encode, encode=True)

    def decode(self, string_to_decode: str) -> str:
        """
        This method delegates to :func:`process` to decode the given string.

        :param string_to_decode: The string to decode
        :return: A decoded string
        """
        return self.process(string_to_decode, encode=False)
