"""
Weight for Weight

:Author: BJ Peter Dela Cruz
"""


def order_weight(string: str) -> str:
    """
    This function sorts the given list of weights by the sum of all the digits in each weight. If
    two weights are the same, then the two are sorted in lexicographical order, i.e. each weight is
    treated as a string not an int when it comes to sorting equal weights.

    :param string: The input string representing weights
    :return: A string of weights sorted according to the description above
    """
    weights = {}
    for weight in string.split():
        total_weight = sum(int(character) for character in weight)
        if total_weight in weights:
            weights[total_weight].append(weight)
        else:
            weights[total_weight] = [weight]
    sorted_keys = sorted(weights.keys())
    sorted_weights = []
    for key in sorted_keys:
        value = weights.get(key)
        if len(value) == 1:
            sorted_weights.append(value[0])
        else:
            sorted_weights.extend(sorted(value))
    return ' '.join(sorted_weights)
