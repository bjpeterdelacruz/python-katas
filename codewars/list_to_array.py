"""
List to Array

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=too-few-public-methods


class LinkedList:
    """
    This class represents a node in a singly-linked list.
    """
    def __init__(self, value, next_node=None):
        """
        Initializes a node in a singly-linked list.

        :param value: The value of the node
        :param next_node: The node that follows this one in the list
        """
        self.value = value
        self.next = next_node


def list_to_array(linked_list: LinkedList) -> list:
    """
    This function returns a Python list that contains all the values in the given linked list.

    :param linked_list: A linked list that contains zero, one, or more nodes
    :return: A Python list that contains all the values in :attr:`linked_list`
    """
    arr = []
    while linked_list:
        arr.append(linked_list.value)
        linked_list = linked_list.next
    return arr
