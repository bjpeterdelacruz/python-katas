"""
Well of Ideas: Easy Version

:Author: BJ Peter Dela Cruz
"""


def well(arr: list[str]) -> str:
    """
    This function counts the number of times the word "good" appears in the given list of strings.
    If it appears once or twice, "Publish!" is returned. If it appears more than two times, the
    string "I smell a series!" is returned. But if it does not appear in the list at all, "Fail!" is
    returned.

    :param arr: The input list of strings
    :return: "I smell a series!", "Publish!" or "Fail!"
    """
    count = sum(1 for idea in arr if idea == 'good')
    return 'I smell a series!' if count > 2 else 'Publish!' if count > 0 else 'Fail!'
