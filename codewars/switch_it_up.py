"""
Switch It Up

:Author: BJ Peter Dela Cruz
"""


def switch_it_up(number: int) -> str:
    """
    This function returns the given number as a word.

    :param number: The input integer, a number from 0 to 9
    :return: The number as a word, e.g. "One", "Two", etc.
    """
    numbers = {0: "Zero", 1: "One", 2: "Two", 3: "Three", 4: "Four",
               5: "Five", 6: "Six", 7: "Seven", 8: "Eight", 9: "Nine"}
    return numbers[number]
