"""
Calculate Meal Total

:Author: BJ Peter Dela Cruz
"""


def calculate_total(subtotal: float, tax: int, tip: int) -> float:
    """
    This function calculates the total amount for a meal given the subtotal, tax percentage, and tip
    percentage. The tip amount is calculated only using the subtotal.

    :param subtotal: The subtotal price
    :param tax: The tax percentage
    :param tip: The tip percentage
    :return: The total amount that includes the subtotal, tax, and tip
    """
    subtotal_with_tax = subtotal * (tax / 100) + subtotal
    tip_amount = subtotal * (tip / 100)
    return round(subtotal_with_tax + tip_amount, 2)
