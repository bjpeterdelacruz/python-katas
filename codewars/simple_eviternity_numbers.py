"""
Simple Eviternity Numbers

:Author: BJ Peter Dela Cruz
"""


def is_eviternity_number(number: str) -> bool:
    """
    This helper function determines whether the given number is an eviternity number. An eviternity
    number only contains 8, 5, and 3; and the number of 8's must be greater than or equal to the
    number of 5's, which must be greater than or equal to the number of 3's.

    :param number: An integer as a string
    :return: True if the number is an eviternity number, False otherwise
    """
    for character in number:
        if character in '0124679':
            return False
    return number.count('8') >= number.count('5') >= number.count('3')


def solve(minimum: int, maximum: int) -> int:
    """
    This function counts the number of eviternity numbers between :attr:`minimum` (inclusive) and
    :attr:`maximum` (exclusive).

    :param minimum: The start of the range (inclusive)
    :param maximum: The end of the range (exclusive)
    :return: The number of eviternity numbers in the given range
    """
    return len([number for number in range(minimum, maximum) if is_eviternity_number(str(number))])
