"""
C.Wars

:Author: BJ Peter Dela Cruz
"""


def initials(name: str) -> str:
    """
    This function creates initials for all but the last name in the given string that represents a
    person's name. For example, given "BJ Peter Dela Cruz", "B.P.D.Cruz" is returned.

    :param name: The input string
    :return: A person's initials and his or her last name
    """
    if not name:
        return name
    arr = name.split()
    names = [nm[0].upper() for nm in arr]
    if len(arr[-1]) > 1:
        names[-1] += arr[-1][1:].lower()
    return '.'.join(names)
