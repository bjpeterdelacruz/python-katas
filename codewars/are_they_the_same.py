"""
Are They The "Same"?

:Author: BJ Peter Dela Cruz
"""


def comp(array1: list[int], array2: list[int]) -> bool:
    """
    This function takes two lists, both of the same size, and returns True if the square of each
    number in :attr:`array1` is present in :attr:`array2`.

    :param array1: The input list of integers
    :param array2: The input list of integers, contains the square of each number of :attr:`array1`
    :return: True if :attr:`array2` contains the square of each number in :attr:`array1`, False
        otherwise
    """
    if array1 is None or array2 is None:
        return False
    if array1 and not array2 or not array1 and array2:
        return False
    set1 = set(array1)
    set2 = set(array2)
    if len(set1) != len(set2):
        return False
    for number in array1:
        count1 = array1.count(number)
        squared = number ** 2
        if squared not in array2 or count1 != array2.count(squared):
            return False
    return True
