"""
Make Equal

:Author: BJ Peter Dela Cruz
"""


def count(numbers: list[int], target_number: int, value: int) -> int:
    """
    This function returns the number of integers in :attr:`numbers` that can be incremented or
    decremented by :attr:`value` any number of times to equal :attr:`target_number`. If
    :attr:`value` is 0, then a count of the number of integers that equal :attr:`target_number` is
    returned.

    :param numbers: The input list of integers
    :param target_number: The target number
    :param value: The value by which to increment or decrement each integer in :attr:`numbers`
    :return: The number of integers in :attr:`numbers` that equal :attr:`target_number`
    """
    if value == 0:
        return len([number for number in numbers if number == target_number])
    num_elements = 0
    abs_value = abs(value)
    for number in numbers:
        while number > target_number:
            number -= abs_value
        while number < target_number:
            number += abs_value
        if number == target_number:
            num_elements += 1
    return num_elements
