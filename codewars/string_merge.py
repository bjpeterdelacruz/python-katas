"""
String Merge!

:Author: BJ Peter Dela Cruz
"""


def string_merge(string_1: str, string_2: str, letter: str) -> str:
    """
    Given two words and a letter, this function returns a single word that is a combination of both
    words, merged at the point at which the given letter first appears in each word. The word that
    is returned will begin with the letters of the first word and end with the letters of the second
    word, and the given letter will be in the middle. It is assumed that both words will always
    contain the given letter.

    :param string_1: The first word
    :param string_2: The second word
    :param letter: A letter that appears in both words
    :return: A word that is a merger of both words
    """
    return string_1[0:string_1.index(letter)] + string_2[string_2.index(letter):]
