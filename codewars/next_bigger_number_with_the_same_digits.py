"""
Next Bigger Number with Same Digits

:Author: BJ Peter Dela Cruz
"""
from itertools import permutations as permute


def next_bigger(integer: int) -> int:
    """
    This function returns the next number that is bigger than :attr:`integer` using only the digits
    in :attr:`integer`.

    :param integer: The input integer
    :return: The next number that is bigger than :attr:`integer` using only the digits in
        :attr:`integer`
    """
    if str(integer) == ''.join(reversed(sorted(str(integer)))):
        return -1
    string = ''.join(reversed(str(integer)))
    temp = []
    for size in range(2, len(string)):
        permutations = permute(string[:size], size)
        for permutation in permutations:
            result = int(''.join(reversed(''.join(permutation) + string[size:])))
            if result > integer:
                temp.append(result)
        if temp:
            break
    temp.sort()
    return temp[0]
