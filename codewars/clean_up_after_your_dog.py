"""
Clean up After Your Dog

:Author: BJ Peter Dela Cruz
"""


def crap(garden: list[list[str]], bags: int, cap: int) -> str:
    """
    This function determines whether a garden can be cleared of a dog's cr@p. If all the cr@p cannot
    fit in the bags, the string "Cr@p" is returned. Otherwise, the string "Clean" is returned. But
    if the dog is present is the garden, the string "Dog!!" is returned.

    :param garden: A two-dimensional list of strings that represents a garden that may or may not
        contain a dog and cr@p.
    :param bags: The number of bags
    :param cap: The capacity of each bag
    :return: "Cr@p", "Clean", or "Dog!!"
    """
    characters = ''.join(value for row in garden for value in row)
    if 'D' in characters:
        return 'Dog!!'
    return 'Cr@p' if bags * cap < characters.count('@') else 'Clean'
