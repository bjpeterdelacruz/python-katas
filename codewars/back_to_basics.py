"""
Back to Basics

:Author: BJ Peter Dela Cruz
"""


def types(obj) -> str:
    """
    This function returns the type of the given object as a string, e.g. types(5) returns 'int'.

    :param obj: The input object
    :return: The type of the object as a string
    """
    type_obj = str(type(obj))
    type_obj = type_obj[type_obj.index("'") + 1:]
    return type_obj[:type_obj.index("'")]
