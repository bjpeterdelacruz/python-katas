"""
Password Check

:Author: BJ Peter Dela Cruz
"""


def decode_pass(pass_list: list[str], bits: str) -> str:
    """
    This function decodes groups of bits that represent a possible password and returns the decoded
    string if it is found in the input list or False if it is not found in the list.

    :param pass_list: The input list of strings
    :param bits: Groups of bits (zeros and ones) separated by whitespace(s)
    :return: The decoded string or False
    """
    string = ''.join(map(chr, map(lambda x: int(x, 2), bits.split())))
    return string if string in pass_list else False
