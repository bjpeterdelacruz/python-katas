"""
Maximum Sum Between Two Negatives

:Author: BJ Peter Dela Cruz
"""
from math import inf


def max_sum_between_two_negatives(arr: list[int]) -> int:
    """
    This function returns the maximum sum between two negative integers. If there are fewer than two
    negative integers in the given list, -1 is returned.

    :param arr: The input list of integers
    :return: The maximum sum between two negative integers, or -1 if there is no sum
    """
    indexes = [idx for idx, value in enumerate(arr) if value < 0]
    if len(indexes) < 2:
        return -1
    max_sum = -inf
    for idx in range(1, len(indexes)):
        start = indexes[idx - 1] + 1
        end = indexes[idx]
        current = sum(arr[start:end])
        if current > max_sum:
            max_sum = current
    return max_sum
