"""
Find the Squares

:Author: BJ Peter Dela Cruz
"""


def find_squares(number: int) -> str:
    """
    This function returns a string that represents the difference of two squares. An even number is
    calculated by solving for N in the equation 2N + 1 = :attr:`number`. The even number is squared
    and becomes the subtrahend in the string representation of the equation that is returned. One is
    added to the even number and then squared. The result is the minuend in the equation.

    :param number: An odd integer that is the result of subtracting one square from another square
    :return: A string representing the difference between two squares
    """
    even_number = (number - 1) // 2  # solve for 2n + 1
    return f'{(even_number + 1) ** 2}-{even_number ** 2}'
