"""
Count IP Addresses

:Author: BJ Peter Dela Cruz
"""


def ips_between(start: str, end: str) -> int:
    """
    This function returns the number of IP version 4 addresses between :attr:`start` (inclusive) and
    :attr:`end` (exclusive).

    :param start: The input string, a valid IP address
    :param end: The input string, a valid IP address
    :return: The number of IP addresses between :attr:`start` and :attr:`end`
    """
    ip1 = int(''.join([f"{int(ip):08b}" for ip in start.split(".")]), 2)
    ip2 = int(''.join([f"{int(ip):08b}" for ip in end.split(".")]), 2)
    return abs(ip1 - ip2)
