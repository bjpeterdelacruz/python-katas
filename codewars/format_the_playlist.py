"""
Format the Playlist

:Author: BJ Peter Dela Cruz
"""

# pylint: disable=too-many-arguments


def generate_row(row: tuple[str, str, str], name_len: int, time_len: int, artist_len: int,
                 padding: str, col_sep: str, left_border: str, right_border: str) -> str:
    """
    This helper function returns a row for the playlist that contains a song's name, artist, and
    length.

    :param row: A tuple that contains a song's name, artist, and length
    :param name_len: The column length for the song's name
    :param time_len: The column length for the song's length
    :param artist_len: The column length for the song's artist
    :param padding: The character to use for padding up to a column's length
    :param col_sep: The character to use to separate columns
    :param left_border: The character to use for the left border of the row
    :param right_border: The character to use for the right border of the row
    :return: A row containing a song's name, artist, and length with appropriate padding
    """
    song_name = row[0] + (padding * (name_len - len(row[0])))
    song_time = row[1] + (padding * (time_len - len(row[1])))
    song_artist = row[2] + (padding * (artist_len - len(row[2])))
    return left_border + song_name + col_sep + song_time + col_sep + song_artist + right_border


def format_playlist(songs: list[tuple[str, str, str]]) -> str:
    """
    This function returns a string representing a playlist that contains a list of songs. The
    playlist will list the songs ordered first by artist and then by name. The first column will
    contain a song's name, followed by its length, and then finally the song's artist.

    :param songs: A list of tuples, each tuple contains a song's name, artist, and length
    :return: A string representing a playlist
    """
    sorted_songs = sorted(songs, key=lambda x: (x[2], x[0]))
    if sorted_songs:
        max_name_length = max(len(song_name[0]) for song_name in sorted_songs)
        max_time_length = max(len(song_name[1]) for song_name in sorted_songs)
        max_artist_length = max(len(song_name[2]) for song_name in sorted_songs)
    else:
        max_name_length = max_time_length = 4
        max_artist_length = 6
    playlist = [generate_row(("-", "-", "-"), max_name_length, max_time_length,
                             max_artist_length, "-", "-+-", "+-", "-+"),
                generate_row(("Name", "Time", "Artist"), max_name_length, max_time_length,
                             max_artist_length, " ", " | ", "| ", " |"),
                generate_row(("-", "-", "-"), max_name_length, max_time_length,
                             max_artist_length, "-", "-+-", "+-", "-+")]
    if sorted_songs:
        for song in sorted_songs:
            playlist.append(generate_row(song, max_name_length, max_time_length, max_artist_length,
                                         " ", " | ", "| ", " |"))
        playlist.append(generate_row(("-", "-", "-"), max_name_length, max_time_length,
                                     max_artist_length, "-", "-+-", "+-", "-+"))
    return '\n'.join(playlist)
