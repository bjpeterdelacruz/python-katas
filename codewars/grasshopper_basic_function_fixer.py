"""
Grasshopper: Basic Function Fixer

:Author: BJ Peter Dela Cruz
"""


def add_five(num):
    """
    This function adds five to a number and returns the result.

    :param num: The number
    :return: The result of adding five to a number
    """
    return num + 5
