"""
Factorial Decomposition

:Author: BJ Peter Dela Cruz
"""
from codewars.primes_in_numbers import get_prime_factors


def decomp(number: int) -> str:
    """
    This function decomposes the result of :attr:`number`! into prime factors with exponents. For
    example, given "5", this function will return "2^3 * 3 * 5". Note that the factorial is not
    actually computed.

    :param number: The input integer
    :return: The result of the factorial expressed as prime factors with exponents multiplied
        together
    """
    numbers = []
    for num in range(number, 1, -1):
        numbers.append(num)
    primes = {}
    for num in numbers:
        primes_list = get_prime_factors(num)
        for prime in primes_list:
            if prime in primes:
                primes[prime] += 1
            else:
                primes[prime] = 1
    numbers = []
    keys = sorted(primes.keys())
    for prime in keys:
        count = primes[prime]
        if count == 1:
            numbers.append(f"{prime}")
        else:
            numbers.append(f"{prime}^{count}")
    return ' * '.join(numbers)
