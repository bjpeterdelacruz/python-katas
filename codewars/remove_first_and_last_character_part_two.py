"""
Remove First and Last Character Part Two

:Author: BJ Peter Dela Cruz
"""


def array(lst: str):
    """
    This function removes the first and last elements from a comma-separated list. If the input list
    contains two elements or fewer, None is returned. Otherwise, a list of elements separated by
    spaces is returned.

    :param lst: The input comma-separated list of elements
    :return: None, or a space-seperated list of the remaining elements after removing the first and
        elements from :attr:`lst`
    """
    arr = [char.strip() for char in lst.split(",")]
    return None if len(arr) <= 2 else ' '.join(arr[1:-1])
