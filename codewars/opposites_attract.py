"""
Opposites Attract

:Author: BJ Peter Dela Cruz
"""


def lovefunc(flower_1: int, flower_2: int) -> bool:
    """
    This function returns True if :attr:`flower_1` is odd and :attr:`flower_2` is even, or vice
    versa. False is returned if both are odd or both are even.

    :param flower_1: The input integer
    :param flower_2: The input integer
    :return: True if one integer is odd and the other is even, False if both are odd or even
    """
    if flower_1 % 2 == 0 and flower_2 % 2 == 0:
        return False
    return not (flower_1 % 2 != 0 and flower_2 % 2 != 0)
