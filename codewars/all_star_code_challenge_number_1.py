"""
All Star Code Challenge #1

Author: BJ Peter Dela Cruz
"""


def sum_ppg(player_one: dict, player_two: dict):
    """
    This function sums the PPG (points per game) of two basketball players.

    :param player_one: The first basketball player
    :param player_two: The second basketball player
    :return: The total PPG
    """
    return player_one['ppg'] + player_two['ppg']
