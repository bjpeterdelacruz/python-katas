"""
Count the Digits

Author: BJ Peter Dela Cruz
"""


def nb_dig(num: int, dgt: int) -> int:
    """
    This function generates a list of squared numbers from 0 to :attr:`num` and then returns a count
    of the number of times :attr:`dgt` appears in each number in the list.

    :param num: The input integer
    :param dgt: The input integer, the digit to count in each squared number
    :return: The number of times :attr:`dgt` appears in each squared number in the list
    """
    numbers = [number * number for number in range(0, num + 1)]
    return sum(1 for number in numbers for digit in str(number) if str(dgt) == digit)
