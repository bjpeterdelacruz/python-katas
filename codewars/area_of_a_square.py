"""
Area of a Square

:Author: BJ Peter Dela Cruz
"""
from math import pi


def square_area(arc_length: int):
    """
    This function calculates the area of a square given the length of an arc that stretches from one
    corner of the square to the opposite corner. The formula for calculating the radius of a circle
    is as follows: radius = arc length / angle (in radians). The radius of the circle is equal to
    the length of each side of the square. Note that ninety degrees is equal to pi / 2 radians.

    :param arc_length: The arc length of a part of a circle
    :return: The area of the square
    """
    return round((arc_length * 2 / pi) ** 2, 2)
