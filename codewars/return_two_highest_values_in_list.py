"""
Return Two Highest Values in List

:Author: BJ Peter Dela Cruz
"""


def two_highest(lst: list) -> list:
    """
    This function returns a list of the two highest values in the given list. If the list contains
    fewer than two elements, then the list is simply returned.

    :param lst: The input list
    :return: A list of two highest values, or the input list if it contains fewer than two elements
    """
    sorted_list = sorted(set(lst))
    if len(sorted_list) < 2:
        return sorted_list
    return [sorted_list[-1], sorted_list[-2]]
