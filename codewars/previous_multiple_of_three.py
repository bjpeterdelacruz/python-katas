"""
Previous Multiple of Three

:Author: BJ Peter Dela Cruz
"""


def prev_mult_of_three(number: int) -> int | None:
    """
    This function removes the last digit in :attr:`number` until the result is a multiple of three.
    If all the digits are removed, then None is returned.

    :param number: The input integer
    :return: The digits that make up a number that is a multiple of three, or None if all the digits
        are removed
    """
    string = str(number)
    while string:
        if int(string) % 3 == 0:
            return int(string)
        string = string[:len(string) - 1]
    return None
