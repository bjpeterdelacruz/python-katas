"""
Exclusive "or" (xor) Logical Operator

:Author: BJ Peter Dela Cruz
"""


def xor(first, second) -> bool:
    """
    This function is an implementation of an exclusive-or (XOR) operation. If both input arguments
    are truthy, False is returned. If both input arguments are falsy, False is returned. Otherwise,
    True is returned if only one argument is truthy.

    :param first: The first input argument
    :param second: The second input argument
    :return: True if only one argument is truthy, Falsy otherwise
    """
    if first and second:
        return False
    return not (not first and not second)
