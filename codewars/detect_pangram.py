"""
Detect Pangram

:Author: BJ Peter Dela Cruz
"""


def is_pangram(string: str) -> bool:
    """
    This function returns True if :attr:`string` is a pangram (a sentence that contains all letters
    of the English alphabet at least once), or False otherwise.

    :param string: The input string
    :return: True if :attr:`string` is a pangram, False otherwise
    """
    characters = set()
    for char in string:
        if char.isalpha():
            characters.add(char.lower())
            if len(characters) == 26:
                return True
    return False
