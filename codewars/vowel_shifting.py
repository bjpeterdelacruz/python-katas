"""
Vowel Shifting

:Author: BJ Peter Dela Cruz
"""
from collections import deque

ALL_VOWELS = "aeiouAEIOU"


def vowel_shift(text: str, num: int) -> str:
    """
    This function rotates all the vowels in the given string right if :attr:`num` is positive or
    left if :attr:`num` is negative. The other characters in the string are left in place. For
    example, given "This is a test!" and :attr:`num` is 3, "This as e tist!" is returned because
    [i, i, a, e] rotated three steps to the right is [i, a, e, i].

    :param text: The input string
    :param num: The number of steps to rotate vowels right (if positive) or left (if negative)
    :return: A new string after rotating all the vowels right or left, or the same string if
        :attr:`text` is empty or :attr:`num` is zero
    """
    if not text or num == 0:
        return text
    vowels = deque([letter for letter in text if letter in ALL_VOWELS])
    vowels.rotate(num)
    characters = []
    idx = 0
    for character in text:
        if character in ALL_VOWELS:
            characters.append(vowels[idx])
            idx += 1
        else:
            characters.append(character)
    return ''.join(characters)
