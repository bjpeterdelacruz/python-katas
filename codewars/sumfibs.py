"""
Sum Fibs

:Author: BJ Peter Dela Cruz
"""


def sum_fibs(position: int) -> int:
    """
    This function sums all the even numbers in the Fibonacci sequence up to index :attr:`position`.

    :param position: The input integer, the last index in the list of Fibonacci numbers
    :return: The sum of all even numbers in the list of Fibonacci numbers
    """
    fibs = [0, 1]
    idx = 2
    while idx <= position:
        fibs.append(fibs[idx - 2] + fibs[idx - 1])
        idx += 1
    return sum(i for i in fibs if i % 2 == 0)
