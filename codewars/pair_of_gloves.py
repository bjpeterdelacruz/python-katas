"""
Pairs of Gloves

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def number_of_pairs(gloves: list[str]) -> int:
    """
    This function returns the number of pairs of gloves that one can make from the given list of
    gloves. A pair consists of two gloves of the same color.

    :param gloves: The input list of strings representing colored gloves
    :return: The number of pairs of gloves
    """
    all_gloves = {key: len(list(value)) for key, value in groupby(sorted(gloves))}
    return sum(value // 2 for value in all_gloves.values())
