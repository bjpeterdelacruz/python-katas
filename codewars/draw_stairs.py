"""
Draw Stairs

:Author: BJ Peter Dela Cruz
"""


def draw_stairs(number: int) -> str:
    """
    This function draws a staircase with :attr:`number` steps using the letter I.

    :param number: The input integer, the number of steps in the staircase
    :return: A string representing a staircase
    """
    return '\n'.join(f'{" " * num}I' for num in range(0, number))
