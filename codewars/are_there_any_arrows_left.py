def any_arrows(arrows: list[dict]) -> bool:
    for arrow in arrows:
        if "damaged" not in arrow or arrow["damaged"] is False:
            return True
    return False
