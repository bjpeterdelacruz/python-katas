"""
A Scandal in Bohemia

:Author: BJ Peter Dela Cruz
"""


def key(text: str, extract: str) -> str:
    """
    This function finds the substitution cipher key that was used to encrypt :attr:`extract`.

    :param text: The decrypted text
    :param extract: An encrypted string
    :return: The substitution cipher key used to encrypt :attr:`extract`
    """
    text_chars = ''.join(['A' if 65 <= ord(char.upper()) <= 90 else char for char in text])
    extract_chars = ''.join(['A' if 65 <= ord(char.upper()) <= 90 else char for char in extract])
    start_idx = text_chars.index(extract_chars)
    cipher = dict(zip(get_characters(text[start_idx:]), get_characters(extract)))
    return ''.join([cipher[letter] for letter in "ABCDEFGHIJKLMNOPQRSTUVWXYZ"]).lower()


def get_characters(string: str) -> list[str]:
    """
    This helper function returns all 26 characters in the English alphabet in the order that they
    appear in :attr:`string`.

    :param string: The string from which to get all 26 letters
    :return: A list of all 26 letters of the English alphabet in the order that they appear in
        :attr:`string`
    """
    letters = []
    for word in string:
        for character in word:
            if 65 <= ord(character.upper()) <= 90 and character.upper() not in letters:
                letters.append(character.upper())
                if len(letters) == 26:
                    break
        if len(letters) == 26:
            break
    return letters
