"""
Where My Anagrams At?

:Author: BJ Peter Dela Cruz
"""
from collections import Counter


def anagrams(string: str, strings: list[str]) -> list[str]:
    """
    This function returns a subset of the words from the given list that form an anagram using the
    letters in :attr:`string`.

    :param string: The input string that contains the letters to form an anagram
    :param strings: The input list of strings, some of which form an anagram
    :return: The list of strings that form an anagram using all the letters in :attr:`string`
    """
    counter_string = Counter(string)
    return [word for word in strings if Counter(word) == counter_string]
