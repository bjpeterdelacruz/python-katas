"""
Get Integers Between Two Numbers

:Author: BJ Peter Dela Cruz
"""


def function(start_num: int, end_num: int) -> list[int]:
    """
    This function generates a list of integers between :attr:`start_num` (exclusive) and
    :attr:`end_num` (exclusive). If :attr:`start_num` is greater than or equal to :attr:`end_num`,
    an empty list is returned.

    :param start_num: The input integer
    :param end_num: The input integer
    :return: A list of integers between :attr:`start_num` and :attr:`end_num`
    """
    return [] if start_num >= end_num else list(range(start_num + 1, end_num))
