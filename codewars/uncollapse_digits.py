"""
Uncollapse Digits

:Author: BJ Peter Dela Cruz
"""
import re


def uncollapse(digits: str) -> str:
    """
    This function adds a space between each word that represents a digit from zero (0) to nine (9).

    :param digits: The input string
    :return: The same string but with a space added between each word that represents a digit
    """
    return ' '.join(re.findall(r"(one|two|three|four|five|six|seven|eight|nine|zero)", digits))
