"""
Celsius Converter

:Author: BJ Peter Dela Cruz
"""


def weather_info(fahrenheit) -> str:
    """
    This function converts the given Fahrenheit temperature to Celsius. If the Celsius temperature
    is greater than zero, then a string containing "above freezing" is returned. Otherwise, a string
    containing "freezing" is returned.

    :param fahrenheit: The input Fahrenheit temperature
    :return: A string that contains "above freezing" or "freezing"
    """
    celsius = (fahrenheit - 32) * (5 / 9)
    if celsius <= 0:
        return f'{celsius} is freezing temperature'
    return f'{celsius} is above freezing temperature'
