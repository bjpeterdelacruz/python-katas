"""
Check Same Case

:Author: BJ Peter Dela Cruz
"""
from string import ascii_letters, ascii_lowercase, ascii_uppercase


def same_case(first_letter: str, second_letter: str) -> int:
    """
    This function returns -1 if either input string contains characters besides ASCII letters
    (numbers, symbols, etc.), 1 if both input strings are either lowercase or uppercase, or 0 if the
    first input string is lowercase and the second input string is uppercase, or vice versa.

    :param first_letter: The first input string
    :param second_letter: The second input string
    :return: -1, 0, or 1
    """
    if first_letter not in ascii_letters or second_letter not in ascii_letters:
        return -1
    if first_letter in ascii_lowercase and second_letter in ascii_lowercase:
        return 1
    return 1 if first_letter in ascii_uppercase and second_letter in ascii_uppercase else 0
