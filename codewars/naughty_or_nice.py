"""
Naughty or Nice

:Author: BJ Peter Dela Cruz
"""


def get_nice_names(people) -> list:
    """
    This function returns a list of names of people who are nice, i.e. was_nice = True.

    :param people: A map containing names of people and whether they are naughty or nice
    :return: A list of names of people who are nice
    """
    return [person["name"] for person in people if person["was_nice"]]


def get_naughty_names(people) -> list:
    """
    This function returns a list of names of people who are naughty, i.e. was_nice = False.

    :param people: A map containing names of people and whether they are naughty or nice
    :return: A list of names of people who are naughty
    """
    return [person["name"] for person in people if not person["was_nice"]]
