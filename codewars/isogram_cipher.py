"""
Isogram Cipher

:Author: BJ Peter Dela Cruz
"""


def isogram_encode(*args) -> str:
    """
    This function encodes a message using a 10-character isogram (a word that does not contain any
    repeated letters).

    :param args: A tuple containing the message to encode and a key
    :return: The encoded message
    """
    if len(args) != 2:
        return "Incorrect key or input!"
    msg, key = args
    if not msg or (not isinstance(msg, str) and not isinstance(msg, int)):
        return "Incorrect key or input!"
    if not key or len(key) != len(set(key)) or len(key) != 10:
        return "Incorrect key or input!"
    msg = str(msg)
    num_to_alpha = {key.index(char) + 1: char.upper() for char in key}
    result = []
    for char in msg:
        try:
            if char == '0':
                result.append(num_to_alpha[10])
            else:
                result.append(num_to_alpha[int(char)])
        except (KeyError, ValueError):
            return "Incorrect key or input!"
    return ''.join(result)


def isogram_decode(*args) -> str:
    """
    This function decodes a message using a 10-character isogram (a word that does not contain any
    repeated letters).

    :param args: A tuple containing an encoded message and a key
    :return: The decoded message
    """
    if len(args) != 2:
        return "Incorrect key or input!"
    msg, key = args
    if not msg or not isinstance(msg, str):
        return "Incorrect key or input!"
    if not key or len(key) != len(set(key)) or len(key) != 10:
        return "Incorrect key or input!"
    alpha_to_num = {char.upper(): key.index(char) + 1 for char in key}
    try:
        return ''.join(str(alpha_to_num[char.upper()]) if alpha_to_num[char.upper()] < 10 else "0"
                       for char in msg)
    except KeyError:
        return "Incorrect key or input!"
