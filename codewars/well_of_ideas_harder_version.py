"""
Well of Ideas: Harder Version

:Author: BJ Peter Dela Cruz
"""


def well(ideas: list[list]) -> str:
    """
    This function counts the number of times the word "good" (regardless of case) appears in the
    given two-dimensional list. Each list may contain not just strings but also other types of
    values such as integers and floats. If the word appears once or twice, "Publish!" is returned.
    If it appears more than two times, the string "I smell a series!" is returned. But if it does
    not appear at all, "Fail!" is returned.

    :param ideas: The input list of strings
    :return: "I smell a series!", "Publish!" or "Fail!"
    """
    count = [idea.lower() for arr in ideas for idea in arr if isinstance(idea, str)].count('good')
    return 'I smell a series!' if count > 2 else 'Publish!' if count > 0 else 'Fail!'
