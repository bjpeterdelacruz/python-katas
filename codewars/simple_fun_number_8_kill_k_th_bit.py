"""
Kill K-th Bit

:Author: BJ Peter Dela Cruz
"""


def kill_kth_bit(number: int, kth: int) -> int:
    """
    This function turns the bit at position :attr:`kth` in the given number to zero if it is one.
    The 0-based index in the binary representation of the number starts from the right.

    :param number: The input number
    :param kth: The bit position to turn off, i.e. switch from 1 to 0
    :return: The same number but with the :attr:`kth` position switched off
    """
    return int(''.join(reversed(['0' if kth - 1 == idx else str(value) for idx, value in enumerate(
        reversed(str(bin(number))[2:]))])), 2)
