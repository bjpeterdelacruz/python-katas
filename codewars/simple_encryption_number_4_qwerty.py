"""
Simple Encryption #4: Qwerty

:Author: BJ Peter Dela Cruz
"""
ROW_1 = "qwertyuiop"
ROW_2 = "asdfghjkl"
ROW_3 = "zxcvbnm,."


def shift(text: str, key: int, should_encrypt: bool = True) -> str:
    """
    This helper function rotates characters in the given string right (to encrypt, i.e. if
    :attr:`should_encrypt` is True) or left (to decrypt, i.e. if :attr:`should_encrypt` is False).
    The value for :attr:`key` is between 000 and 999, inclusive. Each digit in :attr:`key`
    represents one region and the number of steps to rotate each character in :attr:`text` that
    belongs to that region. The case of each character is preserved, i.e. encrypting an uppercase
    character in :attr:`text` will produce an uppercase character in the ciphertext.

    :param text: The input string to encrypt or decrypt
    :param key: The key, between 000 and 999, inclusive, each digit representing one region and the
        number of steps to rotate each character in that region
    :param should_encrypt: True to encrypt, False to decrypt (default is True)
    :return: A ciphertext or plaintext
    """
    key = str(key).rjust(3, '0')
    regions = [[ROW_1, int(key[0])], [ROW_1.upper(), int(key[0])],
               [ROW_2, int(key[1])], [ROW_2.upper(), int(key[1])],
               [ROW_3, int(key[2])], ["ZXCVBNM<>", int(key[2])]]
    ciphertext = []
    for char in text:
        found = False
        for region in regions:
            if char in region[0]:
                index = region[0].index(char)
                index = index + region[1] if should_encrypt else index - region[1]
                ciphertext.append(region[0][index % len(region[0])])
                found = True
                break
        if not found:
            ciphertext.append(char)
    return ''.join(ciphertext)


def encrypt(plaintext: str, key: int) -> str:
    """
    This function encrypts the given plaintext.

    :param plaintext: The input string
    :param key: The key, between 000 and 999, inclusive, each digit representing one region and the
        number of steps to rotate each character in that region
    :return: A ciphertext
    """
    return shift(plaintext, key)


def decrypt(ciphertext: str, key: int) -> str:
    """
    This function decrypts the given ciphertext.

    :param ciphertext: The input string
    :param key: The key, between 000 and 999, inclusive, each digit representing one region and the
        number of steps to rotate each character in that region
    :return: A plaintext
    """
    return shift(ciphertext, key, False)
