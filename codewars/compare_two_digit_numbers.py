def compare(num_1, num_2):
    str_1, str_2 = str(num_1), str(num_2)
    if set(str_1) == set(str_2):
        return '100%'
    for char in str_1:
        if char in str_2:
            return '50%'
    return '0%'
