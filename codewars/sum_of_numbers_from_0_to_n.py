"""
Sum of Numbers from 0 to N

:Author: BJ Peter Dela Cruz
"""


def show_sequence(num: int) -> str:
    """
    This function calculates the sum of all numbers from 0 to :attr:`num` if :attr:`num` is
    positive. A string representing the equation with the sum will be returned.

    :param num: The input integer
    :return: A string representing the equation that sums all numbers from 0 to :attr:`num` and
        includes the sum
    """
    if num == 0:
        return "0=0"
    if num < 0:
        return f"{num}<0"
    numbers = range(0, num + 1)
    return f"{'+'.join(map(str, numbers))} = {sum(numbers)}"
