"""
IP Address Finder (Code Golf)

:Author: BJ Peter Dela Cruz
"""
# pylint: disable=unnecessary-lambda-assignment


f=lambda s:[sum(map(ord,s))*i%256 for i in[1,2,3,4]]
