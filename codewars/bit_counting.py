"""
Bit Counting

:Author: BJ Peter Dela Cruz
"""


def count_bits(integer: int) -> int:
    """
    This function returns the number of 1's in the binary string representation for the given
    :attr:`integer`.

    :param integer: The input integer
    :return: The number of 1's in the binary represetnation of :attr:`integer`
    """
    return f"{integer:0b}".count("1")
