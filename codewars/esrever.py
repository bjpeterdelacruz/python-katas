"""
esreveR

:Author: BJ Peter Dela Cruz
"""


def reverse(lst: list) -> list:
    """
    This function reverses the given list without using slicing or any built-in functions.

    :param lst: The input list to reverse
    :return: A reversed list
    """
    reversed_list = []
    for idx in range(len(lst) - 1, -1, -1):
        reversed_list.append(lst[idx])
    return reversed_list
