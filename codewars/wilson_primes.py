"""
Wilson Primes

:Author: BJ Peter Dela Cruz
"""
from math import factorial


def am_i_wilson(number: int) -> bool:
    """
    This function returns True if :attr:`number` is prime and also a Wilson prime. Note that the
    only Wilson primes that exist are 5, 13, and 563. If any other Wilson primes exist, they must be
    larger than 2 * 10^13, or 20,000,000,000,000.

    :param number: The input integer
    :return: True if :attr:`number` is a Wilson prime
    """
    return (factorial(number - 1) + 1) % (number ** 2) == 0 if number % 2 == 1 else False
