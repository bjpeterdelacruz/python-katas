"""
Gradebook

:Author: BJ Peter Dela Cruz
"""


def get_grade(arg_1: int, arg_2: int, arg_3: int) -> str:
    """
    This function averages the given three grades and returns a grade letter. If the average is
    greater than or equal to 90 and less than or equal to 100, then "A" is returned. If the average
    is less than 90 and greater than or equal to 80, then "B" is returned. If the average is less
    than 80 and greater than or equal to 70, then "C" is returned. If the average is less than 70
    and greater than or equal to 60, then "D" is returned. If the average is less than 60, then "F"
    is returned.

    :param arg_1: The first grade
    :param arg_2: The second grade
    :param arg_3: The third grade
    :return: A grade letter representing the average of the three grades
    """
    average = (arg_1 + arg_2 + arg_3) / 3
    if 90 <= average <= 100:
        return 'A'
    if 80 <= average < 90:
        return 'B'
    if 70 <= average < 80:
        return 'C'
    return 'F' if average < 60 else 'D'
