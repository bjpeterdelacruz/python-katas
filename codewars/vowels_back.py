"""
Vowel Back

:Author: BJ Peter Dela Cruz
"""
from string import ascii_lowercase


def move_letter(letter: str, shift: int, backward: bool) -> str:
    """
    This helper function shifts the given letter either left (:attr:`backward` is True) or right
    :attr:`shift` places.

    :param letter: The letter to shift
    :param shift: The number of places to shift left (backward) or right (forward)
    :param backward: True to shift left, False to shift right
    :return: The result of the shift, or the original letter if the result is 'c', 'o', 'd', or 'e'
    """
    if backward:
        shift *= -1
    moved_letter = chr((ord(letter) - ord('a') + shift) % len(ascii_lowercase) + ord('a'))
    return letter if moved_letter in ['c', 'o', 'd', 'e'] else moved_letter


def vowel_back(string: str) -> str:
    """
    This function shifts all the letters in the given string forward or backward based on the
    following rules: move consonants nine places forward, move vowels five places backward. Wrap
    around to the beginning or end of the alphabet if the shift goes past 'z' or 'a', respectively.
    Exceptions: if the character to shift is 'c' or 'o', move backward one place; if the character
    is 'd', move backward three places, and if the character is 'e', move backward four places. If
    the result of the shift results in 'c', 'o', 'd', or 'e', revert back to the original letter.

    :param string: The input string that contains characters to shift forward or backward
    :return: A new string that contains characters that were the result of a shift
    """
    arr = []
    for char in string.lower():
        if char in ['c', 'o']:
            arr.append(move_letter(char, 1, True))
        elif char in ['d', 'e']:
            arr.append('a')
        elif char in ['a', 'i', 'u']:
            arr.append(move_letter(char, 5, True))
        else:
            arr.append(move_letter(char, 9, False))
    return ''.join(arr)
