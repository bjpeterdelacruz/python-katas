"""
Return a Sorted List of Objects

:Author: BJ Peter Dela Cruz
"""


def sort_list(sort_by: str, lst: list[dict[str, int]]) -> list[dict[str, int]]:
    """
    This function sorts a list of dictionary objects in reverse order by integer value and then
    returns the sorted list.

    :param sort_by: A key that exists in all dictionaries in :attr:`lst`, the value will be used to
        sort the list in reverse order
    :param lst: The input list of dictionaries, each dictionary has keys that are strings and
        values that are integers
    :return: A sorted list of dictionaries
    """
    return sorted(lst, key=lambda dictionary: -dictionary[sort_by])
