"""
Count Adjacent Pairs

:Author: BJ Peter Dela Cruz
"""
from itertools import groupby


def count_adjacent_pairs(string: str) -> int:
    """
    This function counts the number of sections that contain repeating words. For example, given
    "dog Dog DOG cat dOg doG", the function will return 2 because there are two sections that
    contain the word dog. Cases are ignored.

    :param string: The input string containing a space-delimited list of words
    :return: The number of sections that contain repeating words
    """
    return sum(1 for _, val in groupby(string.split(), key=lambda x: x.lower())
                if len(list(val)) > 1)
