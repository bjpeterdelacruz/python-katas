"""
Find the Odd Int

:Author: BJ Peter Dela Cruz
"""


def find_it(seq: list[int]):
    """
    This function finds the first integer that appears an odd number of times in :attr:`seq`.

    :param seq: The input list of integers
    :return: The first integer in :attr:`seq` that appears an odd number of times
    """
    if not seq:
        return None
    found = {key: seq.count(key) for key in seq}
    return [(key, value) for key, value in found.items() if value % 2 != 0][0][0]
