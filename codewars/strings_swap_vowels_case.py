"""
Swap Vowels Case

:Author: BJ Peter Dela Cruz
"""


def swap_vowel_case(string: str) -> str:
    """
    This function swaps the case of all the vowels in :attr:`string`, i.e. a lowercase vowel will be
    turned into uppercase, and vice versa. The consonants and non-alphabetic characters are left in
    the string as is.

    :param string: The input string
    :return: The same string
    """
    return ''.join(char.upper() if char in "aeiou" else char.lower() if char in "AEIOU" else char
                   for char in string)
