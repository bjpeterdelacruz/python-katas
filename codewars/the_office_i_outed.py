"""
The Office I: Outed

:Author: BJ Peter Dela Cruz
"""


def outed(meeting: dict[str, int], boss: str) -> str:
    """
    This function determines whether an employee should be fired or commended. If the sum of all
    the other employees' happiness divided by all the employees in :attr:`meeting` is less than or
    equal to 5, then the employee is fired. Otherwise, the employee is commended. A boss is also in
    :attr:`meeting`, and their happiness level counts twice.

    :param meeting: The input dictionary that contains employees and their happiness level
    :param boss: The name of the boss who may or may not be in the meeting
    :return: "Get Out Now!" if the employee is fired, "Nice Work Champ!" otherwise
    """
    total = sum(value for key, value in meeting.items() if key != boss)
    if boss in meeting:
        total += meeting[boss] * 2
    return "Get Out Now!" if total / len(meeting) <= 5 else "Nice Work Champ!"
